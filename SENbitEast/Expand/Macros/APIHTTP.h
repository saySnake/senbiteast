//
//  APIHTTP.h
//  Senbit
//
//  Created by 张玮 on 2019/12/23.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#ifndef APIHTTP_h
#define APIHTTP_h

/////////////////////////////////////////////////////////////////

/////////////////////////////机服务器配置//////////////////////////

/////////////////////////////////////////////////////////////////


#define DBaseUrl   [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"server"]]
#define WSBaseUrl  [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"SenBitWSSUrl"] description]] //@"https://turkey.senbit.tech"//192.168.123.59
//#define WSBaseUrl  @"ws://192.168.123.59:4000"//

//#define BitZSocket @"wss://ws.zt.com/ws/"

/////////////////////////////////////////////////////////
//////////////                     //////////////////////
//////////////    API地址宏         //////////////////////
//////////////                     //////////////////////
/////////////////////////////////////////////////////////

#pragma mark **************** 首页 ****************
//#ifdef DEBUG
//////生产
////#define LICENCE @"aBzKsSpgmY/HTJR3WM/Jkd/A448P2ET7wS6+XtEHKH3zqBOmT28JMhCFPi1/Af4la6s6TJBwmyiOeJaQsc2zm3fe/PQGBWFWAJWCsgbC7+lPW4zyv6pavRx0r+9zofUyowb+Xz/vLmWWBjkyISYT9dsR8CX+zRUxEipn3IbJxSSOUHFw8qHPL97rvxRwm9Y4W6crEM/ciRz8Pz9m077eOTMghJoiHleFeCNj4KsRLiD8AfSKlZMIoO9XtKEhPlvijNoOc1bAD+nqz6WuZf+xa0NvUkgistB55VnTbGh+3Ru0G5OTDEVDb0GZxXrM/XRrPhCkzC+X77rWbV7Mpjz/qg=="
//
////测试
//#define LICENCE @"OjGJdmHNnvDSTS09bRCFV9aUI1Qz8U9nr7EKUaw4MorB4l/8pf/oIrpbAIFbzSLIo/HHWKBooKQT8C0DnqEQT6GNVISZtjfttgkN9SCw6HSCqaHOz2ElawB2ItEIINCqr8P6Qd/KDDAZmIt8OSsnF0fq+Upivyv6J9hHW7ZG/FyOUHFw8qHPL97rvxRwm9Y4W6crEM/ciRz8Pz9m077eOTMghJoiHleFeCNj4KsRLiD8AfSKlZMIoO9XtKEhPlvijNoOc1bAD+nqz6WuZf+xa0NvUkgistB55VnTbGh+3Ru0G5OTDEVDb0GZxXrM/XRrPhCkzC+X77rWbV7Mpjz/qg=="
//#define FaceAppid @"TIDAIULl"
//
//#else
////生产
//#define LICENCE @"aBzKsSpgmY/HTJR3WM/Jkd/A448P2ET7wS6+XtEHKH3zqBOmT28JMhCFPi1/Af4la6s6TJBwmyiOeJaQsc2zm3fe/PQGBWFWAJWCsgbC7+lPW4zyv6pavRx0r+9zofUyowb+Xz/vLmWWBjkyISYT9dsR8CX+zRUxEipn3IbJxSSOUHFw8qHPL97rvxRwm9Y4W6crEM/ciRz8Pz9m077eOTMghJoiHleFeCNj4KsRLiD8AfSKlZMIoO9XtKEhPlvijNoOc1bAD+nqz6WuZf+xa0NvUkgistB55VnTbGh+3Ru0G5OTDEVDb0GZxXrM/XRrPhCkzC+X77rWbV7Mpjz/qg=="
//#define FaceAppid @"IDAHF2b1"
//
//#endif

#define FaceAppid @"IDAHF2b1"




#pragma mark *********解析
//图片二维码
#define qrcodeImagUrl  @"https://me.senbit.cc"
//信息
#define FailurMessage error[@"message"]
//code200
#define SuccessCode [responseObject[@"code"] intValue]
//成功信息
#define SuccessMessage responseObject[@"message"]

#pragma mark *********************** 土耳其站最新接口 ************************
//wss
#define WssUrl [NSString stringWithFormat:@"%@",WSBaseUrl]
//极验初始数据 ok
#define api_1 [NSString stringWithFormat:@"%@/api/v3/market/common/geetest-captcha",DBaseUrl]
//获取手机验证码 ok
#define EastReciveSMS [NSString stringWithFormat:@"%@/api/v3/market/common/sms",DBaseUrl]
//获取邮箱验证码 ok
#define EastReciveEmail [NSString stringWithFormat:@"%@/api/v3/market/common/email",DBaseUrl]
//校验验证码，获取authToken ok
#define EastAuthToken [NSString stringWithFormat:@"%@/api/v3/market/common/auth-token",DBaseUrl]
//忘记密码
#define EastForgetPassword [NSString stringWithFormat:@"%@/api/v3/market/user/forget-password",DBaseUrl]
//文件位置
#define EastFiels [NSString stringWithFormat:@"%@/api/v3/market/common/files/",DBaseUrl]






#pragma  mark - User
//退出登录
#define EastLoginOut [NSString stringWithFormat:@"%@/api/v3/market/user/logout",DBaseUrl]
//用户首次登录
#define EastFirstLogin [NSString stringWithFormat:@"%@/api/v3/market/user/login",DBaseUrl]
//注册用户 ok
#define EastRegister [NSString stringWithFormat:@"%@/api/v3/market/user/users",DBaseUrl]
//二次登录认证
#define EastTwoAuth [NSString stringWithFormat:@"%@/api/v3/market/user/login-verify",DBaseUrl]
//找回密码根据自身绑定关系显示几个安全认证
#define EastInfoType [NSString stringWithFormat:@"%@/api/v3/market/user/user-info",DBaseUrl]
//ping
#define EastPing [NSString stringWithFormat:@"%@/api/v3/market/user/ping",DBaseUrl]



#pragma mark - Currency
#define EastOrders [NSString stringWithFormat:@"%@/api/v3/market/currency/orders",DBaseUrl]
//根据订单号及市场取消单笔委托 Patch
#define EastTradeOrder [NSString stringWithFormat:@"%@/api/v3/market/currency/trade-orders/",DBaseUrl]
//获取当前用户的委托
#define EastEntrustOrders [NSString stringWithFormat:@"%@/api/v3/market/currency/entrust-orders",DBaseUrl]
//获取当前用户的历史委托
#define EastEntrustHisOrders [NSString stringWithFormat:@"%@/api/v3/market/currency/entrust-orders",DBaseUrl]
//获取当前用户的历史成交
#define EastTradeHisOrders [NSString stringWithFormat:@"%@/api/v3/market/currency/trade-orders",DBaseUrl]
//获取当前用户的委托,订单用
#define EastEntrustOrderList [NSString stringWithFormat:@"%@/api/v3/market/currency/entrust-order-list",DBaseUrl]
//获取委托详情
#define EastMarkPairsDetail [NSString stringWithFormat:@"%@/api/v3/market/currency/entrust-orders/",DBaseUrl]




#pragma  mark - Coins
//获取币对列表
#define EastPairs [NSString stringWithFormat:@"%@/api/v3/market/coin/pairs",DBaseUrl]
//查询用户自选币对列表
#define EastOptionalPairs [NSString stringWithFormat:@"%@/api/v3/market/coin/optional-pairs",DBaseUrl]
//添加用户自选币对 post
#define EastAddOptionalPairs [NSString stringWithFormat:@"%@/api/v3/market/coin/optional-pairs",DBaseUrl]
//删除用户自选币对
#define EastDeleOptionalPairs [NSString stringWithFormat:@"%@/api/v3/market/coin/optional-pairs/",DBaseUrl]





#pragma  mark - UserCenter
//获取当前登录用户信息
#define EastLoginInfo [NSString stringWithFormat:@"%@/api/v3/market/user-center/current",DBaseUrl]
//更换手机
#define EastblindPhone [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/phone",DBaseUrl]
//绑定邮箱
#define EastblindEmail [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/email",DBaseUrl]
//设置资金密码
#define EastSetTradePwd [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/trade-password",DBaseUrl]
//重置资金密码
#define EastReSetTradePwd [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/trade-password",DBaseUrl]
//根据当前登录用户生成谷歌密钥
#define EastGoogleInfo [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/google-secrets",DBaseUrl]
//更换谷歌密钥
#define EastResetGoogle [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/google-secrets",DBaseUrl]

//用户更新登录密码
#define EastPassword [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/password",DBaseUrl]
//实名认证
#define EastReal [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/realname",DBaseUrl]
//获取实名认证上传时需要的数据
#define EastUploadImg [NSString stringWithFormat:@"%@/api/v3/market/user-center/sign-put-object",DBaseUrl]
//高级实名认证
#define EastHeiRealname [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/strict-realname",DBaseUrl]
//绑定谷歌密钥
#define EastBlindGoogle [NSString stringWithFormat:@"%@/api/v3/market/user-center/current/google-secrets",DBaseUrl]




#pragma mark - Content
//获取首页banner列表
#define EastBanner [NSString stringWithFormat:@"%@/api/v3/market/content/banners",DBaseUrl]
//版本更新接口
#define EastVersion [NSString stringWithFormat:@"%@/api/v3/market/content/apps/latest",DBaseUrl]
//查询公告列表
#define EastArtcles [NSString stringWithFormat:@"%@/api/v3/market/content/announce/articles",DBaseUrl]







#pragma  mark - Assets
//获取个人资产列表
#define EastAsserts [NSString stringWithFormat:@"%@/api/v3/market/asset/assets",DBaseUrl]
//Assets 获取汇率
#define EastRate [NSString stringWithFormat:@"%@/api/v3/market/asset/exchange/rate",DBaseUrl]
//获取充币地址
#define EastDeposit [NSString stringWithFormat:@"%@/api/v3/market/asset/charge/address/",DBaseUrl]
//获取充币/提币 币种
#define EastCoinList [NSString stringWithFormat:@"%@/api/v3/market/coin/coins",DBaseUrl]
//获取每个币种所有链的提币额度
#define EastWithLimit [NSString stringWithFormat:@"%@/api/v3/market/asset/withdraw/amount/",DBaseUrl]
//提币
#define EastWithDraw [NSString stringWithFormat:@"%@/api/v3/market/asset/withdraw",DBaseUrl]
//充提历史
#define EastHislist [NSString stringWithFormat:@"%@/api/v3/market/asset/assets/history",DBaseUrl]
//根据币种获取个人资产
#define EastCoinAssert [NSString stringWithFormat:@"%@/api/v3/market/asset/assets/",DBaseUrl]
//获取提币地址列表 get
#define EastWithDrawAddress [NSString stringWithFormat:@"%@/api/v3/market/asset/withdraw/address/",DBaseUrl]
//新增提币地址 post
#define EastAddaddress [NSString stringWithFormat:@"%@/api/v3/market/asset/withdraw/address",DBaseUrl]
//根据地址删除充币地址 dele
#define EastDeleAddaddress [NSString stringWithFormat:@"%@/api/v3/market/asset/withdraw/address/",DBaseUrl]
//取消提币
#define EastcancelWithDraw [NSString stringWithFormat:@"%@/api/v3/market/asset/withdraws/",DBaseUrl]






//
////注册接口，sms
//#define SenPhoneRegisterSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/reg-sms",DBaseUrl]
////注册接口 emil sms
//#define SenEmailRegisterSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/reg-email",DBaseUrl]
////注册
//#define SenRegnerUrl [NSString stringWithFormat:@"%@/api/v1/market/user/sign-up",DBaseUrl]
////找回密码
//#define SenResetAccountUrl [NSString stringWithFormat:@"%@/api/v1/market/user/reset-password/account",DBaseUrl]
////手机号找回密码短信验证码
//#define SenBackPhoneSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/forgot-sms",DBaseUrl]
////忘记密码邮箱短信
//#define SenBackEmailSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/forgot-email",DBaseUrl]
////验证手机号重置密码
//#define SenResetPasswordVerify [NSString stringWithFormat:@"%@/api/v1/market/user/reset-password/verify",DBaseUrl]
////重置密码接口
//#define SenResetPassword [NSString stringWithFormat:@"%@/api/v1/market/user/reset-password/reset",DBaseUrl]
////二次验证接口
//#define SenLoginTwouser [NSString stringWithFormat:@"%@/api/v1/market/user/sign-in",DBaseUrl]
////登录邮箱验证码
//#define SenLoginEmailSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/login-email",DBaseUrl]
////登录短信验证码
//#define SenLoginMsgSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/login-sms",DBaseUrl]
////登录
//#define SenLoginPhoneLogin [NSString stringWithFormat:@"%@/api/v1/common/verify-code/login-verify",DBaseUrl]
////登录验签
//#define SenLoginSignVictory [NSString stringWithFormat:@"%@/api/v1/market/user/sign-in-verify",DBaseUrl]
////测试登录接口 get  ..暂时不需要
//#define SenLoginPing [NSString stringWithFormat:@"%@/api/v1/market/user/ping",DBaseUrl]
////个人信息接口 get
//#define SenLoginUserInfo [NSString stringWithFormat:@"%@/api/v1/market/user/info",DBaseUrl]
////退出登录
//#define SenLoginOut [NSString stringWithFormat:@"%@/api/v1/market/user/sign-out",DBaseUrl]
////判断手机号是否被注册过 get
//#define SenPhoneExist [NSString stringWithFormat:@"%@/api/v1/member/phone/exist",DBaseUrl]
////资金找回密码emial
//#define SenFundVerify [NSString stringWithFormat:@"%@/api/v1/common/verify-code/email",DBaseUrl]
////找回资金密码邮箱
//#define SenFundVerifyEmail [NSString stringWithFormat:@"%@/api/v1/common/verify-code/verify-email",DBaseUrl]
////设置资金密码
//#define SenSetFund [NSString stringWithFormat:@"%@/api/v1/member/trade-password/set",DBaseUrl]
////设置资金密码短信
//#define SenSetFundPhoneSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/sms",DBaseUrl]
////资金密码Phone确认获取key
//#define SenVerifyFundSms [NSString stringWithFormat:@"%@/api/v1/common/verify-code/verify-sms",DBaseUrl]
////设置资金密码google 获取key
//#define SenSetFunGoogleSMS [NSString stringWithFormat:@"%@/api/v1/common/verify-code/verify-google",DBaseUrl]
////修改登录密码
//#define SenLoginPasswordModify [NSString stringWithFormat:@"%@/api/v1/member/login-password/modify",DBaseUrl]
////绑定支付宝
//#define SenPaymentAlipay [NSString stringWithFormat:@"%@/api/v1/member/payment/alipay",DBaseUrl]
////实名认证
//#define SenRealName [NSString stringWithFormat:@"%@/api/v1/member/real-name",DBaseUrl]
////图片下载地址(需要拼接图片)
//#define SenaAliImgDownLoad [NSString stringWithFormat:@"%@/api/v1/s3-download/",DBaseUrl]
////图片上传（获取requesturl）
//#define SenaImgSignInput [NSString stringWithFormat:@"%@/api/v1/s3-sign-put",DBaseUrl]
////绑定银行卡支付方式
//#define SenaBlindBankStyle [NSString stringWithFormat:@"%@/api/v1/member/payment/bank-card",DBaseUrl]
////绑定微信
//#define SenaBlindWechat [NSString stringWithFormat:@"%@/api/v1/member/payment/wechat",DBaseUrl]
////绑定paypal支付方式 西联 neteller
//#define SenaBlindPaypal [NSString stringWithFormat:@"%@/api/v1/member/payment/other",DBaseUrl]
////是否用sgt支付交易手续费
//#define SenSGTPayFee [NSString stringWithFormat:@"%@/api/v1/market/user/set-ptb",DBaseUrl]
////高级实名认证状态（get）
//#define SenHightRealName [NSString stringWithFormat:@"%@/api/v1/member/strict-real-name",DBaseUrl]
////区号匹配操作
//#define SenCountryCode [NSString stringWithFormat:@"%@/api/v1/common/country-code",DBaseUrl]
////高级实名认证状态提交
//#define SenSendHightRealName [NSString stringWithFormat:@"%@/api/v1/member/strict-real-name",DBaseUrl]
////ALiYun OSS 高级实名认证 图片上传地址签名
//#define SenSendHightSignput [NSString stringWithFormat:@"%@/api/v1/s3-u-sign-put",DBaseUrl]
////生成绑定谷歌二维码图片get
//#define SenGoogleBlindImg [NSString stringWithFormat:@"%@/api/v1/member/google-authenticator/request",DBaseUrl]
////绑定谷歌验证
//#define SenGoogleBlind [NSString stringWithFormat:@"%@/api/v1/member/google-authenticator/verify",DBaseUrl]
////关闭谷歌验证
//#define SenClocseGoogleBlind [NSString stringWithFormat:@"%@/api/v1/member/google-authenticator/close",DBaseUrl]
////手机号绑定
//#define SenBlindPhone [NSString stringWithFormat:@"%@/api/v1/member/phone/bind",DBaseUrl]
////手机修改接口 put请求
//#define SenChangePhone [NSString stringWithFormat:@"%@/api/v1/member/phone/change",DBaseUrl]
////绑定邮箱操作
//#define SenBlindEmail [NSString stringWithFormat:@"%@/api/v1/member/email/bind",DBaseUrl]
////获取邀请人以及邀请返佣数量接口 get
//#define SenInvotedPerson [NSString stringWithFormat:@"%@/api/v1/member/invite/summary",DBaseUrl]
////获取个人的邀请链接 get
//#define SenMyInvotelink [NSString stringWithFormat:@"%@/api/v1/member/invite/info",DBaseUrl]
////邀请记录 get
//#define SenInvotelist [NSString stringWithFormat:@"%@/api/v1/member/invite/list",DBaseUrl]
////返佣记录 get
//#define SenInvoteCommission [NSString stringWithFormat:@"%@/api/v1/member/invite/commission",DBaseUrl]
////邀请排行榜top3
//#define SenInvoteTop [NSString stringWithFormat:@"%@/api/v1/member/invite/top3",DBaseUrl]
////所有工会列表 get
//#define SenGuildList [NSString stringWithFormat:@"%@/api/v1/guilds",DBaseUrl]
////申请公会
//#define SenGuildApplication [NSString stringWithFormat:@"%@/api/v1/guild/application",DBaseUrl]
////加入公会
//#define SenGuildApplyUser [NSString stringWithFormat:@"%@/api/v1/guild/application/user",DBaseUrl]
////退出公会 put 需要拼接字符串/drop-out
//#define SenGuildDropOut [NSString stringWithFormat:@"%@/api/v2/guild/",DBaseUrl]
////公会详情 get 需要拼接自己的guildID
//#define SenGuildDetil [NSString stringWithFormat:@"%@/api/v2/guild/",DBaseUrl]
////获取其他公会详情 get 拼接guildid
//#define SenOtherDetail [NSString stringWithFormat:@"%@/api/v1/guild-dashboard/",DBaseUrl]
////资产列表接口 get
//#define SenAssets [NSString stringWithFormat:@"%@/api/v1/assets",DBaseUrl]
//#define SenAssetss [NSString stringWithFormat:@"%@/api/v1/assets",DBaseUrl]
//
////提币记录 get 需要拼接某个字段
//#define SenWithDrawCoin [NSString stringWithFormat:@"%@/api/v1/assets/withdraw/",DBaseUrl]
////获取汇率接口 get
//#define SenCurrencyConfig [NSString stringWithFormat:@"%@/api/v1/currency-config/lang",DBaseUrl]
////获取币种的充币记录 get 需要拼接某个币种
//#define SenWithDepositCoin [NSString stringWithFormat:@"%@/api/v1/assets/charge/",DBaseUrl]
////获取充币地址 get
//#define SenAssetsAddress [NSString stringWithFormat:@"%@/api/v1/assets/address",DBaseUrl]
////获取当前系统配置的币种列表 get
//#define SenAssetsAddressList [NSString stringWithFormat:@"%@/api/v1/assets/coin-list",DBaseUrl]
////获取当前系统配置的提币币种列表 get
//#define SenAssetsWithDrawList [NSString stringWithFormat:@"%@/api/v1/assets/coin-list",DBaseUrl]
////获取提币资产数据 get 需要拼接每个币的名称
//#define SenWithdrawCoinAssert [NSString stringWithFormat:@"%@/api/v1/assets/balance/",DBaseUrl]
////充币记录 get
//#define SenDepositList [NSString stringWithFormat:@"%@/api/v1/assets/charge",DBaseUrl]
////提币记录 get
//#define SenWithDrawitList [NSString stringWithFormat:@"%@/api/v1/assets/withdraw",DBaseUrl]
////资金密码验证
//#define SentradePasswordVerify [NSString stringWithFormat:@"%@/api/v1/member/trade-password/verify",DBaseUrl]
////提币验证资金密码
//#define SenAssetsWithDrawVerify [NSString stringWithFormat:@"%@/api/v1/assets/withdraw-verify",DBaseUrl]
////获取提币地址列表 get
//#define SenWithDrawAddress [NSString stringWithFormat:@"%@/api/v1/assets/withdraw-address",DBaseUrl]
////提交提币接口 post
//#define SenAssetsWithDraw [NSString stringWithFormat:@"%@/api/v1/assets/withdraw",DBaseUrl]
////删除提币地址 DELET (需要拼接某个地址)
//#define SenDeleWithDrawAddress [NSString stringWithFormat:@"%@/api/v1/assets/withdraw-address/",DBaseUrl]
////获取后台配置banner
//#define SenHomeBanner [NSString stringWithFormat:@"%@/api/v1/home-banner",DBaseUrl]
////获取公告文章列表
//#define SenHomeAnnounce [NSString stringWithFormat:@"%@/api/v1/announce/articles",DBaseUrl]
////获取otc 列表的币种get
//#define SenOtcCoinList [NSString stringWithFormat:@"%@/api/v1/otc/coin/list",DBaseUrl]
////otc买方的数据 get unit=? & transtType=? //otc卖方的数据 get unit=ETH&transType=buy
//#define SenOtcBuyCoin [NSString stringWithFormat:@"%@/api/v1/otc/notice",DBaseUrl]
////otc 买币下单
//#define SenOtcBuy [NSString stringWithFormat:@"%@/api/v1/otc/order/add",DBaseUrl]
////otc get数据源
//#define SenGetOtcBuy [NSString stringWithFormat:@"%@/api/v1/otc/order/get/",DBaseUrl]
////付款
//#define SenOrderPay [NSString stringWithFormat:@"%@/api/v1/otc/order/pay",DBaseUrl]
////取消交易
//#define SenOrderCancel [NSString stringWithFormat:@"%@/api/v1/otc/order/cancel",DBaseUrl]
////历史订单记录 get
////#define SenHisOrderList [NSString stringWithFormat:@"%@/api/v1/otc/order/list",DBaseUrl]
////法币交易订单当前订单 get
//#define SenOTCCurrentOrderList [NSString stringWithFormat:@"%@/api/v1/otc/order/list",DBaseUrl]
////otc放行
//#define SenOrderRelease [NSString stringWithFormat:@"%@/api/v1/otc/order/release",DBaseUrl]
//
////获取币对精度列表 get
//#define SenCoinPair [NSString stringWithFormat:@"%@/api/v1/coin/pair",DBaseUrl]
////获取自选商品列表
//#define SenPairCollection [NSString stringWithFormat:@"%@/api/v1/coin/pair/collection",DBaseUrl]
////获取币币交易当前交易信息 get
//#define SenCoinPairMsg [NSString stringWithFormat:@"%@/api/v1/market/coin-pair/",DBaseUrl]
////获取币币交易当前资产信息 get
//#define SenCurrentAssert [NSString stringWithFormat:@"%@/api/v1/assets/balance/",DBaseUrl]
////获取币币历史记录 get
//#define SenCurrentHistory [NSString stringWithFormat:@"%@/api/v1/coin/orders/list",DBaseUrl]
////获取币币订单明细历史手续费
//#define SenCurrentFeehistory [NSString stringWithFormat:@"%@/api/v1/coin/trades/list",DBaseUrl]
////币币交易撤销当前订单
//#define SenCurrenCancelOrder [NSString stringWithFormat:@"%@/api/v1/coin/orders/cancel",DBaseUrl]
////币币下单当前委托列表
//#define SenCurrenOrdersList [NSString stringWithFormat:@"%@/api/v1.1/coin/orders-redis/list",DBaseUrl]
////币币下单接口
//#define SenCurrenOrder [NSString stringWithFormat:@"%@/api/v1/coin/orders/create",DBaseUrl]
////全部撤销接口
//#define SenOrdersRevoke [NSString stringWithFormat:@"%@/api/v1/coin/orders/revoke",DBaseUrl]
////获取合约的交易币种 get
//#define SenTradeCoinPair [NSString stringWithFormat:@"%@/api/v1/futures/contracts",DBaseUrl]
////合约费率
//#define SenContractFee [NSString stringWithFormat:@"%@/api/v1/futures/fee-rate",DBaseUrl]
////获取合约资产 get
//#define SenContractAssets [NSString stringWithFormat:@"%@/api/v1/futures/order-mock/assets",DBaseUrl]
////获取虚拟合约l当前订单列表
//#define SenContractList [NSString stringWithFormat:@"%@/api/v1/futures/position",DBaseUrl]
////提交合约保证金
//#define SenContractMargin [NSString stringWithFormat:@"%@/api/v1/futures/order/",DBaseUrl]
////获取合约历史计划 get
//#define SenContractHisPlan [NSString stringWithFormat:@"%@/api/v1/futures/plan-history",DBaseUrl]
////获取合约费用流水 get
//#define SenContractFeeHis [NSString stringWithFormat:@"%@/api/v1/futures/cost-flow",DBaseUrl]
////获取合约成交明细 get
//#define SenContractOrderDetail [NSString stringWithFormat:@"%@/api/v1/futures/position-txn",DBaseUrl]
////币币交易的收藏按钮
//#define SenCollectionCoin [NSString stringWithFormat:@"%@/api/v1/coin/pair/collection/",DBaseUrl]
////极速开多模拟 or开空
//#define SenSpeedMockMore [NSString stringWithFormat:@"%@/api/v1/futures/order",DBaseUrl]
////计划开仓模拟
//#define SenPlanMockMore [NSString stringWithFormat:@"%@/api/v1/futures/order-plan",DBaseUrl]
////计划列表
//#define SenPlanMorelist [NSString stringWithFormat:@"%@/api/v1/futures/plan",DBaseUrl]
////撤单虚拟计划开仓
//#define SenRemoveFuture [NSString stringWithFormat:@"%@/api/v1/futures/order-plan/",DBaseUrl]
////合约的交割申请
//#define SenContractAppliction [NSString stringWithFormat:@"%@/api/v1/futures/",DBaseUrl]
////极速反手
//#define SenContractSpeedReverse [NSString stringWithFormat:@"%@/api/v1/futures/order/",DBaseUrl]
////极速平仓
//#define SenContractSpeedClose [NSString stringWithFormat:@"%@/api/v1/futures/",DBaseUrl]
////合约的交割记录
//#define SenContractDeliveryList [NSString stringWithFormat:@"%@/api/v1/futures/delivery-txn",DBaseUrl]
////帮助中心
//#define SenHelpCenter @"https://senbit2019.zendesk.com/hc/zh-cn/sections/360007641613-%E5%B8%AE%E5%8A%A9"
////合约保险列表 get
//#define SenContractProtf [NSString stringWithFormat:@"%@/api/v1/futures/lock/list",DBaseUrl]
////合约统计 get
//#define SenContracCircle [NSString stringWithFormat:@"%@/api/v1/futures/lock/statistical",DBaseUrl]
////理财banner get
//#define SenLicaiBanner [NSString stringWithFormat:@"%@/api/v1/money-banner",DBaseUrl]
////理财专属推荐 get
//#define SenLicaiIncome [NSString stringWithFormat:@"%@/api/v2/wealth/production/recommend",DBaseUrl]
////理财精选 get
//#define SenLicaiSpecial [NSString stringWithFormat:@"%@/api/v2/wealth/production/hight",DBaseUrl]
////进行中理财 get
//#define SenLicaido [NSString stringWithFormat:@"%@/api/v2/wealth/production/list/",DBaseUrl]
////根据id 获取产品的详情
//#define SenLicaiDetail [NSString stringWithFormat:@"%@/api/v2/wealth/production/",DBaseUrl]
////购买理财产品
//#define SenLicaiDetailBuy [NSString stringWithFormat:@"%@/api/v2/wealth/order",DBaseUrl]
////查看自己购买的理财产品所有的收益 get
//#define SenLicaiMyDetail [NSString stringWithFormat:@"%@/api/v2/wealth/gains",DBaseUrl]
////查看自己购买的理财产品所有的收益 get
//#define SenLicaiMyTotalGain [NSString stringWithFormat:@"%@/api/v2/wealth/gain/latest",DBaseUrl]
////查看自己购买的理财产品
//#define SenLicaiOWnerBuy [NSString stringWithFormat:@"%@/api/v2/wealth/order/list",DBaseUrl]
////手动领取stz
//#define SenLingQuStz [NSString stringWithFormat:@"%@/api/v1/futures/lock/unlock",DBaseUrl]
////根据交易对获取法币参考价格 get
//#define SenSymbolOtcPrice [NSString stringWithFormat:@"%@/api/v1/otc/one/symbol/price",DBaseUrl]
////一键下单选择收款方式
//#define SenOTCPayStyle [NSString stringWithFormat:@"%@/api/v1/otc/one/pay",DBaseUrl]
////app获取faceid等信息 get
//#define SenRealFaceID [NSString stringWithFormat:@"%@/api/v1/member/wxbank/getFaceId",DBaseUrl]
////app 人脸结果校验
//#define SenFaceResult [NSString stringWithFormat:@"%@/api/v1/member/wxbank/result/verify",DBaseUrl]
////用户入金出金下单
//#define SenOtcIncomOrder [NSString stringWithFormat:@"%@/api/v1/otc/one/createOrder",DBaseUrl]
////检测是否需要做更高级kyc认证或者人脸识别
//#define SenOtcUUEX [NSString stringWithFormat:@"%@/api/v1/otc/uuex/detect",DBaseUrl]
////订单详情 get(orderNum)
//#define SenOtcInfomation [NSString stringWithFormat:@"%@/api/v1/otc/one/order/info/",DBaseUrl]
////根据payId获取支付信息 get
//#define SenOtcPayInfo [NSString stringWithFormat:@"%@/api/v1/otc/one/payment/info/",DBaseUrl]
////用户取消购买订单 put
//#define SenOtcCancleSpeed [NSString stringWithFormat:@"%@/api/v1/otc/one/order/cancel/",DBaseUrl]
////标记为已付款接口
//#define SenOtcPaySpeed [NSString stringWithFormat:@"%@/api/v1/otc/one/order/paid",DBaseUrl]
////获取用户收款方式 get
//#define SenOtcReciveCardStyle [NSString stringWithFormat:@"%@/api/v1/otc/one/payment/list",DBaseUrl]
////标记已经打币 放行接口 put
//#define SenOtcPaidCoin [NSString stringWithFormat:@"%@/api/v1/otc/one/order/paid/coin",DBaseUrl]
////自由交易
////查询委托列表
//#define SenOtcUUexNotice [NSString stringWithFormat:@"%@/api/v1/otc/uuex/notice",DBaseUrl]
////下单接口
//#define SenOtcUUexaddOrder [NSString stringWithFormat:@"%@/api/v1/otc/uuex/addOrder",DBaseUrl]
////根据订单号查询支付方式
//#define SenOtcOrderPay [NSString stringWithFormat:@"%@/api/v1/otc/order/pay",DBaseUrl]
////自由交易买币接口
//#define SenOtcOrderUUEXPaid [NSString stringWithFormat:@"%@/api/v1/otc/uuex/paid",DBaseUrl]
////法币交易订单记录
////获取订单列表
//#define SenOtcOrderList [NSString stringWithFormat:@"%@/api/v1/otc/one/order/list",DBaseUrl]
////申诉接口
//#define SenOtcOrderAppeal [NSString stringWithFormat:@"%@/api/v1/otc/uuex/appeal",DBaseUrl]
////取消申诉
//#define SenOtcCanCleAppeal [NSString stringWithFormat:@"%@/api/v1/otc/uuex/cancelAppeal",DBaseUrl]
////版本更新接口
//#define SenbitUpdateVersion [NSString stringWithFormat:@"%@/api/v2/app/version",DBaseUrl]
////购买解锁
//#define SenHandLock [NSString stringWithFormat:@"%@/api/v1/futures/lock/unlockBuy",DBaseUrl]
//
#endif /* APIHTTP_h */
