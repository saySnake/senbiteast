//
//  LayCell.m
//  IFMShareDemo
//
//  Created by 张玮 on 2020/3/6.
//  Copyright © 2020 刘刚. All rights reserved.
//

#import "LayCell.h"

@implementation LayCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 150)];
        self.img.clipsToBounds = YES;
        self.img.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.img];
    }
    return self;
}
//+ (instancetype)initWithCollection:(UICollectionView *)collectionView cellPath:(NSIndexPath *)indexPath
//{
//    LayoutCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"album" forIndexPath:indexPath];
//    return cell;
//}

@end
