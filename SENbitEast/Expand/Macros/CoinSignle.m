//
//  CoinSignle.m
//  Senbit
//
//  Created by 张玮 on 2020/8/6.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "CoinSignle.h"

static CoinSignle *shareInstance;

@implementation CoinSignle

+ (CoinSignle *)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[CoinSignle alloc] init];
    });
    return shareInstance;
}

@end
