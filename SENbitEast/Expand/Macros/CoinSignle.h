//
//  CoinSignle.h
//  Senbit
//
//  Created by 张玮 on 2020/8/6.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoinSignle : NSObject

+ (CoinSignle *)shareInstance;
/** coin **/
@property (nonatomic ,strong) NSString * coin;

@property (nonatomic ,assign) BOOL isUpdate;

@end

NS_ASSUME_NONNULL_END
