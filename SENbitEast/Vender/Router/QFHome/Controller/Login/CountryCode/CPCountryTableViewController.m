//
//  CPCountryTableViewController.m
//  CountryPicker
//
//  Created by 吕晴阳 on 2018/9/30.
//  Copyright © 2018 Lv Qingyang. All rights reserved.
//

#import "CPCountryTableViewController.h"
#import "MJExtension.h"
#import "CPCountry.h"
#import "CPIndicatorBar.h"
#import "CPTableViewCell.h"
#define ResourceBundle [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Resource" ofType:@"bundle"]]

@interface CPCountryTableViewController () <UITableViewDataSource, UITableViewDelegate, CPIndicatorBarDelegate>
@property(weak, nonatomic) IBOutlet UITableView *countryTable;
@property(copy, nonatomic) NSMutableArray *countries;
@property(copy, nonatomic) NSArray<NSString *> *titles;
@property(copy, nonatomic) NSArray<NSValue *> *ranges;
@end

@implementation CPCountryTableViewController

+ (void)pickWithViewController:(UIViewController *)viewController callback:(void (^)(CPCountry *country))callback {
    CPCountryTableViewController *tableViewController = [CPCountryTableViewController new];
    tableViewController.callback = callback;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:tableViewController];
    [viewController presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark  -- Init

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithLightColorStr:@"FFFFFF" DarkColor:@"FFFFFF"];
    // Do any additional setup after loading the view from its nib.
    [self initNavigationBar];
    [self initTableView];
    [self initCountry];
    [self initIndicatorBar];
}

- (void)initIndicatorBar {
    CPIndicatorBar *indicatorBar = [CPIndicatorBar new];
    indicatorBar.indicators = self.titles;
    indicatorBar.delegate = self;
    indicatorBar.backgroundColor = [UIColor clearColor];
    self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    CGRect parentFrame = self.view.frame;
    CGRect frame = indicatorBar.frame;
    frame.origin.x = parentFrame.size.width - frame.size.width;
    frame.origin.y = (parentFrame.size.height - frame.size.height) / 2;
    indicatorBar.frame = frame;
    [self.view addSubview:indicatorBar];
}

- (void)initNavigationBar {
    self.navigationItem.title = kLocalizedString(@"country_countryCity");
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = item;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
    UIColor * color = [UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"];
    //这里我们设置的是颜色，还可以设置shadow等，具体可以参见api
    NSDictionary * dict = [NSDictionary dictionaryWithObject:color forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
    self.navigationController.navigationBarHidden = NO;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
    self.navigationController.navigationBar.subviews[0].alpha = 1;
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.subviews[0].alpha = 1;
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
//
    self.navigationController.navigationBarHidden = YES;
}

- (void)initTableView {
    self.countryTable.dataSource = self;
    self.countryTable.delegate = self;
    self.countryTable.backgroundColor = WhiteColor;
    [self.countryTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}

- (NSMutableArray *)countries {
    if (!_countries) {
        _countries = [[NSMutableArray alloc] init];
    }
    return _countries;
}
- (void)initCountry {
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *myBundlePath = [mainBundle pathForResource:@"Resources" ofType:@"bundle"];
    NSBundle *myBundle = [NSBundle bundleWithPath:myBundlePath];
    
    //放在自定义bundle中的图片
    NSString *path = [myBundle pathForResource:@"code" ofType:@"json"];
        NSString *json = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *aa = [CPCountry mj_objectArrayWithKeyValuesArray:json];
    CPCountry *country;
    [aa enumerateObjectsUsingBlock:^(CPCountry * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (self.isResigster == YES) {
        if ([obj.en isEqualToString:@"China"] || [obj.zh isEqualToString:@"中国大陆"] || [obj.locale isEqualToString:@"CN"]) {
                [self.countries removeObject:obj];
        } else {
            [self.countries addObject:obj];
        }
    }
        else {
            [self.countries addObject:obj];
        }
    }];
    

    //分组
    NSMutableArray *titles = [NSMutableArray new];
    NSMutableArray *ranges = [NSMutableArray new];
    NSString *en;
    NSString *lastTitle = nil;
    NSString *curTitle = nil;
    int count = 0;
    for (int i = 0, len = self.countries.count; i < len; i++) {
        country = self.countries[i];
        NSString *lan = kLanguageManager.currentLanguage;
        if ([lan  isEqualToString:@"en"]) {
            en = country.en;
        } else if ([lan isEqualToString:@"zh-CN"]) {
            en = country.zh;
        } else {
            en = country.en;
        }
        if (en && en.length > 0) {
            curTitle = [en substringToIndex:1];
            if ([curTitle isEqualToString:lastTitle]) {
                count++;
            } else {
                if (i != 0) {
                    [titles addObject:lastTitle];
                    [ranges addObject:[NSValue valueWithRange:NSMakeRange(i - count, count)]];
                }
                count = 1;
                lastTitle = curTitle;
            }
        }
    }
    [titles addObject:lastTitle];
    [ranges addObject:[NSValue valueWithRange:NSMakeRange(self.countries.count - count, count)]];

    self.titles = titles;
    self.ranges = ranges;
}

#pragma mark -- TableView data source delegate

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.titles[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CPCountry *country = [self countryFormIndexPath:indexPath];
    CPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCells"];
    cell.backgroundColor = WhiteColor;
    if (!cell) {
        cell = [CPTableViewCell cell];
    }
//    cell.textLabel.text = [NSString stringWithFormat:@"%@ +%d", country.zh, country.code];
    NSString *lan = kLanguageManager.currentLanguage;

    if ([lan isEqualToString:@"en"]) {
        cell.titleName.text = [NSString stringWithFormat:@"%@",country.en];
    } else if ([lan isEqualToString:@"zh-CN"]) {
        cell.titleName.text = [NSString stringWithFormat:@"%@",country.zh];
    }
    cell.titleName.textColor = MainWhiteColor;
    cell.numLabel.text = [NSString stringWithFormat:@"+%d",country.code];
    cell.numLabel.textColor = MainWhiteColor;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.ranges[section].rangeValue.length;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.titles.count;
}

#pragma mark -- TableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CPCountry *country = [self countryFormIndexPath:indexPath];
    self.callback(country);
    [self close];
}


#pragma mark -- action

- (void)close {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Other

- (CPCountry *)countryFormIndexPath:(NSIndexPath *)indexPath {
    unsigned long index = self.ranges[[indexPath indexAtPosition:0]].rangeValue.location + [indexPath indexAtPosition:1];
    return self.countries[index];
}

#pragma mark -- IndicatorBar delegate

- (void)indicatorBar:(CPIndicatorBar *)indicatorBar didSelectAtIndex:(int)index {
    NSUInteger indexes[] = {index, 0};
    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
    [self.countryTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    // 设置section背景颜色
    view.tintColor = ThemeDarkBlack;
    
    // 设置section字体颜色
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:PlaceHolderColor];
}


@end
