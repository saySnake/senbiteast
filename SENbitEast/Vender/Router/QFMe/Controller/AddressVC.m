//
//  AddressVC.m
//  Senbit
//
//  Created by 张玮 on 2020/2/23.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "AddressVC.h"
#import "MMScanViewController.h"
#import "BlindPhoneSheet.h"
#import "WithDrawSheet.h"

@interface AddressVC ()<UITextFieldDelegate>

@property (nonatomic ,strong) UIScrollView *scrol;
@property (nonatomic ,strong) UIView *fotView;
@property (nonatomic ,strong) UIButton *confirmBtn;
@property (nonatomic ,strong) UILabel *addressTitle;
@property (nonatomic ,strong) UITextField *addressText;
@property (nonatomic ,strong) UIButton *saoBtn;
@property (nonatomic ,strong) UILabel *line1;
@property (nonatomic ,strong) UILabel *remarkTitle;
@property (nonatomic ,strong) UITextField *remarkTextField;
@property (nonatomic ,strong) UILabel *line2;
@property (nonatomic ,strong) UILabel *tagTitle;
@property (nonatomic ,strong) UITextField *tagField;
@property (nonatomic ,strong) UILabel *line3;
@end

@implementation AddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUpView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"addCoinAddress")];
    self.addressTitle.text = kLocalizedString(@"addCoinAddress_address");
    NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"addCoinAddress_IOSplace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.addressText.attributedPlaceholder = attrString1;
    self.remarkTitle.text = kLocalizedString(@"addCoinAddress_remark");
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"addCoinAddress_enterMark") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.remarkTextField.attributedPlaceholder = attrString;
    NSAttributedString *attrString2 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"addCoinAddress_longPress") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.tagField.attributedPlaceholder = attrString2;
    [self.confirmBtn setTitle:kLocalizedString(@"withDraw_addAress") forState:UIControlStateNormal];
    self.tagTitle.text = kLocalizedString(@"addCoinAddress_tag");
}

- (void)setNav {
    [self setNavBarTitle:kLocalizedString(@"addCoinAddress")];
    [self setLeftBtnImage:@"leftBack"];
}

- (void)setUpView {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.view addGestureRecognizer:tap];

    self.scrol = [[UIScrollView alloc] init];
    self.scrol.backgroundColor = WhiteColor;
    [self.view addSubview:self.scrol];
    
    self.addressTitle = [[UILabel alloc] init];
    self.addressTitle.textAlignment = 0;
    self.addressTitle.text = kLocalizedString(@"addCoinAddress_address");//kLocalizedString(@"addCoinAddress_address");
    self.addressTitle.textColor = MainWhiteColor;
    self.addressTitle.font = TwelveFontSize;
    [self.scrol addSubview:self.addressTitle];
    
    self.addressText = [[UITextField alloc] init];
    self.addressText.delegate = self;
    self.addressText.font = TwelveFontSize;
    self.addressText.textColor = [UIColor blackColor];
    [self.addressText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"addCoinAddress_enerPress") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.addressText.attributedPlaceholder = attrString1;
    [self.scrol addSubview:self.addressText];
    
    self.saoBtn = [[UIButton alloc] init];
    [self.saoBtn setImage:[UIImage imageNamed:@"Assert_saomiao"] forState:UIControlStateNormal];
    [self.saoBtn addTarget:self action:@selector(saoAction) forControlEvents:UIControlEventTouchUpInside];
    [self.scrol addSubview:self.saoBtn];
    
    self.line1 = [[UILabel alloc] init];
    self.line1.backgroundColor = HEXCOLOR(0xF7F6FB);
    [self.scrol addSubview:self.line1];
    
    self.tagTitle = [[UILabel alloc] init];
    self.tagTitle.text = kLocalizedString(@"Notes");//kLocalizedString(@"addCoinAddress_tag");
    self.tagTitle.textColor = MainWhiteColor;
    self.tagTitle.textAlignment = 0;
    self.tagTitle.font = TwelveFontSize;
    [self.scrol addSubview:self.tagTitle];
    
    self.tagField = [[UITextField alloc] init];
    self.tagField.delegate = self;
    self.tagField.font = TwelveFontSize;
    NSAttributedString *attrString2 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"addCoinAddress_markInfo") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.tagField.attributedPlaceholder = attrString2;
    self.tagField.textColor = [UIColor blackColor];
    [self.scrol addSubview:self.tagField];

    
    self.line2 = [[UILabel alloc] init];
    self.line2.backgroundColor = HEXCOLOR(0xF7F6FB);
    [self.scrol addSubview:self.line2];
    
    
    self.remarkTitle = [[UILabel alloc] init];
    self.remarkTitle.text = kLocalizedString(@"addCoinAddress_tag");//kLocalizedString(@"addCoinAddress_remark");
    self.remarkTitle.textAlignment = 0;
    self.remarkTitle.textColor = MainWhiteColor;
    self.remarkTitle.font = TwelveFontSize;
    [self.scrol addSubview:self.remarkTitle];
    
    self.remarkTextField = [[UITextField alloc] init];
    self.remarkTextField.delegate = self;
    self.remarkTextField.font = TwelveFontSize;
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"addCoinAddress_enerPress") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.remarkTextField.attributedPlaceholder = attrString;
    self.remarkTextField.textColor = [UIColor blackColor];
    [self.scrol addSubview:self.remarkTextField];

    self.line3 = [[UILabel alloc] init];
    self.line3.backgroundColor = HEXCOLOR(0xF7F6FB);
    [self.scrol addSubview:self.line3];
    
    self.fotView = [[UIView alloc] init];
    self.fotView.backgroundColor = WhiteColor;
    [self.view addSubview:self.fotView];
    
    self.confirmBtn = [[UIButton alloc] init];
    [self.confirmBtn setTitle:kLocalizedString(@"addCoinAddress_confirm") forState:UIControlStateNormal];
    self.confirmBtn.cornerRadius = 3;
    self.confirmBtn.titleLabel.font = FourteenFontSize;
    self.confirmBtn.backgroundColor = HEXCOLOR(0xDDDDDD);
    self.confirmBtn.userInteractionEnabled = NO;
    [self.confirmBtn addTarget:self action:@selector(showSheet) forControlEvents:UIControlEventTouchUpInside];
    [self.fotView addSubview:self.confirmBtn];
    [self make_layout];
}



//校验验证码
- (void)indeify:(NSString *)phoneCode emailCode:(NSString *)emailCode googleCode:(NSString *)googleCode phone:(NSString *)phone email:(NSString *)email {
        NSDictionary *dic = @{@"emailCode":emailCode,
                              @"phoneCode":phoneCode,
                              @"googleCode":googleCode,
                              @"type":@"CREATE_WITHDRAW_ADDRESS"
        };
        WS(weakSelf);
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                [weakSelf requieAuthToken:responseObject[@"data"][@"authToken"]];
            } else {
                [weakSelf showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [weakSelf showToastView:FailurMessage];
        }];
}


- (void)showSheet {
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    BlindPhoneSheet *sheet = [[BlindPhoneSheet alloc] init];
    sheet.type = BlindTypeTradeAddress;
    sheet.googleAuth = info.googleAuthEnable;
    sheet.emailAuth = info.emailAuthEnable;
    sheet.phoneAuth = info.phoneAuthEnable;
    sheet.emailTitleName.text = info.email;
    sheet.phoneLabel.text = info.phone;
    sheet.clickBlock = ^(NSString * _Nonnull phoneCode, NSString * _Nonnull emailCode, NSString * _Nonnull googleCode, NSString * _Nonnull phone, NSString * _Nonnull email, NSString * _Nonnull userToken) {
        if (phoneCode.length == 0) {
            phoneCode = @"";
        }
        if (emailCode.length == 0) {
            emailCode = @"";
        }
        if (googleCode.length == 0) {
            googleCode = @"";
        }
        if (email.length == 0) {
            email = @"";
        }
        if (phone.length == 0) {
            phone = @"";
        }
        NSDictionary *dic = @{@"email":info.email,
                              @"emailCode":emailCode,
                              @"phone":info.phone,
                              @"phoneCode":phoneCode,
                              @"googleCode":googleCode,
                              @"type":@"CREATE_WITHDRAW_ADDRESS"
        };
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                [self requieAuthToken:responseObject[@"data"][@"authToken"]];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    };
    [sheet showFromView:self.view];

}

- (void)requieAuthToken:(NSString *)token {
    NSDictionary *dic = @{@"authToken":token,
                          @"title":self.tagField.text,
                          @"address":self.addressText.text,
                          @"coin":self.model.coinName,
                          @"destinationTag":self.remarkTextField.text
                        };
    [SBNetworTool postWithUrl:EastAddaddress params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
    
}


- (void)saoAction {
    MMScanViewController *scanVc = [[MMScanViewController alloc] initWithQrType:MMScanTypeQrCode onFinish:^(NSString *result, NSError *error) {
        if (error) {
            NSLog(@"error: %@",error);
        } else {
            NSLog(@"扫描结果：%@",result);
            self.addressText.text = result;
            self.confirmBtn.userInteractionEnabled = YES;
            self.confirmBtn.backgroundColor = ThemeGreenColor;
//            [self showInfo:result];
        }
    }];
    [self.navigationController pushViewController:scanVc animated:YES];

}
- (void)make_layout {

    [self.scrol mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.bottom.mas_equalTo(-100);
        make.width.mas_equalTo(DScreenW);
        make.height.mas_equalTo(DScreenH - kNavBarAndStatusBarHeight);
    }];
        
    [self.addressTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(15);
    }];

    [self.addressText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.addressTitle.mas_bottom).offset(20);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(20);
    }];
    
    [self.saoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addressText.mas_top);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-20);
    }];
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addressText.mas_bottom).offset(5);
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(DScreenW -10);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    
    [self.tagTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.line1.mas_bottom).offset(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(15);
    }];

    [self.tagField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.tagTitle.mas_bottom).offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
    }];

    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagField.mas_bottom).offset(5);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW -10);
        make.height.mas_equalTo(0.5);
    }];

    
    if (self.model.isDestinationTagRequired) {
        [self.remarkTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(100);
            make.top.mas_equalTo(self.line2.mas_bottom).offset(10);
        }];

        [self.remarkTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.remarkTitle.mas_bottom).offset(10);
            make.width.mas_equalTo(200);
        }];
            
        [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(DScreenW -10);
            make.height.mas_equalTo(0.5);
            make.top.mas_equalTo(self.remarkTextField.mas_bottom).offset(5);
            make.bottom.mas_equalTo(0);
        }];
    }
    
    
    [self.fotView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.view.mas_bottom).offset(-100);
        make.height.mas_equalTo(100);
    }];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(50);
    }];
}

#pragma mark - Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self hideKeyboard];
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.addressText.text.length > 0) {
        self.confirmBtn.userInteractionEnabled = YES;
        self.confirmBtn.backgroundColor = ThemeGreenColor;
    } else {
        self.confirmBtn.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.confirmBtn.userInteractionEnabled = NO;
    }
}

#pragma mark - Event response
- (void)tapAction {
    [self hideKeyboard];
}

#pragma mark - Pravite Method
- (void)hideKeyboard {

    [self.view endEditing:YES];
}



@end
