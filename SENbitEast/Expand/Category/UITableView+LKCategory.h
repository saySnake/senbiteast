//
//  UITableView+LKCategory.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/*
 * [self.tbView showNoDataCount:self.dataArray.count image:@""];
 */
@interface UITableView (LKCategory)
/**
 根据count显示s提示信息（无数据，网络不可用，网络请求失败）
 @param count 个数
 @param name 图片名称
 */
- (void)showNoDataCount:(NSInteger)count image:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
