//
//  LeftMenuView.m
//  SENbitEast
//
//  Created by 张玮 on 2021/2/24.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "LeftMenuView.h"
#import "FSCustomButton.h"
#import "LeftMenuView.h"
#import "SettingVC.h"
#import "LeftMenuCell.h"
#import "LoginViewController.h"
#import "QFRouter.h"
#import "QFMoudleProtocol.h"
#import "TipsView.h"

#define ImageviewWidth    18
#define Frame_Width       self.frame.size.width//200


@interface LeftMenuView ()<UITableViewDataSource,UITableViewDelegate>

/** headView **/
@property (nonatomic ,strong) UIView *headView;

/** setBtn **/
@property (nonatomic ,strong) FSCustomButton *setBtn;


@property (nonatomic ,strong) UITableView* contentTableView;
@property (nonatomic ,strong) UIView *logView;
@property (nonatomic ,strong) BaseLabel *loginLb;
@property (nonatomic ,strong) FSCustomButton *wellBtn;


@property (nonatomic ,strong) BaseButton *unLoginBtn;
@property (nonatomic ,strong) BaseLabel *uidLb;

@property (nonatomic ,strong) FSCustomButton *accountBtn;
@property (nonatomic ,strong) FSCustomButton *realBtn;
@property (nonatomic ,strong) FSCustomButton *invotedBtn;

/** titleArray **/
@property (nonatomic ,strong) NSMutableArray *titleArray;
@property (nonatomic ,strong) NSMutableArray *imgArray;

@end

@implementation LeftMenuView

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self setupNotify];
        [self initView];
    }
    return  self;
}

- (void)dealloc {
    [IWNotificationCenter removeObserver:self name:KLoadLeftView object:nil];
    [IWNotificationCenter removeObserver:self];
}

- (void)setupNotify {
    [IWNotificationCenter addObserver:self selector:@selector(initView) name:KLoadLeftView object:nil];
    [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)changeLanguage {
    [self.unLoginBtn setTitle:kLocalizedString(@"left_login") forState:UIControlStateNormal];
    self.uidLb.text = kLocalizedString(@"left_welcome");
    [self.accountBtn setTitle:kLocalizedString(@"left_account") forState:UIControlStateNormal];
    [self.realBtn setTitle:kLocalizedString(@"left_real") forState:UIControlStateNormal];
    [self.invotedBtn setTitle:kLocalizedString(@"left_invoted") forState:UIControlStateNormal];
    self.titleArray = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"left_order"),kLocalizedString(@"left_abount"),kLocalizedString(@"left_sgt"),nil];
    [self.contentTableView reloadData];
}

- (void)initView {
    [self.headView removeFromSuperview];
    [self.logView removeFromSuperview];
    [self.unLoginBtn removeFromSuperview];
    [self.uidLb removeFromSuperview];
    
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    [self addSubview:self.headView];
    [self.headView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(Frame_Width);
        make.height.mas_equalTo(300);
    }];
    
    [self.headView addSubview:self.setBtn];
    [self.setBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(Frame_Width).offset(-20);
        make.height.width.mas_equalTo(25);
        make.top.mas_equalTo(50);
    }];
    
    //登录显示的按钮
    [self.headView addSubview:self.logView];
    [self.logView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.setBtn.mas_bottom).offset(10);
        make.width.mas_equalTo(Frame_Width);
        make.height.mas_equalTo(100);
    }];

    if (info.login == YES) {
        self.unLoginBtn.hidden = YES;
        self.uidLb.hidden = YES;
        self.loginLb.hidden = NO;
        self.wellBtn.hidden = NO;

        [self.logView addSubview:self.loginLb];
        if (info.phone.length > 0) {
            self.loginLb.text = [NSString stringWithFormat:@"Hi %@",info.phone];
        } else {
            self.loginLb.text = [NSString stringWithFormat:@"Hi %@",info.email];
        }
        [self.loginLb mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(25);
            make.top.mas_equalTo(10);
            make.width.mas_lessThanOrEqualTo(250);
            make.height.mas_equalTo(25);
        }];

        [self.logView addSubview:self.wellBtn];
        [self.wellBtn setTitle:[NSString stringWithFormat:@"UID %@",info.uid] forState:UIControlStateNormal];
        self.wellBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        [self.wellBtn setImage:[UIImage imageNamed:@"fuzhi"] forState:UIControlStateNormal];
        [self.wellBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.loginLb.mas_bottom).offset(15);
            make.left.mas_equalTo(25);
            make.width.equalTo(@250);
            make.height.equalTo(@20);
        }];
    } else {
        //未登录
        self.loginLb.hidden = YES;
        self.wellBtn.hidden = YES;
        self.unLoginBtn.hidden = NO;
        self.uidLb.hidden = NO;

        [self.logView addSubview:self.unLoginBtn];
        [self.unLoginBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(25);
            make.top.mas_equalTo(0);
            make.width.mas_lessThanOrEqualTo(300);
            make.height.mas_equalTo(25);
        }];

        [self.logView addSubview:self.uidLb];
        [self.uidLb mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(25);
            make.top.mas_equalTo(self.unLoginBtn.mas_bottom).offset(10);
            make.width.mas_lessThanOrEqualTo(300);
            make.height.mas_equalTo(25);
        }];
    }
    
    [self.headView addSubview:self.accountBtn];
    [self.accountBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(Frame_Width/3);
        make.height.mas_equalTo(60);
        make.top.mas_equalTo(self.logView.mas_bottom);
    }];
    
    [self.headView addSubview:self.realBtn];
    [self.realBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountBtn.mas_right);
        make.top.mas_equalTo(self.accountBtn);
        make.width.mas_equalTo(Frame_Width/3);
        make.height.mas_equalTo(60);
    }];
    
//    [self.headView addSubview:self.invotedBtn];
//    [self.invotedBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.realBtn.mas_right);
//        make.width.mas_equalTo(Frame_Width/3);
//        make.top.mas_equalTo(self.realBtn);
//        make.height.mas_equalTo(60);
//    }];

    
    //tbview
    [self addSubview:self.contentTableView];
    [self.contentTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.headView.mas_bottom);
        make.width.mas_equalTo(Frame_Width);
        make.bottom.mas_equalTo(self);
    }];
}

- (UIView *)headView {
    if (!_headView) {
        _headView = [[UIView alloc] init];
        [_headView setBackgroundColor:[UIColor whiteColor]];
    }
    return _headView;
}

- (UIView *)logView {
    if (!_logView) {
        _logView = [[UIView alloc] init];
        _logView.backgroundColor = [UIColor whiteColor];
        _logView.userInteractionEnabled = YES;
    }
    return _logView;
}

- (BaseLabel *)loginLb {
    if (!_loginLb) {
        _loginLb = [[BaseLabel alloc] init];
        _loginLb.textColor = ThemeTextColor;
        _loginLb.font = [UIFont systemFontOfSize:25];
        _loginLb.adjustsFontSizeToFitWidth = YES;
        _loginLb.textAlignment = 0;
    }
    return _loginLb;
}

- (FSCustomButton *)wellBtn {
    if (!_wellBtn) {
        _wellBtn = [[FSCustomButton alloc] init];
        _wellBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        _wellBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _wellBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _wellBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [_wellBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _wellBtn.titleLabel.font = FONT_WITH_SIZE(14.5);
        _wellBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_wellBtn addTarget:self action:@selector(copyAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _wellBtn;
}

- (void)copyAction:(UIButton *)sender {
    [TipsView showTipOnKeyWindow:kLocalizedString(@"CopySuccessfully")];
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = sender.currentTitle;
}


- (BaseButton *)unLoginBtn {
    if (!_unLoginBtn) {
        _unLoginBtn = [[BaseButton alloc] init];
//        _unLoginBtn.colorName = @"login_loginBtnCor";
        [_unLoginBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _unLoginBtn.titleLabel.font = BOLDSYSTEMFONT(30);
        _unLoginBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _unLoginBtn.titleLabel.textAlignment = 1;
        [_unLoginBtn setTitle:kLocalizedString(@"left_login") forState:UIControlStateNormal];
        [_unLoginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _unLoginBtn;
}

- (BaseLabel *)uidLb {
    if (!_uidLb) {
        _uidLb = [[BaseLabel alloc] init];
        _uidLb.textColor = HEXCOLOR(0x999999);
        _uidLb.font = [UIFont systemFontOfSize:20];
        _uidLb.textAlignment = 0;
        _uidLb.text = kLocalizedString(@"left_welcome");
        _uidLb.adjustsFontSizeToFitWidth = YES;
    }
    return _uidLb;
}

- (void)loginAction {
    NSLog(@"登陆");
    [IWNotificationCenter postNotificationName:KLoginNoti object:nil];
}

- (void)setAction {
    NSLog(@"设置");
    if([self.customDelegate respondsToSelector:@selector(setPushVC)]){
        [self.customDelegate setPushVC];
    }    
}

- (UIButton *)createBtn:(NSString *)title image:(NSString *)img tag:(NSInteger)tag frame:(CGRect)frame {
    FSCustomButton *btn = [[FSCustomButton alloc] init];
    btn.buttonImagePosition = FSCustomButtonImagePositionLeft;
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    btn.frame = frame;
    btn.titleLabel.font = FourteenFontSize;
    btn.tag = tag;
    btn.cornerRadius = 3;
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [btn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    return btn;
}

#pragma mark - tableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.imgArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LeftMenuCell *cell = [LeftMenuCell cellWithTableView:tableView];
    cell.lbText.text = self.titleArray[indexPath.row];
    cell.leftImg.image = [UIImage imageNamed:self.imgArray[indexPath.row]];
    if (indexPath.row == 2) {
        cell.custSwitch1.hidden = NO;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([self.customDelegate respondsToSelector:@selector(LeftMenuViewClick:)]){
        if (indexPath.row == 2) {
            return;
        }
        [self.customDelegate LeftMenuViewClick:indexPath.row];
    }
}

- (UITableView *)contentTableView {
    if (!_contentTableView) {
        _contentTableView        = [[UITableView alloc]initWithFrame:CGRectZero
                                                                           style:UITableViewStylePlain];
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.dataSource          = self;
        _contentTableView.delegate            = self;
        _contentTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.separatorStyle      = UITableViewCellSeparatorStyleNone;
        _contentTableView.tableFooterView = [UIView new];
    }
    return _contentTableView;
}

- (FSCustomButton *)setBtn {
    if (!_setBtn) {
        _setBtn = [[FSCustomButton alloc] init];
        _setBtn.buttonImagePosition = FSCustomButtonImagePositionLeft;
        _setBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_setBtn setImage:[UIImage imageNamed:@"set_set"] forState:UIControlStateNormal];
        _setBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [_setBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _setBtn.titleLabel.font = FONT_WITH_SIZE(14.5);
        [_setBtn addTarget:self action:@selector(setAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _setBtn;
}


- (FSCustomButton *)accountBtn {
    if (!_accountBtn) {
        _accountBtn = [[FSCustomButton alloc] init];
        _accountBtn.buttonImagePosition = FSCustomButtonImagePositionTop;
        _accountBtn.userInteractionEnabled = YES;
        [_accountBtn setImage:[UIImage imageNamed:@"set_accounts"] forState:UIControlStateNormal];
        _accountBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _accountBtn.titleEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 0);
        [_accountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _accountBtn.titleLabel.font = FONT_WITH_SIZE(13);
        [_accountBtn setTitle:kLocalizedString(@"left_account") forState:UIControlStateNormal];
        _accountBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_accountBtn sizeToFit];
        _accountBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [_accountBtn addTarget:self action:@selector(accountAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _accountBtn;
}

- (void)accountAction {
    NSLog(@"账户中心");
    if([self.customDelegate respondsToSelector:@selector(accountPushVC)]){
        [self.customDelegate accountPushVC];
    }
}

- (FSCustomButton *)realBtn {
    if (!_realBtn) {
        _realBtn = [[FSCustomButton alloc] init];
        _realBtn.buttonImagePosition = FSCustomButtonImagePositionTop;
        [_realBtn setImage:[UIImage imageNamed:@"set_real"] forState:UIControlStateNormal];
        _realBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _realBtn.titleEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 0);
        [_realBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _realBtn.titleLabel.font = FONT_WITH_SIZE(13);
        _realBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_realBtn sizeToFit];
        [_realBtn setTitle:kLocalizedString(@"left_real") forState:UIControlStateNormal];
        _realBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [_realBtn addTarget:self action:@selector(realAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _realBtn;
}

- (void)realAction {
    NSLog(@"实名认证");
    if([self.customDelegate respondsToSelector:@selector(realPushVC)]){
        [self.customDelegate realPushVC];
    }
}

- (FSCustomButton *)invotedBtn {
    if (!_invotedBtn) {
        _invotedBtn = [[FSCustomButton alloc] init];
        _invotedBtn.buttonImagePosition = FSCustomButtonImagePositionTop;
        [_invotedBtn setImage:[UIImage imageNamed:@"set_invote"] forState:UIControlStateNormal];
        _invotedBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _invotedBtn.titleEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 0);
        [_invotedBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _invotedBtn.titleLabel.font = FONT_WITH_SIZE(13);
        [_invotedBtn sizeToFit];
        [_invotedBtn addTarget:self action:@selector(invotedAction) forControlEvents:UIControlEventTouchUpInside];
        _invotedBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_invotedBtn setTitle:kLocalizedString(@"left_invoted") forState:UIControlStateNormal];
        _invotedBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    return _invotedBtn;
}

- (void)invotedAction {
    NSLog(@"邀请好友");
    if([self.customDelegate respondsToSelector:@selector(invotedPushVC)]){
        [self.customDelegate invotedPushVC];
    }
}

- (NSMutableArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"left_order"),kLocalizedString(@"left_abount"),kLocalizedString(@"left_sgt"),nil];
    }
    return _titleArray;
}

-  (NSMutableArray *)imgArray {
    if (!_imgArray) {
        _imgArray = [[NSMutableArray alloc] initWithObjects:@"set_order",@"set_about",@"set_sgt", nil];

    }
    return _imgArray;
}

@end
