//
//  HisDepositCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/7.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "HisDepositCell.h"

@implementation HisDepositCell

+ (instancetype)cellWithTbaleView:(UITableView *)tableView {
    static NSString *ID = @"HisDepositCell";
    HisDepositCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[HisDepositCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.cornerRadius = 5;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        self.backgroundColor = WhiteColor;
        [self setUpView];
    }
    return self;
}

- (void)setDepositModel:(AssertListModel *)depositModel {
    if (depositModel) {
        _depositModel = depositModel;
        self.timeText.text = depositModel.createdAt;
        self.entrustText.text = depositModel.amount;
        if ([self.type isEqualToString:@"0"]) {
            self.typeLb.text = kLocalizedString(@"deposit_nomarl");
            _detailBtn.hidden = YES;
            self.stateText.text = depositModel.status;
        } else {
            self.typeLb.text = kLocalizedString(@"deposit_withdrawNo");
            self.stateText.text = depositModel.withdrawStatus;
            if ([depositModel.withdrawCode isEqualToString:@"0"]) {
                _detailBtn.hidden = NO;
            } else {
                _detailBtn.hidden = YES;
            }
        }
        self.entrustNum.text = [NSString stringWithFormat:kLocalizedString(@"deposit_entrustNum%@"),depositModel.coinType];
    }
}

- (void)setUpView {
    [self.contentView addSubview:self.typeLb];
    [self.typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(25);
    }];
    
    [self.contentView addSubview:self.detailBtn];
    [self.detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(24);
        make.centerY.mas_equalTo(self.typeLb);
    }];
    
    [self.contentView addSubview:self.timeLb];
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.typeLb.mas_bottom).offset(15);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(17);
    }];
    
    [self.contentView addSubview:self.stateLb];
    [self.stateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.timeLb);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(self.timeLb);
    }];
    
    [self.contentView addSubview:self.entrustNum];
    [self.entrustNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.stateLb);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(self.stateLb);
    }];
    
    [self.contentView addSubview:self.timeText];
    [self.timeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeLb.mas_bottom).offset(15);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(17);
        make.left.mas_equalTo(self.timeLb);
    }];
    
    [self.contentView addSubview:self.stateText];
    [self.stateText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(self.timeText);
        make.top.mas_equalTo(self.timeText);
    }];
    
    [self.contentView addSubview:self.entrustText];
    [self.entrustText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.entrustNum);
        make.top.height.mas_equalTo(self.stateText);
        make.width.mas_equalTo(100);
    }];
}

- (UILabel *)typeLb {
    if (!_typeLb) {
        _typeLb = [[UILabel alloc] init];
        _typeLb.textAlignment = 0;
        _typeLb.font = kFont15;
        _typeLb.textColor = HEXCOLOR(0x333333);
    }
    return _typeLb;
}

- (FSCustomButton *)detailBtn {
    if (!_detailBtn) {
        _detailBtn = [[FSCustomButton alloc] init];
        _detailBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        _detailBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _detailBtn.titleLabel.font = kFont15;
        _detailBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_detailBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _detailBtn.backgroundColor = ThemeRedColor;
        _detailBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _detailBtn.hidden = YES;
//        [_detailBtn setImage:[[UIImage imageNamed:@"fanhui"] imageWithColor:[UIColor blackColor]]  forState:UIControlStateNormal];
        [_detailBtn setTitle:kLocalizedString(@"deposit_revocatory") forState:UIControlStateNormal];
        [_detailBtn addTarget:self action:@selector(detailBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _detailBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _detailBtn.cornerRadius = 3;
    }
    return _detailBtn;
}

- (void)detailBtnClick:(UIButton *)sender {
    if (self.seeBtnBlock) {
        self.seeBtnBlock(self.depositModel.ID);
    }
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.textAlignment = 0;
        _timeLb.font = kFont14;
        _timeLb.text = kLocalizedString(@"deposit_time");
        _timeLb.textColor = HEXCOLOR(0xA0A0A0);
    }
    return _timeLb;
}

- (UILabel *)stateLb {
    if (!_stateLb) {
        _stateLb = [[UILabel alloc] init];
        _stateLb.textAlignment = 1;
        _stateLb.font = kFont14;
        _stateLb.text = kLocalizedString(@"deposit_state");
        _stateLb.textColor = HEXCOLOR(0xA0A0A0);
    }
    return _stateLb;
}

- (UILabel *)entrustNum {
    if (!_entrustNum) {
        _entrustNum = [[UILabel alloc] init];
        _entrustNum.textAlignment = 2;
        _entrustNum.font = kFont14;
        _entrustNum.textColor = HEXCOLOR(0xA0A0A0);
        _entrustNum.adjustsFontSizeToFitWidth = YES;
    }
    return _entrustNum;
}

- (UILabel *)timeText {
    if (!_timeText) {
        _timeText = [[UILabel alloc] init];
        _timeText.font = kFont15;
        _timeText.textColor = [UIColor blackColor];
        _timeText.text = @"13:15 04/02";
        _timeText.textAlignment = 0;
    }
    return _timeText;
}

- (UILabel *)stateText {
    if (!_stateText) {
        _stateText = [[UILabel alloc] init];
        _stateText.font = kFont15;
        _stateText.textColor = [UIColor blackColor];
        _stateText.text = kLocalizedString(@"deposit_done");
        _stateText.textAlignment = 1;
    }
    return _stateText;
}

- (UILabel *)entrustText {
    if (!_entrustText) {
        _entrustText = [[UILabel alloc] init];
        _entrustText.font = kFont15;
        _entrustText.textColor = [UIColor blackColor];
        _entrustText.textAlignment = 2;
        _entrustText.text = @"0.052";
    }
    return _entrustText;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
