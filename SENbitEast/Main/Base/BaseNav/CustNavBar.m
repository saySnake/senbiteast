//
//  CustNavBar.m
//  Drlink-IOS
//
//  Created by 张玮 on 15/3/17.
//  Copyright (c) 2015年 DrLink. All rights reserved.
//

#import "CustNavBar.h"
#import "ThemeManager.h"
@interface CustNavBar()
//标题
@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UILabel *shakeLabel;
@property (nonatomic ,strong) UILabel *flashLabel;
@property (nonatomic ,strong) UIView *views;

@end

@implementation CustNavBar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        self.colorName = @"kThemeCellLabel"; //多皮肤
//        [self setColor];  //多皮肤
        self.backgroundColor = WhiteColor;
        [self addSubview:self.leftBtn];
//        self.rightBtn.frame = CGRectMake(frame.size.width - 50, KNavBarButtonHeight + 24, 50, 44);
        [self addSubview:self.rightBtn];
        [self addSubview:self.titleBtn];
    }
    return self;
}
- (BaseButton *)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [[BaseButton alloc] init]; //WithImageFrame:CGRectMake(12, 12, 12, 20)];
        _leftBtn.frame = CGRectMake(15, KNavBarButtonHeight, 32, 22);
        _leftBtn.tag = 0;
        _leftBtn.hidden = YES;
        _leftBtn.titleLabel.adjustsFontSizeToFitWidth = NO;
        _leftBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _leftBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_leftBtn addTarget:self action:@selector(actionButton:) forControlEvents:UIControlEventTouchUpInside];
        [_leftBtn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    }
    return _leftBtn;
}

- (UIView *)views {
    if (!_views) {
        _views = [[UIView alloc] init];
        _views.frame = CGRectMake(0, KNavBarButtonHeight, 250, 30);
        _views.centerX = self.centerX;
    }
    return _views;
}
- (BaseButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [[BaseButton alloc] init]; //WithImageFrame:CGRectMake(10, 12 + 24, 22, 22)];
        _rightBtn.frame = CGRectMake(DScreenW - 37,  KNavBarButtonHeight, 32, 22);
        _rightBtn.tag = 1;
//        _rightBtn.hidden = YES;
        [_rightBtn addTarget:self action:@selector(actionButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}

- (BaseButton *)titleBtn {
    if (!_titleBtn) {
        _titleBtn = [[BaseButton alloc]init];
        _titleBtn.frame = CGRectMake(0, KNavBarButtonHeight, 150, 30);
        _titleBtn.userInteractionEnabled = YES;
        _titleBtn.titleLabel.adjustsFontSizeToFitWidth = NO;
        _titleBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _titleBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_titleBtn addTarget:self action:@selector(navTitleClick:) forControlEvents:UIControlEventTouchUpInside];
        _titleBtn.titleLabel.font = kFont16;
        _titleBtn.titleLabel.font = BOLDSYSTEMFONT(16);
        [_titleBtn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        _titleBtn.centerX = self.centerX;
    }
    return _titleBtn;
}

- (instancetype)init {
    NSAssert(0, @"请使用initWithFrame:方法初始化");
    return nil;
}

- (void)setLeftItemImage:(NSString *)image {
    if (image) {
        self.leftBtn.imageName = image;
        self.leftBtn.hidden = NO;
        [self.leftBtn setImage:[UIImage imageNamed:self.leftBtn.imageName] forState:UIControlStateNormal];
        [self.leftBtn loadThemeImage];
    } else {
        self.leftBtn.hidden = YES;
    }
}

- (void)setRightItemImage:(NSString *)image {
    if (image) {
        self.rightBtn.imageName = image;
        self.rightBtn.hidden = NO;
        [self.rightBtn setImage:[UIImage imageNamed:self.rightBtn.imageName] forState:UIControlStateNormal];
        [self.leftBtn loadThemeImage];
    } else {
        [self.rightBtn setHidden:YES];
    }
}

- (void)setBarTitle:(NSString *)title {
    [self.titleBtn setTitle:title forState:UIControlStateNormal];
}

- (void)setLeftTitle:(NSString *)title {
    self.leftBtn.hidden = NO;
    [self.leftBtn setTitle:title forState:UIControlStateNormal];
}

- (void)setRightTitle:(NSString *)title {
    [self.rightBtn setTitle:title forState:UIControlStateNormal];
}
/**
 *  设置中心view
 *
 *  @param view view
 */
- (void)setCenterView:(UIView *)view {
    _views = view;
    [self addSubview:_views];
}

//action
- (void)actionButton:(id)sender {
    if (_itemClick) {
        self.itemClick([sender tag]);
    }
}
- (void)navTitleClick:(id)sender {
    if (_btnClick) {
        _btnClick(sender);
    }
}
/**
 *  晃动
 *
 *  @param str   str
 *  @param shake shake
 */
- (void)showAlertWithString:(NSString *)str isShaeke:(BOOL)shake {
    CGFloat shakeX = 100 * autoSizeScaleX;
    CGFloat shakeY = 20* autoSizeScaleY;
    CGFloat shakeW = DScreenW - shakeX;
    CGFloat shakeH = 40 * autoSizeScaleY;
    UILabel *alertLabel = [[UILabel alloc]initWithFrame:CGRectMake(shakeX,
                                                                  shakeY,
                                                                  shakeW,
                                                                  shakeH)];
    [self addSubview:alertLabel];
    alertLabel.text = str;
    alertLabel.textColor =MainWhiteColor;
    
    if (shake) {
        self.titleLabel.alpha =0;
        CAKeyframeAnimation *animation =[CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
        CGFloat currentTx=alertLabel.transform.tx;
        animation.delegate = self;
        animation.duration =0.5;
        animation.values = @[ @(currentTx),
                              @(currentTx + 10),
                              @(currentTx - 8),
                              @(currentTx + 8),
                              @(currentTx - 5),
                              @(currentTx + 5),
                              @(currentTx) ];
        animation.keyTimes = @[ @(0),
                                @(0.225),
                                @(0.425),
                                @(0.6),
                                @(0.75),
                                @(0.875),
                                @(1) ];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [alertLabel.layer addAnimation:animation
                                forKey:@"shake"];
        
        [UIView animateKeyframesWithDuration:0.55
                                       delay:1.5
                                     options:0
                                  animations:^{
            alertLabel.alpha = 0;
            self.titleLabel.alpha = 1;
        } completion:^(BOOL finished) {
            [alertLabel removeFromSuperview];
        }];
        
    } else {
        alertLabel.alpha = 0;
        [UIView animateKeyframesWithDuration:0.15
                                       delay:0
                                     options:0
                                  animations:^{
            alertLabel.alpha = 1;
            self.titleLabel.alpha = 0;
        } completion:^(BOOL finished) {
            [UIView animateKeyframesWithDuration:0.65
                                           delay:1.5
                                         options:0
                                      animations:^{
               
                alertLabel.alpha = 0;
                self.titleLabel.alpha = 1;
            } completion:^(BOOL finished) {
                [alertLabel removeFromSuperview];
            }];
        }];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end


