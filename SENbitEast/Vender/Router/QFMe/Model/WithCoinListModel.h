//
//  WithCoinListModel.h
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithCoinListModel : NSObject
/** id **/
@property (nonatomic ,copy) NSString *ID;
/** CoinName **/
@property (nonatomic ,copy) NSString *coinName;
/** addressURL **/
@property (nonatomic ,copy) NSString *addressURL;
/** able2charge **/
@property (nonatomic ,assign) BOOL able2charge;
/** able2withdraw **/
@property (nonatomic ,assign) BOOL able2withdraw;
/** chargeConfirmLimit **/
@property (nonatomic ,copy) NSString *chargeConfirmLimit;
/** coinIcon **/
@property (nonatomic ,copy) NSString *coinIcon;
/** collectCoin **/
@property (nonatomic ,copy) NSString *collectCoin;
/** enFullName **/
@property (nonatomic ,copy) NSString *enFullName;
/** enName **/
@property (nonatomic ,copy) NSString *enName;
/** 费率 **/
@property (nonatomic ,copy) NSString *nwFeeRatio;
/** nwMaxAmount **/
@property (nonatomic ,copy) NSString *nwMaxAmount;
/** nwMinAmount **/
@property (nonatomic ,copy) NSString *nwMinAmount;
/** 最小手续费 **/
@property (nonatomic ,copy) NSString *nwMinFee;
/** txURL **/
@property (nonatomic ,copy) NSString *txURL;
/** isDestinationTagRequired **/
@property (nonatomic ,assign) BOOL isDestinationTagRequired;
/** 每日提币上线 **/
@property (nonatomic ,copy) NSString *nwDailyMaxAmount;
/** memo**/
@property (nonatomic ,copy) NSString *memo;
/** balance**/
@property (nonatomic ,copy) NSString *balance;

/** 手动判断是不是一币多链 **/
@property (nonatomic ,assign) BOOL moreChain;

- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
