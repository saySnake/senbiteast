//
//  TradeViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/27.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeViewController.h"
#import "TradeSectionHeaderView.h"
#import "TradeHeadView.h"
#import "ComisCurrentCell.h"
#import "QFRouter.h"
#import "QFMoudleProtocol.h"
#import "TradeListModel.h"
#import "SelectedViewController.h"
#import "TradeDetailVC.h"
#import "SelectedMenView.h"
#import "SelectedCoinView.h"

@interface TradeViewController ()<UITableViewDelegate,UITableViewDataSource,SelectedCoinViewDelegate,TradeHeadViewDelegate> {
}
@property (nonatomic ,strong) TradeSectionHeaderView *sectionHeaderView;
@property (nonatomic, weak) UIScrollView *titleScroll;
@property (nonatomic, strong) NSMutableArray *titleButton;
@property (nonatomic, weak) UIButton *selectBtn;
@property (strong, nonatomic) UITableView *tbleView;
@property (nonatomic ,strong) TradeHeadView *headView;
@property (nonatomic ,assign) BOOL loginSuc;
@property (nonatomic ,strong) XXWebQuoteModel *webModel;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger currentIndex;
@property (nonatomic ,strong) NSMutableArray *orginDataArr;
@property (nonatomic ,strong) SelectedMenView *menu;
@property (nonatomic ,strong) SelectedCoinView *demo;
@property (nonatomic ,strong) UIView *vi;

@end

@implementation TradeViewController
- (void)dealloc {
    [self dismiss];
    [IWNotificationCenter removeObserver:self name:KnotificationSelected object:nil];
    [IWNotificationCenter removeObserver:self];
}

- (void)dismiss {
    [self.headView dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTitleScroll];
    [self setupUI];
    [IWNotificationCenter addObserver:self selector:@selector(reloadSymbol) name:KnotificationSelected object:nil];
    [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    [IWNotificationCenter addObserver:self selector:@selector(loginAction) name:Login_In_NotificationName object:nil];
    [IWNotificationCenter addObserver:self selector:@selector(loginAction) name:Login_Out_NotificationName object:nil];
}

- (void)loginAction {
    [self.orginDataArr removeAllObjects];
    [self.dataArr removeAllObjects];
    [self.tbleView reloadData];
}

- (void)changeLanguage {
    self.titleButton = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"trade_coin"), nil];
    [self setUpTitleScroll];
}

- (void)reloadSymbol {
    [self hideMenuView];
    [self.headView calData];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.headView calData];
    [self requestPing];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    CGFloat a = self.titleScroll.height;
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, a - kLinePixel, DScreenW, kLinePixel)];
    line.backgroundColor =  HEXCOLOR(0xDDDDDD);
    [self.titleScroll addSubview:line];

    [self.view addSubview:self.tbleView];
    self.tbleView.tableHeaderView  = self.headView;
    
    SelectedCoinView *demo = [[SelectedCoinView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width * 0.8, [[UIScreen mainScreen] bounds].size.height)];
    demo.customDelegate = self;
    self.demo = demo;
    SelectedMenView *menu = [SelectedMenView MenuViewWithDependencyView:self.view MenuView:demo isShowCoverView:YES];
    self.menu = menu;
}


- (void)setUpTitleScroll {
    UIScrollView *view = [[UIScrollView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    view.frame = CGRectMake(0, 0, DScreenW, kNavBarAndStatusBarHeight);
    view.showsHorizontalScrollIndicator = NO;
    view.showsVerticalScrollIndicator = NO;
    self.titleScroll = view;
    CGFloat btnW = 100;
    CGFloat btnH = 35;
    for (int i = 0; i < 3; ++i) {
        UIButton *btn = [[UIButton alloc] init];;
        btn.frame = CGRectMake(btnW * i , kNavBarAndStatusBarHeight - 40, btnW, btnH);
        [btn setTitle:self.titleButton[i] forState:UIControlStateNormal];
        [btn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(topTitleBtnClick:) forControlEvents:UIControlEventTouchDown];
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        btn.tag = i;
        btn.backgroundColor = [UIColor whiteColor];
        [view addSubview:btn];
        if (btn.tag == 0) {
            [self topTitleBtnClick:btn];
        }
    }
    [self.view addSubview:view];
}

- (void)topTitleBtnClick:(UIButton *)btn {
    [self selctedBtn:btn];
}


- (void)selctedBtn:(UIButton *)btn {
    [_selectBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
    [btn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
    _selectBtn.transform = CGAffineTransformIdentity;
    btn.transform = CGAffineTransformMakeScale(1.3, 1.3);
    _selectBtn = btn;
}


#pragma mark - 7. 表示图代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentIndex == 0) {
   
        return self.orginDataArr.count;
    } else {
      
        return self.dataArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.sectionHeaderView.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.sectionHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ComisCurrentCell *cell = [ComisCurrentCell cellWithTbaleView:tableView];
    KWeakSelf
    cell.ComisCellBlock = ^(NSString * _Nonnull Id,NSString * _Nonnull market) {
        [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
        [weakSelf traderCancel:Id market:market];
    };
    cell.finishBlock = ^(TradeListModel * _Nonnull model) {
        [weakSelf detailHisVC:model];
    };
    cell.currentIndex = self.currentIndex;
    if (self.currentIndex == 0) {
        cell.model = self.orginDataArr[indexPath.row];
    } else {
        cell.model = self.dataArr[indexPath.row];
    }
    return cell;
}

- (void)detailHisVC:(TradeListModel *)model {
    NSLog(@"订单明细");
    TradeDetailVC *detail = [[TradeDetailVC alloc] init];
    detail.model = model;
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)traderCancel:(NSString *)Id market:(NSString *)market {

    NSString *url = [NSString stringWithFormat:@"%@%@/cancel",EastTradeOrder,Id];
    NSDictionary *dic = @{@"market":market};
    KWeakSelf
    [SBNetworTool patchWithUrl:url params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        if (SuccessCode == 200) {
        
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [self showToastView:FailurMessage];
    }];
}

- (TradeSectionHeaderView *)sectionHeaderView {
    if (!_sectionHeaderView) {
        _sectionHeaderView = [[TradeSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, 42)];
        [_sectionHeaderView setupUI];
        _sectionHeaderView.backgroundColor = [UIColor whiteColor];
        KWeakSelf
        _sectionHeaderView.headActionBlock = ^(NSInteger index) {
            if (index == 0) {
                if (weakSelf.loginSuc == YES) {
                    [weakSelf orginEntrustOrders:@"current"];
                    weakSelf.currentIndex = 0;
                    [weakSelf.tbleView reloadData];
                }
            } else if (index == 1) {
                NSLog(@"历史委托");
                if (weakSelf.loginSuc == YES) {
                    weakSelf.currentIndex = 1;
                    [weakSelf entrustOrders:@"history"];
                }
            } else if (index == 2) {
                NSLog(@"历史成交");
                if (weakSelf.loginSuc == YES) {
                    weakSelf.currentIndex = 2;
                    [weakSelf requestEastTradeHisOrders];
                }
            }
        };
    }
    return _sectionHeaderView;
}

- (void)requestEastTradeHisOrders {
    NSDictionary *dic = @{@"market":KTrade.coinTradeModel.symbolId};
    [SBNetworTool getWithUrl:EastTradeHisOrders params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.dataArr = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
            if (self.dataArr.count == 0) {
                self.tbleView.tableFooterView = self.vi;
            } else {
                self.tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 100 : 100)];
            }
            [self.tbleView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)requestPing {
    KWeakSelf
    [SBNetworTool getWithUrl:EastPing params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            weakSelf.loginSuc = YES;
            [weakSelf orginEntrustOrders:@"current"];
            [weakSelf sendEntrustSocket];
        } else {
            weakSelf.loginSuc = NO;
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {

    }];
}

- (void)orginEntrustOrders:(NSString *)str {
    NSDictionary *dic = @{@"market":KTrade.coinTradeModel.symbolId,
                          @"type":str//@"current"
                        };
    [SBNetworTool getWithUrl:EastEntrustOrders params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.orginDataArr = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
            if (self.orginDataArr.count == 0) {
                self.tbleView.tableFooterView = self.vi;
            } else {
                self.tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 100 : 100)];
            }
            [self.tbleView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)entrustOrders:(NSString *)str {
    NSDictionary *dic = @{@"market":KTrade.coinTradeModel.symbolId,
                          @"type":str//@"current"
                        };
    [SBNetworTool getWithUrl:EastEntrustOrders params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.dataArr = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
            if (self.dataArr.count == 0 ) {
                self.tbleView.tableFooterView = self.vi;
            } else {
                self.tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 100:100)];
            }
            [self.tbleView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (void)sendEntrustSocket {
    [KQuoteSocket cancelWebSocketSubscribeWithWebSocketModel:self.webModel lineType:@"currency-entrust"];
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        NSLog(@"%@",data);
        if (weakSelf.currentIndex == 0) {
            TradeListModel *model = [TradeListModel mj_objectWithKeyValues:data];
            if ([model.state isEqualToString:kLocalizedString(@"trade_wating")]) {
                [weakSelf.orginDataArr insertObject:model atIndex:0];
            } else {
                [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];

                for (TradeListModel *m in weakSelf.orginDataArr) {
                    if ([m.orderId isEqualToString:model.orderId]) {
                        [weakSelf.orginDataArr removeObject:m];
                        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
                        break;
                    }
                }
            }
        }
        else if (weakSelf.currentIndex == 1) {
            [weakSelf entrustOrders:@"history"];
        }
        else if (weakSelf.currentIndex == 2) {
            [weakSelf requestEastTradeHisOrders];
        }
        
        if (self.orginDataArr.count == 0 ) {
            weakSelf.tbleView.tableFooterView = weakSelf.vi;
        } else {
            weakSelf.tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 100:100)];
        }
        
        [weakSelf.tbleView reloadData];
    };
    
    if (KTrade.coinTradeModel.symbolName) {
        NSString *cok = [IWDefault objectForKey:Cookie];
        if (cok) {
            NSDictionary *dic = @{@"pair":KTrade.coinTradeModel.symbolName,@"cookie":cok};
            self.webModel.params = [NSMutableDictionary dictionary];
            self.webModel.params[@"topic"] = @"currency-entrust";
            self.webModel.params[@"data"] = dic;
            [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"currency-entrust"];
        }
    }
}


- (UITableView *)tbleView {
    if (_tbleView == nil) {
        _tbleView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNavBarAndStatusBarHeight, kScreen_Width, kScreen_Height - (kNavBarAndStatusBarHeight)) style:UITableViewStylePlain];
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.dataSource = self;
        _tbleView.delegate = self;
        _tbleView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//        _tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 100 : 100)];
        _tbleView.tableFooterView = self.vi;
        _tbleView.estimatedRowHeight = 0;
        _tbleView.separatorColor = HEXCOLOR(0xF4F2F2);
        _tbleView.estimatedSectionHeaderHeight = 0;
        _tbleView.estimatedSectionFooterHeight = 0;
        if (@available(iOS 11.0, *)) {
            _tbleView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tbleView;
}

- (UIView *)vi {
    if (!_vi) {
        _vi = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 140 : 140)];
        _vi.backgroundColor = [UIColor whiteColor];
        UIImageView *img = [[UIImageView alloc] init];
        img.image = [UIImage imageNamed:@"Assert_noData"];
        img.contentMode = 1;
        img.bounds = CGRectMake(0, 10, 140, 140);
        img.center = _vi.center;
        [_vi addSubview:img];
    }
    return _vi;
}
    
- (NSMutableArray *)titleButton {
    if (_titleButton == nil) {
        _titleButton = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"trade_coin"), nil];
    }
    return _titleButton;
}

- (TradeHeadView *)headView {
    if (!_headView) {
        KWeakSelf
        _headView = [[TradeHeadView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, 470)];
        _headView.delegate = self;
        _headView.TradeHeadViewBlock = ^{
            [weakSelf puKline];
        };
        _headView.TradeBuyBlock = ^(NSDictionary * dic) {
            [weakSelf tradeCoin:dic];
        };
        
        _headView.switchBtnBlock = ^{
            [weakSelf presentVc];
        };
        _headView.backgroundColor = [UIColor whiteColor];
    }
    return _headView;
}

- (void)presentVc {
//    SelectedViewController *se = [[SelectedViewController alloc] init];
//    se.menuViewStyle = 1;
//    se.selectIndex = 0;
//    se.automaticallyCalculatesItemWidths = YES;
//    se.modalPresentationStyle = UIModalPresentationFullScreen;
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:se];
//    nav.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:nav animated:YES completion:nil];
    [self.demo firstAppear];
    [self.menu show];
}

- (void)hideMenuView {
    [self.menu hidenWithAnimation];
}

- (void)tradeCoin:(NSDictionary *)dic {
    [SBNetworTool postWithUrl:EastOrders params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            NSLog(@"%@",responseObject);
            [self.headView calData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)puKline {
    id <MoudleLine>lineMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleLine)];
    UIViewController *viewController = lineMoudle.interfaceViewController;
    lineMoudle.callbackBlock = ^(id parameter) {
        NSLog(@"return paramter = %@",parameter);
    };
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (NSMutableArray *)orginDataArr {
    if (!_orginDataArr) {
        _orginDataArr = [[NSMutableArray alloc] init];
    }
    return _orginDataArr;
}
    

- (void)traderBuyAction {
    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
    if (user.login == NO) {
        LoginViewController *login = [[LoginViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        nav.navigationBarHidden = YES;
        login.finishBlock = ^{
            [self dismissViewControllerAnimated:NO completion:nil];
        };
        login.cancleBlock = ^{

        };
        [self presentViewController:nav animated:NO completion:nil];
    }
}

@end
