//
//  LoginViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/12.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "QFLoginVC.h"
#import "MoudleLogin.h"
#import "AsyncTaskButton.h"
#import <WebKit/WebKit.h>
#import <GT3Captcha/GT3Captcha.h>
#import "TipsView.h"
#import "FindPasswordVC.h"
#import "PhoneViewController.h"
#import "GestureScreen.h"
#import "GestureViewController.h"
#import "GestureIntrdouceViewController.h"

@interface QFLoginVC ()<AsyncTaskCaptchaButtonDelegate,GestureViewDelegate,GestureScreenDelegate>

@property (nonatomic ,strong) BaseButton *cancelBtn;
@property (nonatomic ,strong) BaseButton *registBtn;
@property (nonatomic ,strong) BaseLabel *logoTitle;
@property (nonatomic ,strong) BaseLabel *contentLb;
@property (nonatomic ,strong) UITextField *accountTF;
@property (nonatomic ,strong) BaseLabel *line1;
@property (nonatomic ,strong) UITextField *contenTF;
@property (nonatomic ,strong) BaseLabel *line2;
/** 登录按钮 */
@property (nonatomic ,strong) AsyncTaskButton *loginBtn;
@property (nonatomic ,strong) BaseButton *forgetBtn;


@end

@implementation QFLoginVC
- (void)dealloc {
    NSLog(@"%@ dealloc resumed",NSStringFromClass([self class]));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setUpView];
    [self make_Layout];
    [self configKeyBoardRespond];
}

#pragma mark - InitAppreaence
- (void)setUpView {
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.registBtn];
    [self.view addSubview:self.logoTitle];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.line1];
    [self.view addSubview:self.contenTF];
    [self.view addSubview:self.line2];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.forgetBtn];
}

- (void)make_Layout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.registBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(27);
    }];
    
    [self.logoTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(163);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(140);
    }];
    
    [self fuwenbenLabel:self.logoTitle FontNumber:[UIFont systemFontOfSize:33] AndRange:NSMakeRange(3, 3) AndColor:ThemeGreenColor];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.logoTitle.mas_bottom).offset(27);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(39);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(95);
    }];
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(5);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.contenTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.line1.mas_bottom).offset(50);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(20);
    }];
    
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(self.contenTF.mas_bottom).offset(5);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.line2.mas_bottom).offset(70);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(53);
    }];
    
    [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.loginBtn.mas_bottom).offset(25);
        make.height.mas_equalTo(19);
    }];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak QFLoginVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.contenTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}


- (void)fuwenbenLabel:(UILabel *)labell FontNumber:(id)font AndRange:(NSRange)range AndColor:(UIColor *)vaColor {
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:labell.text];
    //设置字号
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:33] range:range];
    //设置文字颜色
    [str addAttribute:NSForegroundColorAttributeName value:vaColor range:range];
    
    labell.attributedText = str;
}

//demo
- (void)captchaWillDisplay {
    [TipsView removeAllTipsView];
}

- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
    [TipsView showTipOnKeyWindow:@"正在检测\n智能账号安全..."];
    return YES;
}

#pragma mark - GestureScreenDelegate
- (void)dismMisVC {
//    [self dismissViewControllerAnimated:NO completion:^{
//        NSLog(@"2");
//        if (self.finishBlock) {
//            self.finishBlock();
//        }
//    }];
    if (self.finishBlock) {
        self.finishBlock();
    }
}

- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    if (!error) {
        // 演示中全部默认为成功, 不对返回做判断
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([GestureTool isGestureEnable]) {
                [[GestureScreen shared] show];
                [GestureScreen shared].delegate = self;
                
            } else {
                __weak typeof(self) weakSelf = self;
                GestureIntrdouceViewController * gesture = [[GestureIntrdouceViewController alloc] init];
                gesture.cancleBlock = ^{
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    if (strongSelf.finishBlock) {
                        strongSelf.finishBlock();
                    }
                };
                
                gesture.finishBlock = ^{
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    if (strongSelf.finishBlock) {
                        strongSelf.finishBlock();
                    }
                };
                [self.navigationController pushViewController:gesture animated:NO];
            }
        });
    }
    else {
        //处理验证中返回的错误
        if (error.code == -999) {
            // 请求被意外中断,一般由用户进行取消操作导致
        }
        else if (error.code == -10) {
            // 预判断时被封禁, 不会再进行图形验证
        }
        else if (error.code == -20) {
            // 尝试过多
        }
        else {
            // 网络问题或解析失败, 更多错误码参考开发文档
        }
        [TipsView showTipOnKeyWindow:error.error_code fontSize:12.0];
    }
}




//- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
//    if (_currentIndex == 0) {
//        if (self.phoneField.text.length == 0) {
//            [self showToastView:kLocalizedString(@"blindPhone_phonePlace")];
//            return NO;
//        }
//        if (self.passwordTF.text.length == 0) {
//            [self showToastView:kLocalizedString(@"toastNotPwd")];
//            return NO;
//        }
//    } else {
//        if (self.accountTF.text.length == 0) {
//            [self showToastView:kLocalizedString(@"toastNotEmail")];
//            return NO;
//        }
//        if (self.passwordTF.text.length == 0) {
//            [self showToastView:kLocalizedString(@"toastNotPwd")];
//            return NO;
//        }
//        [TipsView showTipOnKeyWindow:@"正在检测\n智能账号安全..."];
//    }
//    return YES;
//}

//- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
//    [self.view endEditing:YES];
//    if (status == NO) {
//
//    }
//
//    if (error) {
//        if (error.code == -999) {
//            // 请求被意外中断,一般由用户进行取消操作导致
//        }
//        else if (error.code == -10) {
//            // 预判断时被封禁, 不会再进行图形验证
//        }
//        else if (error.code == -20) {
//            // 尝试过多
//        }
//        else {
//            // 网络问题或解析失败, 更多错误码参考开发文档
//        }
//        [TipsView showTipOnKeyWindow:error.error_code fontSize:12.0];
//    }
//
//    if (status == YES) {
//        if (_currentIndex == 1) {
//            if (self.accountTF.text.length == 0) {
//                   [self showToastView:kLocalizedString(@"toastNotPhoneOrEmail")];
//                   return;
//               }
//               if (self.passwordTF.text.length == 0) {
//                   [self showToastView:kLocalizedString(@"toastNotLoginPwd")];
//                   return;
//               }
//        }
//    }
//
//    if (status == YES) {
//        NSMutableArray *arr = [[NSMutableArray alloc] init];
//        UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
//        if (user.geetest_challenge && user.geetest_seccode && user.geetest_validate) {
//
//            NSDictionary *dic;
//            if (_currentIndex == 1) {
//                dic = @{@"challenge":user.geetest_challenge,
//                        @"seccode":user.geetest_seccode,
//                        @"validate":user.geetest_validate,
//                        @"account":self.accountTF.text,
//                        @"password":self.passwordTF.text};
//            } else {
//                dic = @{@"challenge":user.geetest_challenge,
//                        @"seccode":user.geetest_seccode,
//                        @"validate":user.geetest_validate,
//                        @"account":[NSString stringWithFormat:@"%@%@",self.phoneBtn.currentTitle,self.phoneField.text],
//                        @"password":self.passwordTF.text};
//            }
//            kWeakSelf(self);
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                            [SVProgressHUD show];
//            });
//
//            [SBNetworTool postWithUrl:SenLoginTwouser params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//
//                });
//
//                if ([responseObject[@"login"] boolValue] == YES) {
//                    [self dismissViewControllerAnimated:NO completion:^{
//                        user.login = YES;
//                        [[CacheManager sharedMnager] saveUserInfo:user];
//                        [self pingLogin];
//                    }];
//                } else {
//                    NSLog(@"%@",responseObject[@""])
//
//                    if ([responseObject[@"phone"] isKindOfClass:[NSArray class]]) {
//                          NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                          [dict setValue:responseObject[@"phone"] forKey:@"phone"];
//                          [arr addObject:dict];
//                    }
//
//                    if (responseObject[@"email"]) {
//                        NSString *st = [NSString stringWithFormat:@"%@",responseObject[@"email"]];
//                        if ([responseObject[@"email"] intValue] == 0 && st.length == 1) {
//                            NSLog(@"11111");
//                        } else {
//                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                            [dict setValue:responseObject[@"email"] forKey:@"email"];
//                            [arr addObject:dict];
//                        }
//                    }
//                    if (responseObject[@"google"]) {
//                        if ([responseObject[@"google"] intValue] == 0) {
//
//                        } else {
//                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                            [dict setValue:responseObject[@"google"] forKey:@"google"];
//                            [arr addObject:dict];
//                        }
//                    }
//                    user.token = responseObject[@"token"];
//                    [[CacheManager sharedMnager] saveUserInfo:user];
//                    LoginSheet *shet = [[LoginSheet alloc] init];
//                    shet.dataArray = arr;
//                    [shet showFromView:self.view];
//                    kStrongSelf(self);
//                    shet.clickBlock = ^(NSInteger index) {
//                        if (index == 0) {
//                            user.login = YES;
//                            [[CacheManager sharedMnager] saveUserInfo:user];
//                            [self dismissViewControllerAnimated:NO completion:^{
//                                [IWDefault removeObjectForKey:Cookie];
//                                [self pingLogin];
//                            }];
//                        }
//                    };
//                }
//            } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                });
//                [self showToastView:FailurMessage];
//            }];
//        }
//    }
//}





#pragma mark - Event Response
- (void)backAction {
    if (self.interface.callbackBlock) {
        self.interface.callbackBlock(@"callBack paramter");
    }
    if (self.cancleBlock) {
        self.cancleBlock();
    }
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (void)forgetAction {
    FindPasswordVC *find = [[FindPasswordVC alloc] init];
    [self.navigationController pushViewController:find animated:YES];
}

- (void)regiserAction {
    PhoneViewController *phone = [[PhoneViewController alloc] init];
    [self.navigationController pushViewController:phone animated:YES];
}


#pragma mark - lazy
- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = TwelveFontSize;
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseButton *)registBtn {
    if (!_registBtn) {
        _registBtn = [[BaseButton alloc] init];
        _registBtn.colorName = @"login_loginBtnCor";
        _registBtn.backColorName = @"login_registerBackground";
        [_registBtn setTitle:kLocalizedString(@"login_register") forState:UIControlStateNormal];
        _registBtn.titleLabel.font = TwelveFontSize;
        [_registBtn addTarget:self action:@selector(regiserAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registBtn;
}

- (BaseLabel *)logoTitle{
    if (!_logoTitle) {
        _logoTitle = [[BaseLabel alloc] init];
        _logoTitle.textColor = ThemeRedColor;
        _logoTitle.font = [UIFont systemFontOfSize:33];
        _logoTitle.textAlignment = 0;
        _logoTitle.text = kLocalizedString(@"login_logoTitle");
    }
    return _logoTitle;
}

- (BaseLabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[BaseLabel alloc] init];
        _contentLb.colorName = @"login_conentColor";
        _contentLb.textAlignment = 0;
        _contentLb.font = FourteenFontSize;
        _contentLb.text = kLocalizedString(@"login_content");
    }
    return _contentLb;
}

- (UITextField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[UITextField alloc] init];
        _accountTF.textColor = FieldTextColor;
        _accountTF.textAlignment = 0;
        _accountTF.font = TwelveFontSize;
        _accountTF.placeholder = kLocalizedString(@"login_tfPlace");
        _accountTF.adjustsFontSizeToFitWidth = YES;
    }
    return _accountTF;
}

- (BaseLabel *)line1 {
    if (!_line1) {
        _line1 = [[BaseLabel alloc] init];
        _line1.backColor = @"login_lineColor";
    }
    return _line1;
}

- (BaseLabel *)line2 {
    if (!_line2) {
        _line2 = [[BaseLabel alloc] init];
        _line2.backColor = @"login_lineColor";
    }
    return _line2;
}

- (UITextField *)contenTF {
    if (!_contenTF) {
        _contenTF = [[UITextField alloc] init];
        _contenTF.textColor = FieldTextColor;
        _contenTF.textAlignment = 0;
        _contenTF.placeholder = kLocalizedString(@"login_contPlace");
        _contenTF.font = TwelveFontSize;
        _contenTF.adjustsFontSizeToFitWidth = YES;
        _contenTF.clearsOnBeginEditing = YES;
        _contenTF.secureTextEntry = YES;
    }
    return _contenTF;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [AsyncTaskButton buttonWithType:UIButtonTypeCustom];
        _loginBtn.cornerRadius = 8;
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        _loginBtn.backgroundColor = ThemeGreenColor;
        [_loginBtn setTitle:kLocalizedString(@"login_loginBtn") forState:UIControlStateNormal];
        _loginBtn.delegate = self;
    }
    return _loginBtn;
}

- (BaseButton *)forgetBtn {
    if (!_forgetBtn) {
        _forgetBtn = [[BaseButton alloc] init];
        _forgetBtn.colorName = @"login_loginBtnCor";
        _forgetBtn.titleLabel.font = ThirteenFontSize;
        _forgetBtn.titleLabel.textAlignment = 1;
        [_forgetBtn setTitle:kLocalizedString(@"login_loginForget") forState:UIControlStateNormal];
        [_forgetBtn addTarget:self action:@selector(forgetAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetBtn;
}

@end
