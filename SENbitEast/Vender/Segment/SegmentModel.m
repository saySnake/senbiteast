//
//  SegmentModel.m
//  Senbit
//
//  Created by 张玮 on 2020/1/13.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SegmentModel.h"

@implementation SegmentModel

+ (instancetype)segmentWithTitle:(NSString *)title subTitle:(NSString *)subTitle titleColor:(UIColor *)titleColor{
    SegmentModel *model = [[SegmentModel alloc] init];
    model.title = title;
    model.subTitle = subTitle;
    model.titleColor = [UIColor redColor];
    return model;
}


@end
