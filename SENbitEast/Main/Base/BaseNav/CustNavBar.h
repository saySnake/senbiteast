//
//  CustNavBar.h
//  Drlink-IOS
//
//  Created by 张玮 on 15/3/17.
//  Copyright (c) 2015年 DrLink. All rights reserved.
// 导航栏

#import <UIKit/UIKit.h>
#import "BaseView.h"
#import "BaseButton.h"
typedef void (^navItemClick)(NSInteger index);
typedef void (^navTitleClick)(UIButton *button);

@class DDButton;

@interface CustNavBar :BaseView

@property (nonatomic, strong) BaseButton *rightBtn;
@property (nonatomic, strong) BaseButton *leftBtn;
@property (nonatomic, strong) BaseButton *titleBtn;

@property (nonatomic, copy)navItemClick itemClick;
@property (nonatomic, copy)navTitleClick btnClick;


- (void)setLeftItemImage:(NSString *)image;
- (void)setRightItemImage:(NSString *)image;
- (void)setBarTitle:(NSString *)title;
- (void)setCenterView:(UIView *)view;
- (void)setRightTitle:(NSString *)title;
- (void)setLeftTitle:(NSString *)title;
///**
// *  晃动
// *
// *  @param str   str
// *  @param shake shake
// */
//- (void)showAlertWithString:(NSString *)str isShaeke:(BOOL)shake;
//
@end

