//
//  YJSliderPointView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/30.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "YJSliderPointView.h"
#import "UIView+YJUiView.h"
#import "NSString+SuitScanf.h"

@interface YJSliderPointView ()

{
    CGFloat piceMoney;
    NSString *maxMoney;
    NSString *minMoney;
    CGFloat _with;
    NSInteger _sectionIndex;

}

@property (nonatomic, strong) NSMutableArray*itemarray;
@property (nonatomic, strong) UIView *lineView;

//上面的线
@property (nonatomic ,strong) UIView *topView;
@property (nonatomic, strong) CAGradientLayer *lineLayer;
@property (nonatomic, strong) NSArray *colorArray;
@property (nonatomic, strong) NSArray *colorLocationArray;
@property (nonatomic, strong) UIImageView *TapTip;//滑块
@property (nonatomic, strong) UILabel *TapTiplable;
@property (nonatomic, strong) UIView *scrView1;
@property (nonatomic, strong) UIView *scrView2;
@property (nonatomic, strong) UIView *scrView3;
@property (nonatomic, strong) UIView *scrView4;
@property (nonatomic, strong) UIView *scrView5;


#define TapTip_W       16.0
#define SLiderLine_H    1.0

@end

@implementation YJSliderPointView

- (instancetype)initWithFrame:(CGRect)frame {
    
    
    self = [super initWithFrame:frame];
    if (self) {
        [self SetupUI:@"0" end:@"1"];
    }
    return self;
}


- (void)SetupUI:(NSString *)first end:(NSString *)end {
    
    if ([end isEqualToString:@"0"]) {
        end = @"1";
    }
    NSString *st = [NSString subV1:end v2:first];
    NSString *s = [NSString divV1:st v2:@"5"];
    /**默认的是升序*/
    self.itemarray = [[NSMutableArray alloc] init];
    for (int i = 1;  i <= 5; i++) {
        NSString *a = [NSString mulV1:[NSString stringWithFormat:@"%d",i] v2:s];
        [self.itemarray addObject:a];
    }
    self.backgroundColor = [UIColor whiteColor];
    [self creatUI];
}

- (void)creatUI {
    [self addSubview:self.lineView];
    
    [self.lineView addSubview:self.topView];
    [self SetupTip];
}

- (void)SetupTip {
    
    CGFloat num = [[NSString stringWithFormat:@"%@",[_itemarray lastObject]] doubleValue];
    // 比率
    piceMoney =  num / self.lineView.frame.size.width;
    // 一段的长度 均分
//    _with = self.lineView.frame.size.width / _itemarray.count;
    _with = self.lineView.frame.size.width / 4;
    for (int i = 0; i <= 4; i++) {
//    for (int i=0; i<=_itemarray.count; i++) {
//        UIView *view = [UIView new];
//        view.frame = CGRectMake(i *_with+4, self.lineView.frame.origin.y - 4, 8, 8);
//        view.layer.cornerRadius=4;
//        view.backgroundColor = [UIColor redColor];
//        view.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
//        view.layer.borderWidth = 1;
//        [self addSubview:view];
        [self addSubview:[self creatView:i]];
    }
    
    [self addSubview:self.TapTiplable];
    [self addSubview:self.TapTip];
    
    UITapGestureRecognizer *pp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ppEvent:)];
    [self addGestureRecognizer:pp];
}


- (UIView *)creatView:(NSInteger )idx {
    if (idx == 0) {
        self.scrView1 = [[UIView alloc] init];
        self.scrView1.frame = CGRectMake(idx *_with + 4, self.lineView.frame.origin.y - 4, 8, 8);
        self.scrView1.layer.cornerRadius = 4;
        self.scrView1.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.scrView1.layer.borderColor = [UIColor whiteColor].CGColor;
        self.scrView1.layer.borderWidth = 1;
        return self.scrView1;
    } else if (idx == 1) {
        self.scrView2 = [[UIView alloc] init];
        self.scrView2.frame = CGRectMake(idx *_with + 4, self.lineView.frame.origin.y - 4, 8, 8);
        self.scrView2.layer.cornerRadius = 4;
        self.scrView2.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.scrView2.layer.borderColor = [UIColor whiteColor].CGColor;
        self.scrView2.layer.borderWidth = 1;
        return self.scrView2;
    } else if (idx == 2) {
        self.scrView3 = [[UIView alloc] init];
        self.scrView3.frame = CGRectMake(idx *_with + 4, self.lineView.frame.origin.y - 4, 8, 8);
        self.scrView3.layer.cornerRadius=4;
        self.scrView3.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.scrView3.layer.borderColor = [UIColor whiteColor].CGColor;
        self.scrView3.layer.borderWidth = 1;
        [self addSubview:self.scrView3];
        return self.scrView3;

    } else if (idx == 3) {
        self.scrView4 = [[UIView alloc] init];
        self.scrView4.frame = CGRectMake(idx *_with + 4, self.lineView.frame.origin.y - 4, 8, 8);
        self.scrView4.layer.cornerRadius = 4;
        self.scrView4.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.scrView4.layer.borderColor = [UIColor whiteColor].CGColor;
        self.scrView4.layer.borderWidth = 1;
        return self.scrView4;

    } else if (idx == 4) {
        self.scrView5 = [[UIView alloc] init];
        self.scrView5.frame = CGRectMake(idx *_with+4, self.lineView.frame.origin.y - 4, 8, 8);
        self.scrView5.layer.cornerRadius = 4;
        self.scrView5.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.scrView5.layer.borderColor = [UIColor whiteColor].CGColor;
        self.scrView5.layer.borderWidth = 1;
        return self.scrView5;
    }
    return nil;
}

- (void)PanEvent:(UIPanGestureRecognizer *)gesture{
    
    //NSLog(@"%@",gesture.view);
    if (gesture.state == UIGestureRecognizerStateChanged) {
        _TapTiplable.hidden = NO;
        CGPoint point = [gesture translationInView:self];
        CGFloat y = gesture.view.center.y;
        CGFloat x = gesture.view.center.x + point.x;
        if (x < self.lineView.frame.origin.x) {
            x = self.lineView.frame.origin.x;
        }
        if (x > self.lineView.width+TapTip_W/2) {
            x =  self.lineView.width+TapTip_W/2;
        }
        self.topView.frame = CGRectMake(0, 0, x - 5, SLiderLine_H);
        self.topView.backgroundColor = ThemeGreenColor;
        gesture.view.center = CGPointMake(x, y);
        [gesture setTranslation:CGPointMake(0, 0) inView:self];
        NSLog(@"%f",piceMoney);
        NSString *cal = [NSString stringWithFormat:@"%.2f",(x - TapTip_W/2)*piceMoney];
        minMoney = [NSString stringWithFormat:@"%@%%",[NSString mulV1:cal v2:@"100"]];
        _TapTiplable.centerX = x;
        _TapTiplable.text = minMoney;
        if (x > self.scrView1.x) {
            self.scrView1.backgroundColor = ThemeGreenColor;
        }
        if (x > self.scrView2.x) {
            self.scrView2.backgroundColor = ThemeGreenColor;
        }
        if (x > self.scrView3.x) {
            self.scrView3.backgroundColor = ThemeGreenColor;
        }
        if (x > self.scrView4.x) {
            self.scrView4.backgroundColor = ThemeGreenColor;
        }
        if (x > self.scrView5.x) {
            self.scrView5.backgroundColor = ThemeGreenColor;
        }
        
        if (x < self.scrView1.x) {
            self.scrView1.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (x < self.scrView2.x) {
            self.scrView2.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (x < self.scrView3.x) {
            self.scrView3.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (x < self.scrView4.x) {
            self.scrView4.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (x < self.scrView5.x) {
            self.scrView5.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        self.SliderBlock(cal);
    }
    else if(gesture.state == UIGestureRecognizerStateEnded){
        _TapTiplable.hidden = YES;
    }
}

- (void)ppEvent:(UITapGestureRecognizer *)gesture {
    if (gesture.numberOfTapsRequired == 1) {
        CGPoint point = [gesture locationInView:self];
//        CGFloat y = gesture.view.center.y;
//        CGFloat x = gesture.view.center.x +point.x;
        if (point.x < self.lineView.frame.origin.x) {
            point.x = self.lineView.frame.origin.x;
            
        }
        if (point.x > self.lineView.width) {
            point.x =  self.lineView.width + TapTip_W/2;
        }
        self.topView.frame = CGRectMake(0, 0, point.x - 5 , SLiderLine_H);
        self.topView.backgroundColor = ThemeGreenColor;
//        gesture.view.center = CGPointMake(x, y);
//        [gesture setTranslation:CGPointMake(0, 0) inView:self];
        NSString *cal = [NSString stringWithFormat:@"%.2f",(point.x - TapTip_W/2) * piceMoney];
        minMoney = [NSString stringWithFormat:@"%@%%",[NSString mulV1:cal v2:@"100"]];

//        minMoney = [NSString stringWithFormat:@"%.2f",(point.x - TapTip_W/2) * piceMoney];
        
        _TapTiplable.centerX = point.x;
        _TapTiplable.text = minMoney;
        self.TapTip.x = point.x - TapTip_W/2;
        
        if (point.x > self.scrView1.x) {
            self.scrView1.backgroundColor = ThemeGreenColor;
        }
        if (point.x > self.scrView2.x) {
            self.scrView2.backgroundColor = ThemeGreenColor;
        }
        if (point.x > self.scrView3.x) {
            self.scrView3.backgroundColor = ThemeGreenColor;
        }
        if (point.x > self.scrView4.x) {
            self.scrView4.backgroundColor = ThemeGreenColor;
        }
        if (point.x > self.scrView5.x) {
            self.scrView5.backgroundColor = ThemeGreenColor;
        }
        if (point.x < self.scrView1.x) {
            self.scrView1.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (point.x < self.scrView2.x) {
            self.scrView2.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (point.x < self.scrView3.x) {
            self.scrView3.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (point.x < self.scrView4.x) {
            self.scrView4.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        if (point.x < self.scrView5.x) {
            self.scrView5.backgroundColor = HEXCOLOR(0xDDDDDD);
        }
        self.SliderBlock(cal);
    }
}


- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(TapTip_W/2, self.height/2, self.frame.size.width - TapTip_W, SLiderLine_H)];
        _lineView.backgroundColor = HEXCOLOR(0xDDDDDD);
//        [_lineView.layer addSublayer:self.lineLayer];
    }
    return _lineView;
}

- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - TapTip_W, SLiderLine_H)];
        _topView.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _topView;
}

- (UIImageView *)TapTip{
    if (!_TapTip) {
        _TapTip = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, TapTip_W, 2*TapTip_W)];
        _TapTip.center = CGPointMake(TapTip_W/2, self.height/2 + SLiderLine_H/2);
//        _TapTip.layer.cornerRadius = 8;
        _TapTip.image = [UIImage imageNamed:@"trade_slider"];
        _TapTip.layer.masksToBounds = YES;
        _TapTip.backgroundColor = [UIColor clearColor];
        _TapTip.userInteractionEnabled = YES;
        _TapTip.alpha = 1;
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(PanEvent:)];
        [_TapTip addGestureRecognizer:pan];
    }
    return _TapTip;
}

- (UILabel *)TapTiplable{
    if (!_TapTiplable) {
        _TapTiplable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 20)];
        _TapTiplable.center = CGPointMake(self.lineView.frame.origin.x, self.height/2+SLiderLine_H/2-20);
        _TapTiplable.font = [UIFont systemFontOfSize:13];
        _TapTiplable.backgroundColor = [UIColor clearColor];
        _TapTiplable.textAlignment=NSTextAlignmentCenter;
        _TapTiplable.adjustsFontSizeToFitWidth = YES;
        _TapTiplable.text = @"0.0%";
        _TapTiplable.hidden = YES;
    }
    return _TapTiplable;
}

- (CAGradientLayer *)lineLayer{
    if (_lineLayer == nil) {
        self.colorArray = @[(id)[HEXCOLOR(0xFF5E37) CGColor],
                            (id)[HEXCOLOR(0xFF5E37) CGColor],
                            (id)[HEXCOLOR(0xFF5E37) CGColor],];
        self.colorLocationArray = @[@0.4, @0.6, @1];
        _lineLayer =  [CAGradientLayer layer];
        _lineLayer.frame = CGRectMake(0, 0, _lineView.frame.size.width, 1);
        [_lineLayer setLocations:self.colorLocationArray];
        [_lineLayer setColors:self.colorArray];
        [_lineLayer setStartPoint:CGPointMake(0, 0)];
        [_lineLayer setEndPoint:CGPointMake(1, 0)];
    }
    return _lineLayer;
}

- (NSMutableArray *)itemarray {
    if (!_itemarray) {
        _itemarray = [[NSMutableArray alloc] init];
    }
    return _itemarray;
}
@end
