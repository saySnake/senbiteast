//
//  WithDrawVC.m
//  Senbit
//
//  Created by 张玮 on 2020/2/20.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "WithDrawVC.h"
#import "SelectCoinView.h"
#import "SearchCoinVC.h"
#import "MoreChainView.h"
#import "AddDrawVC.h"
#import "MMScanViewController.h"
#import "WithCoinListModel.h"
#import "BalanceModel.h"
#import "BalanceCoinsModel.h"
#import "WithDrawSheet.h"
#import "NSString+SuitScanf.h"

//#import "SBSafeSheet.h"
//#import "CustomAlertView.h"

@interface WithDrawVC ()<SelectCoinViewDelegate,UITextFieldDelegate,SearchCoinVCDelegate,AddDrawVCDelegate>

@property (nonatomic ,strong) UIScrollView *scroll;
@property (nonatomic ,strong) SelectCoinView *selectCoinView;
@property (nonatomic ,strong) UILabel *chainTitle;
@property (nonatomic ,strong) MoreChainView *chainView;
@property (nonatomic ,strong) UILabel *drawTitle;
//提币地址
@property (nonatomic ,strong) UITextField *drawTextField;
@property (nonatomic ,strong) UIButton *saoBtn;
@property (nonatomic ,strong) UILabel *shuline1;
@property (nonatomic ,strong) UIButton *addBtn;
@property (nonatomic ,strong) UILabel *numLabel;
@property (nonatomic ,strong) UITextField *numTextField;
@property (nonatomic ,strong) UILabel *numCoinName;

@property (nonatomic ,strong) UILabel *shuline2;
@property (nonatomic ,strong) UIButton *allBtn;
@property (nonatomic ,strong) UILabel *feeTitle;
@property (nonatomic ,strong) UILabel *feeText;
@property (nonatomic ,strong) UILabel *feeCoin;
@property (nonatomic ,strong) UILabel *attenLabel;
@property (nonatomic ,strong) UIView *fotView;
@property (nonatomic ,strong) UIButton *drawBtn;
@property (nonatomic ,assign) CGFloat moreChainH;
@property (nonatomic ,strong) UILabel *line1;
@property (nonatomic ,strong) UILabel *line2;
@property (nonatomic ,strong) UILabel *line3;
@property (nonatomic ,strong) UILabel *arriveName;
@property (nonatomic ,strong) UILabel *arriveNum;
@property (nonatomic ,strong) NSMutableDictionary *dicData;

@property (nonatomic ,strong) UILabel *avaliable; //可用
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) WithCoinListModel *coinModel; //数据源model


@property (nonatomic ,strong) UILabel *tagTitle;
@property (nonatomic ,strong) UITextField *tagField;
@property (nonatomic ,strong) UILabel *tagLine;

@property (nonatomic ,copy) NSString *trade;

@property (nonatomic ,copy) NSString *uName;
@end

@implementation WithDrawVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.dataArr removeAllObjects];
    [self withDrawListCoin];
}

- (void)requestWithInfomation {
    [SBNetworTool getWithUrl:EastAsserts params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            NSArray<BalanceModel *> *m1 = [BalanceModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"balance"]];
            NSArray<BalanceCoinsModel *>*m2 = [BalanceCoinsModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"coins"]];
            if ([self.coinName.uppercaseString isEqualToString:@"USDT"]) {
                self.coinModel.moreChain = YES;
            } else if ([self.coinName.uppercaseString isEqualToString:@"TUSDT"]) {
                self.coinModel.moreChain = YES;
            } else if ([self.coinName.uppercaseString isEqualToString:@"EUSDT"]) {
                self.coinModel.moreChain = YES;
            }
            
            for (BalanceModel *model in m1) {
                if ([self.coinName isEqualToString:@"TUSDT"]) {
                    self.coinName = @"USDT";
                    self.uName = @"TUSDT";
                }
                if ([self.coinName isEqualToString:@"EUSDT"]) {
                    self.coinName = @"USDT";
                    self.uName = @"EUSDT";
                }
              
                if([model.currency isEqualToString:self.coinName]) {
                    self.coinModel.balance = model.balance;
                    if ([self.uName isEqualToString:@"EUSDT"]) {
                        self.coinName = @"EUSDT";
                    }
                    if ([self.uName isEqualToString:@"TUSDT"]) {
                        self.coinName = @"TUSDT";
                    }
                    break;
                }
            }
            
            self.avaliable.text = [NSString stringWithFormat:kLocalizedString(@"withdraw_able%@"),self.coinModel.balance];
            if ([self.coinName isEqualToString:@"EUSDT"] ||[self.coinName isEqualToString:@"TUSDT"] ) {
                self.numCoinName.text = @"USDT";
                self.feeCoin.text = @"USDT";

            } else {
                self.numCoinName.text = self.coinName;
                self.feeCoin.text = self.coinName;
            }
//            self.attenLabel.text = @"";
            self.attenLabel.text = [NSString stringWithFormat:kLocalizedString(@"withDraw_%@num%@coin%@Max%@coin"),self.coinModel.nwMinAmount,self.coinName,self.coinModel.nwMaxAmount,self.coinName];

            NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:kLocalizedString(@"withdraw_littleNum%@"),self.coinModel.nwMinAmount] attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
            self.numTextField.attributedPlaceholder = attrString1;
            [self make_layout];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)requestLimit {
    NSString *url = [NSString stringWithFormat:@"%@%@",EastWithLimit,self.coinName];
    [SBNetworTool getWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

//获取提币列表
- (void)withDrawListCoin {
    [SBNetworTool getWithUrl:EastCoinList params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.dicData = (NSMutableDictionary *)responseObject[@"data"];
            [self.dicData enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                NSLog(@"%@",key);
                if ([key isEqualToString:self.coinName]) {
                    self.coinModel = [[WithCoinListModel alloc] initWithDic:self.dicData[key]];
                    if ([self.coinName isEqualToString:@"EUSDT"] ||[self.coinName isEqualToString:@"TUSDT"] ) {
                        [self.selectCoinView.coinName setTitle:@"USDT" forState:UIControlStateNormal];
                    } else {
                        [self.selectCoinView.coinName setTitle:self.coinName forState:UIControlStateNormal];
                    }
                    self.feeText.text = self.coinModel.nwMinFee;
                    *stop = YES;
                }
            }];
            [self requestWithInfomation];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.numTextField.tag == 2) {
        
        self.feeText.text = [NSString mulV1:self.coinModel.nwFeeRatio v2:self.numTextField.text];
        double a = [self.feeText.text doubleValue];
        double b = [self.coinModel.nwMinFee doubleValue];
        double fee = MAX(a, b);
        self.feeText.text = [NSString stringWithFormat:@"%f",fee];
        if (fee < [self.coinModel.nwMinFee doubleValue]) {
            self.feeText.text = self.coinModel.nwMinFee;
        }
        if ([self.coinName isEqualToString:@"EUSDT"] ||[self.coinName isEqualToString:@"TUSDT"] ) {
            self.arriveNum.text = [NSString stringWithFormat:@"%@ %@",[NSString subV1:self.numTextField.text v2:self.feeText.text],@"USDT"];
        } else {
            self.arriveNum.text = [NSString stringWithFormat:@"%@ %@",[NSString subV1:self.numTextField.text v2:self.feeText.text],self.coinName];
        }
    
        if ([self.arriveNum.text doubleValue] < 0) {
            if ([self.coinName isEqualToString:@"EUSDT"] ||[self.coinName isEqualToString:@"TUSDT"] ) {
                self.arriveNum.text = [NSString stringWithFormat:@"0.00000 %@",@"USDT"];
            } else {
                self.arriveNum.text = [NSString stringWithFormat:@"0.00000 %@",self.coinName];
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   [textField resignFirstResponder];
   return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = WhiteColor;
    self.navBar.backgroundColor = WhiteColor;
    [self setNav];
    [self setUpView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"withDraw_navTitle")];
    _chainTitle.text = kLocalizedString(@"withDraw_chainName");
    _drawTitle.text = kLocalizedString(@"withDraw_withDrawAddress");
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"withDraw_textPlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    _drawTextField.attributedPlaceholder = attrString;
    _numLabel.text = kLocalizedString(@"withDraw_coinNum");
    [_allBtn setTitle:kLocalizedString(@"withDraw_all") forState:UIControlStateNormal];
    _feeTitle.text = kLocalizedString(@"withdraw_fee");
    _arriveName.text = kLocalizedString(@"withDraw_arriveNum");
    [_drawBtn setTitle:kLocalizedString(@"withdraw_withDraw") forState:UIControlStateNormal];
}
- (void)setNav {
    [self setNavBarTitle:kLocalizedString(@"withdraw_native")];
    [self setLeftBtnImage:@"leftBack"];
}

- (void)setUpView {
    [self.view addSubview:self.scroll];
    
    [self.scroll addSubview:self.selectCoinView];
    
    [self.scroll addSubview:self.chainTitle];
    
    self.moreChainH = self.chainView.chainH;
    [self.scroll addSubview:self.chainView];
    
    [self.scroll addSubview:self.drawTitle];
    
    [self.scroll addSubview:self.drawTextField];
    
    [self.scroll addSubview:self.saoBtn];
    
    [self.scroll addSubview:self.shuline1];
    
    [self.scroll addSubview:self.addBtn];
    
    [self.scroll addSubview:self.line1];
    [self.scroll addSubview:self.numLabel];
    
    [self.scroll addSubview:self.tagTitle];
    [self.scroll addSubview:self.tagField];
    [self.scroll addSubview:self.tagLine];
    
    [self.scroll addSubview:self.numTextField];
    
    [self.scroll addSubview:self.numCoinName];
    
    [self.scroll addSubview:self.shuline2];
    
    [self.scroll addSubview:self.allBtn];
    
    [self.scroll addSubview:self.line2];
    
    [self.scroll addSubview:self.avaliable];
    
    [self.scroll addSubview:self.feeTitle];
    
    [self.scroll addSubview:self.feeText];
    
    [self.scroll addSubview:self.feeCoin];
    
    [self.scroll addSubview:self.line3];
    
    [self.scroll addSubview:self.attenLabel];
    
    [self.view addSubview:self.fotView];
    
    [self.fotView addSubview:self.arriveName];
    
    [self.fotView addSubview:self.arriveNum];
    
    [self.fotView addSubview:self.drawBtn];
}

- (void)make_layout {
    
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW);
        make.height.mas_equalTo(DScreenH - kNavBarAndStatusBarHeight);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.bottom.mas_equalTo(-100);
    }];

    [self.selectCoinView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(DScreenW - 20);
    }];
    

    if (self.coinModel.moreChain) {
        if ([self.coinName isEqualToString:@"EUSDT"]) {
            self.chainView.seleIdx = 1;
        } else if([self.coinName isEqualToString:@"USDT"]) {
            self.chainView.seleIdx = 0;
        } else if ([self.coinName isEqualToString:@"TUSDT"]) {
            self.chainView.seleIdx = 2;
        }
        self.chainView.hidden = NO;
        self.chainTitle.hidden = NO;
        [self.chainTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.selectCoinView.mas_bottom).offset(10);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(15);
        }];
        [self.chainView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.chainTitle.mas_bottom).offset(10);
            make.height.mas_equalTo(self.moreChainH);
            make.width.mas_equalTo(self.scroll.mas_width);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
        }];
        
        [self.drawTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.chainView.mas_bottom).offset(10);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(15);
        }];
        [self vie];
    } else {
        self.chainView.hidden = YES;
        self.chainTitle.hidden = YES;
        [self.drawTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.selectCoinView.mas_bottom).offset(10);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(15);
        }];
        [self vie];
    }
}

- (void)vie {
    [self.drawTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.drawTitle.mas_bottom).offset(10);
        make.width.mas_equalTo(DScreenW - 100);
        make.height.mas_equalTo(20);
    }];
    
    [self.saoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-60);
        make.top.mas_equalTo(self.drawTextField.mas_top);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(20);
    }];
    
    [self.shuline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.saoBtn.mas_top);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(self.saoBtn.mas_right).offset(5);
    }];
    
    
    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.saoBtn.mas_right).offset(10);
        make.top.mas_equalTo(self.saoBtn.mas_top);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(20);
    }];
    
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.drawTextField.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(DScreenW -20);
    }];
    
    if (self.coinModel.isDestinationTagRequired) {
        [self.tagTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.width.mas_lessThanOrEqualTo(150);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(20);
            make.height.mas_equalTo(15);
        }];
        
        [self.tagField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.tagTitle);
            make.top.mas_equalTo(self.tagTitle.mas_bottom).offset(20);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(20);
        }];
        
        [self.tagLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.tagField);
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(self.tagField.mas_bottom).offset(10);
            make.height.mas_equalTo(0.5);
        }];

        [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.tagLine.mas_bottom).offset(20);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(100);
        }];
        
        [self.numTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.numLabel.mas_bottom).offset(15);
            make.width.mas_equalTo(DScreenW - 100);
            make.height.mas_equalTo(20);
            make.right.mas_equalTo(-100);
        }];
        
        [self.numCoinName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-60);
            make.top.mas_equalTo(self.numTextField.mas_top);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(20);
        }];
        
        [self.shuline2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.numCoinName.mas_top);
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(18);
            make.left.mas_equalTo(self.numCoinName.mas_right).offset(5);
        }];
        
        
        [self.allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.numCoinName.mas_right).offset(10);
            make.height.mas_equalTo(20);
            make.width.mas_equalTo(30);
            make.top.mas_equalTo(self.numCoinName.mas_top);
        }];
        
        [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.numTextField.mas_bottom).offset(5);
            make.height.mas_equalTo(0.5);
            make.width.mas_equalTo(DScreenW -20);
        }];
        
        [self.avaliable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line2.mas_bottom).offset(5);
            make.width.mas_equalTo(200);
            make.height.mas_equalTo(15);
        }];
        
        [self.feeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.avaliable.mas_bottom).offset(20);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(100);
        }];
        
        [self.feeText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.feeTitle.mas_bottom).offset(10);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(20);
        }];
        
        [self.feeCoin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.width.mas_equalTo(100);
            make.top.mas_equalTo(self.feeText.mas_top);
            make.height.mas_equalTo(20);
        }];
        
        [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(0.5);
            make.width.mas_equalTo(DScreenW - 20);
            make.top.mas_equalTo(self.feeText.mas_bottom).offset(5);
        }];
        
        [self.attenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line3.mas_bottom).offset(20);
            make.width.mas_equalTo(DScreenW - 20);
            make.bottom.mas_equalTo(0);
        }];
    } else {
        [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.line1.mas_bottom).offset(20);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(100);
        }];
        
        [self.numTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.numLabel.mas_bottom).offset(15);
            make.width.mas_equalTo(DScreenW - 100);
            make.height.mas_equalTo(20);
            make.right.mas_equalTo(-100);
        }];
        
        [self.numCoinName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-60);
            make.top.mas_equalTo(self.numTextField.mas_top);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(20);
        }];
        
        [self.shuline2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.numCoinName.mas_top);
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(18);
            make.left.mas_equalTo(self.numCoinName.mas_right).offset(5);
        }];
        
        
        [self.allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.numCoinName.mas_right).offset(10);
            make.height.mas_equalTo(20);
            make.width.mas_equalTo(30);
            make.top.mas_equalTo(self.numCoinName.mas_top);
        }];
        
        [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.numTextField.mas_bottom).offset(5);
            make.height.mas_equalTo(0.5);
            make.width.mas_equalTo(DScreenW -20);
        }];
        
        [self.avaliable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line2.mas_bottom).offset(5);
            make.width.mas_equalTo(200);
            make.height.mas_equalTo(15);
        }];
        
        [self.feeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.avaliable.mas_bottom).offset(20);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(100);
        }];
        
        [self.feeText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.feeTitle.mas_bottom).offset(10);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(20);
        }];
        
        [self.feeCoin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.width.mas_equalTo(100);
            make.top.mas_equalTo(self.feeText.mas_top);
            make.height.mas_equalTo(20);
        }];
        
        [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(0.5);
            make.width.mas_equalTo(DScreenW - 20);
            make.top.mas_equalTo(self.feeText.mas_bottom).offset(5);
        }];
        
        [self.attenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line3.mas_bottom).offset(20);
            make.width.mas_equalTo(DScreenW - 20);
            make.bottom.mas_equalTo(0);
        }];
    }

    
    [self.fotView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(self.view.mas_bottom).offset(-100);
        make.height.mas_equalTo(100);
        make.width.mas_equalTo(DScreenW);
    }];
    
    [self.arriveName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(100);
    }];
    
    [self.arriveNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(200);
    }];
    
    [self.drawBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.arriveName.mas_bottom).offset(10);
        make.height.mas_equalTo(50);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
    }];
}

- (NSMutableDictionary *)dicData {
    if (!_dicData) {
        _dicData = [NSMutableDictionary dictionary];
    }
    return _dicData;
}

- (void)sao:(UIButton *)sender {
    MMScanViewController *scanVc = [[MMScanViewController alloc] initWithQrType:MMScanTypeQrCode onFinish:^(NSString *result, NSError *error) {
        if (error) {
            NSLog(@"error: %@",error);
        } else {
            NSLog(@"扫描结果：%@",result);
            self.drawTextField.text = result;
        }
    }];
    [self.navigationController pushViewController:scanVc animated:YES];
}


- (void)add:(UIButton *)sender {
    AddDrawVC *add = [[AddDrawVC alloc] init];
    add.delegate = self;
    add.model = self.coinModel;
    [self.navigationController pushViewController:add animated:YES];
}

#pragma mark - selectAddressDelegate
- (void)selectAddressModel:(AddressModel *)model {
    self.drawTextField.text = model.address;
    self.tagField.text = model.destinationTag;
}

- (void)draw:(UIButton *)sender {
    
    if ([self.numTextField.text doubleValue] > [self.coinModel.nwMaxAmount doubleValue]) {
        [self showToastView:kLocalizedString(@"toastWithDrawMaxCoin")];
        return;
    }
    if (self.drawTextField.text.length == 0) {
        [self showToastView:kLocalizedString(@"toastWithDrawAddress")];
        return;
    }
    if ([self.numTextField.text doubleValue] < [self.coinModel.nwMinAmount doubleValue]) {
        [self showToastView:kLocalizedString(@"toastWithDrawMinCoin")];
        return;
    }

    if ([self.numTextField.text doubleValue] > [self.coinModel.balance doubleValue]) {
        [self showToastView:kLocalizedString(@"toastNotFullAssert")];
        return;
    }
    WS(weakSelf);
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    WithDrawSheet *sheet = [[WithDrawSheet alloc] init];
    sheet.googleAuth = info.googleAuthEnable;
    sheet.emailAuth = info.emailAuthEnable;
    sheet.phoneAuth = info.phoneAuthEnable;
    sheet.phoneLabel.text = info.phone;
    sheet.emailTitleName.text = info.email;
    sheet.account = info.phone;
    [sheet showFromView:self.view];
    sheet.clickBlock = ^(NSString * _Nonnull phoneCode, NSString * _Nonnull emailCode, NSString * _Nonnull googleCode, NSString * _Nonnull phone, NSString * _Nonnull email) {
        
        if (phoneCode.length == 0) {
            phoneCode = @"";
        }
        if (emailCode.length == 0) {
            emailCode = @"";
        }
        if (googleCode.length == 0) {
            googleCode = @"";
        }
        if (email.length == 0) {
            email = @"";
        }
        if (phone.length == 0) {
            phone = @"";
        }
        [weakSelf indeify:phoneCode emailCode:emailCode googleCode:googleCode phone:phone email:email];
    };
}


//校验验证码
- (void)indeify:(NSString *)phoneCode emailCode:(NSString *)emailCode googleCode:(NSString *)googleCode phone:(NSString *)phone email:(NSString *)email {
    NSDictionary *dic = @{@"emailCode":emailCode,
                          @"phoneCode":phoneCode,
                          @"googleCode":googleCode,
                          @"type":@"WITHDRAW"
    };
    WS(weakSelf);
    [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [weakSelf withDrawRequest:responseObject[@"data"][@"authToken"]];
        } else {
            [weakSelf showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [weakSelf showToastView:FailurMessage];
    }];
}

- (void)withDrawRequest:(NSString *)authToken {
    NSDictionary *dic = @{@"coinId":self.coinName,
                          @"address":self.drawTextField.text,
                          @"numbers":self.numTextField.text,
                          @"authToken":authToken,
                          @"destinationTag":self.tagField.text
    };
    [SBNetworTool postWithUrl:EastWithDraw params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (void)selectCoinNameAction {
    SearchCoinVC *searchCoin = [[SearchCoinVC alloc] init];
    searchCoin.delegate = self;
    searchCoin.dic = self.dicData;
    searchCoin.isWithDraw = YES;
    [self.navigationController pushViewController:searchCoin animated:YES];
}

- (void)selectWithDrawCoin:(WithCoinListModel *)mode {
//    if ([mode.coinName isEqualToString:@"USDT"]) {
//        self.coinModel = mode;
//        self.coinName = mode.coinName;
//        self.arriveNum.text = [NSString stringWithFormat:@"0.00000 %@",self.coinName];
//        [self make_layout];
//        self.chainView.seleIdx = 0;
//    } else  {
    self.numTextField.text = @"";
    self.feeText.text = @"";
    self.coinModel = mode;
    self.coinName = mode.coinName;
    self.arriveNum.text = [NSString stringWithFormat:@"-- %@",self.coinName];
    [self make_layout];
//    }
}


- (void)allAction {
    //每日提币最大值
    double dayTop = [self.coinModel.nwDailyMaxAmount doubleValue];
    //当前数量
    double totalNum = [self.coinModel.balance doubleValue];
    if (dayTop > totalNum) {
        self.numTextField.text = self.coinModel.balance;
    } else {
        self.numTextField.text = self.coinModel.nwDailyMaxAmount;
    }
    self.feeText.text = [NSString mulV1:self.coinModel.nwFeeRatio v2:self.numTextField.text];
    double a = [self.feeText.text doubleValue];
    double b = [self.coinModel.nwMinFee doubleValue];
    double fee = MAX(a, b);
    self.feeText.text = [NSString stringWithFormat:@"%f",fee];
    if (fee < [self.coinModel.nwMinFee doubleValue]) {
        self.feeText.text = self.coinModel.nwMinFee;
    }
    
    self.arriveNum.text = [NSString stringWithFormat:@"%@ %@",[NSString subV1:self.numTextField.text v2:self.feeText.text],self.coinName];
    if ([self.arriveNum.text doubleValue] < 0) {
        self.arriveNum.text = [NSString stringWithFormat:@"-- %@",self.coinName];
    }
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (UIScrollView *)scroll {
    if (!_scroll) {
        _scroll = [[UIScrollView alloc] init];
        _scroll.showsHorizontalScrollIndicator = NO;//隐藏水平滚动条
        _scroll.alwaysBounceVertical = NO;
        _scroll.backgroundColor = WhiteColor;
    }
    return _scroll;
}


- (SelectCoinView *)selectCoinView {
    if (!_selectCoinView) {
        _selectCoinView = [[SelectCoinView alloc] init];
        _selectCoinView.delegate = self;
        _selectCoinView.backgroundColor = ThemeDarkBlack;
    }
    return _selectCoinView;
}
 
- (UILabel *)chainTitle {
    if (!_chainTitle) {
        _chainTitle = [[UILabel alloc] init];
        _chainTitle.text = kLocalizedString(@"withdraw_linkName");
        _chainTitle.hidden = YES;
        _chainTitle.textColor = MainWhiteColor;
        _chainTitle.font = TwelveFontSize;
    }
    return _chainTitle;
}


- (MoreChainView *)chainView {
    if (!_chainView) {
        _chainView = [[MoreChainView alloc] initWithFrame:CGRectZero];
        _chainView.backgroundColor = WhiteColor;
        _chainView.hidden = YES;
        WS(weakSelf);
        _chainView.caluteResult = ^(NSString * _Nonnull restult,NSInteger idx) {
            NSLog(@"%@ %ld",restult,idx);
            weakSelf.drawTextField.text = @"";
            weakSelf.numTextField.text = @"";
            if (idx == 1) {
                weakSelf.coinName = @"EUSDT";
                weakSelf.uName = @"EUSDT";
                [weakSelf withDrawListCoin];
                weakSelf.arriveNum.text = [NSString stringWithFormat:@"-- USDT"];
            } else if (idx == 0) {
                weakSelf.coinName = @"USDT";
                [weakSelf withDrawListCoin];
                weakSelf.uName = @"USDT";
                weakSelf.arriveNum.text = [NSString stringWithFormat:@"-- USDT"];
            } else if (idx == 2) {
                weakSelf.coinName = @"TUSDT";
                weakSelf.uName = @"TUSDT";
                [weakSelf withDrawListCoin];
                weakSelf.arriveNum.text = [NSString stringWithFormat:@"-- USDT"];
            }
        };
    }
    return _chainView;
}


- (UILabel *)drawTitle {
    if (!_drawTitle) {
        _drawTitle = [[UILabel alloc] init];
        _drawTitle.font = TwelveFontSize;
        _drawTitle.text = kLocalizedString(@"withdraw_address");
        _drawTitle.textAlignment = 0;
        _drawTitle.textColor = MainWhiteColor;
    }
    return _drawTitle;
}


- (UITextField *)drawTextField {
    if (!_drawTextField) {
        _drawTextField = [[UITextField alloc] init];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"withdraw_placeholder") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _drawTextField.attributedPlaceholder = attrString;
        _drawTextField.font = TwelveFontSize;
        _drawTextField.delegate = self;
        _drawTextField.textColor = MainWhiteColor;
        _drawTextField.tag = 1;
        [_drawTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _drawTextField;
}

- (UIButton *)saoBtn {
    if (!_saoBtn) {
        _saoBtn = [[UIButton alloc] init];
        [_saoBtn setImage:[UIImage imageNamed:@"Assert_saomiao"] forState:UIControlStateNormal];
        [_saoBtn addTarget:self action:@selector(sao:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saoBtn;
}

- (UILabel *)shuline1 {
    if (!_shuline1) {
        _shuline1 = [[UILabel alloc] init];
        _shuline1.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _shuline1;
}

- (UIButton *)addBtn {
    if (!_addBtn) {
        _addBtn = [[UIButton alloc] init];
        [_addBtn addTarget:self action:@selector(add:)
              forControlEvents:UIControlEventTouchUpInside];
        [_addBtn setImage:[UIImage imageNamed:@"Assert_address"] forState:UIControlStateNormal];
    }
    return _addBtn;
}

- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _line1;
}


- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.text = kLocalizedString(@"withdraw_num");
        _numLabel.textAlignment = 0;
        _numLabel.textColor = MainWhiteColor;
        _numLabel.font = TwelveFontSize;
    }
    return _numLabel;
}

- (UITextField *)numTextField {
    if (!_numTextField) {
        _numTextField = [[UITextField alloc] init];
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"withDraw_minTwoCoin") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _numTextField.attributedPlaceholder = attrString1;
        _numTextField.font = TwelveFontSize;
        _numTextField.delegate = self;
        _numTextField.tag = 2;
        _numTextField.textColor = MainWhiteColor;
        _numTextField.keyboardType = UIKeyboardTypeNumberPad;
        [_numTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        UIToolbar  *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:)],
                               nil];
        _numTextField.inputAccessoryView = numberToolbar;
    }
    return _numTextField;
}

- (void)cancelNumberPad:(UITextField*)textField {
    [self.numTextField resignFirstResponder];
    self.numTextField.text = @"";
}


- (void)doneWithNumberPad:(UITextField*)textField {
    [self.numTextField resignFirstResponder];
}


- (UILabel *)numCoinName {
    if (!_numCoinName) {
        _numCoinName = [[UILabel alloc] init];
        _numCoinName.textColor = HEXCOLOR(0x999999);
        _numCoinName.textAlignment = 0;
        _numCoinName.adjustsFontSizeToFitWidth = YES;
        _numCoinName.font = TwelveFontSize;
    }
    return _numCoinName;
}

- (UILabel *)shuline2 {
    if (!_shuline2) {
        _shuline2 = [[UILabel alloc] init];
        _shuline2.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _shuline2;
}

- (UIButton *)allBtn {
    if (!_allBtn) {
        _allBtn = [[UIButton alloc] init];
        _allBtn.titleLabel.font = TwelveFontSize;
        [_allBtn addTarget:self action:@selector(allAction) forControlEvents:UIControlEventTouchUpInside];
        [_allBtn setTitle:kLocalizedString(@"withdraw_all") forState:UIControlStateNormal];
        [_allBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _allBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _allBtn;
}

- (UILabel *)line2 {
    if (!_line2) {
        _line2 = [[UILabel alloc] init];
        _line2.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _line2;
}

- (UILabel *)avaliable {
    if (!_avaliable) {
        _avaliable = [[UILabel alloc] init];
        _avaliable.textColor = HEXCOLOR(0xDDDDDD);
        _avaliable.textAlignment = 0;
        _avaliable.font = TwelveFontSize;
    }
    return _avaliable;
}

- (UILabel *)feeTitle {
    if (!_feeTitle) {
        _feeTitle = [[UILabel alloc] init];
        _feeTitle.text = kLocalizedString(@"withdraw_fee");
        _feeTitle.textAlignment = 0;
        _feeTitle.textColor = MainWhiteColor;
        _feeTitle.font = TwelveFontSize;
    }
    return _feeTitle;
}

- (UILabel *)feeText {
    if (!_feeText) {
        _feeText = [[UILabel alloc] init];
        _feeText.font = TwelveFontSize;
        _feeText.textAlignment = 0;
        _feeText.textColor = [UIColor blackColor];
    }
    return _feeText;
}

- (UILabel *)feeCoin {
    if (!_feeCoin) {
        _feeCoin = [[UILabel alloc] init];
        _feeCoin.font = TwelveFontSize;
        _feeCoin.textColor = HEXCOLOR(0x999999);
        _feeCoin.textAlignment = 2;
    }
    return _feeCoin;
}

- (UILabel *)line3 {
    if (!_line3) {
        _line3 = [[UILabel alloc] init];
        _line3.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _line3;
}

- (UILabel *)attenLabel {
    if (!_attenLabel) {
        _attenLabel = [[UILabel alloc] init];
        _attenLabel.numberOfLines = 0;
        _attenLabel.textAlignment = 0;
        _attenLabel.font = TwelveFontSize;
        _attenLabel.textColor = PlaceHolderColor;
        _attenLabel.backgroundColor = ThemeDarkBlack;
    }
    return _attenLabel;
}

- (UIView *)fotView {
    if (!_fotView) {
        _fotView = [[UIView alloc] init];
        _fotView.backgroundColor = WhiteColor;
    }
    return _fotView;
}

- (UILabel *)arriveName {
    if (!_arriveName) {
        _arriveName = [[UILabel alloc] init];
        _arriveName.text = kLocalizedString(@"withdraw_arriveNum");
        _arriveName.textColor = PlaceHolderColor;
        _arriveName.textAlignment = 0;
        _arriveName.font = TwelveFontSize;
    }
    return _arriveName;
}

- (UILabel *)arriveNum {
    if (!_arriveNum) {
        _arriveNum = [[UILabel alloc] init];
        _arriveNum.text = [NSString stringWithFormat:@"-- %@",self.coinName];
        _arriveNum.textAlignment = 2;
        _arriveNum.textColor = MainWhiteColor;
        _arriveNum.font = TwelveFontSize;
    }
    return _arriveNum;
}

- (UIButton *)drawBtn {
    if (!_drawBtn) {
        _drawBtn = [[UIButton alloc] init];
        [_drawBtn setTitle:kLocalizedString(@"withdraw_withDraw") forState:UIControlStateNormal];
        [_drawBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        _drawBtn.titleLabel.font = BOLDSYSTEMFONT(18);
        _drawBtn.titleLabel.textAlignment = 1;
        _drawBtn.cornerRadius = 3;
        _drawBtn.backgroundColor = HEXCOLOR(0xDDDDDD);
        _drawBtn.userInteractionEnabled = NO;
        [_drawBtn addTarget:self action:@selector(draw:) forControlEvents:UIControlEventTouchUpInside];
//        _drawBtn.backgroundColor = HEXCOLOR(0xEA1D76);

    }
    return _drawBtn;
}

- (UILabel *)tagTitle {
    if (!_tagTitle) {
        _tagTitle = [[UILabel alloc] init];
        _tagTitle.textAlignment = 0;
        _tagTitle.text = kLocalizedString(@"withdraw_withTag");
        _tagTitle.font = ThirteenFontSize;
        _tagTitle.textColor = [UIColor blackColor];
    }
    return _tagTitle;
}

- (UITextField *)tagField {
    if (!_tagField) {
        _tagField = [[UITextField alloc] init];
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"withdraw_placeholder") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _tagField.attributedPlaceholder = attrString1;
        _tagField.font = TwelveFontSize;
        _tagField.delegate = self;
        _tagField.tag = 2;
        _tagField.textColor = MainWhiteColor;
        _tagField.keyboardType = UIKeyboardTypeDecimalPad;
    }
    return _tagField;
}

- (UILabel *)tagLine {
    if (!_tagLine) {
        _tagLine = [[UILabel alloc] init];
        _tagLine.backgroundColor = HEXCOLOR(0xDDDDDD);
    }
    return _tagLine;
}


- (void)textFieldDidChange:(UITextField *)textField {
    if (self.numTextField.text.length > 0 && self.drawTextField.text.length > 0) {
        self.drawBtn.backgroundColor = ThemeGreenColor;
        self.drawBtn.userInteractionEnabled = YES;
    } else {
        self.drawBtn.backgroundColor = HEXCOLOR(0xDDDDDD);
        self.drawBtn.userInteractionEnabled = NO;
    }
}
@end
