//
//  AppDelegate.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/10.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabbarController;

@end

