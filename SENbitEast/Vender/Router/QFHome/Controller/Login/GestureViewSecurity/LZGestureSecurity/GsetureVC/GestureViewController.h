//
//  GestureViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


@class GestureViewController;

@protocol GestureViewDelegate <NSObject>

@optional

- (void)gestureView:(GestureViewController *)vc didSetted:(NSString *)psw ;
- (void)gestureView:(GestureViewController *)vc didUpdate:(NSString *)psw ;
- (void)gestureViewVerifiedSuccess:(GestureViewController *)vc ;
- (void)gestureViewCanceled:(GestureViewController *)vc ;

@end

typedef NS_ENUM(NSInteger, LZGestureType) {
    
    LZGestureTypeSetting = 0,
    LZGestureTypeVerifying,
    LZGestureTypeUpdate,
    LZGestureTypeScreen,
};

@interface GestureViewController : BaseViewController

@property (nonatomic, assign) id <GestureViewDelegate> delegate;
@property (nonatomic, assign) LZGestureType type;

- (void)showInViewController:(UIViewController *)vc type:(LZGestureType)type ;

- (void)dismiss;


@end

NS_ASSUME_NONNULL_END
