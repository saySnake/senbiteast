//
//  GestureIntrdouceViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^loginSuccessblock)();
typedef void (^loginCancleBlock)();

@interface GestureIntrdouceViewController : BaseViewController


@property (nonatomic ,copy)loginSuccessblock finishBlock;
@property (nonatomic ,copy)loginCancleBlock cancleBlock;

@end

NS_ASSUME_NONNULL_END
