//
//  AppDelegate.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/10.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "AppDelegate.h"
#import "QFRouter.h"
#import "QFMoudleProtocol.h"
#import "LeadViewController.h"
#import "VersionCheck.h"
#import "GestureScreen.h"
#import "BaseNavigationController.h"


#define IosAppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define kVersion @"appversion"

@interface AppDelegate ()<UITabBarControllerDelegate,GestureScreenDelegate> {
    dispatch_block_t _launchHandle;
}

@property (nonatomic ,strong) BaseNavigationController *homeNavi;
@property (nonatomic ,strong) BaseNavigationController *meNavi;
@property (nonatomic ,strong) BaseNavigationController *meNavi2;

@end

@implementation AppDelegate

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (void)getOtcRate {
    [SBNetworTool getWithUrl:EastRate params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            OtcExtinfo *info = [[OtcExtinfo alloc] init];
            info.USDT = responseObject[@"data"][@"LANG_CURRENCY_EN"][1];
            info.CNY = responseObject[@"data"][@"LANG_CURRENCY_ZH_CN"][1];
            info.HKD = responseObject[@"data"][@"LANG_CURRENCY_ZH_TW"][1];
            info.KRW = responseObject[@"data"][@"LANG_CURRENCY_KO"][1];
            [[CacheManager sharedMnager] saveOtcExinfo:info];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            
    }];
}

- (void)changeLanguage {
    self.homeNavi.tabBarItem.title = kLocalizedString(@"tab_home");
    self.meNavi2.tabBarItem.title = kLocalizedString(@"tab_assert");
    self.meNavi.tabBarItem.title = kLocalizedString(@"tab_trade");;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self userAgent];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [_window makeKeyAndVisible];
    [IWNotificationCenter addObserver:self selector:@selector(jumLogin) name:STJUMPLOGIN object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    
    [IWDefault removeObjectForKey:@"first"];

    // 3. 刷新币对列表
    [KMarket loadDataOfSymbols];

//    [KMarket readCachedDataOfMarket];

    //加载汇率数据
    [[RatesManager sharedRatesManager] loadDataOfRates];

//    [self getOtcRate];
    
    // Override point for customization after application launch.
    /*
     测试一个严谨的单例，需要重写alloc copy mutableCopy方法
     */
    QFRouter *r1 = [QFRouter router];
    QFRouter *r2 = [[QFRouter alloc]init];
    QFRouter *r3 = [r1 copy];
    QFRouter *r4 = [r1 mutableCopy];
    NSLog(@"\n%p\n%@\n%@\n%@",r1,r2,r3,r4);
    
    // Home组件
    id <MoudleHome>homeMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleHome)];
    homeMoudle.paramterForHome = @"MoudleHome";
    UIViewController *homeViewController = homeMoudle.interfaceViewController;
    self.homeNavi = [[BaseNavigationController alloc]initWithRootViewController:homeViewController];
    self.homeNavi.navigationBarHidden = YES;
    self.homeNavi.tabBarItem.title = kLocalizedString(@"tab_home");
    self.homeNavi.tabBarItem.image = [UIImage imageNamed:@"home"];
    
    // Me组件
    id <MoudleTrade>meMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleTrade)];
    UIViewController *meViewConterller = meMoudle.interfaceViewController;
    self.meNavi = [[BaseNavigationController alloc]initWithRootViewController:meViewConterller];
    self.meNavi.navigationBarHidden = YES;
    self.meNavi.tabBarItem.title = kLocalizedString(@"tab_trade");;
    self.meNavi.tabBarItem.image = [UIImage imageNamed:@"trade"];
    
//    id <MoudleMe>meeMoudle = [[QFRouter router]interfaceForURL:[NSURL URLWithString:@"MoudleMe://?paramterForMe=ModuleMe"]];
//    UIViewController *meViewConterllers = meeMoudle.interfaceViewController;
//    BaseNavigationController *meNavi1 = [[BaseNavigationController alloc]initWithRootViewController:meViewConterllers];
//    meNavi1.navigationBarHidden = YES;
//    meNavi1.tabBarItem.title = @"社区";
//    meNavi1.tabBarItem.image = [UIImage imageNamed:@"me"];
    
    
    id <MoudleMe>meeeMoudle = [[QFRouter router]interfaceForURL:[NSURL URLWithString:@"MoudleMe://?paramterForMe=ModuleMe"]];
    UIViewController *assetsViewController = meeeMoudle.interfaceViewController;
    self.meNavi2 = [[BaseNavigationController alloc]initWithRootViewController:assetsViewController];
    self.meNavi2.navigationBarHidden = YES;
    self.meNavi2.tabBarItem.title = kLocalizedString(@"tab_assert");
    self.meNavi2.tabBarItem.image = [UIImage imageNamed:@"assert"];

    
    // tabbr和window
    self.tabbarController = [[UITabBarController alloc]init];
    [[UITabBar appearance] setBarTintColor:WhiteColor];//这样写才能达到效果。
    
//    self.tabbarController.viewControllers = @[homeNavi,meNavi,meNavi1,meNavi2];
    self.tabbarController.viewControllers = @[self.homeNavi,self.meNavi,self.meNavi2];
    self.tabbarController.tabBar.tintColor = ThemeGreenColor;
    self.tabbarController.tabBar.backgroundColor = [UIColor whiteColor];
    self.tabbarController.delegate = self;
    self.tabbarController.tabBar.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.tabbarController.tabBar.layer.shadowOffset = CGSizeMake(0, -1);
    self.tabbarController.tabBar.layer.shadowOpacity = 0.3;


//    _window.rootViewController = tabbarController;
    _window.backgroundColor = [UIColor whiteColor];

    LeadViewController *lead;

    NSString *version = [IWDefault objectForKey:kVersion];
    if (![version isEqualToString:IosAppVersion]) {
        lead = [[LeadViewController alloc] initWithType:SLinkLeadTypeNewVersion];
        [IWDefault setObject:IosAppVersion forKey:kVersion];
        [IWDefault setObject:@"https://turkey.senbit.tech" forKey:@"server"];
        [IWDefault setObject:@"https://turkey.senbit.tech/" forKey:@"SenBitWSSUrl"];

    } else {
        [IWDefault setObject:IosAppVersion forKey:kVersion];
        lead = [[LeadViewController alloc] initWithType:SLinkLeadTypeStartAnimation];
    }
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:lead];
    lead.finishBlock = ^{
        if (lead.type == SLinkLeadTypeStartAnimation) {
            _window.rootViewController = self.tabbarController;
            //版本更新 注意plist
            #ifdef DEBUG
            [[VersionCheck sharedCheck] checkVersion];

            #else
            [[VersionCheck sharedCheck] checkVersion];
            
            #endif

        } else {
            self.window.rootViewController = self.tabbarController;
            [UIApplication sharedApplication].statusBarHidden = NO;
        }
    };
    self.window.rootViewController = nav;
    return YES;
}

- (void)jumLogin {
    LoginViewController *login = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    nav.navigationBarHidden = YES;
    login.finishBlock = ^{
        [(UINavigationController *)self.tabbarController.selectedViewController dismissViewControllerAnimated:NO completion:nil];
    };
    login.cancleBlock = ^{

    };
    [((UINavigationController *)self.tabbarController.selectedViewController) presentViewController:nav animated:NO completion:nil];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    //"我的"页面的tabbarItem的下标是2,
    //_privateToken == nil 为判断是否登录的条件
    
    if (viewController == tabBarController.viewControllers[3]) {
        
    }
    

    if (viewController == tabBarController.viewControllers[2] ) {
        [SBNetworTool getWithUrl:EastPing params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                if ([GestureTool isGestureEnable]) {
                    
                    [[GestureScreen shared] show];
                    tabBarController.selectedIndex = 2;
                    
                } else {
                    
                    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
                    if (user.login == YES) {
                        tabBarController.selectedIndex = 2;
                    } else {
                        LoginViewController *login = [[LoginViewController alloc] init];
                        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
                        nav.modalPresentationStyle = UIModalPresentationFullScreen;
                        nav.navigationBarHidden = YES;
                        login.finishBlock = ^{
                            [(UINavigationController *)tabBarController.selectedViewController dismissViewControllerAnimated:NO completion:nil];
                            tabBarController.selectedIndex = 2;
                        };
                        login.cancleBlock = ^{

                        };
                        [((UINavigationController *)tabBarController.selectedViewController) presentViewController:nav animated:NO completion:nil];

                    }
                }
            } else {
                
                
                LoginViewController *login = [[LoginViewController alloc] init];
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
                nav.modalPresentationStyle = UIModalPresentationFullScreen;
                nav.navigationBarHidden = YES;
                login.finishBlock = ^{
                    [(UINavigationController *)tabBarController.selectedViewController dismissViewControllerAnimated:NO completion:nil];
                    tabBarController.selectedIndex = 2;
                };
                login.cancleBlock = ^{

                };
                [((UINavigationController *)tabBarController.selectedViewController) presentViewController:nav animated:NO completion:nil];

            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {

        }];
        
        return NO;
    } else {
        return YES;
    }
}




- (void)userAgent {
    WKWebView* tempWebView = [[WKWebView alloc] initWithFrame:CGRectZero];
    [tempWebView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSString *userAgent = result;
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        NSString *ua = [NSString stringWithFormat:@"%@ TradeApp/%@(SENbit)", userAgent,version]; //项目版本号
        NSLog(@"%@",ua);
        [IWDefault setObject:ua forKey:TradeApp];
        [IWDefault synchronize];
    }];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
//    [IWDefault setObject:@"1" forKey:@"first"];
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




@end
