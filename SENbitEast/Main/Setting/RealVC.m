//
//  RealVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "RealVC.h"
#import "RealNameCell.h"
#import "RealNameVC.h"
#import "HeiRealNameVC.h"

@interface RealVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic ,strong) UITableView* contentTableView;

/** titleArray **/
@property (nonatomic ,strong) NSMutableArray *titleArray;

@end

@implementation RealVC

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestUserinfo];
}

- (void)requestUserinfo {
    NSDictionary *dic = @{};
    [SBNetworTool getWithUrl:EastLoginInfo params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
            info.email = responseObject[@"data"][@"email"];
            info.emailAuthEnable = [responseObject[@"data"][@"emailAuthEnable"] boolValue];
            info.googleAuthEnable = [responseObject[@"data"][@"googleAuthEnable"] boolValue];
            info.lastLoginAt = responseObject[@"data"][@"lastLoginAt"];
            info.lastLoginIP = responseObject[@"data"][@"lastLoginIP"];
            info.nationality = responseObject[@"data"][@"nationality"];
            info.phone = responseObject[@"data"][@"phone"];
            info.phoneAuthEnable = [responseObject[@"data"][@"phoneAuthEnable"] boolValue];
            info.realName = [responseObject[@"data"][@"realName"] boolValue];
            info.strictFace = [responseObject[@"data"][@"strictFace"] boolValue];
            info.strictRealName = [responseObject[@"data"][@"strictRealName"] boolValue];
            info.strictRealNameStatus = responseObject[@"data"][@"strictRealNameStatus"];
            info.tradePassword = [responseObject[@"data"][@"tradePassword"] boolValue];
            info.uid = responseObject[@"data"][@"uid"];
            [[CacheManager sharedMnager] saveUserInfo:info];
            [IWNotificationCenter postNotificationName:KLoadLeftView object:nil];
            [self.contentTableView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    self.view.backgroundColor = ThemeDarkBlack;
    [self setNavBarTitle:kLocalizedString(@"real_native")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"real_native")]; //kLocalizedString(@"search")];
    [self.contentTableView reloadData];
}

- (void)setUpView {
    [self.view addSubview:self.contentTableView];
    [self.contentTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55 ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (indexPath.row == 0) {
        if (info.realName) {
            return;
        }
        RealNameVC *real = [[RealNameVC alloc] init];
        [self.navigationController pushViewController:real animated:YES];
        
    } else if (indexPath.row == 1) {
        if (info.strictRealName) {
            return;
        }
        if ([info.strictRealNameStatus isEqualToString:@"applying"]) {
            return;
        }
        HeiRealNameVC *re = [[HeiRealNameVC alloc] init];
        [self.navigationController pushViewController:re animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RealNameCell *cell = [RealNameCell cellWithTableView:tableView];
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (indexPath.row == 0) {
        if (info.realName == NO) {
            cell.contentLb.text = kLocalizedString(@"real_please");
        } else {
            cell.contentLb.text = kLocalizedString(@"real_certifi");
        }
    } else if (indexPath.row == 1) {
        if (info.strictRealName == NO) {
            if ([info.strictRealNameStatus isEqualToString:@"applying"]) {
                cell.contentLb.text = kLocalizedString(@"real_applying");
            } else {
                cell.contentLb.text = kLocalizedString(@"real_please");
            }
        } else {
            cell.contentLb.text = kLocalizedString(@"real_certifi");
        }
    }
    cell.leftLb.text = self.titleArray[indexPath.row];
    return cell;
}


- (UITableView *)contentTableView {
    if (!_contentTableView) {
        _contentTableView        = [[UITableView alloc]initWithFrame:CGRectZero
                                                                           style:UITableViewStylePlain];
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.dataSource          = self;
        _contentTableView.delegate            = self;
        _contentTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.separatorStyle      = UITableViewCellSeparatorStyleSingleLine;
        _contentTableView.tableFooterView = [UIView new];
    }
    return _contentTableView;
}

- (NSMutableArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"real_real"),kLocalizedString(@"real_high"), nil];
    }
    return _titleArray;
}

@end
