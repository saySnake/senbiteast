//
//  AboutCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AboutCell.h"

@implementation AboutCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"AboutCell";
    AboutCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[AboutCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    [self.contentView addSubview:self.leftLb];
    [self.leftLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_lessThanOrEqualTo(200);
        make.height.mas_equalTo(25);
    }];
    
    [self.contentView addSubview:self.rightImg];
    [self.rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(10);
    }];
}

- (UILabel *)leftLb {
    if (!_leftLb) {
        _leftLb = [[UILabel alloc] init];
        _leftLb.textAlignment = 0;
        _leftLb.font = kFont16;
        _leftLb.textColor = HEXCOLOR(0x333333);
        _leftLb.adjustsFontSizeToFitWidth = YES;
    }
    return _leftLb;
}

- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [[UIImageView alloc] init];
        _rightImg.image = [UIImage imageNamed:@"fanhui"];
    }
    return _rightImg;
}

@end
