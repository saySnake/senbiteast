//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import "Senbit-Bridging-Header.h"

@import SocketIO;

@interface NewSocket : NSObject

@property (strong, nonatomic)  SocketIOClient* socket;

@property (strong, nonatomic) SocketManager* manager;

-(void)contect;

@end
