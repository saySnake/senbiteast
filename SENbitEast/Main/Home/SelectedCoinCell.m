//
//  SelectedCoinCell.m
//  Senbit
//
//  Created by 张玮 on 2020/3/5.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SelectedCoinCell.h"

@implementation SelectedCoinCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"SelectedCoinCell";
    SelectedCoinCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[SelectedCoinCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.contentView.backgroundColor = ThemeDarkBlack;
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    [self.contentView addSubview:self.coin];
    [self.contentView addSubview:self.price];
    [self.contentView addSubview:self.per];
    [self.contentView addSubview:self.starBtn];
    [self make_Layout];
}

- (void)make_Layout {
    [self.coin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView).offset(-30);
        make.top.mas_equalTo(self.coin.mas_top);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    [self.per mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.price.mas_right).offset(15);
        make.top.mas_equalTo(self.price.mas_top);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    [self.starBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.width.height.mas_equalTo(20);
        make.top.mas_equalTo(15);
    }];
}

- (UILabel *)coin {
    if (!_coin) {
        _coin = [[UILabel alloc] init];
        _coin.textColor = MainWhiteColor;
        _coin.textAlignment = 0;
        _coin.font = FourteenFontSize;
        _coin.adjustsFontSizeToFitWidth = YES;
    }
    return _coin;
}

- (UILabel *)price {
    if (!_price) {
        _price = [[UILabel alloc] init];
        _price.textColor = ThemeGreenColor;
        _price.textAlignment = 0;
        _price.adjustsFontSizeToFitWidth = YES;
        _price.font = BOLDSYSTEMFONT(14);
    }
    return _price;
}

- (UILabel *)per {
    if (!_per) {
        _per = [[UILabel alloc] init];
        _per.textColor = ThemeGreenColor;
        _per.textAlignment = 0;
        _per.font = FourteenFontSize;
    }
    return _per;
}

- (UIButton *)starBtn {
    if (!_starBtn) {
        _starBtn = [[UIButton alloc] init];
        [_starBtn setImage:[UIImage imageNamed:@"icon_attention"] forState:UIControlStateNormal];
        [_starBtn setImage:[UIImage imageNamed:@"icon_collect"] forState:UIControlStateSelected];
        [_starBtn addTarget:self action:@selector(collectionAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _starBtn;
}

- (void)collectionAction {
    
//    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
//    if (info.login == YES) {
//        NSString *url = [NSString stringWithFormat:@"%@%@",SenCollectionCoin,self.model.coinName];
//        if (self.starBtn.selected == YES) {
//            [SBNetworTool deleteWithUrl:url params:@{} success:^(id  _Nonnull responseObject) {
//                self.starBtn.selected = NO;
//                if (self.collectionBlock) {
//                    self.collectionBlock(BLOCKFailure);
//                }
//            } failure:^(id  _Nonnull error) {
//                NSLog(@"%@",error);
//            }];
//        } else {
//            [SBNetworTool postWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//                self.starBtn.selected = YES;
//                if (self.collectionBlock) {
//                    self.collectionBlock(BLOCKSuccess);
//                }
//            } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//                NSLog(@"%@",error);
//            }];
//        }
//    } else {
//        self.starBtn.selected = YES;
//    }
}



- (void)setModel:(OneTableSockeModel *)model {
    _model = model;
    NSString *name = model.coinName;
    self.coin.text = name;
    self.price.text = [NSString divV1:model.lastPrice v2:TENZero];
    NSString *per = [NSString returnFormatter:[NSString mulV1:model.change v2:@"100"]];
    if (per.floatValue > 0) {
        per = [NSString stringWithFormat:@"+%.2f%%",per.floatValue];
        self.per.textColor = ThemeGreenColor;
        self.price.textColor = ThemeGreenColor;
    } else {
        per = [NSString stringWithFormat:@"%.2f%%",per.floatValue];
        self.per.textColor = HEXCOLOR(0xED2482);
        self.price.textColor = HEXCOLOR(0xED2482);
    }
    self.per.text = per;
    if (model.isCollection == YES) {
        self.starBtn.selected = YES;
    } else {
        self.starBtn.selected = NO;
    }
}

@end
