//
//  NSString+SuitScanf.m
//  Senbit
//
//  Created by 张玮 on 2020/3/11.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "NSString+SuitScanf.h"



@implementation NSString (SuitScanf)
+ (NSString *)crabNumberSuitScanf:(NSString *)number {
    NSString *numberString = [number stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
    return numberString;
}

+ (NSString *)numberSuitScanf:(NSString*)number {
//    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[06-8])\\\\d{8}$";
//
//    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
//
//    BOOL isOk = [regextestmobile evaluateWithObject:number];
//    if (isOK) {
//        
//    } else {
//        
//    }
    NSString *numberString = [number stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    return numberString;
}

+ (BOOL)judgePassWordLegal:(NSString *)pass {
    BOOL result = false;
        if ([pass length] >= 8){
            // 判断长度大于8位后再接着判断是否同时包含数字和字符
            NSString * regex = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            result = [pred evaluateWithObject:pass];
        }
        return result;
}

+ (BOOL)checkEmail:(NSString *)email {

    //^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$

    NSString *regex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";

    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];

    return [emailTest evaluateWithObject:email];

}

+ (BOOL)isChinese:(NSString *)str {

    NSString *match = @"(^[\u4e00-\u9fa5]+$)";

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];

    return [predicate evaluateWithObject:str];

}

+ (BOOL)isCorrect:(NSString *)IDNumber {
    if (IDNumber.length < 18) {
        return NO;
    }
    NSMutableArray *IDArray = [NSMutableArray array];

        // 遍历身份证字符串,存入数组中

        for (int i = 0; i < 18; i++) {

            NSRange range = NSMakeRange(i, 1);

            NSString *subString = [IDNumber substringWithRange:range];

            [IDArray addObject:subString];

        }

        // 系数数组

        NSArray *coefficientArray = [NSArray arrayWithObjects:@"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7", @"9", @"10", @"5", @"8", @"4", @"2", nil];

        // 余数数组

        NSArray *remainderArray = [NSArray arrayWithObjects:@"1", @"0", @"X", @"9", @"8", @"7", @"6", @"5", @"4", @"3", @"2", nil];

        // 每一位身份证号码和对应系数相乘之后相加所得的和

        int sum = 0;

        for (int i = 0; i < 17; i++) {

            int coefficient = [coefficientArray[i] intValue];

            int ID = [IDArray[i] intValue];

            sum += coefficient * ID;

        }

        // 这个和除以11的余数对应的数

        NSString *str = remainderArray[(sum % 11)];

        // 身份证号码最后一位

        NSString *string = [IDNumber substringFromIndex:17];

        // 如果这个数字和身份证最后一位相同,则符合国家标准,返回YES
        if ([str isEqualToString:string]) {
            return YES;
        } else {
            return NO;

        }
}
// 字符串相加
+ (NSString *)addV:(NSString *)v1 v2:(NSString *)v2 {
    v1 = [self formartScientificNotationWithString:v1];
    v2 = [self formartScientificNotationWithString:v2];

    [v1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [v2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (!v1 || v1.length == 0 || [v1 isEqualToString:@"(null)"]) {
        v1 = @"0";
    }
    if (!v2 || v2.length == 0 || [v2 isEqualToString:@"(null)"]) {
        v2 = @"0";
    }
    NSDecimalNumber *a = [NSDecimalNumber decimalNumberWithString:v1];
    NSDecimalNumber *b = [NSDecimalNumber decimalNumberWithString:v2];
    NSDecimalNumber *result1 = [a decimalNumberByAdding:b];
    return [result1 stringValue];
}

// 字符串相加
+ (NSString *)addV1:(NSString *)v1 v2:(NSString *)v2 v3:(NSString *)v3{
    v1 = [self formartScientificNotationWithString:v1];
    v2 = [self formartScientificNotationWithString:v2];
    v3 = [self formartScientificNotationWithString:v3];

    [v1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [v2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [v3 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (!v1 || v1.length == 0 || [v1 isEqualToString:@"(null)"]) {
        v1 = @"0";
    }
    if (!v2 || v2.length == 0 || [v2 isEqualToString:@"(null)"]) {
        v2 = @"0";
    }
    if (!v3 || v3.length == 0 || [v3 isEqualToString:@"(null)"]) {
        v3 = @"0";
    }
    NSDecimalNumber *a = [NSDecimalNumber decimalNumberWithString:v1];
    NSDecimalNumber *b = [NSDecimalNumber decimalNumberWithString:v2];
    NSDecimalNumber *c = [NSDecimalNumber decimalNumberWithString:v3];
    NSDecimalNumber *result1 = [a decimalNumberByAdding:b];
    NSDecimalNumber *result2 = [result1 decimalNumberByAdding:c];
    return [result2 stringValue];
}

// 字符串减
+ (NSString *)subV1:(NSString *)v1 v2:(NSString *)v2 {
    v1 = [self formartScientificNotationWithString:v1];
    v2 = [self formartScientificNotationWithString:v2];

    [v1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [v2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (!v1 || v1.length == 0) {
        v1 = @"0";
    }
    if (!v2 || v2.length == 0) {
        v2 = @"0";
    }
    NSDecimalNumber *a = [NSDecimalNumber decimalNumberWithString:v1];
    NSDecimalNumber *b = [NSDecimalNumber decimalNumberWithString:v2];
    NSDecimalNumber *result2 = [a decimalNumberBySubtracting:b];
    return [result2 stringValue];
}


// 字符串乘
+ (NSString *)mulV1:(NSString *)v1 v2:(NSString *)v2 {
    v1 = [self formartScientificNotationWithString:v1];
    v2 = [self formartScientificNotationWithString:v2];
    [v1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [v2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (!v1 || v1.length == 0 || [v1 isEqualToString:@"(null)"]) {
        v1 = @"0";
    }
    if (!v2 || v2.length == 0 || [v2 isEqualToString:@"(null)"]) {
        v2 = @"0";
    }
    NSDecimalNumber *a = [NSDecimalNumber decimalNumberWithString:v1];
    NSDecimalNumber *b = [NSDecimalNumber decimalNumberWithString:v2];
    NSDecimalNumber *result3 = [a decimalNumberByMultiplyingBy:b];
    return [result3 stringValue];
}

// 字符串除 除数为0会 crash
+ (NSString *)divV1:(NSString *)v1 v2:(NSString *)v2 {
    v1 = [self formartScientificNotationWithString:v1];
    v2 = [self formartScientificNotationWithString:v2];

    v1 = [NSString stringWithFormat:@"%@",v1];
    if (!v1 || v1.length == 0 || [v1 isEqualToString:@"(null)"]) {
        v1 = @"0";
        return @"0";
    }
    if (!v2 || v2.length == 0 || [v2 isEqualToString:@"(null)"]) {
        v2 = @"1";
        return @"1";
    }
    if ([v2 isEqualToString:@"0"]) {
        v2 = @"1";
    }
    [v1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [v2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSDecimalNumber *a = [NSDecimalNumber decimalNumberWithString:v1];
    NSDecimalNumber *b = [NSDecimalNumber decimalNumberWithString:v2];
    NSDecimalNumber *result4 = [a decimalNumberByDividingBy:b];
    return [result4 stringValue];
}

+ (NSString *)getRoundFloat:(double)floatNumber withPrecisionNum:(NSInteger)precision {
    // 0.123456789  精度2
    //core foundation 的当前确切时间
    CFAbsoluteTime start = CFAbsoluteTimeGetCurrent();
    
    //精度要求为2，算出 10的2次方，也就是100，让小数点右移两位，让原始小数扩大100倍
    double fact = pow(10,precision);//face = 100
    //让小数扩大100倍，四舍五入后面的位数，再缩小100倍，这样成功的进行指定精度的四舍五入
    double result = round(fact * floatNumber) / fact; // result = 0.12
    //组合成字符串 @"%.3f"   想要打印百分号%字符串 需要在前面加一个百分号 表示不转译
    NSString *proString = [NSString stringWithFormat:@"%%.%ldf",(long)precision]; //proString = @"%.2f"
    // 默认会显示6位 多余的n补0，所以需要指定显示多少小数位  @"%.2f" 0.123000
    NSString *resultString = [NSString stringWithFormat:proString,result];
    //time实际上是一个double
    CFAbsoluteTime end = CFAbsoluteTimeGetCurrent();
//    NSLog(@"time cost: %0.6f", end - start);
    return resultString;
}

//IOS字符串截取保留小数点后两位
+ (NSString*)getTheCorrectNum:(NSString*)tempString {
    //计算截取的长度
    NSUInteger endLength = tempString.length;
    //判断字符串是否包含 .
    if ([tempString containsString:@"."]) {
        //取得 . 的位置
        NSRange pointRange = [tempString rangeOfString:@"."];
        NSLog(@"%lu",pointRange.location);
        //判断 . 后面有几位
        NSUInteger f = tempString.length - 1 - pointRange.location;
        //如果大于2位就截取字符串保留两位,如果小于两位,直接截取
        if (f > 2) {
            endLength = pointRange.location + 2;
        }
    }
    //先将tempString转换成char型数组
    NSUInteger start = 0;
    const char *tempChar = [tempString UTF8String];
    //遍历,去除取得第一位不是0的位置
    for (int i = 0; i < tempString.length; i++) {
        if (tempChar[i] == '0') {
            start++;
        }else {
            break;
        }
    }
    //根据最终的开始位置,计算长度,并截取
    NSRange range = {start,endLength-start};
    tempString = [tempString substringWithRange:range];
    return tempString;
}


//截取小数点后俩位 不进位（只截取俩位）
+ (NSString *)returnFormatter:(NSString*)stringNumber {
    stringNumber = [NSString stringWithFormat:@"%@",stringNumber];
    if([stringNumber rangeOfString:@"."].location == NSNotFound) {
        NSString *string_comp = [NSString stringWithFormat:@"%@.00",stringNumber];
        return string_comp;
    } else {
        NSArray *arrays= [stringNumber componentsSeparatedByString:@"."];
        NSString *s_f= [arrays objectAtIndex:0];
        NSString *s_e = [arrays objectAtIndex:1];
        if(s_e.length > 2) {
            s_e = [s_e substringWithRange:NSMakeRange(0,2)];
        }
        else if(s_e.length == 1) {
            s_e = [NSString stringWithFormat:@"%@0",s_e];
        }
        NSString* string_combine = [NSString stringWithFormat:@"%@.%@",s_f,s_e];
        return string_combine;
    }
        return @"";
}


/*
  介绍一下参数：
 price:需要处理的数字，
 position：保留小数点第几位，
 然后调用
 floats = 0.126;
 NSString*sv = [selfnotRounding:safterPoint:2];
 NSLog(@"sv = %@",sv);
 输出结果为：sv =0.12
 接下来介绍NSDecimalNumberHandler初始化时的关键参数：decimalNumberHandlerWithRoundingMode：NSRoundDown，
 NSRoundDown代表的就是 只舍不入。
 scale的参数position代表保留小数点后几位。
 */
+ (NSString*)notRounding:(float)price afterPoint:(int)position {
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler
                                                decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                scale:position
                                                raiseOnExactness:NO
                                                 raiseOnOverflow:NO
                                                raiseOnUnderflow:NO
                                             raiseOnDivideByZero:NO];
    NSDecimalNumber* ouncesDecimal;
    NSDecimalNumber* roundedOunces;
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    return [NSString stringWithFormat:@"%@",roundedOunces];
}

+ (NSString *)getTimeFromTimesTamp:(NSString *)timeStr {
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"HH:mm:ss"];
    if (timeStr.length == 13) {
        NSDate *stampDate = [NSDate dateWithTimeIntervalSince1970:[NSString divV1:timeStr v2:@"1000"].longLongValue];
        return [stampFormatter stringFromDate:stampDate];

    } else {
        NSDate *stampDate = [NSDate dateWithTimeIntervalSince1970:timeStr.longLongValue];
        return [stampFormatter stringFromDate:stampDate];
    }
}

+ (NSString *)getYearMonthDayTimesTamp:(NSString *)timeStr {
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *stampDate = [NSDate dateWithTimeIntervalSince1970:timeStr.longValue];
    return [stampFormatter stringFromDate:stampDate];
}

+ (NSString *)dateTimeDifferenceWithendTime:(NSString *)endTime {
    NSDateFormatter *date = [[NSDateFormatter alloc] init];
    [date setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate*startD = [NSDate date];
    NSDate*endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = end - start;
    NSString *days = [NSString stringWithFormat:@"%d",(int)value / (24*3600)];
    NSString *hours = [NSString stringWithFormat:@"%d",(int)(value-days.integerValue*24*3600) /  3600];
    NSString *minutes = [NSString stringWithFormat:@"%d",(int)value /60%60];
    NSString *seconds = [NSString stringWithFormat:@"%d",(int)value %60];
    NSString *format_time = [NSString stringWithFormat:kLocalizedString(@"subsidy_%@day%@hour%@min%@sec"),days,hours,minutes,seconds];
    return format_time;
}

+ (NSString *)timeFormatted:(long)totalSeconds {
    totalSeconds = totalSeconds/1000;
    int d = totalSeconds / (3600*24);
    int db =  totalSeconds - (d *24 *3600);
    int h = db /3600;
    int hb = db - (h*60*60);
    int m = hb/60;
    int s = hb - (m*60);
    return [NSString stringWithFormat:kLocalizedString(@"frenchDetail_%d%dhour%dmin%dsec"),d ,h, m, s];
}

+ (NSString *)startTimetoNowTime:(NSString *)str {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]]; // 默认使用0时区，所以需要时区的转换
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *newStr = [str substringToIndex:[str length] - 2];
    NSDate *date1 = [formatter dateFromString:newStr];
    
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *localeDate = [date  dateByAddingTimeInterval: interval];

//    NSDate *date = [NSDate date];
//计算时间间隔（单位是秒）
    NSTimeInterval time = [localeDate timeIntervalSinceDate:date1];
    ;
//计算天数、时、分、秒
    int days = ((int)time)/(3600*24);
    int hours = ((int)time)%(3600*24)/3600;
    int minutes = ((int)time)%(3600*24)%3600/60;
    int seconds = ((int)time)%(3600*24)%3600%60;
    int sec = (15 - minutes )*60 - seconds;
    NSString *st = [NSString stringWithFormat:@"%d",sec];
    return st;
}


//科学计数法
+ (NSString *)formartScientificNotationWithString:(NSString *)str {
    double conversionValue = [str doubleValue];
    NSString *doubleString = [NSString stringWithFormat:@"%lf", conversionValue];
    NSDecimalNumber *decNumber = [NSDecimalNumber decimalNumberWithString:doubleString];
    return [decNumber stringValue];
}


@end

