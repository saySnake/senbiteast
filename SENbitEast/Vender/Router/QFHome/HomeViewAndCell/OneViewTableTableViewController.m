//
//  OneViewTableTableViewController.m
//  HeaderViewAndPageView
//
//  Created by su on 16/8/8.
//  Copyright © 2016年 susu. All rights reserved.
//

#import "OneViewTableTableViewController.h"
#import "SYTypeButtonView.h"
#import "WMHomeCell.h"
#import "OneTableSockeModel.h"
#import "CoinSignle.h"
#import "QFRouter.h"
#import "QFMoudleProtocol.h"

@interface OneViewTableTableViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) XXWebQuoteModel *webModel;
@property(nonatomic ,strong) UITableView * myTableView;
@property (nonatomic ,strong) SYTypeButtonView *arrowView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) NSString *selectedCoin;
@property (nonatomic ,assign) BOOL isCollection;//是否自选
@property (nonatomic ,assign) BOOL isOpenZone;//是否开放
@property (nonatomic ,strong) NSMutableArray *collectionArr;
@property (nonatomic ,strong) NSMutableArray *openArr;
@property (nonatomic ,assign) NSInteger descIdx; //0不排序 1 //降序 //2 生序
@property (nonatomic ,assign) NSInteger currentIdx; // 0名称 //1 最新价 //2 涨跌幅
@property (nonatomic ,strong) NSMutableArray *array;
@end

@implementation OneViewTableTableViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Switch_TradeSymbol_NotificationName object:nil];
    [IWNotificationCenter removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    WS(weakSelf);
    _arrowView = [[SYTypeButtonView alloc] initWithFrame:CGRectMake(0.0, 0, DScreenW, heightTypeButtonView) view:self.view];
    _arrowView.buttonClick = ^(NSInteger index, BOOL isDescending, NSInteger currentIndex){
    NSLog(@"click index %ld, isDescending %d   %ld", index, isDescending, currentIndex);
        weakSelf.descIdx = currentIndex;
        weakSelf.currentIdx = index;
        [weakSelf.myTableView reloadData];
    };
    _arrowView.backgroundColor = WhiteColor;
    _arrowView.titles = @[kLocalizedString(@"home_coinName"), kLocalizedString(@"home_coinNewPrice"), kLocalizedString(@"home_coinUpAndDown")];
    _arrowView.enableTitles = @[kLocalizedString(@"home_coinName"), kLocalizedString(@"home_coinNewPrice"), kLocalizedString(@"home_coinUpAndDown")];
    _arrowView.colorNormal = kNormalTitleColor;
    //选择状态颜色
    _arrowView.colorSelected = ThemeGreenColor;
    NSDictionary *dict023 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"priceImage_normal"], keyImageNormal, [UIImage imageNamed:@"priceImage_down"], keyImageSelected, [UIImage imageNamed:@"priceImage_up"], keyImageSelectedDouble, nil];
    _arrowView.imageTypeArray = @[dict023, dict023, dict023];
    _arrowView.showScrollLine = NO;
    _arrowView.selectedIndex = -1;

    [self addObser];
    [self socketData];
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, DScreenH - kNavAndTabHeight - 10)];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    [self.myTableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.myTableView.showsVerticalScrollIndicator = NO;
    self.myTableView.showsHorizontalScrollIndicator = NO;
    self.myTableView.rowHeight = 65;
    self.myTableView.backgroundColor = WhiteColor;
    self.myTableView.separatorColor = SparatorColor;
    self.myTableView.tableFooterView = [[UIView alloc] init];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 110000
    if (@available(iOS 11.0, *)) {
        self.myTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
#endif
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.myTableView];
}


- (void)addObser {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    [IWNotificationCenter addObserver:self selector:@selector(selectedTableView:) name:Switch_TradeSymbol_NotificationName object:nil];
}

- (void)changeLanguage {
    [_arrowView removeFromSuperview];
    WS(weakSelf);
    _arrowView = [[SYTypeButtonView alloc] initWithFrame:CGRectMake(0.0, 0, DScreenW, heightTypeButtonView) view:self.view];
    _arrowView.buttonClick = ^(NSInteger index, BOOL isDescending, NSInteger currentIndex){
    NSLog(@"click index %ld, isDescending %d   %ld", index, isDescending, currentIndex);
        weakSelf.descIdx = currentIndex;
        weakSelf.currentIdx = index;
        [weakSelf.myTableView reloadData];
    };
    _arrowView.backgroundColor = WhiteColor;
    _arrowView.titles = @[kLocalizedString(@"home_coinName"), kLocalizedString(@"home_coinNewPrice"), kLocalizedString(@"home_coinUpAndDown")];
    _arrowView.enableTitles = @[kLocalizedString(@"home_coinName"), kLocalizedString(@"home_coinNewPrice"), kLocalizedString(@"home_coinUpAndDown")];
    _arrowView.colorNormal = kNormalTitleColor;
    //选择状态颜色
    _arrowView.colorSelected = ThemeGreenColor;
    NSDictionary *dict023 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"priceImage_normal"], keyImageNormal, [UIImage imageNamed:@"priceImage_down"], keyImageSelected, [UIImage imageNamed:@"priceImage_up"], keyImageSelectedDouble, nil];
    _arrowView.imageTypeArray = @[dict023, dict023, dict023];
    _arrowView.showScrollLine = NO;
    _arrowView.selectedIndex = -1;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    NSString * str = [CoinSignle shareInstance].coin;
//    if ( [str isEqualToString:@"selected"]) {
//        self.isCollection = YES;
//        [self getPairCollection];
//    }
//    else if ([str isEqualToString:@"开放区"]) {
//        self.isOpenZone = NO;//self.isOpenZone = YES;
//        //        [self openLocal];
//    } else {
//        self.isCollection = NO;
//    }
//    [self receiveHomeOver:self.array];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)selectedTableView:(NSNotification *)noti {
    NSDictionary *dict = noti.userInfo;
//
//    NSLog(@"%@%@",dict[@"coinName"],self.selectedCoin);
//
    if ([[CoinSignle shareInstance].coin isEqualToString:dict[@"coinName"]]) {
//        self.selectedCoin = dict[@"coinName"];
        if ([dict[@"coinName"] isEqualToString:kLocalizedString(@"home_selfSelected")]) {
            self.isCollection = YES;
            [self getPairCollection];
        }
//        else if ([dict[@"coinName"] isEqualToString:@"开放区"]) {
//            self.isOpenZone = NO;//self.isOpenZone = YES;
//    //        [self openLocal];
//        }
        else {
            self.isCollection = NO;
            [self receiveHomeOver:self.array];
        }
    }
}

- (NSString *)selectedCoin {
    if (!_selectedCoin) {
        _selectedCoin = [[NSString alloc] init];
    }
    return _selectedCoin;
}

- (void)socketData {
    [KQuoteSocket cancelWebSocketSubscribeWithWebSocketModel:nil lineType:@"home-overview"];
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        weakSelf.array = [OneTableSockeModel mj_objectArrayWithKeyValuesArray:data];
        [weakSelf receiveHomeOver:weakSelf.array];
    };
    NSDictionary *dic = @{};
    self.webModel.params = [NSMutableDictionary dictionary];
    self.webModel.params[@"topic"] = @"home-overview";
    self.webModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"home-overview"];
}



- (void)receiveHomeOver:(NSMutableArray *)array {
    if (self.isOpenZone) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in array) {
            for (OneTableSockeModel *m1 in self.openArr) {
                if ([m1.coinName isEqualToString:model.coinName]) {
                    [self.dataArr addObject:model];
                }
            }
        }
    }
    if (self.isCollection) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in array) {
            for (OneTableSockeModel *m1 in self.collectionArr) {
                if ([m1.coinName isEqualToString:model.coinName]) {
                    [self.dataArr addObject:model];
                }
            }
        }
    }
    if (self.isCollection  == NO && self.isOpenZone == NO) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in array) {
            NSUInteger seleLenth = [CoinSignle shareInstance].coin.length;//self.selectedCoin.length;
            NSString *endStr = [model.coinName substringFromIndex:model.coinName.length - seleLenth];
            NSString *s = [CoinSignle shareInstance].coin;
            if ([endStr isEqualToString:s]) {
                [self.dataArr addObject:model];
            }
        }
    }
    [self.myTableView reloadData];
}

//自选判重
- (void)getPairCollection {
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (info.login) {
        [SBNetworTool getWithUrl:EastOptionalPairs params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                NSMutableArray * arr = responseObject[@"data"];
                [KMarket.favoritesArray removeAllObjects];
                for (NSString *s in arr) {
                    [KMarket addFavoriteSymbolId:s];
                }
                [self.collectionArr removeAllObjects];
                for (NSString *str in arr) {
                    OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
                    model.coinName = str;
                    [self.collectionArr addObject:model];
                }
//                [self socketData];
                [self receiveHomeOver:self.array];
            }

        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
            [self.collectionArr addObject:model];
            [self socketData];
        }];
    } else {
//        KMarket.favoritesArray
        NSLog(@"%@",KMarket.favoritesArray);
        [self.collectionArr removeAllObjects];
        for (NSString *str in KMarket.favoritesArray) {
            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
            model.coinName = str;
            [self.collectionArr addObject:model];
        }
        [self receiveHomeOver:self.array];
//        [self socketData];
    }
}


//- (void)openLocal {
//    [SBNetworTool getWithUrl:SenOpenZone params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//        NSLog(@"%@",responseObject);
//        NSArray * arr = responseObject[@"data"];
//        [self.openArr removeAllObjects];
//        for (NSDictionary *str in arr) {
//            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
//            model.coinName = str[@"id"];
//            [self.openArr addObject:model];
//        }
//        [self socketData];
//
//    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//        OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
//        [self.openArr addObject:model];
//        [self socketData];
//    }];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WMHomeCell  * cell = [WMHomeCell cellWithTableView:tableView];
    [self sortDataArray];
    cell.model = self.dataArr[indexPath.row];
    return cell;
}

- (void)sortDataArray {
    if (self.currentIdx == 0 && self.descIdx == 1) { //名称降序
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"coinName" ascending:NO];
        [self.dataArr sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

    } else if (self.currentIdx == 0 && self.descIdx == 2) { //名称生序
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"coinName" ascending:YES];
        [self.dataArr sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

    } else if (self.currentIdx == 1 && self.descIdx == 1) { //最新价降序
        [self.dataArr sortUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
            OneTableSockeModel *m1 = obj1;
            OneTableSockeModel *m2 = obj2;
            if ([m1.price doubleValue] > [m2.price doubleValue]) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];

    } else if (self.currentIdx == 1 && self.descIdx == 2) { //最新价生序
        [self.dataArr sortUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
            OneTableSockeModel *m1 = obj1;
            OneTableSockeModel *m2 = obj2;
            if ([m1.price doubleValue] < [m2.price doubleValue]) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];

    } else if (self.currentIdx == 2 && self.descIdx == 1) { //涨跌幅降序
        [self.dataArr sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            OneTableSockeModel *m1 = obj1;
            OneTableSockeModel *m2 = obj2;
            if ([m1.change floatValue] < [m2.change floatValue]) {
                return NSOrderedDescending;
            } else if (m1.change > m2.change) {
                return NSOrderedAscending;//升序
            } else {
                return NSOrderedSame;//相等
            }
        }];

    } else if (self.currentIdx == 2 && self.descIdx == 2) { //涨跌幅生序
        
        [self.dataArr sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            OneTableSockeModel *m1 = obj1;
            OneTableSockeModel *m2 = obj2;
            if ([m1.change floatValue] > [m2.change floatValue]) {
                return NSOrderedDescending;
            } else if (m1.change < m2.change) {
                return NSOrderedAscending;//升序
            } else {
                return NSOrderedSame;//相等
            }
        }];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.arrowView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OneTableSockeModel *model = self.dataArr[indexPath.row];
    
    [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
           KDetail.symbolModel = obj;
           KTrade.coinTradeModel = obj;
           if ([KMarket.favoritesArray containsObject:model.coinName]) {
               KDetail.symbolModel.favorite = YES;
               KTrade.coinTradeModel.favorite = YES;
           }
           NSLog(@"%@",KDetail.symbolModel.symbolId);
           *stop = YES;
         }
   }];
    
    id <MoudleLine>lineMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleLine)];
    UIViewController *viewController = lineMoudle.interfaceViewController;
    lineMoudle.callbackBlock = ^(id parameter) {
        NSLog(@"return paramter = %@",parameter);
    };
    [self.navigationController pushViewController:viewController animated:YES];
}


//降序
- (void)desOrderBy {
    
}

//生序
- (void)ascendBy {
    
}


//- (SYTypeButtonView *)arrowView {
//    if (!_arrowView) {
//        WS(weakSelf);
//        _arrowView = [[SYTypeButtonView alloc] initWithFrame:CGRectMake(0.0, 0, DScreenW, heightTypeButtonView) view:self.view];
//        _arrowView.buttonClick = ^(NSInteger index, BOOL isDescending, NSInteger currentIndex){
//        NSLog(@"click index %ld, isDescending %d   %ld", index, isDescending, currentIndex);
//            weakSelf.descIdx = currentIndex;
//            weakSelf.currentIdx = index;
//            [weakSelf.myTableView reloadData];
//        };
//        _arrowView.backgroundColor = WhiteColor;
//        _arrowView.titles = @[kLocalizedString(@"home_coinName"), kLocalizedString(@"home_coinNewPrice"), kLocalizedString(@"home_coinUpAndDown")];
//        _arrowView.enableTitles = @[kLocalizedString(@"home_coinName"), kLocalizedString(@"home_coinNewPrice"), kLocalizedString(@"home_coinUpAndDown")];
//        _arrowView.colorNormal = kNormalTitleColor;
//        //选择状态颜色
//        _arrowView.colorSelected = ThemeGreenColor;
//        NSDictionary *dict023 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"priceImage_normal"], keyImageNormal, [UIImage imageNamed:@"priceImage_down"], keyImageSelected, [UIImage imageNamed:@"priceImage_up"], keyImageSelectedDouble, nil];
//        _arrowView.imageTypeArray = @[dict023, dict023, dict023];
//        _arrowView.showScrollLine = NO;
//        _arrowView.selectedIndex = -1;
//    }
//    return _arrowView;
//}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (NSMutableArray *)collectionArr {
    if (!_collectionArr) {
        _collectionArr = [[NSMutableArray alloc] init];
    }
    return _collectionArr;
}

- (NSMutableArray *)openArr {
    if (!_openArr) {
        _openArr = [[NSMutableArray alloc] init];
    }
    return _openArr;
}

- (NSMutableArray *)array {
    if (!_array) {
        _array = [[NSMutableArray alloc] init];
    }
    return _array;
}

@end
