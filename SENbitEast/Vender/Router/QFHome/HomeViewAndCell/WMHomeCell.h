//
//  WMHomeCell.h
//  Senbit
//
//  Created by 张玮 on 2020/3/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneTableSockeModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WMHomeCell : UITableViewCell

@property (nonatomic ,strong) OneTableSockeModel *model;
+ (instancetype)cellWithTableView:(UITableView *)tableView;

//名字
@property (nonatomic ,strong) UILabel *name;
@property (nonatomic ,strong) UILabel *nameTrade;

//量
@property (nonatomic ,strong) UILabel *volLabel;
//美金
@property (nonatomic ,strong) UILabel *dolerLabel;
//人民币
@property (nonatomic ,strong) UILabel *cnyLabel;
//百分比
@property (nonatomic ,strong) UILabel *perLabel;

@end

NS_ASSUME_NONNULL_END
