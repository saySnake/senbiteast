//
//  AssertModel.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/2.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AssertModel.h"

@implementation AssertModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{
        @"balance" : @"BalanceModel",
        @"coins" : @"BalanceCoinsModel"
    };
}

@end
