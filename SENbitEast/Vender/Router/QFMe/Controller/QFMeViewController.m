//
//  QFMeViewController.m
//  QFMoudle
//
//  Created by 情风 on 2018/11/5.
//  Copyright © 2018年 qingfengiOS. All rights reserved.
//

#import "QFMeViewController.h"
#import "QFRouter.h"
#import "QFMoudleProtocol.h"
#import "AssertCell.h"
#import "AssertModel.h"
#import "BalanceModel.h"
#import "DepositVC.h"
#import "WithDrawVC.h"
#import "NSString+SuitScanf.h"
#import "HistoryVC.h"

@interface QFMeViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
/** 资产 */
@property (nonatomic ,strong) BaseLabel *titleLb;
/** refresh **/
@property (nonatomic ,strong) BaseButton *reBtn;
/** 资产数量 **/
@property (nonatomic ,strong) BaseLabel *assertNumLb;
/** =人民币 **/
@property (nonatomic ,strong) BaseLabel *cnyLb;
/** deposit **/
@property (nonatomic ,strong) BaseButton *depositBtn;
/** withDraw **/
@property (nonatomic ,strong) BaseButton *withDrawBtn;
/** ToolView **/
@property (nonatomic ,strong) BaseView *toolView;
/** hiddenbtn **/
@property (nonatomic ,strong) BaseButton *hidenBtn;
/** searchTextField **/
@property (nonatomic ,strong) UITextField *searchField;
/** tabView **/
@property (nonatomic ,strong) UITableView *tbView;
/** 数据源 **/
@property (nonatomic ,strong) NSMutableArray *dataArray;


@end

@implementation QFMeViewController

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.dataArray removeAllObjects];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self requestAssert];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestAssert];
    [self initAppreaence];
    [self setUpUI];
    [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)changeLanguage {
    self.titleLb.text = kLocalizedString(@"assert_title");
    [self.hidenBtn setTitle:kLocalizedString(@"assert_hide") forState:UIControlStateNormal];
    self.searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kLocalizedString(@"assert_search") attributes:@{NSForegroundColorAttributeName:ThemeGrayColor}];
}

- (void)requestAssert {
    [SBNetworTool getWithUrl:EastAsserts params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        
        if (SuccessCode == 200) {
            [self removeNotData];
            NSArray<BalanceModel *> *m1 = [BalanceModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"balance"]];
            NSString *total = @"0";
            NSString *total1 = @"0";
            NSString *total2 = @"0";

            NSString *btcTotal1 = @"0";
            NSString *btcTotal2 = @"0";
            NSString *btcTotal = @"0";

            NSString *utotal = @"0";
            NSString *utotal1 = @"0";
            NSString *utotal2 = @"0";
            
            NSString *usdtTotal1 = @"0";
            NSString *usdtTotal2 = @"0";
            NSString *usdtTotal = @"0";
            for (BalanceModel * model in m1) {
                total1 = [NSString mulV1:model.freezed v2:model.btcRate];
                total2 = [NSString mulV1:model.locked v2:model.btcRate];
                total = [NSString mulV1:model.balance v2:model.btcRate];

                btcTotal2 = [NSString addV:total1 v2:total2];
                btcTotal1 = [NSString addV:total v2:btcTotal2];
                btcTotal =  [NSString addV:btcTotal v2:btcTotal1] ;

                
                utotal1 = [NSString mulV1:model.freezed v2:model.usdtRate];
                utotal2 = [NSString mulV1:model.locked v2:model.usdtRate];
                utotal = [NSString mulV1:model.balance v2:model.usdtRate];
                
                
                usdtTotal2 = [NSString addV:utotal1 v2:utotal2];
                usdtTotal1 = [NSString addV:usdtTotal2 v2:utotal];
                usdtTotal = [NSString addV:usdtTotal v2:usdtTotal1];
            }
            self.assertNumLb.text = [NSString stringWithFormat:@"%.8f BTC",btcTotal.floatValue];
            self.cnyLb.text = [NSString stringWithFormat:@"%@",[[RatesManager sharedRatesManager] getRatesPriceValue:[usdtTotal doubleValue]]];

            NSArray<BalanceCoinsModel *>*m2 = [BalanceCoinsModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"coins"]];
            for (BalanceModel *m11 in m1) {
                for (BalanceCoinsModel *m22 in m2) {
                    if ([m22.ID isEqualToString:m11.currency]) {
                        m22.balance = m11.balance;
                        m22.locked = m11.locked;
                        m22.freezed = m11.freezed;
                        m22.usdtRate = m11.usdtRate;
                        break;
                    }
                }
            }
            [self.dataArray addObjectsFromArray:m2];
            [self.tbView reloadData];

        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}



#pragma mark - InitAppreaence
- (void)initAppreaence {
    self.navBar.backgroundColor = ThemeGreenColor;
    self.view.backgroundColor = ThemeGreenColor;
}


- (void)setUpUI {
    [self.view addSubview:self.titleLb];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(0);
        make.width.lessThanOrEqualTo(@300);
        make.height.mas_equalTo(36);
    }];
    
    [self.view addSubview:self.reBtn];
    [self.reBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.titleLb);
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(34);
    }];
    
    [self.view addSubview:self.assertNumLb];
    [self.assertNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(17);
        make.width.mas_lessThanOrEqualTo(300);
        make.height.mas_equalTo(35);
    }];
    
    [self.view addSubview:self.cnyLb];
    [self.cnyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.assertNumLb.mas_bottom).offset(21);
        make.width.mas_lessThanOrEqualTo(200);
        make.height.mas_equalTo(20);
    }];
    
    [self.view addSubview:self.withDrawBtn];
    [self.withDrawBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.width.height.mas_equalTo(35);
        make.top.mas_equalTo(self.assertNumLb.mas_bottom).offset(10);
    }];
    
    [self.view addSubview:self.depositBtn];
    [self.depositBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.withDrawBtn.mas_left).offset(-15);
        make.top.mas_equalTo(self.withDrawBtn);
        make.width.height.mas_equalTo(self.withDrawBtn);
    }];

    [self.view addSubview:self.toolView];
    [self.toolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.withDrawBtn.mas_bottom).offset(20);
        make.height.mas_equalTo(40);
    }];

    [self.toolView addSubview:self.hidenBtn];
    [self.hidenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.centerY.mas_equalTo(self.toolView);
        make.height.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(250);
    }];

//    [self.toolView addSubview:self.searchField];
//    [self.searchField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.toolView);
//        make.right.mas_equalTo(-20);
//        make.width.mas_equalTo(80);
//        make.height.mas_equalTo(20);
//    }];
    
    if (@available(iOS 11.0, *)) {
        self.tbView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.tbView];
    [self.tbView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.toolView.mas_bottom);
        make.bottom.mas_equalTo(-kTabBarHeight);
    }];
}

#pragma mark - Event Response
- (void)buttonAction {
    id <MoudleHome>homeMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleHome)];
    homeMoudle.titleString = @"ModleMe";
    homeMoudle.descString = @"pushed form MeMoudle";
    UIViewController *viewController = homeMoudle.detailViewController;
    homeMoudle.callbackBlock = ^(id parameter) {
        NSLog(@"return paramter = %@",parameter);
    };
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Getters

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AssertCell *cell = [AssertCell cellWithTbaleView:tableView];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}



#pragma mark - action
- (void)hidenAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self requestSmallAssert];
    } else {
        [self requestAssert];
    }
}

- (void)requestSmallAssert {
    [SBNetworTool getWithUrl:EastAsserts params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        [self.dataArray removeAllObjects];
        if (SuccessCode == 200) {
            NSArray<BalanceModel *> *m1 = [BalanceModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"balance"]];
            for (BalanceModel *m11 in m1) {
                BalanceCoinsModel *mm = [[BalanceCoinsModel alloc] init];
                mm.ID = m11.currency;
                mm.balance = m11.balance;
                mm.locked = m11.locked;
                mm.freezed = m11.freezed;
                mm.usdtRate = m11.usdtRate;
                if ([m11.locked intValue] != 0 || [m11.balance intValue] != 0 || [m11.freezed intValue] != 0) {
                    [self.dataArray addObject:mm];
                }
            }
            if (self.dataArray.count == 0) {
                [self showNotData:kLocalizedString(@"assert_noList")];
            } else {
                [self removeNotData];
            }

            [self.tbView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)hisAction {
    HistoryVC *his = [[HistoryVC alloc] init];
    [self.navigationController pushViewController:his animated:YES];
}


#pragma mark - lazy

- (BaseLabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[BaseLabel alloc] init];
//        _titleLb.colorName = @"login_conentColor";
        _titleLb.textColor = WhiteColor;
        _titleLb.textAlignment = 0;
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.text = kLocalizedString(@"assert_title");
//        _titleLb.backColor = @"";
    }
    return _titleLb;
}

- (BaseButton *)reBtn {
    if (!_reBtn) {
        _reBtn = [[BaseButton alloc] init];
//        _reBtn.colorName = @"login_loginBtnCor";
        [_reBtn setImage:[UIImage imageNamed:@"assert_history"] forState:UIControlStateNormal];
//        _reBtn.backColorName = @"login_registerBackground";
        _reBtn.titleLabel.font = TwelveFontSize;
        [_reBtn addTarget:self action:@selector(hisAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reBtn;
}

- (BaseLabel *)assertNumLb {
    if (!_assertNumLb) {
        _assertNumLb = [[BaseLabel alloc] init];
//        _assertNumLb.colorName = @"login_conentColor";
        _assertNumLb.textColor = WhiteColor;
        _assertNumLb.textAlignment = 0;
        _assertNumLb.font = BOLDSYSTEMFONT(35);
        _assertNumLb.text = @"0.00000000 BTC";
        _assertNumLb.adjustsFontSizeToFitWidth = YES;
    }
    return _assertNumLb;
}

- (BaseLabel *)cnyLb {
    if (!_cnyLb) {
        _cnyLb = [[BaseLabel alloc] init];
//        _cnyLb.colorName = @"login_conentColor";
        _cnyLb.textColor = WhiteColor;
        _cnyLb.textAlignment = 0;
        _cnyLb.font = FifteenFontSize;
    }
    return _cnyLb;
}


- (BaseButton *)depositBtn {
    if (!_depositBtn) {
        _depositBtn = [[BaseButton alloc] init];
        _depositBtn.colorName = @"login_loginBtnCor";
        [_depositBtn setImage:[UIImage imageNamed:@"assert_deposit"] forState:UIControlStateNormal];
        [_depositBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
//        [_depositBtn setTitle:kLocalizedString(@"login_loginForget") forState:UIControlStateNormal];
        [_depositBtn addTarget:self action:@selector(deposit:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _depositBtn;
}

- (void)deposit:(UIButton *)sender {
    DepositVC *deposit = [[DepositVC alloc] init];
    deposit.modelName = @"USDT";
    [self.navigationController pushViewController:deposit animated:YES];
}

- (BaseButton *)withDrawBtn {
    if (!_withDrawBtn) {
        _withDrawBtn = [[BaseButton alloc] init];
        _withDrawBtn.colorName = @"login_loginBtnCor";
        [_withDrawBtn setImage:[UIImage imageNamed:@"assert_withdraw"] forState:UIControlStateNormal];
        [_withDrawBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
//        [_withDrawBtn setTitle:kLocalizedString(@"login_loginForget") forState:UIControlStateNormal];
        [_withDrawBtn addTarget:self action:@selector(withdraw:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _withDrawBtn;
}

- (void)withdraw:(UIButton *)sender {
    WithDrawVC *withDraw = [[WithDrawVC alloc] init];
    withDraw.coinName = @"USDT";
    [self.navigationController pushViewController:withDraw animated:YES];
}

- (BaseView *)toolView {
    if (!_toolView) {
        _toolView = [[BaseView alloc] init];
        _toolView.backgroundColor = DefaultBackGroundColor;
    }
    return _toolView;
}

- (BaseButton *)hidenBtn {
    if (!_hidenBtn) {
        _hidenBtn = [[BaseButton alloc] init];
//        _hidenBtn.colorName = @"login_loginBtnCor";
        [_hidenBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
        [_hidenBtn setTitle:kLocalizedString(@"assert_hide") forState:UIControlStateNormal];
        [_hidenBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_hidenBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_hidenBtn addTarget:self action:@selector(hidenAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _hidenBtn;
}


- (UITextField *)searchField {
    if (!_searchField) {
        _searchField = [[UITextField alloc] init];
        _searchField.font = ThirteenFontSize;
        _searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchField.delegate = self;
        _searchField.borderStyle = UITextBorderStyleNone;
        _searchField.returnKeyType = UIReturnKeySearch;
        _searchField.textAlignment = 0;
        _searchField.cornerRadius = 5;
        _searchField.borderColor = ThemeGrayColor;
        _searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kLocalizedString(@"assert_search") attributes:@{NSForegroundColorAttributeName:ThemeGrayColor}];
        _searchField.textColor = ThemeGrayColor;
    }
    return _searchField;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //开始编辑时触发，文本字段将成为first responder
    if (self.dataArray.count) {
        [self.dataArray removeAllObjects];
    }
    
}


- (UITableView *)tbView {
    if (!_tbView) {
        _tbView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navBar.frame), DScreenW, DScreenH - kNavAndTabHeight) style:UITableViewStylePlain];
        _tbView.dataSource = self;
        _tbView.delegate = self;
        _tbView.backgroundColor = ThemeTableColor;
        _tbView.rowHeight = 100;
        [self hideOtherCellLine:_tbView];
    }
    return _tbView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
@end


