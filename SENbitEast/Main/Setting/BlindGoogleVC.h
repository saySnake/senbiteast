//
//  BlindGoogleVC.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/15.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    BlindGoogleType, // 绑定
    BlindGoogleModify //修改
} BlindGoogleTpe;

@interface BlindGoogleVC : BaseViewController

@property (nonatomic ,assign) BlindGoogleTpe type;
@end

NS_ASSUME_NONNULL_END
