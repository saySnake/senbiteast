//
//  TradeViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/27.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseViewController.h"
@class MoudleTrade;

NS_ASSUME_NONNULL_BEGIN

@interface TradeViewController : BaseViewController
/// 外部接口
@property (nonatomic, strong) MoudleTrade *interface;

@end

NS_ASSUME_NONNULL_END
