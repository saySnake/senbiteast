//
//  AboutVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AboutVC.h"
#import "AboutCell.h"

@interface AboutVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView* contentTableView;

/** titleArray **/
@property (nonatomic ,strong) NSMutableArray *titleArray;

@end

@implementation AboutVC



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ThemeDarkBlack;
    [self setNavBarTitle:kLocalizedString(@"about_native")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    self.navBar.backgroundColor = WhiteColor;
    [self setUpView];
}


- (void)setUpView {
    [self.view addSubview:self.contentTableView];
    [self.contentTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view);
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AboutCell *cell = [AboutCell cellWithTableView:tableView];
    cell.leftLb.text = self.titleArray[indexPath.row];
    return cell;
}


- (UITableView *)contentTableView {
    if (!_contentTableView) {
        _contentTableView        = [[UITableView alloc]initWithFrame:CGRectZero
                                                                           style:UITableViewStylePlain];
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.dataSource          = self;
        _contentTableView.delegate            = self;
        _contentTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.separatorStyle      = UITableViewCellSeparatorStyleSingleLine;
        _contentTableView.tableFooterView = [UIView new];
    }
    return _contentTableView;
}

- (NSMutableArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"about_privacy"),kLocalizedString(@"about_protocol"),kLocalizedString(@"about_contract"), nil];
    }
    return _titleArray;
}


@end
