//
//  HeadView.h
//  IFMShareDemo
//
//  Created by 张玮 on 2020/3/6.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HeadView;
@protocol HeadViewDelegate <NSObject>

@required
- (void)HeadView:(HeadView *)headview seletedImg:(NSString *)img;
@end

@interface HeadView : UIView <UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) NSMutableArray *collectArray;
@property (nonatomic ,assign) id<HeadViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
