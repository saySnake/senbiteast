//
//  SelectedCoinView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/20.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN


@protocol SelectedCoinViewDelegate <NSObject>

- (void)hideMenuView;


@end



@interface SelectedCoinView : BaseView

- (void)firstAppear;
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic ,weak)id <SelectedCoinViewDelegate> customDelegate;

@end

NS_ASSUME_NONNULL_END
