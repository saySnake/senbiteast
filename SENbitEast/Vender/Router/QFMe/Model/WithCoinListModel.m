//
//  WithCoinListModel.m
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "WithCoinListModel.h"

@implementation WithCoinListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        self.ID = dic[@"_id"];
        self.able2charge = [dic[@"able2charge"] boolValue];
        self.able2withdraw = [dic[@"able2withdraw"] boolValue];
        self.coinName = dic[@"id"];
        self.chargeConfirmLimit = dic[@"chargeConfirmLimit"];
        self.coinIcon = dic[@"coinIcon"];
        self.collectCoin = dic[@"collectCoin"];
        self.enFullName = dic[@"enFullName"];
                self.addressURL = dic[@"addressURL"];
        self.txURL = dic[@"txURL"];
        self.enName = dic[@"enName"];
        self.isDestinationTagRequired = [dic[@"isDestinationTagRequired"] boolValue];
        self.nwFeeRatio = [dic[@"nwFeeRatio"] stringValue] ;
        self.nwDailyMaxAmount = [dic[@"nwDailyMaxAmount"] stringValue];
        self.nwMinFee = [dic[@"nwMinFee"] stringValue];
        self.nwMaxAmount = [dic[@"nwMaxAmount"] stringValue];
        self.nwMinAmount = [dic[@"nwMinAmount"] stringValue];
        self.memo = dic[@"memo"];
    }
    return self;
}


- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
     if ([property.name isEqualToString:@"balance"]) {
        if (oldValue) {
            NSLog(@"%@",oldValue);
            return oldValue;
        }
    }
    return oldValue;
}


@end

