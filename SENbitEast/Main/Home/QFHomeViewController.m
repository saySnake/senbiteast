//
//  QFHomeViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/29.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "QFHomeViewController.h"
#import "BannerModel.h"
#import "CoinSignle.h"

#import "QFRouter.h"
#import "QFMoudleProtocol.h"
#import "LeftMenuView.h"
#import "MenuView.h"
#import "SettingVC.h"
#import "AccountVC.h"
#import "AboutVC.h"
#import "RealVC.h"
//跑马灯
#import <JhtMarquee/JhtVerticalMarquee.h>
#import <JhtMarquee/JhtHorizontalMarquee.h>
#import "CycleImageView.h"
#import "SDCycleScrollView.h"

#import "WMTableViewController.h"
#import "OneViewTableTableViewController.h"
#import "NoticeListVC.h"
#import "HomeCoinPairModel.h"
#import "HomeOrderListVC.h"
#import "HomeBannerModel.h"
#import "WebViewController.h"


//static CGFloat const headViewHeight = 620;
static CGFloat const headViewHeight = 420;
static CGFloat const kWMMenuViewHeight = 44.0;


@interface QFHomeViewController ()<HomeMenuViewDelegate,SDCycleScrollViewDelegate,
UIGestureRecognizerDelegate,CycleImageViewDelegate> {
    // 纵向 跑马灯
    JhtVerticalMarquee *_verticalMarquee;
    // 是否暂停了纵向 跑马灯
    BOOL _isPauseV;
    NSArray *_UIModels;
}

@property (strong, nonatomic) XXWebQuoteModel *webModel;
@property (nonatomic ,strong) NSMutableArray *dataArray;
@property (nonatomic ,strong) UIView *redView;
@property (nonatomic, strong) NSArray *musicCategories;

@property (nonatomic ,strong) MenuView      *menu;
@property (nonatomic ,strong) UIButton *leftBtn;
@property (nonatomic ,strong) UIView *navBar;

/** tempurl 轮播图地址url **/
@property (nonatomic ,strong) NSMutableArray *tempurl;

/** headView **/
@property (nonatomic ,strong) UIView *headView;
@property (nonatomic ,strong) SDCycleScrollView *sdcscroll;
@property (nonatomic ,strong) UIImageView *cirImg;
@property (nonatomic ,strong) UIButton *moreNotice;
@property (nonatomic ,strong) CycleImageView *cycleImageView;
@property (nonatomic ,strong) UIButton *otcView;
@property (nonatomic ,strong) UILabel *titl;
@property (nonatomic ,strong) UILabel *cont;

@property (nonatomic ,strong) FSCustomButton *invotedBtn;
@property (nonatomic ,strong) FSCustomButton *contractBtn;
@property (nonatomic ,strong) FSCustomButton *loseBtn;
@property (nonatomic ,strong) FSCustomButton *dropBtn;


//@property (nonatomic ,strong) UIButton *tesBtn;
@property (nonatomic ,strong) NSMutableArray *totalVC;


@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *urlArray;

@property (nonatomic ,strong) UIView *ciriView;
@property (nonatomic ,strong) UILabel *rightLb;
@end

@implementation QFHomeViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_isPauseV) {
        [_verticalMarquee marqueeOfSettingWithState:MarqueeContinue_V];
    }
}

- (NSMutableArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [[NSMutableArray alloc] init];
    }
    return _titleArray;
}

- (NSMutableArray *)urlArray {
    if (!_urlArray) {
        _urlArray = [[NSMutableArray alloc] init];
    }
    return _urlArray;
}


- (void)dealloc {
    [IWNotificationCenter removeObserver:self name:KLoginNoti object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [KSystem statusBarSetUpDarkColor];
}

- (void)setUpNoti {
    [IWNotificationCenter addObserver:self selector:@selector(Login) name:KLoginNoti object:nil];
}

- (void)Login {
    LoginViewController *log = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:log];
    nav.navigationBarHidden = YES;
    log.finishBlock = ^{
        [IWNotificationCenter postNotificationName:KLoginNoti object:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
    };
    [self presentViewController:nav animated:NO completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self listBanner];
    [self initAppreaence];
    [self setUpNoti];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];

    [self webSocketHomeDatae];
    [self listAnnounce];
}

- (void)changeLanguage {
    _titl.text = kLocalizedString(@"home_otcTrade");
    _cont.text = kLocalizedString(@"home_otcSupport");
    [self webSocketHomeDatae];
    [self listAnnounce];
    [self listBanner];
    ABCellModel *model1 = [ABCellModel ABImageName:@"home_yue" text:kLocalizedString(@"HomeInvoted") aClass:@"OfficeGroupVC"];
    ABCellModel *model2 = [ABCellModel ABImageName:@"home_heyue" text:kLocalizedString(@"HomeHeyue") aClass:@"TeacherVC"];
    ABCellModel *model3 = [ABCellModel ABImageName:@"home_san" text:kLocalizedString(@"HomeGain") aClass:@"BenefitVC"];
    ABCellModel *model4 = [ABCellModel ABImageName:@"home_drop" text:kLocalizedString(@"HomeDrop") aClass:@"BenefitVC"];
    _UIModels = @[model1,model2,model3,model4];

    [self.headView addSubview:[self menuBtnView:CGRectMake(0,CGRectGetMaxY(self.otcView.frame) + 10.5,DScreenW ,90) uiModels:_UIModels]];
}

- (void)listAnnounce {
    [SBNetworTool getWithUrl:EastArtcles params:@{} isReadCache:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            NSArray<HomeBannerModel *> *arr = [HomeBannerModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            if (arr.count != 0) {
                [self.titleArray removeAllObjects];
                [self.urlArray removeAllObjects];
                for (HomeBannerModel *banner in arr) {
                    [self.titleArray addObject:banner.title];
                    [self.urlArray addObject:banner.url];
                }
                self.verticalMarquee.sourceArray = self.titleArray;
                // 开始滚动
                [self.verticalMarquee marqueeOfSettingWithState:MarqueeStart_V];
            }

        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error, id  _Nonnull responseObject) {

    }];
}

- (void)webSocketHomeDatae {
    [KQuoteSocket cancelWebSocketSubscribeWithWebSocketModel:nil lineType:@"home-coins"];
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        NSMutableArray *arr = [HomeCoinPairModel mj_objectArrayWithKeyValuesArray:data];
        [weakSelf reciveHeaderInfo:arr];
    };
    NSDictionary *dic = @{};
    self.webModel.params = [NSMutableDictionary dictionary];
    self.webModel.params[@"topic"] = @"home-coins";
    self.webModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"home-coins"];
}

- (void)reciveHeaderInfo:(NSArray *)array {
    NSMutableArray *totoalArr = [NSMutableArray array];
    for (HomeCoinPairModel *model in array) {
        [totoalArr addObject:[NSString stringWithFormat:@"%@",model.coinName]];
    }
    [totoalArr insertObject:kLocalizedString(@"home_selfSelected") atIndex:0];
//    [totoalArr addObject:@"开放区"];

    self.musicCategories = totoalArr;
    [self.totalVC removeAllObjects];
    for (int i = 0; i < totoalArr.count; i++) {
        OneViewTableTableViewController *vc = [[OneViewTableTableViewController alloc] init];
        [self.totalVC addObject:vc];
    }
    self.titleFontName = @"ArialHebrew-Bold";
    self.menuViewStyle = WMMenuViewStyleLine;
    self.titleColorSelected = ThemeGreenColor;
    self.menuView.backgroundColor = WhiteColor;
    self.scrollView.backgroundColor = WhiteColor;
    self.menuView.scrollView.height = 50;
    self.titleSizeNormal = 16;//未选中字体大小
    self.titleSizeSelected = 20;
    self.menuView.lineColor = ThemeGreenColor;
    self.titleColorNormal = HEXCOLOR(0x6B6B6B);
    if ([KDetail.homeIndex intValue] > totoalArr.count) {
        KDetail.homeIndex = @"1";
    }
    self.selectIndex = [KDetail.homeIndex intValue];
    self.menuItemWidth = 80; //[UIScreen mainScreen].bounds.size.width / self.musicCategories.count;
    [self reloadData];
}


- (instancetype)init {
    if (self = [super init]) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDarkContent;
        self.menuViewHeight = kWMMenuViewHeight;
        self.maximumHeaderViewHeight = headViewHeight + 6;
        self.minimumHeaderViewHeight = 64;
    }
    return self;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat headerViewHeight = 360;
    CGFloat headerViewX = kNavBarAndStatusBarHeight;
    UIScrollView *scrollView = (UIScrollView *)self.view;
    if (scrollView.contentOffset.y < 0) {
        headerViewX = kNavBarAndStatusBarHeight;
        headerViewHeight -= headerViewX;
    }
    self.redView.frame = CGRectMake(0, headerViewX, CGRectGetWidth(self.view.bounds), headerViewHeight);
}


- (void)listBanner {
    [SBNetworTool getWithUrl:EastBanner params:@{@"platform":@"app"}
                 isReadCache:NO
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            NSArray * arr = responseObject[@"data"];
            self.dataArray = [BannerModel mj_objectArrayWithKeyValuesArray:arr];
            self.dataArray = (NSMutableArray *)[[self.dataArray reverseObjectEnumerator] allObjects];
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.tempurl removeAllObjects];
                for (BannerModel *banner in self.dataArray) {
                    NSString *url = [NSString stringWithFormat:@"%@%@",EastFiels,banner.img];
//                    NSString *url = @"http://1812.img.pp.sohu.com.cn/images/blog/2009/11/18/18/8/125b6560a6ag214.jpg";
                    [self.tempurl addObject:url];
                }
                _sdcscroll.imageURLStringsGroup = self.tempurl;
//            });
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error, id  _Nonnull responseObject) {

    }];
}

#pragma mark - Datasource & Delegate
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    self.titleFontName = @"ArialRoundedMTBold";
    return self.musicCategories.count;
}

- (void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    NSDictionary *dic = @{@"coinName":info[@"title"]};
    [CoinSignle shareInstance].coin = info[@"title"];
    [NotificationManager homePostSwitchTradeSymbolNotification:dic];
    NSLog(@"_________________%@",info[@"title"]);
}

- (void)pageController:(WMPageController *)pageController willEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    NSDictionary *dic = @{@"coinName":info[@"title"]};
    [CoinSignle shareInstance].coin = info[@"title"];
    [NotificationManager homePostSwitchTradeSymbolNotification:dic];
}



- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    OneViewTableTableViewController *vc = self.totalVC[index];
    KDetail.homeIndex = [NSString stringWithFormat:@"%zd", index];
//    OneViewTableTableViewController *vc = [[OneViewTableTableViewController alloc] init];
    return vc;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.musicCategories[index];
}

#pragma mark - lazy
- (NSArray *)musicCategories {
    if (!_musicCategories) {
        _musicCategories = [[NSArray alloc] init];;
    }
    return _musicCategories;
}


#pragma mark - InitAppreaence
- (void)initAppreaence {
//    [self setNavBarTitle:@"首页"]; //kLocalizedString(@"search")];
//    [self setNavBarLeftTitle:@"点击"];
//    self.navBar.backgroundColor = [UIColor yellowColor];
    self.navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, kNavBarAndStatusBarHeight)];
    self.navBar.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.navBar];
    [self.navBar addSubview:self.leftBtn];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.centerY.mas_equalTo(self.navBar).offset(13);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(22);
    }];
    [self.navBar addSubview:self.rightLb];
    [self.rightLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.navBar).offset(13);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(33);
    }];
    self.view.backgroundColor = [UIColor whiteColor];

    
    LeftMenuView *demo = [[LeftMenuView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width * 0.8, [[UIScreen mainScreen] bounds].size.height)];
    demo.customDelegate = self;
    
    MenuView *menu = [MenuView MenuViewWithDependencyView:self.view MenuView:demo isShowCoverView:YES];
//    MenuView *menu = [[MenuView alloc]initWithDependencyView:self.view MenuView:demo isShowCoverView:YES];
    self.menu = menu;
    
    UIView *redView = [[UIView alloc] initWithFrame:CGRectMake(0, kNavBarAndStatusBarHeight, [UIScreen mainScreen].bounds.size.width, headViewHeight)];
    self.redView.backgroundColor = WhiteColor;
    self.redView = redView;
    [self.view addSubview:self.redView];
    [self.redView addSubview:self.headView];
}

#pragma mark - Event Response


- (void)LeftMenuViewClick:(NSInteger)tag {
    [self.menu hidenWithAnimation];
//    NSString *tagstr = [NSString stringWithFormat:@"%d",tag];
//    [[[UIAlertView alloc] initWithTitle:@"提示" message:tagstr delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
    if (tag == 0) {
        NSLog(@"订单列表");
        UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
        if (user.login == NO) {
            LoginViewController *login = [[LoginViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
            nav.modalPresentationStyle = UIModalPresentationFullScreen;
            nav.navigationBarHidden = YES;
            login.finishBlock = ^{
                [self dismissViewControllerAnimated:NO completion:nil];
                HomeOrderListVC *list = [[HomeOrderListVC alloc] init];
                [self.navigationController pushViewController:list animated:YES];
            };
            login.cancleBlock = ^{

            };
            [self presentViewController:nav animated:NO completion:nil];
        } else {
            HomeOrderListVC *list = [[HomeOrderListVC alloc] init];
            [self.navigationController pushViewController:list animated:YES];
        }
    }

    if (tag == 1) {
        NSLog(@"关于我们");
        AboutVC *about = [[AboutVC alloc] init];
        [self.navigationController pushViewController:about animated:YES];
    }
}

- (void)setPushVC {
    [self.menu hidenWithAnimation];
    SettingVC *setVC = [[SettingVC alloc] init];
    [self.navigationController pushViewController:setVC animated:YES];
}

- (void)accountPushVC {
    NSLog(@"账户中心");
    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
    if (user.login == NO) {
        LoginViewController *login = [[LoginViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        nav.navigationBarHidden = YES;
        login.finishBlock = ^{
            [self dismissViewControllerAnimated:NO completion:nil];
            [self.menu hidenWithAnimation];
            AccountVC *account = [[AccountVC alloc] init];
            [self.navigationController pushViewController:account animated:YES];
        };
        [self presentViewController:nav animated:NO completion:nil];
    } else {
        [self.menu hidenWithAnimation];
        AccountVC *account = [[AccountVC alloc] init];
        [self.navigationController pushViewController:account animated:YES];
    }
}

- (void)realPushVC {
    NSLog(@"实名认证");
    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
    if (user.login == NO) {
        LoginViewController *login = [[LoginViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        nav.navigationBarHidden = YES;
        login.finishBlock = ^{
            [self dismissViewControllerAnimated:NO completion:nil];
            [self.menu hidenWithAnimation];
            RealVC *real = [[RealVC alloc] init];
            [self.navigationController pushViewController:real animated:YES];
        };
        [self presentViewController:nav animated:NO completion:nil];
    } else {
        [self.menu hidenWithAnimation];
        RealVC *real = [[RealVC alloc] init];
        [self.navigationController pushViewController:real animated:YES];
    }
}

- (void)invotedPushVC {
    NSLog(@"邀请好友");
    [self.menu hidenWithAnimation];
}

- (void)navBarButtonClickAction {
    [self.menu show];
}


#pragma mark - Getters

- (UIButton *)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [[UIButton alloc] init];
        [_leftBtn addTarget:self action:@selector(navBarButtonClickAction) forControlEvents:UIControlEventTouchUpInside];
        [_leftBtn setImage:[UIImage imageNamed:@"home_set"] forState:UIControlStateNormal];
    }
    return _leftBtn;
}

- (UIView *)headView {
    if (!_headView) {
//        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, 540)];
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, 340)];
        _headView.backgroundColor = HEXCOLOR(0xF7F6FB);
        [_headView addSubview:self.sdcscroll];
        [_headView addSubview:self.ciriView];
        [_ciriView addSubview:self.cirImg];
        [_headView addSubview:self.moreNotice];
        [_headView addSubview:self.verticalMarquee];
        [_headView addSubview:self.cycleImageView];
//        [_headView addSubview:self.otcView];
        
        ABCellModel *model1 = [ABCellModel ABImageName:@"home_yue" text:kLocalizedString(@"HomeInvoted") aClass:@"OfficeGroupVC"];
        ABCellModel *model2 = [ABCellModel ABImageName:@"home_heyue" text:kLocalizedString(@"HomeHeyue") aClass:@"TeacherVC"];
        ABCellModel *model3 = [ABCellModel ABImageName:@"home_san" text:kLocalizedString(@"HomeGain") aClass:@"BenefitVC"];
        ABCellModel *model4 = [ABCellModel ABImageName:@"home_drop" text:kLocalizedString(@"HomeDrop") aClass:@"BenefitVC"];
        _UIModels = @[model1,model2,model3,model4];

//        [self.headView addSubview:[self menuBtnView:CGRectMake(0,CGRectGetMaxY(self.otcView.frame) + 10.5,DScreenW ,90) uiModels:_UIModels]];
    }
    return _headView;
}

- (SDCycleScrollView *)sdcscroll {
    if (_sdcscroll == nil) {
        [self.tempurl removeAllObjects];
        _sdcscroll = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(10, 0, DScreenW - 20, 180) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
        _sdcscroll.cornerRadius = 5;
        _sdcscroll.delegate = self;
        if (self.tempurl.count == 1) {
            _sdcscroll.infiniteLoop = NO;
        }
        _sdcscroll.autoScrollTimeInterval = 6;
        _sdcscroll.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _sdcscroll.pageDotColor = HexRGBA(0x4A4A4A, 0.5);
    }
    return _sdcscroll;
}

- (JhtVerticalMarquee *)verticalMarquee {
    if (!_verticalMarquee) {
        _verticalMarquee = [[JhtVerticalMarquee alloc]  initWithFrame:CGRectMake(40, CGRectGetMaxY(self.sdcscroll.frame), DScreenW - 40, 48)];
        _verticalMarquee.tag = 101;
//        _verticalMarquee.isCounterclockwise = YES;
        _verticalMarquee.numberOfLines = 2;
        [_verticalMarquee sizeToFit];
        _verticalMarquee.backgroundColor = WhiteColor;
        _verticalMarquee.textColor = MainWhiteColor;
        // 添加点击手势
        UITapGestureRecognizer *vtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(marqueeTapGes:)];
        
        [_verticalMarquee addGestureRecognizer:vtap];
    }
    return _verticalMarquee;
}

#pragma mark Get Method
/** 点击 滚动跑马灯 触发方法 */
- (void)marqueeTapGes:(UITapGestureRecognizer *)ges {
    if (ges.view.tag == 100) {
        NSLog(@"点击__水平__滚动的跑马灯啦！！！");
        
    } else if (ges.view.tag == 101) {
        NSLog(@"点击__纵向__滚动的跑马灯_第 %ld 条数据啦！！！", (long)self.verticalMarquee.currentIndex);
    }
    if (self.urlArray.count > 0) {
        NSString *urlStr = self.urlArray[(long)self.verticalMarquee.currentIndex];
        WebViewController *web = [[WebViewController alloc] init];
        web.url = urlStr;
        [self.navigationController pushViewController:web animated:YES];
    }
}


- (CycleImageView *)cycleImageView {
    if (_cycleImageView == nil) {
        self.cycleImageView = [[CycleImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.verticalMarquee.frame)+1, DScreenW, 100)];
        self.cycleImageView.delegate = self;
        self.cycleImageView.movementDirection = MovementDirectionForHorizontally;
        self.cycleImageView.hidePageControl = NO;
        self.cycleImageView.canFingersSliding = YES;
    }
    return _cycleImageView;
};

- (void)singleTapAction:(id)sender {
    NSLog(@"法币洁面");
    [self showToastView:kLocalizedString(@"toastExpect")];
}

- (UIButton *)otcView {
    if (!_otcView) {
        _otcView = [[UIButton alloc] init];
        _otcView.userInteractionEnabled = YES;
//        _otcView.frame = CGRectMake(0, CGRectGetMaxY(self.cycleImageView.frame) + 10.5,DScreenW,87);
        _otcView.frame = CGRectMake(0, CGRectGetMaxY(self.cycleImageView.frame) + 10.5,DScreenW,0);
        _otcView.backgroundColor = WhiteColor;
//        [_otcView addTarget:self action:@selector(singleTapAction:) forControlEvents:UIControlEventTouchUpInside];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
        initWithTarget:self action:@selector(singleTapAction:)];
        tapRecognizer.delegate = self;
        [_otcView addGestureRecognizer:tapRecognizer];
        UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(14.5, 18, 57, 49.5)];
        imgv.image = [UIImage imageNamed:@"home_otc"];
        imgv.contentMode = UIViewContentModeScaleAspectFit;
        imgv.userInteractionEnabled = YES;

        UIImageView *imgvv = [[UIImageView alloc] initWithFrame:CGRectMake(DScreenW - 87, 0, 87, 87)];
        imgvv.image = [UIImage imageNamed:@"home_right"];
        imgvv.contentMode = UIViewContentModeScaleAspectFit;
        imgvv.userInteractionEnabled = YES;
        [_otcView addSubview:imgv];
        [_otcView addSubview:imgvv];

        
        self.titl.frame = CGRectMake(90, 23, 200, 18);
        self.titl.textColor = MainWhiteColor;
        self.titl.textAlignment = 0;
        self.titl.font = [UIFont systemFontOfSize:18];
        self.titl.font = [UIFont boldSystemFontOfSize:18];
        [_otcView addSubview:self.titl];
        
        self.cont.frame = CGRectMake(90, 52, 180, 15);
        self.cont.textColor = PlaceHolderColor;
        self.cont.textAlignment = 0;
        self.cont.adjustsFontSizeToFitWidth = YES;
        self.cont.font = [UIFont systemFontOfSize:16];
        [_otcView addSubview:self.cont];
        
        UIImageView *rig = [[UIImageView alloc] init];
        rig.userInteractionEnabled = YES;
        rig.frame = CGRectMake(DScreenW - 45, 30, 27.5, 27.5);
        rig.image = [UIImage imageNamed:@"home_otcRig"];
        rig.contentMode = UIViewContentModeScaleAspectFit;
        [_otcView addSubview:rig];
    }
    return _otcView;
}

- (UILabel *)titl {
    if (!_titl) {
        _titl = [[UILabel alloc] init];
        _titl.text = kLocalizedString(@"home_otcTrade");
    }
    return _titl;
}

- (UILabel *)cont {
    if (!_cont) {
        _cont = [[UILabel alloc] init];
        _cont.text = kLocalizedString(@"home_otcSupport");
    }
    return _cont;
}

- (NSMutableArray *)tempurl {
    if (!_tempurl) {
        _tempurl = [[NSMutableArray alloc] init];
    }
    return _tempurl;
}

#pragma mark *********** 主菜单 ***********
- (UIView *)menuBtnView:(CGRect)frame uiModels:(NSArray *)models{
    UIView *menuview = [[UIView alloc] initWithFrame:frame];
    menuview.backgroundColor = WhiteColor;
    CGFloat btnW = (menuview.width) / 4;
    CGFloat btnH = 80;//(menuview.height) / 2+ (DiPhone4 ? 20:0);
    for (int i = 0; i < models.count; i++) {
        FSCustomButton *btn = [[FSCustomButton alloc] init];
        btn.titleEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 0);
        btn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        btn.buttonImagePosition = FSCustomButtonImagePositionTop;
        ABCellModel *model = models[i];
        btn.frame = CGRectMake((i%4) * btnW, i/4 * btnH + 3, btnW, btnH);
        btn.clipsToBounds = YES;
        [btn setImage:[UIImage imageNamed:model.imageNamed] forState:UIControlStateNormal];
        [btn setTitle:model.textContent forState:UIControlStateNormal];
        btn.titleLabel.font = BOLDSYSTEMFONT(13);
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        btn.tag = i;
//        [btn addTarget:self action:@selector(menuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
        tap.delegate = self;
        [btn addGestureRecognizer:tap];
        [menuview addSubview:btn];
    }
    return menuview;
}

- (void)tapGesture:(id)sender {
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    WebViewController *web = [[WebViewController alloc] init];
    NSString *lan = kLanguageManager.currentLanguage;
//    if (tapRecognizer.view.tag == 0) {
//
//    }
    [self showToastView:kLocalizedString(@"toastExpect")];
}

- (UIButton *)moreNotice {
    if (!_moreNotice) {
        _moreNotice = [[UIButton alloc] initWithFrame:CGRectMake(DScreenW - 50, CGRectGetMaxY(self.sdcscroll.frame) + 17, 15, 15)];
        _moreNotice.backgroundColor = [UIColor redColor];
        [_moreNotice addTarget:self action:@selector(moreNoticeAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreNotice;
}

- (void)moreNoticeAction {
    NoticeListVC *list = [[NoticeListVC alloc] init];
    [self.navigationController pushViewController:list animated:YES];
}


- (UIImageView *)cirImg {
    if (!_cirImg) {
        _cirImg = [[UIImageView alloc]  initWithFrame:CGRectMake(10, 15 , 15, 15)];
        _cirImg.image = [UIImage imageNamed:@"home_earth"];
        _cirImg.contentMode = UIViewContentModeScaleAspectFit;
        _cirImg.backgroundColor = WhiteColor;
    }
    return _cirImg;
}

- (UIView *)ciriView {
    if (!_ciriView) {
        _ciriView = [[UIView alloc]  initWithFrame:CGRectMake(0, CGRectGetMaxY(self.sdcscroll.frame), 40, 48)];
        _ciriView.backgroundColor = WhiteColor;
    }
    return _ciriView;
}


#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    WebViewController *web = [[WebViewController alloc] init];
//    web.url = self.tempurl[index];
    BannerModel *model = self.dataArray[index];
    web.url = model.link;
    if (web.url == nil) {
        return;
    }
//    web.isShare = YES;
    [self.navigationController pushViewController:web animated:YES];
}


#pragma mark - CycleImageViewDelegate
- (void)didSelectItemAtIndex:(OneTableSockeModel *)model {

    [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
           KDetail.symbolModel = obj;
           KTrade.coinTradeModel = obj;
           if ([KMarket.favoritesArray containsObject:model.coinName]) {
               KDetail.symbolModel.favorite = YES;
               KTrade.coinTradeModel.favorite = YES;
           }
           NSLog(@"%@",KDetail.symbolModel.symbolId);
           *stop = YES;
         }
   }];

    id <MoudleLine>lineMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleLine)];
    UIViewController *viewController = lineMoudle.interfaceViewController;
    lineMoudle.callbackBlock = ^(id parameter) {
        NSLog(@"return paramter = %@",parameter);
    };
    [self.navigationController pushViewController:viewController animated:YES];
}




- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray  alloc] init];
    }
    return _dataArray;
}

- (NSMutableArray *)totalVC {
    if (!_totalVC) {
        _totalVC = [[NSMutableArray alloc] init];
    }
    return _totalVC;
}

- (UILabel *)rightLb {
    if (!_rightLb) {
        _rightLb = [[UILabel alloc] init];
        _rightLb.textAlignment = 2;
        _rightLb.font = BOLDSYSTEMFONT(20);
        _rightLb.text = @"Hi，欢迎加入Dervix家族";
        _rightLb.textColor = HEXCOLOR(0x333333);
        _rightLb.adjustsFontSizeToFitWidth = YES;
    }
    return _rightLb;
}
@end

@implementation ABCellModel

+ (instancetype)ABImageName:(NSString *)name text:(NSString *)text aClass:(NSString *)aClass{
    ABCellModel *model = [[ABCellModel alloc] init];
    model.imageNamed = name;
    model.textContent = text;
    model.controllerClass = aClass;
    return model;
}

@end
