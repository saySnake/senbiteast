//
//  ModifyVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/14.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "ModifyPwdVC.h"
#import <GT3Captcha/GT3Captcha.h>
#import "AsyncTaskButton.h"
#import "TipsView.h"
#import "BlindPhoneSheet.h"

@interface ModifyPwdVC ()<AsyncTaskCaptchaButtonDelegate,UITextFieldDelegate>
@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contentLb;
@property (nonatomic ,strong) BaseField *neAccountTF;
@property (nonatomic ,strong) BaseField *reNewAccountTF;
@property (nonatomic ,strong) AsyncTaskButton *nextBtn;

@property (nonatomic ,strong) UIButton *scrBtn2;
@property (nonatomic ,strong) UIButton *scrBtn3;

@end

@implementation ModifyPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navBar.backgroundColor = WhiteColor;
//    [self setNavBarTitle:kLocalizedString(@"modify_setTitle")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self configKeyBoardRespond];
    [self setUpView];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak ModifyPwdVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.neAccountTF,weakSelf.reNewAccountTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}

- (void)setUpView {
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.neAccountTF];
    [self.view addSubview:self.reNewAccountTF];
    [self.view addSubview:self.nextBtn];
    [self.view addSubview:self.scrBtn2];
    [self.view addSubview:self.scrBtn3];
    [self make_layout];
}

- (void)make_layout {
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(75);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(40);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(25);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(20);
    }];

    [self.neAccountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(60);
        make.height.mas_equalTo(50);
    }];
    [self.scrBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.neAccountTF.mas_right).offset(-15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.neAccountTF.mas_top).offset(30);
        make.width.mas_equalTo(15);
    }];


    [self.reNewAccountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.neAccountTF);
        make.top.mas_equalTo(self.neAccountTF.mas_bottom).offset(60);
        make.height.mas_equalTo(50);
    }];
    
    [self.scrBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.reNewAccountTF.mas_right).offset(-15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.reNewAccountTF.mas_top).offset(30);
        make.width.mas_equalTo(15);
    }];


    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.reNewAccountTF);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(self.reNewAccountTF.mas_bottom).offset(60);
    }];
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.textAlignment = 0;
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.text = kLocalizedString(@"modify_pwd");
    }
    return _titleLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.text = kLocalizedString(@"modify_24H");
        _contentLb.textAlignment = 0;
        _contentLb.font = ThirteenFontSize;
        _contentLb.textColor = HEXCOLOR(0x666666);
    }
    return _contentLb;
}



- (BaseField *)neAccountTF {
    if (!_neAccountTF) {
        _neAccountTF = [[BaseField alloc] init];
        _neAccountTF.placeholder = kLocalizedString(@"modify_newPw");
        _neAccountTF.placeHolderColor = PlaceHolderColor;
        _neAccountTF.textField.secureTextEntry = YES;
        [_neAccountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _neAccountTF;
}

- (BaseField *)reNewAccountTF {
    if (!_reNewAccountTF) {
        _reNewAccountTF = [[BaseField alloc] init];
        _reNewAccountTF.placeholder = kLocalizedString(@"modify_confirmPw");
        _reNewAccountTF.placeHolderColor = PlaceHolderColor;
        _reNewAccountTF.textField.secureTextEntry = YES;
        [_reNewAccountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _reNewAccountTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.reNewAccountTF.textField.text.length > 0 && self.neAccountTF.textField.text.length > 0) {
        self.nextBtn.backgroundColor = ThemeGreenColor;
        self.nextBtn.userInteractionEnabled = YES;

    } else {
        self.nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.nextBtn.userInteractionEnabled = NO;
    }
}

- (AsyncTaskButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [AsyncTaskButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.cornerRadius = 8;
        _nextBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [_nextBtn setTitle:kLocalizedString(@"modify_confirm") forState:UIControlStateNormal];
        _nextBtn.delegate = self;
        _nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _nextBtn.userInteractionEnabled = NO;
    }
    return _nextBtn;
}


- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {

    
    if (self.neAccountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"modify_newPw")];
        return NO;
    }
    
    if (self.reNewAccountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"modify_pleaseConfirmNew")];
        return NO;
    }
   
    if (![self.reNewAccountTF.textField.text isEqualToString:self.neAccountTF.textField.text]) {
        [self showToastView:kLocalizedString(@"modify_scrNot")];
        return NO;
    }
    
    [TipsView showTipOnKeyWindow:kLocalizedString(@"login_safe")];
    return YES;
}

- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    [self.view endEditing:YES];
   
    if (status == YES) {
        [self requestInfoType];
    }
}

- (void)requestInfoType {
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    BlindPhoneSheet *sheet = [[BlindPhoneSheet alloc] init];
    sheet.type = BlindTypeTradePassword;
    sheet.googleAuth = info.googleAuthEnable;
    sheet.emailAuth = info.emailAuthEnable;
    sheet.phoneAuth = info.phoneAuthEnable;
    sheet.emailTitleName.text = info.email;
    sheet.phoneLabel.text = info.phone;
    sheet.clickBlock = ^(NSString * _Nonnull phoneCode, NSString * _Nonnull emailCode, NSString * _Nonnull googleCode, NSString * _Nonnull phone, NSString * _Nonnull email, NSString * _Nonnull userToken) {
        if (phoneCode.length == 0) {
            phoneCode = @"";
        }
        if (emailCode.length == 0) {
            emailCode = @"";
        }
        if (googleCode.length == 0) {
            googleCode = @"";
        }
        if (email.length == 0) {
            email = @"";
        }
        if (phone.length == 0) {
            phone = @"";
        }
        NSDictionary *dic = @{@"email":info.email,
                              @"emailCode":emailCode,
                              @"phone":info.phone,
                              @"phoneCode":phoneCode,
                              @"googleCode":googleCode,
                              @"type":@"RESET_TRADE_PASSWORD"
        };
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                [self requieAuthToken:responseObject[@"data"][@"authToken"]];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    };
    [sheet showFromView:self.view];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super hideKeyboard];
}

- (void)requieAuthToken:(NSString *)str {
    NSDictionary *dic = @{@"newTradePassword":self.reNewAccountTF.textField.text,
                          @"authToken":str
    };
    [SBNetworTool patchWithUrl:EastReSetTradePwd params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
            info.tradePassword = YES;
            [[CacheManager sharedMnager] saveUserInfo:info];
            [self showToastView:kLocalizedString(@"modify_success")];
            [self delay:1 task:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (UIButton *)scrBtn2 {
    if (!_scrBtn2) {
        _scrBtn2 = [[UIButton alloc] init];
        [_scrBtn2 setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn2 setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn2 addTarget:self action:@selector(scr2:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn2;
}

- (UIButton *)scrBtn3 {
    if (!_scrBtn3) {
        _scrBtn3 = [[UIButton alloc] init];
        [_scrBtn3 setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn3 setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn3 addTarget:self action:@selector(scr3:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn3;
}


- (void)scr2:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.neAccountTF.textField.secureTextEntry = NO;
    } else {
        self.neAccountTF.textField.secureTextEntry = YES;
    }
}

- (void)scr3:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.reNewAccountTF.textField.secureTextEntry = NO;
    } else {
        self.reNewAccountTF.textField.secureTextEntry = YES;
    }
}


@end
