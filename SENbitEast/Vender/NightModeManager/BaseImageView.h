//
//  BaseImageView.h
//  Senbit
//
//  Created by 张玮 on 2019/12/27.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//BaseImageView *backgroundImage = [[BaseImageView alloc] initWithImageName:@"tabbar_background"];

@interface BaseImageView : UIImageView

//图片名称
@property(nonatomic,copy)NSString *imageName;

- (id)initWithImageName:(NSString *)imageName;

- (void)loadThemeImage;
@end

NS_ASSUME_NONNULL_END
