//
//  ResetVC.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/13.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "ResetVC.h"
#import "BaseField.h"
#import "WebViewController.h"
@interface ResetVC ()
/** 取消 **/
@property (nonatomic ,strong) BaseButton *cancelBtn;

/** 重制密码 **/
@property (nonatomic ,strong) BaseLabel *findTitle;

/** conent **/
@property (nonatomic ,strong) BaseLabel *conentText;

/** baseField **/
@property (nonatomic ,strong) BaseField *accountTF;

/** password **/
@property (nonatomic ,strong) BaseField *passwordTF;

/** fscuomBtn **/
@property (nonatomic ,strong) FSCustomButton *checkBtn;

@property (nonatomic ,strong) BaseButton *protocolBtn;
/** confirmBtn **/
@property (nonatomic ,strong) BaseButton *confirmBtn;


@property (nonatomic ,strong) UIButton *seeBtn;
@property (nonatomic ,strong) UIButton *scrBtn;


@end

@implementation ResetVC

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = WhiteColor;
    [self setUpView];
    [self make_Layout];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    [self configKeyBoardRespond];
}
- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak ResetVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.passwordTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}

- (void)changeLanguage {
    [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
    _findTitle.text = kLocalizedString(@"reset_title");
    _conentText.text = kLocalizedString(@"reset_content");
}

- (void)setUpView {
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.findTitle];
    [self.view addSubview:self.conentText];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.seeBtn];

    [self.view addSubview:self.passwordTF];
    [self.view addSubview:self.scrBtn];

    [self.view addSubview:self.checkBtn];
    [self.view addSubview:self.protocolBtn];
    [self.view addSubview:self.confirmBtn];
}

- (void)make_Layout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.findTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.cancelBtn.mas_bottom).offset(75);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(39);
    }];
    
    [self.conentText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.findTitle);
        make.top.mas_equalTo(self.findTitle.mas_bottom).offset(28);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(self.conentText.mas_bottom).offset(45);
    }];
    
    [self.seeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.accountTF.mas_top).offset(30);
    }];
    

    
    
    [self.passwordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountTF);
        make.right.mas_equalTo(self.accountTF);
        make.height.mas_equalTo(self.accountTF);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(20);
    }];
    
    
    [self.scrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.passwordTF.mas_top).offset(30);
    }];
    
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.passwordTF);
        make.top.mas_equalTo(self.passwordTF.mas_bottom).offset(76);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(20);
    }];
    
    [self.protocolBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.checkBtn.mas_right);
        make.top.mas_equalTo(self.checkBtn);
        make.width.mas_equalTo(220);
        make.height.mas_equalTo(self.checkBtn);
    }];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(53);
        make.top.mas_equalTo(self.checkBtn.mas_bottom).offset(24);
    }];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = TwelveFontSize;
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseLabel *)findTitle {
    if (!_findTitle) {
        _findTitle = [[BaseLabel alloc] init];
        _findTitle.colorName = @"find_titleColor";
        _findTitle.font = BOLDSYSTEMFONT(33);
        _findTitle.textAlignment = 0;
        _findTitle.text = kLocalizedString(@"reset_title");
    }
    return _findTitle;
}

- (BaseLabel *)conentText {
    if (!_conentText) {
        _conentText = [[BaseLabel alloc] init];
        _conentText.colorName = @"login_conentColor";
        _conentText.font = FourteenFontSize;
        _conentText.textAlignment = 0;
        _conentText.text = kLocalizedString(@"reset_content");
    }
    return _conentText;
}

- (BaseField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[BaseField alloc] init];
        NSString *str = kLocalizedString(@"toast_writePwd");
        _accountTF.placeholder = str;
        _accountTF.placeHolderColor = PlaceHolderColor;
        _accountTF.textField.secureTextEntry = YES;
        _accountTF.textField.font = FifteenFontSize;
        [_accountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (BaseField *)passwordTF {
    if (!_passwordTF) {
        _passwordTF = [[BaseField alloc] init];
        NSString *str = kLocalizedString(@"toast_writeTwPwd");
        _passwordTF.placeholder = str;
        _passwordTF.placeHolderColor = PlaceHolderColor;
        _passwordTF.textField.secureTextEntry = YES;
        [_passwordTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _passwordTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountTF.textField.text.length >0 && self.passwordTF.textField.text.length > 0) {
        self.confirmBtn.backgroundColor = ThemeGreenColor;
        self.confirmBtn.userInteractionEnabled = YES;
    } else {
        self.confirmBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.confirmBtn.userInteractionEnabled = NO;
    }
}

- (FSCustomButton *)checkBtn {
    if (!_checkBtn) {
        _checkBtn = [[FSCustomButton alloc] init];
        _checkBtn.buttonImagePosition = FSCustomButtonImagePositionLeft;
        [_checkBtn setImage:[UIImage imageNamed:@"check_unselected"] forState:UIControlStateNormal];
        [_checkBtn setImage:[UIImage imageNamed:@"check_selected"] forState:UIControlStateSelected];
        _checkBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _checkBtn.backgroundColor = [UIColor clearColor];
        _checkBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_checkBtn setTitleColor:HEXCOLOR(0x9C9C9C) forState:UIControlStateNormal];
        [_checkBtn setTitleColor:ThemeGreenColor forState:UIControlStateSelected];
        _checkBtn.contentHorizontalAlignment = 1;
        _checkBtn.userInteractionEnabled = YES;
//        [_checkBtn setTitle:kLocalizedString(@"reset_attention") forState:UIControlStateNormal];
        [_checkBtn addTarget:self action:@selector(checkClick:) forControlEvents:UIControlEventTouchUpInside];
        _checkBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _checkBtn;
}

- (BaseButton *)protocolBtn {
    if (!_protocolBtn) {
        _protocolBtn = [[BaseButton alloc] init];
        _protocolBtn.titleLabel.font = TwelveFontSize;
        _protocolBtn.backgroundColor = ClearColor;
        [_protocolBtn addTarget:self action:@selector(protocolAction) forControlEvents:UIControlEventTouchUpInside];
        [_protocolBtn setTitle:kLocalizedString(@"setAccount_delegate") forState:UIControlStateNormal];
        [_protocolBtn setTitleColor:HEXCOLOR(0xC9C9D6) forState:UIControlStateNormal];
//        [_protocolBtn setTitleColor:ThemeGreenColor forState:UIControlStateSelected];
        _protocolBtn.contentHorizontalAlignment = 1;
    }
    return _protocolBtn;
}

- (void)protocolAction {
    WebViewController *web = [[WebViewController alloc] init];
    web.url = @"https://senbit2019.zendesk.com/hc/zh-cn/articles/360033849474";
//    web.navigationController.navigationBarHidden = YES;
//    id object = [weakSelf nextResponder];
//        while (![object isKindOfClass:[UIViewController class]] && object != nil) {
//            object = [object nextResponder];
//        }
//    UIViewController *superController = (UIViewController*)object;
    [self.navigationController pushViewController:web animated:YES];
}



- (BaseButton *)confirmBtn {
    if (!_confirmBtn) {
        _confirmBtn = [[BaseButton alloc] init];
        _confirmBtn.colorName = @"reset_confirmTitle";
        _confirmBtn.backColorName = @"login_loginBtnCor";
        [_confirmBtn setTitle:kLocalizedString(@"reset_confirm") forState:UIControlStateNormal];
        _confirmBtn.titleLabel.font = TwentyFontSize;
        _confirmBtn.cornerRadius = 4;
        [_confirmBtn addTarget:self action:@selector(regiserAction) forControlEvents:UIControlEventTouchUpInside];
        _confirmBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _confirmBtn.userInteractionEnabled = NO;
    }
    return _confirmBtn;
}

- (void)regiserAction {
    if (self.accountTF.textField.text.length == 0||self.passwordTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"toast_newPwdOrOldPwdNull")];
        return;
    }
    if (![self.accountTF.textField.text isEqualToString:self.passwordTF.textField.text]) {
        [self showToastView:kLocalizedString(@"toast_newNotEqual")];
        return;
    }
    if (self.checkBtn.selected == NO) {
        [self showToastView:kLocalizedString(@"reset_readProtocol")];
        return;
    }
    
    
    BOOL judge = [NSString judgePassWordLegal:self.accountTF.textField.text];
    if (judge == NO) {
        [self showToastView:kLocalizedString(@"toast_pwdprotocol")];
        return;
    }
    NSDictionary *dic = @{
        @"authToken":self.authToken,
        @"userToken":self.userToken,
        @"password":self.passwordTF.textField.text};
    [SBNetworTool postWithUrl:EastForgetPassword params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [self showToastView:kLocalizedString(@"toast_modifySuc")];
            [self.navigationController popToRootViewControllerAnimated:YES];

        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (void)checkClick:(UIButton *)btn {
    btn.selected = !btn.selected;//每次点击都改变按钮的状态
    if(btn.selected){
        [self.checkBtn setSelected:YES];
        [self.protocolBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];

    }else{
        [self.checkBtn setSelected:NO];
        [self.protocolBtn setTitleColor:HEXCOLOR(0xC9C9D6) forState:UIControlStateNormal];
    //在此实现打勾时的方法
    }
}



- (UIButton *)seeBtn {
    if (!_seeBtn) {
        _seeBtn = [[UIButton alloc] init];
        [_seeBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_seeBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_seeBtn addTarget:self action:@selector(scr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _seeBtn;
}

- (UIButton *)scrBtn {
    if (!_scrBtn) {
        _scrBtn = [[UIButton alloc] init];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn addTarget:self action:@selector(scrr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn;
}


- (void)scr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.accountTF.textField.secureTextEntry = NO;
    } else {
        self.accountTF.textField.secureTextEntry = YES;

    }
}

- (void)scrr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.passwordTF.textField.secureTextEntry = NO;
    } else {
        self.passwordTF.textField.secureTextEntry = YES;
    }
}



@end
