//
//  AddDrawVC.m
//  Senbit
//
//  Created by 张玮 on 2020/2/23.
//  Copyright © 2020 zhangwei. All rights reserved.
//   添加地址

#import "AddDrawVC.h"
#import "AddressCell.h"
#import "AddressVC.h"
#import "AddressModel.h"

@interface AddDrawVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *addTabelView;
@property (nonatomic ,strong) NSMutableArray *dataArray;
/** 底版视图 */
@property (strong, nonatomic) UIVisualEffectView *lowView;
/** 买入按钮 */
@property (strong, nonatomic) XXButton *buyButton;


@end

@implementation AddDrawVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.dataArray removeAllObjects];
    [self accAddresList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUpView];
    self.navBar.backgroundColor = WhiteColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"withDraw_addAress")];
}

- (void)setNav {
    [self setNavBarTitle:kLocalizedString(@"withdraw_address")];
    [self setLeftBtnImage:@"leftBack"];
}

- (void)accAddresList {
    NSString *url = [NSString stringWithFormat:@"%@%@",EastWithDrawAddress,self.model.coinName.uppercaseString];
    [SBNetworTool getWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            NSArray *arr = responseObject[@"data"];
            if (arr.count == 0) {
                [self showNotData:kLocalizedString(@"withdraw_noAddress")];
            } else {
                [self removeNotData];
            }
            self.dataArray = [AddressModel mj_objectArrayWithKeyValuesArray:arr];
            [self.addTabelView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)setUpView {
    [self.view addSubview:self.addTabelView];
    [self.view addSubview:self.lowView];
    [self.lowView.contentView addSubview:self.buyButton];
    [self make_layout];
}

- (void)make_layout {
    NSInteger mainViewHeight = BH_IS_IPHONE_X ? 83 : 65;
    [self.addTabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.height.mas_equalTo(kScreen_Height - mainViewHeight - kNavBarAndStatusBarHeight);
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *cell = [AddressCell cellWithTableView:tableView];
    KWeakSelf
    cell.deleteBtnBlock = ^{
        [weakSelf.addTabelView setEditing:YES];
    };
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressModel *model = self.dataArray[indexPath.row];
    if ([self.delegate respondsToSelector:@selector(selectAddressModel:)]) {
        [self.delegate selectAddressModel:model];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (nullable UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath API_AVAILABLE(ios(11.0)) API_UNAVAILABLE(tvos) {
    
    UIContextualAction *action = [UIContextualAction contextualActionWithStyle:UIContextualActionStyleNormal title:kLocalizedString(@"his_dele") handler:^(UIContextualAction * _Nonnull action, __kindof UIView * _Nonnull sourceView, void (^ _Nonnull completionHandler)(BOOL)) {
        [self deleteAddress:indexPath];
        completionHandler(true);
    }];
    action.backgroundColor = [UIColor redColor];
    UISwipeActionsConfiguration *config = [UISwipeActionsConfiguration configurationWithActions:@[action]];
    config.performsFirstActionWithFullSwipe = NO;
    return config;
}

- (void)deleteAddress:(NSIndexPath *)indexPath {
    AddressModel *model = self.dataArray[indexPath.row];
    NSString *url = [NSString stringWithFormat:@"%@%@",EastDeleAddaddress,model.address];
    [SBNetworTool deleteWithUrl:url params:@{@"coin":self.model.coinName} success:^(id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self.dataArray removeObjectAtIndex:indexPath.row];
            [self.addTabelView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self accAddresList];
        } else {
            [self showToastView:SuccessMessage];
        }

    } failure:^(id  _Nonnull error) {
        [self showToastView:FailurMessage];
    }];
}

- (UITableView *)addTabelView {
    if (!_addTabelView) {
        _addTabelView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _addTabelView.rowHeight = 110;
        _addTabelView.backgroundColor = WhiteColor;
        _addTabelView.delegate = self;
        _addTabelView.dataSource = self;
        _addTabelView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _addTabelView.separatorColor = HEXCOLOR(0xF7F6FB);
        _addTabelView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 83 : 65)];
        [self hideOtherCellLine:_addTabelView];
    }
    return _addTabelView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}


- (UIVisualEffectView *)lowView {
    if (_lowView == nil) {

        NSInteger mainViewHeight = BH_IS_IPHONE_X ? 83 : 65;
        UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        _lowView = [[UIVisualEffectView alloc] initWithEffect:effect];
        _lowView.contentView.backgroundColor = WhiteColor;
        _lowView.frame = CGRectMake(0, kScreen_Height - mainViewHeight, kScreen_Width, mainViewHeight);

        UIView *upLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 1)];
        upLine.backgroundColor = [UIColor whiteColor];
        [_lowView.contentView addSubview:upLine];
    }
    return _lowView;
}


/** 买入按钮 */
- (XXButton *)buyButton {
    if (_buyButton == nil) {
        KWeakSelf
        NSString *btnTitle = kLocalizedString(@"his_addAddres");
        _buyButton = [XXButton buttonWithFrame:CGRectMake(K375(10), 10, (kScreen_Width - K375(20)), 45) title:btnTitle font:kFontBold18 titleColor:kMainTextColor block:^(UIButton *button) {
            NSLog(@"添加");
            AddressVC *adress = [[AddressVC alloc] init];
            adress.model = weakSelf.model;
            [self.navigationController pushViewController:adress animated:YES];
        }];
        _buyButton.backgroundColor = kGreen100;
        _buyButton.layer.cornerRadius = 3;
        _buyButton.layer.masksToBounds = YES;
    }
    return _buyButton;
}

@end
