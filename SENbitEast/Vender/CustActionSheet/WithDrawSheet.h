//
//  WithDrawSheet.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/9.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ClickBlock)();

@interface WithDrawSheet : UIView

@property (nonatomic ,strong) UILabel *safeTitle;
@property (nonatomic ,strong) UIButton *cancBtn;
@property (nonatomic ,strong) UILabel *line1;

@property (nonatomic ,strong) UILabel *phoneTitle;
@property (nonatomic ,strong) UITextField *phoneField;
@property (nonatomic ,strong) UIButton *phoneSms;
@property (nonatomic ,strong) UILabel *phoneLine;

@property (nonatomic ,strong) UILabel *emailTitle;
@property (nonatomic ,strong) UITextField *emailField;
@property (nonatomic ,strong) UIButton *emailSms;
@property (nonatomic ,strong) UILabel *emailLine;

@property (nonatomic ,strong) UITextField *googeleField;
@property (nonatomic ,strong) UIButton *googleCopy;
@property (nonatomic ,strong) UILabel *googleLine;

@property (nonatomic ,strong) UIButton *confirmBtn;

@property (nonatomic ,copy) ClickBlock block;

- (void)showFromView:(UIView *)view;
+ (instancetype)customActionSheet;
@end

NS_ASSUME_NONNULL_END
