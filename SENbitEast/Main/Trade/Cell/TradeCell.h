//
//  TradeCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXDepthOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TradeCell : UITableViewCell
/** 右价格标签 */
@property (strong, nonatomic) XXLabel *rightPriceLabel;

/** 右成交量 */
@property (strong, nonatomic) XXLabel *rightNumberLabel;

/** 右下视图 */
@property (strong, nonatomic) UIView *rightLowView;

/** 右上视图 */
@property (strong, nonatomic) UIView *rightTopView;

/** 平均值 */
@property (assign, nonatomic) CGFloat ordersAverage;

/** 数据模型 */
@property (strong, nonatomic, nullable) XXDepthOrderModel *model;

/** 平均值 */
@property (assign, nonatomic) CGFloat average;

@property (assign, nonatomic) BOOL isAmount;


- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
