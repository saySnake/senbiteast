//
//  SearchCoinCell.h
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoinModel.h"
#import "WithCoinListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SearchCoinCell : UITableViewCell
/** 币种名字 **/
@property (nonatomic ,strong) UILabel *coinName;

/**  不可充提**/
@property (nonatomic ,strong) UILabel *anableLb;

@property (nonatomic ,assign) BOOL isWithdraw;

+ (instancetype)cellWithTableView:(UITableView *)tableView;



@property (nonatomic ,strong) CoinModel *frendModel;
@property (nonatomic ,strong) WithCoinListModel *withDrawModel;
@end

NS_ASSUME_NONNULL_END
