//
//  UserInfo.h
//  Senbit
//
//  Created by 张玮 on 2019/12/25.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "OTCCoinModel.h"
//#import "coinPairModel.h"
NS_ASSUME_NONNULL_BEGIN
@class Banners;


@interface UserInfo : NSObject<NSCoding>

@property (nonatomic ,copy) NSString *loginStatus;

@property (nonatomic, strong) Banners *banners;

@property (nonatomic, copy) NSArray *annonces;



#pragma mark - //土耳其 使用


@property (nonatomic, copy) NSString *geetest_challenge;
@property (nonatomic, copy) NSString *geetest_seccode;
@property (nonatomic, copy) NSString *geetest_validate;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, assign) BOOL login;

//是否设置资金密码
@property (nonatomic, assign) BOOL tradePassword;
//实名认证
@property (nonatomic, assign) BOOL realName;
//true 认证  no 没认证 国内的
@property (nonatomic  ,assign) BOOL strictFace;
//姓名
@property (nonatomic ,copy) NSString *name;
//uid
@property (nonatomic ,copy) NSString *uid;
//inviteCode
@property (nonatomic ,copy) NSString *inviteCode;
//手机号
@property (nonatomic ,copy) NSString *phone;
//邮箱
@property (nonatomic ,copy) NSString *email;
//高级实名认证
@property (nonatomic ,assign) BOOL strictRealName;
//真实姓名状态
@property (nonatomic ,copy) NSString *strictRealNameStatus;
//ID类型
@property (nonatomic ,copy) NSString *IDType;
//ID号
@property (nonatomic ,copy) NSString *IDNumber;
//googleAuthEnable
@property (nonatomic ,assign) BOOL googleAuthEnable;
//phoneAuthEnable
@property (nonatomic ,assign) BOOL phoneAuthEnable;
//EmailAuthEnable
@property (nonatomic ,assign) BOOL emailAuthEnable;
//lastLoginIP
@property (nonatomic ,copy) NSString *lastLoginIP;
//lastLoginAt
@property (nonatomic ,copy) NSString *lastLoginAt;









//google
@property (nonatomic ,assign) BOOL google;
//阿里账号
@property (nonatomic ,copy) NSString *aliAccount;
//开户银行
@property (nonatomic ,copy) NSString *openBankName;
//开户支行
@property (nonatomic ,copy) NSString *openBrankBankName;
//开户银行账号
@property (nonatomic ,copy) NSString *openBankNameNum;
//微信账号
@property (nonatomic ,copy) NSString *weChatAccount;
//PayPal账号
@property (nonatomic ,copy) NSString *payPalAccount;
//PayPal名字
@property (nonatomic ,copy) NSString *payPalName;
//西联汇款账号
@property (nonatomic ,copy) NSString *westAccount;
//西联汇款信息
@property (nonatomic ,copy) NSString *westInfomation;
//neteller
@property (nonatomic ,copy) NSString *neteAccount;
//neteller名字
@property (nonatomic ,copy) NSString *neteName;
//是否使用sgt支付手续费
@property (nonatomic ,assign) BOOL usePTB;
//国籍信息
@property (nonatomic ,copy) NSString *nationality;
//身份证类型
@property (nonatomic ,copy) NSString *cardType;
//身份证号
@property (nonatomic ,copy) NSString *cardNum;
//cookie
@property (nonatomic ,copy) NSString *cookie;
//工会id
@property (nonatomic ,copy) NSString *guildId;
//白名单
@property (nonatomic ,assign) BOOL onWhiteList;

//coinPair
//@property (nonatomic ,copy) NSArray<coinPairModel *> *coinPair;

- (instancetype)initWithJSON:(NSDictionary *)dict;

- (instancetype)initWithSearchJSON:(NSDictionary *)dict;

- (instancetype)initBanner:(NSMutableArray *)array;

- (instancetype)initAnnounce:(NSMutableArray *)array;

@end

#pragma mark - Banner

typedef NS_ENUM(NSInteger,SenlinkBannerType) {
    SenlinkBannerTypeOne        = 10,
    SenlinkBannerTypeTwo        = 12,
    SenlinkBannerTypeThr        = 18,
    SenlinkBannerTypeFor        = 19,
    SenlinkBannerTypeFiv        = 20,
    SenlinkBannerTypeSix        = 22,
};

@interface Banner : NSObject<NSCoding>

@property (nonatomic, copy) NSString *banner_url;
@property (nonatomic, copy) NSString *target;
//anitoTitle
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *target_url;
@property (nonatomic, copy) NSString *ann_url;
@property (nonatomic) SenlinkBannerType type;

@end

@interface Banners : NSObject<NSCoding>

@property (nonatomic, copy) NSArray <Banner *> *homeBanners;
@property (nonatomic, copy) NSArray <Banner *> *anotiBanners;
@end

NS_ASSUME_NONNULL_END
