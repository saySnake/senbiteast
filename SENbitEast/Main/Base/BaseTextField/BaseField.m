//
//  BaseField.m
//  BaseField
//
//  Created by 张玮 on 2020/8/10.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseField.h"

#define YJJ_Color(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
const static CGFloat margin = 10.0;
const static CGFloat tableViewH = 100.0;
const static CGFloat animateDuration = 0.5;

@interface BaseField ()<UITextFieldDelegate>

/** 上浮的占位文本 */
@property (strong, nonatomic) UILabel *placeHolderLabel;
/** 字数限制文本 */
@property (strong, nonatomic) UILabel *textLengthLabel;
/** 底部线条 */
@property (strong, nonatomic) UIView *bottomLine;
/** 错误提示文本 */
@property (strong, nonatomic) UILabel *errorLabel;

@end

@implementation BaseField

- (instancetype)init {
    if (self = [super init]) {
        self.userInteractionEnabled = YES;
        [self setUpViews];
        [self make_Layout];
    }
    return self;
}

- (void)setUpViews {
    self.maxLength = 10000;
    self.textField.delegate = self;
    self.textColor = ThemeTextColor;
    self.textLengthLabelColor = ThemeTextColor;
    self.textLengthLabel.hidden = YES;
    self.placeHolderLabel.hidden = YES;
    self.errorLabel.alpha = 0;
    self.placeHolderLabelColor = PlaceHolderColor;
    self.lineDefaultColor = LineColor;
    self.lineSelectedColor = ThemeGreenColor;
    self.lineWarningColor = ThemeRedColor;
    self.showHistoryList = YES;
    self.placeHolderColor = PlaceHolderColor;
//    NSAttributedString *attrString5 = [[NSAttributedString alloc] initWithString:self.textField.text attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
//    self.textField.attributedPlaceholder = attrString5;
}

- (UILabel *)placeHolderLabel {
    if (!_placeHolderLabel) {
        _placeHolderLabel = [[UILabel alloc] init];
        _placeHolderLabel.textAlignment = 0;
    }
    return _placeHolderLabel;
}

- (UILabel *)textLengthLabel {
    if (!_textLengthLabel) {
        _textLengthLabel = [[UILabel alloc] init];
        _textLengthLabel.textAlignment = 2;
    }
    return _textLengthLabel;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] init];
    }
    return _bottomLine;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.delegate = self;
        _textField.userInteractionEnabled = YES;
        [_textField addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UILabel *)errorLabel {
    if (!_errorLabel) {
        _errorLabel = [[UILabel alloc] init];
    }
    return _errorLabel;
}

- (void)make_Layout {
    [self addSubview:self.placeHolderLabel];
    [self.placeHolderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(18);
    }];
    
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.placeHolderLabel.mas_bottom).offset(5);
        make.right.mas_equalTo(-60);
        make.height.mas_equalTo(30);
    }];
    
    [self addSubview:self.textLengthLabel];
    [self.textLengthLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textField);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(self.textField);
    }];
    
    [self addSubview:self.bottomLine];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(self.textField.mas_bottom).offset(5);
    }];
    [self addSubview:self.errorLabel];
    [self.errorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.bottomLine.mas_bottom).offset(3);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(18);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.userInteractionEnabled = YES;

}
//#pragma mark - 初始化
//- (void)awakeFromNib {
//    [super awakeFromNib];
//    self.maxLength = 10000;
//    self.textField.delegate = self;
//    self.textColor = YJJ_Color(85, 85, 85);
//    self.textLengthLabelColor = YJJ_Color(92, 94, 102);
//    self.textLengthLabel.hidden = YES;
//    self.placeHolderLabelColor = YJJ_Color(1, 183, 164);
//    self.lineDefaultColor = YJJ_Color(220, 220, 220);
//    self.lineSelectedColor = YJJ_Color(1, 183, 164);
//    self.lineWarningColor = YJJ_Color(252, 57, 24);
//    self.showHistoryList = YES;
//}
//
//
//#pragma mark - 公共方法
//+ (instancetype)creatField {
//    return [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass(self) owner:nil options:nil]firstObject];
//}

+ (instancetype)creatField {
    BaseField *tf = [[BaseField alloc] init];
    [tf setUpViews];
    [tf make_Layout];
    return tf;
}


- (void)setPlaceHolderLabelHidden:(BOOL)isHidden {
    if (isHidden) {
        [UIView animateWithDuration:animateDuration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.placeHolderLabel.alpha = 0.0f;
            self.textField.placeholder = self.placeholder;
            self.bottomLine.backgroundColor = self.lineDefaultColor;
            self.textField.userInteractionEnabled = YES;
            self.textField.superview.userInteractionEnabled = YES;
        } completion:nil];
    } else {
        [UIView animateWithDuration:animateDuration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.textField becomeFirstResponder];
            self.placeHolderLabel.alpha = 1.0f;
            self.placeHolderLabel.text = self.placeholder;
            self.textField.placeholder = @"";
            self.bottomLine.backgroundColor = self.lineSelectedColor;
        } completion:nil];
    }
}

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    // 1.判断当前控件能否接收事件
//    if (self.userInteractionEnabled == NO || self.hidden == YES || self.alpha <= 0.01) return nil;
//    // 2. 判断点在不在当前控件
//    if ([self pointInside:point withEvent:event] == NO) return nil;
//    // 3.从后往前遍历自己的子控件
//    NSInteger count = self.subviews.count;
//    for (NSInteger i = count - 1; i >= 0; i--) {
//        UIView *childView = self.subviews[i];
//        // 把当前控件上的坐标系转换成子控件上的坐标系
//     CGPoint childP = [self convertPoint:point toView:childView];
//       UIView *fitView = [childView hitTest:childP withEvent:event];
//        if (fitView) { // 寻找到最合适的view
//            return fitView;
//        }
//    }
//    // 循环结束,表示没有比自己更合适的view
//    return self;
//}
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self.textField becomeFirstResponder];
//}

#pragma mark - UITextFieldDelegate


- (void)textFieldEditingChanged:(UITextField *)sender {
    if (sender.text.length > self.maxLength) {
        [UIView animateWithDuration:animateDuration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.errorLabel.alpha = 1.0;
            self.errorLabel.textColor = self.lineWarningColor;
            self.bottomLine.backgroundColor = self.lineWarningColor;
            self.textLengthLabel.textColor = self.lineWarningColor;
            self.textField.textColor = self.lineWarningColor;
            //self.placeHolderLabel.textColor = self.lineWarningColor;
        } completion:nil];
    } else {
        [UIView animateWithDuration:animateDuration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.errorLabel.alpha = 0.0;
            self.bottomLine.backgroundColor = self.lineSelectedColor;
            self.textLengthLabel.textColor = self.textLengthLabelColor;
            self.textField.textColor = self.textColor;
            //self.placeHolderLabel.textColor = self.placeHolderLabelColor;
        } completion:nil];
    }
    self.textLengthLabel.text = [NSString stringWithFormat:@"%zd/%zd",sender.text.length,self.maxLength];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self setPlaceHolderLabelHidden:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self setPlaceHolderLabelHidden:YES];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEditing:YES];
    [self setPlaceHolderLabelHidden:YES];
    return YES;
}


#pragma mark - setter
- (void)setMaxLength:(NSInteger)maxLength
{
    _maxLength = maxLength;
    self.textLengthLabel.text = [NSString stringWithFormat:@"0/%zd",maxLength];
    self.textLengthLabel.hidden = NO;
}

- (void)setErrorStr:(NSString *)errorStr
{
    _errorStr = errorStr;
    self.errorLabel.text = errorStr;
}
- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    self.textField.placeholder = placeholder;
    self.placeHolderLabel.text = placeholder;
}
- (void)setTextFont:(CGFloat)textFont
{
    _textFont = textFont;
    self.textField.font = [UIFont systemFontOfSize:textFont];
}
- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    self.textField.textColor = textColor;
    self.tintColor = textColor;         // 光标颜色
}
- (void)setPlaceHolderFont:(CGFloat)placeHolderFont
{
    _placeHolderFont = placeHolderFont;
    [self.textField setValue:[UIFont systemFontOfSize:placeHolderFont] forKeyPath:@"_placeholderLabel.font"];
}
- (void)setPlaceHolderColor:(UIColor *)placeHolderColor
{
    _placeHolderColor = placeHolderColor;
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(self.textField, ivar);
    placeholderLabel.textColor = _placeHolderColor;
//    _placeHolderColor = placeHolderColor;
//    [self.textField setValue:placeHolderColor forKeyPath:@"_placeholderLabel.textColor"];
}
- (void)setTextLengthLabelColor:(UIColor *)textLengthLabelColor
{
    _textLengthLabelColor = textLengthLabelColor;
    self.textLengthLabel.textColor = textLengthLabelColor;
}
- (void)setPlaceHolderLabelColor:(UIColor *)placeHolderLabelColor
{
    _placeHolderLabelColor = placeHolderLabelColor;
    self.placeHolderLabel.textColor = placeHolderLabelColor;
}
- (void)setLineDefaultColor:(UIColor *)lineDefaultColor
{
    _lineDefaultColor = lineDefaultColor;
    self.bottomLine.backgroundColor = lineDefaultColor;
}
- (void)setLineSelectedColor:(UIColor *)lineSelectedColor
{
    _lineSelectedColor = lineSelectedColor;
}
- (void)setLineWarningColor:(UIColor *)lineWarningColor
{
    _lineWarningColor = lineWarningColor;
}


- (void)shakeAnimationForView:(UIView*)view {
    CALayer *lbl = [view layer];
    CGPoint posLbl = [lbl position];
    CGPoint y = CGPointMake(posLbl.x-10, posLbl.y);
    CGPoint x = CGPointMake(posLbl.x+10, posLbl.y);
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:x]];
    [animation setToValue:[NSValue valueWithCGPoint:y]];
    [animation setAutoreverses:YES];
    [animation setDuration:0.08];
    [animation setRepeatCount:3];
    [lbl addAnimation:animation forKey:nil];
}

@end
