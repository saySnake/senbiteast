//
//  NoticeListVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/5.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "NoticeListVC.h"
#import "NoticeListCell.h"

@interface NoticeListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView* contentTableView;

@property (nonatomic ,strong) NSMutableArray *dataArray;

@end

@implementation NoticeListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftBtnImage:@"leftBack"];
    [self setNavBarTitle:@"公告"]; //kLocalizedString(@"search")];
    self.navBar.backgroundColor = [UIColor greenColor];
    self.view.backgroundColor = [UIColor yellowColor];
    [self setUpView];
}

- (void)setUpView {
    [self.view addSubview:self.contentTableView];
    [self.contentTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataArray.count;
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoticeListCell *cell = [NoticeListCell cellWithTableView:tableView];
    return cell;
}


- (UITableView *)contentTableView {
    if (!_contentTableView) {
        _contentTableView        = [[UITableView alloc]initWithFrame:CGRectZero
                                                                           style:UITableViewStylePlain];
        [_contentTableView setBackgroundColor:[UIColor grayColor]];
        _contentTableView.dataSource          = self;
        _contentTableView.delegate            = self;
        _contentTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.separatorStyle      = UITableViewCellSeparatorStyleNone;
        _contentTableView.tableFooterView = [UIView new];
    }
    return _contentTableView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
@end
