//
//  HeadImgaeView.h
//  Senbit
//
//  Created by 张玮 on 2020/6/18.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeadImgaeView : UIView

/// frame
/// @param frame frame
/// @param text1 text1
/// @param text2 text2
/// @param text3 text3
/// @param text4 text4 description
/// @param text5 text5 description
/// @param color5 color5 description
/// @param text6 text6 description
/// @param text7 text7 description
/// @param text8 text8 description
/// @param text9 text9 description
/// @param ismore 是不是开多  yes是 为绿色背景
/// @param crease 是不是收益增加 yes 为绿色字体颜色
//合约分享
- (id)initWithFrame:(CGRect)frame
              text1:(NSString *)text1
              text2:(NSString *)text2
              text3:(NSString *)text3
              text4:(NSString *)text4
              text5:(NSString *)text5
              text6:(NSString *)text6
              text7:(NSString *)text7
              text8:(NSString *)text8
              text9:(NSString *)text9
             isMore:(BOOL)ismore
           isCrease:(BOOL)crease;

//合约交易订单分享
- (id)initWithFrame:(CGRect)frame
   text1:(NSString *)text1
   text2:(NSString *)text2
   text3:(NSString *)text3
   text4:(NSString *)text4
   text5:(NSString *)text5
   text6:(NSString *)text6
   text7:(NSString *)text7
  isMore:(BOOL)ismore
isCrease:(BOOL)crease;


//亏损补贴分享
- (id)initWithFrame:(CGRect)frame
text1:(NSString *)text1
text2:(NSString *)text2
text3:(NSString *)text3
text4:(NSString *)text4
text5:(NSString *)text5
text6:(NSString *)text6
text7:(NSString *)text7;

//大闸蟹分享
- (id)initWitCrabFrame:(CGRect)frame;
@end

NS_ASSUME_NONNULL_END
