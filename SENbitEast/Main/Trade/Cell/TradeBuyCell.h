//
//  TradeBuyCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXDepthOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TradeBuyCell : UITableViewCell
/** 左价格标签 */
@property (strong, nonatomic) XXLabel *leftPriceLabel;

/** 左成交数量 */
@property (strong, nonatomic) XXLabel *leftNumberLabel;

/** 左下版式图 */
@property (strong, nonatomic) UIView *leftLowView;

/** 左上版式图nonatomic,  */
@property (strong, nonatomic) UIView *leftTopView;

/** 平均值 */
@property (assign, nonatomic) CGFloat ordersAverage;

/** 数据模型 */
@property (strong, nonatomic, nullable) XXDepthOrderModel *model;

/** 平均值 */
@property (assign, nonatomic) CGFloat average;

@property (assign, nonatomic) BOOL isAmount;


- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
