/************************************************************
  *  * EaseMob CONFIDENTIAL
  * __________________
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
  *
  * NOTICE: All information contained herein is, and remains
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface UIViewController (HUD)

- (MBProgressHUD *)HUD;

/**
 菊花等待框
 */
- (void)showHudInView:(UIView *)view hint:(NSString *)hint;
/**
 影藏菊花等待框
 */
- (void)hideHud;

/**
 头部显示
*/
- (void)showToastView:(NSString *)text;

- (void)showBottomView:(NSString *)text;
/**
 显示一个操作成功的提示框
 */
- (void)showSuccessHint:(NSString *)text
            compeletion:(dispatch_block_t)cblock;

/**
 显示一个操作失败的提示框
 */
- (void)showFailHint:(NSString *)text
         compeletion:(dispatch_block_t)cblock;

@end
