//
//  HistoryVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/7.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "HistoryVC.h"
#import "HisDepositCell.h"
#import "HisDepositDetailVC.h"
#import "AssertListModel.h"
#import "MJChiBaoZiHeader.h"

@interface HistoryVC ()<UITableViewDelegate,UITableViewDataSource>{
    NSInteger _currentIdx;
    int _pageIndex;
}

@property (nonatomic, weak) UIScrollView *titleScroll;
@property (nonatomic, strong) NSMutableArray *titleButton;
@property (nonatomic, weak) UIButton *selectBtn;
@property (nonatomic, strong) UITableView *tbleView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation HistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    self.navBar.backgroundColor = WhiteColor;
    [self setUpTitleScroll];
    [self setUpView];
    
    _pageIndex = 1;
    MJChiBaoZiHeader *header = [MJChiBaoZiHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestUrl)];
    header.lastUpdatedTimeLabel.hidden = YES;    // 隐藏时间
    header.stateLabel.hidden = YES;    // 隐藏状态
    self.tbleView.mj_header = header; // 设置header
    WS(weakSelf);
     // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tbleView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData1];
    }];
    [self requestUrl];
}

- (void)setUpView {
    [self.view addSubview:self.tbleView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 115;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HisDepositCell *cell = [HisDepositCell cellWithTbaleView:tableView];
    if (_currentIdx == 0) {
        cell.type = @"0";
    } else {
        cell.type = @"1";
    }
    cell.depositModel = self.dataArr[indexPath.row];
    KWeakSelf
    cell.seeBtnBlock = ^(NSString * _Nonnull ID) {
        [weakSelf canWithDraw:ID];
    };
    return cell;
}

- (void)canWithDraw:(NSString *)ID {
    NSString *url = [NSString stringWithFormat:@"%@%@/status",EastcancelWithDraw,ID];
    [SBNetworTool patchWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            _currentIdx = 1;
            [self requestUrl];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HisDepositDetailVC *detail = [[HisDepositDetailVC alloc] init];
    if (_currentIdx == 0) {
        detail.current = 0;
    } else {
        detail.current = 1;
    }
    detail.model = self.dataArr[indexPath.row];
    [self.navigationController pushViewController:detail animated:YES];
}

- (NSMutableArray *)titleButton {
    if (!_titleButton) {
        _titleButton = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"assert_depositHis"),kLocalizedString(@"assert_withDrawHis"), nil];
    }
    return _titleButton;
}


- (void)setUpTitleScroll {
    UIScrollView *view = [[UIScrollView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, CGRectGetMaxY(self.navBar.frame), DScreenW, 50);
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 50 - kLinePixel, DScreenW, kLinePixel)];
    line.backgroundColor = HEXCOLOR(0xDDDDDD);
    [view addSubview:line];
    view.showsHorizontalScrollIndicator = NO;
    self.titleScroll = view;
    CGFloat btnW = 120;
    CGFloat btnH = 35;
    for (int i = 0; i < self.titleButton.count; ++i) {
        UIButton *btn = [[UIButton alloc] init];
        btn.frame = CGRectMake(10 + btnW * i , 5, btnW, btnH);
        btn.titleLabel.font = kFont(14);
        [btn setTitle:self.titleButton[i] forState:UIControlStateNormal];
        [btn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn addTarget:self action:@selector(topTitleBtnClick:) forControlEvents:UIControlEventTouchDown];
        btn.tag = i;
        [view addSubview:btn];
        if (btn.tag == 0) {
            _currentIdx = 0;
            [self topTitleBtnClick:btn];
        }
    }
    [self.view addSubview:view];
}

- (void)topTitleBtnClick:(UIButton *)btn {
    NSLog(@"))))))))))))))))))))))))%ld  %@",btn.tag,btn.titleLabel.text);
    [self selctedBtn:btn];
    if ([btn.titleLabel.text isEqualToString:kLocalizedString(@"assert_depositHis")]) {
        NSLog(@"充币历史");
        _currentIdx = 0;
    } else {
        NSLog(@"提币历史");
        _currentIdx = 1;
    }
    [self removeNotData];
    [self requestUrl];
}


- (void)loadMoreData1 {
    if (_currentIdx == 0) {
        _pageIndex ++;
        NSDictionary *dic = @{@"type":@"charge",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastHislist params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            [self.tbleView.mj_header endRefreshing];
            if (SuccessCode == 200) {
                NSLog(@"%@",responseObject);
                NSArray *array = [AssertListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                [self.dataArr addObjectsFromArray:array];
                [self.tbleView.mj_header endRefreshing];
                [self.tbleView.mj_footer endRefreshing];
                [self.tbleView reloadData];
                self.tbleView.mj_footer.hidden = NO;
                if (array.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
        }];
    } else if (_currentIdx == 1) {
        _pageIndex ++;
        NSDictionary *dic = @{@"type":@"withdraw",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastHislist params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            [self.tbleView.mj_header endRefreshing];
            if (SuccessCode == 200) {
                NSLog(@"%@",responseObject);
                NSArray *array = [AssertListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                [self.dataArr addObjectsFromArray:array];
                [self.tbleView.mj_header endRefreshing];
                [self.tbleView.mj_footer endRefreshing];
                [self.tbleView reloadData];
                self.tbleView.mj_footer.hidden = NO;
                if (array.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
        }];
    }
}


- (void)requestUrl {
    if (_currentIdx == 0) {
        _pageIndex = 1;
        NSDictionary *dic = @{@"type":@"charge",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastHislist params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            [self.tbleView.mj_header endRefreshing];
            if (SuccessCode == 200) {
                NSLog(@"%@",responseObject);
                [self.dataArr removeAllObjects];
                self.dataArr = [AssertListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                if (self.dataArr.count == 0) {
                    [self showNotData:kLocalizedString(@"his_noMore")];
                } else {
                    [self removeNotData];
                }
                [self.tbleView reloadData];
                [self.tbleView.mj_header endRefreshing];
                self.tbleView.mj_footer.hidden = NO;
                if (self.dataArr.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                self.tbleView.mj_footer.hidden = NO;
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
        }];
    } else {
        _pageIndex = 1;
        NSDictionary *dic = @{@"type":@"withdraw",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
                            };
        [SBNetworTool getWithUrl:EastHislist params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                NSLog(@"%@",responseObject);
                [self.dataArr removeAllObjects];
                self.dataArr = [AssertListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                if (self.dataArr.count == 0) {
                    [self showNotData:kLocalizedString(@"his_noMore")];
                } else {
                    [self removeNotData];
                }
                [self.tbleView reloadData];
                [self.tbleView.mj_header endRefreshing];
                self.tbleView.mj_footer.hidden = NO;
                if (self.dataArr.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                self.tbleView.mj_footer.hidden = NO;
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
        }];
    }
}

- (void)selctedBtn:(UIButton *)btn {
    [_selectBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
    [btn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
    _selectBtn.transform = CGAffineTransformIdentity;
    _selectBtn.tag = btn.tag;
    btn.transform = CGAffineTransformMakeScale(1.3, 1.3);
    _selectBtn = btn;
}


- (UITableView *)tbleView {
    if (_tbleView == nil) {
        _tbleView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScroll.frame), kScreen_Width, kScreen_Height - CGRectGetMaxY(self.titleScroll.frame)) style:UITableViewStylePlain];
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.dataSource = self;
        _tbleView.delegate = self;
        _tbleView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 83 : 65)];
        _tbleView.estimatedRowHeight = 0;
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.estimatedSectionHeaderHeight = 0;
        _tbleView.estimatedSectionFooterHeight = 0;
        if (@available(iOS 11.0, *)) {
            _tbleView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tbleView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}


@end
