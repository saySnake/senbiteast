//
//  AddressCell.h
//  Senbit
//
//  Created by 张玮 on 2020/2/23.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AddressCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (strong, nonatomic) void(^deleteBtnBlock)();

@property (nonatomic ,strong) UIImageView *img;
@property (nonatomic ,strong) UILabel *content;
@property (nonatomic ,strong) UILabel *addreText;
@property (nonatomic ,strong) AddressModel *model;
@property (nonatomic ,strong) UIButton *deleBtn;

@property (nonatomic ,strong) UILabel *linkLb;

@property (nonatomic ,strong) UILabel *markTitle;
//标签
@property (nonatomic ,strong) UILabel *markLb;
@end

NS_ASSUME_NONNULL_END
