//
//  SelectedCoinCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/20.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneTableSockeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectedCell : UITableViewCell

@property (nonatomic ,strong) UILabel *coin;
@property (nonatomic ,strong) UILabel *price;
@property (nonatomic ,strong) UIColor *backColor;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic ,strong) OneTableSockeModel *model;


@end

NS_ASSUME_NONNULL_END
