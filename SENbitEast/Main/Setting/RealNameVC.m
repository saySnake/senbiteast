//
//  RealNameVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/14.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "RealNameVC.h"
#import "CPCountryTableViewController.h"
#import "CPCountry.h"
#import "LoginOutActionSheet.h"
#import "ActionSheetModel.h"


@interface RealNameVC ()<UITextFieldDelegate>
@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contentLb;
@property (nonatomic ,strong) FSCustomButton *countryBtn;
@property (nonatomic ,strong) BaseField *accountTF;
@property (nonatomic ,strong) FSCustomButton *IDCardBtn;
@property (nonatomic ,strong) UITextField *cardTF;
@property (nonatomic ,strong) UILabel *line;
@property (nonatomic ,strong) UIButton *submitBtn;
@property (nonatomic ,strong) NSString *IDType;

@end

@implementation RealNameVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
//    [self setNavBarTitle:kLocalizedString(@"real_native")]; //kLocalizedString(@"search")];
    self.navBar.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];
    [self configKeyBoardRespond];
}

- (void)setUpView {
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.countryBtn];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.IDCardBtn];
    [self.view addSubview:self.cardTF];
    [self.view addSubview:self.line];
    [self.view addSubview:self.submitBtn];
    [self make_layout];
}

- (void)make_layout {
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(75);
        make.width.mas_equalTo(350);
        make.height.mas_equalTo(40);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(25);
        make.width.mas_equalTo(350);
        make.height.mas_equalTo(20);
    }];
    
    [self.countryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_lessThanOrEqualTo(80);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(60);
        make.height.mas_equalTo(25);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.countryBtn.mas_bottom).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.IDCardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(50);
        make.width.mas_lessThanOrEqualTo(60);
        make.height.mas_equalTo(50);
    }];
    
    [self.cardTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.IDCardBtn.mas_right).offset(0);
        make.top.mas_equalTo(self.IDCardBtn);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.cardTF.mas_bottom).offset(5);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.line.mas_bottom).offset(40);
        make.height.mas_equalTo(50);
    }];
    
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak RealNameVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.cardTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];

}



- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.textAlignment = 0;
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.text = kLocalizedString(@"real_real");
        _titleLb.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.text = kLocalizedString(@"real_over");
        _contentLb.textAlignment = 0;
        _contentLb.font = ThirteenFontSize;
        _contentLb.textColor = HEXCOLOR(0x666666);
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}


- (BaseField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[BaseField alloc] init];
        _accountTF.placeholder = kLocalizedString(@"real_pleaseName");
        _accountTF.placeHolderColor = PlaceHolderColor;
        _accountTF.textField.secureTextEntry = NO;
        _accountTF.textField.keyboardType = UIKeyboardTypeDefault;
        [_accountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}


- (FSCustomButton *)countryBtn {
    if (!_countryBtn) {
        _countryBtn = [[FSCustomButton alloc] init];
        _countryBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        [_countryBtn setImage:[UIImage imageNamed:@"icon_Triangles"] forState:UIControlStateNormal];
        _countryBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _countryBtn.backgroundColor = [UIColor clearColor];
        _countryBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_countryBtn setTitleColor:ThemeTextColor forState:UIControlStateNormal];
        _countryBtn.contentHorizontalAlignment = 1;
        _countryBtn.userInteractionEnabled = YES;
        [_countryBtn setTitle:@"--" forState:UIControlStateNormal];
        [_countryBtn addTarget:self action:@selector(countryAction) forControlEvents:UIControlEventTouchUpInside];
        _countryBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _countryBtn;
}

- (FSCustomButton *)IDCardBtn {
    if (!_IDCardBtn) {
        _IDCardBtn = [[FSCustomButton alloc] init];
        _IDCardBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        [_IDCardBtn setImage:[UIImage imageNamed:@"icon_Triangles"] forState:UIControlStateNormal];
        _IDCardBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _IDCardBtn.backgroundColor = [UIColor clearColor];
        _IDCardBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_IDCardBtn setTitleColor:ThemeTextColor forState:UIControlStateNormal];
        _IDCardBtn.contentHorizontalAlignment = 1;
        _IDCardBtn.userInteractionEnabled = YES;
        [_IDCardBtn setTitle:@"--" forState:UIControlStateNormal];
        [_IDCardBtn addTarget:self action:@selector(cardAction:) forControlEvents:UIControlEventTouchUpInside];
        _IDCardBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _IDCardBtn;
}

- (UITextField *)cardTF {
    if (!_cardTF) {
        _cardTF = [[UITextField alloc] init];
        _cardTF.textColor = FieldTextColor;
        _cardTF.textAlignment = 0;
        _cardTF.font = FifteenFontSize;
        NSAttributedString *attrStrings = [[NSAttributedString alloc] initWithString:kLocalizedString(@"real_cardPlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _cardTF.attributedPlaceholder = attrStrings;
        _cardTF.adjustsFontSizeToFitWidth = YES;
        _cardTF.delegate = self;
        [_cardTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _cardTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.cardTF.text.length > 0 && self.accountTF.textField.text.length > 0) {
        self.submitBtn.userInteractionEnabled = YES;
        self.submitBtn.backgroundColor = ThemeGreenColor;
    } else {
        self.submitBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.submitBtn.userInteractionEnabled = NO;
    }
}

- (UILabel *)line {
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = LineColor;
    }
    return _line;
}

- (UIButton *)submitBtn {
    if (!_submitBtn) {
        _submitBtn = [[UIButton alloc] init];
        _submitBtn.cornerRadius = 3;
        _submitBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _submitBtn.userInteractionEnabled = NO;
        [_submitBtn setTitle:kLocalizedString(@"real_confirm") forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitBtn;
}

- (void)cardAction:(UIButton *)sender {
    [self.view endEditing:YES];
    ActionSheetModel *model1 = [[ActionSheetModel alloc]init];
    model1.title = kLocalizedString(@"real_persion");
    ActionSheetModel *model2 = [[ActionSheetModel alloc]init];
    model2.title = kLocalizedString(@"real_passPd");
    ActionSheetModel *model3 = [[ActionSheetModel alloc]init];
    model3.title = kLocalizedString(@"real_drive");
    ActionSheetModel *model4 = [[ActionSheetModel alloc]init];
    model4.title = kLocalizedString(@"real_hongkon");
    ActionSheetModel *model5 = [[ActionSheetModel alloc]init];
    model5.title = kLocalizedString(@"real_tw");
    ActionSheetModel *model6 = [[ActionSheetModel alloc]init];
    model6.title = kLocalizedString(@"real_other");
    NSArray *arr = @[model1,model2,model3,model4,model5,model6];
    LoginOutActionSheet *sheet = [LoginOutActionSheet customActionSheetWithData:arr];
    [sheet showFromView:self.view];
    sheet.clickBlock =^(NSString * str){
        if ([str isEqualToString:kLocalizedString(@"real_persion")]) {
            self.IDType = @"IDCard";
        } else if ([str isEqualToString:kLocalizedString(@"real_passPd")]) {
            self.IDType = @"Passport";
        } else if ([str isEqualToString:kLocalizedString(@"real_drive")]) {
            self.IDType = @"DrivingLicence";
        } else if ([str isEqualToString:kLocalizedString(@"real_hongkon")]) {
            self.IDType = @"HK&MO-EXIT";
        } else if ([str isEqualToString:kLocalizedString(@"real_tw")]) {
            self.IDType = @"TW-EXIT";
        } else if ([str isEqualToString:kLocalizedString(@"real_other")]) {
            self.IDType = @"Other";
        }
        [self.IDCardBtn setTitle:str forState:UIControlStateNormal];
    };
}

- (void)confirm:(UIButton *)sender {
    if (self.accountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"real_pleaseName")];
        return;
    }
    if ([self.countryBtn.titleLabel.text isEqualToString:@"--"]) {
        [self showToastView:kLocalizedString(@"real_country")];
        return;
    }
    
    if ([self.IDCardBtn.titleLabel.text isEqualToString:@"--"]) {
        [self showToastView:kLocalizedString(@"real_selectedType")];
        return;
    }
    
    if (self.cardTF.text.length == 0) {
        [self showToastView:kLocalizedString(@"real_persinInfo")];
        return;
    }
    
    [self.view endEditing:YES];
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    NSDictionary *dic = @{@"nationality":info.nationality,
                          @"name":self.accountTF.textField.text,
                          @"IDType":self.IDType,
                          @"IDNumber":self.cardTF.text
    };
    [SBNetworTool postWithUrl:EastReal params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
            info.realName = YES;
            [[CacheManager sharedMnager] saveUserInfo:info];
            [self showToastView:kLocalizedString(@"real_submit")];
            [self delay:1 task:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)countryAction {
    [self.view endEditing:YES];
    CPCountryTableViewController *cp = [[CPCountryTableViewController alloc] init];
    cp.callback = ^(CPCountry *country) {
        NSString *lan = kLanguageManager.currentLanguage;

        if ([lan isEqualToString:@"en"]) {
            [self.countryBtn setTitle:[NSString stringWithFormat:@"%@", country.en] forState:UIControlStateNormal];

        } else {
            [self.countryBtn setTitle:[NSString stringWithFormat:@"%@", country.zh] forState:UIControlStateNormal];

        }
    };
    [self.navigationController pushViewController:cp animated:YES];

}
@end
