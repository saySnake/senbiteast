//
//  TimeBtn.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/14.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TimeBtn : UIButton
/** 验证码倒计时的时长 */
@property (nonatomic, assign) NSInteger durationOfCountDown;
//原始 字体颜色
@property (nonatomic,strong) UIColor *originalColor;
//倒计时 字体颜色
@property (nonatomic,strong) UIColor *processColor;

- (void)startCountDown;

@end

NS_ASSUME_NONNULL_END
