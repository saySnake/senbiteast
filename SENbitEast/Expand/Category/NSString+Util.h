//
//  NSString+Util.h
//  UsedCar
//
//  Created by Alan on 13-11-8.
//  Copyright (c) 2013年 Alan. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSString (Util)

+ (BOOL)isPhone:(NSString *)phone ;
- (NSString *)md5;
- (NSString *)trim;
- (NSString *)encodeURL;
- (NSString *)sha1;


- (NSString *)encrypt3DES;
- (NSString *)decrypt3DES;

- (NSString *)encryptAES;
- (NSString *)decryptAES;

/*
 
// 防止显示（null）
- (int)lengthUnicode;
- (NSString *)omitForSize:(CGSize)size font:(UIFont *)font;

// 3DES加密
- (NSString *)encrypt3DES:(NSString *)gkey iv:(NSString *)iv;
// 3DES解密
- (NSString *)decrypt3DES:(NSString *)gkey iv:(NSString *)iv;
*/

//是否有效的密码
- (BOOL)pwValid;
/*是否包含中文*/
- (BOOL)isChinese;

/**
 判断emoji
 */
- (BOOL)isContainsEmoji;
//是不是链接
- (BOOL)isURL;
//是不是文件 如果是返回文件信息 否则 返回 nil
+ (UIImage *)imageWithFileType:(NSString *)type;
+ (NSString *)decodeUTF8ToChinese:(NSString *)encodeStr;
+ (NSString *)encodeChineseToUTF8:(NSString *)encodeStr;
/* 调整行间距 */
- (NSMutableAttributedString *)lineSpacing:(NSInteger)spc;

@end



@interface NSString(NSDate)
//返回指定格式的当前时间字符串
+ (NSString *)timeStamp;
+ (NSString *)dateFmtStr:(NSString *)fmt date:(NSDate *)date;
+ (NSString *)timeIntervalDescription:(NSDate *)date;
+ (NSString *)fmtDateString:(NSString *)date withFmt:(NSString *)fmt;
- (NSDateComponents *)componentsWithFormat:(NSString *)fmt;

+ (NSString *)stringFromDate:(NSNumber *)number;

//计算星座
+ (NSString *)getConstellation:(NSDate *)date;

@end
