//
//  BalanceCoinsModel.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/2.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BalanceCoinsModel.h"

@implementation BalanceCoinsModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
        @"ID":@"id"
    };
}

+ (NSDictionary *)mj_objectClassInArray {
    return @{
        @"coinPair" :@"CoinPairModel",
    };
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
        if ([property.name isEqualToString:@"balance"]) {
            if (oldValue == nil) return @"0.000";
        } else if ([property.name isEqualToString:@"locked"]) {
            if (oldValue == nil) return @"0.000";
        } else if ([property.name isEqualToString:@"freezed"]) {
            if (oldValue == nil) return @"0.000";
        }
    return oldValue;
}

@end
