//
//  NoticeListCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/5.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "NoticeListCell.h"

@implementation NoticeListCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"NoticeListCell";
    NoticeListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[NoticeListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        self.backgroundColor = LineColor;
        self.contentView.backgroundColor = LineColor;
    }
    return self;
}
- (void)setUpView {
    [self.contentView addSubview:self.titleLb];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(10);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(15);
    }];
    
    [self.contentView addSubview:self.timeLb];
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.width.mas_lessThanOrEqualTo(120);
        make.height.mas_equalTo(12);
        make.centerY.mas_equalTo(self.titleLb);
    }];
    
    [self.contentView addSubview:self.contentLb];
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(15);
        make.left.mas_equalTo(self.titleLb);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(50);
    }];
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kLinePixel);
    }];
    [self.contentView addSubview:self.seeDetail];
    [self.seeDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.line.mas_bottom).offset(10);
        make.width.mas_lessThanOrEqualTo(130);
        make.height.mas_equalTo(12);
    }];
    
    [self.contentView addSubview:self.img];
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.width.height.mas_equalTo(15);
        make.centerY.mas_equalTo(self.seeDetail);
    }];
    
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.font = ThirteenFontSize;
        _titleLb.text = @"关于SENbit上线KB公告";
        _titleLb.textAlignment = 0;
    }
    return _titleLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.text = @"尊敬的SENbit用户\n：自从应价合约—亏损补贴机制在全球发布以来，";
        _contentLb.textAlignment = 0;
        _contentLb.font = TenFontSize;
    }
    return _contentLb;
}

- (UILabel *)line {
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = [UIColor blackColor];
    }
    return _line;
}

- (UILabel *)seeDetail {
    if (!_seeDetail) {
        _seeDetail = [[UILabel alloc] init];
        _seeDetail.textAlignment = 0;
        _seeDetail.font = TwelveFontSize;
        _seeDetail.text = @"查看详情";
    }
    return _seeDetail;
}

- (UIImageView *)img {
    if (!_img) {
        _img = [[UIImageView alloc] init];
        _img.backgroundColor = [UIColor yellowColor];
    }
    return _img;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.text = @"2020-08-20";
        _timeLb.textAlignment = 2;
        _timeLb.font = TwelveFontSize;
    }
    return _timeLb;
}

- (void)setFrame:(CGRect)frame {
//    frame.origin.x = 10;
//    frame.size.width -= 20;
    frame.size.height -= 5;
    frame.origin.y += 5;
    [super setFrame:frame];
}
@end
