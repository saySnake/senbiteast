//
//  ShareView.h
//  Senbit
//
//  Created by 张玮 on 2020/1/14.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

/*
 
 //代理
 - (void)shareButtonAction:(NSInteger)integer {
     NSLog(@"截屏分享_测试界面_点击了第 %ld 个按钮", integer);
 }

 - (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
     
     UIImage *image = [self imageWithScreenshot];
     ShareView *shareviews = [[ShareView alloc] initWithFrame:[UIScreen mainScreen].bounds screenshotImage:image];
     shareviews.shareViewDelegate = self;
 //分享App
 //    UIImage *image = [UIImage imageNamed:@"dog01"];
 //    ShareView *shareviews = [[ShareView alloc] initWithFrame:[UIScreen mainScreen].bounds screenshotImage:image];
 //    shareviews.shareViewDelegate = self;

 }

 */
@protocol NNShareViewdDelegate<NSObject>

@optional

- (void)shareButtonAction:(UIImageView *)integer index:(NSInteger)integer;

@end

@interface SharedView : BaseView

- (instancetype)initWithFrame:(CGRect)frame screenshotImage:(UIImage *)screenshotImage;


//大闸蟹分享
- (instancetype)initFrame:(CGRect)frame newCrabImage:(UIImage *)image;

@property (nonatomic, weak) id<NNShareViewdDelegate> shareViewDelegate;

@end

NS_ASSUME_NONNULL_END
