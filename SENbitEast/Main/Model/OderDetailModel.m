//
//  OderDetailModel.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "OderDetailModel.h"

@implementation OderDetailModel
- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
  if ([property.name isEqualToString:@"createdAt"]) {
        if (oldValue) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
            //当地时间
            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
            NSDate *dateFormatted = [dateFormatter dateFromString:oldValue];
            [dateFormatter setDateFormat:@"HH:mm MM/dd"];
            NSString *locationTimeString = [dateFormatter stringFromDate:dateFormatted];
            return locationTimeString;
        }
  } 
    return oldValue;
}

@end
