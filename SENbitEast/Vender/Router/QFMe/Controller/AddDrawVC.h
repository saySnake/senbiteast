//
//  AddDrawVC.h
//  Senbit
//
//  Created by 张玮 on 2020/2/23.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseViewController.h"
#import "AddressModel.h"
#import "WithCoinListModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol AddDrawVCDelegate <NSObject>

@optional

- (void)selectAddressModel:(AddressModel *)model;


@end


@interface AddDrawVC : BaseViewController

@property (nonatomic ,strong) WithCoinListModel *model;

@property (nonatomic ,assign) id<AddDrawVCDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
