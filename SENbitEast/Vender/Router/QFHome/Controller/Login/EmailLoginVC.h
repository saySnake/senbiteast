//
//  EmailLoginVC.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/29.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^loginSuccessblock)();
typedef void (^loginCancleBlock)();

@class MoudleHome;

@interface EmailLoginVC : BaseViewController

/// 外部接口
@property(nonatomic, strong) MoudleHome *interface;

@property (nonatomic ,copy)loginSuccessblock finishBlock;

@property (nonatomic ,copy)loginCancleBlock cancleBlock;

@property (strong, nonatomic) void(^emailLoginBlock)();


@end

NS_ASSUME_NONNULL_END
