//
//  SelectedMenView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/20.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectedMenView : UIView

+(instancetype)MenuViewWithDependencyView:(UIView *)dependencyView MenuView:(UIView *)leftmenuView isShowCoverView:(BOOL)isCover;

-(instancetype)initWithDependencyView:(UIView *)dependencyView MenuView:(UIView *)leftmenuView isShowCoverView:(BOOL)isCover;
    

-(void)show;

-(void)hidenWithoutAnimation;
-(void)hidenWithAnimation;

@end

NS_ASSUME_NONNULL_END
