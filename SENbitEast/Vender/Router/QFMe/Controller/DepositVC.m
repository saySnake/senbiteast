//
//  DepositVC.m
//  Senbit
//
//  Created by 张玮 on 2020/2/20.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "DepositVC.h"
#import "SelectCoinView.h"
#import "SearchCoinVC.h"
#import "FSCustomButton.h"
#import "MMScanViewController.h"
#import "DepositAdressModel.h"
#import "BDFCustomPhotoAlbum.h"
#import "MoreChainView.h"

@interface DepositVC ()<SelectCoinViewDelegate,SearchCoinVCDelegate>

@property (nonatomic ,strong) UIScrollView *scroll;
@property (nonatomic ,strong) SelectCoinView *btnView;

@property (nonatomic ,strong) UILabel *chainName;
@property (nonatomic ,strong) MoreChainView *chainView;
@property (nonatomic ,assign) CGFloat moreChainH;


@property (nonatomic ,strong) UIView *contentView;
@property (nonatomic ,strong) UIImageView *img;
@property (nonatomic ,strong) UIButton *storeImgBtn;
@property (nonatomic ,strong) UILabel *depTitle;
@property (nonatomic ,strong) UILabel *depAdressText;
@property (nonatomic ,strong) UIButton *copysAddBtn;
@property (nonatomic ,strong) UILabel *tagText;
@property (nonatomic ,strong) UILabel *attentTagLabel;
@property (nonatomic ,strong) UIButton *copysTagBtn;
@property (nonatomic ,strong) FSCustomButton *fotBtn;
@property (nonatomic ,strong) UILabel *fotLabel;
@property (nonatomic ,strong) UILabel *line1;
@property (nonatomic ,strong) UILabel *line2;
@property (nonatomic ,strong) UILabel *line3;
@property (nonatomic ,strong) DepositAdressModel *modl;
@property (nonatomic ,assign) BOOL isTag;
@property (nonatomic ,strong) NSMutableDictionary *dic;

@property (nonatomic ,strong) NSMutableArray *dataArr;


@end

@implementation DepositVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.btnView.coinName setTitle:self.modelName forState:UIControlStateNormal];
    [self assertsAddress];
    [self make_layout];
}

- (void)coinList {
    [SBNetworTool getWithUrl:EastCoinList params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        
        NSDictionary *dic = responseObject[@"data"][self.modelName];
        NSString *str = [NSString stringWithFormat:kLocalizedString(@"deposit_num%@coinAddress"),dic[@"chargeConfirmLimit"]];
        _fotLabel.text = str;

        if (SuccessCode == 200) {
            self.dic = (NSMutableDictionary *)responseObject[@"data"];
        } else {
            [self showToastView:SuccessMessage];
        }
        
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)assertsAddress {
    NSString *url = [NSString stringWithFormat:@"%@%@",EastDeposit,self.modelName];
    [SBNetworTool getWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
        self.modl = [DepositAdressModel mj_objectWithKeyValues:responseObject[@"data"]];
        UIImage *ima = [MMScanViewController createQRImageWithString:self.modl.address QRSize:CGSizeMake(250, 250) QRColor:ClearColor bkColor:MainWhiteColor];
        [self.img setImage:ima];
        self.tagText.text = self.modl.memo;
        self.depAdressText.text = self.modl.address;
        } else {
            [self showToastView:SuccessMessage];
        }
        [self coinList];
        
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setUpView];
    self.navBar.backgroundColor = WhiteColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"deposit_navTitle")];
    [_storeImgBtn setTitle:kLocalizedString(@"deposit_saveQrCode") forState:UIControlStateNormal];
    _depTitle.text = kLocalizedString(@"deposit_depositAddress");
    [_copysAddBtn setTitle:kLocalizedString(@"deposit_copyDepositAddres") forState:UIControlStateNormal];
    _attentTagLabel.text = kLocalizedString(@"deposit_lostTag");
    [_copysTagBtn setTitle:kLocalizedString(@"deposit_copyTag") forState:UIControlStateNormal];
}

- (void)setNav {
    [self setNavBarTitle:kLocalizedString(@"deposit_navTitle")];
    [self setLeftBtnImage:@"leftBack"];
}


- (void)setUpView {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.scroll];
    [self.scroll addSubview:self.btnView];
    
    
    self.moreChainH = self.chainView.chainH;
    
    [self.scroll addSubview:self.chainName];
    
    [self.scroll addSubview:self.chainView];

    [self.scroll addSubview:self.contentView];
    
    [self.contentView addSubview:self.img];
    
    [self.contentView addSubview:self.storeImgBtn];
    
    [self.contentView addSubview:self.depTitle];
    
    [self.contentView addSubview:self.depAdressText];
    
    [self.contentView addSubview:self.copysAddBtn];
    [self.contentView addSubview:self.tagText];
    
    [self.contentView addSubview:self.attentTagLabel];
    
    [self.contentView addSubview:self.copysTagBtn];
    
    [self.scroll addSubview:self.fotBtn];
    
    [self.scroll addSubview:self.fotLabel];
    
    [self make_layout];
}

- (void)selectCoinNameAction {
    SearchCoinVC *searchCoin = [[SearchCoinVC alloc] init];
    searchCoin.delegate = self;
    searchCoin.dic = self.dic;
    searchCoin.isWithDraw = NO;
    [self.navigationController pushViewController:searchCoin animated:YES];
}

- (void)selectCoin:(CoinModel *)model {
    NSLog(@"%@",model);
    self.modelName = model.coinName;
    [self.btnView.coinName setTitle:model.ID forState:UIControlStateNormal];
    [self make_layout];
}

- (void)storeImg:(UIButton *)sender {
    NSLog(@"保存图片");
    [self showToastView:kLocalizedString(@"toastKeepImgSuccess")];
    [[BDFCustomPhotoAlbum shareInstance] saveToNewThumb:self.img.image];
}

- (void)copyAdd:(UIButton *)sender {
    NSLog(@"复制地址");
    [self showToastView:kLocalizedString(@"faceToastCopySuce")];
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = self.depAdressText.text;
}

- (void)copyTag:(UIButton *)sender {
    NSLog(@"复制tag");
    [self showToastView:kLocalizedString(@"faceToastCopySuce")];
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = self.tagText.text;
}


- (void)make_layout {
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW);
        make.height.mas_equalTo(DScreenH - kNavBarAndStatusBarHeight);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.btnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(DScreenW - 20);
    }];
    
    
    if ([self.modelName isEqualToString:@"USDT"] || [self.modelName isEqualToString:@"EUSDT"] ||[self.modelName isEqualToString:@"TUSDT"] ) {
        self.moreChainH = self.chainView.chainH;
        self.chainName.hidden = NO;
        self.chainView.hidden = NO;
        [self.chainName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.btnView.mas_bottom).offset(10);
            make.left.mas_equalTo(10);
            make.width.mas_lessThanOrEqualTo(120);
            make.height.mas_equalTo(14);
        }];
        
        if ([self.modelName isEqualToString:@"EUSDT"]) {
            self.chainView.seleIdx = 1;
        } else if([self.modelName isEqualToString:@"USDT"]) {
            self.chainView.seleIdx = 0;
        } else if ([self.modelName isEqualToString:@"TUSDT"]) {
            self.chainView.seleIdx = 2;
        }
        [self.chainView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.chainName.mas_bottom).offset(10);
            make.height.mas_equalTo(self.moreChainH);
            make.width.mas_equalTo(self.scroll.mas_width);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
        }];
    } else {
        self.chainView.hidden = YES;
        self.chainName.hidden = YES;
        self.moreChainH = 0;
    }
    
    self.isTag = NO;
    if (self.modl.memo.length > 1) {
        self.isTag = YES;
    }
    
    if (self.isTag == NO) {
        if (self.moreChainH) {
            [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.chainView.mas_bottom).offset(20);
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.height.mas_equalTo(360);
                make.width.mas_equalTo(DScreenW - 20);
            }];

        } else {
            [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.btnView.mas_bottom).offset(20);
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.height.mas_equalTo(360);
                make.width.mas_equalTo(DScreenW - 20);
            }];
        }
        
    } else {
        if (self.moreChainH) {
            [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                 make.top.mas_equalTo(self.chainView.mas_bottom).offset(20);
                 make.left.mas_equalTo(10);
                 make.right.mas_equalTo(-10);
                 make.height.mas_equalTo(480);
                 make.width.mas_equalTo(DScreenW - 20);
            }];
        } else {
            [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                 make.top.mas_equalTo(self.btnView.mas_bottom).offset(20);
                 make.left.mas_equalTo(10);
                 make.right.mas_equalTo(-10);
                 make.height.mas_equalTo(480);
                 make.width.mas_equalTo(DScreenW - 20);
            }];
        }
    }
    self.tagText.hidden = self.isTag ? NO :YES;
    self.attentTagLabel.hidden = self.isTag ? NO :YES;
    self.copysTagBtn.hidden = self.isTag ? NO :YES;
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.height.mas_equalTo(150);
        make.width.mas_equalTo(150);
    }];
    
    [self.storeImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.img.mas_bottom).offset(10);
        make.height.mas_equalTo(35);
        make.width.mas_lessThanOrEqualTo(180);
    }];

    [self.depTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.storeImgBtn.mas_bottom).offset(20);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(15);
    }];

    [self.depAdressText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.depTitle.mas_bottom).offset(20);
        make.height.mas_equalTo(25);
        make.width.mas_lessThanOrEqualTo(300);
    }];

    [self.copysAddBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.depAdressText.mas_bottom).offset(20);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(25);
    }];

    [self.tagText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.copysAddBtn.mas_bottom).offset(30);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(100);
    }];

    [self.attentTagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.tagText.mas_bottom).offset(20);
        make.width.mas_lessThanOrEqualTo(300);
        make.height.mas_equalTo(20);
    }];
    
    [self.copysTagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.attentTagLabel.mas_bottom).offset(25);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(25);
    }];

    [self.fotBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_bottom).offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(10);
    }];

    [self.fotLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.fotBtn.mas_bottom).offset(20);
        make.right.mas_equalTo(-10);
        make.width.mas_equalTo(DScreenW - 20);
        make.bottom.mas_equalTo(0);
    }];
}

- (UIScrollView *)scroll {
    if (!_scroll) {
        _scroll = [[UIScrollView alloc] init];
        _scroll.backgroundColor = WhiteColor;
        _scroll.showsHorizontalScrollIndicator = NO;//隐藏水平滚动条
    }
    return _scroll;
}

- (UILabel *)chainName {
    if (!_chainName) {
        _chainName = [[UILabel alloc] init];
        _chainName.text = kLocalizedString(@"deposit_chainName");
        _chainName.textColor = [UIColor blackColor];
        _chainName.textAlignment = 0;
        _chainName.font = ThirteenFontSize;
        _chainName.hidden = YES;
    }
    return _chainName;
}

- (SelectCoinView *)btnView {
    if (!_btnView) {
        _btnView = [[SelectCoinView alloc] init];
        _btnView.delegate = self;
        _btnView.cornerRadius = 3;
        _btnView.backgroundColor = ThemeDarkBlack;
    }
    return _btnView;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = ThemeDarkBlack;
    }
    return _contentView;
}

- (UIImageView *)img {
    if (!_img) {
        _img = [[UIImageView alloc] init];
        _img.backgroundColor = [UIColor clearColor];
    }
    return _img;
}

- (UIButton *)storeImgBtn {
    if (!_storeImgBtn) {
        _storeImgBtn = [[UIButton alloc] init];
        [_storeImgBtn setTitle:kLocalizedString(@"deposit_saveImg") forState:UIControlStateNormal];
        _storeImgBtn.backgroundColor = HEXCOLOR(0xECEEFA);
        _storeImgBtn.titleLabel.font = FourteenFontSize;
        [_storeImgBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _storeImgBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _storeImgBtn.cornerRadius = 3;
        [_storeImgBtn addTarget:self action:@selector(storeImg:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _storeImgBtn;
}

- (UILabel *)depTitle {
    if (!_depTitle) {
        _depTitle = [[UILabel alloc] init];
        _depTitle.text = kLocalizedString(@"deposit_depositAddress");
        _depTitle.textColor = HEXCOLOR(0x999999);
        _depTitle.font = TwelveFontSize;
        _depTitle.textAlignment = 1;
        _depTitle.adjustsFontSizeToFitWidth = YES;
    }
    return _depTitle;
}

- (UILabel *)depAdressText {
    if (!_depAdressText) {
        _depAdressText = [[UILabel alloc] init];
        _depAdressText.textColor = MainWhiteColor;
        _depAdressText.font = TwelveFontSize;
        _depAdressText.textAlignment = 1;
    }
    return _depAdressText;
}

- (UIButton *)copysAddBtn {
    if (!_copysAddBtn) {
        _copysAddBtn = [[UIButton alloc] init];
        _copysAddBtn.cornerRadius = 3;
        [_copysAddBtn setTitle:kLocalizedString(@"deposit_copy") forState:UIControlStateNormal];
        _copysAddBtn.titleLabel.font = TwelveFontSize;
        _copysAddBtn.backgroundColor = HEXCOLOR(0xECEEFA);
        [_copysAddBtn setTitleColor:HEXCOLOR(0x999999) forState:UIControlStateNormal];
        _copysAddBtn.titleLabel.textAlignment = 1;
        [_copysAddBtn addTarget:self action:@selector(copyAdd:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _copysAddBtn;
}

- (UILabel *)tagText {
    if (!_tagText) {
        _tagText = [[UILabel alloc] init];
        _tagText.textAlignment = 1;
        _tagText.textColor = MainWhiteColor;
        _tagText.font = BOLDSYSTEMFONT(12);
        _tagText.font = TwelveFontSize;
    }
    return _tagText;
}

- (UILabel *)attentTagLabel {
    if (!_attentTagLabel) {
        _attentTagLabel = [[UILabel alloc] init];
        _attentTagLabel.text = kLocalizedString(@"deposit_attent");
        _attentTagLabel.textAlignment = 1;
        _attentTagLabel.textColor = ThemeGreenColor;
        _attentTagLabel.font = TwelveFontSize;
        _attentTagLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _attentTagLabel;
}

- (UIButton *)copysTagBtn {
    if (!_copysTagBtn) {
        _copysTagBtn = [[UIButton alloc] init];
        [_copysTagBtn setTitle:kLocalizedString(@"deposit_copyTag") forState:UIControlStateNormal];
        _copysTagBtn.titleLabel.font = TwelveFontSize;
        _copysTagBtn.cornerRadius = 3;
        [_copysTagBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
        _copysTagBtn.backgroundColor = HEXCOLOR(0xECEEFA);
        [_copysTagBtn addTarget:self action:@selector(copyTag:) forControlEvents:UIControlEventTouchUpInside];
        _copysTagBtn.titleLabel.textAlignment = 1;
    }
    return _copysTagBtn;
}

- (FSCustomButton *)fotBtn {
    if (!_fotBtn) {
        _fotBtn = [[FSCustomButton alloc] init];
        _fotBtn.buttonImagePosition = FSCustomButtonImagePositionLeft;
        [_fotBtn setImage:[UIImage imageNamed:@"tishi"] forState:UIControlStateNormal];
        _fotBtn.backgroundColor = [UIColor clearColor];
        _fotBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        [_fotBtn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        _fotBtn.contentHorizontalAlignment = 1;
        _fotBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _fotBtn;
}

- (UILabel *)fotLabel {
    if (!_fotLabel) {
        _fotLabel = [[UILabel alloc] init];
        _fotLabel.textColor = PlaceHolderColor;
        _fotLabel.textAlignment = 0;
        _fotLabel.font = TwelveFontSize;
        _fotLabel.numberOfLines = 0;
        _fotLabel.text = @"";
    }
    return _fotLabel;
}

- (NSMutableDictionary *)dic {
    if (!_dic) {
        _dic = [NSMutableDictionary dictionary];
    }
    return _dic;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (MoreChainView *)chainView {
    if (!_chainView) {
        _chainView = [[MoreChainView alloc] initWithFrame:CGRectZero];
        _chainView.hidden = YES;
        _chainView.backgroundColor = WhiteColor;
        WS(weakSelf);
        _chainView.caluteResult = ^(NSString * _Nonnull restult,NSInteger idx) {
            if (idx == 0) {
                weakSelf.modelName = @"USDT";
            } else if (idx == 1){
                weakSelf.modelName = @"EUSDT";
            } else if (idx == 2) {
                weakSelf.modelName = @"TUSDT";
            }
            [weakSelf assertsAddress];
            [weakSelf make_layout];
        };
    }
    return _chainView;
}

@end
