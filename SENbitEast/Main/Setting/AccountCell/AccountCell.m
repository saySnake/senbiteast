//
//  AccountCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AccountCell.h"

@implementation AccountCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"AccountCell";
    AccountCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[AccountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    [self.contentView addSubview:self.leftLb];
    [self.leftLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(20);
    }];
    
    [self.contentView addSubview:self.contentLb];
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-60);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.rightImg];
    [self.rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-40);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(8);
    }];
}

- (UILabel *)leftLb {
    if (!_leftLb) {
        _leftLb = [[UILabel alloc] init];
        _leftLb.font = kFont16;
        _leftLb.textAlignment = 0;
        _leftLb.textColor = HEXCOLOR(0x333333);
        _leftLb.adjustsFontSizeToFitWidth = YES;
    }
    return _leftLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.textAlignment = 2;
        _contentLb.text = kLocalizedString(@"account_modify");
        _contentLb.font = TwelveFontSize;
        _contentLb.textColor = HEXCOLOR(0x999999);
    }
    return _contentLb;
}

- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [[UIImageView alloc] init];
        _rightImg.image = [UIImage imageNamed:@"set_rightImg"];
        _rightImg.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _rightImg;
}
@end
