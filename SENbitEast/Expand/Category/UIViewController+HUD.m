/************************************************************
  *  * EaseMob CONFIDENTIAL
  * __________________
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
  *
  * NOTICE: All information contained herein is, and remains
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "UIViewController+HUD.h"
#import <objc/runtime.h>
#import "OMGToast.h"

static const void *HttpRequestHUDKey = &HttpRequestHUDKey;

@implementation UIViewController (HUD)

- (MBProgressHUD *)HUD {
    return objc_getAssociatedObject(self, HttpRequestHUDKey);
}

- (void)setHUD:(MBProgressHUD *)HUD {
    objc_setAssociatedObject(self, HttpRequestHUDKey, HUD, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)showHudInView:(UIView *)view hint:(NSString *)hint {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
    HUD.labelText = hint;
    [view addSubview:HUD];
    [HUD show:YES];
    [self setHUD:HUD];
}

/**
 *  显示提示框
 *
 *  @param text 提示框内容
 */
- (void)showToastView:(NSString *)text {
    [OMGToast showWithText:text topOffset:0 duration:Duration];
}

- (void)showBottomView:(NSString *)text {
    [OMGToast showWithText:text bottomOffset:Offset duration:Duration];
}


- (void)showHint:(NSString *)hint {
    //显示提示信息
    UIView *view = [[UIApplication sharedApplication].delegate window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = hint;
    hud.opacity = 1;
    hud.margin = 10.f;
    UIImageView *cusview = [[UIImageView alloc] init];
    cusview.image = [[UIImage imageNamed:@"smile_aio_default"] imageWithTintColor:MainWhiteColor];
    cusview.frame = CGRectMake(0, 0, 60, 60);
    
    hud.customView = cusview;
   // hud.yOffset = [UIScreen mainScreen].bounds.size.height/2;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:5];
}

- (void)showSuccessHint:(NSString *)text compeletion:(dispatch_block_t)cblock {
    //显示提示信息
    UIView *view = [[UIApplication sharedApplication].delegate window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeCustomView;
   // hud.labelText = text;
   // hud.label.numberOfLines=0;
   // hud.label.textColor=kColorGray4;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = TwelveFontSize;
    hud.opacity = 0.8;
    hud.margin  = 15.f;
    UIImageView *cusview = [[UIImageView alloc] init];
    cusview.image = [UIImage imageNamed:@"smile_aio_default@2x"];
    cusview.frame = CGRectMake(0, 0, 69, 69);
    hud.customView = cusview;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:2];
    hud.completionBlock = ^(){
        if (cblock) {
            cblock();
        }
    };

}
- (void)showFailHint:(NSString *)text compeletion:(dispatch_block_t)cblock {
    //显示提示信息
    UIView *view = [[UIApplication sharedApplication].delegate window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
    hud.mode = MBProgressHUDModeCustomView;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = TwelveFontSize;
    hud.opacity = 0.8;
    hud.margin = 15.f;
    hud.color = RGBACOLOR(0, 0, 0, 0.9);
    UIImageView *cusview = [[UIImageView alloc] init];
    cusview.image = [UIImage imageNamed:@"smile_aio_default_failed"];
    cusview.frame = CGRectMake(0, 0, 69, 69);
    hud.customView = cusview;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:2];
    hud.completionBlock = ^(){
        if (cblock) {
            cblock();
        }
    };

}
- (void)hideHud{
    [[self HUD] hide:YES];
}

@end
