//
//  HeadView.m
//  IFMShareDemo
//
//  Created by 张玮 on 2020/3/6.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "HeadView.h"
#import "LayCell.h"
#import <Photos/Photos.h>
#import "BDFCustomPhotoAlbum.h"
@interface HeadView()
@property (nonatomic ,strong) UILabel *titleName;
@end

@implementation HeadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = WhiteColor;
        NSString *lan = kLanguageManager.currentLangSymbol;
//        if ([lan isEqualToString:@"KRW"]) {
//            [self.collectArray addObject:@"myInvoteImg1ko"];
//            [self.collectArray addObject:@"myInvoteImg2ko"];
//            [self.collectArray addObject:@"myInvoteImg3ko"];
//        } else if ([lan isEqualToString:@"CNY"]) {
//            [self.collectArray addObject:@"myInvoteImg1"];
//            [self.collectArray addObject:@"myInvoteImg2"];
//            [self.collectArray addObject:@"myInvoteImg3"];
//        } else if ([lan isEqualToString:@"HKD"]) {
//            [self.collectArray addObject:@"myInvoteImg1tw"];
//            [self.collectArray addObject:@"myInvoteImg2tw"];
//            [self.collectArray addObject:@"myInvoteImg3tw"];
//        } else if ([lan isEqualToString:@"USD"]) {
//            [self.collectArray addObject:@"myInvoteImg1en"];
//            [self.collectArray addObject:@"myInvoteImg2en"];
//            [self.collectArray addObject:@"myInvoteImg3en"];
//        } else {
//            [self.collectArray addObject:@"myInvoteImg1"];
//            [self.collectArray addObject:@"myInvoteImg2"];
//            [self.collectArray addObject:@"myInvoteImg3"];
//        }
        [self.collectArray addObject:@"invoted_one"];
        [self.collectArray addObject:@"invoted_two"];
        [self.collectArray addObject:@"invoted_thre"];

        [self.collectionView reloadData];

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPath];
        });
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.collectArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"layCell" forIndexPath:indexPath];
    NSString *imgageName = [self.collectArray objectAtIndex:indexPath.item];
    cell.img.image = [UIImage imageNamed:imgageName];
    return cell;
}

//- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    LayCell *cell = (LayCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self.collectArray removeAllObjects];
    NSString *lan = kLanguageManager.currentLangSymbol;
//    if ([lan isEqualToString:@"KRW"]) {
//        [self.collectArray addObject:@"myInvoteImg1ko"];
//        [self.collectArray addObject:@"myInvoteImg2ko"];
//        [self.collectArray addObject:@"myInvoteImg3ko"];
//    } else if ([lan isEqualToString:@"CNY"]) {
//        [self.collectArray addObject:@"myInvoteImg1"];
//        [self.collectArray addObject:@"myInvoteImg2"];
//        [self.collectArray addObject:@"myInvoteImg3"];
//    } else if ([lan isEqualToString:@"HKD"]) {
//        [self.collectArray addObject:@"myInvoteImg1tw"];
//        [self.collectArray addObject:@"myInvoteImg2tw"];
//        [self.collectArray addObject:@"myInvoteImg3tw"];
//    } else if ([lan isEqualToString:@"USD"]) {
//        [self.collectArray addObject:@"myInvoteImg1en"];
//        [self.collectArray addObject:@"myInvoteImg2en"];
//        [self.collectArray addObject:@"myInvoteImg3en"];
//    } else {
//        [self.collectArray addObject:@"myInvoteImg1"];
//        [self.collectArray addObject:@"myInvoteImg2"];
//        [self.collectArray addObject:@"myInvoteImg3"];
//    }
        [self.collectArray addObject:@"invoted_one"];
        [self.collectArray addObject:@"invoted_two"];
        [self.collectArray addObject:@"invoted_thre"];

    NSString * s1;
    NSString * s2;
    NSString * s3;
    if (indexPath.item == 0) {
//        if ([lan isEqualToString:@"KRW"]) {
//            s1 = @"myInvoteImg1ko";
//        } else if ([lan isEqualToString:@"CNY"]) {
//            s1 = @"myInvoteImg1";
//        } else if ([lan isEqualToString:@"USD"]) {
//            s1 = @"myInvoteImg1en";
//        } else {
//            s1 = @"myInvoteImg1tw";
//        }
        s1 = @"invoted_one";
    }
    
    if (indexPath.item == 1) {
//        if ([lan isEqualToString:@"KRW"]) {
//            s2 = @"myInvoteImg2ko";
//        } else if ([lan isEqualToString:@"CNY"]) {
//            s2 = @"myInvoteImg2";
//        } else if ([lan isEqualToString:@"USD"]) {
//            s2 = @"myInvoteImg2en";
//        } else {
//            s2 = @"myInvoteImg2tw";
//        }
        s2 = @"invoted_two";
    }
    
    if (indexPath.item == 2) {
        s3 = @"invoted_thre";
//        if ([lan isEqualToString:@"KRW"]) {
//            s3 = @"myInvoteImg3ko";
//        } else if ([lan isEqualToString:@"CNY"]) {
//            s3 = @"myInvoteImg3";
//        } else if ([lan isEqualToString:@"USD"]) {
//            s3 = @"myInvoteImg3en";
//        } else {
//            s3 = @"myInvoteImg3tw";
//        }
    }

    if ([self.collectArray[indexPath.item] isEqual:s1]) {
//        if ([lan isEqualToString:@"KRW"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg1ko"];
//        } else if ([lan isEqualToString:@"CNY"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg1"];
//        } else if ([lan isEqualToString:@"HKD"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg1tw"];
//        } else if ([lan isEqualToString:@"USD"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg1en"];
//        } else {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg1"];
//        }
        [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"invoted_one_selected"];
        if ([self.delegate respondsToSelector:@selector(HeadView:seletedImg:)]) {
                [self.delegate HeadView:self seletedImg:@"invoted_one"];
//            if ([lan isEqualToString:@"KRW"]) {
//                [self.delegate HeadView:self seletedImg:@"myInvoteImg1ko"];
//            } else if ([lan isEqualToString:@"CNY"]) {
//                [self.delegate HeadView:self seletedImg:@"myInvoteImg1"];
//            } else if ([lan isEqualToString:@"HKD"]) {
//                [self.delegate HeadView:self seletedImg:@"myInvoteImg1tw"];
//            } else if ([lan isEqualToString:@"USD"]) {
//                [self.delegate HeadView:self seletedImg:@"myInvoteImg1en"];
//            } else {
//                [self.delegate HeadView:self seletedImg:@"myInvoteImg1"];
//            }
        }
    }
    else if ([self.collectArray[indexPath.item] isEqual:s2]) {
        [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"invoted_two_selected"];
//        [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg2"];
//        if ([lan isEqualToString:@"KRW"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg2ko"];
//        } else if ([lan isEqualToString:@"CNY"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg2"];
//        } else if ([lan isEqualToString:@"HKD"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg2tw"];
//        } else if ([lan isEqualToString:@"USD"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg2en"];
//        } else {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg2"];
//        }
        if ([self.delegate respondsToSelector:@selector(HeadView:seletedImg:)]) {
               [self.delegate HeadView:self seletedImg:@"invoted_two"];
//            if ([lan isEqualToString:@"KRW"]) {
//               [self.delegate HeadView:self seletedImg:@"myInvoteImg2ko"];
//           } else if ([lan isEqualToString:@"CNY"]) {
//               [self.delegate HeadView:self seletedImg:@"myInvoteImg2"];
//           } else if ([lan isEqualToString:@"HKD"]) {
//               [self.delegate HeadView:self seletedImg:@"myInvoteImg2tw"];
//           } else if ([lan isEqualToString:@"USD"]) {
//               [self.delegate HeadView:self seletedImg:@"myInvoteImg2en"];
//           } else {
//               [self.delegate HeadView:self seletedImg:@"myInvoteImg2"];
//           }
        }
    }
    else if ([self.collectArray[indexPath.item] isEqual:s3]) {
        [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"invoted_thre_selected"];

//        [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg3"];
//         if ([lan isEqualToString:@"KRW"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg3ko"];
//        } else if ([lan isEqualToString:@"CNY"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg3"];
//        } else if ([lan isEqualToString:@"HKD"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg3tw"];
//        } else if ([lan isEqualToString:@"USD"]) {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg3en"];
//        } else {
//            [self.collectArray replaceObjectAtIndex:indexPath.item withObject:@"myInvoteSelecImg3"];
//        }
        if ([self.delegate respondsToSelector:@selector(HeadView:seletedImg:)]) {
            [self.delegate HeadView:self seletedImg:@"invoted_thre"];
//            if ([lan isEqualToString:@"KRW"]) {
//                 [self.delegate HeadView:self seletedImg:@"myInvoteImg3ko"];
//             } else if ([lan isEqualToString:@"CNY"]) {
//                 [self.delegate HeadView:self seletedImg:@"myInvoteImg3"];
//             } else if ([lan isEqualToString:@"HKD"]) {
//                 [self.delegate HeadView:self seletedImg:@"myInvoteImg3tw"];
//             } else if ([lan isEqualToString:@"USD"]) {
//                 [self.delegate HeadView:self seletedImg:@"myInvoteImg3en"];
//             } else {
//                 [self.delegate HeadView:self seletedImg:@"myInvoteImg3"];
//             }
        }
    }
    NSLog(@"%@",self.collectArray);
    [self.collectionView reloadData];
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.itemSize = CGSizeMake(120, 150);
        flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = WhiteColor;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[LayCell class] forCellWithReuseIdentifier:@"layCell"];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSMutableArray *)collectArray {
    if (!_collectArray) {
        _collectArray = [[NSMutableArray alloc] init];
    }
    return _collectArray;
}

- (UILabel *)titleName {
    if (!_titleName) {
        _titleName = [[UILabel alloc] init];
    }
    return _titleName;
}


@end
