//
//  BannerModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BannerModel : NSObject

/** active **/
@property (nonatomic ,copy) NSString *active;
/** createdAt **/
@property (nonatomic ,copy) NSString *createdAt;
/** img **/
@property (nonatomic ,copy) NSString *img;
/** link **/
@property (nonatomic ,copy) NSString *link;
/** name **/
@property (nonatomic ,copy) NSString *name;
/** updatedAt **/
@property (nonatomic ,copy) NSString *updatedAt;
/** sort*/
@property (nonatomic ,copy) NSString *sort;

@end

NS_ASSUME_NONNULL_END
