//
//  LoginViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/12.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"
@class MoudleLogin;

NS_ASSUME_NONNULL_BEGIN

typedef void (^loginSuccessblock)();
typedef void (^loginCancleBlock)();

//@class MoudleHome;

@interface LoginViewController : BaseViewController
/// 外部接口
//@property(nonatomic, strong) MoudleHome *interface;
@property (nonatomic, strong) MoudleLogin *interfaces;

@property (nonatomic ,copy)loginSuccessblock finishBlock;
@property (nonatomic ,copy)loginCancleBlock cancleBlock;

@end

NS_ASSUME_NONNULL_END
