//
//  SetPwdVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/13.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "SetPwdVC.h"

@interface SetPwdVC ()
@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contentLb;
@property (nonatomic ,strong) BaseField *accountTF;
@property (nonatomic ,strong) UIButton *seeBtn;
@property (nonatomic ,strong) BaseField *pwdTF;
@property (nonatomic ,strong) UIButton *scrBtn;
@property (nonatomic ,strong) UIButton *confirBtn;

@end

@implementation SetPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setNavBarTitle:kLocalizedString(@"set_navSet")]; //kLocalizedString(@"search")];
    self.navBar.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    [self configKeyBoardRespond];
    [self setUpView];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak SetPwdVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.pwdTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];

}


- (void)setUpView {
    self.view.backgroundColor = WhiteColor;
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.seeBtn];
    [self.view addSubview:self.pwdTF];
    [self.view addSubview:self.scrBtn];
    [self.view addSubview:self.confirBtn];
    [self make_layout];
}

- (void)make_layout {
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(75);
        make.width.mas_lessThanOrEqualTo(300);
        make.height.mas_equalTo(40);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(25);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(20);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentLb);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(60);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
    }];
    
    [self.seeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.accountTF.mas_top).offset(30);
    }];
    
    [self.pwdTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(40);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
    }];
    
    [self.scrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.pwdTF.mas_top).offset(30);
    }];
    
    [self.confirBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.pwdTF.mas_bottom).offset(60);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
    }];
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.text = kLocalizedString(@"setCash_scr");
        _titleLb.textAlignment = 0;
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.text = kLocalizedString(@"setCash_trade");
        _contentLb.textAlignment = 0;
        _contentLb.font = ThirteenFontSize;
        _contentLb.textColor = HEXCOLOR(0x666666);
    }
    return _contentLb;
}

- (BaseField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[BaseField alloc] init];
        _accountTF.placeholder = kLocalizedString(@"setCash_account");
        _accountTF.placeHolderColor = PlaceHolderColor;
        _accountTF.textField.secureTextEntry = YES;
        [_accountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (BaseField *)pwdTF {
    if (!_pwdTF) {
        _pwdTF = [[BaseField alloc] init];
        _pwdTF.placeholder = kLocalizedString(@"setCash_password");
        _pwdTF.placeHolderColor = PlaceHolderColor;
        _pwdTF.textField.secureTextEntry = YES;
        [_pwdTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _pwdTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountTF.textField.text.length > 0 && self.pwdTF.textField.text.length > 0) {
        self.confirBtn.backgroundColor = ThemeGreenColor;
        self.confirBtn.userInteractionEnabled = YES;

    } else {
        self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.confirBtn.userInteractionEnabled = NO;
    }
}

- (UIButton *)seeBtn {
    if (!_seeBtn) {
        _seeBtn = [[UIButton alloc] init];
        [_seeBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_seeBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_seeBtn addTarget:self action:@selector(scr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _seeBtn;
}

- (UIButton *)scrBtn {
    if (!_scrBtn) {
        _scrBtn = [[UIButton alloc] init];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn addTarget:self action:@selector(scrr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn;
}



- (UIButton *)confirBtn {
    if (!_confirBtn) {
        _confirBtn = [[UIButton alloc] init];
        _confirBtn.cornerRadius = 3;
        [_confirBtn setTitle:kLocalizedString(@"setCash_ok") forState:UIControlStateNormal];
        [_confirBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
        _confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _confirBtn.userInteractionEnabled = NO;
    }
    return _confirBtn;
}

- (void)confirm:(UIButton *)sender {
    NSLog(@"确认");
    
    BOOL judge = [NSString judgePassWordLegal:self.accountTF.textField.text];
    if (judge == NO) {
        [self showToastView:kLocalizedString(@"toast_pwdprotocol")];
        return;
    }

    if (self.accountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"setCash_pleaseCash")];
        return;
    }
    if (self.pwdTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"setCash_confirmCash")];
        return;
    }
    if (self.accountTF.textField.text.length < 8 || self.pwdTF.textField.text.length < 8) {
        [self showToastView:kLocalizedString(@"setCash_lenth")];
        return;
    }
    if (![self.accountTF.textField.text isEqualToString:self.pwdTF.textField.text]) {
        [self showToastView:kLocalizedString(@"setCash_not")];
        return;
    }
    
    
    
    
    NSDictionary *dic = @{@"tradePassword":self.pwdTF.textField.text};
    [SBNetworTool postWithUrl:EastSetTradePwd params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self showToastView:kLocalizedString(@"setCash_modiSuc")];
            [self delay:1 task:^{
                UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
                info.tradePassword = YES;
                [[CacheManager sharedMnager] saveUserInfo:info];
                 [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
    
}


- (void)scr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.accountTF.textField.secureTextEntry = NO;
    } else {
        self.accountTF.textField.secureTextEntry = YES;

    }
}

- (void)scrr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.pwdTF.textField.secureTextEntry = NO;
    } else {
        self.pwdTF.textField.secureTextEntry = YES;
    }
}


@end
