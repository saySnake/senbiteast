//
//  PhoneSheet.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/12.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "PhoneSheet.h"
#import "TipsView.h"

#define SafeShetH 230
@interface PhoneSheet()<UITextFieldDelegate>

@property (nonatomic ,weak) UIView *containerView;

@end


@implementation PhoneSheet

+ (instancetype)customActionSheets {
    PhoneSheet *sheet = [[PhoneSheet alloc] initWithFrame:CGRectZero];
    return sheet;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        [self addGestureRecognizer:tap];
    }
    return self;
}


- (void)keyboardWillShow:(NSNotification *)noti {
    NSValue *value = [[noti userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardY = DScreenH - [value CGRectValue].size.height;
    [UIView animateWithDuration:0.1 animations:^{
        self.containerView.frame = CGRectMake(0, keyboardY - SafeShetH , DScreenW, SafeShetH);
    }];
}

- (void)endEdit:(NSNotification *)noti {
    [UIView animateWithDuration:0.1 animations:^{
        self.containerView.frame = CGRectMake(0, DScreenH - SafeShetH , DScreenW, SafeShetH);
    }];
}

- (void)showFromView:(UIView *)view {
    self.frame = view.frame;
    [view addSubview:self];
    self.containerView.y = view.height;
    self.containerView.width = view.width;
    [self setUpView];
}

- (void)cancel {
    [self closeAcitonSheet];
}

- (void)tap {
    [self closeAcitonSheet];
}

- (void)closeAcitonSheet {
    __weak PhoneSheet *sheet = self;
    [IWNotificationCenter removeObserver:self];
    [UIView animateWithDuration:0.15 animations:^{
        sheet.containerView.y = sheet.height;
        sheet.containerView.width = sheet.width;
        [sheet setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
    } completion:^(BOOL finished) {
        [sheet removeFromSuperview];
    }];

}

- (UIView *)containerView {
    if (!_containerView) {
        UIView *containerView = [[UIView alloc] init];
        containerView.backgroundColor = HEXCOLOR(0xFAFAFA);
        [self addSubview:containerView];
        
        //添加阴影
        containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
        containerView.layer.shadowOffset = CGSizeMake(0, -2);
        containerView.layer.shadowOpacity = 0.17;
        containerView.layer.shadowRadius = 4;
        self.containerView = containerView;
        self.containerView.backgroundColor = WhiteColor;
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        [containerView addGestureRecognizer:tap];

    }
    return _containerView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    point = [self.containerView.layer convertPoint:point fromLayer:self.layer];
    if (![self.containerView.layer containsPoint:point]) {
        [self closeAcitonSheet];
    }
}

- (void)action:(UIButton *)sender {
    NSLog(@"确认");
    if (self.clickBlock) {
        self.clickBlock(self.msgTF.text);
        [self closeAcitonSheet];
    }
}

- (void)msgAction:(UIButton *)sender {
    NSLog(@"获取验证码");
    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
    NSDictionary *dic = @{@"type":@"NEW_PHONE_NUMBER",
                          @"phone":self.msgLbel.text,
                          @"challenge":user.geetest_challenge,
                          @"validate":user.geetest_validate,
                          @"seccode":user.geetest_seccode
    };
    [SBNetworTool getWithUrl:EastReciveSMS params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [self becomeFirst];
            [self.msgBtn startCountDown];
        } else {
            [TipsView showTipOnKeyWindow:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [TipsView showTipOnKeyWindow:FailurMessage];
    }];
    
//    [SBNetworTool patchWithUrl:EastblindPhone params:dic success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//        if (SuccessCode == 200) {
//            [self becomeFirst];
//            [self.msgBtn startCountDown];
//        } else {
//            [TipsView showTipOnKeyWindow:SuccessMessage];
//        }
//    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//        [TipsView showTipOnKeyWindow:FailurMessage];
//    }];
}

- (void)becomeFirst {
    [self.msgTF becomeFirstResponder];
}

- (void)setUpView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(endEdit:)
                                                  name:UIKeyboardWillHideNotification object:nil];
    [self.containerView addSubview:self.titleName];
    [self.containerView addSubview:self.cancleBtn];
    [self.containerView addSubview:self.line1];
    [self.containerView addSubview:self.msgLbel];
    [self.containerView addSubview:self.msgTF];
    [self.containerView addSubview:self.line2];
    [self.containerView addSubview:self.msgBtn];
    [self.containerView addSubview:self.confirBtn];
    [self make_Layout];
}

- (void)make_Layout {
    [self.titleName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(25);
    }];
    
    [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(25);
    }];
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(10);
        make.top.mas_equalTo(self.titleName.mas_bottom).offset(15);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1);
    }];
    
    [self.msgLbel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(15);
    }];
    
    [self.msgTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.msgLbel.mas_bottom).offset(15);
    }];
    
    [self.msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(self.msgTF.mas_top).offset(-10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(34);
    }];
    
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.msgTF.mas_bottom).offset(5);
        make.height.mas_equalTo(1);
    }];
    
    [self.confirBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(self.line2.mas_bottom).offset(15);
    }];
    
    CGRect frame = CGRectMake(0, DScreenH - SafeShetH, DScreenW, SafeShetH);
    self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, SafeShetH);
     UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.containerView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.containerView.layer.mask = maskLayer;
    [UIView animateWithDuration:0.25 animations:^{
        self.containerView.frame = frame;
        self.alpha = 1;
    }];

}


- (UILabel *)titleName {
    if (!_titleName) {
        _titleName = [[UILabel alloc] init];
        _titleName.textColor = PlaceHolderColor;
        _titleName.font = [UIFont systemFontOfSize:16];
        _titleName.textAlignment = 0;
        _titleName.text = kLocalizedString(@"safePhoneCode");
    }
    return _titleName;
}

- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = LineColor;
    }
    return _line1;
}

- (UILabel *)msgLbel {
    if (!_msgLbel) {
        _msgLbel = [[UILabel alloc] init];
        _msgLbel.textAlignment = 0;
        _msgLbel.textColor = MainWhiteColor;
        _msgLbel.font = TwelveFontSize;
        _msgLbel.adjustsFontSizeToFitWidth = YES;
    }
    return _msgLbel;
}

- (UITextField *)msgTF {
    if (!_msgTF) {
        _msgTF = [[UITextField alloc] init];
        _msgTF.delegate = self;
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"safePhoneCode") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _msgTF.attributedPlaceholder = attrString1;
        _msgTF.secureTextEntry = NO;
        _msgTF.textColor = MainWhiteColor;
        _msgTF.font = TwelveFontSize;
        [_msgTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _msgTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.msgTF.text.length > 0) {
        self.confirBtn.backgroundColor = ThemeGreenColor;
        self.confirBtn.userInteractionEnabled = YES;

    } else {
        self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.confirBtn.userInteractionEnabled = NO;
    }
}

- (UILabel *)line2 {
    if (!_line2) {
        _line2 = [[UILabel alloc] init];
        _line2.backgroundColor = LineColor;
    }
    return _line2;
}

- (UIButton *)confirBtn {
    if (!_confirBtn) {
        _confirBtn = [[UIButton alloc] init];
        [_confirBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        _confirBtn.tag = 0;
        _confirBtn.cornerRadius = 3;
        _confirBtn.titleLabel.font = EighteenFontSize;
        [_confirBtn setTitle:kLocalizedString(@"safeBlind") forState:UIControlStateNormal];
        [_confirBtn addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
        _confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _confirBtn.userInteractionEnabled = NO;
    }
    return _confirBtn;
}

- (TimeBtn *)msgBtn {
    if (!_msgBtn) {
        _msgBtn = [[TimeBtn alloc] init];
        _msgBtn.backgroundColor = ClearColor;
        [_msgBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _msgBtn.processColor = PlaceHolderColor;
        _msgBtn.tag = 1;
        _msgBtn.titleLabel.font = FourteenFontSize;
        _msgBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _msgBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_msgBtn setTitle:kLocalizedString(@"safeSendMsg") forState:UIControlStateNormal];
        [_msgBtn addTarget:self action:@selector(msgAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _msgBtn;
}

- (UIButton *)cancleBtn {
    if (!_cancleBtn) {
        _cancleBtn = [[UIButton alloc] init];
        [_cancleBtn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        _cancleBtn.tag = 1;
        _cancleBtn.cornerRadius = 3;
        _cancleBtn.titleLabel.font = FourteenFontSize;
        _cancleBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_cancleBtn setTitle:kLocalizedString(@"safeCancel") forState:UIControlStateNormal];
        _cancleBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_cancleBtn addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancleBtn;
}

@end
