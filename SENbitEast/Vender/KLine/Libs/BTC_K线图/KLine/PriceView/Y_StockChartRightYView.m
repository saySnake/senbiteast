//
//  Y_StockChartRightYView.m
//  BTC-Kline
//
//  Created by yate1996 on 16/5/3.
//  Copyright © 2016年 yate1996. All rights reserved.
//

#import "Y_StockChartRightYView.h"
#import "UIColor+Y_StockChart.h"
#import "Masonry.h"

@interface Y_StockChartRightYView ()

@end


@implementation Y_StockChartRightYView

-(void)setMaxValue:(double)maxValue
{
    _maxValue = maxValue;
    

    self.maxValueLabel.text = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f",maxValue] RoundingMode:NSRoundDown scale:[KDecimal scale:KDetail.symbolModel.quotePrecision]];
}

-(void)setMax2Value:(double)max2Value
{
    _max2Value = max2Value;
    self.max2ValueLabel.text = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f",max2Value] RoundingMode:NSRoundDown scale:[KDecimal scale:KDetail.symbolModel.quotePrecision]];

}

-(void)setMiddleValue:(double)middleValue
{
    _middleValue = middleValue;
    self.middleValueLabel.text = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f",middleValue] RoundingMode:NSRoundDown scale:[KDecimal scale:KDetail.symbolModel.quotePrecision]];
}

-(void)setMin2Value:(double)min2Value
{
    _min2Value = min2Value;
    self.min2ValueLabel.text = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f",min2Value] RoundingMode:NSRoundDown scale:[KDecimal scale:KDetail.symbolModel.quotePrecision]];
}

-(void)setMinValue:(double)minValue
{
    _minValue = minValue;
    self.minValueLabel.text = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f",minValue] RoundingMode:NSRoundDown scale:[KDecimal scale:KDetail.symbolModel.quotePrecision]];
}

-(void)setMinLabelText:(NSString *)minLabelText
{
    _minLabelText = minLabelText;
    self.minValueLabel.text = minLabelText;
}

#pragma mark - get方法
#pragma mark maxPriceLabel的get方法
-(UILabel *)maxValueLabel
{
    if (!_maxValueLabel) {
        _maxValueLabel = [self private_createLabel];
        [self addSubview:_maxValueLabel];
        [_maxValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self);
            make.right.equalTo(self.mas_right).offset(-7);
            make.height.equalTo(@20);
        }];
    }
    return _maxValueLabel;
}

-(UILabel *)max2ValueLabel
{
    if (!_max2ValueLabel) {
        _max2ValueLabel = [self private_createLabel];
        [self addSubview:_max2ValueLabel];
        [_max2ValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self).multipliedBy(1.5);
            make.right.equalTo(self.mas_right).offset(-7);
            make.height.equalTo(@20);
        }];
    }
    return _max2ValueLabel;
}


#pragma mark middlePriceLabel的get方法
-(UILabel *)middleValueLabel
{
    if (!_middleValueLabel) {
        _middleValueLabel = [self private_createLabel];
        [self addSubview:_middleValueLabel];
        [_middleValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.equalTo(self.mas_right).offset(-7);
            make.height.width.equalTo(self.maxValueLabel);
        }];
    }
    return _middleValueLabel;
}

-(UILabel *)min2ValueLabel
{
    if (!_min2ValueLabel) {
        _min2ValueLabel = [self private_createLabel];
        [self addSubview:_min2ValueLabel];
        [_min2ValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self).multipliedBy(0.5);
            make.right.equalTo(self.mas_right).offset(-7);
            make.height.width.equalTo(self.maxValueLabel);
        }];
    }
    return _min2ValueLabel;
}
#pragma mark minPriceLabel的get方法
-(UILabel *)minValueLabel
{
    if (!_minValueLabel) {
        _minValueLabel = [self private_createLabel];
        [self addSubview:_minValueLabel];
        [_minValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self);
            make.right.equalTo(self.mas_right).offset(-7);
            make.height.width.equalTo(self.maxValueLabel);
        }];
    }
    return _minValueLabel;
}

#pragma mark - 私有方法
#pragma mark 创建Label
- (UILabel *)private_createLabel
{
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentRight;
    label.font = [UIFont systemFontOfSize:10];
    label.textColor = [UIColor assistTextColor];
    label.adjustsFontSizeToFitWidth = YES;
    [self addSubview:label];
    return label;
}
@end
