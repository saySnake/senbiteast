//
//  HeadImgaeView.m
//  Senbit
//
//  Created by 张玮 on 2020/6/18.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "HeadImgaeView.h"


@implementation HeadImgaeView
- (id)initWithFrame:(CGRect)frame
              text1:(NSString *)text1
              text2:(NSString *)text2
              text3:(NSString *)text3
              text4:(NSString *)text4
              text5:(NSString *)text5
              text6:(NSString *)text6
              text7:(NSString *)text7
              text8:(NSString *)text8
              text9:(NSString *)text9
             isMore:(BOOL)ismore
           isCrease:(BOOL)crease {
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor =ThemeDarkBlack;
        UIView *backView = [[UIView alloc] initWithFrame:frame];
        backView.backgroundColor =ThemeDarkBlack;
        [self addSubview:backView];
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(DScreenW/2 - 120, 80, 236, 182)];
        img.backgroundColor = [UIColor clearColor];
        if (crease) { //正
            if ([text1 isEqualToString:kLocalizedString(@"alert_moreRegisterContract")]) {
                img.image = [UIImage imageNamed:@"contract_gainSmall"];
            } else {
                img.image = [UIImage imageNamed:@"contract_gainBig"];
            }
        } else { //负
            if ([text1 isEqualToString:kLocalizedString(@"alert_losRegisterContract")]) {
                img.image = [UIImage imageNamed:@"contract_losSmall"];
            } else {
                img.image = [UIImage imageNamed:@"contract_losBig"];
            }
        }
        img.contentMode = UIViewContentModeScaleAspectFit;
        [backView addSubview:img];
        
        UILabel *lb1 = [[UILabel alloc] init];
        lb1.frame = CGRectMake(0, CGRectGetMaxY(img.frame) + 50, DScreenW, 20);
        lb1.text = text1;
        lb1.adjustsFontSizeToFitWidth = YES;
        lb1.textColor = MainWhiteColor;
        lb1.textAlignment = 1;
        lb1.font = [UIFont systemFontOfSize:25];
        [backView addSubview:lb1];

        UILabel *lb2 = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lb1.frame) + 50, DScreenW, 15)];
        lb2.textAlignment = 1;
        lb2.textColor = MainWhiteColor;
        lb2.font = [UIFont systemFontOfSize:25];
        lb2.text = text2;
        [backView addSubview:lb2];
        
        UILabel *lb3 = [[UILabel alloc] init];
        lb3.frame = CGRectMake(0, CGRectGetMaxY(lb2.frame) + 50, DScreenW, 40);
        lb3.textAlignment = 1;
        if (crease) {
            lb3.textColor = HEXCOLOR(0x00A09B);
        } else {
            lb3.textColor = HEXCOLOR(0xE81D75);
        }
        lb3.font = [UIFont systemFontOfSize:50];
        lb3.text = text3;
        lb3.adjustsFontSizeToFitWidth = YES;
        [backView addSubview:lb3];
        
        UIView * bot = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(lb3.frame) + 50, DScreenW - 20, 90)];
        bot.backgroundColor = WhiteColor;
        [backView addSubview:bot];
        
        UILabel * lb4 = [[UILabel alloc] init];
        lb4.text = text4;
        lb4.textAlignment = 0;
        lb4.adjustsFontSizeToFitWidth = YES;
        lb4.frame = CGRectMake(15, 15, 130, 20);
        lb4.textColor = HEXCOLOR(0x999999);
        lb4.font = [UIFont systemFontOfSize:15];
        [bot addSubview:lb4];
        
        UILabel *lb5 = [[UILabel alloc] init];
        lb5.frame = CGRectMake(15, 45, 60, 20);
        lb5.textAlignment = 1;
        lb5.font = [UIFont systemFontOfSize:15];
        if (ismore) {
            lb5.backgroundColor = HEXCOLOR(0x00A09B);
        } else {
            lb5.backgroundColor = HEXCOLOR(0xE81D75);
        }
        lb5.text = text5;
        lb5.textColor = WhiteColor;
        [bot addSubview:lb5];
        
        UILabel *lb6 = [[UILabel alloc] init];
        lb6.text = text6;
        lb6.adjustsFontSizeToFitWidth = YES;
        lb6.textAlignment = 1;
        lb6.frame = CGRectMake(self.centerX - 80, 15, 160, 20);
//        lb6.center = CGPointMake(self.width / 2, 10);
//        lb6.bounds = CGRectMake(0, 0, 170, 15);
        lb6.textColor = HEXCOLOR(0x999999);
        lb6.font = [UIFont systemFontOfSize:15];
        [bot addSubview:lb6];
        
        UILabel *lb7 = [[UILabel alloc] init];
        lb7.text = text7;
        lb7.adjustsFontSizeToFitWidth = YES;
        lb7.textColor = MainWhiteColor;
//        lb7.center = CGPointMake(self.width / 2, 40);
//        lb7.bounds = CGRectMake(0, 0, 170, 15);
        lb7.frame = CGRectMake(self.centerX - 80, 45, 160, 20);
        lb7.textAlignment = 1;
        lb7.font = [UIFont systemFontOfSize:15];
        [bot addSubview:lb7];
        
        UILabel *lb8 = [[UILabel alloc] init];
        lb8.frame = CGRectMake(DScreenW - 100, 15, 70, 20);
        lb8.text = text8;
        lb8.textAlignment = 2;
        lb8.adjustsFontSizeToFitWidth = YES;
        lb8.textColor = HEXCOLOR(0x999999);
        lb8.font = [UIFont systemFontOfSize:15];
        [bot addSubview:lb8];
        
        UILabel *lb9 = [[UILabel alloc] init];
        lb9.frame = CGRectMake(DScreenW - 100, 45, 70, 20);
        lb9.text = text9;
        lb9.textAlignment = 1;
        lb9.textColor = MainWhiteColor;
        lb9.adjustsFontSizeToFitWidth = YES;
        lb9.font = [UIFont systemFontOfSize:15];
        [bot addSubview:lb9];
    }
    return self;
}



- (id)initWithFrame:(CGRect)frame
              text1:(NSString *)text1
              text2:(NSString *)text2
              text3:(NSString *)text3
              text4:(NSString *)text4
              text5:(NSString *)text5
              text6:(NSString *)text6
              text7:(NSString *)text7
             isMore:(BOOL)ismore
           isCrease:(BOOL)crease {
        self = [super initWithFrame:frame];
        if (self) {
            UIView *backView = [[UIView alloc] initWithFrame:frame];
            backView.backgroundColor = ThemeDarkBlack;
            [self addSubview:backView];
            
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(DScreenW/2 - 120, 80, 236, 182)];
            img.backgroundColor = [UIColor clearColor];
            if (crease) { //正
                if ([text1 isEqualToString:kLocalizedString(@"alert_moreRegisterContract")]) {
                    img.image = [UIImage imageNamed:@"contract_gainSmall"];
                } else {
                    img.image = [UIImage imageNamed:@"contract_gainBig"];
                }
            } else { //负
                if ([text1 isEqualToString:kLocalizedString(@"alert_losRegisterContract")]) {
                    img.image = [UIImage imageNamed:@"contract_losSmall"];
                } else {
                    img.image = [UIImage imageNamed:@"contract_losBig"];
                }
            }
            img.contentMode = UIViewContentModeScaleAspectFit;
            [backView addSubview:img];
            
            UILabel *lb1 = [[UILabel alloc] init];
            lb1.frame = CGRectMake(0, CGRectGetMaxY(img.frame) + 40, DScreenW, 20);
            lb1.text = text1;
            lb1.adjustsFontSizeToFitWidth = YES;
            lb1.textColor = MainWhiteColor;
            lb1.textAlignment = 1;
            lb1.font = [UIFont systemFontOfSize:25];
            [backView addSubview:lb1];
            
            UILabel *lb2 = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lb1.frame) + 60, DScreenW, 15)];
            lb2.textAlignment = 1;
            lb2.textColor = MainWhiteColor;
            lb2.font = [UIFont systemFontOfSize:25];
            lb2.text = text2;
            [backView addSubview:lb2];
            
            UILabel *lb3 = [[UILabel alloc] init];
            lb3.frame = CGRectMake(0, CGRectGetMaxY(lb2.frame) + 50, DScreenW, 40);
            lb3.textAlignment = 1;
            if (crease) {
                lb3.textColor = HEXCOLOR(0x00A09B);
                lb3.text = [NSString stringWithFormat:@"+%@ USDT",text3];
            } else {
                lb3.textColor = HEXCOLOR(0xE81D75);
                lb3.text = [NSString stringWithFormat:@"%@ USDT",text3];
            }
            lb3.font = [UIFont systemFontOfSize:50];
            [backView addSubview:lb3];
            
            UIView * bot = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(lb3.frame) + 50, DScreenW - 20, 90)];
            bot.backgroundColor = WhiteColor;
            [backView addSubview:bot];

            UILabel * lb4 = [[UILabel alloc] init];
            lb4.text = text4;
            lb4.textAlignment = 0;
            lb4.adjustsFontSizeToFitWidth = YES;
            lb4.frame = CGRectMake(15, 15, 130, 20);
            lb4.textColor = HEXCOLOR(0x999999);
            lb4.font = [UIFont systemFontOfSize:15];
            [bot addSubview:lb4];
            
            UILabel *lb5 = [[UILabel alloc] init];
            lb5.frame = CGRectMake(15, 45, 60, 20);
            lb5.textAlignment = 1;
            lb5.adjustsFontSizeToFitWidth = YES;
            lb5.font = [UIFont systemFontOfSize:15];
            if (ismore) {
                lb5.backgroundColor = HEXCOLOR(0x00A09B);
            } else {
                lb5.backgroundColor = HEXCOLOR(0xE81D75);
            }
            lb5.text = text5;
            lb5.textColor = WhiteColor;
            [bot addSubview:lb5];
            
            
            UILabel *lb6 = [[UILabel alloc] init];
            lb6.frame = CGRectMake(DScreenW - 100, 15, 70, 20);
            lb6.text = text6;
            lb6.textAlignment = 2;
            lb6.adjustsFontSizeToFitWidth = YES;
            lb6.textColor = HEXCOLOR(0x999999);
            lb6.font = [UIFont systemFontOfSize:15];
            [bot addSubview:lb6];
            
            UILabel *lb7 = [[UILabel alloc] init];
            lb7.frame = CGRectMake(DScreenW - 100, 45, 70, 20);
            lb7.text = text7;
            lb7.textAlignment = 1;
            lb7.textColor = MainWhiteColor;
            lb7.adjustsFontSizeToFitWidth = YES;
            lb7.font = [UIFont systemFontOfSize:15];
            [bot addSubview:lb7];
        }
        return self;
}

- (id)initWithFrame:(CGRect)frame
              text1:(NSString *)text1
              text2:(NSString *)text2
              text3:(NSString *)text3
              text4:(NSString *)text4
              text5:(NSString *)text5
              text6:(NSString *)text6
              text7:(NSString *)text7 {
        self = [super initWithFrame:frame];
        if (self) {
    //        self.backgroundColor =ThemeDarkBlack;
            
            UIImageView *imag = [[UIImageView alloc] init];
            imag.frame = CGRectMake(20, 20, 100, 70);
            imag.contentMode = UIViewContentModeScaleAspectFit;
            imag.image = [UIImage imageNamed:@"home_TitleImg"];
            
            UIImageView *backView = [[UIImageView alloc] initWithFrame:frame];
            backView.backgroundColor =ThemeDarkBlack;
            backView.contentMode = UIViewContentModeScaleToFill;
            backView.image = [UIImage imageNamed:@"butie_background"];
            
            [backView addSubview:imag];
            
            UILabel *lab1 = [[UILabel alloc] init];
            lab1.frame = CGRectMake(40, DScreenH *0.24, 200, 20);
            lab1.text = text1;
            lab1.textAlignment = 0;
            lab1.textColor = HEXCOLOR(0xC2DAFF);
            lab1.font = [UIFont systemFontOfSize:20];
            [backView addSubview:lab1];
            
            UILabel *lab2 = [[UILabel alloc] init];
            if (kIs_iPhoneX) {
                lab2.frame = CGRectMake(40, CGRectGetMaxY(lab1.frame) + 20, 400, 30);
            } else {
                lab2.frame = CGRectMake(40, CGRectGetMaxY(lab1.frame) + 10, 400, 30);
            }
            lab2.text = text2;
            lab2.textAlignment = 0;
            lab2.textColor = WhiteColor;
            lab2.font = [UIFont systemFontOfSize:25];
            [backView addSubview:lab2];

            UILabel *lab3 = [[UILabel alloc] init];
            lab3.text = text3;
            lab3.textColor = WhiteColor;
            lab3.textAlignment = 2;
            lab3.font = [UIFont systemFontOfSize:20];
            if (kIs_iPhoneX) {
                lab3.frame = CGRectMake(DScreenW - 140, DScreenH * 0.64, 120, 25);
            } else {
                lab3.frame = CGRectMake(DScreenW - 140, DScreenH * 0.54, 120, 25);
            }
            [backView addSubview:lab3];
            
            UILabel *lab4 = [[UILabel alloc] init];
            lab4.text = text4;
            lab4.textColor = WhiteColor;
            lab4.textAlignment = 2;
            lab4.font = [UIFont systemFontOfSize:20];
            lab4.frame = CGRectMake(DScreenW - 200, CGRectGetMaxY(lab3.frame) + 10, 180, 25);
            [backView addSubview:lab4];
            
            UILabel *lab5 = [[UILabel alloc] init];
            lab5.text = text5;
            lab5.textColor = WhiteColor;
            lab5.textAlignment = 2;
            lab5.font = [UIFont systemFontOfSize:25];
            lab5.frame = CGRectMake(DScreenW - 200, CGRectGetMaxY(lab4.frame) + 10, 180, 30);
            [backView addSubview:lab5];

            UILabel *lab6 = [[UILabel alloc] init];
            lab6.text = text6;
            lab6.textColor = WhiteColor;
            lab6.textAlignment = 2;
            lab6.font = [UIFont systemFontOfSize:20];
            lab6.frame = CGRectMake(DScreenW - 280, CGRectGetMaxY(lab5.frame) + 40, 260, 20);
            [backView addSubview:lab6];

            UILabel *lab7 = [[UILabel alloc] init];
            lab7.text = text7;
            lab7.textColor = WhiteColor;
            lab7.textAlignment = 2;
            lab7.font = [UIFont systemFontOfSize:20];
            lab7.frame = CGRectMake(DScreenW - 280, CGRectGetMaxY(lab6.frame) + 20, 260, 20);
            [backView addSubview:lab7];
            [self addSubview:backView];
        }
        return self;
}

- (id)initWitCrabFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *backView = [[UIImageView alloc] initWithFrame:frame];
        backView.backgroundColor = ThemeDarkBlack;
        backView.contentMode = UIViewContentModeScaleToFill;
        backView.image = [UIImage imageNamed:@"invoted_thre"];
        [self addSubview:backView];
    }
    return self;
}


@end
