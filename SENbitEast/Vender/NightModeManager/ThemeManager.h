//
//  ThemeManager.h
//  Senbit
//
//  Created by 张玮 on 2019/12/19.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface ThemeManager : NSObject {
@private
    //主题配置信息
    NSDictionary *_themesConfig;
}

//当前使用的主题名称fi
@property(nonatomic,copy)NSString *themeName;

//主题的配置信息
@property(nonatomic,retain)NSDictionary *themesConfig;

//字体的配置信息
@property(nonatomic,retain)NSDictionary *fontConfig;

+ (ThemeManager *)shareInstance;

//获取当前主题下的图片
- (UIImage *)getThemeImage:(NSString *)imageName;

//返回当前主题下的颜色
- (UIColor *)getColorWithName:(NSString *)name;


@end

NS_ASSUME_NONNULL_END
