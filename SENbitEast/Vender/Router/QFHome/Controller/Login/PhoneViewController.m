//
//  PhoneViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/17.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "PhoneViewController.h"
#import "SafeSheet.h"
#import "SetPasswordVC.h"
#import "CPCountryTableViewController.h"
#import "CPCountry.h"
#import "AsyncTaskButton.h"
#import "TipsView.h"
#import "EmailViewController.h"


@interface PhoneViewController ()<UITextFieldDelegate,AsyncTaskCaptchaButtonDelegate>
/** cancel **/
@property (nonatomic ,strong) BaseButton *cancelBtn;

/** loginBtn **/
@property (nonatomic ,strong) BaseButton *loginBtn;

/** titleLb **/
@property (nonatomic ,strong) BaseLabel *titleLb;

/** conentLb **/
@property (nonatomic ,strong) BaseLabel *contentLb;

/** countryBtn **/
@property (nonatomic ,strong) FSCustomButton *countryBtn;

/** countryCode **/
@property (nonatomic ,strong) FSCustomButton *countryCode;

/** accountTF **/
@property (nonatomic ,strong) UITextField *accountTF;

/** line **/
@property (nonatomic ,strong) UILabel *line;

/** nextBtn **/
@property (nonatomic ,strong) AsyncTaskButton *nextBtn;

/** emailBtn **/
@property (nonatomic ,strong) BaseButton *emailBtn;

/** 地区简称 **/
@property (nonatomic ,strong) NSString *locoal;

@end

@implementation PhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = WhiteColor;
    [self setUpView];
    [self make_Layout];
    [self configKeyBoardRespond];
}

- (void)setUpView {
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.countryBtn];
    [self.view addSubview:self.countryCode];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.line];
    [self.view addSubview:self.nextBtn];
    [self.view addSubview:self.emailBtn];
}

- (void)make_Layout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(27);
    }];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(136);
        make.height.mas_equalTo(32);
        make.right.mas_equalTo(-30);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(27);
        make.width.mas_equalTo(320);
        make.height.mas_equalTo(39);
    }];
    
    [self.countryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(70);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(24);
    }];
    
    [self.countryCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.width.mas_equalTo(60);
        make.top.mas_equalTo(self.countryBtn.mas_bottom).offset(50);
        make.height.mas_equalTo(20);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.countryCode.mas_right).offset(5);
        make.top.mas_equalTo(self.countryCode);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(self.countryCode);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(5);
    }];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(53);
        make.top.mas_equalTo(self.line.mas_bottom).offset(45);
    }];
    
    [self.emailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.nextBtn.mas_bottom).offset(35);
    }];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak PhoneViewController *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}


#pragma mark - action
- (void)backAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)loginAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)nextAction {
    WS(weakSelf);
    SafeSheet *sheet = [[SafeSheet alloc] init];
    [sheet showFromView:self.view];
    sheet.account = [NSString stringWithFormat:@"%@%@",self.countryCode.titleLabel.text,self.accountTF.text];
    sheet.country = self.countryBtn.titleLabel.text;
    sheet.countryCode = self.countryCode.titleLabel.text;
    sheet.type = SafeSheetRegister;
    sheet.clickBlock = ^(NSString * _Nonnull str) {
        [weakSelf authToken:str];
    };
}


- (void)authToken:(NSString *)str {
    NSString *phone = [NSString stringWithFormat:@"%@%@",self.countryCode.titleLabel.text,self.accountTF.text];
    NSDictionary *dic = @{@"phone":phone,@"phoneCode":str,@"type":@"REGISTER"};
    [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self pushSetPassword:responseObject[@"data"][@"authToken"]];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [TipsView showTipOnKeyWindow:FailurMessage];
    }];
}

- (void)pushSetPassword:(NSString *)authToken {
    SetPasswordVC *password = [[SetPasswordVC alloc] init];
    password.account = [NSString stringWithFormat:@"%@%@",self.countryCode.titleLabel.text,self.accountTF.text];
    password.authToken = authToken;
    password.isPhoneRegister = YES;
    password.countryCode = self.countryCode.titleLabel.text;
    password.locale = self.locoal;
    [self.navigationController pushViewController:password animated:YES];
}


- (void)countryAction {
    NSLog(@"国家");
    CPCountryTableViewController *cp = [[CPCountryTableViewController alloc] init];
    cp.isResigster = YES;
    cp.callback = ^(CPCountry *country) {
        NSString *lan = kLanguageManager.currentLanguage;

        if ([lan isEqualToString:@"en"]) {
            [self.countryBtn setTitle:country.en forState:UIControlStateNormal];

        } else {
            [self.countryBtn setTitle:country.zh forState:UIControlStateNormal];
        }
        [self.countryCode setTitle:[NSString stringWithFormat:@"+%d", country.code] forState:UIControlStateNormal];
        self.locoal = country.locale;
    };
    [self.navigationController pushViewController:cp animated:YES];
}

- (void)codeAction {
    NSLog(@"区号");
    CPCountryTableViewController *cp = [[CPCountryTableViewController alloc] init];
    cp.callback = ^(CPCountry *country) {
        [self.countryCode setTitle:[NSString stringWithFormat:@"+%d", country.code] forState:UIControlStateNormal];
    };
    [self.navigationController pushViewController:cp animated:YES];
}

- (void)emailAction {
    NSLog(@"邮箱注册");
    EmailViewController *email = [[EmailViewController alloc] init];
    [self.navigationController pushViewController:email animated:NO];
}

#pragma mark - AsyncTaskCaptchaButtonDelegate

- (void)captchaWillDisplay {
    [TipsView removeAllTipsView];
}

- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
 
    [TipsView showTipOnKeyWindow:kLocalizedString(@"login_safe")];

    return YES;
}

- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    [self.view endEditing:YES];
   if ([self.countryBtn.titleLabel.text isEqualToString:@"--"]) {
        [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_country")];
        return ;
    }
    else if ([self.countryCode.titleLabel.text isEqualToString:@"--"]) {
        [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_countryCode")];
        return ;
    }
    else if (self.accountTF.text.length == 0) {
        [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_noPhone")];
        return ;
    }
    
    if (status == YES) {
        [self nextAction];
    }
}

#pragma mark - lazy
- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = kFont16;
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        _cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [[BaseButton alloc] init];
        _loginBtn.colorName = @"login_loginBtnCor";
        _loginBtn.backColorName = @"login_registerBackground";
        [_loginBtn setTitle:kLocalizedString(@"login_login") forState:UIControlStateNormal];
        _loginBtn.titleLabel.font = BOLDSYSTEMFONT(16);
        [_loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}

- (BaseLabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[BaseLabel alloc] init];
        _titleLb.textColor = ThemeTextColor;
        _titleLb.font = BOLDSYSTEMFONT(33);
        _titleLb.textAlignment = 0;
        _titleLb.adjustsFontSizeToFitWidth = YES;
        _titleLb.text = kLocalizedString(@"login_phoneRegister");
    }
    return _titleLb;
}

- (BaseLabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[BaseLabel alloc] init];
        _contentLb.colorName = @"login_conentColor";
        _contentLb.textAlignment = 0;
        _contentLb.font = kFont16;
        _contentLb.text = kLocalizedString(@"login_contentmodify");
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}

- (FSCustomButton *)countryBtn {
    if (!_countryBtn) {
        _countryBtn = [[FSCustomButton alloc] init];
        _countryBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        [_countryBtn setImage:[UIImage imageNamed:@"icon_Triangles"] forState:UIControlStateNormal];
        _countryBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _countryBtn.backgroundColor = [UIColor clearColor];
        _countryBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_countryBtn setTitleColor:ThemeTextColor forState:UIControlStateNormal];
        _countryBtn.contentHorizontalAlignment = 1;
        _countryBtn.userInteractionEnabled = YES;
        [_countryBtn setTitle:@"--" forState:UIControlStateNormal];
        [_countryBtn addTarget:self action:@selector(countryAction) forControlEvents:UIControlEventTouchUpInside];
        _countryBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _countryBtn;
}

- (FSCustomButton *)countryCode {
    if (!_countryCode) {
        _countryCode = [[FSCustomButton alloc] init];
        _countryCode.buttonImagePosition = FSCustomButtonImagePositionRight;
        [_countryCode setImage:[UIImage imageNamed:@"icon_Triangles"] forState:UIControlStateNormal];
        _countryCode.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _countryCode.backgroundColor = [UIColor clearColor];
        _countryCode.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_countryCode setTitleColor:ThemeTextColor forState:UIControlStateNormal];
        _countryCode.contentHorizontalAlignment = 1;
        _countryCode.userInteractionEnabled = YES;
        [_countryCode setTitle:@"--" forState:UIControlStateNormal];
        [_countryCode addTarget:self action:@selector(codeAction) forControlEvents:UIControlEventTouchUpInside];
        _countryCode.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _countryCode;
}

- (UITextField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[UITextField alloc] init];
        NSAttributedString *attrString5 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"login_phoneNm") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _accountTF.attributedPlaceholder = attrString5;
        _accountTF.delegate = self;
        _accountTF.textColor = FieldTextColor;
        _accountTF.font = FifteenFontSize;
        [_accountTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (_accountTF.text.length > 0) {
        self.nextBtn.userInteractionEnabled = YES;
        self.nextBtn.backgroundColor = ThemeGreenColor;
        
    } else {
        self.nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.nextBtn.userInteractionEnabled = NO;
    }
}

- (UILabel *)line {
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = LineColor;
    }
    return _line;
}

- (AsyncTaskButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [[AsyncTaskButton alloc] init];
//        _nextBtn.colorName = @"reset_confirmTitle";
//        _nextBtn.backColorName = @"login_loginBtnCor";
        _nextBtn.delegate = self;
        [_nextBtn setTitle:kLocalizedString(@"login_next") forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = TwentyFontSize;
//        [_nextBtn addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
        _nextBtn.cornerRadius = 3;
        _nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _nextBtn.userInteractionEnabled = NO;
    }
    return _nextBtn;
}


- (BaseButton *)emailBtn {
    if (!_emailBtn) {
        _emailBtn = [[BaseButton alloc] init];
        _emailBtn.colorName = @"login_loginBtnCor";
        [_emailBtn setTitle:kLocalizedString(@"login_emailRegister") forState:UIControlStateNormal];
        _emailBtn.titleLabel.font = ThirteenFontSize;
        [_emailBtn addTarget:self action:@selector(emailAction) forControlEvents:UIControlEventTouchUpInside];
        _emailBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _emailBtn;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super hideKeyboard];
}

@end
