//
//  LoginSheet.m
//  Senbit
//
//  Created by 张玮 on 2020/3/4.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "LoginSheet.h"
#import "TipsView.h"

#define logShetH 220

@interface LoginSheet()<UITextFieldDelegate> {
    NSInteger _currentIndex; //当前索引
}

@property (nonatomic, weak) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *data;

@end

@implementation LoginSheet

+ (instancetype)customActionSheet {
    LoginSheet *sheet = [[LoginSheet alloc] initWithFrame:CGRectZero];
    return sheet;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        [self addGestureRecognizer:tap];
    }
    return self;
}


- (void)keyboardWillShow:(NSNotification *)noti {
    NSValue *value = [[noti userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardY = DScreenH - [value CGRectValue].size.height;
    [UIView animateWithDuration:0.1 animations:^{
        self.containerView.frame = CGRectMake(0, keyboardY - logShetH , DScreenW, logShetH);
    }];
}

- (void)endEdit:(NSNotification *)noti {
    [UIView animateWithDuration:0.1 animations:^{
        self.containerView.frame = CGRectMake(0, DScreenH - logShetH , DScreenW, logShetH);
    }];
}

- (void)showFromView:(UIView *)view {
    self.frame = view.frame;
    [view addSubview:self];
    self.containerView.y = view.height;
    self.containerView.width = view.width;
    [self setUpView];
}


- (void)tap {
    [self closeAcitonSheet];
}

- (void)closeAcitonSheet {
    __weak LoginSheet *sheet = self;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [UIView animateWithDuration:0.15 animations:^{
        sheet.containerView.y = sheet.height;
        sheet.containerView.width = sheet.width;
        [sheet setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
    } completion:^(BOOL finished) {
        [sheet removeFromSuperview];
    }];
}

- (UIView *)containerView {
    if (!_containerView) {
        UIView *containerView = [[UIView alloc] init];
        containerView.backgroundColor = HEXCOLOR(0xFAFAFA);
        [self addSubview:containerView];
        
        //添加阴影
        containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
        containerView.layer.shadowOffset = CGSizeMake(0, -2);
        containerView.layer.shadowOpacity = 0.17;
        containerView.layer.shadowRadius = 4;
        self.containerView = containerView;
        self.containerView.backgroundColor = WhiteColor;
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        [containerView addGestureRecognizer:tap];

    }
    return _containerView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    point = [self.containerView.layer convertPoint:point fromLayer:self.layer];
    if (![self.containerView.layer containsPoint:point]) {
        [self closeAcitonSheet];
    }
}


- (void)setUpView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(endEdit:)
                                                  name:UIKeyboardWillHideNotification object:nil];
    [self.containerView addSubview:self.safeTitle];
    [self.containerView addSubview:self.cancBtn];
    [self.containerView addSubview:self.line1];
    [self.containerView addSubview:self.msgTF];
    [self.containerView addSubview:self.line2];
    [self.containerView addSubview:self.copBtn];
    [self.containerView addSubview:self.confirBtn];
    [self make_Layout];
}

- (void)make_Layout {
    
    [self.safeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(13);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(15);
    }];
    
    [self.cancBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.safeTitle);
        make.right.mas_equalTo(self.containerView).offset(-20);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(15);
    }];
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.safeTitle.mas_bottom).offset(15);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    
    [self.msgTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(self.line1.mas_bottom).offset(25);
    }];

    [self.copBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.msgTF);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(34);
    }];

    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.msgTF.mas_bottom).offset(10);
        make.height.mas_equalTo(1);
    }];

    [self.confirBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(self.line2.mas_bottom).offset(20);
    }];
    
    CGRect frame = CGRectMake(0, DScreenH - logShetH, DScreenW, logShetH);
    self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, logShetH);
     UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.containerView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.containerView.layer.mask = maskLayer;
    [UIView animateWithDuration:0.25 animations:^{
        self.containerView.frame = frame;
        self.alpha = 1;
    }];
}

- (UILabel *)safeTitle {
    if (!_safeTitle) {
        _safeTitle = [[UILabel alloc] init];
        _safeTitle.textAlignment = 0;
        _safeTitle.font = TwelveFontSize;
        _safeTitle.text = kLocalizedString(@"safeTitle");
        _safeTitle.textColor = HEXCOLOR(0x000116);
    }
    return _safeTitle;
}

- (UIButton *)cancBtn {
    if (!_cancBtn) {
        _cancBtn = [[UIButton alloc] init];
        [_cancBtn setTitleColor:LineColor forState:UIControlStateNormal];
        _cancBtn.cornerRadius = 3;
        _cancBtn.titleLabel.font = FifteenFontSize;
        [_cancBtn setTitle:kLocalizedString(@"safeCancel") forState:UIControlStateNormal];
        [_cancBtn addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancBtn;
}

- (void)cancel {
    [self closeAcitonSheet];
}


- (UITextField *)msgTF {
    if (!_msgTF) {
        _msgTF = [[UITextField alloc] init];
        _msgTF.delegate = self;
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"safePleaseGoogle") attributes:@{NSForegroundColorAttributeName:[UIColor colorWithLightColorStr:@"A0A0A0" DarkColor:@"A0A0A0"]}];
        _msgTF.textColor = [UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"];
        _msgTF.attributedPlaceholder = attrString1;
        _msgTF.font = TwelveFontSize;
        _msgTF.adjustsFontSizeToFitWidth = YES;
        [_msgTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _msgTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.msgTF.text.length > 0) {
        self.confirBtn.userInteractionEnabled = YES;
        self.confirBtn.backgroundColor = ThemeGreenColor;
    } else {
        self.confirBtn.userInteractionEnabled = NO;
        self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
    }
}

- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = LineColor;
    }
    return _line1;
}
- (UILabel *)line2 {
    if (!_line2) {
        _line2 = [[UILabel alloc] init];
        _line2.backgroundColor = LineColor;
    }
    return _line2;
}

- (UIButton *)confirBtn {
    if (!_confirBtn) {
        _confirBtn = [[UIButton alloc] init];
        _confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _confirBtn.userInteractionEnabled = NO;
        [_confirBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        _confirBtn.tag = 0;
        _confirBtn.cornerRadius = 3;
        _confirBtn.titleLabel.font = EighteenFontSize;
        [_confirBtn setTitle:kLocalizedString(@"safeConfirm") forState:UIControlStateNormal];
        [_confirBtn addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirBtn;
}

- (void)setTitleName:(NSString *)titleName {
    _titleName = titleName;
    self.safeTitle.text = titleName;
}

- (void)action:(UIButton *)sender {
    NSLog(@"确认");
    if (self.type == sheetTypeLogin) {
        NSDictionary *dic = @{@"type":@"google",
                              @"token":self.token,
                              @"code":self.msgTF.text,
        };
        [SBNetworTool postWithUrl:EastTwoAuth params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                [self closeAcitonSheet];
                self.block(@"");
            } else {
                [TipsView showTipOnKeyWindow:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
                [TipsView showTipOnKeyWindow:FailurMessage];
        }];
    } else if (self.type == sheetTypeBlindGoogle) {
        if (self.block) {
            self.block(self.msgTF.text);
            [self closeAcitonSheet];
        }
    } else if (self.type == sheetTypeModifyGoogle) {
        if (self.block) {
            self.block(self.msgTF.text);
            [self closeAcitonSheet];
        }
    }
}

- (UIButton *)copBtn {
    if (!_copBtn) {
        _copBtn = [[UIButton alloc] init];
        [_copBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        _copBtn.tag = 1;
        _copBtn.cornerRadius = 3;
        _copBtn.titleLabel.font = FourteenFontSize;
        _copBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_copBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        [_copBtn setTitle:kLocalizedString(@"safeCopy") forState:UIControlStateNormal];
        [_copBtn addTarget:self action:@selector(copyAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _copBtn;
}

- (void)copyAction {
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    self.msgTF.text = pab.string;
    self.confirBtn.userInteractionEnabled = YES;
    self.confirBtn.backgroundColor = ThemeGreenColor;
}


- (NSMutableArray *)data {
    if (!_data) {
        _data = [[NSMutableArray alloc] init];
    }
    return _data;
}


@end
