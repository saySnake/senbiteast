//
//  Message.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/23.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "Message.h"

@implementation Message

- (instancetype)init
{
    self = [super init];
    if (self) {
       
    }
    return self;
}
 
 
-(id)initWith:(NSString *)megFrom msgTo:(NSString *)msgTo msgContent:(NSString *)msgContent{
    
    self.msgFrom=megFrom;
    self.msgTo=msgTo;
    self.msgContent=msgContent;
    
    return self;
}
 
 
-(id)toDictionary{
    
//    NSDictionary *dir=[NSDictionary dictionaryWithObjectsAndKeys:self.msgFrom,@"msgFrom",self.msgTo,@"msgTo",self.msgContent,@"msgContent", nil];
    
//    return dir;
    return self.dic;
}

@end
