//
//  HisEntrustVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/14.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "HisEntrustVC.h"
#import "ComisCurrentCell.h"

@interface HisEntrustVC ()<UITableViewDelegate,UITableViewDataSource> {
    NSInteger _currentIdx;
}

@property (nonatomic, weak) UIScrollView *titleScroll;
@property (nonatomic, strong) NSMutableArray *titleButton;
@property (nonatomic, weak) UIButton *selectBtn;
@property (nonatomic, strong) UITableView *tbleView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation HisEntrustVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    self.navBar.backgroundColor = WhiteColor;
    [self setUpTitleScroll];
    [self setUpView];
}

- (void)setUpView {
    [self.view addSubview:self.tbleView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ComisCurrentCell *cell = [ComisCurrentCell cellWithTbaleView:tableView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSMutableArray *)titleButton {
    if (!_titleButton) {
        _titleButton = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"trade_currentEntrust"),kLocalizedString(@"trade_currentHisTrust"),kLocalizedString(@"trade_hisOrder"), nil];
    }
    return _titleButton;
}


- (void)setUpTitleScroll {
    UIScrollView *view = [[UIScrollView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, CGRectGetMaxY(self.navBar.frame), DScreenW, 50);
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 50 - kLinePixel, DScreenW, kLinePixel)];
    line.backgroundColor = HEXCOLOR(0xDDDDDD);
    [view addSubview:line];
    view.showsHorizontalScrollIndicator = NO;
    self.titleScroll = view;
    CGFloat btnW = 100;
    CGFloat btnH = 35;
    for (int i = 0; i < 3; ++i) {
        UIButton *btn = [[UIButton alloc] init];;
        btn.frame = CGRectMake(10 + btnW * i , 5, btnW, btnH);
        [btn setTitle:self.titleButton[i] forState:UIControlStateNormal];
        [btn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn addTarget:self action:@selector(topTitleBtnClick:) forControlEvents:UIControlEventTouchDown];
        btn.tag = i;
        [view addSubview:btn];
        if (btn.tag == 0) {
            _currentIdx = 0;
            [self topTitleBtnClick:btn];
        }
    }
    [self.view addSubview:view];
}

- (void)topTitleBtnClick:(UIButton *)btn {
    [self selctedBtn:btn];
    if (btn.tag == 0) {
        NSLog(@"充币历史");
        _currentIdx = 0;
        [self requestUrl:@"charge"];
    } else if (btn.tag == 1) {
        NSLog(@"提币历史");
        _currentIdx = 1;
        [self requestUrl:@"withdraw"];
    }
}

- (void)requestUrl:(NSString *)str {
    NSDictionary *dic = @{@"market":KTrade.coinTradeModel.symbolId,
                          @"type":@"current"
                        };
    [SBNetworTool getWithUrl:EastEntrustOrders params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.dataArr = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
            [self.tbleView reloadData];

        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];

}

- (void)selctedBtn:(UIButton *)btn {
    [_selectBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
    [btn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
    _selectBtn.transform = CGAffineTransformIdentity;
    btn.transform = CGAffineTransformMakeScale(1.3, 1.3);
    _selectBtn = btn;
}


- (UITableView *)tbleView {
    if (_tbleView == nil) {
        _tbleView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScroll.frame), kScreen_Width, kScreen_Height - CGRectGetMaxY(self.titleScroll.frame)) style:UITableViewStylePlain];
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.dataSource = self;
        _tbleView.delegate = self;
        _tbleView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 83 : 65)];
        _tbleView.estimatedRowHeight = 0;
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.estimatedSectionHeaderHeight = 0;
        _tbleView.estimatedSectionFooterHeight = 0;
        if (@available(iOS 11.0, *)) {
            _tbleView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tbleView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}


@end
