//
//  BaseField.h
//  BaseField
//
//  Created by 张玮 on 2020/8/10.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

//BaseField *userNameField = [BaseField creatField];
//userNameField.frame = CGRectMake(0, 60, self.view.frame.size.width, 80);
//userNameField.maxLength = 11;
//userNameField.errorStr = @"字数长度不得超过11位";
//userNameField.placeholder = @"请输入用户名";
//userNameField.historyContentKey = @"userName";
//[self.view addSubview:userNameField];
//
//BaseField *passwordField = [BaseField creatField];
//passwordField.frame = CGRectMake(0, 160, self.view.frame.size.width, 80);
//passwordField.maxLength = 6;
//passwordField.errorStr = @"密码长度不得超过6位";
//passwordField.placeholder = @"请输入密码";
//passwordField.historyContentKey = @"password";
//passwordField.leftImageName = @"password_login";
//passwordField.showHistoryList = NO;
//[self.view addSubview:passwordField];

@interface BaseField : UIView

/** 内容文本框 */
@property (strong, nonatomic) UITextField *textField;
/** 最大字数(必填) */
@property (nonatomic,assign) NSInteger maxLength;
/** 输入内容错误提示(必填) */
@property (nonatomic,strong) NSString *errorStr;
/** 占位文字(必填) */
@property (nonatomic,strong) NSString *placeholder;
/** 保存输入内容的KEY(必填) */
@property (nonatomic,strong) NSString *historyContentKey;
/** 文本字体大小(默认15.0) */
@property (nonatomic,assign) CGFloat textFont;
/** 文本颜色(默认DarkGray) */
@property (nonatomic,strong) UIColor *textColor;
/** 文本字体大小(默认15.0) */
@property (nonatomic,assign) CGFloat placeHolderFont;
/** 文本颜色(默认LightGray) */
@property (nonatomic,strong) UIColor *placeHolderColor;
/** 字数限制文本颜色(默认灰色) */
@property (nonatomic,strong) UIColor *textLengthLabelColor;
/** 上浮的占位文本颜色(默认深绿色) */
@property (nonatomic,strong) UIColor *placeHolderLabelColor;
/** 底部线条默认颜色(默认灰色) */
@property (nonatomic,strong) UIColor *lineDefaultColor;
/** 底部线条选中颜色(默认深绿色) */
@property (nonatomic,strong) UIColor *lineSelectedColor;
/** 底部线条错误警告颜色(默认红色) */
@property (nonatomic,strong) UIColor *lineWarningColor;
/** 是否需要显示历史记录列表(默认YES) */
@property (nonatomic,assign) BOOL showHistoryList;

/**
 初始化方法
 */
+ (instancetype)creatField;

/**
 上浮占位文字的显示与隐藏
 @param isHidden 是否隐藏
 */
- (void)setPlaceHolderLabelHidden:(BOOL)isHidden;

///抖动
- (void)shakeAnimationForView:(UIView *)view;
@end
