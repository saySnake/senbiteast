//
//  SeriveAlert.h
//  Senbit
//
//  Created by 张玮 on 2019/12/19.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, LAlertViewActionStyle) {
    LAlertViewActionStyleDefault = 0,
    LAlertViewActionStyleCancel,
    LAlertViewActionStyleDestructive
};

/** alertView的回调block */

typedef void (^CallBackBlock)(NSInteger btnIndex);

/** alertView的回调block */

typedef void (^TextFieldCallBackBlock)(NSString * text);


@interface SeriveAlert : NSObject

+ (void)showAlertViewWith:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message
CallBackBlock:(CallBackBlock)textBlock cancelButtonTitle:(NSString *)cancelBtnTitle
    destructiveButtonTitle:(NSString *)destructiveBtnTitle
        otherButtonTitles:(NSString *)otherBtnTitles,...NS_REQUIRES_NIL_TERMINATION;

@end

NS_ASSUME_NONNULL_END
