//
//  LoginViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/12.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "LoginViewController.h"
#import "MoudleHome.h"
#import "AsyncTaskButton.h"
#import <WebKit/WebKit.h>
#import <GT3Captcha/GT3Captcha.h>
#import "TipsView.h"
#import "FindPasswordVC.h"
#import "PhoneViewController.h"
#import "GestureScreen.h"
#import "GestureViewController.h"
#import "GestureIntrdouceViewController.h"
#import "CPCountryTableViewController.h"
#import "CPCountry.h"
#import "LoginSheet.h"
#import "EmailLoginVC.h"
#import "NSAttributedString+AttributedString.h"


@interface LoginViewController ()<AsyncTaskCaptchaButtonDelegate,GestureViewDelegate,GestureScreenDelegate,UITextFieldDelegate>

@property (nonatomic ,strong) BaseButton *cancelBtn;
@property (nonatomic ,strong) BaseButton *registBtn;
@property (nonatomic ,strong) BaseLabel *logoTitle;
@property (nonatomic ,strong) BaseLabel *contentLb;
@property (nonatomic ,strong) FSCustomButton *countryBtn;
@property (nonatomic ,strong) UITextField *accountTF;
@property (nonatomic ,strong) BaseLabel *line1;
@property (nonatomic ,strong) UITextField *contenTF;
@property (nonatomic ,strong) BaseLabel *line2;
/** 登录按钮 */
@property (nonatomic ,strong) AsyncTaskButton *loginBtn;
@property (nonatomic ,strong) BaseButton *forgetBtn;

@property (nonatomic ,strong) BaseButton *emailBtn;
@property (nonatomic ,strong) UIButton *scrBtn;


/** 地区简称 **/
@property (nonatomic ,strong) NSString *locoal;

@end

@implementation LoginViewController
- (void)dealloc {
    NSLog(@"%@ dealloc resumed",NSStringFromClass([self class]));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setUpView];
    [self make_Layout];
    [self configKeyBoardRespond];
}


#pragma mark - InitAppreaence
- (void)setUpView {
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.registBtn];
    [self.view addSubview:self.logoTitle];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.countryBtn];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.line1];
    [self.view addSubview:self.contenTF];
    [self.view addSubview:self.scrBtn];
    [self.view addSubview:self.line2];
    [self.view addSubview:self.forgetBtn];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.emailBtn];
}

- (void)make_Layout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.height.mas_equalTo(40);
        make.width.mas_lessThanOrEqualTo(140);
    }];

    [self.registBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(40);
    }];

    [self.logoTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(136);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(140);
    }];

//    [self fuwenbenLabel:self.logoTitle FontNumber:[UIFont systemFontOfSize:33] AndRange:NSMakeRange(3, 3) AndColor:ThemeGreenColor];

    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.logoTitle.mas_bottom).offset(27);
        make.width.mas_equalTo(280);
        make.height.mas_equalTo(39);
    }];

    [self.countryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(50);
        make.width.mas_lessThanOrEqualTo(50);
        make.height.mas_equalTo(50);
    }];

    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.countryBtn.mas_right).offset(5);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(65);
    }];

    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(5);
        make.height.mas_equalTo(kLinePixel);
    }];

    [self.contenTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.line1.mas_bottom).offset(50);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(20);
    }];
    
    [self.scrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.contenTF.mas_top);
    }];


    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(self.contenTF.mas_bottom).offset(5);
    }];

    [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.line2.mas_bottom).offset(15);
        make.height.mas_equalTo(19);
        make.width.mas_lessThanOrEqualTo(120);
    }];

    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.line2.mas_bottom).offset(60);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(53);
    }];

    [self.emailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.loginBtn.mas_bottom).offset(25);
        make.width.mas_lessThanOrEqualTo(140);
        make.height.mas_equalTo(15);
    }];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak LoginViewController *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.contenTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}


- (void)fuwenbenLabel:(UILabel *)labell FontNumber:(id)font AndRange:(NSRange)range AndColor:(UIColor *)vaColor {
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:labell.text];
    //设置字号
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:33] range:range];
    //设置文字颜色
    [str addAttribute:NSForegroundColorAttributeName value:vaColor range:range];
    
    labell.attributedText = str;
}

//demo
- (void)captchaWillDisplay {
    [TipsView removeAllTipsView];
}

- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
    if (self.accountTF.text.length == 0 ) {
        [self showToastView:kLocalizedString(@"login_pleaseAccount")];
        return NO;
    }
    if (self.contenTF.text.length == 0) {
        [self showToastView:kLocalizedString(@"login_pleasePwd")];
        return NO;
    }
    
    [TipsView showTipOnKeyWindow:kLocalizedString(@"login_safe")];
    return YES;
}

#pragma mark - GestureScreenDelegate
- (void)dismMisVC {
//    [self dismissViewControllerAnimated:NO completion:^{
//        NSLog(@"2");
//        if (self.finishBlock) {
//            self.finishBlock();
//        }
//    }];
    if (self.finishBlock) {
        self.finishBlock();
    }
}

- (void)requestUserinfo {
    NSDictionary *dic = @{};
    [SBNetworTool getWithUrl:EastLoginInfo params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
            info.email = responseObject[@"data"][@"email"];
            info.emailAuthEnable = [responseObject[@"data"][@"emailAuthEnable"] boolValue];
            info.googleAuthEnable = [responseObject[@"data"][@"googleAuthEnable"] boolValue];
            info.lastLoginAt = responseObject[@"data"][@"lastLoginAt"];
            info.lastLoginIP = responseObject[@"data"][@"lastLoginIP"];
            info.nationality = responseObject[@"data"][@"nationality"];
            info.phone = responseObject[@"data"][@"phone"];
            info.phoneAuthEnable = [responseObject[@"data"][@"phoneAuthEnable"] boolValue];
            info.realName = [responseObject[@"data"][@"realName"] boolValue];
            info.strictFace = [responseObject[@"data"][@"strictFace"] boolValue];
            info.strictRealName = [responseObject[@"data"][@"strictRealName"] boolValue];
            info.strictRealNameStatus = responseObject[@"data"][@"strictRealNameStatus"];
            info.tradePassword = [responseObject[@"data"][@"tradePassword"] boolValue];
            info.uid = responseObject[@"data"][@"uid"];
            info.inviteCode = responseObject[@"data"][@"inviteCode"];
            [[CacheManager sharedMnager] saveUserInfo:info];
            [IWNotificationCenter postNotificationName:KLoadLeftView object:nil];

        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    [self.view endEditing:YES];
    if (status == NO) {
        
    }
    
    if (status == YES) {
        UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
        NSDictionary *dic;
        if (user.geetest_challenge && user.geetest_seccode && user.geetest_validate) {
            dic = @{@"account":[NSString stringWithFormat:@"%@%@",self.countryBtn.titleLabel.text,self.accountTF.text],
                    @"password":self.contenTF.text,
                    @"challenge":user.geetest_challenge,
                    @"seccode":user.geetest_seccode,
                    @"validate":user.geetest_validate
            };
            
            typeof(self) __weak weakSelf = self;

            [SBNetworTool postWithUrl:EastFirstLogin params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                NSLog(@"%@",responseObject);
                typeof(self) __strong strongSelf = weakSelf;
                if (SuccessCode == 200) {
                    [NotificationManager postLoginInSuccessNotification];
                    if (responseObject[@"data"][@"googleAuthEnable"]) {
                        LoginSheet * shet = [[LoginSheet alloc] init];
                        shet.token = responseObject[@"data"][@"token"];
                        shet.type = sheetTypeLogin;
                        [shet showFromView:strongSelf.view];
                        shet.block = ^(NSString * _Nonnull str) {
                            user.login = YES;
                            [[CacheManager sharedMnager] saveUserInfo:user];
                            [strongSelf requestUserinfo];
                            if (strongSelf.finishBlock) {
                                strongSelf.finishBlock();
                            }
                        };
                    } else {
                        user.login = YES;
                        [[CacheManager sharedMnager] saveUserInfo:user];
                        [strongSelf requestUserinfo];
                        if (strongSelf.finishBlock) {
                            strongSelf.finishBlock();
                        }
                    }
                } else {
                    [strongSelf showToastView:SuccessMessage];
                }
            } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
                [self showToastView:FailurMessage];
            }];
//            [SBNetworTool postWithUrl:EastFirstLogin params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//                NSLog(@"%@",responseObject);
//                            
//                } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//                    [self showToastView:FailurMessage];
//
//            }];
        }
    }
    
    
/**
 
 if (!error) {
     // 演示中全部默认为成功, 不对返回做判断
     
     dispatch_async(dispatch_get_main_queue(), ^{
         BOOL delay = [GestureTool isDelayEnable];
         if (delay) {
             if (self.finishBlock) {
                 self.finishBlock();
             }
         }

         if ([GestureTool isGestureEnable]) {
             
             [[GestureScreen shared] show];
             [GestureScreen shared].delegate = self;
             
         } else {
             GestureIntrdouceViewController * gesture = [[GestureIntrdouceViewController alloc] init];
//                gesture.delegate = self;
//                [gesture showInViewController:self type:LZGestureTypeSetting];
             gesture.cancleBlock = ^{
                 if (self.finishBlock) {
                     self.finishBlock();
                 }
             };
             
             gesture.finishBlock = ^{
                 if (self.finishBlock) {
                     self.finishBlock();
                 }
             };
             [self.navigationController pushViewController:gesture animated:NO];
         }
     });
 }
 else {
     //处理验证中返回的错误
     if (error.code == -999) {
         // 请求被意外中断,一般由用户进行取消操作导致
     }
     else if (error.code == -10) {
         // 预判断时被封禁, 不会再进行图形验证
     }
     else if (error.code == -20) {
         // 尝试过多
     }
     else {
         // 网络问题或解析失败, 更多错误码参考开发文档
     }
     [TipsView showTipOnKeyWindow:error.error_code fontSize:12.0];
 }
 */
}




//- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
//    if (_currentIndex == 0) {
//        if (self.phoneField.text.length == 0) {
//            [self showToastView:kLocalizedString(@"blindPhone_phonePlace")];
//            return NO;
//        }
//        if (self.passwordTF.text.length == 0) {
//            [self showToastView:kLocalizedString(@"toastNotPwd")];
//            return NO;
//        }
//    } else {
//        if (self.accountTF.text.length == 0) {
//            [self showToastView:kLocalizedString(@"toastNotEmail")];
//            return NO;
//        }
//        if (self.passwordTF.text.length == 0) {
//            [self showToastView:kLocalizedString(@"toastNotPwd")];
//            return NO;
//        }
//        [TipsView showTipOnKeyWindow:@"正在检测\n智能账号安全..."];
//    }
//    return YES;
//}

//- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
//    [self.view endEditing:YES];
//    if (status == NO) {
//
//    }
//
//    if (error) {
//        if (error.code == -999) {
//            // 请求被意外中断,一般由用户进行取消操作导致
//        }
//        else if (error.code == -10) {
//            // 预判断时被封禁, 不会再进行图形验证
//        }
//        else if (error.code == -20) {
//            // 尝试过多
//        }
//        else {
//            // 网络问题或解析失败, 更多错误码参考开发文档
//        }
//        [TipsView showTipOnKeyWindow:error.error_code fontSize:12.0];
//    }
//
//    if (status == YES) {
//        if (_currentIndex == 1) {
//            if (self.accountTF.text.length == 0) {
//                   [self showToastView:kLocalizedString(@"toastNotPhoneOrEmail")];
//                   return;
//               }
//               if (self.passwordTF.text.length == 0) {
//                   [self showToastView:kLocalizedString(@"toastNotLoginPwd")];
//                   return;
//               }
//        }
//    }
//
//    if (status == YES) {
//        NSMutableArray *arr = [[NSMutableArray alloc] init];
//        UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
//        if (user.geetest_challenge && user.geetest_seccode && user.geetest_validate) {
//
//            NSDictionary *dic;
//            if (_currentIndex == 1) {
//                dic = @{@"challenge":user.geetest_challenge,
//                        @"seccode":user.geetest_seccode,
//                        @"validate":user.geetest_validate,
//                        @"account":self.accountTF.text,
//                        @"password":self.passwordTF.text};
//            } else {
//                dic = @{@"challenge":user.geetest_challenge,
//                        @"seccode":user.geetest_seccode,
//                        @"validate":user.geetest_validate,
//                        @"account":[NSString stringWithFormat:@"%@%@",self.phoneBtn.currentTitle,self.phoneField.text],
//                        @"password":self.passwordTF.text};
//            }
//            kWeakSelf(self);
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                            [SVProgressHUD show];
//            });
//
//            [SBNetworTool postWithUrl:SenLoginTwouser params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//
//                });
//
//                if ([responseObject[@"login"] boolValue] == YES) {
//                    [self dismissViewControllerAnimated:NO completion:^{
//                        user.login = YES;
//                        [[CacheManager sharedMnager] saveUserInfo:user];
//                        [self pingLogin];
//                    }];
//                } else {
//                    NSLog(@"%@",responseObject[@""])
//                  
//                    if ([responseObject[@"phone"] isKindOfClass:[NSArray class]]) {
//                          NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                          [dict setValue:responseObject[@"phone"] forKey:@"phone"];
//                          [arr addObject:dict];
//                    }
//                    
//                    if (responseObject[@"email"]) {
//                        NSString *st = [NSString stringWithFormat:@"%@",responseObject[@"email"]];
//                        if ([responseObject[@"email"] intValue] == 0 && st.length == 1) {
//                            NSLog(@"11111");
//                        } else {
//                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                            [dict setValue:responseObject[@"email"] forKey:@"email"];
//                            [arr addObject:dict];
//                        }
//                    }
//                    if (responseObject[@"google"]) {
//                        if ([responseObject[@"google"] intValue] == 0) {
//                            
//                        } else {
//                            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                            [dict setValue:responseObject[@"google"] forKey:@"google"];
//                            [arr addObject:dict];
//                        }
//                    }
//                    user.token = responseObject[@"token"];
//                    [[CacheManager sharedMnager] saveUserInfo:user];
//                    LoginSheet *shet = [[LoginSheet alloc] init];
//                    shet.dataArray = arr;
//                    [shet showFromView:self.view];
//                    kStrongSelf(self);
//                    shet.clickBlock = ^(NSInteger index) {
//                        if (index == 0) {
//                            user.login = YES;
//                            [[CacheManager sharedMnager] saveUserInfo:user];
//                            [self dismissViewControllerAnimated:NO completion:^{
//                                [IWDefault removeObjectForKey:Cookie];
//                                [self pingLogin];
//                            }];
//                        }
//                    };
//                }
//            } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                });
//                [self showToastView:FailurMessage];
//            }];
//        }
//    }
//}





#pragma mark - Event Response
- (void)backAction {
//    if (self.interface.callbackBlock) {
//        self.interface.callbackBlock(@"callBack paramter");
//    }
    if (self.cancleBlock) {
        self.cancleBlock();
    }
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (void)forgetAction {
    FindPasswordVC *find = [[FindPasswordVC alloc] init];
    [self.navigationController pushViewController:find animated:YES];
}

- (void)regiserAction {
    PhoneViewController *phone = [[PhoneViewController alloc] init];
    [self.navigationController pushViewController:phone animated:YES];
}


#pragma mark - lazy
- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = kFont(16);
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        _cancelBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseButton *)registBtn {
    if (!_registBtn) {
        _registBtn = [[BaseButton alloc] init];
        _registBtn.colorName = @"login_loginBtnCor";
        _registBtn.backColorName = @"login_registerBackground";
        [_registBtn setTitle:kLocalizedString(@"login_register") forState:UIControlStateNormal];
        _registBtn.titleLabel.font = FourteenFontSize;
        _registBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _registBtn.titleLabel.font = BOLDSYSTEMFONT(15);
        [_registBtn addTarget:self action:@selector(regiserAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registBtn;
}

- (BaseLabel *)logoTitle{
    if (!_logoTitle) {
        _logoTitle = [[BaseLabel alloc] init];
        _logoTitle.textColor = HEXCOLOR(0x000000);
        _logoTitle.font = BOLDSYSTEMFONT(33);
        _logoTitle.textAlignment = 0;
        _logoTitle.text = kLocalizedString(@"login_logoTitle");
    }
    return _logoTitle;
}

- (BaseLabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[BaseLabel alloc] init];
        _contentLb.colorName = @"login_conentColor";
        _contentLb.textAlignment = 0;
        _contentLb.font = FourteenFontSize;
        _contentLb.text = kLocalizedString(@"login_content");
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}

- (UITextField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[UITextField alloc] init];
        _accountTF.textColor = FieldTextColor;
        _accountTF.textAlignment = 0;
        _accountTF.font = FifteenFontSize;
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"login_tfPlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _accountTF.attributedPlaceholder = attrString;
        _accountTF.adjustsFontSizeToFitWidth = YES;
        _accountTF.delegate = self;
        [_accountTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (BaseLabel *)line1 {
    if (!_line1) {
        _line1 = [[BaseLabel alloc] init];
        _line1.backColor = @"login_lineColor";
    }
    return _line1;
}

- (BaseLabel *)line2 {
    if (!_line2) {
        _line2 = [[BaseLabel alloc] init];
        _line2.backColor = @"login_lineColor";
    }
    return _line2;
}

- (UITextField *)contenTF {
    if (!_contenTF) {
        _contenTF = [[UITextField alloc] init];
        _contenTF.textColor = FieldTextColor;
        _contenTF.textAlignment = 0;
        _contenTF.placeholder = kLocalizedString(@"login_contPlace");
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"login_contPlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _contenTF.attributedPlaceholder = attrString;
        _contenTF.font = FifteenFontSize;
        _contenTF.adjustsFontSizeToFitWidth = YES;
        _contenTF.clearsOnBeginEditing = YES;
        _contenTF.secureTextEntry = YES;
        _contenTF.delegate = self;
        [_contenTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _contenTF;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [AsyncTaskButton buttonWithType:UIButtonTypeCustom];
        _loginBtn.cornerRadius = 8;
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        _loginBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        [_loginBtn setTitle:kLocalizedString(@"login_loginBtn") forState:UIControlStateNormal];
        _loginBtn.userInteractionEnabled = NO;
        _loginBtn.delegate = self;
    }
    return _loginBtn;
}


- (BaseButton *)forgetBtn {
    if (!_forgetBtn) {
        _forgetBtn = [[BaseButton alloc] init];
        _forgetBtn.colorName = @"login_loginBtnCor";
        _forgetBtn.titleLabel.font = kFont14;
        _forgetBtn.titleLabel.textAlignment = 1;
        [_forgetBtn setTitle:kLocalizedString(@"login_loginForget") forState:UIControlStateNormal];
        [_forgetBtn addTarget:self action:@selector(forgetAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetBtn;
}

- (BaseButton *)emailBtn {
    if (!_emailBtn) {
        _emailBtn = [[BaseButton alloc] init];
        _emailBtn.colorName = @"login_loginBtnCor";
        _emailBtn.titleLabel.font = kFont14;
        _emailBtn.titleLabel.textAlignment = 1;
        [_emailBtn setTitle:kLocalizedString(@"login_changeEm") forState:UIControlStateNormal];
        [_emailBtn addTarget:self action:@selector(emailLogin) forControlEvents:UIControlEventTouchUpInside];
    }
    return _emailBtn;
}

- (void)emailLogin {
    EmailLoginVC *email = [[EmailLoginVC alloc] init];
    email.emailLoginBlock = ^{
        if (self.finishBlock) {
            [NotificationManager postLoginInSuccessNotification];
            self.finishBlock();
        }
    };
    [self.navigationController pushViewController:email animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super hideKeyboard];
}


- (UIButton *)scrBtn {
    if (!_scrBtn) {
        _scrBtn = [[UIButton alloc] init];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn addTarget:self action:@selector(scrr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn;
}


- (void)scrr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.contenTF.secureTextEntry = NO;
    } else {
        self.contenTF.secureTextEntry = YES;
    }
}



- (FSCustomButton *)countryBtn {
    if (!_countryBtn) {
        _countryBtn = [[FSCustomButton alloc] init];
        _countryBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        [_countryBtn setImage:[UIImage imageNamed:@"icon_Triangles"] forState:UIControlStateNormal];
        _countryBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _countryBtn.backgroundColor = [UIColor clearColor];
        _countryBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_countryBtn setTitleColor:ThemeTextColor forState:UIControlStateNormal];
        _countryBtn.contentHorizontalAlignment = 1;
        _countryBtn.userInteractionEnabled = YES;
        [_countryBtn setTitle:@"--" forState:UIControlStateNormal];
        [_countryBtn addTarget:self action:@selector(countryAction) forControlEvents:UIControlEventTouchUpInside];
        _countryBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _countryBtn;
}

- (void)countryAction {
    CPCountryTableViewController *cp = [[CPCountryTableViewController alloc] init];
    cp.callback = ^(CPCountry *country) {
        [self.countryBtn setTitle:[NSString stringWithFormat:@"+%d", country.code] forState:UIControlStateNormal];
        self.locoal = country.locale;
    };
    [self.navigationController pushViewController:cp animated:YES];
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountTF.text.length >0 && self.contenTF.text.length >0) {
        self.loginBtn.userInteractionEnabled = YES;
        self.loginBtn.backgroundColor = ThemeGreenColor;
    } else {
        self.loginBtn.userInteractionEnabled = NO;
        self.loginBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
    }
}

@end
