//
//  SBNetworTool.h
//  Senbit
//
//  Created by 张玮 on 2019/12/23.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import <AFHTTPSessionManager.h>



NS_ASSUME_NONNULL_BEGIN

#define DRLINK_AUTH_TOKEN @"Cookie"
#define DRLINKCUBE_TOKEN @"cube-authorization"

/***
 //    //get请求
 //    NSDictionary * params = @{@"key":@"bfb12777f7e63ffcfb88d2dfb9d529c1",@"ranks":@1};
 //    [JZLNetworkingTool getWithUrl:@"https://kyfw.12306.cn/otn" params:nil isReadCache:YES success:^(NSURLSessionDataTask *task, id responseObject) {
 //        NSLog(@"%@",responseObject);
 //    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
 //        NSLog(@"%@",error);
 //        NSLog(@"%@",responseObject);
 //    }];
 //
     
 //    //post请求
 //    NSDictionary * params = @{@"AppKey":@"16125033",@"AppSecurity":@"583d1cfe5234e89ea4000001"};
 //    [JZLNetworkingTool postWithUrl:@"https://www.corporatetravel.ctrip.com/corpservice/authorize/ticket" params:params isReadCache:YES success:^(NSURLSessionDataTask *task, id responseObject) {
 //        NSLog(@"%@",responseObject);
 //    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
 //                NSLog(@"%@",error);
 //                NSLog(@"%@",responseObject);
 //    }];
     
//     //上传
 //    NSString *path = [[NSBundle mainBundle] pathForResource:@"WE.jpg" ofType:nil];
 //    NSData *data = [NSData dataWithContentsOfFile:path];
 //    [JZLNetworkingTool uploadWithUrl:@"http://localhost" params:nil fileData:data name:@"" fileName:@"WE.jpg" mimeType:@"image/jpeg" progress:^(NSProgress *progress) {
 //
 //    } success:^(NSURLSessionDataTask *task, id responseObject) {
 //        NSLog(@"%@",responseObject);
 //    } failed:^(NSURLSessionDataTask *task, NSError *error) {
 //        NSLog(@"%@",error);
 //    }];
     
 //    //下载
 //    NSString *url = @"http://localhost/test.zip";
 //    [JZLNetworkingTool downloadWithUrl:url];

 //暂停
 - (void)pauseDownload:(id)sender {
     [[JZLNetworkingTool sharedManager] pauseDownload];
 }
 
 //继续下载
 - (void)resumeDownload:(id)sender {
     [[JZLNetworkingTool sharedManager] resumeDownloadprogress:^(NSProgress *progress) {
         
     } success:^(NSURLResponse *response, NSURL *filePath) {
         
     } failed:^(NSError *error) {
         
     }];
 }
 ***/

/**
 *  是否开启https SSL 验证
 *
 *  @return YES为开启，NO为关闭
 */
#define openHttpsSSL NO
/**
 *  SSL 证书名称，仅支持cer格式。“app.bishe.com.cer”,则填“app.bishe.com”
 */
#define certificate @"xiaokaapi.com"
#define Agent @"User-Agent"

//请求成功的回调block
typedef void(^responseSuccess)(NSURLSessionDataTask *task, id  responseObject);

//请求失败的回调block
typedef void(^responseFailed)(NSURLSessionDataTask *task, id error,id responseObject);

//文件下载的成功回调block
typedef void(^downloadSuccess)(NSURLResponse *response, NSURL *filePath);

//文件下载的失败回调block
typedef void(^downloadFailed)( NSError *error);

//文件上传下载的进度block
typedef void (^progress)(NSProgress *progress);

@interface SBNetworTool : AFHTTPSessionManager<NSURLSessionDownloadDelegate>

@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;

//已经下载的数据
@property (nonatomic, strong) NSData *resumeData;

//续传文件沙盒存储路径
@property (nonatomic, copy) NSString *resumeDataPath;

// 自定义session,设置代理
@property (nonatomic, strong) NSURLSession *downloadSession;

/**
 管理者单例
 */
+ (instancetype)sharedManager;
+ (void)postObjectWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(id error))failure;
+ (void)putWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(id error))failure;
+ (void)patchWithUrl:(NSString *)url params: (NSDictionary *)params isReadCache: (BOOL)isReadCache success: (responseSuccess)success failed: (responseFailed)failed;
/**
 get请求

 @param url 请求url
 @param params 参数
 @param isReadCache 是否读取缓存
 @param success 成功回调
 @param failed 失败回调
 */
+ (void)getWithUrl: (NSString *)url params: (NSDictionary *)params isReadCache: (BOOL)isReadCache success: (responseSuccess)success failed: (responseFailed)failed;


/**
 post请求不解析 原生返回
 **/
+ (void)postNojsonWithUrl:(NSString *)url params:(NSDictionary *)params isReadCache: (BOOL)isReadCache success:(responseSuccess)success failed:(responseFailed)failed;
/**
post请求

@param url 请求url
@param params 参数
@param isReadCache 是否读取缓存
@param success 成功回调
@param failed 失败回调
*/
+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params isReadCache: (BOOL)isReadCache success:(responseSuccess)success failed:(responseFailed)failed ;

/*
 DELETE 请求
 
 */
+ (void)deleteWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(id error))failure;

/**
 文件上传(图片上传)

 @param url 请求url
 @param params 请求参数
 @param fileData 上传文件的二进制数据
 @param name 制定参数名
 @param fileName 文件名(加后缀名)
 @param mimeType 文件类型
 @param progress 上传进度
 @param success 成功回调
 @param failed 失败回调
 */
+ (void)uploadWithUrl: (NSString *)url params: (NSDictionary *)params fileData: (NSData *)fileData name: (NSString *)name fileName: (NSString *)fileName mimeType: (NSString *)mimeType progress: (progress)progress success: (responseSuccess)success failed: (responseFailed)failed;



//文件下载
+ (void)downloadWithUrl: (NSString *)url;


/**
 暂停下载
 */
- (void)pauseDownload;


/**
 继续下载(断点下载)

 @param progress 下载进度
 @param success 成功回调
 @param failed 失败回调
 */
- (void)resumeDownloadprogress: (progress)progress success: (downloadSuccess)success failed: (downloadFailed)failed ;


@end

NS_ASSUME_NONNULL_END
