//
//  BalanceCoinsModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/2.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CoinPairModel;
@interface BalanceCoinsModel : NSObject


@property (nonatomic ,strong) NSString *able2charge;
@property (nonatomic ,strong) NSString *able2withdraw;
@property (nonatomic ,strong) NSString *coinIcon;
@property (nonatomic ,strong) NSString *enFullName;
@property (nonatomic ,strong) NSString *ID;
@property (nonatomic ,strong) NSString *isOTCPartition;
@property (nonatomic ,strong) CoinPairModel *coinPair;
@property (nonatomic ,strong) NSString *balance;
@property (nonatomic ,strong) NSString *locked;
@property (nonatomic ,strong) NSString *freezed;
@property (nonatomic ,strong) NSString *usdtRate;

@end

NS_ASSUME_NONNULL_END
