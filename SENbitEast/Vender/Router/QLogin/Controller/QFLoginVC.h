//
//  QFLoginVC.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/19.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"
@class MoudleLogin;
typedef void (^loginSuccessblock)();
typedef void (^loginCancleBlock)();


NS_ASSUME_NONNULL_BEGIN

@interface QFLoginVC : BaseViewController

/// 外部接口
@property (nonatomic, strong) MoudleLogin *interface;


@property (nonatomic ,copy)loginSuccessblock finishBlock;
@property (nonatomic ,copy)loginCancleBlock cancleBlock;


@end

NS_ASSUME_NONNULL_END
