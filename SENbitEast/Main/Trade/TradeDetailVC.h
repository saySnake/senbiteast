//
//  TradeDetailVC.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/14.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseViewController.h"
#import "TradeListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TradeDetailVC : BaseViewController

@property (nonatomic ,strong) TradeListModel *model;

@end

NS_ASSUME_NONNULL_END
