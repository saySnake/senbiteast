//
//  UITableView+LKCategory.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "UITableView+LKCategory.h"

@implementation UITableView (LKCategory)

- (void)showNoDataCount:(NSInteger)count image:(NSString *)name {
    if (count > 0) {
        self.backgroundView = nil;
        return;
    }
    
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.height)];
    UIImageView *showImageView = [[UIImageView alloc]init];
    showImageView.contentMode = UIViewContentModeScaleAspectFill;
    [backgroundView addSubview:showImageView];
    
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.textColor = MainWhiteColor;
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [backgroundView addSubview:tipLabel];
    
    if (name.length) {
        showImageView.image = [UIImage imageNamed:name];
    } else {
        showImageView.image = [UIImage imageNamed:@"icon_Norecord"];
    }
    tipLabel.text = kLocalizedString(@"noListRecord");
 
    [showImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(backgroundView.mas_centerX);
        make.centerY.mas_equalTo(backgroundView.mas_centerY).mas_offset(-20);
    }];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(backgroundView.mas_centerX);
        make.top.mas_equalTo(showImageView.mas_bottom).mas_offset(30);
    }];
    self.backgroundView = backgroundView;
}

@end
