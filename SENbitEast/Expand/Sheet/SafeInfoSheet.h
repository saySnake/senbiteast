//
//  SafeInfoSheet.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/24.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeBtn.h"

NS_ASSUME_NONNULL_BEGIN


//typedef enum : NSUInteger {
//    SafeInfoTypeEmail, //找回密码邮箱
//    SafeInfoTypePhone, //找回密码手机
//} SafeInfoType;


typedef void (^ClickBlocks) (NSString *phoneCode ,
                            NSString *emailCode ,
                            NSString *googleCode,
                            NSString *phone,
                            NSString *email,
                            NSString *userToken);

@interface SafeInfoSheet : UIView
//@property (nonatomic, assign) SafeInfoType type;

@property (nonatomic, copy) ClickBlocks clickBlock;
@property (nonatomic ,strong) UILabel *safeTitle;
@property (nonatomic ,strong) UIButton *canceBtn;
@property (nonatomic ,strong) UILabel *line1;

@property (nonatomic ,strong) UILabel *phoneLabel;
@property (nonatomic ,strong) UITextField *msgTF1;
@property (nonatomic ,strong) TimeBtn *msgBtn;
@property (nonatomic ,strong) UILabel *phoneLine;

//邮箱名字
@property (nonatomic ,strong) UILabel *emailTitleName;
@property (nonatomic ,strong) UITextField *msgTF2;
@property (nonatomic ,strong) TimeBtn *msgBtn2;
@property (nonatomic ,strong) UILabel *emailLine;

@property (nonatomic ,strong) UILabel *googleTitle;
@property (nonatomic ,strong) UITextField *msgTF3;
@property (nonatomic ,strong) TimeBtn *msgBtn3;
@property (nonatomic ,strong) UILabel *googleLine;

//区号
@property (nonatomic ,strong) NSString *countryCode;

//区号
@property (nonatomic ,strong) NSString *country;

//locoal
@property (nonatomic ,strong) NSString *locoal;

//找回密码code
@property (nonatomic ,strong) NSString *code;

@property (nonatomic ,assign) BOOL googleAuth;

@property (nonatomic ,assign) BOOL phoneAuth;

@property (nonatomic ,assign) BOOL emailAuth;

@property (nonatomic ,strong) UIButton *confirBtn;

@property (nonatomic ,strong) NSString *account;

@property (nonatomic ,strong) NSString *userToken;


- (void)showFromView:(UIView *)view;

+ (instancetype)customActionSheets;

- (void)closeAcitonSheet;

@end

NS_ASSUME_NONNULL_END
