//
//  NSString+JsonData.h
//  Senbit
//
//  Created by 张玮 on 2020/1/18.
//  Copyright © 2020 zhangwei. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (JsonData)
+ (NSString *)convertToJsonData:(NSDictionary *)dict;
//获取当前时间戳 （以毫秒为单位）
+ (NSString *)getNowTimeTimestamp;
//字典转json字符串
+ (NSString *)dictionaryToJSONString:(NSDictionary *)dictionary;
//数组转json字符串
+ (NSString *)arrayToJSONString:(NSArray *)array;
//将时间转化成 时间戳
//返回值格式:1555642454396
+ (NSInteger)timeSwitchTimestamp:(NSString *)formatTime;

@end

NS_ASSUME_NONNULL_END
