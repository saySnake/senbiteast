//
//  ComisCurrentCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "ComisCurrentCell.h"

@implementation ComisCurrentCell

+ (instancetype)cellWithTbaleView:(UITableView *)tableView {
    static NSString *ID = @"ComisCurrentCell";
    ComisCurrentCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[ComisCurrentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.cornerRadius = 5;
    }
    return cell;
}



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        self.backgroundColor = WhiteColor;
        [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
        [self setUpView];
    }
    return self;
}

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (void)changeLanguage {
    self.timeLb.text = kLocalizedString(@"trade_timeTitle");
    self.entrustPrice.text = kLocalizedString(@"trade_entrustPrice");
    self.entrustNum.text = kLocalizedString(@"trade_entrustNum");
    self.orderNum.text = kLocalizedString(@"trade_orderNum");
    self.orderTotal.text = kLocalizedString(@"trade_totalAmount");
    self.orderAverage.text = kLocalizedString(@"trade_type");
    [self.finBtn setTitle:kLocalizedString(@"trade_done") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:kLocalizedString(@"trade_cancel") forState:UIControlStateNormal];
}

+ (CGFloat)ComisCurrentCellHeight {
    return 220;
}

- (void)setModel:(TradeListModel *)model {
    if (model) {
        _model = model;
        
        NSRange range;
        range = [model.market rangeOfString:@"/"];
//        NSMutableAttributedString *strs = [[NSMutableAttributedString alloc] initWithString:model.market];
//        [strs addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"] range:NSMakeRange(0, range.location)];
        NSString * string = [model.market substringToIndex:range.location];
        NSString * s = [model.market substringFromIndex:range.location + 1];
        self.entrustPrice.text = [NSString stringWithFormat:@"%@(%@)",kLocalizedString(@"trade_entrustPrice"),s];
        self.entrustNum.text = [NSString stringWithFormat:@"%@(%@)",kLocalizedString(@"trade_entrustNum"),string];
        self.orderTotal.text = [NSString stringWithFormat:@"%@(%@)",kLocalizedString(@"trade_totalAmount"),s];
        self.orderNum.text = [NSString stringWithFormat:@"%@(%@)",kLocalizedString(@"trade_orderNum"),string];

        if ([model.tradeType isEqualToString:@"buy"]) {
            self.direct.textColor = ThemeGreenColor;
            self.direct.text = kLocalizedString(@"trade_buy");
        } else {
            self.direct.textColor = ThemeRedColor;
            self.direct.text = kLocalizedString(@"trade_sellOut");
        }
        self.timeText.text = model.createdAt;
        self.entruPriceText.text = model.price;
        self.entruNumText.text = model.volume;
        self.coin.text = model.market;
        NSString *a = [NSString subV1:model.originVolume v2:model.volume];
        self.orderTotalText.text = [NSString mulV1:model.averagePrice v2:a];
        self.orderAverageText.text = model.orderType;
        self.orderNumText.text = [NSString subV1:model.originVolume v2:model.volume];
    }
    if (self.currentIndex == 0) {
        self.finBtn.hidden = YES;
        self.cancelBtn.hidden = NO;
    } else {
        self.finBtn.hidden = NO;
        self.cancelBtn.hidden = YES;
        [self.finBtn setTitle:model.state forState:UIControlStateNormal];
    }
}



- (void)setUpView {
    [self.contentView addSubview:self.direct];
    [self.direct mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(5);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(25);
    }];
    
    [self.contentView addSubview:self.coin];
    [self.coin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.direct.mas_right);
        make.top.mas_equalTo(self.direct);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(self.direct);
    }];
    
    [self.contentView addSubview:self.cancelBtn];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(5);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];
    
    [self.contentView addSubview:self.finBtn];
    [self.finBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(5);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];
    
    [self.contentView addSubview:self.timeLb];
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(80);
        make.top.mas_equalTo(self.coin.mas_bottom).offset(20);
        make.height.mas_equalTo(17);
    }];
    
    [self.contentView addSubview:self.entrustPrice];
    [self.entrustPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.timeLb);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(self.timeLb);
    }];
    
    [self.contentView addSubview:self.entrustNum];
    [self.entrustNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.entrustPrice);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(self.entrustPrice);
    }];
    
    [self.contentView addSubview:self.timeText];
    [self.timeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.direct);
        make.top.mas_equalTo(self.timeLb.mas_bottom).offset(15);
        make.height.mas_equalTo(18);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    [self.contentView addSubview:self.entruPriceText];
    [self.entruPriceText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.timeText);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.timeText);
    }];
    
    [self.contentView addSubview:self.entruNumText];
    [self.entruNumText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.entrustNum);
        make.top.mas_equalTo(self.entruPriceText);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.entruPriceText);
    }];
    
    [self.contentView addSubview:self.orderTotal];
    [self.orderTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.timeText);
        make.top.mas_equalTo(self.timeText.mas_bottom).offset(35);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(17);
    }];
    
    [self.contentView addSubview:self.orderAverage];
    [self.orderAverage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.orderTotal);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(self.orderTotal);
    }];
    
    [self.contentView addSubview:self.orderNum];
    [self.orderNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.entruNumText);
        make.top.mas_equalTo(self.orderAverage);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.orderAverage);
    }];
    
    [self.contentView addSubview:self.orderTotalText];
    [self.orderTotalText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.orderTotal);
        make.top.mas_equalTo(self.orderTotal.mas_bottom).offset(17);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(17);
    }];
    
    [self.contentView addSubview:self.orderAverageText];
    [self.orderAverageText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.orderTotalText);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.orderTotalText);
    }];
    
    [self.contentView addSubview:self.orderNumText];
    [self.orderNumText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.orderNum);
        make.top.mas_equalTo(self.orderAverageText);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.orderAverageText);
    }];
}



- (UILabel *)direct {
    if (!_direct) {
        _direct = [[UILabel alloc] init];
        _direct.text = kLocalizedString(@"trade_buy");
        _direct.textAlignment = 0;
        _direct.font = kFont14;
        _direct.textColor = ThemeGreenColor;
    }
    return _direct;
}

- (UILabel *)coin {
    if (!_coin) {
        _coin = [[UILabel alloc] init];
        _coin.text = @"";
        _coin.textAlignment = 0;
        _coin.font = kFont14;
        _coin.textColor = [UIColor blackColor];
    }
    return _coin;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc] init];
        _cancelBtn.titleLabel.font = kFont14;
        [_cancelBtn setTitle:kLocalizedString(@"trade_cancel") forState:UIControlStateNormal];
        [_cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        _cancelBtn.backgroundColor = ThemeRedColor;
        _cancelBtn.cornerRadius = 3;
    }
    return _cancelBtn;
}

- (UIButton *)finBtn {
    if (!_finBtn) {
        _finBtn = [[FSCustomButton alloc] init];
        _finBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        _finBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _finBtn.titleLabel.font = kFont14;
        _finBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_finBtn setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        _finBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_finBtn setImage:[[UIImage imageNamed:@"fanhui"] imageWithColor:HEXCOLOR(0xA0A0A0)]  forState:UIControlStateNormal];
        [_finBtn addTarget:self action:@selector(finishAction) forControlEvents:UIControlEventTouchUpInside];
        _finBtn.hidden = YES;
    }
    return _finBtn;
}

- (void)finishAction {
    if (self.finishBlock) {
        self.finishBlock(self.model);
    }
}


- (void)cancelAction {
    if (self.ComisCellBlock) {
        self.ComisCellBlock(self.model.orderId,self.model.market);
    }
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.text = kLocalizedString(@"trade_timeTitle");
        _timeLb.font = kFont12;
        _timeLb.textAlignment = 0;
        _timeLb.textColor = HEXCOLOR(0xC4C4C4);
        _timeLb.adjustsFontSizeToFitWidth = YES;
    }
    return _timeLb;
}

- (UILabel *)entrustPrice {
    if (!_entrustPrice) {
        _entrustPrice = [[UILabel alloc] init];
        _entrustPrice.textColor = HEXCOLOR(0xC4C4C4);
        _entrustPrice.textAlignment = 1;
        _entrustPrice.font = kFont12;
        _entrustPrice.text = kLocalizedString(@"trade_entrustPrice");
        _entrustPrice.adjustsFontSizeToFitWidth = YES;
    }
    return _entrustPrice;
}

- (UILabel *)entrustNum {
    if (!_entrustNum) {
        _entrustNum = [[UILabel alloc] init];
        _entrustNum.text = kLocalizedString(@"trade_entrustNum");
        _entrustNum.font = kFont12;
        _entrustNum.textColor = HEXCOLOR(0xC4C4C4);
        _entrustNum.textAlignment = 2;
        _entrustNum.adjustsFontSizeToFitWidth = YES;
    }
    return _entrustNum;
}

- (UILabel *)timeText {
    if (!_timeText) {
        _timeText = [[UILabel alloc] init];
        _timeText.font = kFont15;
        _timeText.text = @"";
        _timeText.textColor = [UIColor blackColor];
        _timeText.textAlignment = 0;
    }
    return _timeText;
}

- (UILabel *)entruPriceText {
    if (!_entruPriceText) {
        _entruPriceText = [[UILabel alloc] init];
        _entruPriceText.font = kFont15;
        _entruPriceText.textColor = [UIColor blackColor];
        _entruPriceText.textAlignment = 1;
        _entruPriceText.text = @"";
    }
    return _entruPriceText;
}

- (UILabel *)entruNumText {
    if (!_entruNumText) {
        _entruNumText = [[UILabel alloc] init];
        _entruNumText.text = @"";
        _entruNumText.font = kFont15;
        _entruNumText.textAlignment = 2;
        _entruNumText.textColor = [UIColor blackColor];
    }
    return _entruNumText;
}

- (UILabel *)orderTotal {
    if (!_orderTotal) {
        _orderTotal = [[UILabel alloc] init];
        _orderTotal.font = kFont12;
        _orderTotal.text = kLocalizedString(@"trade_totalAmount");
        _orderTotal.textColor = HEXCOLOR(0xC4C4C4);
        _orderTotal.textAlignment = 0;
        _orderTotal.adjustsFontSizeToFitWidth = YES;
    }
    return _orderTotal;
}

- (UILabel *)orderAverage {
    if (!_orderAverage) {
        _orderAverage = [[UILabel alloc] init];
        _orderAverage.font = kFont12;
        _orderAverage.text = kLocalizedString(@"trade_type");
        _orderAverage.textAlignment = 1;
        _orderAverage.textColor = HEXCOLOR(0xC4C4C4);
        _orderAverage.adjustsFontSizeToFitWidth = YES;
    }
    return _orderAverage;
}

- (UILabel *)orderNum {
    if (!_orderNum) {
        _orderNum = [[UILabel alloc] init];
        _orderNum.font = kFont12;
        _orderNum.text = kLocalizedString(@"trade_orderNum");
        _orderNum.adjustsFontSizeToFitWidth = YES;
        _orderNum.textAlignment = 2;
        _orderNum.textColor = HEXCOLOR(0xC4C4C4);
    }
    return _orderNum;
}

- (UILabel *)orderTotalText {
    if (!_orderTotalText) {
        _orderTotalText = [[UILabel alloc] init];
        _orderTotalText.font = kFont15;
        _orderTotalText.textAlignment = 0;
        _orderTotalText.textColor = [UIColor blackColor];
        _orderTotalText.text = @"";
    }
    return _orderTotalText;
}

- (UILabel *)orderAverageText {
    if (!_orderAverageText) {
        _orderAverageText = [[UILabel alloc] init];
        _orderAverageText.font = kFont15;
        _orderAverageText.text = @"";
        _orderAverageText.textColor = [UIColor blackColor];
        _orderAverageText.textAlignment = 1;
    }
    return _orderAverageText;
}

- (UILabel *)orderNumText {
    if (!_orderNumText) {
        _orderNumText = [[UILabel alloc] init];
        _orderNumText.textColor = [UIColor blackColor];
        _orderNumText.font = kFont15;
        _orderNumText.textAlignment = 2;
        _orderNumText.text = @"";
        _orderNumText.adjustsFontSizeToFitWidth = YES;
    }
    return _orderNumText;
}
@end
