//
//  HisDepositDetailVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/8.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "HisDepositDetailVC.h"

@interface HisDepositDetailVC ()
@property (nonatomic ,strong) UILabel *countLb;

@property (nonatomic ,strong) UILabel *typeLb;
@property (nonatomic ,strong) UILabel *typeText;
@property (nonatomic ,strong) UILabel *line1;

@property (nonatomic ,strong) UILabel *stateLb;
@property (nonatomic ,strong) UILabel *stateText;
@property (nonatomic ,strong) UILabel *line2;

@property (nonatomic ,strong) UILabel *timeLb;
@property (nonatomic ,strong) UILabel *timeText;
@property (nonatomic ,strong) UILabel *line3;

@property (nonatomic ,strong) UILabel *tradeLb;
@property (nonatomic ,strong) UILabel *tradeText;

@end

@implementation HisDepositDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setLeftBtnImage:@"leftBack"];
    self.navBar.backgroundColor = WhiteColor;
    if (self.current == 0) {
        [self setNavBarTitle:@"充币明细"];
    } else {
        [self setNavBarTitle:@"提币明细"];
    }
    [self setUpView];
}

- (void)setUpView {
    self.countLb.text = [NSString stringWithFormat:@"+%@ %@",self.model.amount,self.model.coinType];
    [self.view addSubview:self.countLb];
    [self.countLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(25);
    }];
    
    [self.view addSubview:self.typeLb];
    [self.typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.countLb.mas_bottom).offset(40);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(15);
    }];
    
    [self.view addSubview:self.typeText];
    if (self.current == 0) {
        self.typeText.text = @"普通充币";
        self.stateText.text = self.model.status;
    } else {
        self.typeText.text = @"普通提币";
        self.stateText.text = self.model.withdrawStatus;
    }
    [self.typeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.typeLb);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
    }];
    
    [self.view addSubview:self.line1];
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(self.typeText.mas_bottom).offset(10);
    }];
    
    [self.view addSubview:self.stateLb];
    [self.stateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.line1.mas_bottom).offset(10);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(15);
    }];
    
    [self.view addSubview:self.stateText];
    [self.stateText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.stateLb);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(20);
    }];
    [self.view addSubview:self.line2];
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.stateText.mas_bottom).offset(10);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.view addSubview:self.timeLb];
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.line2.mas_bottom).offset(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(15);
    }];
    self.timeText.text = self.model.createdAt;
    [self.view addSubview:self.timeText];
    [self.timeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.timeLb);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(20);
    }];
    [self.view addSubview:self.line3];
    [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.timeText.mas_bottom).offset(10);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.view addSubview:self.tradeLb];
    [self.tradeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.line3.mas_bottom).offset(10);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(15);
    }];
    
    if (self.current == 1 && ![self.model.withdrawStatus isEqualToString:kLocalizedString(@"deposit_done")]) {
        self.tradeText.text = @"";
    } else {
        self.tradeText.text = self.model.toAddress;
    }
    [self.view addSubview:self.tradeText];
    [self.tradeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.tradeLb);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(50);
    }];
}

- (UILabel *)countLb {
    if (!_countLb) {
        _countLb = [[UILabel alloc] init];
        _countLb.textColor = ThemeGreenColor;
        _countLb.textAlignment = 0;
        _countLb.font = kFont16;
    }
    return _countLb;
}

- (UILabel *)typeLb {
    if (!_typeLb) {
        _typeLb = [[UILabel alloc] init];
        _typeLb.textAlignment = 0;
        _typeLb.font = kFont15;
        _typeLb.text = kLocalizedString(@"withdraw_type");
        _typeLb.textColor = HEXCOLOR(0xC4C4C4);
    }
    return _typeLb;
}

- (UILabel *)typeText {
    if (!_typeText) {
        _typeText = [[UILabel alloc] init];
        _typeText.textAlignment = 2;
        _typeText.textColor = [UIColor blackColor];
        _typeText.font = kFont16;
    }
    return _typeText;
}

- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line1;
}

- (UILabel *)stateLb {
    if (!_stateLb) {
        _stateLb = [[UILabel alloc] init];
        _stateLb.text = kLocalizedString(@"withdraw_state");
        _stateLb.textColor = HEXCOLOR(0xC4C4C4);
        _stateLb.font = kFont15;
        _stateLb.textAlignment = 0;
    }
    return _stateLb;
}

- (UILabel *)stateText {
    if (!_stateText) {
        _stateText = [[UILabel alloc] init];
        _stateText.font = kFont16;
        _stateText.textAlignment = 2;
        _stateText.textColor = [UIColor blackColor];
    }
    return _stateText;
}

- (UILabel *)line2 {
    if (!_line2) {
        _line2 = [[UILabel alloc] init];
        _line2.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line2;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.textColor = HEXCOLOR(0xC4C4C4);
        _timeLb.text = kLocalizedString(@"withdraw_time");
        _timeLb.font = kFont15;
        _timeLb.textAlignment = 0;
    }
    return _timeLb;
}


- (UILabel *)timeText {
    if (!_timeText) {
        _timeText = [[UILabel alloc] init];
        _timeText.font = kFont16;
        _timeText.textColor = [UIColor blackColor];
        _timeText.textAlignment = 2;
    }
    return _timeText;
}

- (UILabel *)line3 {
    if (!_line3) {
        _line3 = [[UILabel alloc] init];
        _line3.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line3;
}

- (UILabel *)tradeLb {
    if (!_tradeLb) {
        _tradeLb = [[UILabel alloc] init];
        _tradeLb.font = kFont15;
        _tradeLb.text = kLocalizedString(@"withdraw_tradeId");
        _tradeLb.textColor = HEXCOLOR(0xC4C4C4);
        _tradeLb.textAlignment = 0;
    }
    return _tradeLb;
}

- (UILabel *)tradeText {
    if (!_tradeText) {
        _tradeText = [[UILabel alloc] init];
        _tradeText.numberOfLines = 0;
        _tradeText.textColor = [UIColor blackColor];
        _tradeText.font = kFont16;
        _tradeText.textAlignment = 2;
        _tradeText.adjustsFontSizeToFitWidth = YES;
    }
    return _tradeText;
}
@end
