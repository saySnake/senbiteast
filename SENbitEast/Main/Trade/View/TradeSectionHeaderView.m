//
//  TradeSectionHeaderView.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/28.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeSectionHeaderView.h"
#import "UIColor+Y_StockChart.h"
@interface TradeSectionHeaderView ()

/** 当前委托 */
@property (strong, nonatomic) XXButton *orderButton;

/** 历史委托 */
@property (strong, nonatomic) XXButton *nowButton;

/** 历史成交 */
@property (strong, nonatomic) XXButton *hisTradeButton;

/** 分割线 */
@property (strong, nonatomic) UIView *lineView;

/** 指示线 */
@property (strong, nonatomic) UIView *indexLine;

@end

@implementation TradeSectionHeaderView

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupUI];
        [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    }
    return self;
}

- (void)changeLanguage {
    [self.orderButton setTitle:kLocalizedString(@"trade_current") forState:UIControlStateNormal];
    [self.nowButton setTitle:kLocalizedString(@"trade_history") forState:UIControlStateNormal];
}

#pragma mark - 1. 初始化UI
- (void)setupUI {
    
    [self addSubview:self.orderButton];
    
    [self addSubview:self.nowButton];
    
//    [self addSubview:self.hisTradeButton];
    
    [self addSubview:self.lineView];
    
    [self addSubview:self.indexLine];
}

#pragma mark - || 懒加载

- (XXButton *)hisTradeButton {
    if (_hisTradeButton == nil) {
        KWeakSelf
        _hisTradeButton = [XXButton buttonWithFrame:CGRectMake(CGRectGetMaxX(self.nowButton.frame) + 10, 0, KSpacing*2 + [NSString widthWithText:kLocalizedString(@"trade_hisOrder") font:kFontBold16], self.height - 3) title:kLocalizedString(@"trade_hisOrder") font:kFontBold16 titleColor:[UIColor assistTextColor] block:^(UIButton *button) {
            if (weakSelf.index == 2) {
                return ;
            }
            self.index = 2;
            CGFloat width = button.titleLabel.width;
            CGFloat lineLeftX = button.centerX - width / 2.0f;
            [UIView animateWithDuration:0.3 animations:^{
                weakSelf.indexLine.left = lineLeftX;
                weakSelf.indexLine.width = width;
            }];
            button.selected = YES;
            weakSelf.orderButton.selected = NO;
            weakSelf.nowButton.selected = NO;
            if (weakSelf.headActionBlock) {
                weakSelf.headActionBlock(2);
            }
        }];
        [_hisTradeButton setTitleColor:kBlue100 forState:UIControlStateSelected];
    }
    return _hisTradeButton;
}

- (XXButton *)orderButton {
    if (_orderButton == nil) {
        KWeakSelf
        _orderButton = [XXButton buttonWithFrame:CGRectMake(10, 0, KSpacing*2 + [NSString widthWithText:kLocalizedString(@"trade_current") font:kFontBold16], self.height - 3) title:kLocalizedString(@"trade_current") font:kFontBold16 titleColor:[UIColor assistTextColor] block:^(UIButton *button) {
            if (weakSelf.index == 0) {
                return ;
            }
            self.index = 0;
            CGFloat width = button.titleLabel.width;
            CGFloat lineLeftX = button.centerX - width / 2.0f;
            [UIView animateWithDuration:0.3 animations:^{
                weakSelf.indexLine.left = lineLeftX;
                weakSelf.indexLine.width = width;
            }];
            button.selected = YES;
            weakSelf.nowButton.selected = NO;
            weakSelf.hisTradeButton.selected = NO;
            if (weakSelf.headActionBlock) {
                weakSelf.headActionBlock(0);
            }
        }];
        [_orderButton setTitleColor:kBlue100 forState:UIControlStateSelected];
        _orderButton.selected = YES;
    }
    return _orderButton;
}

- (XXButton *)nowButton {
    if (_nowButton == nil) {
        KWeakSelf
        _nowButton = [XXButton buttonWithFrame:CGRectMake(10 + (KSpacing*2 + [NSString widthWithText:kLocalizedString(@"trade_history") font:kFontBold14]), 0, K375(48) + [NSString widthWithText:kLocalizedString(@"trade_history") font:kFontBold16], self.height - 3) title:kLocalizedString(@"trade_history") font:kFontBold16 titleColor:[UIColor assistTextColor] block:^(UIButton *button) {
            
            if (weakSelf.index == 1) {
                return ;
            }
            self.index = 1;
            
            CGFloat width = button.titleLabel.width;
            CGFloat lineLeftX = button.centerX - width / 2.0f;
            [UIView animateWithDuration:0.3 animations:^{
                weakSelf.indexLine.left = lineLeftX;
                weakSelf.indexLine.width = width;
            }];
            button.selected = YES;
            weakSelf.orderButton.selected = NO;
            weakSelf.hisTradeButton.selected = NO;
            if (weakSelf.headActionBlock) {
                weakSelf.headActionBlock(1);
            }
        }];
        [_nowButton setTitleColor:kBlue100 forState:UIControlStateSelected];
    }
    return _nowButton;
}


- (UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, kScreen_Width, kLinePixel)];
        _lineView.backgroundColor = HEXCOLOR(0xF2F2F2);
    }
    return _lineView;
}

- (UIView *)indexLine {
    if (_indexLine == nil) {
        _indexLine = [[UIView alloc] initWithFrame:CGRectMake(26 , self.height - 1.5, [NSString widthWithText:kLocalizedString(@"trade_current") font:kFontBold16], 1.5)];
        _indexLine.backgroundColor = kBlue100;
    }
    return _indexLine;
}


@end
