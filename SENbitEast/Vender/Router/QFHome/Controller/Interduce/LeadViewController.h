//
//  LeadViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,SLinkLeadType) {
    SLinkLeadTypeNewVersion,
    SLinkLeadTypeStartAnimation,
};

typedef void (^leadFinishBlock)();

@interface LeadViewController : BaseViewController

@property (nonatomic, copy) leadFinishBlock finishBlock;

@property (nonatomic, assign) SLinkLeadType type;

- (id)initWithType:(SLinkLeadType)type;
@end

NS_ASSUME_NONNULL_END
