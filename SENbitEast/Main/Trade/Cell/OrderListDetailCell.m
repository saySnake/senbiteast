//
//  OrderListDetailCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "OrderListDetailCell.h"

@interface OrderListDetailCell()
@property (nonatomic ,strong) NSMutableArray *masonryViewArray;

@end
@implementation OrderListDetailCell

+ (instancetype)cellWithTbaleView:(UITableView *)tableView {
    static NSString *ID = @"OrderListDetailCell";
    OrderListDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[OrderListDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.cornerRadius = 5;
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        self.backgroundColor = WhiteColor;
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    [self.masonryViewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:30 leadSpacing:10 tailSpacing:10];
    // 设置array的垂直方向的约束
    [self.masonryViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@5);
        make.height.equalTo(@30);
    }];
}

- (NSMutableArray *)masonryViewArray {
    if (!_masonryViewArray) {
        _masonryViewArray = [NSMutableArray array];
        for (int i = 0; i < 4; i ++) {
            if (i == 0) {
                [self.contentView addSubview:self.timeLb];
                [_masonryViewArray addObject:self.timeLb];

            } else if (i == 1) {
                [self.contentView addSubview:self.priceLb];
                [_masonryViewArray addObject:self.priceLb];

            } else if (i == 2) {
                [self.contentView addSubview:self.numLb];
                [_masonryViewArray addObject:self.numLb];

            } else if (i == 3) {
                [self.contentView addSubview:self.gasLb];
                [_masonryViewArray addObject:self.gasLb];
            }
        }
    }
    return _masonryViewArray;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.textColor = [UIColor blackColor];
        _timeLb.font = kFont10;
        _timeLb.textAlignment = 0;
        _timeLb.adjustsFontSizeToFitWidth = YES;
    }
    return _timeLb;
}


- (UILabel *)priceLb {
    if (!_priceLb) {
        _priceLb = [[UILabel alloc] init];
        _priceLb.textColor = [UIColor blackColor];
        _priceLb.font = kFont10;
        _priceLb.textAlignment = 0;
        _priceLb.adjustsFontSizeToFitWidth = YES;
    }
    return _priceLb;
}

- (UILabel *)numLb {
    if (!_numLb) {
        _numLb = [[UILabel alloc] init];
        _numLb.textColor = [UIColor blackColor];
        _numLb.font = kFont10;
        _numLb.textAlignment = 0;
        _numLb.adjustsFontSizeToFitWidth = YES;
    }
    return _numLb;
}

- (UILabel *)gasLb {
    if (!_gasLb) {
        _gasLb = [[UILabel alloc] init];
        _gasLb.textColor = [UIColor blackColor];
        _gasLb.font = kFont10;
        _gasLb.textAlignment = 0;
        _gasLb.adjustsFontSizeToFitWidth = YES;
    }
    return _gasLb;
}

- (void)setModel:(OderDetailModel *)model {
    _model = model;
    self.timeLb.text = model.createdAt;
    self.priceLb.text = model.amount;
    self.numLb.text = model.volume;
    self.gasLb.text = model.fees;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
