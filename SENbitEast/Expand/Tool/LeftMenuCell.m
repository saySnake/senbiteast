//
//  LeftMenuCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "LeftMenuCell.h"

@implementation LeftMenuCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"LeftMenuCell";
    LeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[LeftMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    [self.contentView addSubview:self.leftImg];
    [self.leftImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.width.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.lbText];
    [self.lbText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftImg.mas_right).offset(10);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(30);
    }];
    
    [self.contentView addSubview:self.custSwitch1];
    [self.custSwitch1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-50);
        make.top.mas_equalTo(13);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(10);
    }];
}


- (UIImageView *)leftImg {
    if (!_leftImg) {
        _leftImg = [[UIImageView alloc] init];
        _leftImg.contentMode = UIViewContentModeScaleToFill;
    }
    return _leftImg;
}

- (UILabel *)lbText {
    if (!_lbText) {
        _lbText = [[UILabel alloc] init];
        _lbText.font = kFont16;
        _lbText.textAlignment = 0;
        _lbText.textColor = HEXCOLOR(0x333333);
    }
    return _lbText;
}


- (UISwitch *)custSwitch1 {
    if (!_custSwitch1) {
        _custSwitch1 = [[UISwitch alloc] init];
        _custSwitch1.transform = CGAffineTransformMakeScale(0.75, 0.75);

//    WithFrame:CGRectMake(DScreenW - 100, 20, 40, 20)];
        //设置按钮处于关闭状态时边框的颜色
        _custSwitch1.tintColor = MainWhiteColor;
        //设置开关处于开启时的状态
        _custSwitch1.onTintColor = ThemeGreenColor;
        //设置开关的状态钮颜色
        _custSwitch1.thumbTintColor = WhiteColor;
        [_custSwitch1 addTarget:self action:@selector(switchPressed:) forControlEvents:UIControlEventValueChanged];
        _custSwitch1.hidden = YES;
    }
    return _custSwitch1;
}


- (void)switchPressed:(UISwitch *)swi {

}

@end
