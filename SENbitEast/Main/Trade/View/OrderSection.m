//
//  OrderSection.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "OrderSection.h"

@interface OrderSection ()
@property (nonatomic ,strong) NSMutableArray *masonryViewArray;
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UILabel *detailLb;

@end

@implementation OrderSection

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    
    [self addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(25);
    }];
    [self.bgView addSubview:self.detailLb];
    [self.detailLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(10);
    }];
    
    [self.masonryViewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:30 leadSpacing:10 tailSpacing:10];
    
    // 设置array的垂直方向的约束
    [self.masonryViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.detailLb.mas_bottom).offset(5);
        make.height.equalTo(@30);
    }];
}

- (UILabel *)detailLb {
    if (!_detailLb) {
        _detailLb = [[UILabel alloc] init];
        _detailLb.textAlignment = 0;
        _detailLb.font = kFont20;
        _detailLb.text = kLocalizedString(@"detail_detail");
        _detailLb.textColor = [UIColor blackColor];
    }
    return _detailLb;
}



- (NSMutableArray *)masonryViewArray {
    if (!_masonryViewArray) {
        _masonryViewArray = [NSMutableArray array];
        for (int i = 0; i < 4; i ++) {
            UILabel *view = [[UILabel alloc] init];
            view.textColor = ThemeLittleTitleColor;
            view.font = kFont14;
            view.textAlignment = 0;
            view.backgroundColor = [UIColor clearColor];
            if (i == 0) {
                view.text = kLocalizedString(@"entrust_time");
            } else if (i == 1) {
                view.text = kLocalizedString(@"entrust_price");
            } else if (i == 2) {
                view.text = kLocalizedString(@"entrust_num");
            } else if (i == 3) {
                view.text = kLocalizedString(@"entrust_fee");
            }
            [self addSubview:view];
            [_masonryViewArray addObject:view];
        }
    }
    return _masonryViewArray;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = WhiteColor;
    }
    return _bgView;
}

@end
