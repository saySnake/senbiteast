//
//  WMHomeCell.m
//  Senbit
//
//  Created by 张玮 on 2020/3/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "WMHomeCell.h"
#import "NSString+SuitScanf.h"


@implementation WMHomeCell
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"WMHomeCell";
    WMHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[WMHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        self.backgroundColor = WhiteColor;
        self.contentView.backgroundColor = WhiteColor;
    }
    return self;
}
- (void)setUpView {
    [self.contentView addSubview:self.name];
//    [self.contentView addSubview:self.nameTrade];
//    [self.contentView addSubview:self.volLabel];
    [self.contentView addSubview:self.dolerLabel];
//    [self.contentView addSubview:self.cnyLabel];
    [self.contentView addSubview:self.perLabel];
    [self make_Layout];
}

- (void)setModel:(OneTableSockeModel *)model {
    _model = model;
    if ([model isKindOfClass:[OneTableSockeModel class]]) {
        OtcExtinfo *info = [[CacheManager sharedMnager] getOtcInfo];
    //    NSString *name = model.coinName;
    //    NSRange range = [name rangeOfString:@"/" options:NSBackwardsSearch];
    //    NSString *from = [name substringToIndex:range.location];
    //    NSString *to = [name substringFromIndex:range.location];
    //    self.name.text = from;
        
        NSRange range;
        range = [model.coinName rangeOfString:@"/"];
        NSMutableAttributedString *strs = [[NSMutableAttributedString alloc] initWithString:model.coinName];
        [strs addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"] range:NSMakeRange(0, range.location)];
//        _name.font = BOLDSYSTEMFONT(20);

        [strs addAttribute:NSFontAttributeName
                     value:[UIFont boldSystemFontOfSize:20]
                     range: NSMakeRange (0, range.location)];
        self.name.attributedText = strs;

    //    self.nameTrade.text = to;
//        self.volLabel.text = [NSString returnFormatter:[NSString stringWithFormat:kLocalizedString(@"home_voume%@"),[NSString divV1:model.volume v2:TENZero]]];
//        KDetail.symbolsArray
        
         [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
                XXSymbolModel *m = obj;
                NSLog(@"%@",KDetail.symbolModel.symbolId);
                self.dolerLabel.text = [KDecimal decimalNumber:[NSString divV1:model.lastPrice v2:TENZero] RoundingMode:NSRoundDown scale:[KDecimal scale:m.quotePrecision]];
                *stop = YES;
              }
        }];
    

//        self.cnyLabel.text = [NSString stringWithFormat:@"≈%@ %@",[NSString returnFormatter:[NSString mulV1:model.price v2:kLanguageManager.currentExchange]],kLanguageManager.currentLangSymbol];
        NSString *per = [NSString returnFormatter:[NSString mulV1:model.change v2:@"100"]];
        if (per.floatValue > 0) {
            per = [NSString stringWithFormat:@"+%.2f%%",per.floatValue];
            self.perLabel.backgroundColor = ThemeGreenColor;
        } else if (per.floatValue < 0) {
            per = [NSString stringWithFormat:@"%.2f%%",per.floatValue];
            self.perLabel.backgroundColor = HEXCOLOR(0xED2482);
        } else {
            per = [NSString stringWithFormat:@"%.2f%%",per.floatValue];
            self.perLabel.backgroundColor = PlaceHolderColor;
        }
        self.perLabel.text = per;
    }
}

- (void)make_Layout {
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(23);
    }];
    
//    [self.nameTrade mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.name.mas_right);
//        make.top.mas_equalTo(15);
//        make.width.mas_equalTo(60);
//        make.height.mas_equalTo(12.5);
//    }];
    
//    [self.volLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(20);
//        make.top.mas_equalTo(self.name.mas_bottom).offset(10);
//        make.width.mas_equalTo(140);
//        make.height.mas_equalTo(12);
//    }];
    
    [self.dolerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView).offset(-4);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(20);
    }];
    
//    [self.cnyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.contentView);
//        make.top.mas_equalTo(self.dolerLabel.mas_bottom).offset(10);
//        make.width.mas_equalTo(130);
//        make.height.mas_equalTo(10);
//    }];
    
    [self.perLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
//        make.top.mas_equalTo(12);
        make.centerY.mas_equalTo(self);
        make.height.mas_equalTo(34);
        make.width.mas_equalTo(80);
    }];
}

- (UILabel *)name {
    if (!_name) {
        _name = [[UILabel alloc] init];
        _name.textColor = [UIColor colorWithLightColorStr:@"A0A0A0" DarkColor:@"A0A0A0"];
        _name.font = BOLDSYSTEMFONT(15);
        _name.adjustsFontSizeToFitWidth = YES;
        _name.textAlignment = 0;
//        _name.text = KDetail.symbolModel.symbolId;
    }
    return _name;
}

//- (UILabel *)nameTrade {
//    if (!_nameTrade) {
//        _nameTrade = [[UILabel alloc] init];
//        _nameTrade.textColor = PlaceHolderColor;
//        _nameTrade.font = BOLDSYSTEMFONT(16);
//        _nameTrade.textAlignment = 0;
//    }
//    return _nameTrade;
//}

- (UILabel *)volLabel {
    if (!_volLabel) {
        _volLabel = [[UILabel alloc] init];
        _volLabel.textColor = [UIColor colorWithLightColorStr:@"6B6B6B" DarkColor:@"6B6B6B"];
        _volLabel.textAlignment = 0;
        _volLabel.adjustsFontSizeToFitWidth = YES;
        _volLabel.font = TwelveFontSize;
    }
    return _volLabel;
}

- (UILabel *)dolerLabel {
    if (!_dolerLabel) {
        _dolerLabel = [[UILabel alloc] init];
        _dolerLabel.textColor = MainWhiteColor;
        _dolerLabel.textAlignment = 2;
//        _dolerLabel.font = [UIFont systemFontOfSize:11.5];
        _dolerLabel.font = BOLDSYSTEMFONT(18);
        _dolerLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _dolerLabel;
}

- (UILabel *)cnyLabel {
    if (!_cnyLabel) {
        _cnyLabel = [[UILabel alloc] init];
        _cnyLabel.textColor = [UIColor colorWithLightColorStr:@"6B6B6B" DarkColor:@"6B6B6B"];
        _cnyLabel.textAlignment = 1;
        _cnyLabel.font = TwelveFontSize;
    }
    return _cnyLabel;
}

- (UILabel *)perLabel {
    if (!_perLabel) {
        _perLabel = [[UILabel alloc] init];
        _perLabel.textColor = WhiteColor;
        _perLabel.backgroundColor = ThemeGreenColor;
//        _perLabel.font = TwelveFontSize;
        _perLabel.font = BOLDSYSTEMFONT(16);
        _perLabel.textAlignment = 1;
        _perLabel.cornerRadius = 2;
        _perLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _perLabel;
}

@end
