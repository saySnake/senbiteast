//
//  QFHomeViewController.h
//  QFMoudle
//
//  Created by 情风 on 2018/11/5.
//  Copyright © 2018年 qingfengiOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMStickyPageController.h"
@class MoudleHome;

@interface QFHomeViewController : WMStickyPageController

/// 外部接口
@property(nonatomic, strong) MoudleHome *interface;

@end


@interface ABCellModel : NSObject

@property (nonatomic, strong) NSString *imageNamed;
@property (nonatomic, strong) NSString *textContent;
@property (nonatomic, strong) NSString *controllerClass;
@property (nonatomic, strong) NSString *badgeValue;
@property (nonatomic, assign) BOOL isNew;

+ (instancetype)ABImageName:(NSString *)name text:(NSString *)text aClass:(NSString *)aClass;

@end
