//
//  AddressModel.h
//  Senbit
//
//  Created by 张玮 on 2020/4/4.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddressModel : NSObject
/** title **/
@property (nonatomic ,copy) NSString *title;
/** address **/
@property (nonatomic ,copy) NSString *address;

@property (nonatomic ,copy) NSString *destinationTag;

@property (nonatomic ,copy) NSString *coin;

@end

NS_ASSUME_NONNULL_END
