//
//  SelectedCoinCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/20.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "SelectedCell.h"

@implementation SelectedCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"SelectedCell";
    SelectedCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[SelectedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    [self.contentView addSubview:self.coin];
    [self.contentView addSubview:self.price];
    [self make_Layout];
}

- (void)make_Layout {
    [self.coin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-30);
        make.top.mas_equalTo(self.coin.mas_top);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
}

- (UILabel *)coin {
    if (!_coin) {
        _coin = [[UILabel alloc] init];
        _coin.textColor = [UIColor yellowColor];
        _coin.textAlignment = 0;
        _coin.font = FourteenFontSize;
        _coin.adjustsFontSizeToFitWidth = YES;
        _coin.text = @"BTC/USDT";
    }
    return _coin;
}

- (UILabel *)price {
    if (!_price) {
        _price = [[UILabel alloc] init];
        _price.textColor = ThemeGreenColor;
        _price.textAlignment = 0;
        _price.adjustsFontSizeToFitWidth = YES;
        _price.font = BOLDSYSTEMFONT(14);
        _price.text = @"2000";
    }
    return _price;
}



- (void)setModel:(OneTableSockeModel *)model {
    _model = model;
    
//    NSRange range;
//    range = [model.coinName rangeOfString:@"/"];
//    NSMutableAttributedString *strs = [[NSMutableAttributedString alloc] initWithString:model.coinName];
//    [strs addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"] range:NSMakeRange(0, range.location)];
    self.coin.text = model.coinName;
//    self.price.text = [NSString divV1:model.lastPrice v2:TENZero];
    
    [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
           XXSymbolModel *m = obj;
           NSLog(@"%@",KDetail.symbolModel.symbolId);
           self.price.text = [KDecimal decimalNumber:[NSString divV1:model.lastPrice v2:TENZero] RoundingMode:NSRoundDown scale:[KDecimal scale:m.quotePrecision]];
           *stop = YES;
         }
   }];
//    self.price.text = [KDecimal decimalNumber:[NSString divV1:model.lastPrice v2:TENZero] RoundingMode:NSRoundDown scale:[KDecimal scale:m.quotePrecision]];

    NSString *per = [NSString returnFormatter:[NSString mulV1:model.change v2:@"100"]];
    if (per.floatValue > 0) {
        per = [NSString stringWithFormat:@"+%.2f%%",per.floatValue];
        self.price.textColor = ThemeGreenColor;
    } else {
        per = [NSString stringWithFormat:@"%.2f%%",per.floatValue];
        self.price.textColor = HEXCOLOR(0xED2482);
    }
}

@end
