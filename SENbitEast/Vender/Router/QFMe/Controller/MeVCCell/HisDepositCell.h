//
//  HisDepositCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/7.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssertListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HisDepositCell : UITableViewCell

@property (strong, nonatomic) void(^seeBtnBlock)(NSString *ID);


+ (instancetype)cellWithTbaleView:(UITableView *)tableView;

@property (nonatomic ,strong) UILabel *typeLb;

@property (nonatomic ,strong) FSCustomButton * detailBtn;

@property (nonatomic ,strong) UILabel *timeLb;

@property (nonatomic ,strong) UILabel *stateLb;

@property (nonatomic ,strong) UILabel *entrustNum;

@property (nonatomic ,strong) UILabel *timeText;

@property (nonatomic ,strong) UILabel *stateText;

@property (nonatomic ,strong) UILabel *entrustText;

@property (nonatomic ,strong) AssertListModel *depositModel;

@property (nonatomic ,strong) NSString *type;

@end

NS_ASSUME_NONNULL_END
