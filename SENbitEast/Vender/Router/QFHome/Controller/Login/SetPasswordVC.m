//
//  SetPasswordVC.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/17.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "SetPasswordVC.h"
#import "WebViewController.h"

@interface SetPasswordVC ()
/** cancelBtn **/
@property (nonatomic ,strong) BaseButton *cancelBtn;

/** title **/
@property (nonatomic ,strong) BaseLabel *titleLb;

/** contentLb **/
@property (nonatomic ,strong) BaseLabel *contentLb;

/** account **/
@property (nonatomic ,strong) BaseField *accountTF;

/** password **/
@property (nonatomic ,strong) BaseField *passwordTF;

/** selectTF **/
@property (nonatomic ,strong) BaseField *selectTF;

/** fscuomBtn **/
@property (nonatomic ,strong) BaseButton *checkBtn;

/** delegatebtn **/
@property (nonatomic ,strong) BaseButton *protocolBtn;

/** nextBtn **/
@property (nonatomic ,strong) BaseButton *nextBtn;

@property (nonatomic ,strong) UIButton *seeBtn;
@property (nonatomic ,strong) UIButton *scrBtn;
@end

@implementation SetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = WhiteColor;
    [self setUpViews];
    [self make_Layout];
    [self configKeyBoardRespond];
}

- (void)setUpViews {
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.seeBtn];
    [self.view addSubview:self.passwordTF];
    [self.view addSubview:self.scrBtn];
    [self.view addSubview:self.selectTF];
    [self.view addSubview:self.checkBtn];
    [self.view addSubview:self.protocolBtn];
    [self.view addSubview:self.nextBtn];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak SetPasswordVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.passwordTF,weakSelf.selectTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}

- (void)make_Layout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.height.width.mas_equalTo(40);
    }];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.cancelBtn.mas_bottom).offset(75);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(39);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(10);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(18);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(50);
    }];
    
    [self.seeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.accountTF.mas_top).offset(30);
    }];
    
    [self.passwordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountTF);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(40);
        make.right.mas_equalTo(self.accountTF);
        make.height.mas_equalTo(50);
    }];
    
    [self.selectTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.passwordTF);
        make.top.mas_equalTo(self.passwordTF.mas_bottom).offset(40);
        make.height.mas_equalTo(50);
    }];
    
    [self.scrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.passwordTF.mas_top).offset(30);
    }];

    
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.selectTF.mas_bottom).offset(40);
        make.width.height.mas_equalTo(20);
    }];
    
    [self.protocolBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.checkBtn.mas_right).offset(5);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.checkBtn);
        make.height.mas_equalTo(self.checkBtn);
    }];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(54);
        make.top.mas_equalTo(self.protocolBtn.mas_bottom).offset(20);
    }];
}


- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)checkAction:(UIButton *)btn {
    btn.selected = !btn.selected;//每次点击都改变按钮的状态
    if(btn.selected) {
        [self.checkBtn setSelected:YES];
        [self.protocolBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];

    } else {
        [self.checkBtn setSelected:NO];
        [self.protocolBtn setTitleColor:HEXCOLOR(0xC9C9D6) forState:UIControlStateNormal];
    //在此实现打勾时的方法
    }
}

- (void)protocolAction {
    NSLog(@"协议跳转");
    WebViewController *web = [[WebViewController alloc] init];
    web.url = @"https://senbit2019.zendesk.com/hc/zh-cn/articles/360033849474";
//    web.navigationController.navigationBarHidden = YES;
//    id object = [weakSelf nextResponder];
//        while (![object isKindOfClass:[UIViewController class]] && object != nil) {
//            object = [object nextResponder];
//        }
//    UIViewController *superController = (UIViewController*)object;
    [self.navigationController pushViewController:web animated:YES];

}

- (void)registerAction {
    BOOL judge = [NSString judgePassWordLegal:self.accountTF.textField.text];
    if (judge == NO) {
        [self showToastView:kLocalizedString(@"toast_pwdprotocol")];
        return;
    }
         
    if (self.accountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"toast_writePwd")];
        return;
    }
    
    if (self.passwordTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"toast_writeTwPwd")];
        return;
    }
    
    if (![self.accountTF.textField.text isEqualToString:self.passwordTF.textField.text]) {
        [self showToastView:kLocalizedString(@"toast_pwdOrRequir")];
        return;
    }
    
    if (self.checkBtn.selected == NO) {
        [self showToastView:kLocalizedString(@"toast_readProtocol")];
        return;
    }
    
    
    
    if (self.isPhoneRegister) {
        self.countryCode = [self.countryCode stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSDictionary *dic = @{@"authToken":self.authToken,@"nationality":self.locale,@"phone":self.account,@"inviteCode":self.selectTF.textField.text,@"password":self.passwordTF.textField.text};
        [SBNetworTool postWithUrl:EastRegister params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if ([responseObject[@"code"] intValue] == 200) {
                [self showToastView:kLocalizedString(@"toast_registerSuccess")];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                [self showToastView:SuccessMessage];
            }
            
        } failed:^(NSURLSessionDataTask * _Nonnull task, id _Nonnull error, id  _Nonnull responseObject) {
            NSLog(@"%@",FailurMessage);
            [self showToastView:FailurMessage];
        }];
    } else {
        self.countryCode = [self.countryCode stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSDictionary *dic = @{@"authToken":self.authToken,@"nationality":self.locale,@"email":self.account,@"inviteCode":self.selectTF.textField.text,@"password":self.passwordTF.textField.text};
        [SBNetworTool postWithUrl:EastRegister params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if ([responseObject[@"code"] intValue] == 200) {
                [self showToastView:kLocalizedString(@"toast_registerSuccess")];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                [self showToastView:SuccessMessage];
            }
            
        } failed:^(NSURLSessionDataTask * _Nonnull task, id _Nonnull error, id  _Nonnull responseObject) {
            NSLog(@"%@",FailurMessage);
            [self showToastView:FailurMessage];
        }];
    }
}


#pragma mark - lazy
- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = TwelveFontSize;
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseLabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[BaseLabel alloc] init];
//        _titleLb.colorName = @"setPassword_password";
        _titleLb.font = [UIFont systemFontOfSize:33];
        _titleLb.textAlignment = 0;
        _titleLb.text = kLocalizedString(@"setPassword_password");
        _titleLb.textColor = [UIColor blackColor];
    }
    return _titleLb;
}

- (BaseLabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[BaseLabel alloc] init];
        _contentLb.text = kLocalizedString(@"setPassword_content");
        _contentLb.font = FourteenFontSize;
        _contentLb.colorName = @"login_conentColor";
        _contentLb.textAlignment = 0;
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}

- (BaseField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[BaseField alloc] init];
        _accountTF.placeholder = kLocalizedString(@"setAccount_placeholder");
        _accountTF.placeHolderColor = PlaceHolderColor;
        _accountTF.textField.font = FifteenFontSize;
        _accountTF.textField.secureTextEntry = YES;
        [_accountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (BaseField *)passwordTF {
    if (!_passwordTF) {
        _passwordTF = [[BaseField alloc] init];
        _passwordTF.placeholder = kLocalizedString(@"setAccount_confirmplace");
        _passwordTF.placeHolderColor = PlaceHolderColor;
        _passwordTF.textField.secureTextEntry = YES;
        _passwordTF.textField.font = FifteenFontSize;
        [_passwordTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _passwordTF;
}

- (BaseField *)selectTF {
    if (!_selectTF) {
        _selectTF = [[BaseField alloc] init];
        _selectTF.placeholder = kLocalizedString(@"setAccount_selected");
        _selectTF.placeHolderColor = PlaceHolderColor;
    }
    return _selectTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountTF.textField.text.length > 0 && self.passwordTF.textField.text.length > 0) {
        self.nextBtn.userInteractionEnabled = YES;
        self.nextBtn.backgroundColor = ThemeGreenColor;
    } else {
        self.nextBtn.userInteractionEnabled = NO;
        self.nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
    }
}

- (BaseButton *)checkBtn {
    if (!_checkBtn) {
        _checkBtn = [[BaseButton alloc] init];
        _checkBtn.titleLabel.font = TwelveFontSize;
        _checkBtn.backgroundColor = ClearColor;
        [_checkBtn addTarget:self action:@selector(checkAction:) forControlEvents:UIControlEventTouchUpInside];
        [_checkBtn setImage:[UIImage imageNamed:@"check_unselected"] forState:UIControlStateNormal];
        [_checkBtn setImage:[UIImage imageNamed:@"check_selected"] forState:UIControlStateSelected];
    }
    return _checkBtn;
}

- (BaseButton *)protocolBtn {
    if (!_protocolBtn) {
        _protocolBtn = [[BaseButton alloc] init];
        _protocolBtn.titleLabel.font = TwelveFontSize;
        _protocolBtn.backgroundColor = ClearColor;
        [_protocolBtn addTarget:self action:@selector(protocolAction) forControlEvents:UIControlEventTouchUpInside];
        [_protocolBtn setTitle:kLocalizedString(@"setAccount_delegate") forState:UIControlStateNormal];
        [_protocolBtn setTitleColor:HEXCOLOR(0xC9C9D6) forState:UIControlStateNormal];
//        [_protocolBtn setTitleColor:ThemeGreenColor forState:UIControlStateSelected];
        _protocolBtn.contentHorizontalAlignment = 1;
        _protocolBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _protocolBtn;
}

- (BaseButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [[BaseButton alloc] init];
        _nextBtn.titleLabel.font = TwentyFontSize;
        [_nextBtn setTitle:kLocalizedString(@"setAccount_register") forState:UIControlStateNormal];
        _nextBtn.backColorName = @"login_loginBtnCor";
        _nextBtn.colorName = @"reset_confirmTitle";
        _nextBtn.cornerRadius = 4;
        _nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _nextBtn.userInteractionEnabled = NO;
        [_nextBtn addTarget:self action:@selector(registerAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextBtn;
}


- (UIButton *)seeBtn {
    if (!_seeBtn) {
        _seeBtn = [[UIButton alloc] init];
        [_seeBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_seeBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_seeBtn addTarget:self action:@selector(scr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _seeBtn;
}

- (UIButton *)scrBtn {
    if (!_scrBtn) {
        _scrBtn = [[UIButton alloc] init];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn addTarget:self action:@selector(scrr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn;
}


- (void)scr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.accountTF.textField.secureTextEntry = NO;
    } else {
        self.accountTF.textField.secureTextEntry = YES;

    }
}

- (void)scrr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.passwordTF.textField.secureTextEntry = NO;
    } else {
        self.passwordTF.textField.secureTextEntry = YES;
    }
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super hideKeyboard];
}

@end
