//
//  LoginOutActionSheet.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/30.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "LoginOutActionSheet.h"
#import "ActionSheetModel.h"
@interface ActionSheetBtn : UIButton

@end

@interface LoginOutActionSheet(){
    NSInteger cancelTag;
}
/** 存放按钮的视图 */
@property (nonatomic, weak) UIView *containerView;
/** 标题 */
@property (nonatomic, weak) UILabel *titlelb;
@end

@implementation LoginOutActionSheet

+ (instancetype)customActionSheetWithData:(NSArray *)data {
    LoginOutActionSheet *sheet = [[LoginOutActionSheet alloc] init];
    sheet.datas = data;
    return sheet;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
        [self addGestureRecognizer:tap];
    }
    return self;
}


- (UIView *)containerView {
    if (!_containerView) {
        UIView *containerView = [[UIView alloc] init];
        containerView.backgroundColor = WhiteColor;//HEXCOLOR(0xFAFAFA);
        [self addSubview:containerView];
        
        //添加阴影
        containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
        containerView.layer.shadowOffset = CGSizeMake(0, -2);
        containerView.layer.shadowOpacity = 0.17;
        containerView.layer.shadowRadius = 4;
        self.containerView = containerView;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(containerViewTap)];
        [containerView addGestureRecognizer:tap];
    }
    return _containerView;
}

- (UILabel *)titlelb {
    if (!_titlelb) {
        UILabel *titlelb = [[UILabel alloc] init];
        titlelb.font = [UIFont systemFontOfSize:14];
        titlelb.textColor = MainWhiteColor;
        titlelb.textAlignment = 1;
        titlelb.adjustsFontSizeToFitWidth = YES;
        [self.containerView addSubview:titlelb];
        self.titlelb = titlelb;
    }
    return _titlelb;
}

/**
 *  加一个空的点击事件，防止和空白区域的点击事件冲突
 */
- (void)containerViewTap {

}

- (void)tap {
//    if (self.clickBlock) {
//        self.clickBlock(cancelTag);
//    }
    [self closeAcitonSheet];
}

- (void)closeAcitonSheet {
    __weak LoginOutActionSheet *sheet = self;
    [UIView animateWithDuration:0.15 animations:^{
        sheet.containerView.y = sheet.height;
        sheet.containerView.width = sheet.width;
        [sheet setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
    } completion:^(BOOL finished) {
        [sheet removeFromSuperview];
    }];
}

- (void)showFromView:(UIView *)view {
    self.frame = view.frame;
    [view addSubview:self];
    self.containerView.y = view.height;
    self.containerView.width = view.width;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titlelb.text = title;
}

- (void)setDatas:(NSArray *)datas {
    _datas = datas;
    NSInteger tag = 0;
    for (ActionSheetModel *model in datas) {
        ActionSheetBtn *btn = [[ActionSheetBtn alloc] init];
        btn.tag = tag;
        [btn setTitle:model.title forState:UIControlStateNormal];
        [btn setImage:[UIImage imageWithName:model.img] forState:UIControlStateNormal];
        btn.titleLabel.textAlignment = 1;//使图片和文字水平居中显示
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        btn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.containerView addSubview:btn];
        tag ++;
    }
    
    ActionSheetBtn *btn = [[ActionSheetBtn alloc] init];
    btn.tag = tag;
    cancelTag = btn.tag;
    [btn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
    btn.titleLabel.textAlignment = 1;
    [btn setImage:[UIImage imageNamed:@"actionSheet_cancel"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(tap) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.containerView addSubview:btn];
}

- (void)btnClicked:(ActionSheetBtn *)btn {
    
    if (self.clickBlock) {
        self.clickBlock([btn currentTitle]);
    }
    [self closeAcitonSheet];
}

#pragma mark - 布局

- (void)layoutSubviews {
    [super layoutSubviews];
    
    NSInteger btnCount = self.containerView.subviews.count - 1;
    CGFloat marginTop = 53;
    
    if (self.title == nil||[self.title isEqual:@""]) {
        btnCount++;
        [self.titlelb removeFromSuperview];
        marginTop = 13.5;
    } else {
        CGFloat titleX = 0;
        CGFloat titleY = 27;
        CGFloat titleW = DScreenW - 2 * titleX;
        CGFloat titleH = 15;
        self.titlelb.frame = CGRectMake(titleX, titleY, titleW, titleH);
    }
    
    CGFloat x = 0;
    CGFloat w = DScreenW - (2 * x );
    CGFloat h = 48;
    CGFloat maxH = h * btnCount + marginTop + 15;
    
    for (NSInteger index = 0; index < btnCount; index++) {
        ActionSheetBtn *btn = self.containerView.subviews[index];
        CGFloat y = marginTop + h * index;
        if (index != btnCount - 1) {
            UILabel *line = [[UILabel alloc] init];
            line.frame = CGRectMake(0, h - 1, w, 1);
            line.backgroundColor = LineColor;
            [btn addSubview:line];
        }
        btn.frame = CGRectMake(x, y, w, h);
    }
    __weak __typeof(self)Action = self;
    
    [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseOut
                  animations:^{
                      Action.containerView.frame = CGRectMake(0, Action.height - maxH, Action.width, maxH);
                      [Action setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.47]];
                  }
                  completion:nil];
}

@end

@implementation ActionSheetBtn

//- (CGRect)imageRectForContentRect:(CGRect)contentRect {
//    CGFloat x = 23;
//    CGFloat y = 9;
//    CGFloat w = 30;
//    CGFloat h = 30;
//    return CGRectMake(x, y, w, h);
//}
//
//- (CGRect)titleRectForContentRect:(CGRect)contentRect {
//    CGFloat x = 62;
//    CGFloat y = 16.5;
//    CGFloat w = 230;
//    CGFloat h = 16;
//    return CGRectMake(x, y, w, h);
//}

@end
