//
//  ShareView.m
//  Senbit
//
//  Created by 张玮 on 2020/1/14.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SharedView.h"
#import "MMScanViewController.h"

@interface SharedView()

@property (nonatomic ,strong) UIView *view;

@property (nonatomic ,strong) UIImageView *img;

@end

@implementation SharedView

- (instancetype)initFrame:(CGRect)frame newCrabImage:(UIImage *)image  {
    if (self = [super initWithFrame:frame]) {
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        self.view = [[UIView alloc] initWithFrame:self.frame];
        self.view.backgroundColor = [UIColor blackColor];
        self.view.alpha = 0;
        [UIView animateWithDuration:0 animations:^{
            self.view.alpha = 0.3;
        }];
        [self addSubview:self.view];
        [self animationDuration:0.7];
        [UIView animateWithDuration:0.1 animations:^{
            [self crabImage:image ];
        }];
    }
    return self;
}

- (void)crabImage:(UIImage *)screenshotImage {
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 145, self.frame.size.width, 145)];
    bottomView.backgroundColor = WhiteColor;
    [self addSubview:bottomView];
    UIButton *bottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottomView addSubview:bottomButton];
    bottomButton.frame = CGRectMake(0, 100, bottomView.frame.size.width, 45);
    [bottomButton setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    [bottomButton setTitle:kLocalizedString(@"alertView_cancelName") forState:UIControlStateNormal];
    [bottomButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    bottomButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    NSArray *imageArray = @[@"my_xiazai"];
    for (int i = 0; i<imageArray.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [bottomView addSubview:button];
        button.frame = CGRectMake((self.frame.size.width / imageArray.count) * i, 0, self.frame.size.width / imageArray.count, 100);
        [button setTitle:kLocalizedString(@"invitation_downPaper") forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        CGFloat imageWith = button.imageView.frame.size.width;
        CGFloat imageHeight = button.imageView.frame.size.height;
        CGFloat labelWidth = button.titleLabel.intrinsicContentSize.width;
        CGFloat labelHeight = button.titleLabel.intrinsicContentSize.height;
        button.imageEdgeInsets = UIEdgeInsetsMake(-labelHeight, 0, 0, -labelWidth);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight, 0);
        button.tag = i;
    }
    
    CGSize size = CGSizeMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height);
    UIGraphicsBeginImageContext(size);
    
    CGFloat scaleW = screenshotImage.size.width / DScreenH;
    CGFloat scaleH = screenshotImage.size.height / DScreenH;

    /*
//    [screenshotImage drawInRect:CGRectMake(0, 0, screenshotImage.size.width * scaleW, screenshotImage.size.height * scaleH)];
    [screenshotImage drawInRect:CGRectMake(0, 0, self.frame.size.width, DScreenH - 80)];

    UIImageView * imge2V = [[UIImageView alloc] init];
    imge2V.contentMode = UIViewContentModeScaleAspectFit;
    imge2V.image = [UIImage imageNamed:@"newCrabBottom"];
    imge2V.image = [self creatImg:imge2V.image];
    [imge2V.image drawInRect:CGRectMake(0, DScreenH - 90 ,  self.frame.size.width  , 80)];
    UIImage *ZImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *imageV = [[UIImageView alloc] init];
    imageV.frame = CGRectMake(40, 60, self.frame.size.width - 80, self.frame.size.height - 245);
    imageV.image = [self creatCrabImg:screenshotImage];
    imageV.contentMode = UIViewContentModeScaleAspectFit;

    self.img = imageV;
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageV];
     */
    
    //1.获取图片
    //2.开启上下文
    UIGraphicsBeginImageContextWithOptions(screenshotImage.size, NO, [UIScreen mainScreen].scale);
    //3.绘制背景图片
    [screenshotImage drawInRect:CGRectMake(0, 0, screenshotImage.size.width, screenshotImage.size.height)];
    //绘制水印图片到当前上下文
    NSString *myurl = [IWDefault objectForKey:MyInvoteUrl];
    UIImage *image2 = [MMScanViewController createQRImageWithString:myurl QRSize:CGSizeMake(200, 200) QRColor:[UIColor blackColor] bkColor:WhiteColor];

    [image2 drawInRect:CGRectMake(screenshotImage.size.width - 250, screenshotImage.size.height - 240, image2.size.width, image2.size.height)];
    //4.从上下文中获取新图片
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    //5.关闭图形上下文
    UIGraphicsEndImageContext();
    
    UIImageView *imgV = [[UIImageView alloc] init];
    imgV.frame = CGRectMake(40, 60, self.frame.size.width - 80, self.frame.size.height - 245);
    imgV.contentMode = UIViewContentModeScaleAspectFit;
    imgV.image = newImage;
    self.img = imgV;
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imgV];
}


- (UIImage *)creatCrabImg:(UIImage *)image1 {
    NSString *myurl = [IWDefault objectForKey:MyInvoteUrl];
    UIImage *image2 = [MMScanViewController createQRImageWithString:myurl QRSize:CGSizeMake(70, 70) QRColor:[UIColor blackColor] bkColor:WhiteColor];
    CGSize size = CGSizeMake(DScreenW, 90);
    UIGraphicsBeginImageContext(size);
    //放第一个图片
    [image1 drawInRect:CGRectMake(0, 10, size.width, DScreenH - 70)];
    //放第二个图片
    [image2 drawInRect:CGRectMake(size.width - 80, 15, 70, 70)];
    
    UIImage *ZImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    self.imageView.image = ZImage;
    return ZImage;
}










- (instancetype)initWithFrame:(CGRect)frame screenshotImage:(UIImage *)screenshotImage {
    
    if (self = [super initWithFrame:frame]) {
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        
        self.view = [[UIView alloc] initWithFrame:self.frame];
        self.view.backgroundColor = [UIColor blackColor];
        self.view.alpha = 0;
        [UIView animateWithDuration:0 animations:^{
            self.view.alpha = 0.3;
        }];
        [self addSubview:self.view];
        [self animationDuration:0.7];
        
        [UIView animateWithDuration:0.1 animations:^{
            [self creatChildViews:screenshotImage];
        }];
    }
    return self;
}

/** 创建子视图 */
- (void)creatChildViews:(UIImage *)screenshotImage {
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 145, self.frame.size.width, 145)];
    bottomView.backgroundColor = WhiteColor;
    [self addSubview:bottomView];
    UIButton *bottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottomView addSubview:bottomButton];
    bottomButton.frame = CGRectMake(0, 100, bottomView.frame.size.width, 45);
    [bottomButton setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    [bottomButton setTitle:kLocalizedString(@"alertView_cancelName") forState:UIControlStateNormal];
    [bottomButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    bottomButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    NSArray *imageArray = @[@"my_xiazai"];
    for (int i = 0; i<imageArray.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [bottomView addSubview:button];
        button.frame = CGRectMake((self.frame.size.width / imageArray.count) * i, 0, self.frame.size.width / imageArray.count, 100);
        [button setTitle:kLocalizedString(@"invitation_downPaper") forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        CGFloat imageWith = button.imageView.frame.size.width;
        CGFloat imageHeight = button.imageView.frame.size.height;
        CGFloat labelWidth = button.titleLabel.intrinsicContentSize.width;
        CGFloat labelHeight = button.titleLabel.intrinsicContentSize.height;
        button.imageEdgeInsets = UIEdgeInsetsMake(-labelHeight, 0, 0, -labelWidth);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight, 0);
        button.tag = i;
    }
    
    CGSize size = CGSizeMake([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height);
    UIGraphicsBeginImageContext(size);

//    UIImageView *shareimage = [[UIImageView alloc] initWithFrame:CGRectMake(40, 60, DScreenW - 80, self.frame.size.height - 245)];
//    shareimage.image = screenshotImage;
    [screenshotImage drawInRect:CGRectMake(0, 0, self.frame.size.width , DScreenH - 70)];
    UIImage *imge2 = [UIImage imageNamed:@"K_fotImg"];
    imge2 = [self creatImg:imge2];
    [imge2 drawInRect:CGRectMake(0, DScreenH - 80 ,  self.frame.size.width  , 80)];
    UIImage *ZImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.frame = CGRectMake(40, 60, self.frame.size.width - 80, self.frame.size.height - 245);
    imageV.image = ZImage;
    
    self.img = imageV;
    self.img.contentMode = UIViewContentModeScaleAspectFit;
//    [self addSubview:shareimage];
    [self addSubview:imageV];
}

- (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 {
    UIGraphicsBeginImageContext(image1.size);
     
      // Draw image1
      [image1 drawInRect:CGRectMake(0, 0, image1.size.width, image1.size.height)];
     
      // Draw image2
      [image2 drawInRect:CGRectMake(0, 0, image2.size.width, 40)];
     
      UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
     
      UIGraphicsEndImageContext();
      return resultingImage;
}




- (UIImage *)creatImg:(UIImage *)image1 {
    NSString *myurl = [IWDefault objectForKey:MyInvoteUrl];
    UIImage *image2 = [MMScanViewController createQRImageWithString:myurl QRSize:CGSizeMake(70, 70) QRColor:[UIColor blackColor] bkColor:WhiteColor];
    CGSize size = CGSizeMake(DScreenW, 90);
    UIGraphicsBeginImageContext(size);
    //放第一个图片
    [image1 drawInRect:CGRectMake(0, 10, size.width, 80)];
    //放第二个图片
    [image2 drawInRect:CGRectMake(size.width - 80, 15, 70, 70)];
    
    UIImage *ZImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    self.imageView.image = ZImage;
    return ZImage;
}

/** 按钮点击事件 */
- (void)shareButtonAction:(UIButton *)sender {
    if (self.shareViewDelegate && [self.shareViewDelegate respondsToSelector:@selector(shareButtonAction:index:)]) {
        [self.shareViewDelegate shareButtonAction:self.img index:sender.tag];
        [self dismiss];
    }
}

/** 视图消失 */
- (void)dismiss {
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0.01;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

/** 缩放动画 */
- (void)animationDuration:(CFTimeInterval)duration {
    self.transform = CGAffineTransformMakeScale(0.8, 0.8);
    self.alpha=0;
     [UIView animateWithDuration:0.35 delay:0 usingSpringWithDamping:0.6f initialSpringVelocity:0.8f options:UIViewAnimationOptionCurveEaseOut
                  animations:^{
                      self.transform=CGAffineTransformMakeScale(1, 1);
                      self.alpha=1;
                  }
                  completion:^(BOOL finished){
         
                  }];

//    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
//    animation.duration = duration;
//    animation.removedOnCompletion = NO;
//    animation.fillMode = kCAFillModeForwards;
//    NSMutableArray *values = [NSMutableArray array];
//    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
//    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.05, 1.05, 1.0)]];
//    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
//    animation.values = values;
//    animation.timingFunction = [CAMediaTimingFunction functionWithName:@"easeInEaseOut"];
//    [self.layer addAnimation:animation forKey:nil];
}



@end
