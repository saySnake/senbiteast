//
//  MoreChainView.m
//  Senbit
//
//  Created by 张玮 on 2020/2/22.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "MoreChainView.h"
@interface MoreChainView ()

@property (nonatomic, strong) NSMutableArray *btns;

@property (nonatomic ,strong) UIButton *statsBtn;

@end


@implementation MoreChainView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = WhiteColor;
        //创建按钮
        [self setupBtns];
    }
    return self;
}

- (void)setupBtns {
    self.statsBtn = nil;
    [self createBtn:@"OmniCore" tag:0];
    [self createBtn:@"ERC20" tag:1];
    [self createBtn:@"TRC20" tag:2];
    
    CGFloat btnCount = self.btns.count;
    NSInteger totalCol = 3;
    CGFloat btnW = (DScreenW - 60) / 3;
    CGFloat btnH = 40;
    for (NSInteger index = 0; index < btnCount; index++) {
        UIButton *btn = self.btns[index];
        NSInteger col = index % totalCol;
        NSInteger row = index / totalCol;
        
        CGFloat btnX = 10 + 20 * col + col * btnW;
        CGFloat btnY = row * btnH + row * 10;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        self.chainH = (btnY + btnH);
    }
}


- (UIButton *)createBtn:(NSString *)title tag:(NSInteger)tag {
    UIButton *btn = [[UIButton alloc] init];
    btn.tag = tag;
    if (tag == 0) {
        self.statsBtn = btn;
        self.statsBtn.selected = YES;
        self.statsBtn.borderColor = ThemeGreenColor;
    }
    btn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    [btn setTitleColor:ThemeGreenColor forState:UIControlStateSelected];
    [btn setBackgroundImage:[self imageWithColor:ThemeDarkBlack] forState:UIControlStateSelected];
    [btn setBackgroundImage:[self imageWithColor:ThemeDarkBlack] forState:UIControlStateNormal];
    btn.titleLabel.font = ThirteenFontSize;
    btn.cornerRadius = 3;
    [self addSubview:btn];
    [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btns addObject:btn];
    return btn;
}

 
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)setSeleIdx:(NSInteger)seleIdx {
    _seleIdx = seleIdx;
    self.statsBtn.tag = seleIdx;
    for (UIButton *btn in self.btns) {
        btn.selected = NO;
        btn.borderColor = ThemeDarkBlack;
    }
    UIButton *btn = self.btns[seleIdx];
    btn.selected = YES;
    self.statsBtn = btn;
    self.statsBtn.borderColor = ThemeGreenColor;
}


- (void)btnClicked:(UIButton *)sender {
    if (self.statsBtn == nil) {
        sender.selected = YES;
        self.statsBtn = sender;
    }
    else if (self.statsBtn != nil && self.statsBtn == sender) {
        sender.selected = YES;
    }
    else if (self.statsBtn != sender && self.statsBtn != nil) {
        self.statsBtn.selected = NO;
        sender.selected = YES;
        self.statsBtn = sender;
    }
    
    NSString *message = [sender currentTitle];
    if ([message isEqualToString:@"TRC20"]) {
        sender.tag = 2;
    } else if ([message isEqualToString:@"ERC20"]) {
        sender.tag = 1;
    } else {
        sender.tag = 0;
    }
    self.caluteResult(message,sender.tag);
}

- (NSMutableArray *)btns {
    if (!_btns) {
        self.btns = [NSMutableArray array];
    }
    return _btns;
}


- (void)setChainH:(CGFloat)chainH {
    _chainH = chainH;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}


@end
