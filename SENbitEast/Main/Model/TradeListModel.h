//
//  TradeListModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/13.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//averagePrice = 0;
//createdAt = "2021-05-13T03:26:59.000Z";
//"funds_received" = 0;
//market = "BTC/USDT";
//orderId = 609c9c8328e5000013c94131;
//orderType = limit;
//originVolume = 1;
//price = 4444;
//state = wait;
//tradeType = buy;
//volume = 1;

@interface TradeListModel : NSObject
@property (nonatomic ,copy) NSString *averagePrice;

@property (nonatomic ,copy) NSString *createdAt;

@property (nonatomic ,copy) NSString *market;

@property (nonatomic ,copy) NSString *orderId;

@property (nonatomic ,copy) NSString *orderType;

@property (nonatomic ,copy) NSString *originVolume;

@property (nonatomic ,copy) NSString *price;

@property (nonatomic ,copy) NSString *state;

@property (nonatomic ,copy) NSString *tradeType;

@property (nonatomic ,copy) NSString *volume;


@property (nonatomic ,copy) NSString *amount;
@property (nonatomic ,copy) NSString *fees;
@property (nonatomic ,copy) NSString *tradeId;

@end

NS_ASSUME_NONNULL_END
