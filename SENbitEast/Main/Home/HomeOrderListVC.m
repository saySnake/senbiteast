//
//  HomeOrderListVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "HomeOrderListVC.h"
#import "ComisCurrentCell.h"
#import "MJChiBaoZiHeader.h"
#import "TradeDetailVC.h"
@interface HomeOrderListVC ()<UITableViewDelegate,UITableViewDataSource>{
    int _pageIndex;
}

@property (nonatomic, weak) UIScrollView *titleScroll;
@property (nonatomic, strong) NSMutableArray *titleButton;
@property (nonatomic, weak) UIButton *selectBtn;
@property (nonatomic, strong) UITableView *tbleView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger currentIndex;


@end

@implementation HomeOrderListVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    self.navBar.backgroundColor = WhiteColor;
    [self setUpTitleScroll];
    [self setUpView];
    _pageIndex = 1;
    MJChiBaoZiHeader *header = [MJChiBaoZiHeader headerWithRefreshingTarget:self refreshingAction:@selector(requestUrl)];
    header.lastUpdatedTimeLabel.hidden = YES;    // 隐藏时间
    header.stateLabel.hidden = YES;    // 隐藏状态
//    [header beginRefreshing]; //马上进入刷新
    self.tbleView.mj_header = header; // 设置header
    WS(weakSelf);
     // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tbleView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData1];
    }];
    
    [self requestUrl];
}

//获取当前用户的委托
- (void)requestUrl {
    if (self.currentIndex == 0) {
        [self.dataArr removeAllObjects];
        _pageIndex = 1;
        NSDictionary *dic = @{@"type":@"current",
                              @"source":@"APP",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastEntrustOrderList params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            [self.tbleView.mj_header endRefreshing];

            if (SuccessCode == 200) {
                self.dataArr = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                [self.tbleView reloadData];
                self.tbleView.mj_footer.hidden = NO;
                if (self.dataArr.count == 0) {
                    [self showNotData:@""];
                } else {
                    [self removeNotData];
                }
                if (self.dataArr.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                [self showToastView:SuccessMessage];
                self.tbleView.mj_footer.hidden = NO;
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
            self.tbleView.mj_footer.hidden = NO;

        }];
    }
    
    else if (self.currentIndex == 1) {
        [self removeNotData];
        [self.dataArr removeAllObjects];
        _pageIndex = 1;
        NSDictionary *dic = @{@"type":@"history",
                              @"source":@"APP",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastEntrustOrderList params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                self.dataArr = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                [self.tbleView reloadData];
                [self.tbleView.mj_header endRefreshing];
                self.tbleView.mj_footer.hidden = NO;
                if (self.dataArr.count == 0) {
                    [self showNotData:@""];
                } else {
                    [self removeNotData];
                }
                if (self.dataArr.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    }
}

- (void)loadMoreData1 {
    
    if (self.currentIndex == 0) {
        _pageIndex++;
        NSDictionary *dic = @{@"type":@"current",
                              @"source":@"APP",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastEntrustOrderList params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                NSArray *array = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                [self.dataArr addObjectsFromArray:array];
                [self.tbleView.mj_header endRefreshing];
                [self.tbleView.mj_footer endRefreshing];
                [self.tbleView reloadData];
                self.tbleView.mj_footer.hidden = NO;
                if (array.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
        }];
    } else if (self.currentIndex == 1) {
        
        _pageIndex++;
        NSDictionary *dic = @{@"type":@"history",
                              @"source":@"APP",
                              @"page":@(_pageIndex),
                              @"pageSize":@(20)
        };
        [SBNetworTool getWithUrl:EastEntrustOrderList params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                NSArray *array = [TradeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
                [self.dataArr addObjectsFromArray:array];
                [self.tbleView.mj_header endRefreshing];
                [self.tbleView.mj_footer endRefreshing];
                [self.tbleView reloadData];
                self.tbleView.mj_footer.hidden = NO;
                if (array.count < 20) {
                    self.tbleView.mj_footer.hidden = YES;
                }
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
            self.tbleView.mj_footer.hidden = NO;
        }];
    }
}


- (void)setUpView {
    [self.view addSubview:self.tbleView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ComisCurrentCell *cell = [ComisCurrentCell cellWithTbaleView:tableView];
    cell.currentIndex = self.currentIndex;
    KWeakSelf
    cell.ComisCellBlock = ^(NSString * _Nonnull Id,NSString * _Nonnull market) {
        [weakSelf traderCancel:Id market:market];
    };
    
    cell.finishBlock = ^(TradeListModel * _Nonnull model) {
        [weakSelf detailHisVC:model];
    };

    if (self.currentIndex == 0) {
        cell.model = self.dataArr[indexPath.row];
    } else if (self.currentIndex == 1) {
        cell.model = self.dataArr[indexPath.row];
    }
    return cell;
}


- (void)detailHisVC:(TradeListModel *)model {
    NSLog(@"订单明细");
    TradeDetailVC *detail = [[TradeDetailVC alloc] init];
    detail.model = model;
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)traderCancel:(NSString *)Id market:(NSString *)market {
    NSString *url = [NSString stringWithFormat:@"%@%@/cancel",EastTradeOrder,Id];
    NSDictionary *dic = @{@"market":market};
    [SBNetworTool patchWithUrl:url params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.currentIndex = 0;
            [self requestUrl];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSMutableArray *)titleButton {
    if (!_titleButton) {
        _titleButton = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"entrust_current"),kLocalizedString(@"entrust_his"), nil];
    }
    return _titleButton;
}


- (void)setUpTitleScroll {
    UIScrollView *view = [[UIScrollView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, CGRectGetMaxY(self.navBar.frame), DScreenW, 50);
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 50 - kLinePixel, DScreenW, kLinePixel)];
    line.backgroundColor = HEXCOLOR(0xDDDDDD);
    [view addSubview:line];
    view.showsHorizontalScrollIndicator = NO;
    self.titleScroll = view;
    CGFloat btnW = 100;
    CGFloat btnH = 35;
    for (int i = 0; i < 3; ++i) {
        UIButton *btn = [[UIButton alloc] init];;
        btn.frame = CGRectMake(10 + btnW * i , 5, btnW, btnH);
        [btn setTitle:self.titleButton[i] forState:UIControlStateNormal];
        [btn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn addTarget:self action:@selector(topTitleBtnClick:) forControlEvents:UIControlEventTouchDown];
        btn.tag = i;
        [view addSubview:btn];
        if (btn.tag == 0) {
            self.currentIndex = 0;
            [self topTitleBtnClick:btn];
        }
    }
    [self.view addSubview:view];
}

- (void)topTitleBtnClick:(UIButton *)btn {
    [self selctedBtn:btn];
    if (btn.tag == 0) {
        NSLog(@"当前委托");
        self.currentIndex = 0;
        [self requestUrl];
    } else if (btn.tag == 1) {
        NSLog(@"历史委托");
        self.currentIndex = 1;
        [self requestUrl];
    }
}


- (void)selctedBtn:(UIButton *)btn {
    [_selectBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
    [btn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
    _selectBtn.transform = CGAffineTransformIdentity;
    btn.transform = CGAffineTransformMakeScale(1.3, 1.3);
    _selectBtn = btn;
}


- (UITableView *)tbleView {
    if (_tbleView == nil) {
        _tbleView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScroll.frame), kScreen_Width, kScreen_Height - CGRectGetMaxY(self.titleScroll.frame)) style:UITableViewStylePlain];
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.dataSource = self;
        _tbleView.delegate = self;
        _tbleView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 83 : 65)];
        _tbleView.estimatedRowHeight = 0;
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.estimatedSectionHeaderHeight = 0;
        _tbleView.estimatedSectionFooterHeight = 0;
        if (@available(iOS 11.0, *)) {
            _tbleView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tbleView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}


@end
