//
//  OneTableSockeModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/4.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OneTableSockeModel : NSObject
/** change **/
@property (nonatomic ,copy) NSString *change;
/** high **/
@property (nonatomic ,copy) NSString *high;
/** id **/
@property (nonatomic ,copy) NSString *coinName;
/** last_price **/
@property (nonatomic ,copy) NSString *lastPrice;
/** low **/
@property (nonatomic ,copy) NSString *low;
/** price **/
@property (nonatomic ,copy) NSString *price;
/** recommend **/
@property (nonatomic ,copy) NSString *recommend;
/** sortWeight **/
@property (nonatomic ,assign) double sortWeight;
/** volume **/
@property (nonatomic ,copy) NSString *volume;
/** slected **/
@property (nonatomic ,copy) NSString *selectedCoin;

@property (nonatomic ,assign) BOOL isCollection;

@end

NS_ASSUME_NONNULL_END
