//
//  AccountVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AccountVC.h"
#import "AccountCell.h"
#import "BlandPhoneVC.h"
#import "BlindEmailVC.h"
#import "SetPwdVC.h"
#import "ModifyVC.h"
#import "ModifyPwdVC.h"
#import "BlindGoogleVC.h"

@interface AccountVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic ,strong) UITableView *contentTableView;

/** titleArray **/
@property (nonatomic ,strong) NSMutableArray *titleArray;

@end

@implementation AccountVC

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.contentTableView reloadData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    self.view.backgroundColor = WhiteColor;
    self.navBar.backgroundColor = WhiteColor;
    [self setNavBarTitle:kLocalizedString(@"account_native")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"account_native")]; //kLocalizedString(@"search")];
    [self.contentTableView reloadData];
}

- (void)setUpView {
    [self.view addSubview:self.contentTableView];
    [self.contentTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AccountCell *cell = [AccountCell cellWithTableView:tableView];
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (indexPath.row == 0) {
        if (info.phoneAuthEnable) {
            cell.contentLb.text = kLocalizedString(@"account_modify");
        } else {
            cell.contentLb.text = kLocalizedString(@"account_notBlind");
        }
    } else if (indexPath.row == 1) {
        if (info.emailAuthEnable) {
            cell.contentLb.text = kLocalizedString(@"account_blind");
        } else {
            cell.contentLb.text = kLocalizedString(@"account_notBlind");
        }
    } else if (indexPath.row == 2) {
        if (info.googleAuthEnable) {
            cell.contentLb.text = kLocalizedString(@"account_modify");
        } else {
            cell.contentLb.text = kLocalizedString(@"account_notBlind");
        }
    } else if (indexPath.row == 3) {
        if (info.tradePassword) {
            cell.contentLb.text = kLocalizedString(@"account_modify");
        } else {
            cell.contentLb.text = kLocalizedString(@"account_notSet");
        }
    } else if (indexPath.row == 4) {
        cell.contentLb.text = kLocalizedString(@"account_modify");
    } else if (indexPath.row == 5) {
        
    }
    cell.leftLb.text = self.titleArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (indexPath.row == 0) {
        BlandPhoneVC *phoneVC = [[BlandPhoneVC alloc] init];
        [self.navigationController pushViewController:phoneVC animated:YES];
    } else if (indexPath.row == 1) {
        if (info.emailAuthEnable) {
            return;
        }
        BlindEmailVC *emailVC = [[BlindEmailVC alloc] init];
        [self.navigationController pushViewController:emailVC animated:YES];
    } else if (indexPath.row == 2) {
        if (info.googleAuthEnable) {
            BlindGoogleVC *google = [[BlindGoogleVC alloc] init];
            google.type = BlindGoogleModify;
            [self.navigationController pushViewController:google animated:YES];

        } else {
            BlindGoogleVC *google = [[BlindGoogleVC alloc] init];
            google.type = BlindGoogleType;
            [self.navigationController pushViewController:google animated:YES];
        }
        
    } else if (indexPath.row == 3) {
        if (info.tradePassword) {
            ModifyPwdVC *modi = [[ModifyPwdVC alloc] init];
            [self.navigationController pushViewController:modi animated:YES];
        } else {
            SetPwdVC *pwd = [[SetPwdVC alloc] init];
            [self.navigationController pushViewController:pwd animated:YES];
        }

    } else if (indexPath.row == 4) {
        ModifyVC *modi = [[ModifyVC alloc] init];
        [self.navigationController pushViewController:modi animated:YES];
    }
}

- (UITableView *)contentTableView {
    if (!_contentTableView) {
        _contentTableView        = [[UITableView alloc]initWithFrame:CGRectZero
                                                                           style:UITableViewStylePlain];
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.dataSource          = self;
        _contentTableView.delegate            = self;
        _contentTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [_contentTableView setBackgroundColor:[UIColor whiteColor]];
        _contentTableView.separatorStyle      = UITableViewCellSeparatorStyleSingleLine;
        _contentTableView.tableFooterView = [UIView new];
    }
    return _contentTableView;
}

- (NSMutableArray *)titleArray {
    if (!_titleArray) {
//        _titleArray = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"account_blindPhone"),kLocalizedString(@"account_blindEmail"),kLocalizedString(@"account_google"),kLocalizedString(@"account_setPwd"),kLocalizedString(@"account_modifyLogin"),kLocalizedString(@"account_otcType"), nil];
        _titleArray = [[NSMutableArray alloc] initWithObjects:kLocalizedString(@"account_blindPhone"),kLocalizedString(@"account_blindEmail"),kLocalizedString(@"account_google"),kLocalizedString(@"account_setPwd"),kLocalizedString(@"account_modifyLogin"), nil];

    }
    return _titleArray;
}

@end
