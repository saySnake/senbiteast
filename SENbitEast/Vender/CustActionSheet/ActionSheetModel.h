//
//  ActionSheetModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/30.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActionSheetModel : NSObject
/** 标题 */
@property (nonatomic, copy) NSString *title;
/** 图片 */
@property (nonatomic, copy) NSString *img;

@end

NS_ASSUME_NONNULL_END
