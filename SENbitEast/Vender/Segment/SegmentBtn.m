//
//  SegmentBtn.m
//  Senbit
//
//  Created by 张玮 on 2020/1/13.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SegmentBtn.h"

@implementation SegmentBtn

- (UILabel *)titlelb {
    if (!_titlelb) {
        self.titlelb = [self createlb];;
    }
    return _titlelb;
}

- (UILabel *)subTitlelb {
    if (!_subTitlelb) {
        self.subTitlelb = [self createlb];
        self.subTitlelb.font = [UIFont systemFontOfSize:11];
    }
    return _subTitlelb;
}

- (UILabel *)createlb {
    UILabel *lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.textColor = HEXCOLOR(0xE3FCFF);
    lb.backgroundColor = [UIColor clearColor];
    lb.adjustsFontSizeToFitWidth = YES;
    [self addSubview:lb];
    return lb;
}

- (void)setModel:(SegmentModel *)model {
    _model = model;
    self.titlelb.text = model.title;
    self.subTitlelb.text = model.subTitle;
//    self.titlelb.textColor = [UIColor redColor];//model.titleColor;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    int titlelbW = self.width;
    if (self.model.subTitle.length) {
        self.subTitlelb.frame = CGRectMake(0, self.height - 18, titlelbW, 12);
        self.subTitlelb.alpha = 0.54;
        self.titlelb.frame = CGRectMake(7, 12, titlelbW - 14, 26);
        self.titlelb.adjustsFontSizeToFitWidth = YES;
    } else {
        self.titlelb.frame = CGRectMake(0, 9, titlelbW, 18);
        self.titlelb.font = [UIFont boldSystemFontOfSize:15];
        self.titlelb.textColor = PlaceHolderColor;
    }
}


@end
