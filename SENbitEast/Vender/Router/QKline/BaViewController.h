//
//  BaViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaViewController : UIViewController

/** 导航栏视图 */
@property (strong, nonatomic) UIView *navView;

/** 标题 */
@property (strong, nonatomic) UILabel *titleLabel;

/** 左侧返回按钮 */
@property (strong, nonatomic) UIButton *leftButton;

/** 右侧按钮 */
@property (strong, nonatomic) UIButton *rightButton;

/** 导航栏分割线 */
@property (strong, nonatomic) UIView *navLineView;

/**
 0. 创建导航栏
 */
- (void)createNavigation;

/**
 1. 刷新导航栏样式
 */
- (void)reloadNavigationStyle;

/**
 3. 左侧按钮点击事件
 */
- (void)leftButtonClick:(UIButton *)sender;

/**
 3. 右侧按钮点击事件
 */
- (void)rightButtonClick:(UIButton *)sender;

/**
 4. 接收来网通知
 */
- (void)comeNetNotification;

/**
5. 后台进入前台
*/
- (void)didBecomeActive;

/**
6. 前台台进入后
*/
- (void)didEnterBackground;

@end

NS_ASSUME_NONNULL_END
