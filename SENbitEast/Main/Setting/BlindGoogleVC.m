//
//  BlindGoogleVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/15.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BlindGoogleVC.h"
#import "FSCustomButton.h"
#import "UIImageView+WebCache.h"
#import "LoginSheet.h"
#import "MMScanViewController.h"
#import "BlindPhoneSheet.h"

@interface BlindGoogleVC ()

@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contenLb;
@property (nonatomic ,strong) UILabel *contentScr;
@property (nonatomic ,strong) UIImageView *googleImg;
@property (nonatomic ,strong) FSCustomButton *copBtn;
@property (nonatomic ,strong) FSCustomButton *copImgBtn;

@property (nonatomic ,strong) UIButton *nextBtn;
@property (nonatomic ,strong) NSString *neToken;

@end

@implementation BlindGoogleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    if (self.type == BlindGoogleType) {
        self.titleLb.text = kLocalizedString(@"blind_googleCode");
    } else if (self.type == BlindGoogleModify) {
        self.titleLb.text = kLocalizedString(@"blind_googleModify");
    }
    self.navBar.backgroundColor = WhiteColor;
//    [self setNavBarTitle:kLocalizedString(@"set_navSet")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];
    [self googleInfo];
}


- (void)googleInfo {
    NSDictionary *dic = @{};
    [SBNetworTool getWithUrl:EastGoogleInfo params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            UIImage *ima = [MMScanViewController createQRImageWithString:responseObject[@"data"][@"url"] QRSize:CGSizeMake(250, 250) QRColor:ClearColor bkColor:MainWhiteColor];
            [self.googleImg setImage:ima];
            [self.copBtn setTitle:responseObject[@"data"][@"secret"] forState:UIControlStateNormal];

        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)setUpView {
    [self.view addSubview:self.titleLb];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(40);
        make.width.mas_equalTo(350);
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(10);
    }];
    
    [self.view addSubview:self.contenLb];
    [self.contenLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(15);
        make.width.mas_equalTo(350);
        make.height.mas_equalTo(20);
    }];
    
    [self.view addSubview:self.contentScr];
    [self.contentScr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contenLb.mas_bottom).offset(40);
        make.width.mas_equalTo(350);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.contenLb);
    }];
    
    [self.view addSubview:self.googleImg];
    [self.googleImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.height.mas_equalTo(170);
        make.top.mas_equalTo(self.contentScr.mas_bottom).offset(30);
    }];
    [self.view addSubview:self.copBtn];
    [self.copBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.googleImg);
        make.top.mas_equalTo(self.googleImg.mas_bottom).offset(20);
        make.height.mas_equalTo(25);
        make.width.mas_lessThanOrEqualTo(250);
    }];
    
    [self.view addSubview:self.copImgBtn];
    [self.copImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.copBtn.mas_right).offset(4);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(25);
        make.top.mas_equalTo(self.copBtn);
    }];
    
    [self.view addSubview:self.nextBtn];
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(-40);
    }];
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.textAlignment = 0;
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLb;
}

- (UILabel *)contenLb {
    if (!_contenLb) {
        _contenLb = [[UILabel alloc] init];
        _contenLb.textAlignment = 0;
        _contenLb.font = FourteenFontSize;
        _contenLb.textColor = HEXCOLOR(0x666666);
        _contenLb.text = kLocalizedString(@"blind_googleSafe");
        _contenLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contenLb;
}

- (UILabel *)contentScr {
    if (!_contentScr) {
        _contentScr = [[UILabel alloc] init];
        _contentScr.textAlignment = 0;
        _contentScr.font = FourteenFontSize;
        _contentScr.textColor = [UIColor blackColor];
        _contentScr.text = kLocalizedString(@"blind_quard");
        _contentScr.adjustsFontSizeToFitWidth = YES;
    }
    return _contentScr;
}

- (UIImageView *)googleImg {
    if (!_googleImg) {
        _googleImg = [[UIImageView alloc] init];
        _googleImg.backgroundColor = WhiteColor;
    }
    return _googleImg;
}

- (FSCustomButton *)copImgBtn {
    if (!_copImgBtn) {
        _copImgBtn = [[FSCustomButton alloc] init];
        _copImgBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
//        [_copImgBtn setImage:[UIImage imageNamed:@"fuzhi"] forState:UIControlStateNormal];
        [_copImgBtn setTitle:kLocalizedString(@"blind_copy") forState:UIControlStateNormal];
        _copImgBtn.titleLabel.font = ThirteenFontSize;
        _copImgBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        _copImgBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_copImgBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        [_copImgBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _copImgBtn;
}

- (FSCustomButton *)copBtn {
    if (!_copBtn) {
        _copBtn = [[FSCustomButton alloc] init];
        _copBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
//        [_copBtn setImage:[UIImage imageNamed:@"fuzhi"] forState:UIControlStateNormal];
        _copBtn.titleLabel.font = TwelveFontSize;
        _copBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        _copBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_copBtn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
        [_copBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _copBtn;
}


- (UIButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [[UIButton alloc] init];
        _nextBtn.cornerRadius = 3;
        _nextBtn.backgroundColor = ThemeGreenColor;
        [_nextBtn setTitle:kLocalizedString(@"blind_next") forState:UIControlStateNormal];
        [_nextBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextBtn;
}

- (void)confirm:(UIButton *)sender {
    WS(weakSelf);
    
    LoginSheet * shet = [[LoginSheet alloc] init];
    shet.titleName = @"谷歌绑定";
    if (self.type == BlindGoogleType) {
        shet.type = sheetTypeBlindGoogle;
    } else if (self.type == BlindGoogleModify) {
        shet.type = sheetTypeModifyGoogle;
    }
    shet.block = ^(NSString * _Nonnull str) {
        [weakSelf requestNewToken:str];
    };
    [shet showFromView:self.view];
}

//获取newtoken
- (void)requestNewToken:(NSString *)str {
    if (self.type == BlindGoogleType) {
        NSDictionary *dic = @{@"googleCode":str,
                              @"googleSecret":self.copBtn.currentTitle,
                              @"type":@"NEW_GOOGLE_SECRET"
        };
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                self.neToken = responseObject[@"data"][@"authToken"];
                [self showSheet];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    } else if (self.type == BlindGoogleModify) {
        NSDictionary *dic = @{@"googleCode":str,
                              @"googleSecret":self.copBtn.currentTitle,
                              @"type":@"NEW_GOOGLE_SECRET"
        };
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                self.neToken = responseObject[@"data"][@"authToken"];
                [self showSheet];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];

    }
}

- (void)showSheet {
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    BlindPhoneSheet *sheet = [[BlindPhoneSheet alloc] init];
    if (self.type == BlindGoogleType) {
        sheet.type = BlindTypeTradeGoogle;
    } else if (self.type == BlindGoogleModify) {
        sheet.type = BlindTypeTradeModifyGoogle;
    }
    sheet.googleAuth = info.googleAuthEnable;
    sheet.emailAuth = info.emailAuthEnable;
    sheet.phoneAuth = info.phoneAuthEnable;
    sheet.emailTitleName.text = info.email;
    sheet.phoneLabel.text = info.phone;
    sheet.clickBlock = ^(NSString * _Nonnull phoneCode, NSString * _Nonnull emailCode, NSString * _Nonnull googleCode, NSString * _Nonnull phone, NSString * _Nonnull email, NSString * _Nonnull userToken) {
        if (phoneCode.length == 0) {
            phoneCode = @"";
        }
        if (emailCode.length == 0) {
            emailCode = @"";
        }
        if (googleCode.length == 0) {
            googleCode = @"";
        }
        if (email.length == 0) {
            email = @"";
        }
        if (phone.length == 0) {
            phone = @"";
        }
        NSDictionary *dic = @{};
        if (self.type == BlindGoogleType) {
            dic = @{@"email":info.email,
                      @"emailCode":emailCode,
                      @"phone":info.phone,
                      @"phoneCode":phoneCode,
                      @"googleCode":googleCode,
                      @"type":@"BIND_GOOGLE_SECRET"
            };
        } else if (self.type == BlindGoogleModify) {
            dic = @{@"email":info.email,
                      @"emailCode":emailCode,
                      @"phone":info.phone,
                      @"phoneCode":phoneCode,
                      @"googleCode":googleCode,
                      @"type":@"UPDATE_GOOGLE_SECRET"
            };
        }
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                [self requieAuthToken:responseObject[@"data"][@"authToken"]];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    };
    [sheet showFromView:self.view];
}
//绑定谷歌
- (void)requieAuthToken:(NSString *)str {
    if (self.type == BlindGoogleType) {
        NSDictionary *dic = @{@"googleSecret":self.copBtn.currentTitle,
                              @"newToken":self.neToken,
                              @"authToken":str
        };

        [SBNetworTool postWithUrl:EastBlindGoogle params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                [self showToastView:kLocalizedString(@"blind_success")];
                [self delay:1 task:^{
                    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
                    info.googleAuthEnable = YES;
                    [[CacheManager sharedMnager] saveUserInfo:info];
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showInfo:FailurMessage];
        }];

    } else if (self.type == BlindGoogleModify) {
        NSDictionary *dic = @{@"newGoogleSecret":self.copBtn.currentTitle,
                              @"newToken":self.neToken,
                              @"authToken":str
        };
        [SBNetworTool patchWithUrl:EastResetGoogle params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                [self showToastView:kLocalizedString(@"blind_success")];
                [self delay:1 task:^{
                    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
                    info.googleAuthEnable = YES;
                    [[CacheManager sharedMnager] saveUserInfo:info];
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    }
}

- (void)btnClicked:(UIButton *)sender {
    NSLog(@"复制");
    
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = self.copBtn.currentTitle;
    [self showToastView:kLocalizedString(@"blind_copySuc")];
}


@end
