//
//  WithDrawSheet.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/9.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "WithDrawSheet.h"

#define logShet1 220
#define logShet2 300
#define logShet3 350
@interface WithDrawSheet()<UITextFieldDelegate> {
    NSMutableArray *dataArray;
}
@property (nonatomic ,weak) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *data;

@end

@implementation WithDrawSheet

+ (instancetype)customActionSheet {
    WithDrawSheet *sheet = [[WithDrawSheet alloc] initWithFrame:CGRectZero];
    return sheet;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
    }
    return self;
}

- (void)keyboardWillShow:(NSNotification *)noti {
    NSValue *value = [[noti userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardY = DScreenH - [value CGRectValue].size.height;
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    
    if (info.em) {
        <#statements#>
    }
    
    [UIView animateWithDuration:0.1 animations:^{
        self.containerView.frame = CGRectMake(0, keyboardY - logShetH , DScreenW, logShetH);
    }];
}

- (void)endEdit:(NSNotification *)noti {
    [UIView animateWithDuration:0.1 animations:^{
        self.containerView.frame = CGRectMake(0, DScreenH - logShetH , DScreenW, logShetH);
    }];
}




@end
