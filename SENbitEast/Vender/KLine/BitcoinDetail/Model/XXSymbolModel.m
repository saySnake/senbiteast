//
//  XXCoinPairModel.m
//  iOS
//
//  Created by iOS on 2018/6/26.
//  Copyright © 2018年 iOS. All rights reserved.
//

#import "XXSymbolModel.h"
#import "NSString+SuitScanf.h"


@implementation XXSymbolModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"baseTokenName" : @"sellCurrency",
             @"symbolId":@"id",
             @"symbolName":@"id",
             @"baseTokenId":@"buyCurrency",
             @"quoteTokenId":@"buyCurrency",
             @"quoteTokenName":@"buyCurrency",
             @"basePrecision":@"amountPrecision",
             @"quotePrecision":@"pricePrecision"
             };
}


- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
    if ([property.name isEqualToString:@"basePrecision"]) {
        if (oldValue) {
            NSString *t = @"10";
            for (int i = 1; i < [oldValue intValue]; i++) {
                t = [NSString mulV1:t v2:@"10"];
            }
            t = [NSString divV1:@"1" v2:t];
            return t;
        }
        
    } else if ([property.name isEqualToString:@"quotePrecision"]) {
        if (oldValue) {
            NSString *t = @"10";
            for (int i = 1; i < [oldValue intValue]; i++) {
                t = [NSString mulV1:t v2:@"10"];
            }
            t = [NSString divV1:@"1" v2:t];
            NSLog(@"%@",t);
            return t;
        }
    }
    return oldValue;
}
@end
