//
//  DepositVC.h
//  Senbit
//
//  Created by 张玮 on 2020/2/20.
//  Copyright © 2020 zhangwei. All rights reserved.
//  充币界面

#import "BaseViewController.h"
#import "AssetsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface DepositVC : BaseViewController

@property (nonatomic ,copy) NSString *modelName;

@end

NS_ASSUME_NONNULL_END
