//
//  OtcExtinfo.h
//  Senbit
//
//  Created by 张玮 on 2020/4/6.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "OTCCoinModel.h"
NS_ASSUME_NONNULL_BEGIN
@class OtcCoins;

@interface OtcExtinfo : NSObject <NSCoding>

@property (nonatomic, strong) OtcCoins *otcModel;


//otc数据源
- (instancetype)initWithOtcCoins:(NSArray *)array;
//符号
@property (nonatomic ,strong) NSString *symbol;

@property (nonatomic ,strong) NSString *nomal;
//汇率U
@property (nonatomic ,strong) NSString *USDT;
//KRW
@property (nonatomic ,strong) NSString *KRW;
//CNY
@property (nonatomic ,strong) NSString *CNY;
//台湾
@property (nonatomic ,strong) NSString *HKD;
//合约费率
@property (nonatomic ,strong) NSString *settle;

//交易手续费
@property (nonatomic ,strong) NSString *deliveryFeeRate;
@end

@interface OtcCoins : NSObject<NSCoding>

//@property (nonatomic ,copy) NSArray <OTCCoinModel *> *otcArr;

@end

NS_ASSUME_NONNULL_END
