//
//  TradeCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeBuyCell.h"
#import "UIColor+Y_StockChart.h"
#define LeftSpace K375(15)
#define TopSpace K375(0)

@implementation TradeBuyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.leftLowView];
    /** 左上视图 */
    [self.leftLowView addSubview:self.leftTopView];

    /** 左价格标签 */
    [self.leftLowView addSubview:self.leftPriceLabel];
    
    /** 左成交量 */
    [self.leftLowView addSubview:self.leftNumberLabel];
}

- (void)reloadData {
    if (self.model == nil) {
        self.leftTopView.width = 0;
        self.leftNumberLabel.text = @"--";
        self.leftPriceLabel.text = @"--";
    } else {
        
    }

    self.leftPriceLabel.font = kFont(12.0f);
    self.leftNumberLabel.font = kFont(12.0f);
    
    if (self.model.leftPrice) {
        
        if (self.isAmount) {
//            self.leftNumberLabel.text = [NSString handValuemeLengthWithAmountStr:self.model.leftNumber];
        } else {
//            self.leftNumberLabel.text = [NSString handValuemeLengthWithAmountStr:[NSString stringWithFormat:@"%.13f", self.model.leftSumNumber]];
            self.leftNumberLabel.text = [NSString handValuemeLengthWithAmountStr:self.model.leftNumber];
        }
        
        self.leftPriceLabel.text = self.model.leftPrice;
        if (self.ordersAverage == 0) {
            self.ordersAverage = 1;
        }
        
        CGFloat leftValue = self.model.leftSumNumber/self.ordersAverage;
        if (leftValue >= 1.0) {
            self.leftTopView.width = 0;
        } else {
            self.leftTopView.width = (1.0 - leftValue)*self.leftLowView.width;
        }
    } else {
        self.leftTopView.width = self.leftLowView.width;
        self.leftNumberLabel.text = @"--";
        self.leftPriceLabel.text = @"--";
    }
}

+ (CGFloat)getCellHeight {
    return 32;
}

#pragma mark - || 懒加载
/** 左成交数量 */
- (XXLabel *)leftNumberLabel {
    if (_leftNumberLabel == nil) {
        _leftNumberLabel = [XXLabel labelWithFrame:CGRectMake(CGRectGetMaxX(self.leftLowView.frame) - DScreenW/4 -10, 0, DScreenW/4, 32) text:@"" font:kFont12 textColor:[UIColor blackColor] alignment:2];
        _leftNumberLabel.text = @"--";
        _leftNumberLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _leftNumberLabel;
}

/** 左价格标签 */
- (XXLabel *)leftPriceLabel {
    if (_leftPriceLabel == nil) {
        _leftPriceLabel = [XXLabel labelWithFrame:CGRectMake(K375(5), 0, Kscal(170), 32) font:kFont12 textColor:kGreen100];
        _leftPriceLabel.text = @"--";
        _leftPriceLabel.textAlignment = 0;
        _leftPriceLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _leftPriceLabel;
}

/** 左上版式图 */
- (UIView *)leftTopView {
    if (_leftTopView == nil) {
        _leftTopView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 32)];
        _leftTopView.backgroundColor = [UIColor whiteColor];
        _leftTopView.userInteractionEnabled = NO;
    }
    return _leftTopView;
}

/** 左下版式图 */
- (UIView *)leftLowView {
    if (_leftLowView == nil) {
        _leftLowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DScreenW/2 - 10, 32)];
        _leftLowView.backgroundColor = kGreen20;
    }
    return _leftLowView;
}


@end
