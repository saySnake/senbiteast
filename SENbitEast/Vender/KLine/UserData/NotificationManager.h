//
//  NotificationManager.h
//  iOS
//
//  Created by iOS on 2018/7/15.
//  Copyright © 2018年 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

// 1.1 切换币币交易币对
#define Switch_TradeSymbol_NotificationName @"SwitchTradeSymbolNotificationKey"

// 1.3 币对列表需要更新通知
#define SymbolList_NeedUpdate_NotificationName @"SymbolListNeedUpdateNotificationKey"

// 3.1 【合约】切换交易币对
#define Switch_ContractTradeSymbol_NotificationName @"SwitchContractTradeSymbolNotificationKey"
//3.3通知切换币种（k线或者币币）
#define KnotificationExchangeSelected @"KnotificationExchangeSelected"
//3.4选中切换币种（k线或者币币）
#define KnotificationSelected @"KnotificationSelected"

// 登录
#define Login_In_NotificationName @"loginInNotificationKey"

// 退出
#define Login_Out_NotificationName @"loginOutNotificationKey"

// 来网通知
#define ComeNet_NotificationName @"ComeNetNotificationKey"

// app进入前台通知
#define ApplicationEnterForegroundNotificationName @"applicationWillEnterForeground"

//添加自选通知
#define AddFavorioutficationNmae @"AddFavorioutficationNmae"



@interface NotificationManager : NSObject

/**
 1. 发送登录成功通知
 */
+ (void)postLoginInSuccessNotification;

/**
 2. 发送退出登录成功通知
 */
+ (void)postLoginOutSuccessNotification;

/**
 3. 发送交易切换币对通知 //首页
 */
+ (void)homePostSwitchTradeSymbolNotification:(NSDictionary *)dic;

/**
 3.3 k线交易切换币对通知 //首页
 */
+ (void)tradePostSwitchTradeSymbolNotification:(NSDictionary *)dic;

/**
 3.4 k线交易切换币选中通知 //交易或者k线
 */
+ (void)tradeSelectedSwitchTradeSymbolNotification:(NSDictionary *)dic;

/**
 4. 发送来网通知
 */
+ (void)postComeNetNotification;

/**
 5. 发送app从后台进入前台通知
 */
+ (void)postApplicationEnterForegroundNotification;

/**
 7. 币对列表需要更新通知
 */
+ (void)postSymbolListNeedUpdateNotification;

/**
 11. 【合约】交易切换币对通知
 */
+ (void)postSwitchContractTradeSymbolNotification;


/**
12.添加自选通知
 */
+ (void)postAddFaviroutrNotification;

@end
