//
//  LanguageManager.h
//  Senbit
//
//  Created by 张玮 on 2019/12/15.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//使用demo
//    self.title =kLocalizedString(@"search"); //kLocalizedTableString(@"home", @"HomeLocalizable");
//UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:kLocalizedTableString(@"preference",@"HomeLocalizable") style:UIBarButtonItemStyleDone target:self action:@selector(gotoPreferenceViewController)];
//self.navigationItem.rightBarButtonItem = item;
//
//_titleLabel.text = kLocalizedTableString(@"welcome",@"HomeLocalizable");
//_icoImageView.image = [kLanguageManager ittemInternationalImageWithName:@"github"];

#define ChangeLanguageNotificationName @"changeLanguage"
#define kLocalizedString(key) [kLanguageManager localizedStringForKey:key]
#define kLocalizedTableString(key,tableN) [kLanguageManager localizedStringForKey:key tableName:tableN]

NS_ASSUME_NONNULL_BEGIN

@interface LanguageManager : NSObject

@property (nonatomic,copy) void (^completion)(NSString *currentLanguage);
- (NSString *)currentExchange; //当前汇率
- (NSString *)currentLangSymbol; //当前语言符号
- (NSString *)currentLanguage; //当前语言
- (NSString *)languageFormat:(NSString*)language;
- (void)setUserlanguage:(NSString *)language;//设置当前语言

- (NSString *)localizedStringForKey:(NSString *)key;

- (NSString *)localizedStringForKey:(NSString *)key tableName:(NSString *)tableName;

- (UIImage *)ittemInternationalImageWithName:(NSString *)name;

+ (instancetype)shareInstance;

#define kLanguageManager [LanguageManager shareInstance]

@end

NS_ASSUME_NONNULL_END
