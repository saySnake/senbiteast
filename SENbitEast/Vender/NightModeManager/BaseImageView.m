//
//  BaseImageView.m
//  Senbit
//
//  Created by 张玮 on 2019/12/27.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "BaseImageView.h"
#import "ThemeManager.h"

@implementation BaseImageView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //监听主题切换的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
        
        [self loadThemeImage];
    }
    return self;
}

- (id)initWithImageName:(NSString *)imageName {
    self = [super initWithFrame:CGRectZero];
    if (self != nil) {
        self.imageName = imageName;
        
        //监听主题切换的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
        
        [self loadThemeImage];
    }
    return self;
}

//使用xib创建此类的对象，会调用此方法
- (void)awakeFromNib {
    [super awakeFromNib];
    
    //监听主题切换的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
    
    [self loadThemeImage];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SThemeDidChangeNotification object:nil];
}

- (void)loadThemeImage {
    if(self.imageName.length == 0) {
        return;
    }
    UIImage *image = [[ThemeManager shareInstance] getThemeImage:self.imageName];
    self.image = image;
}

- (void)themeNotification:(NSNotification *)notification {
    [self loadThemeImage];
}

@end
