//
//  AssetsModel.h
//  Senbit
//
//  Created by 张玮 on 2020/4/2.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "coinPairModel.h"
NS_ASSUME_NONNULL_BEGIN
@class coinPairModel;
@interface AssetsModel : NSObject
/** able2charge 充币**/
@property (nonatomic ,assign) BOOL able2charge;
/** able2withdraw 提币**/
@property (nonatomic ,assign) BOOL able2withdraw;
/** 名字 **/
@property (nonatomic ,copy) NSString *ID;
/** enFullName **/
@property (nonatomic ,copy) NSString *enFullName;
/** coinIconUrl **/
@property (nonatomic ,copy) NSString *coinIcon;
/** isOTCPartition **/
@property (nonatomic ,copy) NSString *isOTCPartition;
/** coinPairArray **/
@property (nonatomic ,copy) NSMutableArray<coinPairModel *> *coinPair;
/** currency币种名称 **/
@property (nonatomic ,copy) NSString *currency;
/** 可用 **/
@property (nonatomic ,copy) NSString *balance;
/** 冻结 **/
@property (nonatomic ,strong) NSString *freezed;
/** 锁仓 **/
@property (nonatomic ,strong) NSString *locked;

//合约的
/** BTC **/
@property (nonatomic ,copy) NSString *BTC;
/** ETH **/
@property (nonatomic ,copy) NSString *ETH;
/** USDT **/
@property (nonatomic ,copy) NSString *USDT;





@end

NS_ASSUME_NONNULL_END
