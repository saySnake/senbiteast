//
//  CPTableViewCell.m
//  CountryPicker
//
//  Created by 张玮 on 2020/1/10.
//  Copyright © 2020 Lv Qingyang. All rights reserved.
//

#import "CPTableViewCell.h"

@implementation CPTableViewCell

+ (CPTableViewCell *)cell {
    CPTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"CPTableViewCell" owner:self options:nil] lastObject];
    cell.contentView.backgroundColor = WhiteColor;
    return cell;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
