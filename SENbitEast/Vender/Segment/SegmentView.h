//
//  SegmentView.h
//  Senbit
//
//  Created by 张玮 on 2020/1/13.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseView.h"
#import "SegmentModel.h"
#import "SegmentBtn.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SegmentClickBlock)(NSInteger index);

@interface SegmentView : BaseView

/** 分段数组 */
@property (nonatomic, strong) NSArray *segments;
/** 点击索引 */
@property (nonatomic, copy) SegmentClickBlock segmentClicked;

/** 点击的类型 */
@property (nonatomic, assign) SegmentModel *selectModel;

/** 滚动视图的索引 */
@property (nonatomic, assign) NSInteger segmentIndex;

@property (nonatomic, strong) UIColor *lineColor;

@property (nonatomic, strong) UIColor *titleColor;

@end

NS_ASSUME_NONNULL_END
