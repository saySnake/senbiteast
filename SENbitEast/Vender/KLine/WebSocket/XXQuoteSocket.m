//
//  XXQuoteSocket.m
//  iOS
//
//  Created by iOS on 2018/8/29.
//  Copyright © 2018年 iOS. All rights reserved.
//

#import "XXQuoteSocket.h"
#import <zlib.h>

@import SocketIO;


@interface XXQuoteSocket () <SRWebSocketDelegate> {
    
}
@property (strong, nonatomic) SocketManager *manager;
@property (strong, nonatomic) SocketIOClient *socket;
@property (assign, nonatomic) BOOL klineConnect;
@property (assign, nonatomic) BOOL currentKline;
@property (assign, nonatomic) BOOL depConnect;
@property (assign, nonatomic) BOOL tradConnect;
@property (assign, nonatomic) BOOL currentOverConnect;
@property (assign, nonatomic) BOOL homeCurrentOverConnect;
@property (assign, nonatomic) BOOL homeCoinsConnect;
@property (assign, nonatomic) BOOL entrustConnect;

/** 索引id */
@property (assign, nonatomic) NSInteger indexId;

/** 所有订阅模型 */
@property (strong, nonatomic) NSMutableDictionary *dataDictionary;


@end

@implementation XXQuoteSocket
singleton_implementation(XXQuoteSocket)


//- (void)setManager:(SocketManager *)manager {
//    NSString *cok = [IWDefault objectForKey:Cookie];
//    NSString *trpp = [IWDefault objectForKey:TradeApp];
//    BOOL s = [IWDefault boolForKey:@"secure"];
//    NSURL *url = [NSURL URLWithString:WssUrl];
//    if (cok) {
//        manager = [[SocketManager alloc]
//                        initWithSocketURL:url
//                        config:@{
//                            @"log": @NO,
//                            @"forcePolling": @NO,
//                            @"secure": @(s),
//                            @"forceNew":@YES,
//                            @"forceWebsockets":@YES,
//                            @"selfSigned":@YES,
//                            @"reconnectWait":@1000,
//                            @"transports":@"websocket",
//                            @"reconnection":@YES,
//                            @"extraHeaders":@{
//                                    @"Cookie":cok,
//                                    @"User-Agent":trpp
//                                }
//                            }];
//    } else {
//        manager = [[SocketManager alloc]
//                        initWithSocketURL:url
//                        config:@{
//                            @"log": @NO,
//                            @"forcePolling": @NO,
//                            @"secure": @(s),
//                            @"forceNew":@YES,
//                            @"forceWebsockets":@YES,
//                            @"selfSigned":@YES,
//                            @"reconnectWait":@1000,
//                            @"transports":@"websocket",
//                            @"reconnection":@YES,
//                            }];
//    }
////    self.socket = [manager defaultSocket];
//}
//
//- (void)setSocket:(SocketIOClient *)socket {
//    _socket = socket;
//    _socket = [self.manager defaultSocket];
//}


- (instancetype)init {
    self = [super init];
    if (self) {
        BOOL s = [IWDefault boolForKey:@"secure"];
        NSURL *url = [NSURL URLWithString:WssUrl];
        
        NSString *cok = [IWDefault objectForKey:Cookie];
        NSString *trpp = [IWDefault objectForKey:TradeApp];
//        if (cok) {
//            self.manager = [[SocketManager alloc]
//                            initWithSocketURL:url
//                            config:@{
//                                @"log": @NO,
//                                @"forcePolling": @NO,
//                                @"secure": @(s),
//                                @"forceNew":@YES,
//                                @"forceWebsockets":@YES,
//                                @"selfSigned":@YES,
//                                @"reconnectWait":@1000,
//                                @"transports":@"websocket",
//                                @"reconnection":@YES,
//                                @"extraHeaders":@{
//                                        @"Cookie":cok,
//                                        @"User-Agent":trpp
//                                    }
//                                }];
//        } else {
            self.manager = [[SocketManager alloc]
                            initWithSocketURL:url
                            config:@{
                                @"log": @NO,
                                @"forcePolling": @NO,
                                @"secure": @(s),
                                @"forceNew":@YES,
                                @"forceWebsockets":@YES,
                                @"selfSigned":@YES,
                                @"reconnectWait":@1000,
                                @"transports":@"websocket",
                                @"reconnection":@YES,
                                }];
//        }
        self.socket = [self.manager defaultSocket];
        // 1. 来网通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveComeNetNotificationName) name:ComeNet_NotificationName object:nil];
    }
    return self;
}


- (void)reloadManager:(NSDictionary *)managerInfo {
    NSURL *url = [NSURL URLWithString:WssUrl];
    self.manager = [[SocketManager alloc] initWithSocketURL:url config:managerInfo];
    self.socket = [self.manager defaultSocket];
}

#pragma mark - 1.3 接收到来网通知
- (void)didReceiveComeNetNotificationName {
    [self openWebSocket];
}

- (void)closeWebSocket {
    [self.socket connect];
}
#pragma mark - 3.0 订阅
- (void)sendWebSocketSubscribeWithWebSocketModel:(XXWebQuoteModel *)model lineStype:(NSString *)lineStype {
    //    遍历完以后看有没有现成的链接对象，有就直接赋值
//    for (NSString *modelKey in self.dataDictionary.allKeys) {
//        if ([modelKey isEqualToString:lineStype]) {
//            model = self.dataDictionary[modelKey];
//        }
//    }
    
    self.klineConnect = NO;
    self.currentKline = NO;
    self.depConnect = NO;
    self.tradConnect = NO;
    self.currentOverConnect = NO;
    self.homeCurrentOverConnect = NO;
    //    没有就保存，然后去连接socket
    NSLog(@"%@",lineStype);
    [self.dataDictionary removeAllObjects];
    [self.dataDictionary setValue:model forKey:lineStype];
    NSString *a = [IWDefault objectForKey:@"first"];
    [IWDefault setObject:@"1" forKey:@"first"];
    [IWDefault synchronize];
    if (!a) {
        [self.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
            for (NSString *modelKey in self.dataDictionary.allKeys) {
                NSLog(@"__________%@",modelKey);
                XXWebQuoteModel *m = self.dataDictionary[modelKey];
                NSDictionary *dic = m.params[@"data"];
                if ([modelKey isEqualToString:@"currency-kline"]) {
                    [self.socket emit:@"currency-kline:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"currency-kline-history"]) {
                    [self.socket emit:@"currency-kline-history:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"currency-depth"]) {
                    [self.socket emit:@"currency-depth:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"currency-latest-trade"]) {
                    [self.socket emit:@"currency-latest-trade:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"currency-overview"]) {
                    [self.socket emit:@"currency-overview:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"home-coins"]) {
                    [self.socket emit:@"home-coins:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"home-overview"]) {
                    [self.socket emit:@"home-overview:subscribe" with:@[dic]];
                } else if ([modelKey isEqualToString:@"currency-entrust"]) {
                    [self.socket emit:@"currency-entrust:subscribe" with:@[dic]];
                }
            }
        }];
    } else {
        for (NSString *modelKey in self.dataDictionary.allKeys) {
            XXWebQuoteModel *m = self.dataDictionary[modelKey];
            NSDictionary *dic = m.params[@"data"];
            if ([modelKey isEqualToString:@"currency-kline"]) {
                if (!self.klineConnect) {
                    [self.socket emit:@"currency-kline:subscribe" with:@[dic]];
                    self.klineConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-kline-history"]) {
                if (!self.currentKline) {
                    [self.socket emit:@"currency-kline-history:subscribe" with:@[dic]];
                    self.currentKline = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-depth"]) {
                if (!self.depConnect) {
                    [self.socket emit:@"currency-depth:subscribe" with:@[dic]];
                    self.depConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-latest-trade"]) {
                if (!self.tradConnect) {
                    [self.socket emit:@"currency-latest-trade:subscribe" with:@[dic]];
                    self.tradConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-overview"]) {
                if (!self.currentOverConnect) {
                    [self.socket emit:@"currency-overview:subscribe" with:@[dic]];
                    self.currentOverConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"home-overview"]) {
                if (!self.homeCurrentOverConnect) {
                    [self.socket emit:@"home-overview:subscribe" with:@[dic]];
                    self.homeCurrentOverConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"home-coins"]) {
                if (!self.homeCoinsConnect) {
                    [self.socket emit:@"home-coins:subscribe" with:@[dic]];
                    self.homeCoinsConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-entrust"]) {
                if (!self.entrustConnect) {
                    [self.socket emit:@"currency-entrust:subscribe" with:@[dic]];
                    self.entrustConnect = YES;
                }
            }
        }
    }
    
    [self.socket on:@"error" callback:^(NSArray * data, SocketAckEmitter * ack) {
        NSLog(@"socket.io error %@",data);
    }];

    [self.socket on:@"disconnect" callback:^(NSArray * _Nonnull, SocketAckEmitter * _Nonnull) {
        NSLog(@"链接断开");
        for (NSString *modelKey in self.dataDictionary.allKeys) {
            XXWebQuoteModel *m = self.dataDictionary[modelKey];
            NSDictionary *dic = m.params[@"data"];
            if ([modelKey isEqualToString:@"currency-kline"]) {
                if (!self.klineConnect) {
                    [self.socket emit:@"currency-kline:subscribe" with:@[dic]];
                    self.klineConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-kline-history"]) {
                if (!self.currentKline) {
                    [self.socket emit:@"currency-kline-history:subscribe" with:@[dic]];
                    self.currentKline = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-depth"]) {
                if (!self.depConnect) {
                    [self.socket emit:@"currency-depth:subscribe" with:@[dic]];
                    self.depConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-latest-trade"]) {
                if (!self.tradConnect) {
                    [self.socket emit:@"currency-latest-trade:subscribe" with:@[dic]];
                    self.tradConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-overview"]) {
                if (!self.currentOverConnect) {
                    [self.socket emit:@"currency-overview:subscribe" with:@[dic]];
                    self.currentOverConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"home-overview"]) {
                if (!self.homeCurrentOverConnect) {
                    [self.socket emit:@"home-overview:subscribe" with:@[dic]];
                    self.homeCurrentOverConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"home-coins"]) {
                if (!self.homeCoinsConnect) {
                    [self.socket emit:@"home-coins:subscribe" with:@[dic]];
                    self.homeCoinsConnect = YES;
                }
            } else if ([modelKey isEqualToString:@"currency-entrust"]) {
                if (!self.entrustConnect) {
                    [self.socket emit:@"currency-entrust:subscribe" with:@[dic]];
                    self.entrustConnect = YES;
                }
            }
        }
    }];


    
    NSString * type = model.params[@"topic"];
    if ([type isEqualToString:@"currency-kline"]) {
        [self.socket on:@"currency-kline" callback:^(NSArray* data, SocketAckEmitter * ack) {
            model.successBlock(data[0][@"data"]);
        }];
    } else if ([type isEqualToString:@"currency-kline-history"]) {
        [self.socket on:@"currency-kline-history" callback:^(NSArray* data, SocketAckEmitter* ack) {
//             NSLog(@"%@",data);
            model.successBlock(data[0][@"data"]);
        }];
    } else if ([type isEqualToString:@"currency-depth"]) {
        [self.socket on:@"currency-depth" callback:^(NSArray * data, SocketAckEmitter * ack) {
//            NSLog(@"%@",data);
            model.successBlock(data);
        }];
    } else if ([type isEqualToString:@"currency-latest-trade"]) {
        [self.socket on:@"currency-latest-trade" callback:^(NSArray * data, SocketAckEmitter * ack) {
//            NSLog(@"%@",data);
            model.successBlock(data);
        }];
    } else if ([type isEqualToString:@"currency-overview"]) {
        [self.socket on:@"currency-overview" callback:^(NSArray * data, SocketAckEmitter * ack) {
//            NSLog(@"%@",data);
            model.successBlock(data);
        }];
    } else if ([type isEqualToString:@"home-overview"]) {
        [self.socket on:@"home-overview" callback:^(NSArray * data, SocketAckEmitter * ack) {
//            NSLog(@"%@",data);
            model.successBlock(data[0]);
        }];
    } else if ([type isEqualToString:@"home-coins"]) {
        [self.socket on:@"home-coins" callback:^(NSArray * data, SocketAckEmitter * ack) {
//            NSLog(@"%@",data);
            model.successBlock(data[0]);
        }];
    } else if ([type isEqualToString:@"currency-entrust"]) {
        [self.socket off:@"currency-entrust"];
        [self.socket on:@"currency-entrust" callback:^(NSArray * data, SocketAckEmitter * ack) {
//            NSLog(@"%@",data);
            model.successBlock(data[0]);
        }];
    }
    [self.socket connect];
}

- (void)disconnect {
    [self.socket disconnect];
}

#pragma mark - 3.1 取消订阅*/
- (void)cancelWebSocketSubscribeWithWebSocketModel:(XXWebQuoteModel *)model lineType:(NSString *)lineType {
    self.currentKline = NO;
    self.klineConnect = NO;
    self.depConnect = NO;
    self.tradConnect = NO;
    self.currentOverConnect = NO;
    self.homeCurrentOverConnect = NO;
    self.homeCoinsConnect = NO;
    self.entrustConnect = NO;

    if ([lineType isEqualToString:@"currency-kline"]) {
        [self.socket emit:@"currency-kline:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"currency-kline-history"]) {
        [self.socket emit:@"currency-kline-history:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"currency-depth"]) {
//        [self.socket emit:@"currency-depth:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"currency-latest-trade"]) {
        [self.socket emit:@"currency-latest-trade:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"currency-overview"]) {
        [self.socket emit:@"currency-overview:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"home-coins"]) {
        [self.socket emit:@"home-coins:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"home-overview"]) {
        [self.socket emit:@"home-overview:unsubscribe" with:@[]];
        
    } else if ([lineType isEqualToString:@"currency-entrust"]) {
        [self.socket emit:@"currency-entrust:unsubscribe" with:@[]];
    }
}

#pragma mark - 4. 解压缩
- (NSData *)gzipInflate:(NSData*)data {
    if ([data length] == 0) {
        return data;
    }
    
    unsigned long full_length = [data length];
    unsigned long  half_length = [data length] / 2;
    
    NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
    BOOL done = NO;
    int status;
    
    z_stream strm;
    strm.next_in = (Bytef *)[data bytes];
    strm.avail_in = (uInt)[data length];
    strm.total_out = 0;
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    
    if (inflateInit2(&strm, (15+32)) != Z_OK) {
        return nil;
    }
    
    while (!done) {
        // Make sure we have enough room and reset the lengths.
        if (strm.total_out >= [decompressed length])
            [decompressed increaseLengthBy: half_length];
        strm.next_out = [decompressed mutableBytes] + strm.total_out;
        strm.avail_out = (uInt)([decompressed length] - strm.total_out);
        
        // Inflate another chunk.
        status = inflate (&strm, Z_SYNC_FLUSH);
        if (status == Z_STREAM_END) {
            done = YES;
        } else if (status != Z_OK) {
            break;
        }
    }
    if (inflateEnd (&strm) != Z_OK) {
        return nil;
    }
    
    // Set real length.
    if (done) {
        [decompressed setLength: strm.total_out];
        return [NSData dataWithData: decompressed];
    } else {
        return nil;
    }
}
- (void)umFailureError:(NSError *)error {
    
}

- (NSMutableDictionary *)dataDictionary {
    if (_dataDictionary == nil) {
        _dataDictionary = [NSMutableDictionary dictionary];
    }
    return _dataDictionary;
}

@end
