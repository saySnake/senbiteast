//
//  SBSafeSheet.h
//  Senbit
//
//  Created by 张玮 on 2020/3/23.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseView.h"
#import "TimeBtn.h"

NS_ASSUME_NONNULL_BEGIN


typedef enum : NSUInteger {
    SafeSheetEmail, //找回密码邮箱
    SafeSheetPhone, //找回密码手机
    SafeSheetRegister, //手机号注册
    SafeSheetEmailRegister, //邮箱注册
} SafeSheetType;

/**
 图片位置
 */

typedef void (^ClickBlock) (NSString * str);

@interface SafeSheet : BaseView

@property (nonatomic, assign) SafeSheetType type;
@property (nonatomic ,strong) UIButton *cancleBtn;

/** 点击事件 */
@property (nonatomic, copy) ClickBlock clickBlock;

@property (nonatomic ,strong) UILabel *titleName;

@property (nonatomic ,strong) UILabel *line1;

@property (nonatomic ,strong) UILabel *msgLbel;

@property (nonatomic ,strong) UITextField *msgTF;

@property (nonatomic ,strong) UILabel *line2;

@property (nonatomic ,strong) UIButton *confirBtn;

@property (nonatomic ,strong) TimeBtn *msgBtn;

//手机号或者邮箱
@property (nonatomic ,strong) NSString *account;

//区号
@property (nonatomic ,strong) NSString *countryCode;

//区号
@property (nonatomic ,strong) NSString *country;

//locoal
@property (nonatomic ,strong) NSString *locoal;

//找回密码code
@property (nonatomic ,strong) NSString *code;


- (void)showFromView:(UIView *)view;

+ (instancetype)customActionSheets;

- (void)closeAcitonSheet;

@end

NS_ASSUME_NONNULL_END
