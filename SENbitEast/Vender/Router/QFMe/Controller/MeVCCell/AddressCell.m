//
//  AddressCell.m
//  Senbit
//
//  Created by 张玮 on 2020/2/23.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"AddressCell";
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[AddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.contentView.backgroundColor = WhiteColor;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        
        [self setUpView];
    }
    return self;
}

- (void)setModel:(AddressModel *)model {
    _model = model;
    if ([model isKindOfClass:[AddressModel class]]) {
        if ([model.coin isEqualToString:@"EUSDT"]) {
            self.linkLb.hidden = NO;
            self.content.text = [NSString stringWithFormat:@"%@ %@",@"USDT",model.title];
            self.linkLb.text = @"ERC20";
        } else if ([model.coin isEqualToString:@"TUSDT"]) {
            self.content.text = [NSString stringWithFormat:@"%@ %@",@"USDT",model.title];
            self.linkLb.hidden = NO;
            self.linkLb.text = @"TRC20";
        } else if ([model.coin isEqualToString:@"USDT"]) {
            self.content.text = [NSString stringWithFormat:@"%@ %@",@"USDT",model.title];
            self.linkLb.text = @"OMNI";
            self.linkLb.hidden = NO;
        } else {
            self.content.text = [NSString stringWithFormat:@"%@ %@",model.coin,model.title];
        }
        self.addreText.text = model.address;
        self.markLb.text = model.destinationTag;
    }
}

- (void)setUpView {
    self.img = [[UIImageView alloc] init];
    self.img.image = [UIImage imageNamed:@"Assert_addressImg"];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.img];
    
    self.content = [[UILabel alloc] init];
    self.content.textAlignment = 0;
    self.content.font = BOLDSYSTEMFONT(15);
    self.content.textColor = [UIColor blackColor];
    self.content.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.content];
    
    [self.contentView addSubview:self.markTitle];
    
    self.markLb = [[UILabel alloc] init];
    self.markLb.font = kFont12;
    self.markLb.textColor = ThemeLittleTitleColor;
    self.markLb.textAlignment = 0;
    [self.contentView addSubview:self.markLb];
    
    self.addreText = [[UILabel alloc] init];
    self.addreText.font = TwelveFontSize;
    self.addreText.textAlignment = 0;
    self.addreText.textColor = [UIColor blackColor];
    self.addreText.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.addreText];
    
    self.deleBtn = [[UIButton alloc] init];
    [self.deleBtn setTitle:kLocalizedString(@"his_dele") forState:UIControlStateNormal];
    self.deleBtn.titleLabel.font = kFont15;
    self.deleBtn.backgroundColor = ThemeRedColor;
    [self.deleBtn addTarget:self action:@selector(deleAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.deleBtn];
    
    [self.contentView addSubview:self.linkLb];
    [self make_layout];
}

- (void)deleAction {
    self.deleteBtnBlock();
}

- (void)make_layout {
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(10.5);
        make.height.mas_equalTo(12);
    }];
    
    [self.content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.img.mas_right).offset(10);
        make.top.mas_equalTo(7);
        make.right.mas_equalTo(-80);
        make.height.mas_equalTo(25);
    }];
    
    
    
    [self.markLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.content.mas_left);
        make.top.mas_equalTo(self.content.mas_bottom).offset(5);
        make.right.mas_equalTo(self.content.mas_right);
        make.height.mas_equalTo(15);
    }];
    
    [self.markTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.markLb.mas_left);
        make.top.mas_equalTo(self.markLb.mas_bottom).offset(5);
        make.right.mas_equalTo(self.content.mas_right);
        make.height.mas_equalTo(20);
    }];
    
    [self.addreText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.markTitle.mas_left);
        make.top.mas_equalTo(self.markTitle.mas_bottom).offset(5);
        make.right.mas_equalTo(self.content.mas_right);
        make.height.mas_equalTo(20);
    }];
    
    [self.linkLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(25);
    }];
    
//    [self.deleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.contentView);
//        make.right.mas_equalTo(-10);
//        make.width.mas_equalTo(80);
//        make.height.mas_equalTo(60);
//    }];
}

- (UILabel *)markTitle {
    if (!_markTitle) {
        _markTitle = [[UILabel alloc] init];
        _markTitle.textColor = HEXCOLOR(0xC4C4C4);
        _markTitle.font = kFont(12);
        _markTitle.textAlignment = 0;
        _markTitle.text = kLocalizedString(@"addCoinAddress_tag");
    }
    return _markTitle;
}

- (UILabel *)linkLb {
    if (!_linkLb) {
        _linkLb = [[UILabel alloc] init];
        _linkLb.backgroundColor = HEXCOLOR(0xe5f5f5);
        _linkLb.hidden = YES;
        _linkLb.font = kFont(14);
        _linkLb.adjustsFontSizeToFitWidth = YES;
        _linkLb.textColor = ThemeGreenColor;
        _linkLb.textAlignment = 1;
        _linkLb.font = kFont14;
    }
    return _linkLb;
}

//- (void)setFrame:(CGRect)frame {
//    frame.origin.x = 10;
//    frame.size.width -= 20;
//    frame.size.height -= 5;
//    frame.origin.y += 5;
//    [super setFrame:frame];
//}

@end
