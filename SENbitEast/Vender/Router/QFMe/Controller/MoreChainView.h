//
//  MoreChainView.h
//  Senbit
//
//  Created by 张玮 on 2020/2/22.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^ChainViewBlock) (NSString *restult , NSInteger idx);

@interface MoreChainView : BaseView

@property (nonatomic, copy) ChainViewBlock caluteResult;

- (instancetype)initWithFrame:(CGRect)frame;

@property (nonatomic ,assign) CGFloat chainH;

@property (nonatomic ,assign) NSInteger seleIdx;
@end

NS_ASSUME_NONNULL_END
