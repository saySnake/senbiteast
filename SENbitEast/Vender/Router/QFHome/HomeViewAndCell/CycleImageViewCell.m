//
//  CycleImageViewCell.m
//  CycleIMG
//
//  Created by 张玮 on 2020/1/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "CycleImageViewCell.h"
#import "NSString+SuitScanf.h"


@interface CycleImageViewCell ()


@end

@implementation CycleImageViewCell

//+ (instancetype)cellWithTableView:(UITableView *)tableView {
//    static NSString *Indentify =@"Indentify";
//    HomeCell *cell =[tableView dequeueReusableCellWithIdentifier:Indentify ];
//    if (!cell) {
//        cell =[[[NSBundle mainBundle]loadNibNamed:@"HomeCell" owner:self options:nil] lastObject];
//    }
//    return cell;
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = WhiteColor;
    [self.perName setCornerRadius:2];
    self.titleName.font = BOLDSYSTEMFONT(14);
    self.titleName.adjustsFontSizeToFitWidth = YES;
    self.titleName.textColor = [UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"];
    self.usdName.textColor = MainWhiteColor;
    self.usdName.font = BOLDSYSTEMFONT(20);
    self.perName.font = BOLDSYSTEMFONT(14);
    self.perName.adjustsFontSizeToFitWidth = YES;
    self.cnyName.adjustsFontSizeToFitWidth = YES;
    self.cnyName.textColor = [UIColor colorWithLightColorStr:@"A0A0A0" DarkColor:@"A0A0A0"];
    [self make_layout];
}

- (void)setModel:(OneTableSockeModel *)model {
    _model = model;
    OtcExtinfo *info = [[CacheManager sharedMnager] getOtcInfo];
    self.titleName.text = model.coinName;
    
    NSRange range;
    range = [model.coinName rangeOfString:@"/"];
    NSMutableAttributedString *strs = [[NSMutableAttributedString alloc] initWithString:model.coinName];
    [strs addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"] range:NSMakeRange(0, range.location)];
    self.titleName.attributedText = strs;
    self.usdName.text = model.price;
    NSString *per = [NSString returnFormatter:[NSString mulV1:model.change v2:@"100"]];
    if (per.floatValue > 0) {
        per = [NSString stringWithFormat:@"+%.2f%%",per.floatValue];
        self.perName.textColor = [UIColor colorWithLightColorStr:@"00A19B" DarkColor:@"00A19B"];
        self.usdName.textColor = [UIColor colorWithLightColorStr:@"00A19B" DarkColor:@"00A19B"];
    } else {
        per = [NSString stringWithFormat:@"%.2f%%",per.floatValue];
        self.perName.textColor = [UIColor colorWithLightColorStr:@"ED2482" DarkColor:@"ED2482"];
        self.usdName.textColor = [UIColor colorWithLightColorStr:@"ED2482" DarkColor:@"ED2482"];
    }
    self.perName.cornerRadius = 3;
//    NSString *cnyText = [NSString stringWithFormat:@"%@",[NSString mulV1:model.price v2:kLanguageManager.currentExchange]];
    self.cnyName.text = [NSString stringWithFormat:@"%@",[[RatesManager sharedRatesManager] getRatesPriceValue:[model.price doubleValue]]];
//    [NSString stringWithFormat:@"≈%@ %@",[NSString returnFormatter:cnyText],kLanguageManager.currentLangSymbol];
    self.perName.text = per;
}

- (void)make_layout {
    
}

//- (void)setModel:(CRModel *)model {
//    _model = model;
//}
@end
