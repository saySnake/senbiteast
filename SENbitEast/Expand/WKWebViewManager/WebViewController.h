//
//  WebViewController.h
//  Senbit
//
//  Created by 张玮 on 2020/4/5.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseViewController.h"
#import "HXWKWebView.h"

NS_ASSUME_NONNULL_BEGIN

@class WebViewController;

@protocol WebViewControllerDelegate <NSObject>

@optional

- (void)webViewControllerL:(WebViewController *)webView loginAction:(NSString *)login;

- (void)webViewControllerL:(WebViewController *)webView token:(NSString *)token name:(NSString *)name;

- (void)webViewControllerL:(WebViewController *)webView auth:(NSString *)auth;

- (void)webViewControllerL:(WebViewController *)webView setPw:(NSString *)pw;

- (void)webViewControllerL:(WebViewController *)webView version:(NSString *)version name:(NSString *)name;

@end

@interface WebViewController : BaseViewController

@property (nonatomic ,strong) HXWKWebView *wkWebView;

@property (nonatomic ,assign) BOOL isShare;

@property (nonatomic ,assign) BOOL isDelete;

/** URL **/
@property (nonatomic ,copy) NSString *url;

@property (nonatomic, weak) id <WebViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
