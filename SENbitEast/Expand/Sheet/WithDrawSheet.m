//
//  SafeInfoSheet.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/24.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "WithDrawSheet.h"
#import "TipsView.h"
#define SafeShetH 230
#define SafeShetH2 310
#define SafeShetH3 400


@interface WithDrawSheet()<UITextFieldDelegate>

@property (nonatomic ,weak) UIView *containerView;

@end

@implementation WithDrawSheet

+ (instancetype)customActionSheets {
    WithDrawSheet *sheet = [[WithDrawSheet alloc] initWithFrame:CGRectZero];
    return sheet;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        [self addGestureRecognizer:tap];
    }
    return self;
}


- (void)keyboardWillShow:(NSNotification *)noti {
    NSValue *value = [[noti userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardY = DScreenH - [value CGRectValue].size.height;
    [UIView animateWithDuration:0.1 animations:^{
        if (self.googleAuth && self.emailAuth && self.phoneAuth) {
            self.containerView.frame = CGRectMake(0, keyboardY - SafeShetH3 , DScreenW, SafeShetH3);
        } else if (self.googleAuth && self.emailAuth) {
            self.containerView.frame = CGRectMake(0, keyboardY - SafeShetH2 , DScreenW, SafeShetH2);
        } else if (self.googleAuth && self.phoneAuth) {
            self.containerView.frame = CGRectMake(0, keyboardY - SafeShetH2 , DScreenW, SafeShetH2);
        } else if (self.emailAuth && self.phoneAuth) {
            self.containerView.frame = CGRectMake(0, keyboardY - SafeShetH2 , DScreenW, SafeShetH2);
        } else {
            self.containerView.frame = CGRectMake(0, keyboardY - SafeShetH , DScreenW, SafeShetH);
        }
    }];
}

- (void)endEdit:(NSNotification *)noti {
    [UIView animateWithDuration:0.1 animations:^{
        if (self.googleAuth && self.emailAuth && self.phoneAuth) {
            self.containerView.frame = CGRectMake(0, DScreenH - SafeShetH3 , DScreenW, SafeShetH3);
        } else if (self.googleAuth && self.emailAuth) {
            self.containerView.frame = CGRectMake(0, DScreenH - SafeShetH2 , DScreenW, SafeShetH2);
        } else if (self.emailAuth && self.phoneAuth) {
            self.containerView.frame = CGRectMake(0, DScreenH - SafeShetH2 , DScreenW, SafeShetH2);
        } else if (self.phoneAuth && self.googleAuth ) {
            self.containerView.frame = CGRectMake(0, DScreenH - SafeShetH2 , DScreenW, SafeShetH2);
        } else {
            self.containerView.frame = CGRectMake(0, DScreenH - SafeShetH , DScreenW, SafeShetH);
        }
    }];
}

- (void)showFromView:(UIView *)view {
    self.frame = view.frame;
    [view addSubview:self];
    self.containerView.y = view.height;
    self.containerView.width = view.width;
    [self setUpView];
}


- (void)tap {
    [self closeAcitonSheet];
}

- (void)closeAcitonSheet {
    __weak WithDrawSheet *sheet = self;
    [IWNotificationCenter removeObserver:self];
    [UIView animateWithDuration:0.15 animations:^{
        sheet.containerView.y = sheet.height;
        sheet.containerView.width = sheet.width;
        [sheet setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
    } completion:^(BOOL finished) {
        [sheet removeFromSuperview];
    }];
}

- (UIView *)containerView {
    if (!_containerView) {
        UIView *containerView = [[UIView alloc] init];
        containerView.backgroundColor = HEXCOLOR(0xFAFAFA);
        [self addSubview:containerView];
        
        //添加阴影
        containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
        containerView.layer.shadowOffset = CGSizeMake(0, -2);
        containerView.layer.shadowOpacity = 0.17;
        containerView.layer.shadowRadius = 4;
        self.containerView = containerView;
        self.containerView.backgroundColor = WhiteColor;
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        [containerView addGestureRecognizer:tap];

    }
    return _containerView;
}

- (void)canceAction {
    [self closeAcitonSheet];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    point = [self.containerView.layer convertPoint:point fromLayer:self.layer];
    if (![self.containerView.layer containsPoint:point]) {
        [self closeAcitonSheet];
    }
}

- (void)action:(UIButton *)sender {
    NSLog(@"确认");
    if (self.phoneAuth && self.emailAuth && self.googleAuth) {
        if (self.msgTF1.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleasePhoneCode")];
            return;
        }
        if (self.msgTF2.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseEmCode")];
            return;
        }
        if (self.msgTF3.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseGoogleCode")];
            return;
        }
        
    } else if (self.phoneAuth && self.emailAuth) {
        if (self.msgTF1.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleasePhoneCode")];
            return;
        }
        if (self.msgTF2.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseEmCode")];
            return;
        }
    } else if (self.phoneLabel && self.googleAuth) {
        if (self.msgTF1.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleasePhoneCode")];
            return;
        }
        if (self.msgTF3.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseGoogleCode")];
            return;
        }
    } else if (self.emailAuth && self.googleAuth) {
        if (self.msgTF2.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseEmCode")];
            return;
        }
        if (self.msgTF3.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseGoogleCode")];
            return;
        }
    } else if (self.phoneAuth) {
        if (self.msgTF1.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleasePhoneCode")];
            return;
        }
    } else if (self.emailAuth) {
        if (self.msgTF2.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseEmCode")];
            return;
        }
    } else if (self.googleAuth) {
        if (self.msgTF3.text.length == 0) {
            [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_pleaseGoogleCode")];
            return;
        }
    }
    if (self.clickBlock) {
        self.clickBlock(self.msgTF1.text, self.msgTF2.text, self.msgTF3.text, self.account, self.emailTitleName.text);
        [self closeAcitonSheet];
    }
}

- (void)becomeFirst {
    [self.msgTF1 becomeFirstResponder];
    [self.msgTF2 becomeFirstResponder];
    [self.msgTF3 becomeFirstResponder];;
}

- (void)setAccount:(NSString *)account {
    _account = account;
}
- (void)setEmailAuth:(BOOL)emailAuth {
    _emailAuth = emailAuth;
}

- (void)setGoogleAuth:(BOOL)googleAuth {
    _googleAuth = googleAuth;
}

- (void)setPhoneAuth:(BOOL)phoneAuth {
    _phoneAuth = phoneAuth;
}

- (void)msgAction1:(UIButton *)sender {
    NSLog(@"获取手机验证码");
    NSDictionary *dic = @{@"type":@"WITHDRAW"};
    [SBNetworTool getWithUrl:EastReciveSMS params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self.msgTF1 becomeFirstResponder];
            [self.msgBtn startCountDown];
        } else {
            [TipsView showTipOnKeyWindow:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [TipsView showTipOnKeyWindow:FailurMessage];
    }];
}

- (void)msgAction2:(UIButton *)sender {
    NSLog(@"获取邮箱验证码");
    NSDictionary *dic = @{@"type":@"WITHDRAW"};
    [SBNetworTool getWithUrl:EastReciveEmail params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [self.msgTF2 becomeFirstResponder];
            [self.msgBtn2 startCountDown];
        } else {
            [TipsView showTipOnKeyWindow:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [TipsView showTipOnKeyWindow:FailurMessage];
    }];
}

- (void)msgAction3:(UIButton *)sender {
    NSLog(@"粘贴");
    UIPasteboard * pasteboard = [UIPasteboard generalPasteboard];
    self.msgTF3.text = pasteboard.string;
}


- (void)setUpView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(endEdit:)
                                                  name:UIKeyboardWillHideNotification object:nil];
    
    [self.containerView addSubview:self.safeTitle];
    [self.containerView addSubview:self.canceBtn];
    [self.containerView addSubview:self.line1];
    
//    self.phoneLabel.text = self.account;
//    self.emailTitleName.text = self.account;

    if (self.googleAuth && self.emailAuth && self.phoneAuth) {

        [self.containerView addSubview:self.phoneLabel];
        [self.containerView addSubview:self.msgTF1];
        [self.containerView addSubview:self.msgBtn];
        [self.containerView addSubview:self.phoneLine];

        [self.containerView addSubview:self.emailTitleName];
        [self.containerView addSubview:self.msgTF2];
        [self.containerView addSubview:self.msgBtn2];
        [self.containerView addSubview:self.emailLine];

        [self.containerView addSubview:self.googleTitle];
        [self.containerView addSubview:self.msgTF3];
        [self.containerView addSubview:self.msgBtn3];
        [self.containerView addSubview:self.googleLine];


    } else if (self.googleAuth && self.emailAuth) {
        [self.containerView addSubview:self.emailTitleName];
        [self.containerView addSubview:self.msgTF2];
        [self.containerView addSubview:self.msgBtn2];
        [self.containerView addSubview:self.emailLine];

        [self.containerView addSubview:self.googleTitle];
        [self.containerView addSubview:self.msgTF3];
        [self.containerView addSubview:self.msgBtn3];
        [self.containerView addSubview:self.googleLine];

        
    } else if (self.googleAuth && self.phoneAuth) {
        [self.containerView addSubview:self.phoneLabel];
        [self.containerView addSubview:self.msgTF1];
        [self.containerView addSubview:self.msgBtn];
        [self.containerView addSubview:self.phoneLine];

        [self.containerView addSubview:self.googleTitle];
        [self.containerView addSubview:self.msgTF3];
        [self.containerView addSubview:self.msgBtn3];
        [self.containerView addSubview:self.googleLine];

    } else if (self.emailAuth && self.phoneAuth) {
        [self.containerView addSubview:self.phoneLabel];
        [self.containerView addSubview:self.msgTF1];
        [self.containerView addSubview:self.msgBtn];
        [self.containerView addSubview:self.phoneLine];

        [self.containerView addSubview:self.emailTitleName];
        [self.containerView addSubview:self.msgTF2];
        [self.containerView addSubview:self.msgBtn2];
        [self.containerView addSubview:self.emailLine];


    } else if (self.googleAuth) {
        
        [self.containerView addSubview:self.googleTitle];
        [self.containerView addSubview:self.msgTF3];
        [self.containerView addSubview:self.msgBtn3];
        [self.containerView addSubview:self.googleLine];
        
    } else if (self.emailAuth) {
        [self.containerView addSubview:self.emailTitleName];
        [self.containerView addSubview:self.msgTF2];
        [self.containerView addSubview:self.msgBtn2];
        [self.containerView addSubview:self.emailLine];

    } else if (self.phoneAuth) {
        [self.containerView addSubview:self.phoneLabel];
        [self.containerView addSubview:self.msgTF1];
        [self.containerView addSubview:self.msgBtn];
        [self.containerView addSubview:self.phoneLine];
    }
    
    [self.containerView addSubview:self.confirBtn];
    [self make_Layout];
}

- (void)make_Layout {
    [self.safeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(25);
    }];
    
    [self.canceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.safeTitle);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(10);
        make.top.mas_equalTo(self.safeTitle.mas_bottom).offset(15);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1);
    }];
    
    if (self.googleAuth && self.emailAuth && self.phoneAuth) {
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
            make.width.mas_equalTo(300);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).offset(15);
        }];
        
        [self.msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF1);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(self.msgTF1);
        }];
        
        [self.phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(self.msgTF1.mas_bottom).offset(5);
            make.height.mas_equalTo(kLinePixel);
        }];
        
        [self.emailTitleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.phoneLine.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.emailTitleName.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF2);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF2);
        }];
        
        [self.emailLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF2.mas_bottom).offset(5);
        }];
        
        [self.googleTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.emailLine.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.googleTitle.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF3);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF3);
        }];

        [self.googleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF3.mas_bottom).offset(5);
        }];
        
    } else if (self.googleAuth && self.emailAuth) {
        [self.emailTitleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.emailTitleName.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF2);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF2);
        }];
        
        [self.emailLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF2.mas_bottom).offset(5);
        }];
        
        [self.googleTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.emailLine.mas_bottom).offset(10);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.top.mas_equalTo(self.googleTitle.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF3);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF3);
        }];

        [self.googleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF3.mas_bottom).offset(5);
        }];

    } else if (self.googleAuth && self.phoneAuth) {
        
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
            make.width.mas_equalTo(300);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).offset(15);
        }];
        
        [self.msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF1);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(self.msgTF1);
        }];
        
        [self.phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(self.msgTF1.mas_bottom).offset(5);
            make.height.mas_equalTo(1);
        }];
        
        [self.googleTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.phoneLine.mas_bottom).offset(10);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.googleTitle.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF3);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF3);
        }];

        [self.googleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF3.mas_bottom).offset(5);
        }];

    } else if (self.emailAuth && self.phoneAuth) {
        
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
            make.width.mas_equalTo(300);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).offset(15);
        }];
        
        [self.msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF1);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(self.msgTF1);
        }];
        
        [self.phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(self.msgTF1.mas_bottom).offset(5);
            make.height.mas_equalTo(1);
        }];
        
        [self.emailTitleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.phoneLine.mas_bottom).offset(10);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.emailTitleName.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF2);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF2);
        }];
        
        [self.emailLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF2.mas_bottom).offset(5);
        }];

    } else if (self.emailAuth) {
        [self.emailTitleName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.emailTitleName.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF2);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF2);
        }];
        
        [self.emailLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF2.mas_bottom).offset(5);
        }];

    } else if (self.googleAuth) {
        
        [self.googleTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(10);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.googleTitle.mas_bottom).offset(15);
            make.width.mas_lessThanOrEqualTo(150);
            make.height.mas_equalTo(20);
        }];
        
        [self.msgBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF3);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(self.msgTF3);
        }];

        [self.googleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(kLinePixel);
            make.top.mas_equalTo(self.msgTF3.mas_bottom).offset(5);
        }];

    } else if (self.phoneAuth) {
        
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
            make.width.mas_equalTo(300);
            make.height.mas_equalTo(15);
        }];
        
        [self.msgTF1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).offset(15);
        }];
        
        [self.msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(self.msgTF1);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(self.msgTF1);
        }];
        
        [self.phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(self.msgTF1.mas_bottom).offset(5);
            make.height.mas_equalTo(1);
        }];
    }
    
    
    [self.confirBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(self.containerView).offset(-50);
    }];
    
    CGRect frame;
    if (self.googleAuth && self.emailAuth && self.phoneAuth) {
        frame = CGRectMake(0, DScreenH - SafeShetH3, DScreenW, SafeShetH3);
        self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, SafeShetH3);
    } else if (self.googleAuth && self.emailAuth) {
        frame = CGRectMake(0, DScreenH - SafeShetH2, DScreenW, SafeShetH2);
        self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, SafeShetH2);
    } else if (self.emailAuth && self.phoneAuth) {
        frame = CGRectMake(0, DScreenH - SafeShetH2, DScreenW, SafeShetH2);
        self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, SafeShetH2);
    } else if (self.googleAuth && self.phoneAuth) {
        frame = CGRectMake(0, DScreenH - SafeShetH2, DScreenW, SafeShetH2);
        self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, SafeShetH2);
    } else {
        frame = CGRectMake(0, DScreenH - SafeShetH, DScreenW, SafeShetH);
        self.containerView.frame = CGRectMake(0, DScreenH, DScreenW, SafeShetH);
    }
    
     UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.containerView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.containerView.layer.mask = maskLayer;
    [UIView animateWithDuration:0.25 animations:^{
        self.containerView.frame = frame;
        self.alpha = 1;
    }];
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.phoneAuth && self.emailAuth && self.googleAuth) {
        if (self.msgTF1.text.length > 0 && self.msgTF2.text.length > 0 && self.msgTF3.text.length > 0) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    } else if (self.phoneAuth && self.emailAuth) {
        if (self.msgTF1.text.length > 0 && self.msgTF2.text.length > 0 ) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    } else if (self.phoneLabel && self.googleAuth) {
        if (self.msgTF1.text.length > 0 && self.msgTF3.text.length > 0) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    } else if (self.emailAuth && self.googleAuth) {
        if (self.msgTF2.text.length > 0 && self.msgTF3.text.length > 0) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    } else if (self.phoneAuth) {
        if (self.msgTF1.text.length > 0) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    } else if (self.emailAuth) {
        if (self.msgTF2.text.length > 0) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    } else if (self.googleAuth) {
        if (self.msgTF3.text.length > 0) {
            self.confirBtn.userInteractionEnabled = YES;
            self.confirBtn.backgroundColor = ThemeGreenColor;
        } else {
            self.confirBtn.userInteractionEnabled = NO;
            self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        }
    }
}




- (UILabel *)safeTitle {
    if (!_safeTitle) {
        _safeTitle = [[UILabel alloc] init];
        _safeTitle.textColor = ThemeTextColor;
        _safeTitle.font = [UIFont systemFontOfSize:16];
        _safeTitle.textAlignment = 0;
        _safeTitle.text = kLocalizedString(@"safeTitle");
    }
    return _safeTitle;
}

- (UIButton *)canceBtn {
    if (!_canceBtn) {
        _canceBtn = [[UIButton alloc] init];
        [_canceBtn setTitleColor:PlaceHolderColor forState:UIControlStateNormal];
        [_canceBtn setTitle:kLocalizedString(@"safeCancel") forState:UIControlStateNormal];
        [_canceBtn addTarget:self action:@selector(canceAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _canceBtn;
}


- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = LineColor;
    }
    return _line1;
}

- (UILabel *)phoneLabel {
    if (!_phoneLabel) {
        _phoneLabel = [[UILabel alloc] init];
        _phoneLabel.textAlignment = 0;
        _phoneLabel.textColor = MainWhiteColor;
        _phoneLabel.font = TwelveFontSize;
    }
    return _phoneLabel;
}


- (UITextField *)msgTF1 {
    if (!_msgTF1) {
        _msgTF1 = [[UITextField alloc] init];
        _msgTF1.delegate = self;
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"safePhoneCode") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _msgTF1.attributedPlaceholder = attrString1;
        _msgTF1.secureTextEntry = NO;
        _msgTF1.textColor = MainWhiteColor;
        _msgTF1.font = TwelveFontSize;
        [_msgTF1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _msgTF1;
}


- (TimeBtn *)msgBtn {
    if (!_msgBtn) {
        _msgBtn = [[TimeBtn alloc] init];
        _msgBtn.backgroundColor = ClearColor;
        [_msgBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _msgBtn.processColor = PlaceHolderColor;
        _msgBtn.tag = 1;
        _msgBtn.titleLabel.font = FourteenFontSize;
        _msgBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _msgBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_msgBtn setTitle:kLocalizedString(@"safeSendMsg") forState:UIControlStateNormal];
        [_msgBtn addTarget:self action:@selector(msgAction1:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _msgBtn;
}

- (UILabel *)phoneLine {
    if (!_phoneLine) {
        _phoneLine = [[UILabel alloc] init];
        _phoneLine.backgroundColor = LineColor;
    }
    return _phoneLine;
}

- (UILabel *)emailTitleName {
    if (!_emailTitleName) {
        _emailTitleName = [[UILabel alloc] init];
        _emailTitleName.textColor = MainWhiteColor;
        _emailTitleName.font = TwelveFontSize;
        _emailTitleName.textAlignment = 0;
    }
    return _emailTitleName;
}


- (UITextField *)msgTF2 {
    if (!_msgTF2) {
        _msgTF2 = [[UITextField alloc] init];
        _msgTF2.delegate = self;
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"safePlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _msgTF2.attributedPlaceholder = attrString1;
        _msgTF2.secureTextEntry = NO;
        _msgTF2.textColor = MainWhiteColor;
        _msgTF2.font = TwelveFontSize;
        [_msgTF2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _msgTF2;
}

- (TimeBtn *)msgBtn2 {
    if (!_msgBtn2) {
        _msgBtn2 = [[TimeBtn alloc] init];
        _msgBtn2.backgroundColor = ClearColor;
        [_msgBtn2 setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _msgBtn2.processColor = PlaceHolderColor;
        _msgBtn2.tag = 1;
        _msgBtn2.titleLabel.font = FourteenFontSize;
        _msgBtn2.titleLabel.adjustsFontSizeToFitWidth = YES;
        _msgBtn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_msgBtn2 setTitle:kLocalizedString(@"safeSendMsg") forState:UIControlStateNormal];
        [_msgBtn2 addTarget:self action:@selector(msgAction2:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _msgBtn2;
}

- (UILabel *)emailLine {
    if (!_emailLine) {
        _emailLine = [[UILabel alloc] init];
        _emailLine.backgroundColor = LineColor;
    }
    return _emailLine;
}


- (UILabel *)googleTitle {
    if (!_googleTitle) {
        _googleTitle = [[UILabel alloc] init];
        _googleTitle.textColor = MainWhiteColor;
        _googleTitle.font = TwelveFontSize;
        _googleTitle.textAlignment = 0;
//        _googleTitle.text = @"18741120597";;
    }
    return _googleTitle;
}


- (UITextField *)msgTF3 {
    if (!_msgTF3) {
        _msgTF3 = [[UITextField alloc] init];
        _msgTF3.delegate = self;
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"safeGoogleCode") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _msgTF3.attributedPlaceholder = attrString1;
        _msgTF3.secureTextEntry = NO;
        _msgTF3.textColor = MainWhiteColor;
        _msgTF3.font = TwelveFontSize;
        [_msgTF3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _msgTF3;
}

- (TimeBtn *)msgBtn3 {
    if (!_msgBtn3) {
        _msgBtn3 = [[TimeBtn alloc] init];
        _msgBtn3.backgroundColor = ClearColor;
        [_msgBtn3 setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _msgBtn3.processColor = PlaceHolderColor;
        _msgBtn3.tag = 1;
        _msgBtn3.titleLabel.font = FourteenFontSize;
        _msgBtn3.titleLabel.adjustsFontSizeToFitWidth = YES;
        _msgBtn3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_msgBtn3 setTitle:kLocalizedString(@"safeCopy") forState:UIControlStateNormal];
        [_msgBtn3 addTarget:self action:@selector(msgAction3:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _msgBtn3;
}

- (UILabel *)googleLine {
    if (!_googleLine) {
        _googleLine = [[UILabel alloc] init];
        _googleLine.backgroundColor = LineColor;
    }
    return _googleLine;
}


- (UIButton *)confirBtn {
    if (!_confirBtn) {
        _confirBtn = [[UIButton alloc] init];
        _confirBtn.userInteractionEnabled = NO;
        _confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        [_confirBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        _confirBtn.tag = 0;
        _confirBtn.cornerRadius = 3;
        _confirBtn.titleLabel.font = EighteenFontSize;
        [_confirBtn setTitle:kLocalizedString(@"safeConfirm") forState:UIControlStateNormal];
        [_confirBtn addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirBtn;
}


@end
