//
//  AssertModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/2.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BalanceModel;
@class BalanceCoinsModel;

NS_ASSUME_NONNULL_BEGIN

@interface AssertModel : NSObject

//@property (nonatomic ,strong) NSArray<BalanceModel *> *balance;

@property (nonatomic ,strong) NSArray<BalanceCoinsModel *> *coins;


@end

NS_ASSUME_NONNULL_END
