//
//  Theme.h
//  Senbit
//
//  Created by 张玮 on 2019/12/19.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#ifndef Theme_h
#define Theme_h

#pragma mark -  * * * * * * * * * * * * * * 主题 * * * * * * * * * * * * * *

/**
*  灰色
*/
#define ThemeGrayColor UIColorFromRGBValue(0xC9C9D6)

/**
*  小字颜色
*/
#define ThemeLightGrayColor UIColorFromRGBValue(0x999999)
/**
*  文字颜色
*/
#define ThemeTextColor UIColorFromRGBValue(0x000216)

/**
*  输入框文字颜色
*/
#define FieldTextColor UIColorFromRGBValue(0x000216)
/**
 *  无色
 */
#define ClearColor [UIColor clearColor]

/**
 *  UITableview背景色
 */
#define ThemeTableColor UIColorFromRGBValue(0xF7F6FB)


/**
 *  默认字体颜色(非点击状态)
 */
#define kNormalTitleColor UIColorFromRGBValue(0x6B6B6B)

/**
 *  pageControlerColor
 */
#define ThemeColorPage UIColorFromRGBValue(0x535DCC)

/**
 *  默认页面背景色
 */
#define DefaultBackGroundColor UIColorFromRGBValue(0Xf5f7fa)

/**
 *  lineColor
 */
#define LineColor UIColorFromRGBValue(0XD8D8D8)

/**
 *  默认BorderColor
 */
#define BorderColor UIColorFromRGBValue(0XA4A4A4)

/**
 *  默认主题文字颜色
 */
#define WhiteColor UIColorFromRGBValue(0XFFFFFF)

/**
 *  主题颜色深绿色
 */
#define ThemeGreenColor UIColorFromRGBValue(0x00A19B)

/**
 *  主题颜色深绿色
 */
#define ThemeLittleTitleColor UIColorFromRGBValue(0xC4C4C4)



/**
 *  主题颜色红色
 */
#define ThemeRedColor UIColorFromRGBValue(0xEA1D76)

/**
 *  主题深黑色/白色背景
 */
#define ThemeDarkBlack UIColorFromRGBValue(0xF5F8FD)

/**
 *  主题深2级黑色背景
 */
#define MainWhiteColor UIColorFromRGBValue(0x4A4A4A)



/**
 *  UITextField文字输入框颜色
 */
#define PlaceHolderColor UIColorFromRGBValue(0xA0A0A0)

/**
 *  page control default color
 */
#define kPageDefaultColor UIColorFromRGBValue(0X828CA1)

/**
 *  主题辅助颜色（状态，提示等...）
 */
#define OrangeColor UIColorFromRGBValue(0Xef5a50)

/**
 *  遮盖半透明色
 */
#define kCoverColor [UIColorFromRGBValue(0X000000)colorWithAlphaComponent:0.3]

/**
 *  分割线白色背景等...
 */
#define SparatorColor UIColorFromRGBValue(0xF4F2F2)
/**
 *  头像默认背景色
 */
#define ImgBackColor UIColorFromRGBValue(0xD7D7D7)

/**
*  分割线灰色颜色
*/
#define kBackDefaultGrayColor UIColorFromRGBValue(0Xdbdfe8)

/**
 *  随机色
 */
#define kRandomColor [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1]

/**
*  十六进制颜色
*/
#define UIColorFromRGBValue(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#pragma mark -  * * * * * * * * * * * * * * set Font * * * * * * * * * * * * * *
/**
 *  10号字体
 */
#define TenFontSize [UIFont systemFontOfSize:10] //[UIFont fontWithName:@"MicrosoftYaHei" size:10]
/**
 *  11号字体
 */
#define ElevenFontSize [UIFont systemFontOfSize:11]
/**
 *  12号字体
 */
#define TwelveFontSize [UIFont systemFontOfSize:12]
/**
 *  13号字体
 */
#define ThirteenFontSize [UIFont systemFontOfSize:13]
/**
 *  14号字体
 */
#define FourteenFontSize [UIFont systemFontOfSize:14]
/**
 *  15号字体
 */
#define FifteenFontSize [UIFont systemFontOfSize:15]
/**
 *  17号字体
 */
#define SeventeenFontSize [UIFont systemFontOfSize:17]
/**
 *  18号字体
 */
#define EighteenFontSize [UIFont systemFontOfSize:18]
/**
 *  20号字体
 */
#define TwentyFontSize [UIFont systemFontOfSize:20]


#define kFont48 [UIFont systemFontOfSize:48.0f]
#define kFontBold48 [UIFont boldSystemFontOfSize:48.0f]

#define kFont32 [UIFont systemFontOfSize:32.0f]
#define kFontBold32 [UIFont boldSystemFontOfSize:32.0f]

#define kFont24 [UIFont systemFontOfSize:24.0f]
#define kFontBold24 [UIFont boldSystemFontOfSize:24.0f]

#define kFont20 [UIFont systemFontOfSize:20.0f]
#define kFontBold20 [UIFont boldSystemFontOfSize:20.0f]

#define kFont18 [UIFont systemFontOfSize:18.0f]
#define kFontBold18 [UIFont boldSystemFontOfSize:18.0f]

#define kFont16 [UIFont systemFontOfSize:16.0f]
#define kFontBold16 [UIFont boldSystemFontOfSize:16.0f]

#define kFont15 [UIFont systemFontOfSize:15.0f]
#define kFontBold15 [UIFont boldSystemFontOfSize:15.0f]

#define kFont14 [UIFont systemFontOfSize:14.0f]
#define kFontBold14 [UIFont boldSystemFontOfSize:14.0f]

#define kFont12 [UIFont systemFontOfSize:12.0f]
#define kFontBold12 [UIFont boldSystemFontOfSize:12.0f]

#define kFont10 [UIFont systemFontOfSize:10.0f]
#define kFontBold10 [UIFont boldSystemFontOfSize:10.0f]

#define kFont(value) [UIFont systemFontOfSize:value]
#define kFontBold(value) [UIFont boldSystemFontOfSize:value]




#pragma mark -  * * * * * * * * * * * * * * set Button * * * * * * * * * * * * * *
/**
 *  按钮的背景默认颜色
 */
#define kButtonBackColorForNormal UIColorFromRGBValue(0XEFEFEF)
/**
 *  按钮的背景点击颜色
 */
#define kButtonBackColorForSelect UIColorFromRGBValue(0X008ce3)
/**
 *  按钮的背景不可点击颜色
 */
#define kButtonBackColorForDisabled UIColorFromRGBValue(0X7fcaf9)

#pragma mark -  * * * * * * * * * * * * * * 交易面板的背景 * * * * * * * * * * * * * *

/**
 *  面板背景默认颜色
 */
#define kTradeBackColor UIColorFromRGBValue(0XF5F8FD)

/**
 *  按钮的圆角
 */
#define kButtonCornerRad 5
/**
*   line0.5
*/
#define kLinePixel  0.5



#endif /* Theme_h */
