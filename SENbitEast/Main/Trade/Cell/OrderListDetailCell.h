//
//  OrderListDetailCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OderDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderListDetailCell : UITableViewCell

@property (nonatomic ,strong) UILabel *timeLb;

@property (nonatomic ,strong) UILabel *priceLb;

@property (nonatomic ,strong) UILabel *numLb;

@property (nonatomic ,strong) UILabel *gasLb;

+ (instancetype)cellWithTbaleView:(UITableView *)tableView;

@property (nonatomic ,strong) OderDetailModel *model;

@end

NS_ASSUME_NONNULL_END
