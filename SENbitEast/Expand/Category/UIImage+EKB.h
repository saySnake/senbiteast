//
//  UIImage+EKB.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (EKB)

+ (UIImage *)imageWithName:(NSString *)imageName;

+ (UIImage *)resizeWithName:(NSString *)imageName;

+ (UIImage *)resizeWithName:(NSString *)imageName leftRate:(CGFloat)leftRate topRatio:(CGFloat)topRatio;

+ (UIImage *)getImageWithColor:(UIColor *)color;

- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize;

+ (UIImage *)imageWithScreenshot;

+ (UIImage *)getImageFromURL:(NSString *)fileUrl;

@end

NS_ASSUME_NONNULL_END
