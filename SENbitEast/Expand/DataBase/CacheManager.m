//
//  CacheManager.m
//  Senbit
//
//  Created by 张玮 on 2019/12/25.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "CacheManager.h"
NSString *const SenlinkCacheUserInfoKey = @"Eastuser_model";
NSString *const SenlinkCacheSearchHistoryInfoKey = @"EastsearchHistory";
NSString *const SenlinkCacheUserExtInfoKey = @"EastotcUserInfo";

@implementation CacheManager
@synthesize user = _user;
@synthesize otcInfo = _otcInfo;
+ (instancetype)sharedMnager {
    static CacheManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CacheManager alloc] init];
    });
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _cacheQueue = dispatch_queue_create("EastSenBit.cacheQueue.manager", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

#pragma mark 获取当前登录用户的的信息
- (UserInfo *)getUserInfo {
    if (_user) {
        return _user;
    }
    //获取自己的id
    NSData *_data = [[NSData alloc] initWithContentsOfFile:[self getFilePathWithModelKey:[NSString stringWithFormat:@"%@.data",SenlinkCacheUserInfoKey]]];
    //解档辅助类
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:_data];
    UserInfo *userModel = [unarchiver  decodeObjectForKey:SenlinkCacheUserInfoKey];
    return userModel;
}

- (OtcExtinfo *)getOtcInfo {
//    if (_otcInfo) {
//        return _otcInfo;
//    }
//    //获取自己的id
//    NSData *_data = [[NSData alloc] initWithContentsOfFile:[self getFilePathWithModelKey:[NSString stringWithFormat:@"%@.data",SenlinkCacheUserExtInfoKey]]];
//    //解档辅助类
//    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:_data];
//    OtcExtinfo *userModel = [unarchiver  decodeObjectForKey:SenlinkCacheUserExtInfoKey];
//    return userModel;
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
     
     NSData *data = [user objectForKey:SenlinkCacheUserExtInfoKey];
        
    OtcExtinfo *info = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return info;
}

- (NSString *)getFilePathWithModelKey:(NSString *)modelkey {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[array objectAtIndex:0] stringByAppendingPathComponent:modelkey];
}

- (void)clearCache {
    UserInfo *info = [[UserInfo alloc] init];
    OtcExtinfo *otcInfo = [[OtcExtinfo alloc] init];
    [self saveUserInfo:info];
    [self saveOtcExinfo:otcInfo];
}

#pragma mark 保存当前登录用户信息到本地
- (void)saveUserInfo:(UserInfo *)info {
    _user = info;
    dispatch_async(_cacheQueue, ^{
        NSMutableData *data = [[NSMutableData alloc] init];
        NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        //编码
        [archiver encodeObject:info forKey:SenlinkCacheUserInfoKey];
        //结束编码
        [archiver finishEncoding];
        BOOL rs = [data writeToFile:[self getFilePathWithModelKey:[NSString stringWithFormat:@"%@.data",SenlinkCacheUserInfoKey]] atomically:YES];
        NSLog(@"rs----%d",rs);
    });
}


- (void)saveOtcExinfo:(OtcExtinfo *)extInfo {
//    _otcInfo = extInfo;
//    @synchronized(self) {
//        dispatch_async(_cacheQueue, ^{
//            NSMutableData *data = [[NSMutableData alloc] init];
//            NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
//
//            //编码
//            [archiver encodeObject:extInfo forKey:SenlinkCacheUserExtInfoKey];
//            //结束编码
//            [archiver finishEncoding];
//            [data writeToFile:[self getFilePathWithModelKey:[NSString stringWithFormat:@"%@.data",SenlinkCacheUserExtInfoKey]] atomically:YES];
//        });
//    }
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:extInfo];
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:data forKey:SenlinkCacheUserExtInfoKey];
//    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
//    [[NSUserDefaults standardUserDefaults] synchronize];

}
@end
 
#pragma mark - 搜索历史记录
@implementation CacheManager (searchHistory)

- (void)insertSearchHistory:(NSString *)history {
    if (history.length == 0) {
        return;
    }
    NSNumber *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"ID"];
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@-%@",SenlinkCacheSearchHistoryInfoKey,uid]];
    NSMutableArray *marr = [NSMutableArray arrayWithArray:array];
    if ([marr containsObject:history]) {
        [marr removeObject:history];
    }
    [marr insertObject:history atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:marr forKey:[NSString stringWithFormat:@"%@-%@",SenlinkCacheSearchHistoryInfoKey,uid]];
    
}

- (void)removeSearchHistory:(NSString *)history{
    NSNumber *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"ID"];
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@-%@",SenlinkCacheSearchHistoryInfoKey,uid]];
    NSMutableArray *marr = [NSMutableArray arrayWithArray:array];
    [marr addObject:history];
    [marr removeObject:history];
    [[NSUserDefaults standardUserDefaults] setObject:marr forKey:[NSString stringWithFormat:@"%@-%@",SenlinkCacheSearchHistoryInfoKey,uid]];

}
- (NSArray *)histories {
    NSNumber *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"ID"];
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@-%@",SenlinkCacheSearchHistoryInfoKey,uid]];
    return array;
}
- (void)removeAllHistory {
    NSNumber *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"ID"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@-%@",SenlinkCacheSearchHistoryInfoKey,uid]];
}
@end
