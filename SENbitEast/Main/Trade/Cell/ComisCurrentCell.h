//
//  ComisCurrentCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradeListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ComisCurrentCell : UITableViewCell

@property (nonatomic ,strong) UILabel *direct;
@property (nonatomic ,strong) UILabel *coin;
@property (nonatomic ,strong) UIButton *cancelBtn;
@property (nonatomic ,strong) FSCustomButton *finBtn;


@property (nonatomic ,strong) UILabel *timeLb;
@property (nonatomic ,strong) UILabel *entrustPrice;
@property (nonatomic ,strong) UILabel *entrustNum;

@property (nonatomic ,strong) UILabel *timeText;
@property (nonatomic ,strong) UILabel *entruPriceText;
@property (nonatomic ,strong) UILabel *entruNumText;

@property (nonatomic ,strong) UILabel *orderTotal;
@property (nonatomic ,strong) UILabel *orderAverage;
@property (nonatomic ,strong) UILabel *orderNum;

@property (nonatomic ,strong) UILabel *orderTotalText;
@property (nonatomic ,strong) UILabel *orderAverageText;
@property (nonatomic ,strong) UILabel *orderNumText;

@property (nonatomic ,strong) TradeListModel *model;
@property (strong, nonatomic) void(^ComisCellBlock)(NSString *Id,NSString *market);
@property (strong, nonatomic) void(^finishBlock)(TradeListModel *model);

@property (nonatomic ,assign) NSInteger currentIndex;


+ (instancetype)cellWithTbaleView:(UITableView *)tableView;
+ (CGFloat)ComisCurrentCellHeight;
@end

NS_ASSUME_NONNULL_END
