//
//  SelectedCoinCell.h
//  Senbit
//
//  Created by 张玮 on 2020/3/5.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneTableSockeModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, BLOCKStat) {
    BLOCKSuccess                 = 0,
    BLOCKFailure   = 1 << 0,
};

typedef void (^btnBlock)(BLOCKStat state);

@interface SelectedCoinCell : UITableViewCell

@property (nonatomic ,strong) UILabel *coin;
@property (nonatomic ,strong) UILabel *price;
@property (nonatomic ,strong) UILabel *per;
@property (nonatomic ,strong) UIButton *starBtn;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic ,strong) OneTableSockeModel *model;
@property (nonatomic ,copy) btnBlock collectionBlock;

@end

NS_ASSUME_NONNULL_END
