//
//  TradeDetailVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/14.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeDetailVC.h"
#import "OrderListDetailCell.h"
#import "TradeDetailHeaderVIew.h"
#import "OrderSection.h"
#import "OderDetailModel.h"

@interface TradeDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) TradeDetailHeaderVIew *sectionHeaderView;
@property (nonatomic ,strong) OrderSection *toolView;
@property (nonatomic ,strong) UITableView *tbView;
@property (nonatomic ,strong) NSMutableArray *dataArray;

@end

@implementation TradeDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navBar.backgroundColor = WhiteColor;
    [self setNavBarTitle:kLocalizedString(@"detail_title")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];
}

- (void)setUpView {
    [self.view addSubview:self.tbView];
}


- (void)requirDeail {
    NSString *url = [NSString stringWithFormat:@"%@%@",EastMarkPairsDetail,self.model.orderId];
    [SBNetworTool getWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.dataArray = [OderDetailModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
            [self.tbView reloadData];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)setModel:(TradeListModel *)model {
    if (model) {
        _model = model;
        self.sectionHeaderView.model = model;
        [self requirDeail];
        [self make_Layout];
    }
}

- (void)make_Layout {
    
//    @param axisType     布局方向
//   *  @param fixedSpacing 两个item之间的间距(最左面的item和左边, 最右边item和右边都不是这个)
//   *  @param leadSpacing  第一个item到父视图边距
//    *  @param tailSpacing  最后一个item到父视图边距
    self.tbView.tableHeaderView = self.sectionHeaderView;
    self.tbView.tableFooterView = [[UIView alloc] init];
}


- (UIView *)toolView {
    if (!_toolView) {
        _toolView = [[OrderSection alloc] initWithFrame:CGRectMake(0, 0, DScreenW, 60)];
        _toolView.backgroundColor = HEXCOLOR(0xF2F2F2);
    }
    return _toolView;
}

- (TradeDetailHeaderVIew *)sectionHeaderView {
    if (!_sectionHeaderView) {
        _sectionHeaderView = [[TradeDetailHeaderVIew alloc] initWithFrame:CGRectMake(0, 0, DScreenW, 180)];
        [_sectionHeaderView setUpview];
    }
    return _sectionHeaderView;
}


- (UITableView *)tbView {
    if (_tbView == nil) {
        _tbView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNavBarAndStatusBarHeight, kScreen_Width, kScreen_Height - (kNavBarAndStatusBarHeight)) style:UITableViewStylePlain];
        _tbView.backgroundColor = [UIColor whiteColor];
        _tbView.dataSource = self;
        _tbView.delegate = self;
        _tbView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tbView.scrollEnabled = YES;
        _tbView.rowHeight = 50;
        if (@available(iOS 11.0, *)) {
            _tbView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tbView;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.toolView.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.toolView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderListDetailCell *cell = [OrderListDetailCell cellWithTbaleView:tableView];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}


- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}





@end
