//
//  GestureFile.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#ifndef GestureFile_h
#define GestureFile_h

// 保存用户是否开启手势解锁的状态
#define kLZGestureEnableByUserKey  @"LZGestureEnableByUserKey"

// 保存 用户设置的手势密码
#define  kLZGesturePsw @"LZGesturePswIsSectted"


//用户是不是稍后添加手势
#define KLGestureHold @"KLGestureHold"



#endif /* GestureFile_h */
