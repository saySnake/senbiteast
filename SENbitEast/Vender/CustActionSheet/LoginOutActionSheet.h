//
//  LoginOutActionSheet.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/30.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN
typedef void(^ClickedBlock) (NSString *str);

@interface LoginOutActionSheet : UIView

+ (instancetype)customActionSheetWithData:(NSArray *)data;

/** 操作类型数组 */
@property (nonatomic, strong) NSArray *datas;

/** 标题 */
@property (nonatomic, strong) NSString *title;

- (void)showFromView:(UIView *)view;

/** 点击事件 */
@property (nonatomic, copy) ClickedBlock clickBlock;

@end

NS_ASSUME_NONNULL_END
