//
//  SelectCoinView.h
//  Senbit
//
//  Created by 张玮 on 2020/2/21.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SelectCoinViewDelegate <NSObject>

@optional

- (void)selectCoinNameAction;

@end

@interface SelectCoinView : BaseView

@property (nonatomic ,strong) UIImageView *coinImg;

@property (nonatomic ,strong) UIButton *coinName;

@property (nonatomic ,strong) UILabel *selectName;

@property (nonatomic ,strong) UIImageView *rigImg;

@property (nonatomic ,assign) id<SelectCoinViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
