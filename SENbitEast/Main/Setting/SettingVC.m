//
//  SettingVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/1.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "SettingVC.h"
#import "LanguageVC.h"
#import "ActionSheetModel.h"
#import "LoginOutActionSheet.h"
#import "CoinSignle.h"

@interface SettingVC ()

/** uibtn **/
@property (nonatomic ,strong) UIButton *btn1;
/** language **/
@property (nonatomic ,strong) UILabel *language;
/** languageText **/
@property (nonatomic ,strong) UILabel *languageText;
/** img1 **/
@property (nonatomic ,strong) UIImageView *img1;
/** line1 **/
@property (nonatomic ,strong) UILabel *line1;


/** btn2 **/
@property (nonatomic ,strong) UIButton *btn2;
/** priceStyle **/
@property (nonatomic ,strong) UILabel *priceStyle;
/** priceText **/
@property (nonatomic ,strong) UILabel *priceText;
/** img2 **/
@property (nonatomic ,strong) UIImageView *img2;
/** line2 **/
@property (nonatomic ,strong) UILabel *line2;


/** btn3 **/
@property (nonatomic ,strong) UIButton *btn3;
/** version **/
@property (nonatomic ,strong) UILabel *versionLb;
/** versionText **/
@property (nonatomic ,strong) UILabel *versionText;
/** img3 **/
@property (nonatomic ,strong) UIImageView *img3;
/** line3 **/
@property (nonatomic ,strong) UILabel *line3;

/** logOutBtn **/
@property (nonatomic ,strong) UIButton *logOutBtn;

@end

@implementation SettingVC
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.languageText.text = kLanguageManager.currentLanguage;
    [self requestPing];
}


- (void)requestPing {
    KWeakSelf
    [SBNetworTool getWithUrl:EastPing params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            self.logOutBtn.hidden = NO;
        } else {
            self.logOutBtn.hidden = YES;
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {

    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    
    [self setNavBarTitle:kLocalizedString(@"set_native")]; //kLocalizedString(@"search")];
    self.navBar.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];

}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"set_native")]; //kLocalizedString(@"search")];
    self.language.text = kLocalizedString(@"set_language");
    self.priceStyle.text = kLocalizedString(@"set_money");
    self.versionLb.text = kLocalizedString(@"set_version");
    [self.logOutBtn setTitle:kLocalizedString(@"set_loginOut") forState:UIControlStateNormal];
}

- (void)setUpView {
    self.versionText.text = [NSString stringWithFormat:@"v%@",[self getLocalVersion]];
    [self.view addSubview:self.btn1];
    [self.btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    [self.btn1 addSubview:self.language];
    [self.language mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(100);
        make.centerY.mas_equalTo(self.btn1);
        make.height.mas_equalTo(20);
    }];
    
    [self.btn1 addSubview:self.languageText];
    [self.languageText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.centerY.mas_equalTo(self.btn1);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(15);
    }];
    
    [self.btn1 addSubview:self.img1];
    [self.img1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(self.btn1);
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(10);
    }];
    
    [self.btn1 addSubview:self.line1];
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.btn1).offset(-kLinePixel);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.view addSubview:self.btn2];
    [self.btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.btn1.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(self.btn1);
    }];
    
    [self.btn2 addSubview:self.priceStyle];
    [self.priceStyle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.language);
        make.centerY.mas_equalTo(self.btn2);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(20);
    }];
    
    [self.btn2 addSubview:self.priceText];
    [self.priceText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.languageText);
        make.centerY.mas_equalTo(self.btn2);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(15);
    }];
    
    [self.btn2 addSubview:self.img2];
    [self.img2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.img1);
        make.centerY.mas_equalTo(self.btn2);
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(10);
    }];
    
    [self.btn2 addSubview:self.line2];
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.btn2).offset(-kLinePixel);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.view addSubview:self.btn3];
    [self.btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.btn2.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    [self.btn3 addSubview:self.versionLb];
    [self.versionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceStyle);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.priceStyle);
        make.centerY.mas_equalTo(self.btn3);
    }];
    
    [self.btn3 addSubview:self.versionText];
    [self.versionText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(self.btn3);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(self.languageText);
    }];
    
    [self.btn3 addSubview:self.img3];
    if ([CoinSignle shareInstance].isUpdate == YES) {
        self.img3.hidden = NO;
    } else {
        self.img3.hidden = YES;
    }
    [self.img3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.versionText.mas_left);
        make.centerY.mas_equalTo(self.btn3);
        make.width.height.mas_equalTo(5);
    }];
    
    [self.btn3 addSubview:self.line3];
    [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.btn3).offset(-kLinePixel);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    
    [self.view addSubview:self.logOutBtn];
    [self.logOutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(50);
        make.right.mas_equalTo(-50);
        make.bottom.mas_equalTo(self.view).offset(-60);
        make.height.mas_equalTo(50);
    }];
}

- (NSString *)getLocalVersion {
    NSDictionary * infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString * versionNum = [infoDict objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"iOS本地版本号:%@",versionNum);
    return versionNum;
}


- (UIButton *)btn1 {
    if (!_btn1) {
        _btn1 = [[UIButton alloc] init];
        [_btn1 addTarget:self action:@selector(btn1Action) forControlEvents:UIControlEventTouchUpInside];
        _btn1.backgroundColor = WhiteColor;
    }
    return _btn1;
}

- (UILabel *)language {
    if (!_language) {
        _language = [[UILabel alloc] init];
        _language.text = kLocalizedString(@"set_language");
        _language.textAlignment = 0;
        _language.font = FifteenFontSize;
        _language.textColor = HEXCOLOR(0x333333);
        _language.adjustsFontSizeToFitWidth = YES;
    }
    return _language;
}

- (UILabel *)languageText {
    if (!_languageText) {
        _languageText = [[UILabel alloc] init];
        _languageText.text = kLanguageManager.currentLanguage;
        _languageText.textAlignment = 2;
        _languageText.textColor = PlaceHolderColor;
        _languageText.font = ThirteenFontSize;
    }
    return _languageText;
}

- (UIImageView *)img1 {
    if (!_img1) {
        _img1 = [[UIImageView alloc] init];
        _img1.image = [UIImage imageNamed:@"fanhui"];
    }
    return _img1;
}

- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line1;
}

- (UIButton *)btn2 {
    if (!_btn2) {
        _btn2 = [[UIButton alloc] init];
        _btn2.backgroundColor = WhiteColor;
        [_btn2 addTarget:self action:@selector(btn2Action) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btn2;
}

- (UILabel *)priceStyle {
    if (!_priceStyle) {
        _priceStyle = [[UILabel alloc] init];
        _priceStyle.text = kLocalizedString(@"set_money");
        _priceStyle.textAlignment = 0;
        _priceStyle.font = FifteenFontSize;
        _priceStyle.textColor = HEXCOLOR(0x333333);
        _priceStyle.adjustsFontSizeToFitWidth = YES;
    }
    return _priceStyle;
}

- (UILabel *)priceText {
    if (!_priceText) {
        _priceText = [[UILabel alloc] init];
        _priceText.text = @"CNY";
        _priceText.textAlignment = 2;
        _priceText.font = ThirteenFontSize;
        _priceText.textColor = PlaceHolderColor;
    }
    return _priceText;
}

- (UIImageView *)img2 {
    if (!_img2) {
        _img2 = [[UIImageView alloc] init];
        _img2.image = [UIImage imageNamed:@"fanhui"];
    }
    return _img2;
}

- (UILabel *)line2 {
    if (!_line2) {
        _line2 = [[UILabel alloc] init];
        _line2.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line2;
}

- (UILabel *)versionLb {
    if (!_versionLb) {
        _versionLb = [[UILabel alloc] init];
        _versionLb.textAlignment = 0;
        _versionLb.text = kLocalizedString(@"set_version");
        _versionLb.font = FifteenFontSize;
        _versionLb.textColor = HEXCOLOR(0x333333);
        _versionLb.adjustsFontSizeToFitWidth = YES;
    }
    return _versionLb;
}

- (UILabel *)versionText {
    if (!_versionText) {
        _versionText = [[UILabel alloc] init];
        _versionText.textAlignment = 2;
        _versionText.textColor = PlaceHolderColor;
        _versionText.font = ThirteenFontSize;
    }
    return _versionText;
}

- (UILabel *)line3 {
    if (!_line3) {
        _line3 = [[UILabel alloc] init];
        _line3.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line3;
}


- (UIButton *)btn3 {
    if (!_btn3) {
        _btn3 = [[UIButton alloc] init];
        _btn3.backgroundColor = WhiteColor;
        [_btn3 addTarget:self action:@selector(btn3Action) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btn3;
}

- (UIImageView *)img3 {
    if (!_img3) {
        _img3 = [[UIImageView alloc] init];
        _img3.cornerRadius = 2.5;
        _img3.backgroundColor = [UIColor redColor];
        _img3.hidden = YES;
    }
    return _img3;
}
- (UIButton *)logOutBtn {
    if (!_logOutBtn) {
        _logOutBtn = [[UIButton alloc] init];
        [_logOutBtn setTitle:kLocalizedString(@"set_loginOut") forState:UIControlStateNormal];
        _logOutBtn.cornerRadius = 3;
        _logOutBtn.backgroundColor = ThemeGreenColor;
        [_logOutBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
        _logOutBtn.hidden = YES;
    }
    return _logOutBtn;
}

- (void)logout:(UIButton *)sender {
    ActionSheetModel *model1 = [[ActionSheetModel alloc]init];
    model1.title = kLocalizedString(@"set_ok");
    LoginOutActionSheet *sheet = [LoginOutActionSheet customActionSheetWithData:@[model1]];
    [sheet showFromView:self.view];
    sheet.clickBlock =^(NSString *str){
        [self loginOutAction];
    };
}

- (void)loginOutAction {
    NSLog(@"退出");
    [SBNetworTool postWithUrl:EastLoginOut params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [NotificationManager postLoginOutSuccessNotification];
            [[CacheManager sharedMnager] clearCache];
            [self.navigationController popViewControllerAnimated:YES];
            [IWNotificationCenter postNotificationName:KLoadLeftView object:nil];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)btn1Action {
    LanguageVC *lang = [[LanguageVC alloc] init];
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController pushViewController:lang animated:YES];
}

- (void)btn2Action {
    
}

- (void)btn3Action {
    
}





@end
