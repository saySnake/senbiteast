//
//  ShareView.m
//  ScreenshotsShareView
//
//  Created by 苟应航 on 2018/3/31.
//  Copyright © 2018年 GouHang. All rights reserved.
//

#import "ShareView.h"
#import "MBProgressHUD+ADD.h"
#import "BDFCustomPhotoAlbum.h"

@implementation ShareView

- (instancetype)initWithFrame:(CGRect)frame image:(UIImage *)images{
    if (self = [super initWithFrame:frame]) {
//        [UIView animateWithDuration:0.1 animations:^{
        [self loadviewUI:images];
//        }];
    }
    return self;
}
- (void)loadviewUI:(UIImage *)images{
    [self animationWithView:self duration:0.25];
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor blackColor];
    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiddenview)];
    [view addGestureRecognizer:taps];
    view.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        view.alpha = 0.5;
    }];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(0);
    }];

    NSArray *imageicon = @[@"icon_save",];
    NSArray *imagetitle = @[kLocalizedString(@"SaveImage"),];
    
    UIImageView *shareimage = [UIImageView new];
    shareimage.image = images;
    shareimage.contentMode = UIViewContentModeScaleAspectFill;
    shareimage.alpha = 1;
    [self addSubview:shareimage];

    [shareimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).mas_offset(100);
        make.left.mas_equalTo(40);
        make.right.mas_equalTo(-40);
//        make.bottom.mas_equalTo(endview.mas_top).mas_offset(-100);
        make.bottom.mas_equalTo(self.mas_bottom).mas_offset(-100);
    }];
    self.img = shareimage.image;

    UIView *endview = [UIView new];
    endview.backgroundColor=[UIColor whiteColor];
    [self addSubview:endview];
    [endview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(180);
    }];

    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.text = kLocalizedString(@"ShareTo");
    titleLb.textAlignment = 1;
    titleLb.textColor = [UIColor blackColor];
    [endview addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(endview);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(30);
    }];
    for (int i = 0; i < imageicon.count; i++) {
        FSCustomButton *btn = [[FSCustomButton alloc] init];
        btn.buttonImagePosition = FSCustomButtonImagePositionTop;
        [btn setImage:[UIImage imageNamed:imageicon[i]] forState:UIControlStateNormal];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [btn addTarget:self action:@selector(shareaction:) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = FourteenFontSize;
        btn.titleEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        [btn setTitle:imagetitle[i] forState:UIControlStateNormal];
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [endview addSubview:btn];
        btn.tag = 100 + i;
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(DScreenW / 5);
            make.left.mas_equalTo((DScreenW / 5) * i);
            make.top.mas_equalTo(titleLb.mas_bottom);
            make.height.mas_equalTo(80);
        }];
//        UIView *imageview = [UIView new];
//        [endview addSubview:imageview];
//        [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.mas_equalTo(DScreenW / 3);
//            make.left.mas_equalTo((DScreenW / 3) * i);
//            make.top.mas_equalTo(titleLb.mas_bottom);
//            make.height.mas_equalTo(60);
//        }];
//
//        UIButton *image = [UIButton buttonWithType:UIButtonTypeCustom];
//        image.tag = 100 + i;
//        [image setImage:[UIImage imageNamed:imageicon[i]] forState:UIControlStateNormal];
//        [image addTarget:self action:@selector(shareaction:) forControlEvents:UIControlEventTouchUpInside];
//        [imageview addSubview:image];
//        [image mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.mas_equalTo(25);
//            make.centerX.mas_equalTo(imageview.mas_centerX);
//            make.centerY.mas_equalTo(imageview.mas_centerY).mas_offset(-5);
//        }];
//
//        UIButton *titilelabe = [UIButton buttonWithType:UIButtonTypeCustom];
//        titilelabe.tag = 100 + i;
//        [titilelabe setTitle:imagetitle[i] forState:UIControlStateNormal];
//        [image addTarget:self action:@selector(shareaction:) forControlEvents:UIControlEventTouchUpInside];
//        [titilelabe setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        titilelabe.titleLabel.font = [UIFont systemFontOfSize:14];
//        [imageview addSubview:titilelabe];
//        [titilelabe mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(image.mas_bottom).mas_offset(6);
//            make.centerX.mas_equalTo(image.mas_centerX);
//        }];
    }
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = ThemeGrayColor;
    [endview addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(120);
    }];
    
    UIButton *cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancleBtn setTitle:kLocalizedString(@"Cancel") forState:UIControlStateNormal];
    [cancleBtn setTitleColor:ThemeGrayColor forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    cancleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [endview addSubview:cancleBtn];
    [cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(endview);
        make.top.mas_equalTo(endview.mas_bottom).offset(-70);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(70);
    }];

//    [self addSubview:self.shareLowView];
}



- (void)cancelAction {
    NSLog(@"取消");
    [self hiddenview];
}

- (void)hiddenview {
    self.headActionBlock();
    [UIView animateWithDuration:0.1 animations:^{
        [self removeFromSuperview];
    }];
}

- (void)shareaction:(UIButton *)sender{
    if (sender.tag == 100) { //朋友圈
        UIImage *img;
        NSData *imageData = UIImageJPEGRepresentation(self.img,1.0f);//第二个参数为压缩倍数
        img = [UIImage imageWithData:imageData];
        UIImageWriteToSavedPhotosAlbum(img,self, @selector(image:didFinishSavingWithError:contextInfo:),nil);

//        [[BDFCustomPhotoAlbum shareInstance] saveToNewThumb:self.img];
//        [MBProgressHUD showInformationCenter:@"我点击了朋友圈" toView:self andAfterDelay:2.0];
    } else if (sender.tag == 101) {//QQ
        [MBProgressHUD showInformationCenter:@"我点击了QQ" toView:self andAfterDelay:2.0];
    } else{//微信
        [MBProgressHUD showInformationCenter:@"我点击了微信" toView:self andAfterDelay:2.0];
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo {
    if (error != NULL) {
        [self hiddenview];
//        [MBProgressHUD showInformationCenter:@"图片保存失败" toView:self andAfterDelay:2.0];
        [self showAlertMessage:kLocalizedString(@"SaveImageFailed")];

    } else {
        // No errors
        [self hiddenview];
        [self showAlertMessage:kLocalizedString(@"SaveImageSuccess")];
//        [MBProgressHUD showInformationCenter:@"图片保存成功" toView:self andAfterDelay:2.0];
    }
}


- (void)showAlertMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:nil
                                         otherButtonTitles:kLocalizedString(@"safeConfirm"), nil];
    
    [alert show];
}



- (void)animationWithView:(UIView *)view duration:(CFTimeInterval)duration{
    self.transform = CGAffineTransformMakeScale(0.8, 0.8);
    self.alpha = 0;
     [UIView animateWithDuration:0.35 delay:0 usingSpringWithDamping:0.6f initialSpringVelocity:0.8f options:UIViewAnimationOptionCurveEaseOut
                  animations:^{
                      self.transform=CGAffineTransformMakeScale(1, 1);
                      self.alpha=1;
                  }
                  completion:^(BOOL finished){
         
                  }];

    //暂时不用
//    CAKeyframeAnimation * animation;
//    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
//    animation.duration = duration;
//    animation.removedOnCompletion = NO;
//
//    animation.fillMode = kCAFillModeForwards;
//
//    NSMutableArray *values = [NSMutableArray array];
//    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
//    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
//    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
//
//    animation.values = values;
//    animation.timingFunction = [CAMediaTimingFunction functionWithName: @"easeInEaseOut"];
//
//    [view.layer addAnimation:animation forKey:nil];
}

- (XXKlineShareView *)shareLowView {
    if (_shareLowView == nil) {
        _shareLowView = [[XXKlineShareView alloc] initWithFrame:CGRectMake(0, self.height - 91, kScreen_Width, 91)];
        _shareLowView.backgroundColor = kBlue100;
    }
    return _shareLowView;
}


@end
