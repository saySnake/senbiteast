//
//  HomeBannerModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/26.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeBannerModel : NSObject
/** locale **/
@property (nonatomic ,copy) NSString *locale;

/** title **/
@property (nonatomic ,copy) NSString *title;

/** url **/
@property (nonatomic ,copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
