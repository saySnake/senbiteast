//
//  KSelectedCoinView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/21.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol KSelectedCoinViewDelegate <NSObject>

- (void)hideMenuView;

@end


@interface KSelectedCoinView : BaseView
- (void)firstAppear;
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic ,weak)id <KSelectedCoinViewDelegate> customDelegate;

@end

NS_ASSUME_NONNULL_END
