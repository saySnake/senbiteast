//
//  TradeListModel.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/13.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeListModel.h"

@implementation TradeListModel

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
  if ([property.name isEqualToString:@"createdAt"]) {
        if (oldValue) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
            //当地时间
            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
            NSDate *dateFormatted = [dateFormatter dateFromString:oldValue];
            [dateFormatter setDateFormat:@"HH:mm MM/dd "];
            NSString *locationTimeString = [dateFormatter stringFromDate:dateFormatted];
            return locationTimeString;
        }
  } else if ([property.name isEqualToString:@"state"]) {
      if ([oldValue isEqualToString:@"wait"]) {
          return kLocalizedString(@"trade_wating");
      } else  if ([oldValue isEqualToString:@"done"]) {
          return kLocalizedString(@"trade_done");
      } else  if ([oldValue isEqualToString:@"cancel"]) {
          return kLocalizedString(@"trade_cancled");
      } else  if ([oldValue isEqualToString:@"canceling"]) {
          return kLocalizedString(@"trade_canceling");
      }
  } else if ([property.name isEqualToString:@"orderType"]) {
      if ([oldValue isEqualToString:@"limit"]) {
          return kLocalizedString(@"trade_limit");
      } else if ([oldValue isEqualToString:@"market"]) {
          return kLocalizedString(@"trade_market");
      }
  }
    return oldValue;
}

//- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
//    if ([property.name isEqualToString:@"status"]) {
//        if ([oldValue intValue] == -1) {
//            return @"充币失败";
//        } else if ([oldValue intValue] == 0) {
//            return @"进行中";
//        } else if ([oldValue intValue] == 1) {
//            return @"确认中";
//        } else if ([oldValue intValue] == 2) {
//            return @"已完成";
//        }
//    } else if ([property.name isEqualToString:@"createdAt"]) {
//        if (oldValue) {
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
//            //当地时间
//            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
//            NSDate *dateFormatted = [dateFormatter dateFromString:oldValue];
//            [dateFormatter setDateFormat:@"MM/dd HH:mm"];
//            NSString *locationTimeString = [dateFormatter stringFromDate:dateFormatted];
//            return locationTimeString;
//        }
//    }
//    return oldValue;
//}
//

@end
