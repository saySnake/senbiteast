//
//  FindPasswordVC.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/13.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "FindEmailVC.h"
#import <GT3Captcha/GT3Captcha.h>
#import "AsyncTaskButton.h"
#import "TipsView.h"
#import "SafeInfoSheet.h"
#import "ResetVC.h"
#import "CPCountryTableViewController.h"
#import "CPCountry.h"


@interface FindEmailVC ()<AsyncTaskCaptchaButtonDelegate,UITextFieldDelegate>
/** baseButton **/
@property (nonatomic ,strong) BaseButton *cancelBtn;

/** title **/
@property (nonatomic ,strong) BaseLabel *findTitle;

/** conentLb **/
@property (nonatomic ,strong) BaseLabel *contentLabel;


/** UITextField **/
//@property (nonatomic ,strong) BaseField *accountTF;
@property (nonatomic ,strong) UITextField *accountTF;

@property (nonatomic ,strong) UIView *line;
/** nextBtn **/
@property (nonatomic ,strong) AsyncTaskButton *nextBtn;

/** 地区简称 **/
@property (nonatomic ,strong) NSString *locoal;

@property (nonatomic ,strong) BaseButton *findEmail;


@end

@implementation FindEmailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpViews];
    [self make_Layout];
    [self configKeyBoardRespond];

}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak FindEmailVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}


- (void)setUpViews {
    self.view.backgroundColor = WhiteColor;
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.findTitle];
    [self.view addSubview:self.contentLabel];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.line];
    [self.view addSubview:self.nextBtn];
    [self.view addSubview:self.findEmail];
}

- (void)make_Layout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.findTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(136);
        make.right.mas_equalTo(-30);
        make.left.mas_equalTo(30);
        make.height.mas_equalTo(26);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.findTitle.mas_bottom).offset(27);
        make.height.mas_equalTo(50);
        make.right.mas_equalTo(-30);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).offset(70);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(kLinePixel);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(5);
    }];
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(70);
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(53);
    }];
    
    [self.findEmail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nextBtn.mas_bottom).offset(25);
        make.centerX.mas_equalTo(self.view);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(20);
    }];
}

#pragma mark - Event Response
- (void)cancelAction {
    [self.navigationController popViewControllerAnimated:YES];
}


//demo
- (void)captchaWillDisplay {
    [TipsView removeAllTipsView];
}

- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
    if (self.accountTF.text.length == 0) {
        [self showToastView:kLocalizedString(@"toast_notAccount")];
        return NO;
    }
    [TipsView showTipOnKeyWindow:kLocalizedString(@"login_safe")];
    return YES;
}

- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    [self.view endEditing:YES];
    if (status == YES) {
        if (self.accountTF.text.length == 0) {
            [self.view endEditing:YES];
            [self showToastView:kLocalizedString(@"lgoin_pleaseEmOrPhone")];
            return;
        }
        [self requestInfoType];
    }
}

- (void)requestInfoType {
    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
    NSDictionary *dic = @{@"challenge":user.geetest_challenge,
                          @"seccode":user.geetest_seccode,
                          @"validate":user.geetest_validate,
                          @"email":self.accountTF.text};
    [SBNetworTool getWithUrl:EastInfoType params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self nextAction:[responseObject[@"data"][@"emailAuthEnable"] boolValue] googleAuthEnable:[responseObject[@"data"][@"googleAuthEnable"] boolValue] phoneAuthEnable:[responseObject[@"data"][@"phoneAuthEnable"] boolValue] userToken:responseObject[@"data"][@"userToken"] email:responseObject[@"data"][@"email"] phone:responseObject[@"data"][@"phone"]];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)nextAction:(BOOL)emailAuthEnable
  googleAuthEnable:(BOOL)googleAuthEnable
   phoneAuthEnable:(BOOL)phoneAuthEnable
         userToken:(NSString *)token
             email:(NSString *)email
             phone:(NSString *)phone{
    WS(weakSelf);
    SafeInfoSheet *sheet = [[SafeInfoSheet alloc] init];
    sheet.userToken = token;
    sheet.googleAuth = googleAuthEnable;
    sheet.emailAuth = emailAuthEnable;
    sheet.phoneAuth = phoneAuthEnable;
    sheet.country = self.locoal;
    sheet.emailTitleName.text = email;
    sheet.phoneLabel.text = phone;
    sheet.clickBlock = ^(NSString * _Nonnull phoneCode, NSString * _Nonnull emailCode, NSString * _Nonnull googleCode, NSString * _Nonnull phone, NSString * _Nonnull email, NSString * _Nonnull userToken) {
        if (phoneCode.length == 0) {
            phoneCode = @"";
        }
        if (emailCode.length == 0) {
            emailCode = @"";
        }
        if (googleCode.length == 0) {
            googleCode = @"";
        }
        if (email.length == 0) {
            email = @"";
        }
        if (phone.length == 0) {
            phone = @"";
        }
        [weakSelf indeify:phoneCode emailCode:emailCode googleCode:googleCode phone:phone email:email token:userToken];
    };
    [sheet showFromView:self.view];
}

//校验验证码
- (void)indeify:(NSString *)phoneCode emailCode:(NSString *)emailCode googleCode:(NSString *)googleCode phone:(NSString *)phone email:(NSString *)email  token:(NSString *)token {
    NSDictionary *dic = @{@"userToken":token,
                          @"emailCode":emailCode,
                          @"phoneCode":phoneCode,
                          @"googleCode":googleCode,
                          @"type":@"FORGET_PASSWORD"
    };
    WS(weakSelf);
    [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [weakSelf nextController:responseObject[@"data"][@"authToken"] userToken:token];
        } else {
            [weakSelf showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [weakSelf showToastView:FailurMessage];
    }];
}

- (void)nextController:(NSString *)authToken userToken:(NSString *)userToken {
    ResetVC *reset = [[ResetVC alloc] init];
    reset.authToken = authToken;
    reset.userToken = userToken;
    [self.navigationController pushViewController:reset animated:YES];
}

- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"find_cancel") forState:UIControlStateNormal];
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        _cancelBtn.titleLabel.font = kFont16;
        _cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseLabel *)findTitle {
    if (!_findTitle) {
        _findTitle = [[BaseLabel alloc] init];
        _findTitle.colorName = @"find_titleColor";
        _findTitle.font = BOLDSYSTEMFONT(33);
        _findTitle.text = kLocalizedString(@"find_title");
        _findTitle.textAlignment = 0;
        _findTitle.adjustsFontSizeToFitWidth = YES;
    }
    return _findTitle;
}

- (BaseLabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[BaseLabel alloc] init];
        _contentLabel.colorName = @"Login_conentColor";
        _contentLabel.font = kFont16;
        _contentLabel.textAlignment = 0;
        _contentLabel.text = kLocalizedString(@"find_content");
        _contentLabel.adjustsFontSizeToFitWidth = YES;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}

//- (BaseField *)accountTF {
//    if (!_accountTF) {
//        _accountTF = [[BaseField alloc] init];
//        _accountTF.placeholder = kLocalizedString(@"find_placeder");
//        _accountTF.placeHolderColor = PlaceHolderColor;
//    }
//    return _accountTF;
//}

- (UITextField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[UITextField alloc] init];
        _accountTF.textColor = FieldTextColor;
        _accountTF.textAlignment = 0;
        _accountTF.font = FifteenFontSize;
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:kLocalizedString(@"login_pleaseEmailPlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _accountTF.attributedPlaceholder = attrString;
        _accountTF.adjustsFontSizeToFitWidth = YES;
        _accountTF.delegate = self;
        [_accountTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}


- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = LineColor;
    }
    return _line;
}

- (AsyncTaskButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [AsyncTaskButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.cornerRadius = 8;
        _nextBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [_nextBtn setTitle:kLocalizedString(@"login_next") forState:UIControlStateNormal];
        _nextBtn.delegate = self;
        _nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _nextBtn.userInteractionEnabled = NO;

    }
    return _nextBtn;
}



- (BaseButton *)findEmail {
    if (!_findEmail) {
        _findEmail = [[BaseButton alloc] init];
        [_findEmail setTitle:kLocalizedString(@"find_phone") forState:UIControlStateNormal];
        [_findEmail setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _findEmail.titleLabel.font = FourteenFontSize;
        _findEmail.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_findEmail addTarget:self action:@selector(findEmailAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _findEmail;
}

- (void)findEmailAction {
    //邮箱找回
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super hideKeyboard];
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountTF.text.length > 0) {
        self.nextBtn.userInteractionEnabled = YES;
        self.nextBtn.backgroundColor = ThemeGreenColor;

    } else {
        self.nextBtn.userInteractionEnabled = NO;
        self.nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);

    }
}


@end
