//
//  TradeDetailHeaderVIew.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradeListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TradeDetailHeaderVIew : UIView

@property (nonatomic ,strong) UILabel *line;
@property (nonatomic ,strong) UIScrollView *scroll;
@property (nonatomic ,strong) UILabel *direct;
@property (nonatomic ,strong) UILabel *coin;

//时间
@property (nonatomic ,strong) UILabel *timeLb;
//委托价格
@property (nonatomic ,strong) UILabel *entrustLb;
//委托数量
@property (nonatomic ,strong) UILabel *entrustNum;

@property (nonatomic ,strong) UILabel *timeText;
@property (nonatomic ,strong) UILabel *entrustText;
@property (nonatomic ,strong) UILabel *entrustNumText;
//委托总额
@property (nonatomic ,strong) UILabel *totalLb;
//类型
@property (nonatomic ,strong) UILabel *typeLb;
//成交数量
@property (nonatomic ,strong) UILabel *orderNumLb;

@property (nonatomic ,strong) UILabel *totalLbText;
@property (nonatomic ,strong) UILabel *typeLbText;
@property (nonatomic ,strong) UILabel *orderText;


@property (nonatomic ,strong) TradeListModel *model;
- (void)setUpview;



@end

NS_ASSUME_NONNULL_END
