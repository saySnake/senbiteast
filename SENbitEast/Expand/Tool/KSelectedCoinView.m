//
//  SelectedCoinView.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/20.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "KSelectedCoinView.h"
#import "SelectedCell.h"
#import "HomeCoinPairModel.h"


@interface KSelectedCoinView()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate> {
    
}
@property (nonatomic ,strong) UIView *headView;
@property (nonatomic, weak) UIScrollView *titleScroll;
@property (nonatomic, strong) NSMutableArray *titleButton;
@property (nonatomic, weak) UIButton *selectBtn;
@property (nonatomic, strong) UITableView *tbleView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (strong, nonatomic) XXWebQuoteModel *webModel;
@property (strong, nonatomic) XXWebQuoteModel *tbModel;

@property (nonatomic ,strong) NSString *selectedCoin;
@property (nonatomic ,assign) BOOL isCollection;//是否自选
@property (nonatomic ,assign) BOOL isOpenZone; //是否开放
@property (nonatomic ,strong) NSMutableArray *data;
@property (nonatomic ,strong) NSMutableArray *collectionArr;
@property (nonatomic ,strong) NSMutableArray *openArray;

/** 分割线 */
@property (strong, nonatomic) UIView *lineView;


@end

@implementation KSelectedCoinView

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = HEXCOLOR(0x1B1B1B);
        [self webSocketHomeDatae];
        [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
        [IWNotificationCenter addObserver:self selector:@selector(reload) name:AddFavorioutficationNmae object:nil];
        
    }
    return self;
}

- (void)reload {
    for (int i =0; i <self.titleButton.count; i ++) {
        UIButton *btn = [[UIButton alloc] init];
        if (i == 0) {
            [self topTitleBtnClick:btn];
            [self firstAppear];
        }
    }

}
- (void)changeLanguage {
    self.titleLb.text = kLocalizedString(@"kline_Coin");
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (NSMutableArray *)collectionArr {
    if (!_collectionArr) {
        _collectionArr = [[NSMutableArray alloc] init];
    }
    return _collectionArr;
}

- (NSMutableArray *)openArray {
    if (!_openArray) {
        _openArray = [[NSMutableArray alloc] init];
    }
    return _openArray;
}


- (void)firstAppear {
    self.selectedCoin = self.titleButton[self.selectBtn.tag];
    if ([self.selectedCoin isEqualToString:kLocalizedString(@"kline_selected")]) {
        self.isCollection = YES;
        [self getPairCollection];
    } else if ([self.selectedCoin isEqualToString:@"开放区"]) {
        self.isOpenZone = YES;
        [self getLocalZone];
    } else {
        self.isCollection = NO;
    }
    
    if (self.isCollection) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in self.data) {
            for (OneTableSockeModel *m1 in self.collectionArr) {
                if ([m1.coinName isEqualToString:model.coinName]) {
                    model.isCollection = YES;
                    [self.dataArr addObject:model];
                }
            }
        }
    }
    if (self.isOpenZone) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in self.data) {
            for (OneTableSockeModel *m1 in self.openArray) {
                for (OneTableSockeModel *m2 in self.collectionArr) {
                    if ([m2.coinName isEqualToString:m1.coinName]) {
                        model.isCollection = YES;
                    }
                }
                if ([m1.coinName isEqualToString:model.coinName]) {
                        [self.dataArr addObject:model];
                }
            }
        }
    }
    if (!self.isCollection && !self.isOpenZone) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in self.data) {
            NSUInteger seleLenth = self.selectedCoin.length;
            NSString *endStr = [model.coinName substringFromIndex:model.coinName.length - seleLenth];
            if ([endStr isEqualToString:self.selectedCoin]) {
                for (OneTableSockeModel *m in self.collectionArr) {
                    if ([m.coinName isEqualToString:model.coinName]) {
                        model.isCollection = YES;
                        break;
                    }
                }
                [self.dataArr addObject:model];
            }
        }
    }
    [self.tbleView reloadData];
}

- (void)socketData {
    KWeakSelf
    self.tbModel = nil;
    self.tbModel = [[XXWebQuoteModel alloc] init];
    self.tbModel.successBlock = ^(id data) {
        weakSelf.data = [OneTableSockeModel mj_objectArrayWithKeyValuesArray:data];
        if (weakSelf.isCollection) {
            [weakSelf.dataArr removeAllObjects];
            for (OneTableSockeModel *model in weakSelf.data) {
                for (OneTableSockeModel *m1 in weakSelf.collectionArr) {
                    if ([m1.coinName isEqualToString:model.coinName]) {
                        model.isCollection = YES;
                        [weakSelf.dataArr addObject:model];
                    }
                }
            }
        }
        if (weakSelf.isOpenZone) {
            [weakSelf.dataArr removeAllObjects];
            for (OneTableSockeModel *model in weakSelf.data) {
                for (OneTableSockeModel *m1 in weakSelf.openArray) {
                    for (OneTableSockeModel *m2 in weakSelf.collectionArr) {
                        if ([m2.coinName isEqualToString:m1.coinName]) {
                            model.isCollection = YES;
                        }
                    }
                    if ([m1.coinName isEqualToString:model.coinName]) {
                            [weakSelf.dataArr addObject:model];
                    }
                }
            }
        }
        if (!weakSelf.isCollection && !weakSelf.isOpenZone) {
            [weakSelf.dataArr removeAllObjects];
            for (OneTableSockeModel *model in weakSelf.data) {
                NSUInteger seleLenth = weakSelf.selectedCoin.length;
                NSString *endStr = [model.coinName substringFromIndex:model.coinName.length - seleLenth];
                if ([endStr isEqualToString:weakSelf.selectedCoin]) {
                    for (OneTableSockeModel *m in weakSelf.collectionArr) {
                        if ([m.coinName isEqualToString:model.coinName]) {
                            model.isCollection = YES;
                            break;
                        }
                    }
                    [weakSelf.dataArr addObject:model];
                }
            }
        }
        [weakSelf.tbleView reloadData];
    };
    NSDictionary *dic = @{};
    self.tbModel.params = [NSMutableDictionary dictionary];
    self.tbModel.params[@"topic"] = @"home-overview";
    self.tbModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.tbModel lineStype:@"home-overview"];
}




- (void)getLocalZone {
    
}
- (void)getPairCollection {
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (info.login) {
        [SBNetworTool getWithUrl:EastOptionalPairs params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                NSMutableArray * arr = responseObject[@"data"];
                [KMarket.favoritesArray removeAllObjects];
                for (NSString *s in arr) {
                    [KMarket addFavoriteSymbolId:s];
                }
                [self.collectionArr removeAllObjects];
                for (NSString *str in arr) {
                    OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
                    model.coinName = str;
                    [self.collectionArr addObject:model];
                }
                [self socketData];
            }

        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
            [self.collectionArr addObject:model];
            [self socketData];
        }];
    } else {
//        KMarket.favoritesArray
        NSLog(@"%@",KMarket.favoritesArray);
        [self.collectionArr removeAllObjects];
        for (NSString *str in KMarket.favoritesArray) {
            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
            model.coinName = str;
            [self.collectionArr addObject:model];
        }
        [self socketData];
    }

}

- (void)webSocketHomeDatae {
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        NSMutableArray *titleArr = [HomeCoinPairModel mj_objectArrayWithKeyValuesArray:data];
        [weakSelf.titleButton removeAllObjects];
        for (HomeCoinPairModel *model in titleArr) {
            [weakSelf.titleButton addObject:[NSString stringWithFormat:@"%@",model.coinName]];
        }
        [weakSelf.titleButton insertObject:kLocalizedString(@"kline_selected") atIndex:0];
        [weakSelf setUpTitleScroll];
        [weakSelf socketData];
    };
    NSDictionary *dic = @{};
    self.webModel.params = [NSMutableDictionary dictionary];
    self.webModel.params[@"topic"] = @"home-coins";
    self.webModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"home-coins"];
    
}

- (void)initView {
    self.titleLb.frame = CGRectMake(10, 30, 100, 50);
    [self.headView addSubview:self.titleLb];
    self.headView.frame = CGRectMake(0, 0, self.width, 90);
    [self addSubview:self.headView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectedCell *cell = [SelectedCell cellWithTableView:tableView];
    
    cell.coin.textColor = HEXCOLOR(0x4A4A4A);
    cell.contentView.backgroundColor = HEXCOLOR(0x1B1B1B);
    OneTableSockeModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    NSRange range;
    range = [model.coinName rangeOfString:@"/"];
    NSMutableAttributedString *strs = [[NSMutableAttributedString alloc] initWithString:model.coinName];
    [strs addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithLightColorStr:@"FFFFFF" DarkColor:@"FFFFFF"] range:NSMakeRange(0, range.location)];
    [strs addAttribute:NSFontAttributeName
                value:[UIFont fontWithName:@"Arial" size:20.0] range:NSMakeRange(0, range.location)];
    cell.coin.attributedText = strs;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OneTableSockeModel *model = self.dataArr[indexPath.row];
    if ([self.customDelegate respondsToSelector:@selector(hideMenu)]) {
        [self.customDelegate hideMenuView];
    }
    NSDictionary *dic = @{@"coinName":model.coinName};
    [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
           KDetail.symbolModel = obj;
           KTrade.coinTradeModel = obj;
           NSLog(@"%@",KDetail.symbolModel.symbolId);
           *stop = YES;
         }
   }];
   
    [NotificationManager tradeSelectedSwitchTradeSymbolNotification:dic];
  
}


- (NSMutableArray *)titleButton {
    if (!_titleButton) {
        _titleButton = [[NSMutableArray alloc] init];
    }
    return _titleButton;
}


- (void)selctedBtn:(UIButton *)btn {
    NSLog(@"_________________%d",btn.tag);
    [_selectBtn setTitleColor:HEXCOLOR(0x999999) forState:UIControlStateNormal];
    [btn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
    [self setupTitleButtonCenter:btn];
    _selectBtn.transform = CGAffineTransformIdentity;
    btn.transform = CGAffineTransformMakeScale(1.0, 1.0);
    _selectBtn = btn;
    _selectBtn.tag = btn.tag;
}


- (void)setUpTitleScroll {
    [self initView];
    UIScrollView *view = [[UIScrollView alloc] init];
    view.backgroundColor = HEXCOLOR(0x1B1B1B);
    view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    view.frame = CGRectMake(0, CGRectGetMaxY(self.headView.frame), self.width, 50);
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 50 - kLinePixel, self.width*self.titleButton.count, kLinePixel)];
    line.backgroundColor = HEXCOLOR(0xDDDDDD);
    
    [view addSubview:line];

    view.showsHorizontalScrollIndicator = NO;
    self.titleScroll = view;

    CGFloat btnW = 60;
    CGFloat btnH = 35;
    for (int i = 0; i < self.titleButton.count; i++) {
        UIButton *btn = [[UIButton alloc] init];;
        btn.frame = CGRectMake(10 + btnW * i , 5, btnW, btnH);
        btn.titleLabel.font = kFont14;
        [btn setTitle:self.titleButton[i] forState:UIControlStateNormal];
        [btn setTitleColor:HEXCOLOR(0x999999) forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn addTarget:self action:@selector(topTitleBtnClicks:) forControlEvents:UIControlEventTouchDown];
        btn.tag = i;
        [view addSubview:btn];
        if (btn.tag == 0) {
            CGFloat width = [NSString widthWithText:kLocalizedString(@"kline_selected") font:kFontBold14];
            CGFloat lineLeftX = btn.centerX - width/2;
            self.lineView = [[UIView alloc] initWithFrame:CGRectMake(lineLeftX, 50 -2*kLinePixel, [NSString widthWithText:kLocalizedString(@"kline_selected") font:kFontBold14], 2*kLinePixel)];
            self.lineView.backgroundColor = ThemeGreenColor;
            [view addSubview:self.lineView];
            [self topTitleBtnClick:btn];
        }
    }
    [self addSubview:view];
    self.titleScroll.contentSize = CGSizeMake(btnW *self.titleButton.count, 0);
    [self addSubview:self.tbleView];
}

- (void)topTitleBtnClicks:(UIButton *)btn {
    CGFloat width = btn.titleLabel.width;
    CGFloat lineLeftX = btn.centerX - width / 2.0f;

    [UIView animateWithDuration:0.3 animations:^{
        self.lineView.left = lineLeftX;
        self.lineView.width = width;
    }];

    [self selctedBtn:btn];
    [self firstAppear];
}

- (void)topTitleBtnClick:(UIButton *)btn {
    [self selctedBtn:btn];
    [self firstAppear];
}

//让标题居中
- (void)setupTitleButtonCenter:(UIButton *)button {
    CGFloat a = DScreenW / self.titleButton.count;
    if (a < 60) {
        // 修改偏移量
        CGFloat offsetX = button.center.x - self.width * 0.5;
        
        // 处理最小滚动偏移量
        if (offsetX < 0) {
            offsetX = 0;
        }
        
        // 处理最大滚动偏移量
        CGFloat maxOffsetX = self.titleScroll.contentSize.width - self.width;
        if (offsetX > maxOffsetX) {
            offsetX = maxOffsetX;
        }
        [self.titleScroll setContentOffset:CGPointMake(offsetX, 0) animated:YES];
    }
}


- (UITableView *)tbleView {
    if (_tbleView == nil) {
        _tbleView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScroll.frame), self.width, kScreen_Height - CGRectGetMaxY(self.titleScroll.frame)) style:UITableViewStylePlain];
        _tbleView.backgroundColor = [UIColor whiteColor];
        _tbleView.dataSource = self;
        _tbleView.delegate = self;
        _tbleView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, BH_IS_IPHONE_X ? 83 : 65)];
        _tbleView.estimatedRowHeight = 0;
        _tbleView.backgroundColor = HEXCOLOR(0x1B1B1B);
        _tbleView.estimatedSectionHeaderHeight = 0;
        _tbleView.estimatedSectionFooterHeight = 0;
        if (@available(iOS 11.0, *)) {
            _tbleView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tbleView;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    NSInteger i = scrollView.contentOffset.x / self.width;
    
//    UIButton *btn = self.titleButton[i];
    
//    [self selctedBtn:btn];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger leftI = scrollView.contentOffset.x / self.width;

    NSInteger rightI = leftI + 1;

    // 1.获取需要形变的按钮

    // left
    // 获取左边按钮
    UIButton *leftButton = self.titleButton[leftI];

    // right
    NSUInteger count = self.titleButton.count;
    UIButton *rigthButton;
    // 获取右边按钮
    if (rightI < count) {
        rigthButton = self.titleButton[rightI];
    }

    // 计算右边按钮偏移量
    CGFloat rightScale = scrollView.contentOffset.x / self.width;
    // 只想要 0~1
    rightScale = rightScale - leftI;

    CGFloat leftScale = 1 - rightScale;

    // 形变按钮
    // scale 0 ~ 1 => 1 ~ 1.3
//    leftButton.transform = CGAffineTransformMakeScale(leftScale * 0.3 + 1, leftScale * 0.3 + 1);
//    rigthButton.transform = CGAffineTransformMakeScale(rightScale * 0.3 + 1, rightScale * 0.3 + 1);

//    [rigthButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//    [leftButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
}


- (UIView *)headView {
    if (!_headView) {
        _headView = [[UIView alloc] init];
        _headView.backgroundColor = HEXCOLOR(0x1B1B1B);
    }
    return _headView;
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.text = kLocalizedString(@"kline_Coin");
        _titleLb.textAlignment = 0;
        _titleLb.textColor = [UIColor whiteColor];
        _titleLb.font = kFont18;
    }
    return _titleLb;
}

//- (UIView *)lineView {
//    if (_lineView == nil) {
////        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 -2*kLinePixel, [NSString widthWithText:@"自选" font:kFontBold14], 2*kLinePixel)];
//        _lineView = [[UIView alloc] init];
//        _lineView.backgroundColor = ThemeGreenColor;
//    }
//    return _lineView;
//}



@end
