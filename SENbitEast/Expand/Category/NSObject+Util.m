//
//  NSObject+Util.m
//  Senbit
//
//  Created by 张玮 on 2019/12/20.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "NSObject+Util.h"


@implementation NSObject (Util)

- (CGSize)sizeWithText:(NSString *)text {
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.font = ThirteenFontSize;
    //    label.font =[UIFont boldSystemFontOfSize:13];
    NSDictionary *attribute =@{NSFontAttributeName :label.font};
    CGSize size =[text boundingRectWithSize:CGSizeMake(1000, MAXFLOAT) options:0|1 attributes:attribute context:nil].size;
    return size;
}


- (CGSize)sizeWithText:(NSString *)text withFont:(CGFloat)font {
    UILabel *label = [[UILabel alloc]init];
    label.text = text;
    label.font = [UIFont systemFontOfSize:font];
    //    label.font =[UIFont boldSystemFontOfSize:13];
    NSDictionary *attribute = @{NSFontAttributeName :label.font};
    CGSize size = [text boundingRectWithSize:CGSizeMake(1000, MAXFLOAT) options:0|1 attributes:attribute context:nil].size;
    return size;
}

- (CGSize)sizeWithWidth:(NSString *)text float:(CGFloat )flo {
    UILabel *label = [[UILabel alloc]init];
    label.text = text;
    label.font = ThirteenFontSize;
    //    label.font =[UIFont boldSystemFontOfSize:13];
    NSDictionary *attribute = @{NSFontAttributeName :label.font};
    CGSize size = [text boundingRectWithSize:CGSizeMake(flo, MAXFLOAT) options:0|1 attributes:attribute context:nil].size;
    return size;
}


- (CGSize)sizeWithWidth:(NSString *)text float:(CGFloat )flo heght:(CGFloat)height {
    UILabel *label =[[UILabel alloc]init];
    label.text = text;
    label.font = ThirteenFontSize;
    NSDictionary *attribute = @{NSFontAttributeName :label.font};
    CGSize size =[text boundingRectWithSize:CGSizeMake(flo, height) options:0|1 attributes:attribute context:nil].size;
    return size;
}


- (void)delay:(NSTimeInterval)timer task:(dispatch_block_t)task {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timer * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        task();
    });
}

- (void)cancle:(dispatch_block_t)task {
    if (task) {
        dispatch_block_cancel(task);
    }
}

- (void)fastEncode:(NSCoder *)aCoder {
    u_int count = 0;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    for (int i=0; i<count; i++) {
        const char* pname = property_getName(properties[i]);
        NSString *key = [NSString stringWithUTF8String:pname];
        id value = [self valueForKey:key];
        [aCoder encodeObject:value forKey:key];
    }
    free(properties);
}

- (void)fastDecode:(NSCoder *)aDecoder {
    u_int count = 0;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    for (int i=0; i<count; i++) {
        const char* pname = property_getName(properties[i]);
        NSString *key = [NSString stringWithUTF8String:pname];
        id value = [aDecoder decodeObjectForKey:key];
        if (value) {
            [self setValue:value forKey:key];
        }
    }
    free(properties);
}


@end
