//
//  CPTableViewCell.h
//  CountryPicker
//
//  Created by 张玮 on 2020/1/10.
//  Copyright © 2020 Lv Qingyang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CPTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
+ (CPTableViewCell *)cell;
@end

NS_ASSUME_NONNULL_END
