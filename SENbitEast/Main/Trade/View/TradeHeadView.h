//
//  TradeHeadView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/29.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TradeHeadViewDelegate <NSObject>

@optional

- (void)traderBuyAction;

@end

@interface TradeHeadView : UIView

@property (strong, nonatomic) void(^TradeHeadViewBlock)();
@property (strong, nonatomic) void(^switchBtnBlock)();

@property (strong, nonatomic) void(^TradeBuyBlock)(NSDictionary *);
//计算新的数据
- (void)calData;

- (void)dismiss;

@property (nonatomic ,assign) id<TradeHeadViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
