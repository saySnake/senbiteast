//
//  TradeCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeCell.h"
#import "UIColor+Y_StockChart.h"
#define LeftSpace K375(15)
#define TopSpace K375(0)

@implementation TradeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.rightLowView];
    /** 右上视图 */
    [self.rightLowView addSubview:self.rightTopView];

    /** 右价格标签 */
    [self.rightLowView addSubview:self.rightPriceLabel];
    
    /** 右成交量 */
    [self.rightLowView addSubview:self.rightNumberLabel];
}

- (void)reloadData {
    if (self.model == nil) {
        self.rightTopView.width = 0;
        self.rightPriceLabel.text = @"--";
        self.rightNumberLabel.text = @"--";
    } else {
        
    }
    self.rightPriceLabel.font = kFont(12.0f);
    self.rightNumberLabel.font = kFont(12.0f);
    
    if (self.model.rightPrice) {
        
        self.rightPriceLabel.text = self.model.rightPrice;
        
        if (self.isAmount) {
//            self.rightNumberLabel.text = [NSString handValuemeLengthWithAmountStr:self.model.rightNumber];
        } else {
//            self.rightNumberLabel.text = [NSString handValuemeLengthWithAmountStr:[NSString stringWithFormat:@"%.13f", self.model.rightSumNumber]];
            self.rightNumberLabel.text = [NSString handValuemeLengthWithAmountStr:self.model.rightNumber];

        }
        
        self.rightPriceLabel.text = self.model.rightPrice;
        if (self.ordersAverage == 0) {
            self.ordersAverage = 1;
        }
        
        CGFloat rightValue = self.model.rightSumNumber/self.ordersAverage;
        if (rightValue >= 1.0) {
            self.rightTopView.width = 0;
        } else {
            self.rightTopView.width = (1 - rightValue)*self.rightLowView.width;
        }
    } else {
        self.rightTopView.width = self.rightLowView.width;
        self.rightPriceLabel.text = @"--";
        self.rightNumberLabel.text = @"--";
    }
}

+ (CGFloat)getCellHeight {
    return 32;
}

#pragma mark - || 懒加载
/** 右成交量 */
- (XXLabel *)rightNumberLabel {
    if (_rightNumberLabel == nil) {
        _rightNumberLabel = [XXLabel labelWithFrame:CGRectMake(CGRectGetMaxX(self.rightLowView.frame) - DScreenW/4 -10, 0,DScreenW/4, 32) text:@"" font:kFont12 textColor:[UIColor blackColor] alignment:2];
        _rightNumberLabel.text = @"--";
        _rightNumberLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _rightNumberLabel;
}

/** 右价格标签 */
- (XXLabel *)rightPriceLabel {
    if (_rightPriceLabel == nil) {
        _rightPriceLabel = [XXLabel labelWithFrame:CGRectMake(K375(5), 0, Kscal(170), 32) font:kFont12 textColor:kRed100];
        _rightPriceLabel.text = @"--";
        _rightPriceLabel.textAlignment = 0;
        _rightPriceLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _rightPriceLabel;
}

/** 右上视图 */
- (UIView *)rightTopView {
    if (_rightTopView == nil) {
        _rightTopView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 32)];
        _rightTopView.backgroundColor = [UIColor whiteColor];
        _rightTopView.userInteractionEnabled = NO;
    }
    return _rightTopView;
}

/** 右下视图 */
- (UIView *)rightLowView {
    if (_rightLowView == nil) {
        _rightLowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DScreenW/2 - 10, 32)];
        _rightLowView.backgroundColor = kRed20;
    }
    return _rightLowView;
}


@end
