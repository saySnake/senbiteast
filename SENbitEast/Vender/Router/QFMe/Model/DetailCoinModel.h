//
//  DetailCoinModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/28.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailCoinModel : NSObject

@property (nonatomic ,strong) NSString *addressURL;
@property (nonatomic ,strong) NSString *chargeConfirmLimit;
@property (nonatomic ,strong) NSString *enName;
@property (nonatomic ,strong) NSString *ID;
@property (nonatomic ,strong) NSString *txURL;
@property (nonatomic ,strong) NSString *withdrawalConfirmLimit;

@end

NS_ASSUME_NONNULL_END
