//
//  AssertListModel.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/8.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AssertListModel.h"

@implementation AssertListModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"_id",
             @"withdrawStatus" :@"status",
             @"withdrawCode" : @"status"
             };
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
    if ([property.name isEqualToString:@"status"]) {
        if ([oldValue intValue] == -1) {
            return kLocalizedString(@"deposit_coinFail");
        } else if ([oldValue intValue] == 0) {
            return kLocalizedString(@"deposit_coining");
        } else if ([oldValue intValue] == 1) {
            return kLocalizedString(@"deposit_ok");
        } else if ([oldValue intValue] == 2) {
            return kLocalizedString(@"deposit_done");
        }
    } else if ([property.name isEqualToString:@"createdAt"]) {
        if (oldValue) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
            //当地时间
            dateFormatter.timeZone = [NSTimeZone systemTimeZone];
            NSDate *dateFormatted = [dateFormatter dateFromString:oldValue];
            [dateFormatter setDateFormat:@"HH:mm MM/dd"];
            NSString *locationTimeString = [dateFormatter stringFromDate:dateFormatted];
            return locationTimeString;
        }
    } else if ([property.name isEqualToString:@"withdrawStatus"]) {
        if ([oldValue intValue] == -1) {
            return kLocalizedString(@"deposit_cancel");
        } else if ([oldValue intValue] == 0) {
            return kLocalizedString(@"deposit_coining");
        } else if ([oldValue intValue] == 1) {
            return kLocalizedString(@"deposit_coining");
        } else if ([oldValue intValue] == 2) {
            return kLocalizedString(@"deposit_coining");
        } else if ([oldValue intValue] == 3) {
            return kLocalizedString(@"deposit_coining");
        } else if ([oldValue intValue] == 4) {
            return kLocalizedString(@"deposit_coining");
        } else if ([oldValue intValue] == 5) {
            return kLocalizedString(@"deposit_done");
        } else if ([oldValue intValue] == 6) {
            return kLocalizedString(@"deposit_done");
        } else if ([oldValue intValue] == 7) {
            return kLocalizedString(@"deposit_expe");
        } else if ([oldValue intValue] == -6) {
            return kLocalizedString(@"deposit_expe");
        } else if ([oldValue intValue] == -5) {
            return kLocalizedString(@"withdraw_fail");
        } else if ([oldValue intValue] == -4) {
            return kLocalizedString(@"withdraw_fail");
        } else if ([oldValue intValue] == -3) {
            return kLocalizedString(@"withdraw_fail");
        } else if ([oldValue intValue] == -2) {
            return kLocalizedString(@"withdraw_notAllow");
        }
    }
    return oldValue;
}

@end
