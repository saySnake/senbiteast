//
//  XXBitcoinDetailVC.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "XXBitcoinDetailVC.h"
#import "XXBDetailHeaderView.h"
#import "XXBDetailSectionHeaderView.h"
#import "XXTabBarController.h"
#import "XXDetailDepthListView.h"
#import "XXDetailTradesListView.h"
#import "XXKlineShareView.h"
#import "UIColor+Y_StockChart.h"
#import "XXCoinInfoView.h"
#import "ShareView.h"
#import "SelectedViewController.h"
#import "SelectedMenView.h"
#import "KSelectedCoinView.h"

@interface XXBitcoinDetailVC () <UITableViewDataSource, UITableViewDelegate,KSelectedCoinViewDelegate>

/** 收藏按钮 */
@property (strong, nonatomic) UIButton *saveButton;
/** 全凭按钮 */
@property (strong, nonatomic) UIButton *allScreentButton;
/** 分享按钮 */
@property (strong, nonatomic) UIButton *shareButton;

/** 竖线 */
@property (strong, nonatomic, nullable) UIView *lineView;

/** 切换按钮 */
@property (strong, nonatomic, nullable) FSCustomButton *switchButton;

/** 看涨看跌标签 */
@property (strong, nonatomic) XXLabel *markLabel;

/** 头视图 */
@property (strong, nonatomic) XXBDetailHeaderView *headView;

/** 区头视图 */
@property (strong, nonatomic) XXBDetailSectionHeaderView *sectionHeaderView;

/** 表示图 */
@property (strong, nonatomic) UITableView *tableView;

/** 底版视图 */
@property (strong, nonatomic) UIVisualEffectView *lowView;

/** 买入按钮 */
@property (strong, nonatomic) XXButton *buyButton;

/** 卖出按钮 */
@property (strong, nonatomic) XXButton *sellButton;

/** 分享底视图 */
@property (strong, nonatomic) XXKlineShareView *shareLowView;

/** 深度图 */
@property (strong, nonatomic) XXDetailDepthListView *depthView;

/** 交易图 */
@property (strong, nonatomic) XXDetailTradesListView *tradesView;

/** 简介视图 */
@property (strong, nonatomic) XXCoinInfoView *infoView;

@property (nonatomic ,strong) SelectedMenView *menu;
@property (nonatomic ,strong) KSelectedCoinView *demo;

@end

@implementation XXBitcoinDetailVC
static NSString *identifier = @"XXBDetailCell";
static NSString *identifierTwo = @"XXEntrustmentOrderCell";

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navView.hidden = NO;
    [self.depthView show];
    [KSystem statusBarSetUpWhiteColor];
    // 3. 刷新币对数据
    [self showAndReloadSymbolData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navView.hidden = YES;
    [KSystem statusBarSetUpDefault];
    [self dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(klineViewImageShare) name:UIApplicationUserDidTakeScreenshotNotification object:nil];

    KDetail.isReloadKlineUI = YES;

    // 1. 加载分享配置信息

    // 2. 初始化UI
    [self setupUI];
//    // 3. 刷新币对数据
//    [self showAndReloadSymbolData];

    
    [IWNotificationCenter addObserver:self selector:@selector(reloadSymbol:) name:KnotificationSelected object:nil];
}

- (void)reloadSymbol:(NSNotification *)noti {
    [self hideMenuView];
    [self showAndReloadSymbolData];
}

- (void)hideMenuView {
    [self.menu hidenWithAnimation];
}


#pragma mark - 1. 初始化UI
- (void)setupUI {

    self.view.backgroundColor = [UIColor backgroundColor];
    self.navView.height = kStatusBarHeight + 44;
    
    self.leftButton.top = kStatusBarHeight;
    self.leftButton.height = 44;
    self.leftButton.width = 37;
    self.leftButton.left = 8;
    
    self.titleLabel.top = kStatusBarHeight;
    self.titleLabel.height = 44;
    self.rightButton.top = kStatusBarHeight;
    self.rightButton.height = 44;
    self.navView.backgroundColor = [UIColor backgroundColor];
    self.titleLabel.textColor = [UIColor mainTextColor];
    [self.leftButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    
    self.rightButton.left = kScreen_Width - 50;
    self.rightButton.width = 40;
    [self.rightButton setImage:[UIImage imageNamed:@"shareKline_icon"] forState:UIControlStateNormal];
    [self.rightButton setImage:[UIImage imageNamed:@"shareKline_icon"] forState:UIControlStateHighlighted];
    
    // 2. 收藏按钮
    if (KDetail.symbolModel.type == SymbolTypeCoin) {
        [self.navView addSubview:self.saveButton];
        [self.navView addSubview:self.shareButton];
        [self.navView addSubview:self.allScreentButton];
    }

    // 3. 标题赋值
    [self.navView addSubview:self.lineView];
    [self.navView addSubview:self.switchButton];

    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = self.headView;
    if (!KDetail.symbolModel.isDelivered) {
        [self.view addSubview:self.lowView];
        [self.lowView.contentView addSubview:self.buyButton];
        [self.lowView.contentView addSubview:self.sellButton];
    }
    
    KSelectedCoinView *demo = [[KSelectedCoinView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width * 0.8, [[UIScreen mainScreen] bounds].size.height)];
    demo.customDelegate = self;
    SelectedMenView *menu = [SelectedMenView MenuViewWithDependencyView:self.view MenuView:demo isShowCoverView:YES];
    self.menu = menu;
}


#pragma mark - 2. 刷新币对数据
- (void)showAndReloadSymbolData {
    
    // 1. 币对名称
    NSString *symbolName = @"";
    if (!IsEmpty(KDetail.symbolModel.baseTokenName) && !IsEmpty(KDetail.symbolModel.quoteTokenName)) {
        if (KDetail.symbolModel.type == SymbolTypeCoin) {
            symbolName = [NSString stringWithFormat:@" %@ /%@", KDetail.symbolModel.baseTokenName, KDetail.symbolModel.quoteTokenName];
        }
        NSLog(@"%@",KDetail.symbolModel.symbolId);

        [self.switchButton setTitle:symbolName forState:UIControlStateNormal];
    }
    
    XXSymbolModel *m = KDetail.symbolModel;
    
    if (KDetail.symbolModel.favorite) {
        self.saveButton.selected = YES;
    }

    
    // 3. 简介视图
    if (KDetail.symbolModel.type != SymbolTypeOption) {
        [self.infoView loadDataOfCoin];
    }
    
    // 4. header-show 【24H行情、k线】
    [self.headView show];
    
    // 5. 深度
    [self.depthView show];
    
    // 6. 成交
    [self.tradesView show];
}

#pragma mark - 3. 消失
- (void)dismiss {
    
    // 1. header-show 【24H行情、k线】
    [self.headView dismiss];
    
    // 2. 深度
    [self.depthView dismiss];
    
    // 3. 成交
    [self.tradesView dismiss];
}

#pragma mark - 4. 清理数据
- (void)cleanData {

    // 1. header-show 【24H行情、k线】
    [self.headView cleanData];
    
    // 2. 深度
    [self.depthView cleanData];
    
    // 3. 成交
    [self.tradesView cleanData];
    
    // 4. 简介视图
    if (KDetail.symbolModel.type != SymbolTypeOption) {
        [self.infoView cleanData];
    }
}

#pragma mark - 5. 切换按钮点击事件
- (void)switchButtonClick:(UIButton *)sender {
    [self.demo firstAppear];
    [self.menu show];

//    // 取消所有订阅
//    SelectedViewController *se = [[SelectedViewController alloc] init];
//    se.menuViewStyle = 1;
//    se.selectIndex = 0;
//    se.automaticallyCalculatesItemWidths = YES;
//    se.modalPresentationStyle = UIModalPresentationFullScreen;
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:se];
//    nav.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - 5. 右侧分享按钮点击事件
- (void)rightButtonClick:(UIButton *)sender {
    [self klineViewImageShare];
}


#pragma mark - 6. 收藏按钮点击事件
- (void)saveButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    
//    if (sender.selected) {
//        [KMarket addFavoriteSymbolId:KDetail.symbolModel.symbolId];
//    } else {
//        [KMarket cancelFavoriteSymbolId:KDetail.symbolModel.symbolId];
//    }
    
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    if (info.login) {
        if (sender.selected) {
            NSDictionary *dic = @{@"pair":KDetail.symbolModel.symbolId};
            [SBNetworTool postWithUrl:EastAddOptionalPairs params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                if (SuccessCode == 200) {
                    [self showToastView:kLocalizedString(@"AddFavoritesSucceeded")];
                    [KMarket addFavoriteSymbolId:KDetail.symbolModel.symbolId];
                    [NotificationManager postAddFaviroutrNotification];
                } else {
                    sender.selected = !sender.selected;
                    [self showToastView:SuccessMessage];
                }
                
            } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
                [self showToastView:FailurMessage];
                sender.selected = NO;
            }];
        } else {
            NSString *url = [NSString stringWithFormat:@"%@%@",EastDeleOptionalPairs,KDetail.symbolModel.symbolId];
            [SBNetworTool deleteWithUrl:url params:@{} success:^(id  _Nonnull responseObject) {
                NSLog(@"%@",responseObject);
                if (SuccessCode == 200) {
                    [NotificationManager postAddFaviroutrNotification];
                    [self showToastView:kLocalizedString(@"CancelFavoritesSucceeded")];
                    [KMarket cancelFavoriteSymbolId:KDetail.symbolModel.symbolId];
                } else {
                    [self showToastView:SuccessMessage];
                }
            } failure:^(id  _Nonnull error) {
                [self showToastView:FailurMessage];
            }];
        }
    } else {
        sender.enabled = YES;
        if (sender.selected) {
            [self showToastView:kLocalizedString(@"AddFavoritesSucceeded")];
            [KMarket addFavoriteSymbolId:KDetail.symbolModel.symbolId];
            [NotificationManager postAddFaviroutrNotification];

        } else {
            [self showToastView:kLocalizedString(@"CancelFavoritesSucceeded")];
            [KMarket cancelFavoriteSymbolId:KDetail.symbolModel.symbolId];
            [NotificationManager postAddFaviroutrNotification];
        }
    }
}


#pragma mark - 7. 表示图代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.sectionHeaderView.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0.001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.sectionHeaderView.index == 0) {
        return self.depthView.height;
    } else if (self.sectionHeaderView.index == 1) {
       return self.tradesView.height;
    } else {
        return self.infoView.height;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.sectionHeaderView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (self.sectionHeaderView.index == 0) {
        return self.depthView;
    } else if (self.sectionHeaderView.index == 1) {
        return self.tradesView;
    } else {
        return self.infoView;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailSymbol"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"detailSymbol"];
    }
    return cell;
}

#pragma mark - 8. 滑动代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offY = scrollView.contentOffset.y;
    if (offY > kNavShadowHeight) {
        self.navView.layer.shadowOpacity = 1;
    } else {
        self.navView.layer.shadowOpacity = offY/kNavShadowHeight;

    }
}

#pragma mark - 9. 截图分享
- (void)klineViewImageShare {
    [self.view addSubview:self.shareLowView];
    UIImage *image_ = [self imageWithScreenshot];
    ShareView *shareviews = [[ShareView alloc] initWithFrame:[UIScreen mainScreen].bounds image:image_];
    shareviews.headActionBlock = ^{
        [self.shareLowView removeFromSuperview];
    };
    [[[UIApplication sharedApplication] keyWindow] addSubview:shareviews];
//    [self.view addSubview:shareviews];
    if (self.headView.isScreen) {
        return;
    }
}

- (UIImage *)imageWithScreenshot {
   NSData *imageData = [NSDate dataWithScreenshotInPNGFormat];
   return [UIImage imageWithData:imageData];
}

#pragma mark - 10. 后台进入前台
- (void)didBecomeActive {
    
    XXNavigationController *navigationVC = self.tabBarController.selectedViewController;
    if ([navigationVC isKindOfClass:[XXNavigationController class]] && navigationVC.viewControllers.lastObject == self) {
        [self showAndReloadSymbolData];
    }
}

#pragma mark - 11. 前台台进入后
- (void)didEnterBackground {
    
    XXNavigationController *navigationVC = self.tabBarController.selectedViewController;
    if ([navigationVC isKindOfClass:[XXNavigationController class]] && navigationVC.viewControllers.lastObject == self) {
        [self dismiss];
    }
}

#pragma mark - || 懒加载
- (UIButton *)saveButton {
    if (_saveButton == nil) {
        _saveButton = [[UIButton alloc] initWithFrame:CGRectMake(DScreenW - 3 *self.rightButton.width, self.rightButton.top, self.rightButton.width, self.rightButton.height)];
        _saveButton.selected = KDetail.symbolModel.favorite;
        [_saveButton setImage:[UIImage imageNamed:@"icon_attention"] forState:UIControlStateNormal];
        [_saveButton setImage:[UIImage imageNamed:@"icon_collect"] forState:UIControlStateSelected];
        [_saveButton addTarget:self action:@selector(saveButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveButton;
}

- (void)screentAction {
    [self.headView fullScreenAction];
}

- (UIButton *)allScreentButton {
    if (_allScreentButton == nil) {
        _allScreentButton = [[UIButton alloc] initWithFrame:CGRectMake(DScreenW - 2*self.rightButton.width, self.rightButton.top, self.rightButton.width, self.rightButton.height)];
        _allScreentButton.selected = KDetail.symbolModel.favorite;
        [_allScreentButton setImage:[UIImage imageNamed:@"fullScreen_0"] forState:UIControlStateNormal];
        [_allScreentButton addTarget:self action:@selector(screentAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _allScreentButton;
}

- (UIButton *)shareButton {
    if (_shareButton == nil) {
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(DScreenW - self.rightButton.width, self.rightButton.top, self.rightButton.width, self.rightButton.height)];
        _shareButton.selected = KDetail.symbolModel.favorite;
        [_shareButton setImage:[UIImage imageNamed:@"icon_share"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

/** 竖线 */
- (UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(52, kStatusBarHeight + 13, 1, 18)];
        _lineView.backgroundColor = [UIColor mainTextColor];
    }
    return _lineView;
}


/** 切换按钮 */
- (FSCustomButton *)switchButton {
    if (_switchButton == nil) {
        _switchButton = [[FSCustomButton alloc] initWithFrame:CGRectMake(64, kStatusBarHeight, kScreen_Width - 150 - 64, 44)];
        _switchButton.buttonImagePosition = FSCustomButtonImagePositionLeft;
        _switchButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _switchButton.titleLabel.font = kFontBold18;
        _switchButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_switchButton setTitleColor:[UIColor mainTextColor] forState:UIControlStateNormal];
        _switchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_switchButton setImage:[[UIImage imageNamed:@"left_more"] imageWithColor:[UIColor mainTextColor]]  forState:UIControlStateNormal];
        [_switchButton addTarget:self action:@selector(switchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _switchButton;
}

/** 看涨看跌标签 */
- (XXLabel *)markLabel {
    if (_markLabel == nil) {
        _markLabel = [XXLabel labelWithFrame:CGRectMake(K375(64), 0 + (self.switchButton.height - 16)/2.0, K375(30), 16) text:@"" font:kFontBold10 textColor:kWhite100 alignment:NSTextAlignmentCenter];
        _markLabel.layer.borderWidth = 1.5;
        _markLabel.layer.cornerRadius = 3;
        _markLabel.layer.masksToBounds = YES;
    }
    return _markLabel;
}

- (XXBDetailHeaderView *)headView {
    if (_headView == nil) {
        _headView = [[XXBDetailHeaderView alloc] initWithFrame:CGRectMake(0, kNavBarAndStatusBarHeight, kScreen_Width, 200)];
    }
    return _headView;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kStatusBarHeight + 44, kScreen_Width, kScreen_Height - (kStatusBarHeight + 44)) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor backgroundColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, BH_IS_IPHONE_X ? 83 : 65)];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (XXBDetailSectionHeaderView *)sectionHeaderView {
    if (_sectionHeaderView == nil) {
        _sectionHeaderView = [[XXBDetailSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 42)];
        [_sectionHeaderView setupUI];
        KWeakSelf
        // index: 0. 委托订单 1. 最新成交
        _sectionHeaderView.headActionBlock = ^(NSInteger index) {
            [weakSelf.tableView reloadData];
        };
    }
    return _sectionHeaderView;
}

- (XXKlineShareView *)shareLowView {
    if (_shareLowView == nil) {
        _shareLowView = [[XXKlineShareView alloc] initWithFrame:CGRectMake(0, self.view.height - 91, kScreen_Width, 91)];
        _shareLowView.backgroundColor = WhiteColor;
    }
    return _shareLowView;
}

- (UIVisualEffectView *)lowView {
    if (_lowView == nil) {

        NSInteger mainViewHeight = BH_IS_IPHONE_X ? 83 : 65;
        UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        _lowView = [[UIVisualEffectView alloc] initWithEffect:effect];
        _lowView.frame = CGRectMake(0, kScreen_Height - mainViewHeight, kScreen_Width, mainViewHeight);

        UIView *upLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 1)];
        upLine.backgroundColor = [UIColor assistBackgroundColor];
        [_lowView.contentView addSubview:upLine];
    }
    return _lowView;
}
/** 买入按钮 */
- (XXButton *)buyButton {
    if (_buyButton == nil) {
        KWeakSelf
        NSString *btnTitle = kLocalizedString(@"BUY");
        _buyButton = [XXButton buttonWithFrame:CGRectMake(K375(10), 10, (kScreen_Width - K375(30))/2, 45) title:btnTitle font:kFontBold18 titleColor:kMainTextColor block:^(UIButton *button) {
            if (KDetail.symbolModel.type == SymbolTypeCoin) {
                NSLog(@"%@",KDetail.symbolModel.symbolId);
                KTrade.coinTradeModel = KDetail.symbolModel;
                KTrade.coinIsSell = NO;
                KTrade.indexTrade = 0;
                [weakSelf.navigationController popToRootViewControllerAnimated:NO];
                UITabBarController *tabBarVC = (UITabBarController *)KWindow.rootViewController;
                tabBarVC.selectedIndex = 1;
            }
            [KSystem statusBarSetUpDefault];
        }];
        _buyButton.backgroundColor = kGreen100;
        _buyButton.layer.cornerRadius = 3;
        _buyButton.layer.masksToBounds = YES;
    }
    return _buyButton;
}

/** 卖出按钮 */
- (XXButton *)sellButton {
    if (_sellButton == nil) {
        KWeakSelf
        NSString *btnTitle = kLocalizedString(@"SELL");
        _sellButton = [XXButton buttonWithFrame:CGRectMake(CGRectGetMaxX(self.buyButton.frame) + K375(10), self.buyButton.top, self.buyButton.width, self.buyButton.height) title:btnTitle font:kFontBold18 titleColor:kMainTextColor block:^(UIButton *button) {
            if (KDetail.symbolModel.type == SymbolTypeCoin) {
                NSLog(@"%@",KDetail.symbolModel.symbolId);
                KTrade.coinTradeModel = KDetail.symbolModel;
                KTrade.coinIsSell = YES;
                KTrade.indexTrade = 0;
                [weakSelf.navigationController popToRootViewControllerAnimated:NO];
//                XXTabBarController *tabBarVC = (XXTabBarController *)KWindow.rootViewController;
//                [tabBarVC setIndex:2];
                UITabBarController *tabBarVC = (UITabBarController *)KWindow.rootViewController;
                tabBarVC.selectedIndex = 1;
            }
            [KSystem statusBarSetUpDefault];
        }];
        _sellButton.backgroundColor = kRed100;
        _sellButton.layer.cornerRadius = 3;
        _sellButton.layer.masksToBounds = YES;
    }
    return _sellButton;
}

/** 深度图 */
- (XXDetailDepthListView *)depthView {
    if (_depthView == nil) {
        _depthView = [[XXDetailDepthListView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 911)];
    }
    return _depthView;
}

/** 交易图 */
- (XXDetailTradesListView *)tradesView {
    if (_tradesView == nil) {
        _tradesView = [[XXDetailTradesListView alloc] initWithFrame:CGRectMake(kScreen_Width, 0, kScreen_Width, 680)];
    }
    return _tradesView;
}

/** 简介视图 */
- (XXCoinInfoView *)infoView {
    if (_infoView == nil) {
        _infoView = [[XXCoinInfoView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 250)];
        KWeakSelf
        _infoView.reloadMainUI = ^{
            [weakSelf.tableView reloadData];
        };
    }
    return _infoView;
}

- (void)dealloc {
    NSLog(@"==+==币对详情释放了");
    [self dismiss];
    [IWNotificationCenter removeObserver:self name:KnotificationSelected object:nil];
}
@end
