//
//  AssertCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/2/22.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BalanceCoinsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AssertCell : UITableViewCell

/** img **/
@property (nonatomic ,strong) UIImageView *img;
/** name **/
@property (nonatomic ,strong) BaseLabel *coinName;
/** leftName **/
@property (nonatomic ,strong) BaseLabel *leftTitle;
/** centerName **/
@property (nonatomic ,strong) BaseLabel *centerTitle;
/** rightName **/
@property (nonatomic ,strong) BaseLabel *rightTitle;
/** leftLb **/
@property (nonatomic ,strong) BaseLabel *leftLb;
/** centerLb **/
@property (nonatomic ,strong) BaseLabel *centerLb;
/** rightLb **/
@property (nonatomic ,strong) BaseLabel *rightLb;

@property (nonatomic ,strong) BalanceCoinsModel *model;


+ (instancetype)cellWithTbaleView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
