//
//  AddressVC.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/7.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseViewController.h"
#import "WithCoinListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressVC : BaseViewController

@property (nonatomic ,strong) WithCoinListModel *model;

@end

NS_ASSUME_NONNULL_END
