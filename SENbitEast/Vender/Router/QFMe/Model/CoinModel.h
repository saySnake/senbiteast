//
//  CoinModel.h
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoinModel : NSObject
/** ID  **/
@property (nonatomic ,copy) NSString *ID;
/** 币的名称 **/
@property (nonatomic ,copy) NSString *coinName;
/** addressURL **/
@property (nonatomic ,copy) NSString *addressURL;
/** txURL **/
@property (nonatomic ,copy) NSString *txURL;
/** enName **/
@property (nonatomic ,copy) NSString *enName;
/** enFullName **/
@property (nonatomic ,copy) NSString *enFullName;
/** coinIcon **/
@property (nonatomic ,copy) NSString *coinIcon;
/** isDestinationTagRequired **/
@property (nonatomic ,assign) BOOL isDestinationTagRequired;
@property (nonatomic ,assign) BOOL able2withdraw;
@property (nonatomic ,assign) BOOL able2charge;
/** chargeConfirmLimit **/
@property (nonatomic ,copy) NSString *chargeConfirmLimit;
@property (nonatomic ,strong) NSString *nwDailyMaxAmount;
@property (nonatomic ,strong) NSString *nwFeeRatio;
@property (nonatomic ,strong) NSString *nwMaxAmount;
@property (nonatomic ,strong) NSString *nwMinAmount;
@property (nonatomic ,strong) NSString *nwMinFee;
- (instancetype)initWithDic:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
