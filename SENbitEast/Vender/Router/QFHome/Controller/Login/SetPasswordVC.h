//
//  SetPasswordVC.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/17.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetPasswordVC : BaseViewController

@property (nonatomic ,strong) NSString *account;

@property (nonatomic ,strong) NSString *authToken;

@property (nonatomic ,assign) BOOL isPhoneRegister;

@property (nonatomic ,strong) NSString *countryCode;

@property (nonatomic ,strong) NSString *locale;

@end

NS_ASSUME_NONNULL_END
