//
//  ComisCurrentView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ComisCurrentView : UIView
- (void)show;
- (void)dismiss;
- (void)cleanData;

@end

NS_ASSUME_NONNULL_END
