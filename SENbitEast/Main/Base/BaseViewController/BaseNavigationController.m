//
//  BaseNavigationController.m
//  WeChat
//
//  Created by zhengwenming on 16/6/5.
//  Copyright © 2016年 zhengwenming. All rights reserved.
//

#import "BaseNavigationController.h"
#define IOS7 ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController
+ (void)initialize {
    
    [self setNavBarTheme];
    
    [self setNavBarButtenItem];
}


+ (void)setNavBarTheme {
    //设置导航栏
    UINavigationBar *navBar = [UINavigationBar appearance];
    //设置背景
    if (!IOS7) {
        [navBar setBackgroundImage:[UIImage imageNamed:@"dialog bg@3x"] forBarMetrics:UIBarMetricsDefault];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDarkContent;
    }
    //设置标题属性
    NSMutableDictionary *texAttributes = [NSMutableDictionary dictionary];
    texAttributes[NSForegroundColorAttributeName] = [UIColor blackColor];
    texAttributes[UITextAttributeTextShadowOffset] = [NSValue valueWithUIOffset:UIOffsetZero];
    texAttributes[NSFontAttributeName] = [UIFont boldSystemFontOfSize:19];
    [navBar setTitleTextAttributes:texAttributes];
}


+ (void)setNavBarButtenItem {
    //设置导航栏的主题
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    
    if (!IOS7) {
        //设置背景
        [item setBackgroundImage:[UIImage imageNamed:@"dialog bg"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        //高亮
        [item setBackgroundImage:[UIImage imageNamed:@"dialog bg"] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
        //disAble
        [item setBackgroundImage:[UIImage imageNamed:@"dialog bg"] forState:UIControlStateDisabled barMetrics:UIBarMetricsDefault];
    }
    
    //设置文字属性
    NSMutableDictionary *textDic = [NSMutableDictionary dictionary];
    NSMutableDictionary *texAttributes = [NSMutableDictionary dictionary];
    texAttributes[NSForegroundColorAttributeName] = [UIColor grayColor];
    texAttributes[NSShadowAttributeName] = [NSValue valueWithUIOffset:UIOffsetZero];
    texAttributes[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    [item setTitleTextAttributes:textDic forState:UIControlStateNormal];
    [item setTitleTextAttributes:textDic forState:UIControlStateHighlighted];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDarkContent;
//    [self setupNav];
//    [self setupNavigationItem];
    
}

//- (void)setupNav
//{
//    nav = [RTNavigationBar defaultBar];
//    CGFloat rgb = 0.1;
//    nav.barTintColor = [UIColor colorWithRed:rgb green:rgb blue:rgb alpha:0.9];
//    nav.tintColor = MainWhiteColor;
//    nav.titleTextAttributes = @{NSForegroundColorAttributeName : MainWhiteColor};
//}
//
//- (void)setupNavigationItem
//{
//    navigationItem = [[UINavigationItem alloc] initWithTitle:@""];
//    UIButton* left = [UIButton buttonWithType:UIButtonTypeCustom];
//    [left setFrame:CGRectMake(0, 0, 40, 40)];
//    [left setImage:[[UIImage imageNamed:@"common_btn_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
//    [left setImageEdgeInsets:UIEdgeInsetsMake(0, /*0*/-23, 0, 0)];
//    [left addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* leftBtn = [[UIBarButtonItem alloc] initWithCustomView:left];
//    [navigationItem setLeftBarButtonItem:leftBtn animated:NO];
//    [nav pushNavigationItem:navigationItem animated:NO];
//}
//
//- (void)onBack{
//    NSLog(@"touch onBack");
//    if (self.navigationController.viewControllers.count > 1) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}
//
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

@end
