//
//  AssertListModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/8.
//  Copyright © 2021 张玮. All rights reserved.
//  充币历史记录model

#import <Foundation/Foundation.h>
#import "DetailCoinModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AssertListModel : NSObject

@property (nonatomic ,strong) NSString *amount;
@property (nonatomic ,strong) NSString *confirmationCount;
@property (nonatomic ,strong) NSString *createdAt;
@property (nonatomic ,strong) NSString *status;
@property (nonatomic ,strong) NSString *toAddress;
@property (nonatomic ,strong) NSString *txid;
@property (nonatomic ,strong) NSString *ID;
@property (nonatomic ,strong) NSString *coinType;
@property (nonatomic ,strong) NSString *type;
@property (nonatomic ,strong) NSString *withdrawStatus;
@property (nonatomic ,strong) NSString *withdrawCode;
@property (nonatomic ,strong) DetailCoinModel *coin;
@end

NS_ASSUME_NONNULL_END
