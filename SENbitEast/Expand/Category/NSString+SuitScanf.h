//
//  NSString+SuitScanf.h
//  Senbit
//
//  Created by 张玮 on 2020/3/11.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (SuitScanf)

+ (NSString *)crabNumberSuitScanf:(NSString *)number;

+ (NSString *)numberSuitScanf:(NSString*)number;
/*
 *  判断用户输入的密码是否符合规范，符合规范的密码要求：
    1. 长度大于8位
    2. 密码中必须同时包含数字和字母
 */
+ (BOOL)judgePassWordLegal:(NSString *)pass;

//正则判断是不是邮箱
+ (BOOL)checkEmail:(NSString *)email;
//判断是不是汉子 是汉字返回yes
+ (BOOL)isChinese:(NSString *)str;
//判断是不是身份证 是身份证返回yes
+ (BOOL)isCorrect:(NSString *)IDNumber;
//想加俩个数
+ (NSString *)addV:(NSString *)v1 v2:(NSString *)v2;
// 字符串加
+ (NSString *)addV1:(NSString *)v1 v2:(NSString *)v2 v3:(NSString *)v3;
// 字符串减
+ (NSString *)subV1:(NSString *)v1 v2:(NSString *)v2;
// 字符串乘
+ (NSString *)mulV1:(NSString *)v1 v2:(NSString *)v2;
// 字符串除
+ (NSString *)divV1:(NSString *)v1 v2:(NSString *)v2;
//截取小数点后几位  四舍五入
+ (NSString *)getRoundFloat:(double)floatNumber withPrecisionNum:(NSInteger)precision;
//IOS字符串截取保留小数点后两位（如果是0.03）显示（.03)
+ (NSString*)getTheCorrectNum:(NSString*)tempString;
//IOS保留两位小数点 不四舍五入
+(NSString*)returnFormatter:(NSString*)stringNumber;
//截取小数点后几位 不四舍五入
+ (NSString*)notRounding:(float)price afterPoint:(int)position;
//将时间戳转为时间
+ (NSString *)getTimeFromTimesTamp:(NSString *)timeStr;
//时间戳转为时间 2019-01-23 12:32:32
+ (NSString *)getYearMonthDayTimesTamp:(NSString *)timeStr;
//2020-03-23  2019-03-12相差多少天分秒
+ (NSString *)dateTimeDifferenceWithendTime:(NSString *)endTime;
+ (NSString *)timeFormatted:(long)totalSeconds;
//法币交易计算倒计时时间
+ (NSString *)startTimetoNowTime:(NSString *)str;
//科学计数法 8.9e-7 处理为 0.000001
+ (NSString *)formartScientificNotationWithString:(NSString *)str;

@end

NS_ASSUME_NONNULL_END
