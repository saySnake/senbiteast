//
//  SegmentView.m
//  Senbit
//
//  Created by 张玮 on 2020/1/13.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SegmentView.h"

@interface SegmentView()

/** 按钮数组 */
@property (nonatomic, strong) NSMutableArray *btns;

/** 分割线 */
@property (nonatomic, weak) UIView *dividView;

@end

@implementation SegmentView

- (NSMutableArray *)btns {
    if(!_btns){
        self.btns = [NSMutableArray array];
    }
    return _btns;
}

- (UIView *)dividView {
    if (!_dividView) {
        UIView *divideView = [[UIView alloc] init];
        divideView.backgroundColor = EColorWithAlpha(0x00a19b, 1);
        [self addSubview:divideView];
        self.dividView = divideView;
    }
    return _dividView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setSegments:(NSArray *)segments {
    _segments = segments;
    [self.btns removeAllObjects];
    for (SegmentModel *model in segments) {
        [self createBtn:model];
    }
}

/**
 *  添加按钮到数组中
 *
 *  @param title 按钮的标题
 */
- (void)createBtn:(SegmentModel *)model {
    SegmentBtn *btn = [[SegmentBtn alloc] init];
    [btn setTitleColor:model.titleColor forState:UIControlStateNormal];
    btn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:btn];
    [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btns addObject:btn];
    btn.model = model;
}

- (void)setFront:(CGFloat )font title:(NSString *)title {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    [self addSubview:btn];
    [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btns addObject:btn];
}

- (void)setLineColor:(UIColor *)lineColor {
    self.dividView.backgroundColor = lineColor;
}



- (void)setSelectModel:(SegmentModel *)selectModel {
    _selectModel = selectModel;
    //1.先去的对应的索引
    NSInteger index = [_segments indexOfObject:selectModel];
    //2.根据索引取出来对应的按钮
    SegmentBtn *btn = self.btns[index];
    //3.给按钮附上对应的模型类
    btn.model = selectModel;
}

- (void)setSegmentIndex:(NSInteger)segmentIndex {
    _segmentIndex = segmentIndex;
    SegmentBtn *btn = self.btns[_segmentIndex];
    btn.titlelb.textColor = PlaceHolderColor;// ThemeGreenColor;
    [UIView animateWithDuration:0.25 animations:^{
        self.dividView.x = segmentIndex * self.dividView.width;
    }];
}

/**
 *  按钮的点击事件
 *
 */
- (void)btnClicked:(UIButton *)btn {
    [UIView animateWithDuration:0.25 animations:^{
        self.dividView.x = btn.tag * self.dividView.width;
    }];
    if (self.segmentClicked) {
        self.segmentClicked(btn.tag);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSInteger count = self.btns.count;
    CGFloat btnW = self.width / count;
    CGFloat btnH = self.height - 3;
    CGFloat btnY = self.height - 40;
    
    for (NSInteger index = 0;index < count; index++) {
        CGFloat btnX = index *btnW;
        UIButton *btn = self.btns[index];
        btn.tag = index;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
    }
    self.dividView.frame = CGRectMake(0, self.height - 2, btnW, 2);
}


@end
