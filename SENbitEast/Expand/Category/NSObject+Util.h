//
//  NSObject+Util.h
//  Senbit
//
//  Created by 张玮 on 2019/12/20.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Util)
- (CGSize)sizeWithText:(NSString *)text;
- (CGSize)sizeWithText:(NSString *)text withFont:(CGFloat)font;

- (CGSize)sizeWithWidth:(NSString *)text float:(CGFloat )flo;

- (CGSize)sizeWithWidth:(NSString *)text float:(CGFloat )flo heght:(CGFloat)height;
@property (nonatomic,strong,readonly)NSArray *properties;

@property (nonatomic,strong,readonly)NSArray *values;


-(instancetype)initWithDict:(NSDictionary *)dict;

-(void)delay:(NSTimeInterval)timer task:(dispatch_block_t) task;
-(void)cancle:(dispatch_block_t)task;

- (void)fastEncode:(NSCoder *)aCoder;
- (void)fastDecode:(NSCoder *)aDecoder;

@end

NS_ASSUME_NONNULL_END
