//
//  HeadImage.m
//  Senbit
//
//  Created by 张玮 on 2020/6/18.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "HeadImage.h"
#import "HeadImgaeView.h"


@implementation HeadImage

+ (UIImage*)imageWithText1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 text4:(NSString *)text4 text5:(NSString *)text5 text6:(NSString *)text6 text7:(NSString *)text7 text8:(NSString *)text8 text9:(NSString *)text9 isMore:(BOOL)isMore isCrease:(BOOL)isCrease {
    //初始化并绘制UI
    HeadImgaeView *view = [[HeadImgaeView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, DScreenH) text1:text1 text2:text2 text3:text3 text4:text4 text5:text5 text6:text6 text7:text7 text8:text8 text9:text9 isMore:isMore isCrease:isCrease];
    //转化成image
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tImage;
}

//isCrease yes 收益率是正的
+ (UIImage*)pingContractText1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 text4:(NSString *)text4 text5:(NSString *)text5 text6:(NSString *)text6 text7:(NSString *)text7 isMore:(BOOL)isMore isCrease:(BOOL)isCrease {
    HeadImgaeView *view = [[HeadImgaeView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, DScreenH) text1:text1 text2:text2 text3:text3 text4:text4 text5:text5 text6:text6 text7:text7 isMore:isMore isCrease:isCrease];
    //转化成image
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tImage;
}

+ (UIImage*)kuiSunContractText1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 text4:(NSString *)text4 text5:(NSString *)text5 text6:(NSString *)text6 text7:(NSString *)text7 {
    HeadImgaeView *view = [[HeadImgaeView alloc] initWithFrame:CGRectMake(0, 0, DScreenW, DScreenH) text1:text1 text2:text2 text3:text3 text4:text4 text5:text5 text6:text6 text7:text7];
    //转化成image
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tImage;
}

+ (UIImage *)shareCrabPaper {
    HeadImgaeView *view = [[HeadImgaeView alloc] initWitCrabFrame:CGRectMake(0, 0, DScreenW, DScreenH)];
    //转化成image
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tImage;
}



+ (UIImage *)newPager:(UIImage *)image {
    //1.开启上下文
    UIGraphicsBeginImageContextWithOptions(image.size, NO, [UIScreen mainScreen].scale);
    //2.绘制图片
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    //添加水印文字
//    [text drawAtPoint:point withAttributes:attributed];
    //3.从上下文中获取新图片
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    //4.关闭图形上下文
    UIGraphicsEndImageContext();
    //保存图片至相册
//    UIImageWriteToSavedPhotosAlbum(newImage, nil, nil, nil);
    //返回图片
    return newImage;

}
@end
