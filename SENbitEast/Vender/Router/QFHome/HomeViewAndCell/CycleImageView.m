//
//  CycleImageView.m
//  CycleIMG
//
//  Created by 张玮 on 2020/1/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#define kPageControl_H      3

#define kCellIdentifier     @"CycleImageViewCell"

#import "CycleImageView.h"
#import "CycleImageViewCell.h"
#import "CycleImageViewPageControl.h"
#import "OneTableSockeModel.h"
#import "QFRouter.h"
#import "QFMoudleProtocol.h"



@interface CycleImageView ()<UICollectionViewDelegate,UICollectionViewDataSource> {
    NSInteger _index;
    BOOL _isScrol;
    UICollectionViewScrollPosition _scrollPosition;
    UICollectionViewScrollDirection _scrollDirection;
}

@property(nonatomic, strong) UICollectionView *collectionView;
@property(strong, nonatomic) NSMutableArray *dataArr;
@property(nonatomic ,strong) CycleImageViewPageControl *pageControl;
@property(nonatomic ,assign) CGFloat startContentOffsetX;
@property(nonatomic ,assign) CGFloat willEndContentOffsetX;
@property(nonatomic ,strong) XXWebQuoteModel *webModel;

@end

@implementation CycleImageView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.movementDirection = MovementDirectionForHorizontally;
        self.collectionView.backgroundColor = WhiteColor;
        self.collectionView.userInteractionEnabled = YES;
        [self socketData];
        [self.collectionView reloadData];
        self.pageControl.numberOfPages = 0;
    }
    return self;
}


- (void)socketData {
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        NSMutableArray *array = [OneTableSockeModel mj_objectArrayWithKeyValuesArray:data];
        [weakSelf.dataArr removeAllObjects];
        for (OneTableSockeModel *model in array) {
            if ([model.recommend isEqualToString:@"1"]) {
                [weakSelf.dataArr addObject:model];
            }
        }
        if (weakSelf.dataArr.count > 3) {
            weakSelf.pageControl.numberOfPages = 2;
        }
        [weakSelf.collectionView reloadData];
    };
    NSDictionary *dic = @{};
    self.webModel.params = [NSMutableDictionary dictionary];
    self.webModel.params[@"topic"] = @"home-overview";
    self.webModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"home-overview"];
}


#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}



////路由跳转
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    OneTableSockeModel *model = self.dataArr[indexPath.row];
//
//    [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//       if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
//           KDetail.symbolModel = obj;
//           KTrade.coinTradeModel = obj;
//           if ([KMarket.favoritesArray containsObject:model.coinName]) {
//               KDetail.symbolModel.favorite = YES;
//               KTrade.coinTradeModel.favorite = YES;
//           }
//           NSLog(@"%@",KDetail.symbolModel.symbolId);
//           *stop = YES;
//         }
//   }];
//
//    id <MoudleLine>lineMoudle = [[QFRouter router]interfaceForProtocol:@protocol(MoudleLine)];
//    UIViewController *viewController = lineMoudle.interfaceViewController;
//    lineMoudle.callbackBlock = ^(id parameter) {
//        NSLog(@"return paramter = %@",parameter);
//    };
//    [self.navigationController pushViewController:viewController animated:YES];
//}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CycleImageViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.model = self.dataArr[indexPath.row];
    cell.backgroundColor = ThemeDarkBlack;
//    NSInteger idx = indexPath.item % 3;
//    if (idx == 0) {
//        cell.backgroundColor = [UIColor purpleColor];
//    } else if (idx == 1) {
//        cell.backgroundColor = [UIColor yellowColor];
//    } else if (idx == 2) {
//        cell.backgroundColor = [UIColor lightGrayColor];
//    }
    return cell;
}

//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//    if (_isScrol) {
//        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:_scrollPosition animated:NO];
//        _isScrol = NO;
//    }
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{ //拖动前的起始坐标
    self.startContentOffsetX = scrollView.contentOffset.x;
}

//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{ //将要停止前的坐标
//    self.willEndContentOffsetX = scrollView.contentOffset.x;
//}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    CGFloat  endContentOffsetX = scrollView.contentOffset.x;
//    if (endContentOffsetX < self.willEndContentOffsetX && self.willEndContentOffsetX < self.startContentOffsetX) {
//        //画面从右往左移动，前一页
//        self.pageControl.currentPage = 0;
//    } else if (endContentOffsetX > self.willEndContentOffsetX && self.willEndContentOffsetX > self.startContentOffsetX) {
//        //画面从左往右移动，后一页
//        self.pageControl.currentPage = 1;
//    }
//}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSLog(@"%f",scrollView.contentOffset.x);
    if (scrollView.contentOffset.x > 30) {
        self.pageControl.currentPage = 1;
    } else {
        self.pageControl.currentPage = 0;
    }
}
//
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
////    currentPage = self.collectionView.contentSize.width / self.collectionView.width;
//
//    NSInteger currentPage = 0;
//    if (!self.movementDirection) {
//
//        currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
//
//    } else
//    {
//        currentPage = scrollView.contentOffset.y / scrollView.bounds.size.height;
//    }
//
//    currentPage = currentPage % 2;
//
//    self.pageControl.currentPage = currentPage;
//    _index = currentPage;
//
//    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:currentPage inSection:1] atScrollPosition:_scrollPosition animated:NO];
//}


//设置最小列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.001;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectItemAtIndex:)]) {
        OneTableSockeModel *model = self.dataArr[indexPath.row];
        [self.delegate didSelectItemAtIndex:model];
    }
}

#pragma mark - setter
- (void)setMovementDirection:(MovementDirectionType)movementDirection {
    _movementDirection = movementDirection;
    if (!movementDirection) {
        _scrollPosition = UICollectionViewScrollPositionCenteredHorizontally;
        _scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
    } else {
        _scrollPosition = UICollectionViewScrollPositionCenteredVertically;
        _scrollDirection = UICollectionViewScrollDirectionVertical;
    }
}


- (void)setHidePageControl:(BOOL)hidePageControl {
    _hidePageControl = hidePageControl;
    self.pageControl.hidden = hidePageControl;
}


- (void)setCanFingersSliding:(BOOL)canFingersSliding {
    _canFingersSliding = canFingersSliding;
    self.collectionView.scrollEnabled = canFingersSliding;
}

- (CycleImageViewPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[CycleImageViewPageControl alloc]initWithFrame:CGRectMake(0, self.bounds.size.height - kPageControl_H, self.bounds.size.width, kPageControl_H)];
        _pageControl.currentPage = 0;
        _pageControl.currentPageIndicatorTintColor = MainWhiteColor;
        _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        _pageControl.enabled = NO;
        [self addSubview:_pageControl];
    }
    return _pageControl;
}


- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CGFloat w = DScreenW / 3;
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = _scrollDirection;
        flowLayout.itemSize = CGSizeMake(w, 107);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor yellowColor];
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerNib:[UINib nibWithNibName:kCellIdentifier bundle:nil] forCellWithReuseIdentifier:kCellIdentifier];
        _collectionView.backgroundColor = MainWhiteColor;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}


- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}
@end
