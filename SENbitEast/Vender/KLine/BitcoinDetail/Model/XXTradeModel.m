//
//  XXTradeModel.m
//  iOS
//
//  Created by iOS on 2018/6/14.
//  Copyright © 2018年 iOS. All rights reserved.
//

#import "XXTradeModel.h"
#import "NSString+SuitScanf.h"


@implementation XXTradeModel
- (instancetype)initWithArray:(NSArray *)array {
    self = [super init];
    if (self) {
        self.q = [NSString divV1:array[0] v2:TENZero];
        if (array.count > 1) {
            self.p = [NSString divV1:array[1] v2:TENZero];
            if ([array[2] isEqualToString:@"bid"]) {
                self.m = YES;
            } else {
                self.m = NO;
            }
//            self.t = [array[3] longLongValue];
            self.t = array[3];
        }

//        self.time = [NSString getTimeFromTimesTamp:array[0]];
    }
    return self;

}
@end
