//
//  CoinModel.m
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "CoinModel.h"

@implementation CoinModel

- (instancetype)initWithDic:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        self.ID = dic[@"_id"];
        self.coinName = dic[@"id"];
        self.addressURL = dic[@"addressURL"];
        self.txURL = dic[@"txURL"];
        self.enName = dic[@"enName"];
        self.enFullName = dic[@"enFullName"];
        self.coinIcon = dic[@"coinIcon"];
        self.isDestinationTagRequired = [dic[@"isDestinationTagRequired"] boolValue];
        self.chargeConfirmLimit = dic[@"chargeConfirmLimit"];
        self.able2charge = [dic[@"able2charge"] boolValue];
        self.able2withdraw = [dic[@"able2withdraw"] boolValue];
        self.nwDailyMaxAmount = dic[@"nwDailyMaxAmount"];
        self.nwFeeRatio = dic[@"nwFeeRatio"];
        self.nwMaxAmount = dic[@"nwMaxAmount"];
        self.nwMinAmount = dic[@"nwMinAmount"];
        self.nwMinFee = dic[@"nwMinFee"];
    }
    return self;
}

@end
