//
//  WebViewController.m
//  Senbit
//
//  Created by 张玮 on 2020/4/5.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "WebViewController.h"
#import "BDFCustomPhotoAlbum.h"
#import "AcctConst.h"
#import "SharedView.h"
#import "HeadImage.h"

@interface WebViewController ()<HXWKWebViewDelegate,NNShareViewdDelegate>
@property (nonatomic ,strong) UIButton *deleBtn;
@end

@implementation WebViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.wkWebView.wkWebView reload];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navigationController.navigationBarHidden = YES;
    [self setLeftBtnImage:@"leftBack"];
    [self initWkWebView];
}

- (void)setIsDelete:(BOOL)isDelete {
    _isDelete = isDelete;
    if (self.isDelete) {
        [self.navBar addSubview:self.deleBtn];
        [self.deleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(40);
            make.top.mas_equalTo(KNavBarButtonHeight);
            make.width.mas_equalTo(72);
            make.height.mas_equalTo(22);
        }];
    }
}
- (void)setIsShare:(BOOL)isShare {
    _isShare = isShare;
    if (self.isShare) {
        [self.navBar addSubview:self.deleBtn];
        [self.deleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(40);
            make.top.mas_equalTo(KNavBarButtonHeight);
            make.width.mas_equalTo(72);
            make.height.mas_equalTo(22);
        }];
        [self setRightBtnStyle:NavBarBtnStyleSave];
    }
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)navBarButtonClick:(NSInteger)idx {
    if (idx == 1) {
//        UIImage *image = [HeadImage shareCrabPaper];
        UIImageView *imgv = [[UIImageView alloc] init];
        imgv.image = [HeadImage newPager:[UIImage imageNamed:@"newCrab"]];
        imgv.contentMode = UIViewContentModeScaleAspectFit;
        
//        ShareView *shareviews = [[ShareView alloc] initWithFrame:[UIScreen mainScreen].bounds screenshotImage:image];
//        ShareView *shareviews = [[ShareView alloc] initWithFrame:[UIScreen mainScreen].bounds screenshotImage:imgv.image];
        SharedView *shareviews = [[SharedView alloc] initFrame:[UIScreen mainScreen].bounds newCrabImage:imgv.image ];
        shareviews.shareViewDelegate = self;
      
    } else {
        if (self.wkWebView.wkWebView.canGoBack == YES) {
            [self.wkWebView.wkWebView goBack];
        } else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)shareButtonAction:(UIImageView *)integer index:(NSInteger)index {
    NSLog(@"截屏分享_测试界面_点击了第 %ld 个按钮", index);
    if (index == 0) {
        [[BDFCustomPhotoAlbum shareInstance] saveToNewThumb:integer.image];
    }
}


- (void)initWkWebView {
    self.wkWebView = [[HXWKWebView alloc] initWithFrame:CGRectMake(0, kNavBarAndStatusBarHeight, DScreenW, DScreenH - kNavBarAndStatusBarHeight)];
    self.wkWebView.backgroundColor = [UIColor whiteColor];
    NSString *str = self.url;
    NSString *urlStr = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.wkWebView hx_loadURL:[NSURL URLWithString:urlStr]];
    self.wkWebView.delegate = self;
    [self.view addSubview:self.wkWebView];
}

- (void)clickButton:(UIButton *)sender {
    [self.wkWebView hx_stringByEvaluatingSendMessageToJavaScript:@"setValue" paremeter:@"在这里通过OC给JS赋值" completionHandler:^(id object) {
        NSLog(@"%@",object);
    }];
}

#pragma mark - HXWKWebViewDelegate
/**
 HTML开始加载
 */
- (void) hx_webViewDidStartLoad:(HXWKWebView *)hxWebview {
    NSLog(@"The page starts loading...");
    [self setLeftBtnImage:@"leftBack"];

}

/**
 HTML加载完毕
 */
- (void) hx_webView:(HXWKWebView *)hxWebview didFinishLoadingURL:(NSURL *)URL {
    NSLog(@"The page is loaded!");
    [self setNavBarTitle:self.wkWebView.titleString]; //kLocalizedString(@"search")];
}

/**
 HTML加载失败
 */
- (void) hx_webView:(HXWKWebView *)hxWebview didFailToLoadURL:(NSURL *)URL error:(NSError *)error {
    
    NSLog(@"Loading error!");
    
}


/**
 在url变化或网页内容变化的时候也能调用
 */
- (BOOL) hx_webView:(HXWKWebView *)hxWebview shouldStartLoadWithURL:(NSURL *)URL{
    NSLog(@"Intercept to URL：%@",URL);
    return YES;
}



+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

/**
 WKWebView和JS交互代理
 */
- (void)hx_userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(HXWebModel *)message {
    NSLog(@"JS交互:%@",message.body);
    NSDictionary *dic = message.body;
    NSString * methods = [NSString stringWithFormat:@"%@",message.name];
    
    NSDictionary *dics = [WebViewController dictionaryWithJsonString:dic[@"data"]];
    NSLog(@"%@",dics);
    
    if ([methods isEqualToString:BuyPangXieScript]) {
        NSLog(@"%@",dic);
        NSString *ns = dic[@"cmd"];
        if ([ns isEqualToString:@"openLogin"]) {
            if ([self.delegate respondsToSelector:@selector(webViewControllerL:loginAction:)]) {
                [self.delegate webViewControllerL:self loginAction:@""];
            }
        } else if ([ns isEqualToString:@"getToken"]) {
            if ([self.delegate respondsToSelector:@selector(webViewControllerL:token:name:)]) {
                [self.delegate webViewControllerL:self token:@"" name:dics[@"cbName"]];
            }
        } else if ([ns isEqualToString:@"openAuth"]) {
            if ([self.delegate respondsToSelector:@selector(webViewControllerL:auth:)]) {
                [self.delegate webViewControllerL:self auth:@""];
            }
        } else if ([ns isEqualToString:@"openAssetsPw"]) {
            if ([self.delegate respondsToSelector:@selector(webViewControllerL:setPw:)]) {
                [self.delegate webViewControllerL:self setPw:@""];
            }
        } else if ([ns isEqualToString:@"getAppInfo"]) {
            if ([self.delegate respondsToSelector:@selector(webViewControllerL:version:name:)]) {
                [self.delegate webViewControllerL:self version:@"" name:dics[@"cbName"]];
            }
        }
    }
    
/***
     ///点击网页上的按钮 获取文本款的值
     NSString *textString = @"document.getElementById('nameID').value";
     
     [self.wkWebView hx_stringByEvaluatingJavaScriptFromString:textString completionHandler:^(id object) {
 //        self.htmlStr = object;
         NSLog(@"获取html文本框上的值:%@",object);
     }];
     
     ///JS调用OC方法
     //body只支持NSNumber, NSString, NSDate, NSArray,NSDictionary 和 NSNull类型
     if ([message.body isKindOfClass:[NSString class]]) {
         //在点击HTML里面的"点击跳转到下一个页面"的时候会发送一个包含"pushNextVC"的message
         if ([message.body isEqualToString:@"pushNextVC"]) {
 //            SecondViewController *secondVC = [SecondViewController new];
 //            secondVC.htmlText = self.htmlStr;
 //            [self.navigationController pushViewController:secondVC animated:YES];
         }
     }
 */
}

- (UIButton *)deleBtn {
    if (!_deleBtn) {
        _deleBtn = [[UIButton alloc] init];
        [_deleBtn setTitle:@"X" forState:UIControlStateNormal];
        [_deleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _deleBtn.backgroundColor = [UIColor clearColor];
        _deleBtn.titleLabel.font = FONT_WITH_SIZE(20);
        [_deleBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleBtn;
}



@end
