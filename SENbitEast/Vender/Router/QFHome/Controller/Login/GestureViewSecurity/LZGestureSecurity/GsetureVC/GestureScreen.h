//
//  GestureScreen.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@class GestureScreen;

@protocol GestureScreenDelegate <NSObject>

- (void)screen:(GestureScreen *)screen didSetup:(NSString *)psw;

- (void)dismMisVC;

@end


@interface GestureScreen : NSObject

+ (instancetype)shared;
- (void)show;
- (void)dismiss;

@property(nonatomic, weak) id <GestureScreenDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
