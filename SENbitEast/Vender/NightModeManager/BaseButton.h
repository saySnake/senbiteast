//
//  BaseButton.h
//  Senbit
//
//  Created by 张玮 on 2019/12/27.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//BaseButton *button = [[BaseButton alloc] initWithImage:backImage
//                                             highlighted:higlightImage];

@interface BaseButton : UIButton


//normal状态下的图片名称
@property(nonatomic,copy) NSString *imageName;
//高亮状态下的图片名称
@property(nonatomic,copy) NSString *highligtImageName;

@property (nonatomic ,copy) NSString *colorName;
@property (nonatomic ,copy) NSString *backColorName;

- (id)initWithImage:(NSString *)imageName
        highlighted:(NSString *)highligtImageName;
- (void)loadThemeImage;


@end

NS_ASSUME_NONNULL_END
