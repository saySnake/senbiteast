//
// Created by 吕晴阳 on 2018/9/29.
// Copyright (c) 2018 Lv Qingyang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CPCountry : NSObject
@property(nonatomic, copy) NSString *en; //Angola
@property(nonatomic, copy) NSString *zh; //中国香港(中国香港)",
@property(nonatomic, copy) NSString *locale; //CN
@property(nonatomic) int code;  //852

- (NSString *)description;

+ (instancetype)countryWithEn:(NSString *)en zh:(NSString *)zh locale:(NSString *)locale code:(int)code;

@end
