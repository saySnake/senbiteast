//
//  SegmentBtn.h
//  Senbit
//
//  Created by 张玮 on 2020/1/13.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseButton.h"
#import "SegmentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SegmentBtn : BaseButton
/** 按钮模型 */
@property (nonatomic, strong) SegmentModel *model;
/** 标题标签 */
@property (nonatomic, weak) UILabel *titlelb;
/** 副标题 */
@property (nonatomic, weak) UILabel *subTitlelb;

@end

NS_ASSUME_NONNULL_END
