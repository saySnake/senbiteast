//
//  WarnLabel.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (Anim)

/*
 *  摇动
 */
- (void)shake;

@end

@interface WarnLabel : UILabel

/*
 *  普通提示信息
 */
- (void)showNormalMsg:(NSString *)msg;


/*
 *  警示信息
 */
- (void)showWarnMsg:(NSString *)msg;

/*
 *  警示信息(shake)
 */
- (void)showWarnMsgAndShake:(NSString *)msg;

- (void)showSuccessMsg;

@end

NS_ASSUME_NONNULL_END
