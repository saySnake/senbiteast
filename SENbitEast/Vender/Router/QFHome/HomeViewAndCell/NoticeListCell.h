//
//  NoticeListCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/5.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoticeListCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;

/** titleLb **/
@property (nonatomic ,strong) UILabel *titleLb;

/** contentLB **/
@property (nonatomic ,strong) UILabel *contentLb;

/** timeLb **/
@property (nonatomic ,strong) UILabel *timeLb;

/** line **/
@property (nonatomic ,strong) UILabel *line;

/** seeDetail **/
@property (nonatomic ,strong) UILabel *seeDetail;

/** img **/
@property (nonatomic ,strong) UIImageView *img;

@end

NS_ASSUME_NONNULL_END
