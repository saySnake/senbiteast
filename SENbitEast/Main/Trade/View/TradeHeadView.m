//
//  TradeHeadView.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/29.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeHeadView.h"
#import "UIView+YJUiView.h"
#import "YJSliderPointView.h"
#import "TradeCell.h"
#import "TradeBuyCell.h"
#import "XXDepthMapData.h"
#import "NSString+SuitScanf.h"
#import "XXTickerModel.h"
#import "UIColor+Y_StockChart.h"
#import "LoginViewController.h"


@interface TradeHeadView()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource> {
    dispatch_queue_t serialQueue;
    //当前索引 是买是卖
    NSInteger _currentIndex;
}
@property (strong, nonatomic) XXWebQuoteModel *webModel;
@property (strong, nonatomic) XXWebQuoteModel *headModel;

/** 行情数据模型 */
@property (strong, nonatomic) XXTickerModel *tickerModel;
//切换按钮
@property (nonatomic ,strong) FSCustomButton *switchButton;
//百分比
@property (nonatomic ,strong) UILabel *perLb;
//K线
@property (nonatomic ,strong) UIButton *klineBtn;
//买入
@property (nonatomic ,strong) UIButton *buyBtn;
//卖出
@property (nonatomic ,strong) UIButton *selBtn;
//可买
@property (nonatomic ,strong) UILabel *ableBuyTitle;
//可买数量
@property (nonatomic ,strong) UILabel *ableCount;
//aview
@property (nonatomic ,strong) UIView *aView;
//价格tf
@property (nonatomic ,strong) UITextField *priceTF;
//基础价格
@property (nonatomic ,strong) UILabel *priceLb;
//价格
@property (nonatomic ,strong) UILabel *cnyLb;
//bview
@property (nonatomic ,strong) UIView *bView;
//数量
@property (nonatomic ,strong) UITextField *amountTF;
//买数量的币种
@property (nonatomic ,strong) UILabel *amountLb;
//可用
@property (nonatomic ,strong) UILabel *ableTitle;
//可用数量
@property (nonatomic ,strong) UILabel *ablePrice;
//滑块
@property (nonatomic ,strong) YJSliderPointView *sliderView;
//0
@property (nonatomic ,strong) UILabel *zeroNum;
//最大数量
@property (nonatomic ,strong) UILabel *maxNum;
//交易额
@property (nonatomic ,strong) UILabel *tradeNum;
//买入or 卖出
@property (nonatomic ,strong) UIButton *tradeBtn;
//line
@property (nonatomic ,strong) UILabel *line;
//k线
@property (nonatomic ,strong) UIButton *kLineBtn;
//右边价格
@property (nonatomic ,strong) UILabel *rightPrice;
//右边数量
@property (nonatomic ,strong) UILabel *rightNum;
//selTabl
@property (nonatomic ,strong) UITableView *selTable;
//buyTabl
@property (nonatomic ,strong) UITableView *buyTable;
//美金
@property (nonatomic ,strong) UILabel *rightCenterLb;
//人民币
@property (nonatomic ,strong) UILabel *cnLb;
/** cell数组 */
@property (strong, nonatomic) NSMutableArray *cellsArray;
@property (strong, nonatomic) NSMutableArray *cellsaArray;

@property (assign, nonatomic) BOOL isAmount;

/** 模型数组 */
@property (strong, nonatomic) NSMutableArray *modelsArray;
/** 盘口平均值 */
@property (assign, nonatomic) double ordersAverage;
/**  数据 */
@property (strong, nonatomic) XXDepthMapData *mapData;

@property (assign, nonatomic) BOOL loginSuc;

//获取可买资产
@property (nonatomic ,strong) NSString *ableAssertCount;
//最大可买数量
@property (nonatomic ,strong) NSString *maxCount;
//记录当前滑块的百分比
@property (nonatomic ,strong) NSString *sliderFloat;

//获取可卖资产
@property (nonatomic ,strong) NSString *ableSelAssertCount;
@property (nonatomic ,assign) BOOL isFirstAppear;

@property (nonatomic ,strong) NSMutableArray *sellModel;
@property (nonatomic ,strong) NSMutableArray *buyModel;

@end

@implementation TradeHeadView

- (NSMutableArray *)sellModel {
    if (!_sellModel) {
        _sellModel = [[NSMutableArray alloc] init];
    }
    return _sellModel;
}

- (NSMutableArray *)buyModel {
    if (!_buyModel) {
        _buyModel = [[NSMutableArray alloc] init];
    }
    return _buyModel;
}

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
        self.backgroundColor = [UIColor whiteColor];
        self.maxCount = @"0";
        self.ableSelAssertCount = @"0";
        [self setupUI];
        [self tradeShow];
   
        KWeakSelf
        self.headModel = [[XXWebQuoteModel alloc] init];
        // 2. webSocket成功回调
        self.headModel.successBlock = ^(NSArray *data) {
            if ([data isKindOfClass:[NSArray class]] && data.count > 0) {
                NSDictionary *dict = [data firstObject];
                if (!IsEmpty(dict)) {
                    weakSelf.tickerModel.t = [dict[@"t"] longLongValue];
                    weakSelf.tickerModel.c = [NSString divV1:dict[@"lastPrice"] v2:TENZero];
                    weakSelf.tickerModel.l = [NSString divV1:dict[@"low"] v2:TENZero];
                    weakSelf.tickerModel.h = [NSString divV1:dict[@"high"] v2:TENZero];
                    weakSelf.tickerModel.v = [NSString divV1:dict[@"volume"] v2:TENZero];
                    weakSelf.tickerModel.o = [NSString divV1:dict[@"open"] v2:TENZero];
                    [weakSelf refreshDataOfTicker];
                }
            }
        };
    }
    return self;
}


- (void)changeLanguage {
    [self.buyBtn setTitle:kLocalizedString(@"trade_buy") forState:UIControlStateNormal];
    [self.selBtn setTitle:kLocalizedString(@"trade_sel") forState:UIControlStateNormal];
    NSAttributedString *attrStrings = [[NSAttributedString alloc] initWithString:kLocalizedString(@"trade_price") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.priceTF.attributedPlaceholder = attrStrings;
    NSAttributedString *attrStrings1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"trade_count") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
    self.amountTF.attributedPlaceholder = attrStrings1;
    self.ableTitle.text = kLocalizedString(@"trade_ableUse");
    [self.tradeBtn setTitle:kLocalizedString(@"trade_buy") forState:UIControlStateNormal];
    self.rightPrice.text = kLocalizedString(@"trade_price");
    self.rightNum.text = kLocalizedString(@"trade_count");
    self.tradeNum.text = kLocalizedString(@"trade_trade");
}



- (void)dismiss {
    [self traderDismiss];
    [KQuoteSocket cancelWebSocketSubscribeWithWebSocketModel:self.webModel lineType:@"currency-overview"];
}

- (void)refreshDataOfTicker {
    NSString *riseString = [NSString riseFallValue:([self.tickerModel.c doubleValue] - [self.tickerModel.o doubleValue])*100/[self.tickerModel.o doubleValue]];
    UIColor *textColor;

    if (([self.tickerModel.c doubleValue] - [self.tickerModel.o doubleValue]) >= 0) {
        textColor = [UIColor increaseColor];
        self.perLb.textColor = textColor;
        riseString = [NSString stringWithFormat:@"+%@", riseString];
        self.perLb.backgroundColor = HEXCOLOR(0xe5f5f5);

    } else {
        textColor = [UIColor decreaseColor];

        self.perLb.textColor = textColor;
        riseString = [NSString stringWithFormat:@"%@", riseString];
        self.perLb.backgroundColor = HEXCOLOR(0xfde8f1);
    }
    self.rightCenterLb.textColor = textColor;
    self.perLb.text = riseString;
    NSString *closePrice = [KDecimal decimalNumber:self.tickerModel.c RoundingMode:NSRoundDown scale:[KDecimal scale:KDetail.symbolModel.quotePrecision]];
    self.rightCenterLb.text = closePrice;
    self.cnLb.text = [[RatesManager sharedRatesManager] getRatesPriceValue:[closePrice doubleValue]];
    if (!self.isFirstAppear) {
        self.isFirstAppear = YES;
        self.priceTF.text = self.rightCenterLb.text;
        
        NSString *hStr = [KDecimal decimalNumber:[NSString divV1:self.ableAssertCount v2:self.priceTF.text] RoundingMode:NSRoundDown scale:[KTrade.coinTradeModel.pricePrecision integerValue]];
        self.maxCount = hStr;
        if (!self.loginSuc) {
            self.ableCount.text = [NSString stringWithFormat:@"--%@",KTrade.coinTradeModel.baseTokenName];
        } else {
            self.ableCount.text = [NSString stringWithFormat:@"%@%@",hStr,KTrade.coinTradeModel.baseTokenName];
        }
        self.cnyLb.text = [[RatesManager sharedRatesManager] getRatesPriceValue:[self.priceTF.text doubleValue]];
        self.maxNum.text = self.ableCount.text;
    }
}

- (XXDepthMapData *)mapData {
    if (_mapData == nil) {
        _mapData = [[XXDepthMapData alloc] init];
    }
    return _mapData;
}

- (void)tradeShow {
    [self.mapData show];
}

- (void)traderDismiss {
    [self cleanData];
    [self.mapData dismiss];
    self.isFirstAppear = NO;
}


- (void)cleanData {
    [self.cellsArray removeAllObjects];
    [self.cellsaArray removeAllObjects];
    [self reloadDataOfDepth];
}

- (void)calData {
    self.isFirstAppear = NO;
    [self tradeShow];
    KWeakSelf
    self.mapData.reloadDataFinish = ^{
        KDetail.blockList = ^(NSMutableArray * _Nonnull modelsArray, double ordersAverage) {
            weakSelf.modelsArray = modelsArray;
            weakSelf.ordersAverage  = ordersAverage;
            [weakSelf reloadDataOfDepth];
            [weakSelf reloadDataOfDeptha];
        };
    };
    
    NSLog(@"%@",KTrade.coinTradeModel.symbolId);
    [self.switchButton setTitle:KTrade.coinTradeModel.symbolName forState:UIControlStateNormal];
    self.priceLb.text = KTrade.coinTradeModel.quoteTokenName;
    self.amountLb.text = KTrade.coinTradeModel.baseTokenName;
    self.cnyLb.text = @"--";
    self.ableCount.text = [NSString stringWithFormat:@"--%@",KTrade.coinTradeModel.baseTokenName];
    self.ablePrice.text = [NSString stringWithFormat:@"--%@",KTrade.coinTradeModel.quoteTokenName];

    [self requestPing];
    if (KTrade.coinIsSell == NO) {
        [self buyAction];
    }
    if (KTrade.coinIsSell == YES) {
        [self selAction];
    }
    [self sendDetailTikerSubscribe];
    self.priceTF.text = @"";
    self.amountTF.text = @"";
    [self endEditing:YES];
}

- (void)sendDetailTikerSubscribe {
    self.headModel.params = [NSMutableDictionary dictionary];
    if (KTrade.coinTradeModel.symbolId) {
        NSDictionary *dic = @{@"pair":KTrade.coinTradeModel.symbolId};
        self.headModel.params[@"data"] = dic;
        self.headModel.params[@"topic"] = @"currency-overview";
        [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.headModel lineStype:@"currency-overview"];
    }
}


- (void)requestPing {
    [SBNetworTool getWithUrl:EastPing params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            self.loginSuc = YES;
            [self requirAssert];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {

    }];
}

- (void)requirAssert {
    if (_currentIndex == 0) {
        NSString *url = [NSString stringWithFormat:@"%@%@",EastCoinAssert,KTrade.coinTradeModel.quoteTokenName];
        [SBNetworTool getWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                NSLog(@"%@",responseObject);
                self.ableAssertCount = responseObject[@"data"][@"balance"];
                NSString *pric = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f", [responseObject[@"data"][@"balance"] floatValue]] RoundingMode:NSRoundDown scale:[KDecimal scale:KTrade.coinTradeModel.basePrecision]];
                self.ablePrice.text = [NSString stringWithFormat:@"%@%@",pric,KTrade.coinTradeModel.quoteTokenName];
                self.ableCount.text = [NSString stringWithFormat:@"%@%@",self.maxCount,KTrade.coinTradeModel.baseTokenName];
                self.maxNum.text = self.ableCount.text;
//                self.amountTF.text = [NSString mulV1:self.sliderFloat v2:self.maxCount];
                self.tradeNum.text = [NSString stringWithFormat:kLocalizedString(@"trade_totalCash%@%@"),[NSString mulV1:self.amountTF.text v2:self.priceTF.text],KTrade.coinTradeModel.quoteTokenName];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            
        }];
    } else {
        NSString *url = [NSString stringWithFormat:@"%@%@",EastCoinAssert,KTrade.coinTradeModel.baseTokenName];
        [SBNetworTool getWithUrl:url params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            if (SuccessCode == 200) {
                NSLog(@"%@",responseObject);
                self.ableSelAssertCount = responseObject[@"data"][@"balance"];
                self.ableCount.text = [NSString stringWithFormat:@"%@%@",responseObject[@"data"][@"balance"],KTrade.coinTradeModel.baseTokenName];
                
                NSString *pric = [KDecimal decimalNumber:[NSString stringWithFormat:@"%.12f", [responseObject[@"data"][@"balance"] floatValue]] RoundingMode:NSRoundDown scale:[KDecimal scale:KTrade.coinTradeModel.basePrecision]];

                self.ablePrice.text = [NSString stringWithFormat:@"%@%@",pric,KTrade.coinTradeModel.baseTokenName];
                self.maxNum.text = self.ableCount.text;
//                self.amountTF.text = [NSString mulV1:self.sliderFloat v2:self.ableSelAssertCount];
                self.tradeNum.text = [NSString stringWithFormat:kLocalizedString(@"trade_totalCash%@%@"),[NSString mulV1:self.amountTF.text v2:self.priceTF.text],KTrade.coinTradeModel.quoteTokenName];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            
        }];
    }
}


- (void)setupUI {
    CGFloat x = 20;
    self.switchButton.frame = CGRectMake(x, 5, self.width/2 - 80, 50);
    [self addSubview:self.switchButton];
    self.perLb.frame = CGRectMake(CGRectGetMaxX(self.switchButton.frame) + 5, 18, 80, 25);
    [self addSubview:self.perLb];
    self.buyBtn.frame = CGRectMake(x, CGRectGetMaxY(self.perLb.frame) + 10, (self.width/2 - x)/2, 40);
    [self addSubview:self.buyBtn];
    self.selBtn.frame = CGRectMake(CGRectGetMaxX(self.buyBtn.frame), CGRectGetMaxY(self.perLb.frame) + 10, (self.width/2 - x)/2, 40);
    [self addSubview:self.selBtn];
    self.ableBuyTitle.frame = CGRectMake(CGRectGetMinX(self.buyBtn.frame), CGRectGetMaxY(self.buyBtn.frame) + 10, 80, 20);
    [self addSubview:self.ableBuyTitle];
    self.ableCount.frame = CGRectMake(CGRectGetMinX(self.selBtn.frame), CGRectGetMaxY(self.buyBtn.frame) + 10, (self.width/2 - x)/2, 20);
    [self addSubview:self.ableCount];
    self.aView.frame = CGRectMake(CGRectGetMinX(self.buyBtn.frame), CGRectGetMaxY(self.ableCount.frame) + 10, self.width/2 - x, 50);
    [self addSubview:self.aView];
    self.priceTF.frame = CGRectMake(6, 5, self.aView.width/2 +40, 40);
    [self.aView addSubview:self.priceTF];
    self.priceLb.frame = CGRectMake(self.aView.width/2 + 40, 5, self.aView.width/2 - 44, 40);
    [self.aView addSubview:self.priceLb];
    self.cnyLb.frame = CGRectMake(x, CGRectGetMaxY(self.aView.frame) + 5, 140, 15);
    [self addSubview:self.cnyLb];
    self.bView.frame = CGRectMake(x, CGRectGetMaxY(self.cnyLb.frame) + 10, self.aView.width, self.aView.height);
    [self addSubview:self.bView];
    self.amountTF.frame = CGRectMake(6, 5, self.bView.width/2 + 40, 40);
    [self.bView addSubview:self.amountTF];
    self.amountLb.frame = CGRectMake(self.bView.width/2 + 40, 5, self.bView.width/2 - 44, self.priceLb.height);
    [self.bView addSubview:self.amountLb];
    self.ableTitle.frame = CGRectMake(x, CGRectGetMaxY(self.bView.frame) + 8, 80, 15);
    [self addSubview:self.ableTitle];
    self.ablePrice.frame = CGRectMake(self.bView.width/2, CGRectGetMaxY(self.bView.frame) + 8, CGRectGetMinX(self.ableCount.frame), 15);
    [self addSubview:self.ablePrice];
    
    self.sliderView = [[YJSliderPointView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.ableTitle.frame), CGRectGetMaxY(self.ableTitle.frame) + 5, self.bView.width, 40)];
    KWeakSelf
    self.sliderView.SliderBlock = ^(NSString * fl) {
        NSLog(@"%@-----------",fl);
        weakSelf.sliderFloat = fl;
        if (_currentIndex == 0) {
            weakSelf.amountTF.text = [NSString mulV1:weakSelf.maxCount v2:fl];
        } else {
            weakSelf.amountTF.text = [NSString mulV1:weakSelf.ableSelAssertCount v2:fl];
        }
        weakSelf.tradeNum.text = [NSString stringWithFormat:kLocalizedString(@"trade_totalCash%@%@"),[NSString mulV1:weakSelf.amountTF.text v2:weakSelf.priceTF.text],KTrade.coinTradeModel.quoteTokenName];
    };
    [self addSubview:self.sliderView];
    self.zeroNum.frame = CGRectMake(x, CGRectGetMaxY(self.sliderView.frame) + 5, 70, 15);
    [self addSubview:self.zeroNum];
    self.maxNum.frame = CGRectMake(DScreenW/2  - 130, CGRectGetMinY(self.zeroNum.frame), 130, 15);
    [self addSubview:self.maxNum];
    self.tradeNum.frame = CGRectMake(x, CGRectGetMaxY(self.zeroNum.frame) + 10, DScreenW*0.5 - 20, 20);
    [self addSubview:self.tradeNum];
    self.tradeBtn.frame = CGRectMake(x, CGRectGetMaxY(self.tradeNum.frame) + 11, DScreenW / 2 - x, 50);
    [self addSubview:self.tradeBtn];
    self.line.frame = CGRectMake(0, CGRectGetMaxY(self.tradeBtn.frame) + 15, DScreenW, 10);
    [self addSubview:self.line];
    
    self.kLineBtn.frame = CGRectMake(DScreenW - 50, x, 50, 25);
    [self addSubview:self.kLineBtn];
    self.rightPrice.frame = CGRectMake(DScreenW/2 + 10, CGRectGetMinY(self.buyBtn.frame), 100, 16);
    [self addSubview:self.rightPrice];
    self.rightNum.frame = CGRectMake(DScreenW - 50, CGRectGetMinY(self.buyBtn.frame), 40, 16);
    [self addSubview:self.rightNum];
    
    self.selTable.frame = CGRectMake(DScreenW/2 + 10, CGRectGetMaxY(self.rightPrice.frame) + 5, DScreenW/2 - 10, 152);
    self.selTable.dataSource = self;
    self.selTable.delegate = self;

    [self addSubview:self.selTable];
    [self.selTable reloadData];

    self.rightCenterLb.frame = CGRectMake(DScreenW/2 + 10, CGRectGetMaxY(self.selTable.frame) + 10, 100, 25);
    [self addSubview:self.rightCenterLb];
    self.cnLb.frame = CGRectMake(DScreenW/2 + 10, CGRectGetMaxY(self.rightCenterLb.frame) + 3, DScreenW/2 - 10, 21);
    [self addSubview:self.cnLb];
    
    self.buyTable.frame = CGRectMake(DScreenW/2 + 10, CGRectGetMaxY(self.cnLb.frame) + 5, DScreenW/2 - 10, 152);
    self.buyTable.delegate = self;
    self.buyTable.dataSource = self;
    [self addSubview:self.buyTable];
    [self.buyTable reloadData];
    
    self.priceTF.text =  self.rightCenterLb.text;
}

#pragma mark - 2. 表示图代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.selTable) {
        return 5;
    } else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.selTable) {
        return [TradeCell getCellHeight];
    }
    else {
        return [TradeBuyCell getCellHeight];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.selTable) {
        NSInteger idx = self.cellsArray.count - 1 - indexPath.row;
        TradeCell *cell = self.cellsArray[idx];
        return cell;
    } else {
        TradeBuyCell *cell = self.cellsaArray[indexPath.row];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.selTable) {
        NSInteger idx = self.cellsArray.count - 1 - indexPath.row;
        XXDepthOrderModel *m = self.sellModel[idx];
        self.priceTF.text = m.rightPrice;
    } else {
        XXDepthOrderModel *m = self.buyModel[indexPath.row];
        self.priceTF.text = m.leftPrice;
    }
    
    if (_currentIndex == 0) { //可买
            if (self.loginSuc) {
                NSString *hStr = [KDecimal decimalNumber:[NSString divV1:self.ableAssertCount v2:self.priceTF.text] RoundingMode:NSRoundDown scale:[KTrade.coinTradeModel.pricePrecision integerValue]];
                self.maxCount = hStr;
                self.ableCount.text = [NSString stringWithFormat:@"%@%@",hStr,KTrade.coinTradeModel.baseTokenName];
                self.cnyLb.text = [[RatesManager sharedRatesManager] getRatesPriceValue:[self.priceTF.text doubleValue]];
                self.maxNum.text = self.ableCount.text;
            }
    } else { //可卖
            if (self.loginSuc) {
                self.cnyLb.text = [[RatesManager sharedRatesManager] getRatesPriceValue:[self.priceTF.text doubleValue]];
                self.maxNum.text = self.ableCount.text;
        }
    }
    self.tradeNum.text = [NSString stringWithFormat:kLocalizedString(@"trade_totalCash%@%@"),[NSString mulV1:self.amountTF.text v2:self.priceTF.text],KTrade.coinTradeModel.quoteTokenName];

}

/** cell数组 */
- (NSMutableArray *)cellsArray {
    if (_cellsArray == nil) {
        _cellsArray = [NSMutableArray array];
        for (NSInteger i = 0; i < 5; i ++) {
            TradeCell *cell = [[TradeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [_cellsArray addObject:cell];
        }
    }
    return _cellsArray;
}


- (NSMutableArray *)cellsaArray {
    if (_cellsaArray == nil) {
        _cellsaArray = [NSMutableArray array];
        for (NSInteger i = 0; i < 5; i ++) {
            TradeBuyCell *cell = [[TradeBuyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [_cellsaArray addObject:cell];
        }
    }
    return _cellsaArray;
}

#pragma mark -  刷新数据5
- (void)reloadDataOfDepth {
    [self.sellModel removeAllObjects];
    [UIView animateWithDuration:0.2 animations:^{
        for (NSInteger i = 0; i < self.cellsArray.count; i ++) {
            TradeCell *cell = self.cellsArray[i];
            cell.ordersAverage = self.ordersAverage;
            cell.model = self.modelsArray[i];
            [self.sellModel addObject:self.modelsArray[i]];
            cell.isAmount = self.isAmount;
            [cell reloadData];
        }
    }];
}

- (void)reloadDataOfDeptha {
    [self.buyModel removeAllObjects];
    [UIView animateWithDuration:0.2 animations:^{
        for (NSInteger i = 0; i < self.cellsaArray.count; i ++) {
            TradeBuyCell *cell = self.cellsaArray[i];
            cell.ordersAverage = self.ordersAverage;
            cell.model = self.modelsArray[i];
            [self.buyModel addObject:self.modelsArray[i]];
            cell.isAmount = self.isAmount;
            [cell reloadData];
        }
    }];
}


- (void)switchButtonClick:(UIButton *)sender {
    if (self.switchBtnBlock) {
        self.switchBtnBlock();
    }
}

- (FSCustomButton *)switchButton {
    if (_switchButton == nil) {
        _switchButton = [[FSCustomButton alloc] initWithFrame:CGRectMake(64, kStatusBarHeight, kScreen_Width - 150 - 64, 50)];
        _switchButton.buttonImagePosition = FSCustomButtonImagePositionLeft;
        _switchButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _switchButton.titleLabel.font = kFontBold20;
        _switchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        _switchButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_switchButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _switchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_switchButton setImage:[[UIImage imageNamed:@"left_more"] imageWithColor:[UIColor blackColor]]  forState:UIControlStateNormal];
        [_switchButton setTitle:KDetail.symbolModel.symbolId forState:UIControlStateNormal];
        [_switchButton addTarget:self action:@selector(switchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _switchButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _switchButton;
}

- (UILabel *)perLb {
    if (!_perLb) {
        _perLb = [[UILabel alloc] init];
        _perLb.textAlignment = 1;
        _perLb.font = BOLDSYSTEMFONT(16);
        _perLb.textColor = ThemeGreenColor;
    }
    return _perLb;
}


- (UIButton *)buyBtn {
    if (!_buyBtn) {
        _buyBtn = [[UIButton alloc] init];
        [_buyBtn setTitle:kLocalizedString(@"trade_buy") forState:UIControlStateNormal];
        [_buyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_buyBtn setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        _buyBtn.titleLabel.font = BOLDSYSTEMFONT(16);
        [_buyBtn addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
        [_buyBtn setBackgroundImage:[self imageWithColor:HEXCOLOR(0xF7F6FC)] forState:UIControlStateNormal];
        [_buyBtn setBackgroundImage:[self imageWithColor:ThemeGreenColor] forState:UIControlStateSelected];
        _buyBtn.selected = YES;
    }
    return _buyBtn;
}

- (void)buyAction {
    self.buyBtn.selected = YES;
    self.selBtn.selected = NO;
    _currentIndex = 0;
    self.tradeBtn.backgroundColor = ThemeGreenColor;
    self.ableBuyTitle.text = kLocalizedString(@"trade_ableBuy");
    [self.tradeBtn setTitle:kLocalizedString(@"trade_buy") forState:UIControlStateNormal];
    [self requestPing];
}

- (UIButton *)selBtn {
    if (!_selBtn) {
        _selBtn = [[UIButton alloc] init];
        [_selBtn setTitle:kLocalizedString(@"trade_sel") forState:UIControlStateNormal];
        [_selBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_selBtn setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        [_selBtn addTarget:self action:@selector(selAction) forControlEvents:UIControlEventTouchUpInside];
        [_selBtn setBackgroundImage:[self imageWithColor:HEXCOLOR(0xF7F6FC)] forState:UIControlStateNormal];
        [_selBtn setBackgroundImage:[self imageWithColor:ThemeRedColor] forState:UIControlStateSelected];
        _selBtn.titleLabel.font = BOLDSYSTEMFONT(16);
    }
    return _selBtn;
}

- (void)selAction {
    self.buyBtn.selected = NO;
    self.selBtn.selected = YES;
    _currentIndex = 1;
    self.tradeBtn.backgroundColor = ThemeRedColor;
    self.ableBuyTitle.text = kLocalizedString(@"trade_ableSel");
    [self.tradeBtn setTitle:kLocalizedString(@"trade_sel") forState:UIControlStateNormal];
    [self requestPing];
}

- (UILabel *)ableBuyTitle {
    if (!_ableBuyTitle) {
        _ableBuyTitle = [[UILabel alloc] init];
        _ableBuyTitle.backgroundColor = [UIColor clearColor];
        _ableBuyTitle.textAlignment = 0;
        _ableBuyTitle.font = kFont14;
        _ableBuyTitle.textColor = HEXCOLOR(0xA0A0A0);
        _ableBuyTitle.text = kLocalizedString(@"trade_ableBuy");
    }
    return _ableBuyTitle;
}

- (UILabel *)ableCount {
    if (!_ableCount) {
        _ableCount = [[UILabel alloc] init];
        _ableCount.font = kFont14;
        _ableCount.backgroundColor = [UIColor clearColor];
        _ableCount.textAlignment = 2;
        _ableCount.textColor = HEXCOLOR(0xA0A0A0);
        _ableCount.text = @"0.000BTC";
        _ableCount.adjustsFontSizeToFitWidth = YES;
    }
    return _ableCount;
}

- (UIView *)aView {
    if (!_aView) {
        _aView = [[UIView alloc] init];
        _aView.backgroundColor = WhiteColor;
        _aView.cornerRadius = 2;
        _aView.borderColor = BorderColor;
    }
    return _aView;
}

- (UITextField *)priceTF {
    if (!_priceTF) {
        _priceTF = [[UITextField alloc] init];
        _priceTF.delegate = self;
        _priceTF.textColor = [UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"];
        _priceTF.keyboardType = UIKeyboardTypeDecimalPad;
        _priceTF.borderStyle = UITextBorderStyleNone;
        _priceTF.font = kFont14;
        NSAttributedString *attrStrings = [[NSAttributedString alloc] initWithString:kLocalizedString(@"trade_price") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _priceTF.attributedPlaceholder = attrStrings;
       [_priceTF addTarget:self action:@selector(textFieldDidChange: ) forControlEvents:UIControlEventEditingChanged];
        UIToolbar  *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:)],
                               nil];
        _priceTF.inputAccessoryView = numberToolbar;

    }
    return _priceTF;
}

- (UILabel *)priceLb {
    if (!_priceLb) {
        _priceLb = [[UILabel alloc] init];
        _priceLb.textAlignment = 2;
        _priceLb.textColor = PlaceHolderColor;
        _priceLb.adjustsFontSizeToFitWidth = YES;
        _priceLb.font = kFont14;
        _priceLb.text = @"USDT";
    }
    return _priceLb;
}

- (UILabel *)cnyLb {
    if (!_cnyLb) {
        _cnyLb = [[UILabel alloc] init];
        _cnyLb.textAlignment = 0;
        _cnyLb.font = kFont14;
        _cnyLb.textColor = PlaceHolderColor;
        _cnyLb.text = @"--";
    }
    return _cnyLb;
}

- (UIView *)bView {
    if (!_bView) {
        _bView = [[UIView alloc] init];
        _bView.backgroundColor = WhiteColor;
        _bView.cornerRadius = 2;
        _bView.borderColor = BorderColor;
    }
    return _bView;
}

- (UITextField *)amountTF {
    if (!_amountTF) {
        _amountTF = [[UITextField alloc] init];
        _amountTF.delegate = self;
        _amountTF.textColor = [UIColor colorWithLightColorStr:@"4A4A4A" DarkColor:@"4A4A4A"];
        _amountTF.keyboardType = UIKeyboardTypeDecimalPad;
        _amountTF.borderStyle = UITextBorderStyleNone;
        _amountTF.font = kFont14;
        NSAttributedString *attrStrings = [[NSAttributedString alloc] initWithString:kLocalizedString(@"trade_count") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _amountTF.attributedPlaceholder = attrStrings;
       [_amountTF addTarget:self action:@selector(textFieldDidChange: ) forControlEvents:UIControlEventEditingChanged];
        UIToolbar  *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:)],
                               nil];
        _amountTF.inputAccessoryView = numberToolbar;

    }
    return _amountTF;
}

- (void)cancelNumberPad:(UITextField*)textField {
    [self.amountTF resignFirstResponder];
    [self.priceTF resignFirstResponder];
}


- (void)doneWithNumberPad:(UITextField*)textField {
    [self.amountTF resignFirstResponder];
    [self.priceTF resignFirstResponder];
}



- (UILabel *)amountLb {
    if (!_amountLb) {
        _amountLb = [[UILabel alloc] init];
        _amountLb.textAlignment = 2;
        _amountLb.textColor = PlaceHolderColor;
        _amountLb.adjustsFontSizeToFitWidth = YES;
        _amountLb.font = kFont14;
        _amountLb.text = @"BTC";
    }
    return _amountLb;
}

- (UILabel *)ableTitle {
    if (!_ableTitle) {
        _ableTitle = [[UILabel alloc] init];
        _ableTitle.textColor = PlaceHolderColor;
        _ableTitle.textAlignment = 0;
        _ableTitle.adjustsFontSizeToFitWidth = YES;
        _ableTitle.font = kFont12;
        _ableTitle.text = kLocalizedString(@"trade_ableUse");
    }
    return _ableTitle;
}

- (UILabel *)ablePrice {
    if (!_ablePrice) {
        _ablePrice = [[UILabel alloc] init];
        _ablePrice.textColor = PlaceHolderColor;
        _ablePrice.textAlignment = 2;
        _ablePrice.adjustsFontSizeToFitWidth = YES;
        _ablePrice.font = kFont12;
        _ablePrice.text = @"--USDT";
    }
    return _ablePrice;
}

- (YJSliderPointView *)sliderView{
    if (_sliderView == nil) {
        _sliderView = [[YJSliderPointView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.ableTitle.frame), CGRectGetMaxY(self.ableTitle.frame) - 5, self.bView.width, 40)];
        _sliderView.backgroundColor = [UIColor clearColor];
    }
    return _sliderView;
}

- (UILabel *)zeroNum {
    if (!_zeroNum) {
        _zeroNum = [[UILabel alloc] init];
        _zeroNum.text = @"0";
        _zeroNum.textColor = PlaceHolderColor;
        _zeroNum.textAlignment = 0;
        _zeroNum.font = kFont14;
    }
    return _zeroNum;
}

- (UILabel *)maxNum {
    if (!_maxNum) {
        _maxNum = [[UILabel alloc] init];
        _maxNum.text = @"--";
        _maxNum.textColor = PlaceHolderColor;
        _maxNum.textAlignment = 2;
        _maxNum.font = kFont14;
        _maxNum.adjustsFontSizeToFitWidth = YES;
    }
    return _maxNum;
}

- (UILabel *)tradeNum {
    if (!_tradeNum) {
        _tradeNum = [[UILabel alloc] init];
        _tradeNum.text = kLocalizedString(@"trade_trade");
        _tradeNum.font = kFont14;
        _tradeNum.textAlignment = 0;
        _tradeNum.textColor = PlaceHolderColor;
    }
    return _tradeNum;
}

- (UIButton *)tradeBtn {
    if (!_tradeBtn) {
        _tradeBtn = [[UIButton alloc] init];
        _tradeBtn.cornerRadius = 2;
        [_tradeBtn setTitle:kLocalizedString(@"trade_buy") forState:UIControlStateNormal];
        [_tradeBtn addTarget:self action:@selector(tradeAction:) forControlEvents:UIControlEventTouchUpInside];
        _tradeBtn.titleLabel.font = BOLDSYSTEMFONT(18);
        _tradeBtn.backgroundColor = ThemeGreenColor;
    }
    return _tradeBtn;
}

- (void)tradeAction:(UIButton *)sender {
    UserInfo *user = [[CacheManager sharedMnager] getUserInfo];
    if (user.login == YES) {
        [self buyOrSell];
    } else {
        if ([self.delegate respondsToSelector:@selector(traderBuyAction)]) {
            [self.delegate traderBuyAction];
        }
    }
}

- (void)buyOrSell {
    NSLog(@"下单");
    NSString *ty = @"";
    if (_currentIndex == 0) {
        ty = @"buy";
    } else {
        ty = @"sell";
    }
    NSDictionary *dic = @{@"market":KDetail.symbolModel.symbolId,
                          @"tradeType":ty,
                          @"price":self.priceTF.text,
                          @"source":@"APP",
                          @"volume":self.amountTF.text,
                          @"orderType":@"limit"
    };
    self.TradeBuyBlock(dic);

}



- (UILabel *)line {
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = HEXCOLOR(0xF2F2F2);
    }
    return _line;
}

- (UITableView *)selTable {
    if (_selTable == nil) {
        _selTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width / 2 , 40) style:UITableViewStylePlain];
        _selTable.backgroundColor = [UIColor clearColor];
        _selTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _selTable.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _selTable.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _selTable;
}

- (UITableView *)buyTable {
    if (_buyTable == nil) {
        _buyTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width /2, 40) style:UITableViewStylePlain];
        _buyTable.backgroundColor = [UIColor clearColor];
        _buyTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _buyTable.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _buyTable.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _buyTable;
}

- (UILabel *)rightCenterLb {
    if (_rightCenterLb == nil) {
        _rightCenterLb = [[UILabel alloc] init];
        _rightCenterLb.textAlignment = 0;
        _rightCenterLb.font = BOLDSYSTEMFONT(15);
        _rightCenterLb.textColor = ThemeGreenColor;
        _rightCenterLb.text = @"--";
    }
    return _rightCenterLb;
}

- (UILabel *)cnLb {
    if (!_cnLb) {
        _cnLb = [[UILabel alloc] init];
        _cnLb.text = @"--";
        _cnLb.textAlignment = 0;
        _cnLb.font = kFont14;
        _cnLb.textColor = HEXCOLOR(0xA0A0A0);
        _cnLb.adjustsFontSizeToFitWidth = YES;
    }
    return _cnLb;
}

- (UIButton *)kLineBtn {
    if (!_kLineBtn) {
        _kLineBtn = [[UIButton alloc] init];
        [_kLineBtn setImage:[UIImage imageNamed:@"k_line"] forState:UIControlStateNormal];
        [_kLineBtn addTarget:self action:@selector(kLineAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _kLineBtn;
}

- (void)kLineAction {
    self.TradeHeadViewBlock();
}

- (UILabel *)rightPrice {
    if (!_rightPrice) {
        _rightPrice = [[UILabel alloc] init];
        _rightPrice.textAlignment = 0;
        _rightPrice.font = kFont14;
        _rightPrice.text = kLocalizedString(@"trade_price");
        _rightPrice.textColor = HEXCOLOR(0xA0A0A0);
    }
    return _rightPrice;
}

- (UILabel *)rightNum {
    if (!_rightNum) {
        _rightNum = [[UILabel alloc] init];
        _rightNum.text = kLocalizedString(@"trade_count");
        _rightNum.font = kFont14;
        _rightNum.textAlignment = 2;
        _rightNum.textColor = HEXCOLOR(0xA0A0A0);
    }
    return _rightNum;
}

- (void)textFieldDidChange:(UITextField *)textField {
    NSLog(@"textField = %@",textField.text);
    if (_currentIndex == 0) { //可买
        if (textField == self.priceTF) {
            if (self.loginSuc) {
                NSString *hStr = [KDecimal decimalNumber:[NSString divV1:self.ableAssertCount v2:textField.text] RoundingMode:NSRoundDown scale:[KTrade.coinTradeModel.pricePrecision integerValue]];
                self.maxCount = hStr;
                self.ableCount.text = [NSString stringWithFormat:@"%@%@",hStr,KTrade.coinTradeModel.baseTokenName];
                self.cnyLb.text = [[RatesManager sharedRatesManager] getRatesPriceValue:[textField.text doubleValue]];
                self.maxNum.text = self.ableCount.text;
//                self.amountTF.text = [NSString mulV1:self.sliderFloat v2:self.maxCount];
            }
        }
    } else { //可卖
        if (textField == self.priceTF) {
            if (self.loginSuc) {
//                NSString *hStr = [KDecimal decimalNumber:[NSString divV1:self.ableAssertCount v2:textField.text] RoundingMode:NSRoundDown scale:[KTrade.coinTradeModel.pricePrecision integerValue]];
                self.cnyLb.text = [[RatesManager sharedRatesManager] getRatesPriceValue:[textField.text doubleValue]];
                self.maxNum.text = self.ableCount.text;
//                self.amountTF.text = [NSString mulV1:self.sliderFloat v2:self.ableSelAssertCount];
            }
        }
    }
    self.tradeNum.text = [NSString stringWithFormat:kLocalizedString(@"trade_totalCash%@%@"),[NSString mulV1:self.amountTF.text v2:self.priceTF.text],KTrade.coinTradeModel.quoteTokenName];
}



- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string    {
    NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
    [futureString  insertString:string atIndex:range.location];
    if(textField.text.length == 0 && [string isEqualToString:@"."]) {
        //        textField.text = [textField.text substringToIndex:([textField.text length]-1)];
        return NO;
    }
    NSArray *arr = [textField.text componentsSeparatedByString:@"."];
    if(arr.count >= 2 && [string isEqualToString:@"."]) {
        return NO;
    }
    if (textField == self.priceTF) {
        NSInteger flag = 0;
        const NSInteger limited = [KTrade.coinTradeModel.pricePrecision intValue];//[self.model.pricePrecision intValue];//小数点后需要限制的个数
        for (int i = (int)(futureString.length - 1); i>=0; i--) {
            if ([futureString characterAtIndex:i] == '.') {
                if (flag > limited) {
                    return NO;
                }
                break;
            }
            flag++;
        }
        return YES;

    } else if (textField == self.amountTF) {
        NSInteger flag = 0;
        const NSInteger limited = [KTrade.coinTradeModel.amountPrecision intValue];//[self.model.amountPrecision intValue];//小数点后需要限制的个数
        for (int i = (int)(futureString.length - 1); i>=0; i--) {
            if ([futureString characterAtIndex:i] == '.') {
                if (flag > limited) {
                    return NO;
                }
                break;
            }
            flag++;
        }
        return YES;
    }
    return NO;
}


- (XXTickerModel *)tickerModel {
    if (_tickerModel == nil) {
        _tickerModel = [XXTickerModel new];
    }
    return _tickerModel;
}




@end
