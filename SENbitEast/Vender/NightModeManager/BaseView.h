//
//  BaseView.h
//  Senbit
//
//  Created by 张玮 on 2020/1/8.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseView : UIView

@property(nonatomic ,copy) NSString *colorName;

@end

NS_ASSUME_NONNULL_END
