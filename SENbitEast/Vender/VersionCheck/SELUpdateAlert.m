//
//  SELUpdateAlert.m
//  SelUpdateAlert
//
//  Created by 张玮 on 2019/12/31.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "SELUpdateAlert.h"
#import "SELUpdateAlertConst.h"
//应用发布地址的程序plist文件
static NSString *kVersion_plist = @"https://senbit.com/api/v1/s3-download/Senbit_prod_3.0.11.plist";//@"https://app.dr-link.cn/ios/app.plist";

#define DEFAULT_MAX_HEIGHT SCREEN_HEIGHT/3*2

@interface SELUpdateAlert()

/** 版本号 */
@property (nonatomic, copy) NSString *version;
/** 版本更新内容 */
@property (nonatomic, copy) NSString *desc;
/** 下载链接 */
@property (nonatomic, copy) NSString *downUrl;

@end

@implementation SELUpdateAlert

/**
 添加版本更新提示

 @param version 版本号
 @param descriptions 版本更新内容（数组）
 
 descriptions 格式如 @[@"1.xxxxxx",@"2.xxxxxx"]
 */
+ (void)showUpdateAlertWithVersion:(NSString *)version Descriptions:(NSArray *)descriptions isDoUpdate:(BOOL)upate url:(NSString *)downUrl {
    if (!descriptions || descriptions.count == 0) {
        return;
    }
    
    //数组转换字符串，动态添加换行符\n
    NSString *description = @"";
    for (NSInteger i = 0;  i < descriptions.count; ++i) {
        id desc = descriptions[i];
        if (![desc isKindOfClass:[NSString class]]) {
            return;
        }
        description = [description stringByAppendingString:desc];
        if (i != descriptions.count-1) {
            description = [description stringByAppendingString:@"\n"];
        }
    }
    NSLog(@"====%@",description);
    SELUpdateAlert *updateAlert = [[SELUpdateAlert alloc] initVersion:version description:description isDoUpdate:upate url:downUrl];
    [[UIApplication sharedApplication].delegate.window addSubview:updateAlert];
}

/**
 添加版本更新提示

 @param version 版本号
 @param description 版本更新内容（字符串）
 
description 格式如 @"1.xxxxxx\n2.xxxxxx"
 */
+ (void)showUpdateAlertWithVersion:(NSString *)version Description:(NSString *)description isDoUpdate:upate url:(NSString *)downUrl {
    SELUpdateAlert *updateAlert = [[SELUpdateAlert alloc]initVersion:version description:description isDoUpdate:upate url:downUrl];
    [[UIApplication sharedApplication].delegate.window addSubview:updateAlert];
}

- (instancetype)initVersion:(NSString *)version description:(NSString *)description isDoUpdate:(BOOL)upate url:(NSString *)downUrl {
    self = [super init];
    if (self) {
        self.version = version;
        self.desc = description;
//        self.image = [UIImage imageNamed:@"homeUpdte"];
//        self.contentMode = 2;
        self.userInteractionEnabled = YES;
        [self _setupUIisDoUpdate:upate url:downUrl];
    }
    return self;
}

- (void)_setupUIisDoUpdate:(BOOL)upate url:(NSString *)downUrl{
    self.downUrl = downUrl;
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3/1.0];
    
    //获取更新内容高度
    CGFloat descHeight = [self _sizeofString:self.desc font:[UIFont systemFontOfSize:SELDescriptionFont] maxSize:CGSizeMake(self.frame.size.width - Ratio(80) - Ratio(56), 1000)].height;
    
    //bgView实际高度
    CGFloat realHeight = descHeight + Ratio(314);
    
    //bgView最大高度
    CGFloat maxHeight = DEFAULT_MAX_HEIGHT;
    //更新内容可否滑动显示
    BOOL scrollEnabled = NO;
    
    //重置bgView最大高度 设置更新内容可否滑动显示
    if (realHeight > DEFAULT_MAX_HEIGHT) {
        scrollEnabled = YES;
        descHeight = DEFAULT_MAX_HEIGHT - Ratio(314);
    }else
    {
        maxHeight = realHeight;
    }
    
    //backgroundView
    UIView *bgView = [[UIView alloc]init];
    bgView.center = self.center;
    bgView.userInteractionEnabled = YES;
    bgView.bounds = CGRectMake(0, 0, self.frame.size.width - Ratio(40), maxHeight + Ratio(25));
    [self addSubview:bgView];
    
    //添加更新提示
    UIImageView *updateView = [[UIImageView alloc]initWithFrame:CGRectMake(Ratio(20), Ratio(18), bgView.frame.size.width - Ratio(40), maxHeight)];
    updateView.image = [UIImage imageNamed:@"homeUpdte"];
    updateView.contentMode = 2;
    updateView.layer.masksToBounds = NO;
    updateView.userInteractionEnabled = YES;
    updateView.layer.cornerRadius = 4.0f;
    [bgView addSubview:updateView];
    
    //20+166+10+28+10+descHeight+20+40+20 = 314+descHeight 内部元素高度计算bgView高度
//    UIImageView *updateIcon = [[UIImageView alloc]initWithFrame:CGRectMake((updateView.frame.size.width - Ratio(178))/2, Ratio(20), Ratio(178), Ratio(166))];
//    updateIcon.image = [UIImage imageNamed:@"VersionUpdate_Icon"];
//    [updateView addSubview:updateIcon];
    
    //版本号
    UILabel *versionLabel = [[UILabel alloc]initWithFrame:CGRectMake(Ratio(28), Ratio(10) + 140, updateView.frame.size.width, Ratio(28))];
    versionLabel.font = [UIFont systemFontOfSize:15];
    versionLabel.textColor = MainWhiteColor;
    versionLabel.textAlignment = NSTextAlignmentLeft;
    versionLabel.text = [NSString stringWithFormat:kLocalizedString(@"updateVersion%@"),self.version];
    [updateView addSubview:versionLabel];
    
    //title
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(Ratio(28), CGRectGetMaxY(versionLabel.frame) + 20, updateView.frame.size.width, Ratio(28))];
    title.font = [UIFont boldSystemFontOfSize:15];
    title.textColor = MainWhiteColor;
    title.textAlignment = NSTextAlignmentLeft;
    title.text = kLocalizedString(@"currentUpdate");
    [updateView addSubview:title];

    
    
    //更新内容
    UITextView *descTextView = [[UITextView alloc]initWithFrame:CGRectMake(Ratio(28), Ratio(10) + CGRectGetMaxY(title.frame), updateView.frame.size.width - Ratio(56), descHeight)];
    descTextView.backgroundColor = [UIColor clearColor];
    descTextView.font = [UIFont systemFontOfSize:SELDescriptionFont];
    descTextView.textContainer.lineFragmentPadding = 0;
    descTextView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    descTextView.backgroundColor = [UIColor clearColor];
    descTextView.text = self.desc;
    descTextView.editable = NO;
    descTextView.selectable = NO;
    descTextView.scrollEnabled = scrollEnabled;
    descTextView.showsVerticalScrollIndicator = scrollEnabled;
    descTextView.showsHorizontalScrollIndicator = NO;
    descTextView.textColor = MainWhiteColor;
    [updateView addSubview:descTextView];
    
    if (scrollEnabled) {
        //若显示滑动条，提示可以有滑动条
        [descTextView flashScrollIndicators];
    }
    
    UILabel *attentLb = [[UILabel alloc] init];
    attentLb.frame = CGRectMake(Ratio(28), CGRectGetMaxY(descTextView.frame) + Ratio(10), updateView.frame.size.width - Ratio(56), Ratio(15));
    attentLb.text = kLocalizedString(@"currentUpdateFotText");
    attentLb.adjustsFontSizeToFitWidth = YES;
    attentLb.textAlignment = 0;
    attentLb.textColor = HEXCOLOR(0x6B6B6B);
    attentLb.font = [UIFont systemFontOfSize:14];
    [updateView addSubview:attentLb];
    
    //更新按钮
    UIButton *updateButton = [UIButton buttonWithType:UIButtonTypeSystem];
    updateButton.titleLabel.font = [UIFont systemFontOfSize:18];
    updateButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [updateButton setTitle:kLocalizedString(@"rightUpdate") forState:UIControlStateNormal];
    CGSize titleSize = [kLocalizedString(@"rightUpdate") sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:updateButton.titleLabel.font.fontName size:updateButton.titleLabel.font.pointSize]}];
    CGFloat width = titleSize.width;
    CGFloat height = titleSize.height;
    updateButton.frame = CGRectMake(updateView.frame.size.width - Ratio(width), bgView.frame.size.height - 60, Ratio(width), Ratio(height));
    updateButton.clipsToBounds = YES;
    updateButton.layer.cornerRadius = 2.0f;
    [updateButton addTarget:self action:@selector(updateVersion) forControlEvents:UIControlEventTouchUpInside];
    [updateButton setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
    [updateView addSubview:updateButton];
    
    //取消按钮
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:18];
    cancelButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [cancelButton setTitle:kLocalizedString(@"laterUpdata") forState:UIControlStateNormal];
    CGSize titleSize2 = [kLocalizedString(@"laterUpdata") sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:cancelButton.titleLabel.font.fontName size:cancelButton.titleLabel.font.pointSize]}];
    CGFloat width2 = titleSize2.width;
    CGFloat height2= titleSize.height;

    cancelButton.frame = CGRectMake(updateView.frame.size.width - Ratio(width) - width2 - 20, bgView.frame.size.height - 60, Ratio(width2), Ratio(height2));
    cancelButton.clipsToBounds = YES;
    cancelButton.layer.cornerRadius = 2.0f;
    [cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitleColor:HEXCOLOR(0xDDDDDD) forState:UIControlStateNormal];
    [updateView addSubview:cancelButton];
    if (upate) {
        cancelButton.hidden = YES;
    } else {
        cancelButton.hidden = NO;
    }
    //显示更新
    [self showWithAlert:bgView];
}

/** 更新按钮点击事件 跳转AppStore更新 */
- (void)updateVersion {
//    NSString *str = [NSString stringWithFormat:@"http://itunes.apple.com/us/app/id%@", APP_ID];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
//    downloadUrl = [NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@",kVersion_plist];
    
    NSString *downloadUrl ;
    downloadUrl = [NSString stringWithFormat:@"%@",self.downUrl];
    NSURL *appurl = [NSURL URLWithString:downloadUrl];
    [[UIApplication sharedApplication] openURL:appurl];
    exit(0);
}

/** 取消按钮点击事件 */
- (void)cancelAction {
    [self dismissAlert];
}

/**
 添加Alert入场动画
 @param alert 添加动画的View
 */
- (void)showWithAlert:(UIView*)alert {
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = SELAnimationTimeInterval;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [alert.layer addAnimation:animation forKey:nil];
}


/** 添加Alert出场动画 */
- (void)dismissAlert {
    [UIView animateWithDuration:SELAnimationTimeInterval animations:^{
        self.transform = (CGAffineTransformMakeScale(1.5, 1.5));
        self.backgroundColor = [UIColor clearColor];
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    } ];
}

/**
 计算字符串高度
 @param string 字符串
 @param font 字体大小
 @param maxSize 最大Size
 @return 计算得到的Size
 */
- (CGSize)_sizeofString:(NSString *)string font:(UIFont *)font maxSize:(CGSize)maxSize {
    return [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;
}




@end
