//
//  GestureScreen.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "GestureScreen.h"
#import "GestureViewController.h"



@interface GestureScreen()<GestureViewDelegate>
{
    GestureViewController *_gestureVC;
}
@property (nonatomic, strong) UIWindow *window;

@end
@implementation GestureScreen

+ (instancetype)shared {
    
    static dispatch_once_t onceToken;
    static GestureScreen *share = nil;
    dispatch_once(&onceToken, ^{
        
        share = [[self alloc]init];
    });
    return share;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        _window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = UIWindowLevelStatusBar;
        
        UIViewController *vc = [[UIViewController alloc]init];
        _window.rootViewController = vc;
    }
    return self;
}

- (void)show {
    
    self.window.hidden = NO;
    [self.window makeKeyWindow];
    self.window.windowLevel = UIWindowLevelStatusBar;
    
    GestureViewController *viV = [[GestureViewController alloc]init];
    viV.delegate = self;
    [viV showInViewController:self.window.rootViewController type:LZGestureTypeScreen];
    _gestureVC = viV;
}

- (void)dismiss {
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        [self.window resignKeyWindow];
        self.window.windowLevel = UIWindowLevelNormal;
        self.window.hidden = YES;
    }];
}

- (void)dealloc {
    if (self.window) {
        self.window = nil;
    }
}

- (void)gestureViewVerifiedSuccess:(GestureViewController *)vc {
    [self performSelector:@selector(hide) withObject:nil afterDelay:0.6];
}

- (void)hide {
    [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
        [self.window resignKeyWindow];
        self.window.windowLevel = UIWindowLevelNormal;
        self.window.hidden = YES;
        if ([self.delegate respondsToSelector:@selector(dismiss)]) {
            [self.delegate dismMisVC];
        }
    }];
    
//    [self.window resignKeyWindow];
//    self.window.windowLevel = UIWindowLevelNormal;
//    self.window.hidden = YES;
}



@end
