//
//  SelectedViewController.m
//  Senbit
//
//  Created by 张玮 on 2020/3/5.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SelectedViewController.h"
#import "SelectedTableViewController.h"
#import "HomeCoinPairModel.h"


@interface SelectedViewController ()
@property (nonatomic, strong) NSMutableArray *musicCategories;
@property (nonatomic ,strong) UIView *headView;
@property (nonatomic ,strong) UIButton *backBtn;
@property (strong, nonatomic) XXWebQuoteModel *webModel;
@property (strong, nonatomic) NSMutableArray *totalVC;
@end

@implementation SelectedViewController
- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:KnotificationExchangeSelectedCoin object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [IWNotificationCenter addObserver:self
                             selector:@selector(backAction)
                                 name:KnotificationSelected
                               object:nil];

    self.view.backgroundColor = ThemeDarkBlack;
    [self setUpNav];
    [self webSocketHomeDatae];
    [KSystem statusBarSetUpDarkColor];
}

- (void)setUpNav {
    [self.view addSubview:self.headView];
    [self.headView addSubview:self.backBtn];
    [self make_Layout];
}
- (void)make_Layout {
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(kNavBarAndStatusBarHeight);
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.mas_equalTo(44);
        make.height.mas_equalTo(37);
    }];
}

- (void)webSocketHomeDatae {
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        NSMutableArray *titleArr = [HomeCoinPairModel mj_objectArrayWithKeyValuesArray:data];
        NSMutableArray *totoalArr = [NSMutableArray array];
        for (HomeCoinPairModel *model in titleArr) {
            [totoalArr addObject:[NSString stringWithFormat:@"%@",model.coinName]];
        }
        [totoalArr insertObject:kLocalizedString(@"home_selfSelected") atIndex:0];
//        for (int i = 0; i < totoalArr.count; i++) {
//            SelectedTableViewController *vc = [[SelectedTableViewController alloc] init];
//            [weakSelf.totalVC addObject:vc];
//        }
        weakSelf.musicCategories = totoalArr;
        weakSelf.titleFontName = @"ArialRoundedMTBold";
        weakSelf.menuViewStyle = WMMenuViewStyleLine;
        weakSelf.titleColorSelected = ThemeGreenColor;
        weakSelf.menuView.backgroundColor = ThemeDarkBlack;
        weakSelf.scrollView.backgroundColor = ThemeDarkBlack;
        weakSelf.menuView.scrollView.height = 40;
        weakSelf.menuView.lineColor = PlaceHolderColor;
        weakSelf.titleColorNormal = PlaceHolderColor;
        weakSelf.selectIndex = 0;
        weakSelf.menuItemWidth = 80;//[UIScreen mainScreen].bounds.size.width / self.musicCategories.count;
        [weakSelf reloadData];
    };
    NSDictionary *dic = @{};
    self.webModel.params = [NSMutableDictionary dictionary];
    self.webModel.params[@"topic"] = @"home-coins";
    self.webModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"home-coins"];
}



- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    self.titleFontName = @"ArialRoundedMTBold";
    return self.musicCategories.count;
}

- (void)pageController:(WMPageController *)pageController willEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    NSDictionary *dic = @{@"coinName":info[@"title"]};
//    [CoinSignle shareInstance].coin = info[@"title"];
    [NotificationManager tradePostSwitchTradeSymbolNotification:dic];
    
//    [IWNotificationCenter postNotificationName:KnotificationExchangeSelected object:self userInfo:dic];
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.musicCategories[index];
}
 

//#pragma mark 返回某个index对应的页面
- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    return [[SelectedTableViewController alloc] init];
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width + 20;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGFloat originY = self.showOnNavigationBar ? 0 : CGRectGetMaxY(self.navigationController.navigationBar.frame);
    return CGRectMake(leftMargin, CGRectGetMaxY(self.headView.frame), self.view.frame.size.width - 2*leftMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

- (void)backAction {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (UIView *)headView {
    if (!_headView) {
        _headView = [[UIView alloc] init];
        _headView.backgroundColor = ThemeDarkBlack;//ThemeDarkBlack;
    }
    return _headView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc] init];
        [_backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        _backBtn.userInteractionEnabled = YES;
        [_backBtn setImage:[UIImage imageNamed:@"leftBack"] forState:UIControlStateNormal];
    }
    return _backBtn;
}

- (NSMutableArray *)totalVC {
    if (!_totalVC) {
        _totalVC = [[NSMutableArray alloc] init];
    }
    return _totalVC;
}

@end
