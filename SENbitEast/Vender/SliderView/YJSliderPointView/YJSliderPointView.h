//
//  YJSliderPointView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/30.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YJSliderPointView : UIView
@property (strong, nonatomic) void(^SliderBlock)(NSString * fl);
- (void)SetupUI:(NSString *)first end:(NSString *)end;
- (void)creatUI;
@end

NS_ASSUME_NONNULL_END
