//
//  SearchCoinVC.m
//  Senbit
//
//  Created by 张玮 on 2020/2/20.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SearchCoinVC.h"
#import "SearchCoinCell.h"
#import "CoinModel.h"
#import "WithCoinListModel.h"
#import "PinYin4Objc.h"

@interface SearchCoinVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    NSMutableArray *dataSource;
    NSMutableArray *searchSource;
    NSMutableArray *updateArray;
    UITextField *_textField;
}
@property(nonatomic,strong) UITableView *friendTableView;
@property(nonatomic,strong) NSArray *lettersArray;
@property(nonatomic,strong) NSMutableDictionary *nameDic;
@property(nonatomic,strong) UIView *naView;

@end

@implementation SearchCoinVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
    [self setNav];
    [self setUpView];
    [self setupSearchBar];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
}

- (void)setupSearchBar {
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(20, KNavBarButtonHeight, 290, 30)];
    
    //设置圆角效果
    bgView.layer.cornerRadius = 14;
    bgView.layer.masksToBounds = YES;
    
    bgView.backgroundColor = HEXCOLOR(0xF7F6FB);

    //输入框
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, CGRectGetWidth(bgView.frame) - 10, CGRectGetHeight(bgView.frame))];
    _textField.font = [UIFont systemFontOfSize:13];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange) name:UITextFieldTextDidChangeNotification object:nil];

    //清除按钮
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    _textField.delegate = self;
    //键盘属性
    _textField.returnKeyType = UIReturnKeySearch;
    
    //为textField设置属性占位符
    _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kLocalizedString(@"deposit_quickDeposit") attributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}];
    
    _textField.textColor = HEXCOLOR(0x333333);
    
    [bgView addSubview:_textField];
    
    [self.naView addSubview:bgView];
    
    UIButton *cancleBtn = [[UIButton alloc] init];
    [cancleBtn setTitle:kLocalizedString(@"deposit_cancel") forState:UIControlStateNormal];
    cancleBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [cancleBtn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(clickRightUserBarButton) forControlEvents:UIControlEventTouchUpInside];
    [self.naView addSubview:cancleBtn];
    [cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(bgView);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(30);
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}

- (void)clickRightUserBarButton {
    NSLog(@"qu小");
    [_textField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeLanguage {
    [self setNavBarTitle:kLocalizedString(@"withDraw_coinName")];
}
- (void)setNav {
//    [self setNavBarTitle:kLocalizedString(@"withDraw_coinName")];
//    [self setLeftBtnImage:@"leftBack"];
    [self.view addSubview:self.naView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadAddressBookData];
}

- (void)setUpView {
    dataSource = [[NSMutableArray alloc]init];
    updateArray = [[NSMutableArray alloc]init];
    self.lettersArray = [[NSArray alloc]init];
    self.nameDic = [[NSMutableDictionary alloc]init];
    [self loadAddressBookData];
    [self.view addSubview:self.friendTableView];
    [self.friendTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(self.naView.mas_bottom);
        make.bottom.mas_equalTo(0);
//        make.edges.mas_equalTo(UIEdgeInsetsMake(self.navBar.mas_bottom, 0, 0, 0));
    }];
    self.definesPresentationContext = YES;
}

- (void)loadAddressBookData {
    [dataSource removeAllObjects];
    if (self.isWithDraw) { //提币
        [self.dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [dataSource addObject:[[WithCoinListModel alloc] initWithDic:self.dic[key]]];
        }];
    } else { //chongbi
        [self.dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [dataSource addObject:[[CoinModel alloc] initWithDic:self.dic[key]]];
        }];
    }
    [self handleLettersArray];
}


#pragma mark
#pragma mark tableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.lettersArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *nameArray = [self.nameDic objectForKey:self.lettersArray[section]];
    return nameArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchCoinCell *cell = [SearchCoinCell cellWithTableView:tableView];
    cell.isWithdraw = self.isWithDraw;
    if (self.isWithDraw == YES) { //ti
        WithCoinListModel *model = [[self.nameDic objectForKey:[self.lettersArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        cell.withDrawModel = model;
    } else { //chong
        CoinModel *frends = [[self.nameDic objectForKey:[self.lettersArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        cell.frendModel = frends;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.isWithDraw) {
        if ([self.delegate respondsToSelector:@selector(selectWithDrawCoin:)]) {
            WithCoinListModel *model = [[self.nameDic objectForKey:[self.lettersArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            if ([model.coinName isEqualToString:@"USDT"]) {
                model.moreChain = YES;
            }
            if (model.able2withdraw == NO) {
                return;
            }
            [self.delegate selectWithDrawCoin:model];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(selectCoin:)]) {
            CoinModel *model = [[self.nameDic objectForKey:[self.lettersArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            if (model.able2charge == NO) {
                return;
            }
            [self.delegate selectCoin:model];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    headerView.backgroundColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
    NSString *letterString =  self.lettersArray[section];
    UILabel *letterLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, headerView.frame.origin.y, headerView.frame.size.width - 10, headerView.frame.size.height)];
    letterLabel.textColor = [UIColor grayColor];
    letterLabel.font = [UIFont systemFontOfSize:14.f];
    letterLabel.text = letterString;
    [headerView addSubview:letterLabel];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.friendTableView) {
        return 20.0;
    }
    return CGFLOAT_MIN;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.lettersArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
        NSInteger count = 0;
        for(NSString *letter in self.lettersArray){
            if([letter isEqualToString:title]){
                return count;
            }
            count++;
        }
        return 0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.lettersArray;
}

- (void)textFieldTextDidChange {
    NSLog(@"%@",dataSource);
    searchSource = [[NSMutableArray alloc] init];
    if (_textField.text.length == 0) {
        [self handleLettersArray];
    } else {
        for (CoinModel *model in dataSource) {
            if ([model.coinName  containsString:[_textField.text uppercaseString]]) {
                [searchSource addObject:model];
            }
        }
        [self searchHandleLettersArray];
    }
}

- (void)searchHandleLettersArray {
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc]init];
    for(CoinModel *friends  in searchSource){
        HanyuPinyinOutputFormat *formatter =  [[HanyuPinyinOutputFormat alloc] init];
        formatter.caseType = CaseTypeLowercase;
        formatter.vCharType = VCharTypeWithV;
        formatter.toneType = ToneTypeWithoutTone;
        NSString *outputPinyin=[PinyinHelper toHanyuPinyinStringWithNSString:friends.coinName withHanyuPinyinOutputFormat:formatter withNSString:@""];
//        NSLog(@"%@",[[outputPinyin substringToIndex:1] uppercaseString]);
        [tempDic setObject:friends forKey:[[outputPinyin substringToIndex:1] uppercaseString]];
    }
    
    self.lettersArray = tempDic.allKeys;
    for (NSString *letter in self.lettersArray) {
        NSMutableArray *tempArry = [[NSMutableArray alloc] init];
        
        for (NSInteger i = 0; i < searchSource.count; i++) {
            CoinModel *friends = searchSource[i];
            HanyuPinyinOutputFormat *formatter =  [[HanyuPinyinOutputFormat alloc] init];
            formatter.caseType = CaseTypeUppercase;
            formatter.vCharType = VCharTypeWithV;
            formatter.toneType = ToneTypeWithoutTone;
            //把friend的userName汉子转为汉语拼音，比如：张玮---->zhangwei
            NSString *outputPinyin=[PinyinHelper toHanyuPinyinStringWithNSString:friends.coinName withHanyuPinyinOutputFormat:formatter withNSString:@""];
            if ([letter isEqualToString:[[outputPinyin substringToIndex:1] uppercaseString]]) {
                [tempArry addObject:friends];
            }
        }
        [self.nameDic setObject:tempArry forKey:letter];
    }
    
    self.lettersArray = tempDic.allKeys;
    //排序，排序的根据是字母
    NSComparator cmptr = ^(id obj1, id obj2){
        if ([obj1 characterAtIndex:0] > [obj2 characterAtIndex:0]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 characterAtIndex:0] < [obj2 characterAtIndex:0]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    self.lettersArray = [[NSMutableArray alloc]initWithArray:[self.lettersArray sortedArrayUsingComparator:cmptr]];
    [self.friendTableView reloadData];
}

//处理letterArray，包括按英文字母顺序排序
- (void)handleLettersArray{
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc]init];
    for(CoinModel *friends  in dataSource){
        HanyuPinyinOutputFormat *formatter =  [[HanyuPinyinOutputFormat alloc] init];
        formatter.caseType = CaseTypeLowercase;
        formatter.vCharType = VCharTypeWithV;
        formatter.toneType = ToneTypeWithoutTone;
        if (![friends.coinName isEqualToString:@"TUSDT"] && ![friends.coinName isEqualToString:@"EUSDT"]) {
            NSString *outputPinyin = [PinyinHelper toHanyuPinyinStringWithNSString:friends.coinName withHanyuPinyinOutputFormat:formatter withNSString:@""];
    //        NSLog(@"%@",[[outputPinyin substringToIndex:1] uppercaseString]);
            [tempDic setObject:friends forKey:[[outputPinyin substringToIndex:1] uppercaseString]];
        }
    }
    
    self.lettersArray = tempDic.allKeys;
    for (NSString *letter in self.lettersArray) {
        NSMutableArray *tempArry = [[NSMutableArray alloc] init];
        
        for (NSInteger i = 0; i < dataSource.count; i++) {
            CoinModel *friends = dataSource[i];
            HanyuPinyinOutputFormat *formatter =  [[HanyuPinyinOutputFormat alloc] init];
            formatter.caseType = CaseTypeUppercase;
            formatter.vCharType = VCharTypeWithV;
            formatter.toneType = ToneTypeWithoutTone;
            //把friend的userName汉子转为汉语拼音，比如：张玮---->zhangwei
            if (![friends.coinName isEqualToString:@"TUSDT"] && ![friends.coinName isEqualToString:@"EUSDT"]) {
                NSString *outputPinyin=[PinyinHelper toHanyuPinyinStringWithNSString:friends.coinName withHanyuPinyinOutputFormat:formatter withNSString:@""];
                if ([letter isEqualToString:[[outputPinyin substringToIndex:1] uppercaseString]]) {
                    [tempArry addObject:friends];
                }
            }
        }
        [self.nameDic setObject:tempArry forKey:letter];
    }
    
    self.lettersArray = tempDic.allKeys;
    //排序，排序的根据是字母
    NSComparator cmptr = ^(id obj1, id obj2){
        if ([obj1 characterAtIndex:0] > [obj2 characterAtIndex:0]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 characterAtIndex:0] < [obj2 characterAtIndex:0]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    self.lettersArray = [[NSMutableArray alloc]initWithArray:[self.lettersArray sortedArrayUsingComparator:cmptr]];
    [self.friendTableView reloadData];
}


- (UITableView *)friendTableView {
    if (_friendTableView == nil) {
        _friendTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _friendTableView.delegate = self;
        _friendTableView.dataSource = self;
        _friendTableView.rowHeight = 40.f;
        _friendTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _friendTableView.backgroundColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
        //设置右边索引index的字体颜色和背景颜色
        _friendTableView.sectionIndexColor = [UIColor darkGrayColor];
        _friendTableView.separatorColor = HEXCOLOR(0xF7F6FB);
        _friendTableView.sectionIndexBackgroundColor = [UIColor colorWithLightColorStr:@"F5F8FD" DarkColor:@"F5F8FD"];
        _friendTableView.tableFooterView = [UIView new];
        if (@available(iOS 11.0, *)) {
            _friendTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _friendTableView;
}

- (UIView *)naView {
    if (!_naView) {
        _naView = [[UIView alloc] init];
        _naView.frame = CGRectMake(0, 0, DScreenW, kNavBarAndStatusBarHeight);
        _naView.backgroundColor = [UIColor whiteColor];
    }
    return _naView;
}

@end
