//
//  BlandPhoneVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/9.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BlindEmailVC.h"
#import "TipsView.h"
#import "AsyncTaskButton.h"
#import "PhoneSheet.h"
#import "BlindEmailSheet.h"
#import "BlindPhoneSheet.h"



@interface BlindEmailVC ()<UITextFieldDelegate,AsyncTaskCaptchaButtonDelegate>

@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contenLb;

@property (nonatomic ,strong) UITextField *accountField;
@property (nonatomic ,strong) UILabel *accountLine;
@property (nonatomic ,strong) AsyncTaskButton *nextBtn;
@property (nonatomic ,strong) NSString *reNewToken;

@end

@implementation BlindEmailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navBar.backgroundColor = WhiteColor;
    [self setLeftBtnImage:@"leftBack"];
    [self configKeyBoardRespond];
    [self setUpView];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak BlindEmailVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountField, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.accountField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.accountField resignFirstResponder];
    return YES;
}


- (void)setUpView {
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contenLb];
    [self.view addSubview:self.accountField];
    [self.view addSubview:self.accountLine];
    [self.view addSubview:self.nextBtn];
    [self make_Layout];
}



- (void)make_Layout {
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(30);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(40);
    }];
    
    [self.contenLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(20);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(20);
    }];
    
    
    [self.accountField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(35);
        make.top.mas_equalTo(self.contenLb.mas_bottom).offset(100);
    }];
    
    [self.accountLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.accountField.mas_bottom).offset(5);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.accountLine.mas_bottom).offset(75);
        make.height.mas_equalTo(50);
    }];
}


- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.textAlignment = 0;
        _titleLb.text = kLocalizedString(@"blind_email");
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLb;
}

- (UILabel *)contenLb {
    if (!_contenLb) {
        _contenLb = [[UILabel alloc] init];
        _contenLb.textAlignment = 0;
        _contenLb.font = FourteenFontSize;
        _contenLb.textColor = HEXCOLOR(0x666666);
        _contenLb.text = kLocalizedString(@"blind_emailSafe");
        _contenLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contenLb;
}


- (UITextField *)accountField {
    if (!_accountField) {
        _accountField = [[UITextField alloc] init];
        _accountField.delegate = self;
        NSAttributedString *attrString1 = [[NSAttributedString alloc] initWithString:kLocalizedString(@"login_pleaseEmailPlace") attributes:@{NSForegroundColorAttributeName:PlaceHolderColor}];
        _accountField.attributedPlaceholder = attrString1;
        _accountField.secureTextEntry = NO;
        _accountField.textColor = MainWhiteColor;
        _accountField.font = FifteenFontSize;
        [_accountField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountField;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountField.text.length > 0) {
        self.nextBtn.backgroundColor = ThemeGreenColor;
        self.nextBtn.userInteractionEnabled = YES;
    } else {
        self.nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.nextBtn.userInteractionEnabled = NO;
    }
}

- (UILabel *)accountLine {
    if (!_accountLine) {
        _accountLine = [[UILabel alloc] init];
        _accountLine.backgroundColor = LineColor;
    }
    return _accountLine;
}

- (AsyncTaskButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [[AsyncTaskButton alloc] init];
        _nextBtn.delegate = self;
        [_nextBtn setTitle:kLocalizedString(@"blind_next") forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = TwentyFontSize;
        _nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _nextBtn.userInteractionEnabled = NO;
        _nextBtn.cornerRadius = 3;
    }
    return _nextBtn;
}

- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
    if (self.accountField.text.length == 0 ) {
        [self showToastView:kLocalizedString(@"blind_emailAccount")];
        return NO;
    }
    
    [TipsView showTipOnKeyWindow:kLocalizedString(@"login_safe")];
    return YES;
}


- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    [self.view endEditing:YES];
    if (status == NO) {
        
    }
    
    BOOL ieEmail = [NSString checkEmail:self.accountField.text];
     if (ieEmail == NO) {
        [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_emailnotFomart")];
        return;
    }

    
    if (status == YES) {
        BlindEmailSheet *sheet = [[BlindEmailSheet alloc] init];
        sheet.msgLbel.text = self.accountField.text;
        [sheet showFromView:self.view];
        sheet.clickBlock = ^(NSString *str) {
            [self requireNewtoken:str];
        };
    }
}

//校验验证码获取newtoken
- (void)requireNewtoken:(NSString *)str {
    NSDictionary *dic = @{@"type":@"NEW_EMAIL",
                          @"emailCode":str,
                          @"email":self.accountField.text
    };
    [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            NSLog(@"%@",responseObject);
            self.reNewToken = responseObject[@"data"][@"authToken"];
            [self showSheet];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

//谈验证
- (void)showSheet {
    UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
    BlindPhoneSheet *sheet = [[BlindPhoneSheet alloc] init];
    sheet.type = BlindTypeEmail;
    sheet.googleAuth = info.googleAuthEnable;
    sheet.emailAuth = info.emailAuthEnable;
    sheet.phoneAuth = info.phoneAuthEnable;
    sheet.emailTitleName.text = info.email;
    sheet.phoneLabel.text = info.phone;
    sheet.clickBlock = ^(NSString * _Nonnull phoneCode, NSString * _Nonnull emailCode, NSString * _Nonnull googleCode, NSString * _Nonnull phone, NSString * _Nonnull email, NSString * _Nonnull userToken) {
        if (phoneCode.length == 0) {
            phoneCode = @"";
        }
        if (emailCode.length == 0) {
            emailCode = @"";
        }
        if (googleCode.length == 0) {
            googleCode = @"";
        }
        if (email.length == 0) {
            email = @"";
        }
        if (phone.length == 0) {
            phone = @"";
        }
        NSDictionary *dic = @{@"email":info.email,
                              @"emailCode":emailCode,
                              @"phone":info.phone,
                              @"phoneCode":phoneCode,
                              @"googleCode":googleCode,
                              @"type":@"BIND_EMAIL"
        };
        [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            NSLog(@"%@",responseObject);
            if (SuccessCode == 200) {
                [self requieAuthToken:responseObject[@"data"][@"authToken"]];
            } else {
                [self showToastView:SuccessMessage];
            }
        } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
            [self showToastView:FailurMessage];
        }];
    };
    [sheet showFromView:self.view];
}

- (void)requieAuthToken:(NSString *)str {
    NSDictionary *dic = @{@"newEmail":self.accountField.text,
                          @"newToken":self.reNewToken,
                          @"authToken":str
    };
    [SBNetworTool postWithUrl:EastblindEmail params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
            info.emailAuthEnable = YES;
            info.email = self.accountField.text;
            [[CacheManager sharedMnager] saveUserInfo:info];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}



@end
