//
//  EmailSheet.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/13.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeBtn.h"


typedef void (^ClickBlock) (NSString* str);


NS_ASSUME_NONNULL_BEGIN

@interface BlindEmailSheet : UIView

/** 点击事件 */
@property (nonatomic, copy) ClickBlock clickBlock;

@property (nonatomic ,strong) UIButton *cancleBtn;

@property (nonatomic ,strong) UILabel *titleName;

@property (nonatomic ,strong) UILabel *line1;

@property (nonatomic ,strong) UILabel *msgLbel;

@property (nonatomic ,strong) UITextField *msgTF;

@property (nonatomic ,strong) UILabel *line2;

@property (nonatomic ,strong) UIButton *confirBtn;

@property (nonatomic ,strong) TimeBtn *msgBtn;

- (void)showFromView:(UIView *)view;

+ (instancetype)customActionSheets;

- (void)closeAcitonSheet;

@end

NS_ASSUME_NONNULL_END
