//
//  VersionCheck.h
//  Senbit
//
//  Created by 张玮 on 2020/6/19.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VersionCheck : NSObject {
    NSString *_dowlondUrl;
    BOOL _isAuth;
    NSString *_version;
    
    BOOL _isUpdate;
    BOOL _isMustUpdate;
}

@property(nonatomic) BOOL isMustUpdate;

+ (instancetype)sharedCheck;

- (void)checkVersion;

@end

NS_ASSUME_NONNULL_END
