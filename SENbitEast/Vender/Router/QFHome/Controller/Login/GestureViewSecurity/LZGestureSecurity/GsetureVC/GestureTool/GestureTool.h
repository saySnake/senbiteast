//
//  GestureTool.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GestureTool : NSObject

// 用户是否开启手势解锁
+ (void)saveGestureEnableByUser:(BOOL)isEnable;
+ (BOOL)isGestureEnableByUser;

// 保存 读取用户设置的密码
+ (void)saveGesturePsw:(NSString *)psw;
+ (NSString *)getGesturePsw;

+ (void)deleteGesturePsw;
+ (BOOL)isGesturePswEqualToSaved:(NSString *)psw;

+ (BOOL)isGestureEnable;
+ (BOOL)isGesturePswSavedByUser;

+ (void)saveGestureResetEnableByTouchID:(BOOL)enable;
+ (BOOL)isGestureResetEnableByTouchID;


//是否稍等
+ (void)saveDelayEnable:(BOOL)isDelay;
+ (BOOL)isDelayEnable;

@end

NS_ASSUME_NONNULL_END
