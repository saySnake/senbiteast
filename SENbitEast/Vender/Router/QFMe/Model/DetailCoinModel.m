//
//  DetailCoinModel.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/28.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "DetailCoinModel.h"

@implementation DetailCoinModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"id",
             @"withdrawStatus" :@"status"
             };
}

@end
