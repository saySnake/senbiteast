//
//  NSNotifiDefine.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/11.
//  Copyright © 2020 张玮. All rights reserved.
//

#ifndef NSNotifiDefine_h
#define NSNotifiDefine_h


//保存主题到本地的key
#define SThemeName @"SThemeName"

///通知的主题
#define SThemeDidChangeNotification @"SThemeDidChangeNotification"

//401跳登录
#define STJUMPLOGIN @"JumoLogin"

//通知刷新
#define KLoadLeftView @"KLoadLeftView"

#define KLoginNoti @"KLoginNoti"


#endif /* NSNotifiDefine_h */
