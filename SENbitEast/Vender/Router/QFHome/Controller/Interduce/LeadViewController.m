//
//  LeadViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "LeadViewController.h"
#import "StyledPageControl.h"
#import "SeriveAlert.h"

NSTimeInterval const kAnimationDuration = 1.0f;

@interface LeadViewController ()<UIScrollViewDelegate> {
    
    int kCount;
    StyledPageControl *_pageControl;
}

@end

@implementation LeadViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (id)initWithType:(SLinkLeadType)type {
    if (self = [super init]) {
        _type = type;
        kCount = 3;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = RGB(10, 27, 51);
    self.navigationController.navigationBar.hidden = YES;
    if (_type == SLinkLeadTypeStartAnimation) {
        [self runAnimationImga];
    }else {
        [self loadScrollView];
    }
}

#pragma mark - start animation
- (void)runAnimationImga {
//    [self getCoinPair];
    UIImageView *lastBgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    lastBgView.backgroundColor = WhiteColor;
    UIImageView *gifView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    gifView.contentMode = UIViewContentModeScaleAspectFill;
    lastBgView.contentMode = UIViewContentModeScaleAspectFill;

    gifView.userInteractionEnabled = YES;
    NSMutableArray *arrrays = [NSMutableArray array];
    for (int i = 1; i < 11; i++) {
        NSString *str = [NSString stringWithFormat:@"launch_image10.png"];
        [arrrays addObject:str];
    }
    lastBgView.image = [UIImage imageNamed:arrrays.lastObject];
    //图片数组
    NSMutableArray *pics =[NSMutableArray array];
    for (int i = 0; i < 10; i++) {
        UIImage *image = [UIImage imageNamed:[arrrays objectAtIndex:i]];
        [pics addObject:image];
    }
    gifView.animationImages = pics;
    gifView.animationDuration = kAnimationDuration;
    gifView.animationRepeatCount = 1;
    [gifView startAnimating];
    
    [self.view addSubview:lastBgView];
    [self.view addSubview:gifView];
    [self delay:kAnimationDuration task:^{
        
        #ifdef DEBUG
        [self alertActionClick];
        #else
        [[NSUserDefaults standardUserDefaults] setObject:@"https://turkey.senbit.tech" forKey:@"server"];
        [[NSUserDefaults standardUserDefaults] setObject:@"https://turkey.senbit.tech/" forKey:@"SenBitWSSUrl"];
        self.finishBlock();
        #endif
    }];
}




//- (void)listAnnounce {
//    NSDictionary *dic = @{@"lang":@"zh-CN"};
//    [SBNetworTool getWithUrl:SenHomeAnnounce params:dic isReadCache:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//        UserInfo *info = [[UserInfo alloc] initAnnounce:responseObject];
//        [[CacheManager sharedMnager] saveUserInfo:info];
//
//    } failed:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error, id  _Nonnull responseObject) {
//
//    }];
//}


- (void)alertActionClick {
    [SeriveAlert showAlertViewWith:self
                             title:@"选择网络环境"
                           message:@""
                     CallBackBlock:^(NSInteger btnIndex) {
        NSLog(@"%ld", btnIndex);
        if (btnIndex == 0) { //测试1 /http://106.14.227.102:81
//            [IWDefault setObject:@"http://192.168.123.59:4000" forKey:@"server"];
            [IWDefault setObject:@"http://106.14.227.102:86" forKey:@"server"];
            [IWDefault setObject:@"ws://192.168.123.59:4000" forKey:@"SenBitWSSUrl"];
            //                        @"secure": @YES,
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"secure"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else if (btnIndex == 1) { //测试2
//            [IWDefault setObject:@"http://192.168.123.59:4000" forKey:@"server"];
            [IWDefault setObject:@"http://106.14.227.102:86" forKey:@"server"];
            [IWDefault setObject:@"ws://192.168.123.59:4000" forKey:@"SenBitWSSUrl"];
            [IWDefault setBool:NO forKey:@"secure"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else if (btnIndex == 2) { //生产环境
            
            [[NSUserDefaults standardUserDefaults] setObject:@"https://turkey.senbit.tech" forKey:@"server"];
            [[NSUserDefaults standardUserDefaults] setObject:@"https://turkey.senbit.tech/" forKey:@"SenBitWSSUrl"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"secure"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];

        NSArray *_tmpArray = [NSArray arrayWithArray:[cookieJar cookies]];

        for (id obj in _tmpArray) {

        [cookieJar deleteCookie:obj];

        }

        self.finishBlock();
        
    } cancelButtonTitle:NULL
            destructiveButtonTitle:NULL
                 otherButtonTitles:
     @"测试环境1",
     @"测试环境2",
     @"生产环境",
     nil];
}


#pragma mark - 加载可滑动视图
- (void)loadScrollView {
    CGSize viewSize = self.view.bounds.size;
    int imgcount = kCount;
    //设置scrollView
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.frame = self.view.bounds;
    scrollView.backgroundColor = RGB(10, 27, 51);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(imgcount *viewSize.width, 0);
    scrollView.bounces = NO;
    scrollView.delegate = self;
    [self.view  addSubview:scrollView];
    
    //加载images
    for (int i = 0; i < imgcount; i++) {
        [self addImageViewAtIndex:i inView:scrollView];
    }
    //加载分页
    
    if (kCount > 1) {
        StyledPageControl *pageControl = [[StyledPageControl alloc] init];
        pageControl.center = CGPointMake(viewSize.width * 0.5, viewSize.height * 0.95);
        pageControl.bounds = CGRectMake(0, 0, self.view.width - 40, 20);
        pageControl.coreNormalColor = HEXCOLOR(0xC3D0F4);
        pageControl.coreSelectedColor = ThemeColorPage;
        pageControl.numberOfPages = imgcount;
        pageControl.gapWidth = 10;
        pageControl.diameter = 10;
        pageControl.enabled = NO;
        _pageControl = pageControl;
//        [self.view addSubview:_pageControl];
    }
}

#pragma mark 添加scrollview里面的imageview
- (void)addImageViewAtIndex:(int)index inView:(UIView *)view {
    CGSize viewSize = self.view.frame.size;
    //初始化imageView
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
//    imageView.frame = (CGRect){{index * viewSize.width, 0} , viewSize};
    imageView.frame = (CGRect){{index * DScreenW, 0} , viewSize};
//    imageView.frame = CGRectMake(0, 0, index *DScreenW, DScreenH);
    //设置图片
    NSString *name;
    if (DScreenH < 568) {
        name = [NSString stringWithFormat:@"common_h%d",index + 1];
    } else  {
        name = [NSString stringWithFormat:@"common_h%d", index + 1];
        NSLog(@"%@",name);
        imageView.image = [UIImage imageNamed:name];
        [view addSubview:imageView];
    }
    if (index == kCount - 1) {
        imageView.userInteractionEnabled = YES;
        UIButton *jumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        jumpBtn.backgroundColor = HEXCOLOR(0x535DCC);
        [jumpBtn setTitle:kLocalizedString(@"lead_experience") forState:UIControlStateNormal];
        jumpBtn.cornerRadius = 3;
        [jumpBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        jumpBtn.titleLabel.font = BOLDSYSTEMFONT(16);
        jumpBtn.center = CGPointMake(self.view.width / 2, self.view.height * 0.85);
        jumpBtn.bounds = CGRectMake(0, 0, 170, 44);
        [jumpBtn addTarget:self action:@selector(jumpOutLeadView:) forControlEvents:UIControlEventTouchUpInside];
        [imageView addSubview:jumpBtn];
    }
}

- (void)jumpOutLeadView:(UIButton *)sender {
    //btn的父类(UIImageView)
    UIImageView *view = (UIImageView *)sender.superview;
    view.image = nil;
    view.backgroundColor = RGB(10, 27, 51);
    //动画
    [UIView animateWithDuration:0.5 animations:^{
        //放大
        view.transform = CGAffineTransformMakeScale(2.0, 2.0);
        //View透明
        view.alpha = 0;
        //按钮透明
        _pageControl.alpha = 0;
        
    } completion:^(BOOL finished) {
        self.finishBlock();
        [self.view removeFromSuperview];
    }];
}

#pragma mark- scroolView代理方法
//滑动结束、减速完毕就会调用（scrollview静止）
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
}


@end
