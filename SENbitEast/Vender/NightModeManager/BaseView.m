//
//  BaseView.m
//  Senbit
//
//  Created by 张玮 on 2020/1/8.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseView.h"
#import "ThemeManager.h"
@implementation BaseView

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SThemeDidChangeNotification object:nil];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
    }
    return self;
}


- (void)setColorName:(NSString *)colorName {
    _colorName = colorName;
    UIColor *viewColor = [[ThemeManager shareInstance] getColorWithName:colorName];
    [self setBackgroundColor:viewColor];
}


- (void)themeNotification:(NSNotification *)notification {
    [self loadThemeImage];
}

- (void)loadThemeImage {
    if (self.colorName) {
        [self setColorName:self.colorName];
    }
}
@end
