//
//  VersionCheck.m
//  Senbit
//
//  Created by 张玮 on 2020/6/19.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "VersionCheck.h"
#import "SELUpdateAlert.h"
#import "CoinSignle.h"

//应用发布地址的程序plist文件
static NSString *kVersion_plist = @"https://senbit.com/api/v1/s3-download/Senbit_prod_3.0.14.plist";

@implementation VersionCheck
+ (instancetype)sharedCheck {
    static VersionCheck *shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[VersionCheck alloc] init];
    });
    return shared;
}

- (void)checkUpdateInfo {
    NSString *localVersion = [self getLocalVersion];
    [SBNetworTool getWithUrl:EastVersion params:@{@"type":@"ios"} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (SuccessCode == 200) {
                _isMustUpdate = [responseObject[@"data"][@"isForcibly"] boolValue];
                NSString *st = responseObject[@"data"][@"text"];
                _version = responseObject[@"data"][@"versionName"];
                _dowlondUrl = responseObject[@"data"][@"url"];
                if (_isMustUpdate) {
                    if (![localVersion isEqualToString:responseObject[@"data"][@"versionName"]]) {
                        NSString *a = [localVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
                        NSString *b = [responseObject[@"data"][@"versionName"] stringByReplacingOccurrencesOfString:@"." withString:@""];
                        if ([a intValue] < [b intValue]) {
                            [SELUpdateAlert showUpdateAlertWithVersion:_version Descriptions:@[st] isDoUpdate:YES url:_dowlondUrl];
                        }
                    }
                } else {
                    if (_version.length == 0) {
                        return;
                    }
                    if (![localVersion isEqualToString:responseObject[@"data"][@"versionName"]]) {
                        NSString *a = [localVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
                        NSString *b = [responseObject[@"data"][@"versionName"] stringByReplacingOccurrencesOfString:@"." withString:@""];
                        if ([a intValue] < [b intValue]) {
                            [CoinSignle shareInstance].isUpdate = YES;
                             [SELUpdateAlert showUpdateAlertWithVersion:_version Descriptions:@[st] isDoUpdate:NO url:_dowlondUrl];
                        } else {
                            [CoinSignle shareInstance].isUpdate = NO;
                        }
                    }
                }
            }
        });
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
    }];
}

- (void)checkVersion{
    [self checkUpdateInfo];
}

- (NSString *)getLocalVersion {
    NSDictionary * infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString * versionNum =[infoDict objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"iOS本地版本号:%@",versionNum);
    return versionNum;
}

- (void)doUpdate {
    NSString *downloadUrl ;
    downloadUrl = [NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@",kVersion_plist];
    NSURL *appurl = [NSURL URLWithString:downloadUrl];
    [[UIApplication sharedApplication] openURL:appurl];
    exit(0);
}

@end
