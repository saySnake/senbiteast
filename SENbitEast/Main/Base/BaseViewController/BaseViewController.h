//
//  BaseViewController.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/11.
//  Copyright © 2020 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustNavBar.h"
#import "FSCustomButton.h"
#import "ZYKeyboardUtil.h"

NS_ASSUME_NONNULL_BEGIN

/**
 模态弹出控制器时的标志
 */
extern void *const isModelPresent;

/**
 导航栏按钮样式
 */
typedef NS_ENUM(NSInteger, NavBarBtnStyle) {
    NavBarBtnStyleDone,
    NavBarBtnStyleEdit,
    NavBarBtnStyleSave,
    NavBarBtnStyleCancle,
    NavBarBtnStyleHistory,
    NavBarBtnStyleGuidList,
    NavBarBtnStylePlus
};

@interface BaseViewController : UIViewController

/**
导航栏
*/
@property(nonatomic ,strong) CustNavBar *navBar;
/**
 导航栏右边按钮
 */
@property(nonatomic,strong,readonly) FSCustomButton *rightBtn;
/**
 导航栏左边按钮
 */
@property(nonatomic,strong,readonly) FSCustomButton *leftBtn;

/**
 导航栏左边按钮的hidden
 */
@property (nonatomic) BOOL leftBackItemHiden;
/**
 初始化一个导航栏
 */
- (void)setNavBarVisible:(NSString *)title;
/**
 给已有导航栏设置右边按钮的图片
 */

- (void)setRightBtnImage:(NSString *)image;
/**
 给已有导航栏设置右边按钮的样式
 目前自定义了集中常用的
 如果没有则自行扩展
 */
- (void)setRightBtnStyle:(NavBarBtnStyle)style;

/**
 给已有导航栏设置左边按钮的图片
 */
- (void)setLeftBtnImage:(NSString *)image;
/**
 给已有导航栏设置左边按钮的样式
 目前自定义了集中常用的
 如果没有则自行扩展
 */
- (void)setLeftBtnStyle:(NavBarBtnStyle)style;

/**
 给已有的导航栏设置标题
 */
- (void)setNavBarLeftTitle:(NSString *)title;

- (void)setNavBarTitle:(NSString *)title;

- (void)setNavRigTitle:(NSString *)title;

/**
 自定义导航栏返回和最右边按钮点击事件
 如果保持pop或dismiss 需要在重写时调用super
 */
- (void)navBarButtonClick:(NSInteger )idx;
/**
 自定义导航栏标题点击事件
 */
- (void)navBarTitleClick:(UIButton *)button;

/**
 当没有数据时显示在空白页面上的标签
 */
- (void)showNotData:(NSString *)label;

- (void)showNotDataView:(UIView *)view lab:(NSString *)label;

/**
 移除没有数据的标签
 */
- (void)removeNotData;
/**
 *  设置中心
 *
 *  @param custView custView
 */
- (void)setCustCenterView:(UIView *)custView;
/*!
 * 隐藏多余间隔线 @lxa
 */
- (void)hideOtherCellLine:(UITableView *)tableView;

/**
 显示一个用户没有审核过自定义YJAlertView
 */
- (void)showUnAuthAlert;

/// 展示hud
- (void)show;

/// 隐藏hud
- (void)dismiss;

@property(nonatomic, strong)UITableView *tableView;

@property(nonatomic, strong)NSMutableArray *dataSource;

- (void)registerCellWithNib:(NSString *)nibName tableView:(UITableView *)tableView;

- (void)registerCellWithClass:(NSString *)className tableView:(UITableView *)tableView;

- (int)getRandomNumber:(int)from to:(int)to;

/*** 提示弹窗 **/
- (void)showInfo:(NSString*)str;

@property (nonatomic, strong) ZYKeyboardUtil *keyboardUtil;

- (void)hideKeyboard;




/** 导航栏视图 */
@property (strong, nonatomic) UIView *navView;

/** 标题 */
@property (strong, nonatomic) UILabel *titleLabel;

/** 左侧返回按钮 */
@property (strong, nonatomic) UIButton *leftButton;

/** 右侧按钮 */
@property (strong, nonatomic) UIButton *rightButton;

/** 导航栏分割线 */
@property (strong, nonatomic) UIView *navLineView;

/**
 0. 创建导航栏
 */
- (void)createNavigation;

/**
 1. 刷新导航栏样式
 */
- (void)reloadNavigationStyle;

/**
 3. 左侧按钮点击事件
 */
- (void)leftButtonClick:(UIButton *)sender;

/**
 3. 右侧按钮点击事件
 */
- (void)rightButtonClick:(UIButton *)sender;

/**
 4. 接收来网通知
 */
- (void)comeNetNotification;

/**
5. 后台进入前台
*/
- (void)didBecomeActive;

/**
6. 前台台进入后
*/
- (void)didEnterBackground;


@end

NS_ASSUME_NONNULL_END
