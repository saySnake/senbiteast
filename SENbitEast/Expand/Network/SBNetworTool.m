//
//  SBNetworTool.m
//  Senbit
//
//  Created by 张玮 on 2019/12/23.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "SBNetworTool.h"
#import "SBCache.h"
#import "LoginViewController.h"
#import "LeadViewController.h"
@implementation SBNetworTool

//https验证
+ (AFSecurityPolicy*)customSecurityPolicy {
    // /先导入证书
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:certificate ofType:@"cer"];//证书的路径
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    // AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    
    // allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    // 如果是需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = NO;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
    //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
    //如置为NO，建议自己添加对应域名的校验逻辑。
    securityPolicy.validatesDomainName = YES;
    securityPolicy.pinnedCertificates = [NSSet setWithArray:@[certData]];
    return securityPolicy;
}


//单例
+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static SBNetworTool *instance;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:@""];
        instance = [[SBNetworTool alloc] initWithBaseURL:baseUrl];
        // https ssl 验证。
            if(openHttpsSSL) {
                [self customSecurityPolicy];
            }
        instance.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                                   @"application/xml",
                                                                                   @"text/html",
                                                                                   @"text/json",
                                                                                   @"text/plain",
                                                                                   @"text/javascript",
                                                                                   @"text/xml",
                                                                                   @"image/*"]];
//        instance.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/plain",@"text/html",@"image/*",@"text/javascript",@"application/rss+xml",nil];
//        instance.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];

        [instance.requestSerializer setValue:@"" forHTTPHeaderField:DRLINKCUBE_TOKEN];
    });
    return instance;
}

- (NSURLSession *)downloadSession {
    if (_downloadSession == nil) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        // nil : nil的效果跟 [[NSOperationQueue alloc] init] 是一样的
        _downloadSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    return _downloadSession;
}


- (void)cookie:(NSString *)url {

}

//get请求
+ (void)getWithUrl: (NSString *)url params: (NSDictionary *)params isReadCache: (BOOL)isReadCache success: (responseSuccess)success failed: (responseFailed)failed  {
    [self removeCookie:url];
    NSString * cook = [IWDefault objectForKey:Cookie];
    if (!cook) {
        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
        [IWDefault setObject:cookie forKey:Cookie];
        cook = [IWDefault objectForKey:Cookie];
    }
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];
    [[SBNetworTool sharedManager].requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];

    //设置http的header的cookie
    [SBNetworTool sharedManager].responseSerializer = [AFJSONResponseSerializer serializer];
    [[SBNetworTool sharedManager].requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    [[SBNetworTool sharedManager] GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //请求成功的回调
        if (success) {
            NSString *url1 = [NSString stringWithFormat:@"%@",EastPing];
            
            if (SuccessCode == 401 && ![url1 isEqualToString:url]) {
                [[CacheManager sharedMnager] clearCache];
                [IWNotificationCenter postNotificationName:STJUMPLOGIN object:self userInfo:nil];
                return ;
            }
            success(task,responseObject);
        }
        //请求成功,保存数据
        [SBCache saveDataCache:responseObject forKey:url];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary * serializedData;
        if (errorData) {
            serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            NSLog(@"error--%@",serializedData);
        }

        //请求失败的回调
        
        id cacheData = nil;
        NSString *url1 = [NSString stringWithFormat:@"%@",EastPing];
//        NSString *url2 = [NSString stringWithFormat:@"%@",SenPairCollection];
//        NSString *url3 = [NSString stringWithFormat:@"%@",SenCurrenOrdersList];
//        NSString *url4 = [NSString stringWithFormat:@"%@",SenContractList];
//        NSString *url5 = [NSString stringWithFormat:@"%@",SenLoginUserInfo];
//        NSString *url6 = [NSString stringWithFormat:@"%@",SenPlanMorelist];
//        NSString *url7 = [NSString stringWithFormat:@"%@",SenContractList];
//        NSString *url8 = [NSString stringWithFormat:@"%@",SenAssetss];
//        NSString *url9 = [NSString stringWithFormat:@"%@",SenCollectionCoin];
//        NSString *url10 = [NSString stringWithFormat:@"%@",SenContractAssets];
//        NSString *url11 = [NSString stringWithFormat:@"%@",SenInvoteTop];
//        NSString *url12 = [NSString stringWithFormat:@"%@",SenInvotedPerson];
//        NSString *url13 = [NSString stringWithFormat:@"%@",SenMyInvotelink];
        
        NSHTTPURLResponse * pose = (NSHTTPURLResponse *)task.response;
        if (pose.statusCode == 401 &&
            ![url isEqualToString:url1]
//            ![url isEqualToString:url2] &&
//            ![url isEqualToString:url3] &&
//            ![url isEqualToString:url4] &&
//            ![url isEqualToString:url5] &&
//            ![url isEqualToString:url6] &&
//            ![url isEqualToString:url7] &&
//            ![url isEqualToString:url8] &&
//            ![url isEqualToString:url9] &&
//            ![url isEqualToString:url10] &&
//            ![url isEqualToString:url11] &&
//            ![url isEqualToString:url12]
            ) {
//            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
//            info.login = NO;
//            [IWDefault removeObjectForKey:Cookie];
//            [IWNotificationCenter postNotificationName:STJUMPLOGIN object:self userInfo:nil];
            return ;
        }
        

        //是否读取缓存
        if (isReadCache) {
             cacheData = [SBCache readCache:url];
        }else {
            cacheData = nil;
        }
        
        if (failed) {
             failed(task,serializedData,cacheData);
        }
    }];
}

+ (void)putWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(id error))failure {
    [self removeCookie:url];
    NSString * cook = [IWDefault objectForKey:Cookie];
    if (!cook) {
        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
        [IWDefault setObject:cookie forKey:Cookie];
        cook = [IWDefault objectForKey:Cookie];
    }
//    NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
//    NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
//    NSString *cookie = [cookieDict objectForKey:@"Cookie"];
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];
    [manager.requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary * serializedData;
        if (errorData) {
            serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            NSLog(@"error--%@",serializedData);
        }
        if (failure) {
            failure(serializedData);
        }
    }];
}

//patch
+ (void)patchWithUrl:(NSString *)url params: (NSDictionary *)params isReadCache: (BOOL)isReadCache success: (responseSuccess)success failed: (responseFailed)failed {
    [self removeCookie:url];
    NSString * cook = [IWDefault objectForKey:Cookie];
    if (!cook) {
        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
        [IWDefault setObject:cookie forKey:Cookie];
        cook = [IWDefault objectForKey:Cookie];
    }
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];
    [[SBNetworTool sharedManager].requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    [SBNetworTool sharedManager].responseSerializer = [AFJSONResponseSerializer serializer];
    [SBNetworTool sharedManager].responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                               @"application/xml",
                                                                               @"text/html",
                                                                               @"text/json",
                                                                               @"text/plain",
                                                                               @"text/javascript",
                                                                               @"text/xml",
                                                                               @"image/*"]];
    [[SBNetworTool sharedManager].requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];
    [[SBNetworTool sharedManager] PATCH:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *fields = ((NSHTTPURLResponse*)task.response).allHeaderFields;
        NSURL *urls = [NSURL URLWithString:url];
        NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:fields forURL:urls];
        for (NSHTTPCookie *cookie in cookies) {
            NSLog(@"cookie,name:= %@,valuie = %@",cookie.name,cookie.value);
            if ([cookie.name isEqualToString:@"market-token"]) {
                [IWDefault setObject:[NSString stringWithFormat:@"%@=%@",cookie.name,cookie.value] forKey:Cookie];
            }
        }
        if (success) {
            success(task,responseObject);
        }
        //请求成功,保存数据
        [SBCache saveDataCache:responseObject forKey:url];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        id cacheData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary * serializedData;
        if (errorData) {
            serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            NSLog(@"error--%@",serializedData);
        }
        
        NSHTTPURLResponse * pose = (NSHTTPURLResponse *)task.response;
        if (pose.statusCode == 401) {
        [IWNotificationCenter postNotificationName:STJUMPLOGIN object:self userInfo:nil];
            return ;
        }
        //是否读取缓存
        if (isReadCache) {
            cacheData = [SBCache readCache:url];
        }else {
            cacheData = nil;
        }
        if (failed) {
            failed(task,serializedData,cacheData);
        }
    }];
}

//返回值不是json样式
+ (void)postNojsonWithUrl:(NSString *)url params:(NSDictionary *)params isReadCache: (BOOL)isReadCache success:(responseSuccess)success failed:(responseFailed)failed {
    [self removeCookie:url];
    NSString * cook = [IWDefault objectForKey:Cookie];
    if (!cook) {
        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
        [IWDefault setObject:cookie forKey:Cookie];
        cook = [IWDefault objectForKey:Cookie];
    }
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];
    [[SBNetworTool sharedManager].requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    
    [SBNetworTool sharedManager].responseSerializer = AFHTTPResponseSerializer.serializer; //AFHTTPRequestSerializer：第一种是普通的http的编码格式也就是mid=10&method=userInfo&dateInt=20160818，这种格式的。
    [SBNetworTool sharedManager].responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                               @"application/xml",
                                                                               @"text/html",
                                                                               @"text/json",
                                                                               @"text/plain",
                                                                               @"text/javascript",
                                                                               @"text/xml",
                                                                               @"image/*"]];
    [[SBNetworTool sharedManager].requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];
    [[SBNetworTool sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(task,responseObject);
        }
        //请求成功,保存数据
        [SBCache saveDataCache:responseObject forKey:url];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * error) {
        id cacheData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary * serializedData;
        if (errorData) {
            serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            NSLog(@"error--%@",serializedData);
        }
        NSHTTPURLResponse * pose = (NSHTTPURLResponse *)task.response;
        if (pose.statusCode == 401) {
        [IWNotificationCenter postNotificationName:STJUMPLOGIN object:self userInfo:nil];
            return ;
        }
        //是否读取缓存
        if (isReadCache) {
            cacheData = [SBCache readCache:url];
        }else {
            cacheData = nil;
        }
        if (failed) {
            failed(task,serializedData,cacheData);
        }
    }];

}

//post object请求
+ (void)postObjectWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(id error))failure  {
    [self removeCookie:url];
    NSString * cook = [IWDefault objectForKey:Cookie];
    if (!cook) {
        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
        [IWDefault setObject:cookie forKey:Cookie];
        cook = [IWDefault objectForKey:Cookie];
    }
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //申明请求的数据是json类型 {“mid”:“11”,“method”:“userInfo”,“dateInt”:“20160818”}
    [manager.requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];
    [manager.requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"]; 
    [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            NSDictionary * serializedData;
            if (errorData) {
                serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSLog(@"error--%@",serializedData);
            }
            failure(serializedData);
        }
    }];
}



//post请求
+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params isReadCache: (BOOL)isReadCache success:(responseSuccess)success failed:(responseFailed)failed {
    [self removeCookie:url];
    
    
    NSString * cook = [IWDefault objectForKey:Cookie];
//    if (!cook) {
//        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
//        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
//        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
//        [IWDefault setObject:cookie forKey:Cookie];
//        cook = [IWDefault objectForKey:Cookie];
//    }
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];
    [[SBNetworTool sharedManager].requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    
    [SBNetworTool sharedManager].responseSerializer = [AFJSONResponseSerializer serializer];
    [SBNetworTool sharedManager].responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                               @"application/xml",
                                                                               @"text/html",
                                                                               @"text/json",
                                                                               @"text/plain",
                                                                               @"text/javascript",
                                                                               @"text/xml",
                                                                               @"image/*"]];
    [[SBNetworTool sharedManager].requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];
    [[SBNetworTool sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *fields = ((NSHTTPURLResponse*)task.response).allHeaderFields;
        NSURL *urls = [NSURL URLWithString:url];
        NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:fields forURL:urls];
        for (NSHTTPCookie *cookie in cookies) {
            NSLog(@"cookie,name:= %@,valuie = %@",cookie.name,cookie.value);
            if ([cookie.name isEqualToString:@"market-token"]) {
                [IWDefault setObject:[NSString stringWithFormat:@"%@=%@",cookie.name,cookie.value] forKey:Cookie];
            }
        }
        if (success) {
            if (SuccessCode == 401) {
                [IWNotificationCenter postNotificationName:STJUMPLOGIN object:self userInfo:nil];
                return ;
            }
            success(task,responseObject);
        }
        //请求成功,保存数据
        [SBCache saveDataCache:responseObject forKey:url];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *  error) {
        id cacheData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary * serializedData;
        if (errorData) {
            serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            NSLog(@"error--%@",serializedData);
        }
        
        NSHTTPURLResponse * pose = (NSHTTPURLResponse *)task.response;
        if (pose.statusCode == 401) {
        [IWNotificationCenter postNotificationName:STJUMPLOGIN object:self userInfo:nil];
            return ;
        }
        //是否读取缓存
        if (isReadCache) {
            cacheData = [SBCache readCache:url];
        }else {
            cacheData = nil;
        }
        if (failed) {
            failed(task,serializedData,cacheData);
        }
    }];
}


+ (void)deleteWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(id error))failure  {
    [self removeCookie:url];
    NSString * cook = [IWDefault objectForKey:Cookie];
    if (!cook) {
        NSArray *cookiesArray = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary *cookieDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookiesArray];
        NSString *cookie = [cookieDict objectForKey:@"Cookie"];
        [IWDefault setObject:cookie forKey:Cookie];
        cook = [IWDefault objectForKey:Cookie];
    }

    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:cook forHTTPHeaderField:DRLINK_AUTH_TOKEN];
    [manager.requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];
    [manager.requestSerializer setValue:kLanguageManager.currentLanguage forHTTPHeaderField:@"Accept-Language"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer.timeoutInterval = 30.0f;//超时时间
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", nil];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager DELETE:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            NSDictionary * serializedData;
            if (errorData) {
                serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSLog(@"error--%@",serializedData);
            }

            failure(serializedData);
        }
    }];
}

//文件上传

+ (void)uploadWithUrl: (NSString *)url params: (NSDictionary *)params fileData: (NSData *)fileData name: (NSString *)name fileName: (NSString *)fileName mimeType: (NSString *)mimeType progress: (progress)progress success: (responseSuccess)success failed: (responseFailed)failed {
    NSString *tradeApp =  [IWDefault objectForKey:TradeApp];
    [[SBNetworTool sharedManager].requestSerializer setValue:tradeApp forHTTPHeaderField:Agent];

    [SBNetworTool sharedManager].requestSerializer = [AFJSONRequestSerializer serializer];
    [SBNetworTool sharedManager].responseSerializer = [AFHTTPResponseSerializer serializer];
    [SBNetworTool sharedManager].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[SBNetworTool sharedManager].requestSerializer setValue:@"application/octet-stream" forHTTPHeaderField:@"Content-Type"];
    [[SBNetworTool sharedManager] POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:fileData name:name fileName:fileName mimeType:mimeType];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(task,responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failed) {
            failed(task,error,nil);
        }
    }];
}

//文件下载 支持断点下载
+ (void)downloadWithUrl: (NSString *)url {
    // 1. URL
    NSURL *URL = [NSURL URLWithString:url];
    
    // 2. 发起下载任务
    [SBNetworTool sharedManager].downloadTask = [[SBNetworTool sharedManager].downloadSession downloadTaskWithURL:URL];
    
    // 3. 启动下载任务
    [[SBNetworTool sharedManager].downloadTask resume];
}



//暂停下载
- (void)pauseDownload {
    [self.downloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
        self.resumeData = resumeData;
        //将已经下载的数据存到沙盒,下次APP重启后也可以继续下载
        NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        // 拼接文件路径   上面获取的文件路径加上文件名
        NSString *path = [@"sssssaad" stringByAppendingString:@".plist"];
        NSString *plistPath = [doc stringByAppendingPathComponent:path];
        self.resumeDataPath = plistPath;
        [resumeData writeToFile:plistPath atomically:YES];
        self.resumeData = resumeData;
        self.downloadTask = nil;
    }];
}

//继续下载
- (void)resumeDownloadprogress: (progress)progress success: (downloadSuccess)success failed: (downloadFailed)failed  {
    if (self.resumeData == nil) {
        NSData *resume_data = [NSData dataWithContentsOfFile:self.resumeDataPath];
        if (resume_data == nil) {
            // 即没有内存续传数据,也没有沙盒续传数据,就续传了
            return;
        } else {
            // 当沙盒有续传数据时,在内存中保存一份
            self.resumeData = resume_data;
        }
    }
    
    // 续传数据时,依然不能使用回调
    // 续传数据时起始新发起了一个下载任务,因为cancel方法是把之前的下载任务干掉了 (类似于NSURLConnection的cancel)
    // resumeData : 当新建续传数据时,resumeData不能为空,一旦为空,就崩溃
    // downloadTaskWithResumeData :已经把Range封装进去了
    
    if (self.resumeData != nil) {
        self.downloadTask = [self.downloadSession downloadTaskWithResumeData:self.resumeData];
        // 重新发起续传任务时,也要手动的启动任务
        [self.downloadTask resume];
    }
}

#pragma NSURLSessionDownloadDelegate
    
    /// 监听文件下载进度的代理方法
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
        // 计算进度
        float downloadProgress = (float)totalBytesWritten / totalBytesExpectedToWrite;
        NSLog(@"%f",downloadProgress);
}
    
/// 文件下载结束时的代理方法 (必须实现的)
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
    // location : 文件下载结束之后的缓存路径
    // 使用session实现文件下载时,文件下载结束之后,默认会删除,所以文件下载结束之后,需要我们手动的保存一份
    NSLog(@"%@",location.path);
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
   // NSString *path = @"/Users/allenjzl/Desktop/ssssss/zzzz.zip";
    // 文件下载结束之后,需要立即把文件拷贝到一个不会销毁的地方
    [[NSFileManager defaultManager] copyItemAtPath:location.path toPath:[path stringByAppendingString:@"/.zzzzzzz.zip"] error:NULL];
    NSLog(@"%@",path);
}


+ (void)removeCookie:(NSString *)url {
//    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:url]];
//    for (int i = 0; i < [cookies count]; i++) {
//        NSHTTPCookie *cookie = (NSHTTPCookie *)[cookies objectAtIndex:i];
//        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
//    }
}

    

@end
