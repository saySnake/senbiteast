//
//  SelectCoinView.m
//  Senbit
//
//  Created by 张玮 on 2020/2/21.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SelectCoinView.h"

@implementation SelectCoinView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setUpview];
    }
    return self;
}

- (void)setUpview {
    self.coinImg = [[UIImageView alloc] init];
    self.coinImg.userInteractionEnabled  = YES;
    self.coinImg.cornerRadius = 12.5;
    [self addSubview:self.coinImg];
    
    self.coinName = [[UIButton alloc] init];
    self.coinName.titleLabel.font = BOLDSYSTEMFONT(14.5);
    self.coinName.titleLabel.textAlignment = 0;
    [self.coinName setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    self.coinName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.coinName addTarget:self action:@selector(selectedCoinName:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.coinName];
    
    self.selectName = [[UILabel alloc] init];
    self.selectName.text = kLocalizedString(@"deposit_selecedCoin");
    self.selectName.textColor = HEXCOLOR(0x999999);
    self.selectName.font = ThirteenFontSize;
    self.selectName.textAlignment = 2;
    self.selectName.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.selectName];
    
    self.rigImg = [[UIImageView alloc] init];
    self.rigImg.image = [UIImage imageNamed:@"fanhui"];
    [self addSubview:self.rigImg];
    
    [self make_layout];
}
- (void)make_layout {
    [self.coinImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(5);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(25);
    }];
    
    [self.coinName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];

    
    [self.rigImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(11);
    }];
    
    [self.selectName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rigImg.mas_left).offset(-20);
        make.centerY.mas_equalTo(self);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(18);
    }];
}

- (void)selectedCoinName:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(selectCoinNameAction)]) {
        [self.delegate selectCoinNameAction];
    }
}
@end
