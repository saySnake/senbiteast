//
//  TradeSectionHeaderView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/28.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TradeSectionHeaderView : UIView


/** 索引 0. 当前委托1. 计划委托 */
@property (assign, nonatomic) NSInteger index;

/** 回调block
 index: 0. 委托订单 1. 最新成交 2. 币种简介
 */
@property (strong, nonatomic) void(^headActionBlock)(NSInteger index);

/** 1. 初始化UI */
- (void)setupUI;

@end

NS_ASSUME_NONNULL_END
