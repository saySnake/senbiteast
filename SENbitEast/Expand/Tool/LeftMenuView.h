//
//  LeftMenuView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/2/24.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol HomeMenuViewDelegate <NSObject>

- (void)LeftMenuViewClick:(NSInteger)tag;
//设置界面跳转
- (void)setPushVC;

- (void)accountPushVC;

- (void)realPushVC;

- (void)invotedPushVC;

- (void)initView;


@end

@interface LeftMenuView : BaseView

@property (nonatomic ,weak)id <HomeMenuViewDelegate> customDelegate;

@end


NS_ASSUME_NONNULL_END
