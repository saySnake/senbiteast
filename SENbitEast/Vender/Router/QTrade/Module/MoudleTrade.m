//
//  MoudleLine.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "MoudleTrade.h"
#import "TradeViewController.h"

@implementation MoudleTrade

@synthesize callbackBlock;

@synthesize detailViewController;

@synthesize interfaceViewController;

@synthesize paramterForHome;

@synthesize titleString;

@synthesize descString;

- (UIViewController *)interfaceViewController {
    TradeViewController *interfaceViewController = [[TradeViewController alloc]init];
    interfaceViewController.interface = self;
    return (UIViewController *)interfaceViewController;
}

@end
