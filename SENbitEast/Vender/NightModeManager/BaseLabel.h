//
//  BaseLabel.h
//  Senbit
//
//  Created by 张玮 on 2019/12/27.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//BaseLabel *textLabel = [[BaseLabel alloc] initWithTextColor:@"title"];

@interface BaseLabel : UILabel

@property(nonatomic ,copy) NSString *colorName;
@property(nonatomic ,copy) NSString *backColor;

- (id)initWithTextColor:(NSString *)colorName;

@end

NS_ASSUME_NONNULL_END
