//
//  CacheManager.h
//  Senbit
//
//  Created by 张玮 on 2019/12/25.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
#import "OtcExtinfo.h"

NS_ASSUME_NONNULL_BEGIN

#if defined(__cplusplus)
#define CACHE_EXTERN extern "C"
#else
#define CACHE_EXTERN extern
#endif


/**
    用户对象缓存key
 */
CACHE_EXTERN NSString *const SenlinkCacheUserInfoKey;
CACHE_EXTERN NSString *const SenlinkCacheSearchHistoryInfoKey;
CACHE_EXTERN NSString *const SenlinkCacheUserExtInfoKey;


@interface CacheManager : NSObject {
    dispatch_queue_t _cacheQueue;
}

@property (nonatomic, strong,setter = saveUserInfo:,getter = getUserInfo) UserInfo *user;
@property (nonatomic, strong,setter = saveOtcExinfo:,getter = getOtcInfo) OtcExtinfo *otcInfo;
+ (instancetype)sharedMnager;

- (void)saveOtcExinfo:(OtcExtinfo *)extInfo;

/**
 *  归档 个人信息
 *
 *  @param info 归档
 */
- (void)saveUserInfo:(UserInfo *)info;

/**
 *  接档 个人信息
 *
 *  @return 解档
 */
- (UserInfo *)getUserInfo;

- (OtcExtinfo *)getOtcInfo;
/**
 *  清理数据缓存
 */
- (void)clearCache;

@end

#pragma mark -  Search

@interface CacheManager (searchHistory)

- (void)insertSearchHistory:(NSString *)history;

- (void)removeSearchHistory:(NSString *)history;

- (NSArray *)histories;

- (void)removeAllHistory;

@end


NS_ASSUME_NONNULL_END
