//
//  BaseButton.m
//  Senbit
//
//  Created by 张玮 on 2019/12/27.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "BaseButton.h"
#import "ThemeManager.h"

@implementation BaseButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //监听主题切换的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
        
        [self loadThemeImage];
    }
    return self;
}

- (id)initWithImage:(NSString *)imageName
        highlighted:(NSString *)highligtImageName {
    self = [super initWithFrame:CGRectZero];
    if (self != nil) {
        self.imageName = imageName;
        self.highligtImageName = highligtImageName;
        
        //监听主题切换的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
        
        [self loadThemeImage];
    }
    return self;
}

- (void)dealloc {
    //移除指定的通知
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SThemeDidChangeNotification object:nil];
}

- (void)loadThemeImage {
    ThemeManager *themeManager = [ThemeManager shareInstance];
    //获取当前主题normal状态下的图片
    UIImage *image = [themeManager getThemeImage:self.imageName];
    UIImage *highligtImage = [themeManager getThemeImage:self.highligtImageName];
    
    //设置图片
    [self setImage:image forState:UIControlStateNormal];
    [self setImage:highligtImage forState:UIControlStateHighlighted];
    
    //字体颜色
    if (self.colorName) {
        [self setColorName:self.colorName];
    }
    //背景颜色
    if (self.backColorName) {
        [self setBackColorName:self.backColorName];
    }
}

#pragma mark - NSNotification actions
//当切换主题时，会调用的方法
- (void)themeNotification:(NSNotification *)notification {
    [self loadThemeImage];
}

- (void)setColorName:(NSString *)colorName {
    _colorName = colorName;
    UIColor *viewColor = [[ThemeManager shareInstance] getColorWithName:self.colorName];
    [self setTitleColor:viewColor forState:UIControlStateNormal];
}

- (void)setBackColorName:(NSString *)backColorName {
    _backColorName = backColorName;
    UIColor *viewColor = [[ThemeManager shareInstance] getColorWithName:self.backColorName];
    [self setBackgroundColor:viewColor];
}



@end
