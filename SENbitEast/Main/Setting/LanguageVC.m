//
//  LanguageVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/3/1.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "LanguageVC.h"

@interface LanguageVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *meTableView;
@property (nonatomic,strong) NSMutableArray *dataAry;
@property (nonatomic,copy) NSString *searchText;//搜索词

@end

@implementation LanguageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = ThemeDarkBlack;
    _dataAry = [NSMutableArray new];
    [self setNavBarVisible:kLocalizedString(@"set_setLanguage")]; //kLocalizedString(@"preference")];
    [self addSearchBar];
    [self changeLanguage];
}

- (void)addSearchBar {
    [self.view addSubview:self.tableView];
    
    //block 回调
    __weak __typeof(self)weakSelf = self;
    kLanguageManager.completion = ^(NSString *currentLanguage) {
        __strong __typeof(self)self = weakSelf;
        [self changeLanguage];
    };
}

#pragma mark - data

//目前支持的语言
- (NSArray *)languageAry {
    return @[@"zh-Hans" ,//中文简体
//             @"zh-Hant-CN", //中文繁体
             @"en-CN", //英语
//             @"ko-CN" //韩语
//             @"ja-CN", //日语
//             @"fr-CN", //法语
//             @"it-CN" //意大利语
    ];
}

- (NSArray *)dataAry {
    if (_searchText.length > 0) {
        //搜索则返回搜索数据
        return _dataAry;
    }
    else {
        //反之返回所有数据
        return self.languageAry;
    }
}

////对应国家的语言
- (NSString *)ittemCountryLanguage:(NSString *)lang {
    NSString *language = [kLanguageManager languageFormat:lang];
    NSString *countryLanguage = [[[NSLocale alloc] initWithLocaleIdentifier:language] displayNameForKey:NSLocaleIdentifier value:language];
    return countryLanguage;
}

////当前语言下的对应国家语言翻译
- (NSString *)ittemCurrentLanguageName:(NSString *)lang {
    NSString *language = [kLanguageManager languageFormat:lang];
    //当前语言
    NSString *currentLanguage = kLanguageManager.currentLanguage;
    //当前语言下的对应国家语言翻译
    NSString *currentLanguageName = [[[NSLocale alloc] initWithLocaleIdentifier:currentLanguage] displayNameForKey:NSLocaleIdentifier value:language] ;
    return currentLanguageName;
}

#pragma mark UITableViewDataSource/UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataAry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CellID"];
        cell.backgroundColor = WhiteColor;
        cell.contentView.backgroundColor = WhiteColor;
    }
    
    NSString *language = [kLanguageManager languageFormat:self.dataAry[indexPath.row]];
    
    //对应国家的语言
    NSString *countryLanguage = [self ittemCountryLanguage:self.dataAry[indexPath.row]];
    //当前语言下的对应国家语言翻译
    NSString *currentLanguageName = [self ittemCurrentLanguageName:self.dataAry[indexPath.row]] ;
    
    cell.textLabel.text = countryLanguage;
    cell.textLabel.textColor = MainWhiteColor;
    cell.detailTextLabel.text = currentLanguageName;
    cell.detailTextLabel.textColor = MainWhiteColor;
    if (_searchText.length > 0) {
        cell.textLabel.attributedText = [self searchTitle:countryLanguage key:_searchText keyColor:[UIColor redColor]];
        cell.detailTextLabel.attributedText = [self searchTitle:currentLanguageName key:_searchText keyColor:[UIColor redColor]];
    } else {
        cell.textLabel.text = countryLanguage;
        cell.detailTextLabel.text = currentLanguageName;
    }
    
    //当前语言
    NSString *currentLanguage = kLanguageManager.currentLanguage;
    if ([currentLanguage isEqualToString:@"zh-CN"]) {
        currentLanguage = @"zh-Hans";
    } else if ([currentLanguage isEqualToString:@"zh-TW"]) {
        currentLanguage = @"zh-Hant-CN";
    }
    if([currentLanguage rangeOfString:language].location != NSNotFound) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else  {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = WhiteColor;
    NSString *language = self.dataAry[indexPath.row];
    [kLanguageManager setUserlanguage:language];
    
    [self cancleButtonAction:nil];
}

#pragma mark - UISearchBar Delegate

//已经开始编辑时的回调
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
    [self changeSearchBarCancleText:searchBar];
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 252);
    self.tableView.backgroundColor = MainWhiteColor;
    [self hideOtherCellLine:self.tableView];
}

//编辑文字改变的回调
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"searchText:%@",searchText);
    _searchText = searchText;
    
    [self ittemSearchResultsDataAryWithSearchText:searchText];
    
    [self.tableView reloadData];
}

//取消按钮点击的回调
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    _searchText = nil;
    searchBar.text = nil;
    [self.view endEditing:YES];
    self.tableView.frame = [UIScreen mainScreen].bounds;
    [self.tableView reloadData];
}

//搜索结果按钮点击的回调
- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
    [kLanguageManager setUserlanguage:searchBar.text];
    [self cancleButtonAction:nil];
}

//修改searchBar中的文字为多语言
- (void)changeSearchBarCancleText:(UISearchBar *)searchBar {
    for (UIView *view in [[searchBar.subviews lastObject] subviews]) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *cancelBtn = (UIButton *)view;
            [cancelBtn setTitle:kLocalizedString(@"cancel") forState:UIControlStateNormal];
            [cancelBtn setTitle:kLocalizedString(@"cancel") forState:UIControlStateHighlighted];
            [cancelBtn setTitle:kLocalizedString(@"cancel") forState:UIControlStateSelected];
            [cancelBtn setTitle:kLocalizedString(@"cancel") forState:UIControlStateDisabled];
        }
    }
}

#pragma mark - 自定义方法

//根据搜索词来查找符合的数据
- (void)ittemSearchResultsDataAryWithSearchText:(NSString *)searchText {
    [_dataAry removeAllObjects];
    
    [self.languageAry enumerateObjectsUsingBlock:^(NSString *lang, NSUInteger idx, BOOL * _Nonnull stop) {
        //对应国家的语言
        NSString *countryLanguage = [self ittemCountryLanguage:lang];
        //当前语言下的对应国家语言翻译
        NSString *currentLanguageName = [self ittemCurrentLanguageName:lang] ;
        
        if([countryLanguage rangeOfString:_searchText options:NSCaseInsensitiveSearch].location != NSNotFound || [currentLanguageName rangeOfString:_searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [_dataAry addObject:lang];
        }
    }];
}

// 设置文字中关键字高亮
- (NSMutableAttributedString *)searchTitle:(NSString *)title key:(NSString *)key keyColor:(UIColor *)keyColor {
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:title];
    NSString *copyStr = title;
    
    NSMutableString *xxstr = [NSMutableString new];
    for (int i = 0; i < key.length; i++) {
        [xxstr appendString:@"*"];
    }
    
    while ([copyStr rangeOfString:key options:NSCaseInsensitiveSearch].location != NSNotFound) {
        
        NSRange range = [copyStr rangeOfString:key options:NSCaseInsensitiveSearch];
        
        [titleStr addAttribute:NSForegroundColorAttributeName value:keyColor range:range];
        copyStr = [copyStr stringByReplacingCharactersInRange:NSMakeRange(range.location, range.length) withString:xxstr];
    }
    return titleStr;
}

#pragma mark - Action

//刷新界面
- (void)changeLanguage {
    self.navigationController.navigationBarHidden = YES;
    [self setLeftBtnImage:@"leftBack"];
    [self setNavBarVisible:kLocalizedString(@"set_setLanguage")]; //kLocalizedString(@"preference")];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:kLocalizedString(@"cancel") style:UIBarButtonItemStyleDone target:self action:@selector(cancleButtonAction:)];
    self.navigationItem.leftBarButtonItem = item;
//    self.searchBar.placeholder = kLocalizedString(@"search");
    [self.tableView reloadData];
}

//取消按钮
- (void)cancleButtonAction:(UIButton *)button {
    [self.view endEditing:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 懒加载

- (UITableView *)tableView {
    if (!_meTableView) {
        _meTableView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStylePlain];
        _meTableView.frame = CGRectMake(0, CGRectGetMaxY(self.navBar.frame), DScreenW, kScreenHeight);
        _meTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _meTableView.dataSource = self;
        _meTableView.delegate = self;
        _meTableView.backgroundColor = [UIColor clearColor];
        _meTableView.separatorColor = SparatorColor;
        [self hideOtherCellLine:_meTableView];
    }
    return _meTableView;
}


@end
