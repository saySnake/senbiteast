//
//  Message.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/23.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Message : NSObject
@property (nonatomic,assign) NSString * msgFrom;//消息发送方
@property (nonatomic,assign) NSString * msgTo;//消息接收方
@property (nonatomic,assign) NSString * msgContent;//消息体
@property (nonatomic,assign) NSDictionary *dic;
@property (nonatomic,strong) NSString *lineType;
 
 
 
-(id) initWith:(NSString*) megFrom msgTo:(NSString*) msgTo msgContent:(NSString*)msgContent;//单例方法
 
-(id)toDictionary;//转成字典

@end

NS_ASSUME_NONNULL_END
