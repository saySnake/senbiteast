//
//  ResetVC.h
//  SENbitEast
//
//  Created by 张玮 on 2020/8/13.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResetVC : BaseViewController

@property (nonatomic ,strong) NSString *account;


@property (nonatomic ,strong) NSString *authToken;

@property (nonatomic ,strong) NSString *userToken;

@end

NS_ASSUME_NONNULL_END
