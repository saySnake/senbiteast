//
//  RealNameCell.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/3.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RealNameCell : UITableViewCell


/** lbtext **/
@property (nonatomic ,strong) UILabel *leftLb;

/** content **/
@property (nonatomic ,strong) UILabel *contentLb;

/** img **/
@property (nonatomic ,strong) UIImageView *rightImg;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
