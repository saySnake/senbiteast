//
//  XXBDetailHeaderView.h
//  iOS
//
//  Created by iOS on 2018/6/13.
//  Copyright © 2018年 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XXBDetailHeaderView : UIView

/** 是否全屏 */
@property (assign, nonatomic) BOOL isScreen;

@property (strong, nonatomic) void(^screenActionBlock)();
//全屏
- (void)fullScreenAction;
- (void)show;
- (void)dismiss;
- (void)cleanData;
@end
