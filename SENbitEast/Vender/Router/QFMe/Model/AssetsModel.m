//
//  AssetsModel.m
//  Senbit
//
//  Created by 张玮 on 2020/4/2.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "AssetsModel.h"

@implementation AssetsModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"id"
             };
}

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"coinPair" : @"coinPairModel",
             };
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
    if ([property.name isEqualToString:@"balance"]) {
        if (oldValue == nil) {
            return @"0.0000";
        }
    } else if ([property.name isEqualToString:@"freezed"]) {
        if (oldValue == nil) {
            return @"0.0000";
        }
    } else if ([property.name isEqualToString:@"locked"]) {
        if (oldValue == nil) {
            return @"0.0000";
        }
    }
    return oldValue;
}

@end
