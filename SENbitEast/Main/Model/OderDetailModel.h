//
//  OderDetailModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//amount = 5054;
//createdAt = "2021-05-14T09:28:02.000Z";
//fees = "12.635 USDT";
//market = "BTC/USDT";
//price = 5054;
//tradeId = 609e38a83d96461620793096609e42a13abcb5035cdcdfd9;
//tradeType = sell;
//volume = 1;


@interface OderDetailModel : NSObject
@property (nonatomic ,copy) NSString *amount;
@property (nonatomic ,copy) NSString *createdAt;
@property (nonatomic ,copy) NSString *fees;
@property (nonatomic ,copy) NSString *market;
@property (nonatomic ,copy) NSString *price;
@property (nonatomic ,copy) NSString *tradeId;
@property (nonatomic ,copy) NSString *volume;
@end

NS_ASSUME_NONNULL_END
