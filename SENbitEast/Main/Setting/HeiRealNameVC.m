//
//  HeiRealNameVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/15.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "HeiRealNameVC.h"
#import "WMUIImagePickerView.h"
#import "AliHeight.h"

@interface HeiRealNameVC ()
@property (nonatomic ,strong) UIScrollView *scroll;
@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contentLb;
@property (nonatomic ,strong) UIButton *frontBtn;
@property (nonatomic ,strong) UILabel *alb;
@property (nonatomic ,strong) UILabel *blb;
@property (nonatomic ,strong) UIButton *backBtn;
@property (nonatomic ,strong) UIButton *submitBtn;
@property (nonatomic ,strong) UIImage *img1;
@property (nonatomic ,strong) UIImage *img2;
//图片名称
@property (nonatomic ,copy) NSString *imgName1;
@property (nonatomic ,copy) NSString *imgName2;
@property (nonatomic ,strong) NSString *endImgStr1;
@property (nonatomic ,strong) NSString *endImgStr2;
@property (nonatomic ,strong) UIView *line;

@property (nonatomic ,assign) BOOL a1;
@property (nonatomic ,assign) BOOL a2;
@end

@implementation HeiRealNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    self.navBar.backgroundColor = WhiteColor;
//    [self setNavBarTitle:kLocalizedString(@"high_native")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self setUpView];
    self.a1 = NO;
    self.a2 = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)setUpView {
    [self.view addSubview:self.scroll];
    [self.scroll addSubview:self.titleLb];
    [self.scroll addSubview:self.contentLb];
    [self.scroll addSubview:self.frontBtn];
    [self.frontBtn addSubview:self.alb];
    [self.scroll addSubview:self.backBtn];
    [self.backBtn addSubview:self.blb];
    [self.scroll addSubview:self.submitBtn];
    [self.scroll addSubview:self.line];

    [self make_Layout];
}

- (void)make_Layout {
    
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW);
        make.height.mas_equalTo(DScreenH - kNavBarAndStatusBarHeight);
        make.top.mas_equalTo(self.navBar.mas_bottom);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(75);
        make.width.mas_lessThanOrEqualTo(250);
        make.height.mas_equalTo(40);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(25);
        make.width.mas_equalTo(350);
        make.height.mas_equalTo(50);
    }];
    
    [self.frontBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.width.mas_equalTo(285);
        make.height.mas_equalTo(196);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(30);
    }];
    
    [self.alb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.frontBtn);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(15);
        make.centerY.mas_equalTo(self.frontBtn).offset(15);
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.width.mas_equalTo(285);
        make.height.mas_equalTo(196);
        make.top.mas_equalTo(self.frontBtn.mas_bottom).offset(30);
    }];
    [self.blb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backBtn);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(15);
        make.centerY.mas_equalTo(self.backBtn).offset(15);
    }];
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(DScreenW - 20);
        make.top.mas_equalTo(self.backBtn.mas_bottom).offset(40);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(10);
        make.top.mas_equalTo(self.submitBtn.mas_bottom).offset(10);
    }];
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.text = kLocalizedString(@"high_title");
        _titleLb.textAlignment = 0;
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.text = kLocalizedString(@"high_lock");
        _contentLb.textAlignment = 0;
        _contentLb.font = ThirteenFontSize;
        _contentLb.numberOfLines = 0;
        _contentLb.textColor = HEXCOLOR(0x666666);
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}

- (UIButton *)submitBtn {
    if (!_submitBtn) {
        _submitBtn = [[UIButton alloc] init];
        _submitBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        [_submitBtn setTitle:kLocalizedString(@"high_submit") forState:UIControlStateNormal];
        [_submitBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(commitAction) forControlEvents:UIControlEventTouchUpInside];
        _submitBtn.userInteractionEnabled = NO;
        _submitBtn.cornerRadius = 3;
    }
    return _submitBtn;
}

- (void)commitAction {
    NSLog(@"提交");
    if (!self.endImgStr1 || !self.endImgStr2) {
        [self showToastView:kLocalizedString(@"high_please")];
        return;
    }
    
    NSDictionary *dic = @{@"IDFront":self.endImgStr1,
                          @"Photo":self.endImgStr2
    };
    [SBNetworTool postWithUrl:EastHeiRealname params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        if (SuccessCode == 200) {
            [self showToastView:kLocalizedString(@"high_suc")];
            [self delay:1 task:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (UIButton *)frontBtn {
    if (!_frontBtn) {
        _frontBtn = [[UIButton alloc] init];
        [_frontBtn setImage:[UIImage imageNamed:@"real_front"] forState:UIControlStateNormal];
        [_frontBtn addTarget:self action:@selector(clickfrontBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_frontBtn.imageView setContentMode:UIViewContentModeScaleToFill|UIViewContentModeScaleAspectFit];
    }
    return _frontBtn;
}

- (void)clickfrontBtn:(UIButton *)button {
    WMUIImagePickerView *pickerView = [[WMUIImagePickerView alloc] initWithType:WMUIImagePickerTypeDefault];
    pickerView.photographBlock = ^(UIImage *cropImage,PHAsset *asset) {
        self.img1 = cropImage;
        [button setImage:cropImage forState:UIControlStateNormal];
        self.alb.hidden = YES;
        [button.imageView setContentMode:UIViewContentModeScaleToFill];
        PHImageManager * imageManager = [PHImageManager defaultManager];
         [imageManager requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
             self.imgName1 = @"public.jpeg";//[info valueForKey:@"PHImageFileUTIKey"];
             self.img1 = cropImage;
             [self singInOut1];
        }];
    };
    pickerView.albumsBlock = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [button setImage:photos[0] forState:UIControlStateNormal];
        PHAsset * asset = assets[0];
        self.alb.hidden = YES;

        PHImageManager * imageManager = [PHImageManager defaultManager];
         [imageManager requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
             self.imgName1 = @"public.jpeg";//[info valueForKey:@"PHImageFileUTIKey"];
             self.img1 = photos[0];
             [self singInOut1];
        }];
    };
    [self.view addSubview:pickerView];
}

- (void)singInOut1 {
    NSDictionary *dic = @{@"fileName":self.imgName1,@"fileType":@"image/png"};
    [SBNetworTool getWithUrl:EastUploadImg params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.a1 = YES;
            if (self.a1 == YES && self.a2 == YES) {
                self.submitBtn.userInteractionEnabled = YES;
                self.submitBtn.backgroundColor = ThemeGreenColor;
            }
            [self aliUpLoad1:responseObject[@"data"][@"key"]];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}

- (void)singInOut2 {
    NSDictionary *dic = @{@"fileName":self.imgName1,@"fileType":@"image/png"};
    [SBNetworTool getWithUrl:EastUploadImg params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            self.a2 = YES;
            if (self.a1 == YES && self.a2 == YES) {
                self.submitBtn.userInteractionEnabled = YES;
                self.submitBtn.backgroundColor = ThemeGreenColor;
            }
            [self aliUpLoad2:responseObject[@"data"][@"key"]];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}


- (void)aliUpLoad1:(NSString *)key {
    [AliHeight asyncUploadImage:self.img1 originalPhoto:YES complete:^(NSString *name, UploadImageStates state) {
        self.endImgStr1 = key;
    }];
}

- (void)aliUpLoad2:(NSString *)key {
    [AliHeight asyncUploadImage:self.img2 originalPhoto:YES complete:^(NSString *name, UploadImageStates state) {
        self.endImgStr2 = key;
    }];
}





- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [[UIButton alloc] init];
        [_backBtn setImage:[UIImage imageNamed:@"real_back"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(clickbackBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_backBtn.imageView setContentMode:UIViewContentModeScaleToFill|UIViewContentModeScaleAspectFit];
        [_backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _backBtn;
}


- (void)clickbackBtn:(UIButton *)button {
    WMUIImagePickerView *pickerView = [[WMUIImagePickerView alloc] initWithType:WMUIImagePickerTypeDefault];
    pickerView.photographBlock = ^(UIImage *cropImage,PHAsset *asset) {
        self.img2 = cropImage;
        [button setImage:cropImage forState:UIControlStateNormal];
        self.blb.hidden = YES;
        [[button imageView] setContentMode:UIViewContentModeScaleToFill];
        PHImageManager * imageManager = [PHImageManager defaultManager];
         [imageManager requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
             self.imgName2 = @"public.jpeg";//[info valueForKey:@"PHImageFileUTIKey"];
             self.img2 = cropImage;
             [self singInOut2];
        }];
    };
    pickerView.albumsBlock = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        [button setImage:photos[0] forState:UIControlStateNormal];
        PHAsset * asset = assets[0];
        self.blb.hidden = YES;
        PHImageManager * imageManager = [PHImageManager defaultManager];
         [imageManager requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
             self.imgName2 = @"public.jpeg";//[info valueForKey:@"PHImageFileUTIKey"];
             self.img2 = photos[0];
             [self singInOut2];
        }];
    };
    [self.view addSubview:pickerView];
}

- (UIScrollView *)scroll {
    if (!_scroll) {
        _scroll = [[UIScrollView alloc] init];
        _scroll.backgroundColor = WhiteColor;
        _scroll.showsHorizontalScrollIndicator = NO;//隐藏水平滚动条
    }
    return _scroll;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = WhiteColor;
    }
    return _line;
}

- (UILabel *)alb {
    if (!_alb) {
        _alb = [[UILabel alloc] init];
        _alb.textColor = HEXCOLOR(0x999999);
        _alb.text = kLocalizedString(@"cardFront");
        _alb.adjustsFontSizeToFitWidth = YES;
        _alb.font = kFont(12);
        _alb.textAlignment = 1;
    }
    return _alb;
}

- (UILabel *)blb {
    if (!_blb) {
        _blb = [[UILabel alloc] init];
        _blb.textColor = HEXCOLOR(0x999999);
        _blb.text = kLocalizedString(@"cardBack");
        _blb.font = kFont(12);
        _blb.textAlignment = 1;
        _blb.adjustsFontSizeToFitWidth = YES;
    }
    return _blb;
}


@end
