//
//  SelectedTableViewController.m
//  Senbit
//
//  Created by 张玮 on 2020/3/5.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SelectedTableViewController.h"
#import "SelectedCoinCell.h"
#import "OneTableSockeModel.h"
@import SocketIO;

@interface SelectedTableViewController ()
@property (strong, nonatomic) SocketManager* manager;
@property (strong, nonatomic) SocketIOClient *socket;
@property (nonatomic ,strong) NSString *selectedCoin;
@property (nonatomic ,strong) NSMutableArray *collectionArr;
@property (nonatomic ,strong) NSMutableArray *openArray;
@property (nonatomic ,assign) BOOL isCollection;//是否自选
@property (nonatomic ,assign) BOOL isOpenZone; //是否开放
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) XXWebQuoteModel *webModel;
@property (nonatomic ,strong) NSMutableArray *data;

@end

@implementation SelectedTableViewController
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (NSMutableArray *)collectionArr {
    if (!_collectionArr) {
        _collectionArr = [[NSMutableArray alloc] init];
    }
    return _collectionArr;
}

- (NSMutableArray *)openArray {
    if (!_openArray) {
        _openArray = [[NSMutableArray alloc] init];
    }
    return _openArray;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KnotificationExchangeSelected object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [IWNotificationCenter addObserver:self selector:@selector(selectedTableView:) name:KnotificationExchangeSelected object:nil];
    self.tableView.rowHeight = 50;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = ThemeDarkBlack;
    self.tableView.separatorColor = PlaceHolderColor;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView reloadData];
    [KSystem statusBarSetUpDarkColor];
    [self socketData];
}

- (void)selectedTableView:(NSNotification *)noti {
    NSDictionary *dict = noti.userInfo;
    self.selectedCoin = dict[@"coinName"];
    if ([dict[@"coinName"] isEqualToString:kLocalizedString(@"home_selfSelected")]) {
        self.isCollection = YES;
        [self getPairCollection];
    } else if ([dict[@"coinName"] isEqualToString:@"开放区"]) {
        self.isOpenZone = YES;
        [self getLocalZone];
    } else {
        self.isCollection = NO;
    }
    
    if (self.isCollection) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in self.data) {
            for (OneTableSockeModel *m1 in self.collectionArr) {
                if ([m1.coinName isEqualToString:model.coinName]) {
                    model.isCollection = YES;
                    [self.dataArr addObject:model];
                }
            }
        }
    }
    if (self.isOpenZone) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in self.data) {
            for (OneTableSockeModel *m1 in self.openArray) {
                for (OneTableSockeModel *m2 in self.collectionArr) {
                    if ([m2.coinName isEqualToString:m1.coinName]) {
                        model.isCollection = YES;
                    }
                }
                if ([m1.coinName isEqualToString:model.coinName]) {
                        [self.dataArr addObject:model];
                }
            }
        }
    }
    if (!self.isCollection && !self.isOpenZone) {
        [self.dataArr removeAllObjects];
        for (OneTableSockeModel *model in self.data) {
            NSUInteger seleLenth = self.selectedCoin.length;
            NSString *endStr = [model.coinName substringFromIndex:model.coinName.length - seleLenth];
            if ([endStr isEqualToString:self.selectedCoin]) {
                for (OneTableSockeModel *m in self.collectionArr) {
                    if ([m.coinName isEqualToString:model.coinName]) {
                        model.isCollection = YES;
                        break;
                    }
                }
                [self.dataArr addObject:model];
            }
        }
    }
    [self.tableView reloadData];

}

- (void)socketData {
    KWeakSelf
    self.webModel = nil;
    self.webModel = [[XXWebQuoteModel alloc] init];
    self.webModel.successBlock = ^(id data) {
        weakSelf.data = [OneTableSockeModel mj_objectArrayWithKeyValuesArray:data];
        if (weakSelf.isCollection) {
            [weakSelf.dataArr removeAllObjects];
            for (OneTableSockeModel *model in weakSelf.data) {
                for (OneTableSockeModel *m1 in weakSelf.collectionArr) {
                    if ([m1.coinName isEqualToString:model.coinName]) {
                        model.isCollection = YES;
                        [weakSelf.dataArr addObject:model];
                    }
                }
            }
        }
        if (weakSelf.isOpenZone) {
            [weakSelf.dataArr removeAllObjects];
            for (OneTableSockeModel *model in weakSelf.data) {
                for (OneTableSockeModel *m1 in weakSelf.openArray) {
                    for (OneTableSockeModel *m2 in weakSelf.collectionArr) {
                        if ([m2.coinName isEqualToString:m1.coinName]) {
                            model.isCollection = YES;
                        }
                    }
                    if ([m1.coinName isEqualToString:model.coinName]) {
                            [weakSelf.dataArr addObject:model];
                    }
                }
            }
        }
        if (!weakSelf.isCollection && !weakSelf.isOpenZone) {
            [weakSelf.dataArr removeAllObjects];
            for (OneTableSockeModel *model in weakSelf.data) {
                NSUInteger seleLenth = weakSelf.selectedCoin.length;
                NSString *endStr = [model.coinName substringFromIndex:model.coinName.length - seleLenth];
                if ([endStr isEqualToString:weakSelf.selectedCoin]) {
                    for (OneTableSockeModel *m in weakSelf.collectionArr) {
                        if ([m.coinName isEqualToString:model.coinName]) {
                            model.isCollection = YES;
                            break;
                        }
                    }
                    [weakSelf.dataArr addObject:model];
                }
            }
        }
        [weakSelf.tableView reloadData];
    };
    NSDictionary *dic = @{};
    self.webModel.params = [NSMutableDictionary dictionary];
    self.webModel.params[@"topic"] = @"home-overview";
    self.webModel.params[@"data"] = dic;
    [KQuoteSocket sendWebSocketSubscribeWithWebSocketModel:self.webModel lineStype:@"home-overview"];
}


- (void)getLocalZone {
//    [SBNetworTool getWithUrl:SenOpenZone params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//        NSArray * arr = responseObject[@"data"];
//        [self.openArray removeAllObjects];
//        for (NSDictionary *str in arr) {
//            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
//            model.coinName = str[@"id"];
//            [self.openArray addObject:model];
//        }
//        [self socketData];
//    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//        OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
//        [self.openArray addObject:model];
//        [self socketData];
//    }];
}

- (void)getPairCollection {
//    [SBNetworTool getWithUrl:SenPairCollection params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//        NSLog(@"%@",responseObject);
//        NSArray * arr = responseObject;
//        [self.collectionArr removeAllObjects];
//        for (NSString *str in arr) {
//            OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
//            model.coinName = str;
//            [self.collectionArr addObject:model];
//        }
//        [self socketData];
//
//    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//        NSLog(@"%@",responseObject);
//        OneTableSockeModel *model = [[OneTableSockeModel alloc] init];
//        [self.collectionArr addObject:model];
//        [self socketData];
//    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getPairCollection];
//    [self collection];
}

//- (void)collection {
//    [SBNetworTool getWithUrl:SenCollectionCoin params:@{} isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//        NSArray * arr = responseObject;
//        for (NSString *str in arr) {
//            if ([str isEqualToString:((EProtocolSI *)self.protocolSI).epCoinName]) {
//                self.collect.selected = YES;
//                break;
//            }
//        }
//    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
//
//    }];
//}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    OneTableSockeModel *model = self.dataArr[indexPath.row];
    NSDictionary *dic = @{@"coinName":model.coinName};
    [KDetail.symbolsArray enumerateObjectsUsingBlock:^(XXSymbolModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       if ([obj.symbolId isEqualToString:model.coinName]) {//数组中已经存在该对象
           KDetail.symbolModel = obj;
           KTrade.coinTradeModel = obj;
           NSLog(@"%@",KDetail.symbolModel.symbolId);
           *stop = YES;
         }
   }];
    [NotificationManager tradeSelectedSwitchTradeSymbolNotification:dic];
//    [IWNotificationCenter postNotificationName:KnotificationExchangeSelectedCoin object:self userInfo:dic];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectedCoinCell *cell = [SelectedCoinCell cellWithTableView:tableView];
    OneTableSockeModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    cell.collectionBlock = ^(BLOCKStat state) {
        [self getPairCollection];
        if (state == BLOCKSuccess) {
            [self showToastView:kLocalizedString(@"toastAddSelectedSuccess")];
        }else {
            [self showToastView:kLocalizedString(@"toastDelSelectedSuccess")];
        }
    };
    return cell;
}

@end
