//
//  SegmentModel.h
//  Senbit
//
//  Created by 张玮 on 2020/1/13.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SegmentModel : NSObject

/** 标题 */
@property (nonatomic, copy) NSString *title;
/** 标题的字体 */
@property (nonatomic, strong) UIFont *titlelbFont;
/** 副标题 */
@property (nonatomic, copy) NSString *subTitle;
/** 副标题的字体 */
@property (nonatomic, strong) UIFont *subTitleFont;
/** titleColor **/
@property (nonatomic ,strong) UIColor *titleColor;


+ (instancetype)segmentWithTitle:(NSString *)title subTitle:(NSString *)subTitle titleColor:(UIColor *)titleColor;

@end

NS_ASSUME_NONNULL_END
