//
//  EmailViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "EmailViewController.h"
#import "PhoneViewController.h"
#import "SafeSheet.h"
#import "SetPasswordVC.h"
#import "CPCountryTableViewController.h"
#import "CPCountry.h"
#import "AsyncTaskButton.h"
#import "TipsView.h"
@interface EmailViewController ()<UITextFieldDelegate,AsyncTaskCaptchaButtonDelegate>

/** cancel **/
@property (nonatomic ,strong) BaseButton *cancelBtn;

/** loginBtn **/
@property (nonatomic ,strong) BaseButton *loginBtn;

/** titleLb **/
@property (nonatomic ,strong) BaseLabel *titleLb;

/** conentLb **/
@property (nonatomic ,strong) BaseLabel *contentLb;

/** countryBtn **/
@property (nonatomic ,strong) FSCustomButton *countryBtn;

/** baseField **/
@property (nonatomic ,strong) BaseField *accountTF;

/** nextBtn **/
@property (nonatomic ,strong) AsyncTaskButton *nextBtn;

/** phoneBtn **/
@property (nonatomic ,strong) BaseButton *phoneBtn;

/** 区号简称 **/
@property (nonatomic ,strong) NSString *locoal;

@end

@implementation EmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setUpViews];
    [self make_Layoout];
    [self configKeyBoardRespond];
}

- (void)setUpViews {
    [self.view addSubview:self.cancelBtn];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.countryBtn];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.nextBtn];
    [self.view addSubview:self.phoneBtn];
}

- (void)make_Layoout {
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(KNavBarButtonHeight);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(27);
    }];

    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(136);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(-30);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(27);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(39);
    }];

    [self.countryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(70);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(24);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.countryBtn);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(self.countryBtn.mas_bottom).offset(20);
        make.height.mas_equalTo(40);
    }];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(53);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(60);
    }];
    
    [self.phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.nextBtn.mas_bottom).offset(35);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(200);
    }];
}

#pragma mark - action
- (void)loginAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)backAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)countryAction {
    CPCountryTableViewController *cp = [[CPCountryTableViewController alloc] init];
    cp.isResigster = YES;
    cp.callback = ^(CPCountry *country) {
        NSString *lan = kLanguageManager.currentLanguage;
        if ([lan isEqualToString:@"en"]) {
            [self.countryBtn setTitle:country.en forState:UIControlStateNormal];
        } else {
            [self.countryBtn setTitle:country.zh forState:UIControlStateNormal];
        }
        self.locoal = country.locale;
    };
    [self.navigationController pushViewController:cp animated:YES];
}


- (void)nextAction {
    WS(weakSelf);
    SafeSheet *sheet = [[SafeSheet alloc] init];
    [sheet showFromView:self.view];
    sheet.type = SafeSheetEmailRegister;
    sheet.account = self.accountTF.textField.text;
    sheet.country = self.countryBtn.titleLabel.text;
    sheet.locoal = self.locoal;
    sheet.clickBlock = ^(NSString * _Nonnull str) {
        [weakSelf authToken:str];
    };
}

- (void)authToken:(NSString *)str {
    NSDictionary *dic = @{@"email":self.accountTF.textField.text,@"emailCode":str,@"type":@"REGISTER"};
    [SBNetworTool getWithUrl:EastAuthToken params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self pushSetPassword:responseObject[@"data"][@"authToken"]];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [TipsView showTipOnKeyWindow:FailurMessage];
    }];
}

- (void)pushSetPassword:(NSString *)authToken {
    SetPasswordVC *password = [[SetPasswordVC alloc] init];
    password.account = self.accountTF.textField.text;
    password.locale = self.locoal;
    password.isPhoneRegister = NO;
    password.authToken = authToken;
    [self.navigationController pushViewController:password animated:YES];
}



#pragma mark - AsyncTaskCaptchaButtonDelegate

- (void)captchaWillDisplay {
    [TipsView removeAllTipsView];
}

- (BOOL)captchaButtonShouldBeginTapAction:(AsyncTaskButton *)button {
 
    [TipsView showTipOnKeyWindow:kLocalizedString(@"login_safe")];

    return YES;
}

- (void)captchaDidFinish:(BOOL)status error:(GT3Error *)error {
    [self.view endEditing:YES];
    BOOL ieEmail = [NSString checkEmail:self.accountTF.textField.text];

   if ([self.countryBtn.titleLabel.text isEqualToString:@"--"]) {
        [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_country")];
        return ;
    }
   else if (self.accountTF.textField.text.length == 0) {
        [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_noPhone")];
        return ;
    }
   else if (ieEmail == NO) {
       [TipsView showTipOnKeyWindow:kLocalizedString(@"toast_emailnotFomart")];
       return;
   }
    if (status == YES) {
        [self nextAction];
    }
}

- (void)phoneAction {
    PhoneViewController *phone = [[PhoneViewController alloc] init];
    [self.navigationController pushViewController:phone animated:NO];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak EmailViewController *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}

#pragma mark - lazy
- (BaseButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[BaseButton alloc] init];
        [_cancelBtn setTitle:kLocalizedString(@"login_cancel") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = kFont16;
        _cancelBtn.colorName = @"login_cancel";
        _cancelBtn.backgroundColor = ClearColor;
        _cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (BaseButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [[BaseButton alloc] init];
        _loginBtn = [[BaseButton alloc] init];
        _loginBtn.colorName = @"login_loginBtnCor";
        _loginBtn.backColorName = @"login_registerBackground";
        [_loginBtn setTitle:kLocalizedString(@"login_login") forState:UIControlStateNormal];
        _loginBtn.titleLabel.font = BOLDSYSTEMFONT(16);
        [_loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}

- (BaseLabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[BaseLabel alloc] init];
        _titleLb.textColor = ThemeTextColor;
        _titleLb.font = BOLDSYSTEMFONT(33);
        _titleLb.textAlignment = 0;
        _titleLb.adjustsFontSizeToFitWidth = YES;
        _titleLb.text = kLocalizedString(@"login_emailRegister");
    }
    return _titleLb;
}

- (BaseLabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[BaseLabel alloc] init];
        _contentLb.colorName = @"login_conentColor";
        _contentLb.textAlignment = 0;
        _contentLb.font = kFont16;
        _contentLb.text = kLocalizedString(@"login_contentmodify");
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}

- (FSCustomButton *)countryBtn {
    if (!_countryBtn) {
        _countryBtn = [[FSCustomButton alloc] init];
        _countryBtn.buttonImagePosition = FSCustomButtonImagePositionRight;
        [_countryBtn setImage:[UIImage imageNamed:@"icon_Triangles"] forState:UIControlStateNormal];
        _countryBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _countryBtn.backgroundColor = [UIColor clearColor];
        _countryBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [_countryBtn setTitleColor:ThemeTextColor forState:UIControlStateNormal];
        _countryBtn.contentHorizontalAlignment = 1;
        _countryBtn.userInteractionEnabled = YES;
        [_countryBtn setTitle:@"--" forState:UIControlStateNormal];
        [_countryBtn addTarget:self action:@selector(countryAction) forControlEvents:UIControlEventTouchUpInside];
        _countryBtn.titleLabel.font = BOLDSYSTEMFONT(14.5);
    }
    return _countryBtn;
}

- (BaseField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[BaseField alloc] init];
        _accountTF.placeholder = kLocalizedString(@"login_pleaseEmailPlace");
        _accountTF.placeHolderColor = PlaceHolderColor;
        _accountTF.textField.font = FifteenFontSize;
        [_accountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (_accountTF.textField.text.length > 0) {
        self.nextBtn.userInteractionEnabled = YES;
        self.nextBtn.backgroundColor = ThemeGreenColor;
        
    } else {
        self.nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.nextBtn.userInteractionEnabled = NO;
    }
}


- (AsyncTaskButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [[AsyncTaskButton alloc] init];
        _nextBtn.delegate = self;
//        _nextBtn.colorName = @"reset_confirmTitle";
//        _nextBtn.backColorName = @"login_loginBtnCor";
        [_nextBtn setTitle:kLocalizedString(@"login_next") forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = TwentyFontSize;
        _nextBtn.backgroundColor = ThemeGreenColor;
        _nextBtn.cornerRadius = 3;
        _nextBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _nextBtn.userInteractionEnabled = NO;
    }
    return _nextBtn;
}


- (BaseButton *)phoneBtn {
    if (!_phoneBtn) {
        _phoneBtn = [[BaseButton alloc] init];
        _phoneBtn.colorName = @"login_loginBtnCor";
        [_phoneBtn setTitle:kLocalizedString(@"login_phoneRe") forState:UIControlStateNormal];
        _phoneBtn.titleLabel.font = ThirteenFontSize;
        [_phoneBtn addTarget:self action:@selector(phoneAction) forControlEvents:UIControlEventTouchUpInside];
        _phoneBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _phoneBtn;
}

#pragma mark - textDeleagte
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super hideKeyboard];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super hideKeyboard];
}


@end
