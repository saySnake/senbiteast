//
//  UIView+Util.m
//  UsedCar
//
//  Created by Alan on 13-10-25.
//  Copyright (c) 2013年 Alan. All rights reserved.
//

#import "UIView+Util.h"

@implementation UIView (Util)

@dynamic borderColor;
/**
 *  设置所有圆角
 */
- (void)setAllCorner:(CGFloat)radius {
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                          cornerRadius:radius];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (void)setX:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)x {
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)y {
    return self.frame.origin.y;
}

- (CGFloat)minX {
    return self.frame.origin.x;
}

- (void)setMinX:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)minY {
    return self.frame.origin.y;
}

- (void)setMinY:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)maxX {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setMaxX:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x - frame.size.width;
    self.frame = frame;
}

- (CGFloat)maxY {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setMaxY:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y - frame.size.height;
    self.frame = frame;
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}

- (CGFloat)centerY {
    return self.center.y;
}
- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (CGFloat)cornerRadius{
    return self.layer.cornerRadius;
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

-(void)setBorderColor:(UIColor *)borderColor{
    self.layer.borderWidth = kLinePixel;
    self.layer.cornerRadius = 4.f;
    self.layer.borderColor = borderColor.CGColor;
}
- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)ttScreenX {
    CGFloat x = 0;
    for (UIView* view = self; view; view = view.superview) {
        x += view.minX;
    }
    return x;
}

- (CGFloat)ttScreenY {
    CGFloat y = 0;
    for (UIView* view = self; view; view = view.superview) {
        y += view.minY;
    }
    return y;
}

- (CGFloat)screenViewX {
    CGFloat x = 0;
    for (UIView* view = self; view; view = view.superview) {
        x += view.minX;
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView* scrollView = (UIScrollView*)view;
            x -= scrollView.contentOffset.x;
        }
    }
    
    return x;
}

- (CGFloat)screenViewY {
    CGFloat y = 0;
    for (UIView* view = self; view; view = view.superview) {
        y += view.minY;
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView* scrollView = (UIScrollView*)view;
            y -= scrollView.contentOffset.y;
        }
    }
    return y;
}

- (CGRect)screenFrame {
    return CGRectMake(self.screenViewX, self.screenViewY, self.width, self.height);
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGPoint)centerBounds {
    return CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
}

- (void)setCenterBounds:(CGPoint)centerBounds {
    
}

- (UIView*)subviewWithFirstResponder {
    if ([self isFirstResponder])
        return self;
    
    for (UIView *subview in self.subviews) {
        UIView *view = [subview subviewWithFirstResponder];
        if (view) return view;
    }
    
    return nil;
}

- (UIView*)subviewWithClass:(Class)cls {
    if ([self isKindOfClass:cls])
        return self;
    
    for (UIView* subview in self.subviews) {
        UIView* view = [subview subviewWithClass:cls];
        if (view) return view;
    }
    
    return nil;
}

- (UIView*)superviewWithClass:(Class)cls {
    if ([self isKindOfClass:cls]) {
        return self;
    } else if (self.superview) {
        return [self.superview superviewWithClass:cls];
    } else {
        return nil;
    }
}

-(void)addTopLine:(CGFloat)h{
    UIView *line = [[UIView alloc]initLineWithFrame:CGRectMake(0, 0, self.width, h) color:kBackDefaultGrayColor];
    [self addSubview:line];
}
-(void)addBottomLine:(CGFloat)h{
    UIView *line=[[UIView alloc]initLineWithFrame:CGRectMake(0, self.height - h, self.width, h) color:kBackDefaultGrayColor];
    [self addSubview:line];

}
-(void)addLeftLine:(CGFloat)w{
    UIView *line=[[UIView alloc]initLineWithFrame:CGRectMake(0, 0, w, self.height) color:kBackDefaultGrayColor];
    [self addSubview:line];

}
- (void)addRightLine:(CGFloat)w {
    UIView *line = [[UIView alloc]initLineWithFrame:CGRectMake(self.width-w, 0, w, self.height) color:kBackDefaultGrayColor];
    [self addSubview:line];
}


- (void)removeAllSubviews {
    while (self.subviews.count) {
        UIView* child = self.subviews.lastObject;
        [child removeFromSuperview];
    }
}

- (UIImage *)screenshot {
//    //支持retina高分的关键
//    if(/* DISABLES CODE */ (&UIGraphicsBeginImageContextWithOptions) != NULL){
//    }
//    else{
//        UIGraphicsBeginImageContext(self.bounds.size);
//    }
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);

    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/** 全局区域头部视图 */
+ (UIView *)sectionViewWithFrame:(CGRect)frame title:(NSString *)title {
    UIView *view = [[UIView alloc] initWithFrame:frame];
    //view.backgroundColor = kColorGray3;
    
    UILabel *labSection = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, view.width - 20, view.height)];
    labSection.text = title;
    //labSection.textColor = kColorBlue;
    
    [view addSubview:labSection];
    
    return view;
}

/** 设置背景图片 */
- (void)setBackgroundImage:(UIImage *)image {
    UIImageView *iv = [[UIImageView alloc] initWithImage:image];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [self setBackgroundView:iv];
}

///** 设置背景模糊 */
//- (void)setBackgroundBlur:(UIColor *)color
//{
//    UIView *bgView = [[AMBlurView alloc] initWithFrame:self.bounds];
//    [(AMBlurView *)bgView setBlurTintColor:color];
//    [self setBackgroundView:bgView];
//}

/** 设置背景视图 */
- (void)setBackgroundView:(UIView *)bgView {
    static int kBackgroundViewTag = 0x06746292;
    self.backgroundColor = [UIColor clearColor];

    bgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    bgView.tag = kBackgroundViewTag;

    UIView* oldView = [self viewWithTag:kBackgroundViewTag];
    if (oldView)
        [oldView removeFromSuperview];
    
    [self insertSubview:bgView atIndex:0];
}

//初始化一根线
- (id)initLineWithFrame:(CGRect)frame color:(UIColor *)color {
    self = [self initWithFrame:frame];
    self.backgroundColor = color;
    return self;
}
/**
 *  返回图片
 *
 *  @param name  name
 *  @param frame frame
 *
 *  @return UIColor
 */
- (UIColor *)colorWithImageName:(NSString *)name frame:(CGRect)frame {
    UIColor *bgColor = [UIColor colorWithPatternImage: [UIImage imageNamed:name]];
    [self setFrame:frame];
    [self setBackgroundColor:bgColor];
    return bgColor;
}
/** super UIViewController */
- (UIViewController*)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

/** 文字组织视图结构 */
- (void)dumpViewIntoString:(NSMutableString *)str view:(UIView *)view level:(int)level {
    for (int i = 0; i < level; i++)
        [str appendString:@"--"];
    [str appendFormat:@"[%d] %@ %@\n", level, [view.class description], NSStringFromCGRect(view.frame)];
    for (UIView *v in view.subviews)
        [self dumpViewIntoString:str view:v level:level + 1];
}

/** 文字视图结构 */
- (NSString *)stringViewStruct {
    NSMutableString *str = [NSMutableString string];
    [self dumpViewIntoString:str view:self level:0];
    return str;
}

/** 图片组织视图结构 */
- (void)dumpViewIntoImages:(NSMutableArray *)imgs view:(UIView *)view {
    UIImage *img = [view screenshot];
    if (img)
        [imgs addObject:img];
    for (UIView *v in view.subviews)
        [self dumpViewIntoImages:imgs view:v];
}

/** 图片视图结构 */
- (NSMutableArray *)imageViewStruct {
    NSMutableArray *imgs = [NSMutableArray array];
    [self dumpViewIntoImages:imgs view:self];
    return imgs;
}

@end

@implementation UITextField (Util)

- (void)showToolBar {
    UIToolbar *bar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 0, 39)];
    bar.backgroundColor = kBackDefaultGrayColor;
    UIButton *finish = [UIButton buttonWithType:UIButtonTypeCustom];
    finish.frame = CGRectMake(DScreenW - 54, 0, 54, bar.height);
    finish.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    [finish setTitle:@"完成" forState:UIControlStateNormal];
    [finish setTitleColor:kBackDefaultGrayColor forState:UIControlStateNormal];
    [finish addTarget:self action:@selector(finishInput) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:finish];
    self.inputAccessoryView = bar;
}

- (void)finishInput {
    [self resignFirstResponder];
}
@end

@implementation UITextView (Util)

- (void)showToolBar {
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 0, 39)];
    bar.backgroundColor = kBackDefaultGrayColor;
    UIButton *finish = [UIButton buttonWithType:UIButtonTypeCustom];
    finish.frame = CGRectMake(DScreenW - 54, 0, 54, bar.height);
    finish.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [finish setTitle:@"完成" forState:UIControlStateNormal];
    [finish setTitleColor:kBackDefaultGrayColor forState:UIControlStateNormal];
    [finish addTarget:self action:@selector(finishInput) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:finish];
    self.inputAccessoryView = bar;
}

- (void)finishInput {
    [self resignFirstResponder];
}

@end


@implementation UILabel (RedPoint)

- (instancetype)initRedPoint:(CGPoint)center{
    if (self = [super init]) {
        self.bounds = CGRectMake(0, 0, 14, 14);
        self.center = center;
        self.cornerRadius = 7;
        self.backgroundColor = [UIColor redColor];//RGBACOLOR(230, 70, 60, 1);
        self.textAlignment = NSTextAlignmentCenter;
        self.font = TenFontSize;
        self.clipsToBounds = YES;
        self.textColor = WhiteColor;
    }
    return self;
}
- (instancetype)initUnreadRedPoint:(CGPoint)center {
    if (self = [super init]) {
        self.bounds = CGRectMake(0, 0, 18, 18);
        self.center = center;
        self.cornerRadius = 9;
        self.backgroundColor = [UIColor redColor];//RGBACOLOR(239, 49, 65, 1);
        self.textAlignment = NSTextAlignmentCenter;
        self.font = TenFontSize;
        self.clipsToBounds = YES;
        self.textColor = WhiteColor;
    }
    return self;
}
- (void)setBadgeValue:(NSString *)value attribute:(NSDictionary *)attribute {
    UIFont *font = attribute[NSFontAttributeName];
    UIColor *fcolor = attribute[NSForegroundColorAttributeName];
    if (font) {
        self.font = font;
    }
    if (fcolor) {
        self.textColor = fcolor;
    }
    CGSize size = [value sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:self.font.pointSize + 4]}];
    self.textAlignment = NSTextAlignmentCenter;
//    size.width+=5;
//    size.height+=5;
    CGPoint center = self.center;
    if (size.width > size.height) {
        self.size = size;
    }else{
        self.size = CGSizeMake(size.height, size.height);
    }
    BOOL isStopUpdate = [attribute[@"stopUpdateCenter"] boolValue];
    if (isStopUpdate == NO) {
        self.center = center;
    }
    self.cornerRadius = size.height/2;
    self.clipsToBounds = YES;
    self.hidden = NO;
    self.text = value;
}
@end





















