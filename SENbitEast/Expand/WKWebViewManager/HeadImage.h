//
//  HeadImage.h
//  Senbit
//
//  Created by 张玮 on 2020/6/18.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface HeadImage : NSObject
//合约分享
+ (UIImage*)imageWithText1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 text4:(NSString *)text4 text5:(NSString *)text5 text6:(NSString *)text6 text7:(NSString *)text7 text8:(NSString *)text8 text9:(NSString *)text9 isMore:(BOOL)isMore isCrease:(BOOL)isCrease;

//平仓分享
+ (UIImage*)pingContractText1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 text4:(NSString *)text4 text5:(NSString *)text5 text6:(NSString *)text6 text7:(NSString *)text7 isMore:(BOOL)isMore isCrease:(BOOL)isCrease;


//亏损分享
+ (UIImage*)kuiSunContractText1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 text4:(NSString *)text4 text5:(NSString *)text5 text6:(NSString *)text6 text7:(NSString *)text7;

//螃蟹分享
+ (UIImage *)shareCrabPaper;

+ (UIImage *)newPager:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
