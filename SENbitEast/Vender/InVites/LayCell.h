//
//  LayCell.h
//  IFMShareDemo
//
//  Created by 张玮 on 2020/3/6.
//  Copyright © 2020 刘刚. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LayCell : UICollectionViewCell

@property (nonatomic ,strong) UIImageView *img;
@end

NS_ASSUME_NONNULL_END
