//
//  BaseViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/11.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "BaseViewController.h"
//#import "MBProgressHUD.h"

void *const isModelPresent = "isModelPresent";

@interface BaseViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
//@property(nonatomic,assign) BOOL can_scroll;
@property(nonatomic,strong) UIScrollView *scrollview;

@end

@implementation BaseViewController

- (void)dealloc {
    NSLog(@"\n\n****************************************\n***销毁%@***\n****************************************\n",self);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSMutableArray *)dataSource {
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

/**
 状态栏样式
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDarkContent;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (void)registerCellWithNib:(NSString *)nibName tableView:(UITableView *)tableView {
    [tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void)registerCellWithClass:(NSString *)className tableView:(UITableView *)tableView {
    [tableView registerClass:NSClassFromString(className) forCellReuseIdentifier:className];
}


- (void)viewDidLoad {
    __weak typeof(self)Action = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = Action;
    [super viewDidLoad];
    self.view.backgroundColor = MainWhiteColor;
//    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(childScrollStop:) name:@"childScrollStop" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(childScrollStop:) name:@"childScrollCan" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(childScrollStop:) name:@"MainTableScroll" object:nil];
//    [self createNavigation];
}

- (void)show:(UIButton *)sender {
//    [SVProgressHUD show];
}

- (void)dismiss:(UIButton *)sender {
//    [SVProgressHUD dismiss];
}

/// 展示hud
- (void)show {
//    [SVProgressHUD show];
}

/// 隐藏hud
- (void)dismiss {
//    [SVProgressHUD dismiss];
}



- (CustNavBar *)navBar {
    if (_navBar == nil) {
        _navBar = [[CustNavBar alloc]initWithFrame:CGRectMake(0, 0, DScreenW, kNavBarAndStatusBarHeight)];
        [self.view addSubview:_navBar];
        __weak typeof(self) weakself = self;
        _navBar.itemClick =^(NSInteger type){
            [weakself navBarButtonClick:type];
        };
        _navBar.btnClick =^(UIButton *btn){
            [weakself navBarTitleClick:btn];
        };
//        [_navBar setLeftItemImage:@"navigationbar_background"];
        if (self == self.navigationController.viewControllers[0]) {
            self.leftBackItemHiden = YES;
        }
        _navBar.hidden = NO;
    }
    return _navBar;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.view bringSubviewToFront:_navBar];
}

- (void)setNavBarVisible:(NSString *)title {
    [self.navBar setBarTitle:title];
    self.navBar.hidden = NO;
}

- (void)setCustCenterView:(UIView *)custView {
    _navBar = [[CustNavBar alloc] initWithFrame:CGRectMake(0, 0, DScreenW, kNavBarAndStatusBarHeight)];
    [_navBar setCenterView:custView];
    [_navBar setLeftItemImage:@"navigationbar_background"];
    __weak typeof(self) weakself = self;
    _navBar.itemClick =^(NSInteger type){
        [weakself navBarButtonClick:type];
    };
    _navBar.btnClick =^(UIButton *btn){
        [weakself navBarTitleClick:btn];
    };
    [self.view addSubview:_navBar];
}

- (void)setLeftBackItemHiden:(BOOL)leftBackItemHiden {
    _leftBackItemHiden = leftBackItemHiden;
    self.navBar.leftBtn.hidden = leftBackItemHiden;
}

- (void)setLeftBtnImage:(NSString *)image {
    [self.navBar setLeftItemImage:image];
}

- (void)setRightBtnImage:(NSString *)image {
    [self.navBar setRightItemImage:image];
}

- (void)setRightBtnStyle:(NavBarBtnStyle)style {
    [_rightBtn removeFromSuperview];
    _rightBtn = nil;
    _rightBtn = [[FSCustomButton alloc] init];
//    _rightBtn.layer.borderColor = MainWhiteColor.CGColor;
//    _rightBtn.layer.borderWidth = 1.f;
//    _rightBtn.cornerRadius = 2.f;
    _rightBtn.titleLabel.textAlignment = 1;
    _rightBtn.titleLabel.font = TwelveFontSize ;
    _rightBtn.titleLabel.font = BOLDSYSTEMFONT(14);
    CGSize size;
    NSString *title;
    switch (style) {
        case NavBarBtnStyleDone://"完成"
        {
            title = kLocalizedString(@"nav_done");
            size = CGSizeMake(40, 20);
        }
            break;
        case NavBarBtnStyleEdit://"编辑"
        {
            title = kLocalizedString(@"nav_edit");
            size = CGSizeMake(40, 20);

        }
            break;
        case NavBarBtnStyleCancle://"取消"
        {
            title = kLocalizedString(@"nav_cancel");
            size = CGSizeMake(40, 20);
        }
            break;
        case NavBarBtnStyleHistory://"确定"
        {
            title = kLocalizedString(@"nav_his");
            size = CGSizeMake(40, 20);

        }
            break;
        case NavBarBtnStyleGuidList: //工会列表
        {
            title = kLocalizedString(@"nav_myGuild");
            size = CGSizeMake(40, 20);
        }
            break;
        case NavBarBtnStylePlus: //我的plus
        {
            title = kLocalizedString(@"nav_myPlus");
            size = CGSizeMake(40, 20);
        }
        default:
            break;
    }
    [_rightBtn setTitleColor:MainWhiteColor forState:UIControlStateNormal];
    _rightBtn.frame = CGRectMake(0, 0, 80, 22);
    _rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    _rightBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_rightBtn setTitle:title forState:UIControlStateNormal];
    [_rightBtn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.navBar addSubview:_rightBtn];
}

//导航栏标题
- (void)setNavBarTitle:(NSString *)title {
    [self.navBar setBarTitle:title];
}

- (void)setNavBarLeftTitle:(NSString *)title {
    [self.navBar setLeftTitle:title];
}

- (void)setNavRigTitle:(NSString *)title {
    [self.navBar setRightTitle:title];
}

- (void)rightBtnClick:(UIButton *)sender {
    [self navBarButtonClick:1];
}

//导航栏按钮点击时间
- (void)navBarButtonClick:(NSInteger )idx {
    switch (idx) {
        case 0:{
            if ([objc_getAssociatedObject(self, isModelPresent) boolValue]) {
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }else
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
}

/**
 *  标题按钮点击方法
 *
 *  @param button button
 */
- (void)navBarTitleClick:(UIButton *)button {
    /*
     supper code
     */
}

- (void)showNotData:(NSString *)label {
    UIImageView *img = [[UIImageView alloc] init];
    img.image = [UIImage imageNamed:@"Assert_noData"];
    img.bounds = CGRectMake(0, 0, 81, 81);
    img.center = self.view.center;
    img.tag = 9787;
    [self.view addSubview:img];
    UILabel *labl = [[UILabel alloc] init];
    labl.frame = CGRectMake(0, CGRectGetMaxY(img.frame), self.view.width, 120);
    labl.font = [UIFont systemFontOfSize:15];
    labl.textColor = PlaceHolderColor;
    labl.textAlignment = NSTextAlignmentCenter;
    labl.text = label;
    labl.tag = 9876;
    [self.view addSubview:labl];
}

- (void)showNotDataView:(UIView *)view lab:(NSString *)label {
    UIImageView *img = [[UIImageView alloc] init];
    img.image = [UIImage imageNamed:@"Assert_noData"];
    img.bounds = CGRectMake(0, 0, 81, 81);
    img.center = view.center;
    img.tag = 9787;
    [view addSubview:img];
    UILabel *labl = [[UILabel alloc] init];
    labl.frame = CGRectMake(0, CGRectGetMaxY(img.frame), self.view.width, 120);
    labl.font = [UIFont systemFontOfSize:15];
    labl.textColor = PlaceHolderColor;
    labl.textAlignment = NSTextAlignmentCenter;
    labl.text = label;
    labl.tag = 9876;
    [view addSubview:labl];
}

- (void)removeNotData {
    UILabel *lbl = (UILabel *)[self.view viewWithTag:9876];
    UIImageView *img = (UIImageView *)[self.view viewWithTag:9787];
    [lbl removeFromSuperview];
    [img removeFromSuperview];
}

- (void)showUnAuthAlert {
//    YJAlertView *alert = [[YJAlertView alloc] initWithTitle:@"提示" message:@"肥水不给圈外人，留言隐私有保障，软件功能仅对实名认证的医生用户开放哦~\n提交实名信息，2个工作日内短信会告知您结果。如果没有收到短信，随时联系小秘书：400-6860-925。" btnTitles:@[@"返回",@"去认证"] click:^(YJAlertView *alert, NSInteger index) {
//        if (index == 1) {
//            UIViewController *auth = [[UIViewController alloc] init];
//            UserInfo *info = [[CacheManager sharedMnager] getUserInfo];
//            [self.navigationController pushViewController:auth animated:YES];
//        }
//    }];
//    [alert show];
}


#pragma mark TableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (int)getRandomNumber:(int)from to:(int)to {
    return (int)(from + (arc4random() % (to - from + 1)));
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (!_can_scroll) {
//        [scrollView setContentOffset:CGPointZero];
//    }
//    if (scrollView.contentOffset.y <= 0) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"MainTableScroll" object:nil];
//    }
//    self.scrollview = scrollView;
}

- (void)childScrollStop:(NSNotification *)user {
//    if ([user.name isEqualToString:@"childScrollStop"]) {
//        self.can_scroll = NO;
//        [self.scrollview setContentOffset:CGPointZero];
//
//    }else if ([user.name isEqualToString:@"childScrollCan"]){
//
//        self.can_scroll = YES;
//    }else if ([user.name isEqualToString:@"MainTableScroll"]){
//
//        self.can_scroll = NO;
//        [self.scrollview setContentOffset:CGPointZero];
//    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)hideOtherCellLine:(UITableView *)tableView {
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

#pragma mark - Error handle
- (void)showInfo:(NSString*)str {
    [self showInfo:str andTitle:kLocalizedString(@"faceToastInfo")];
}


- (void)showInfo:(NSString*)str andTitle:(NSString *)title {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:str preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = ({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:NULL];
        action;
    });
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:NULL];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}





#pragma mark - 1. 创建导航栏
- (void)createNavigation {
    
    // 创建假的导航栏
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kNavBarAndStatusBarHeight)];
    self.navView.backgroundColor = kViewBackgroundColor;
    self.navView.layer.cornerRadius = 0;
    self.navView.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.navView.layer.shadowRadius = 1.0;
    self.navView.layer.shadowOpacity = 0;
    self.navView.layer.shadowColor = (KBigLine_Color).CGColor;
    
    // 标题
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(K375(64), kStatusBarHeight + 12, K375(247), kNavBarAndStatusBarHeight - (kStatusBarHeight + 14))];
    self.titleLabel.font = kFontBold18;
    self.titleLabel.textColor = KNavigationBar_TitleColor;
//    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.titleLabel];
    
    // 左侧按钮
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(K375(8), self.titleLabel.top, K375(56), self.titleLabel.height);
    [self.leftButton setImage:[UIImage textImageName:@"icon_back_0"] forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(leftButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.leftButton];

    // 右侧按钮
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(kScreen_Width - K375(64), self.leftButton.top, self.leftButton.width, self.leftButton.height);
    [self.rightButton addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.rightButton setTitleColor:KNavigationBar_TitleColor forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = kFontBold18;
    [self.navView addSubview:self.rightButton];
    
    // 分割线
    self.navLineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.navView.height - 1, kScreen_Width, 1)];
    self.navLineView.backgroundColor = KLine_Color;
    self.navLineView.hidden = YES;
    [self.navView addSubview:self.navLineView];
    
    // 在主线程异步加载，使下面的方法最后执行，防止其他的控件挡住了导航栏
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view addSubview:self.navView];
    });
}

#pragma mark - 2. 左侧返回按钮点击事件
- (void)leftButtonClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightButtonClick:(UIButton *)sender {}

#pragma mark - 3. 刷新导航栏样式
- (void)reloadNavigationStyle {
    self.navView.backgroundColor = kViewBackgroundColor;
    self.titleLabel.textColor = KNavigationBar_TitleColor;
    [self.leftButton setImage:[UIImage textImageName:@"icon_back_0"] forState:UIControlStateNormal];
}

#pragma mark - 4. 接收来网通知
- (void)comeNetNotification {
    
}

#pragma mark - 5. 后台进入前台
- (void)didBecomeActive {
    
}


#pragma mark - 6. 前台台进入后
- (void)didEnterBackground {
    
}




@end
