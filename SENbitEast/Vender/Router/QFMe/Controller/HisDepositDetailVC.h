//
//  HisDepositDetailVC.h
//  SENbitEast
//
//  Created by 张玮 on 2021/5/8.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "BaseViewController.h"
#import "AssertListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HisDepositDetailVC : BaseViewController

@property (nonatomic ,assign) NSInteger current;

@property (nonatomic ,strong) AssertListModel *model;

@end

NS_ASSUME_NONNULL_END
