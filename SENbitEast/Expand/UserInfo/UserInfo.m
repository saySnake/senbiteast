//
//  UserInfo.m
//  Senbit
//
//  Created by 张玮 on 2019/12/25.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

- (instancetype)initAnnounce:(NSMutableArray *)array {
    if (self = [super init]) {
        NSArray *announces = array;
        NSMutableArray *anns = [[NSMutableArray alloc] init];
        Banners *banners = [[Banners alloc] init];
        for (NSDictionary *dic in announces) {
            Banner *banner = [[Banner alloc] init];
            banner.title = dic[@"title"];
            banner.ann_url = dic[@"url"];
            [anns addObject:banner];
        }
        banners.anotiBanners = anns;
        self.banners = banners;
    }
    return self;
}


- (instancetype)initBanner:(NSMutableArray *)array {
    if (self = [super init]) {
        NSArray *bannerList = array;
        Banners *banners = [[Banners alloc] init];
        NSMutableArray *homeBanners = [NSMutableArray array];
           for (NSDictionary *dic in bannerList) {
            Banner *banner = [[Banner alloc] init];
            banner.banner_url = dic[@"img"];
            banner.target_url = dic[@"link"];
            banner.type = [dic[@"sort"] integerValue];
            switch (banner.type) {
                case SenlinkBannerTypeOne:
                    [homeBanners addObject:banner];
                    break;
                case SenlinkBannerTypeTwo:
                    [homeBanners addObject:banner];
                    break;
                case SenlinkBannerTypeThr:
                    [homeBanners addObject:banner];
                    break;
                case SenlinkBannerTypeFor:
                    [homeBanners addObject:banner];
                    break;
                case SenlinkBannerTypeFiv:
                    [homeBanners addObject:banner];
                    break;
                case SenlinkBannerTypeSix:
                    [homeBanners addObject:banner];
                    break;
                default:
                    break;
            }
        }
        banners.homeBanners = homeBanners;
        self.banners = banners;
    }
    return self;
}


- (instancetype)initWithJSON:(NSDictionary *)dict{
    if (self = [super init]) {
        u_int count = 0;
        objc_property_t *properties = class_copyPropertyList([self class], &count);
        for (int i = 0; i < count; i++) {
            const char* pname = property_getName(properties[i]);
            NSString *key = [NSString stringWithUTF8String:pname];
            id value = [dict valueForKey:key];
            [self setValue:value forKey:key];
        }
        free(properties);
        
//        NSArray *bannerList = dict[@"bannerList"];
//        Banners *banners = [[Banners alloc] init];
//        NSMutableArray *homeBanners = [NSMutableArray array];
//        NSMutableArray *drlineBanners = [NSMutableArray array];
//        NSMutableArray *orgBanners = [NSMutableArray array];
//        NSMutableArray *yjBanners = [NSMutableArray array];
//        NSMutableArray *adBanners = [NSMutableArray array];
//        NSMutableArray *luckBanners = [NSMutableArray array];
//
//        for (NSDictionary *dict in bannerList) {
//            Banner *banner = [[Banner alloc] init];
//            banner.banner_url = dict[@"banner_url"];
//            banner.target = dict[@"target"];
//            banner.target_url = dict[@"target_url"];
//            banner.type = [dict[@"type"] integerValue];
//            banner.target_title = dict[@"target_title"];
//            switch (banner.type) {
//                case SenlinkBannerTypeHome:
//                    [homeBanners addObject:banner];
//                    break;
//                case SenlinkBannerTypeDoctorLine:
//                    [drlineBanners addObject:banner];
//                    break;
//                case SenlinkBannerTypeOrg:
//                    [orgBanners addObject:banner];
//                    break;
//                case SenlinkBannerTypeYJ:
//                    [yjBanners addObject:banner];
//                    break;
//                case SenlinkBannerTypeAd:
//                    [adBanners addObject:banner];
//                    break;
//                case SenlinkBannerTypeLuck:
//                    [luckBanners addObject:banner];
//                    break;
//                    default:
//                    break;
//            }
//        }
//        banners.homeBanners = homeBanners;
//        banners.drLineBanners = drlineBanners;
//        banners.orgBanners = orgBanners;
//        banners.yjBanners = yjBanners;
//        banners.adBanners = adBanners;
//        banners.luckBanners = luckBanners;
//        self.banners = banners;
    }
    return self;
}

- (instancetype)initWithSearchJSON:(NSDictionary *)dict {
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - archiver
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self fastEncode:aCoder];
}

//解码
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        [self fastDecode:aDecoder];
    }
    return self;
}

@end

@implementation Banner

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        [self fastDecode:aDecoder];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self fastEncode:aCoder];
}

@end

@implementation Banners

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        [self fastDecode:aDecoder];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self fastEncode:aCoder];
}

@end
