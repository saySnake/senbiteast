//
//  GestureIntrdouceViewController.m
//  SENbitEast
//
//  Created by 张玮 on 2020/8/18.
//  Copyright © 2020 张玮. All rights reserved.
//

#import "GestureIntrdouceViewController.h"
#import "GestureViewController.h"

@interface GestureIntrdouceViewController ()<GestureViewDelegate>

@property (nonatomic ,strong) UIImageView *gestureImgView;
@property (nonatomic ,strong) BaseLabel *titleLb;
@property (nonatomic ,strong) BaseLabel *conentLb;
@property (nonatomic ,strong) BaseButton *openBtn;
@property (nonatomic ,strong) BaseButton *cancBtn;

@end

@implementation GestureIntrdouceViewController
- (void)dealloc {
    if (_gestureImgView) {
        _gestureImgView = nil;
    }
    if (!_conentLb) {
        _conentLb = nil;
    }
    
    if (_openBtn) {
        _openBtn = nil;
    }
    
    if (_cancBtn) {
        _cancBtn = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WhiteColor;
    [self setUpMainView];
}

- (void)setUpMainView {
    [self.view addSubview:self.titleLb];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(130);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
    }];
    
    [self.view addSubview:self.conentLb];
    [self.conentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(15);
    }];

    [self.view addSubview:self.gestureImgView];
    [self.gestureImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.conentLb);
        make.top.mas_equalTo(self.conentLb.mas_bottom).offset(20);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(100);
    }];

    [self.view addSubview:self.openBtn];
    [self.openBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.bottom.mas_equalTo(DScreenH).offset(- 120);
        make.height.mas_equalTo(45);
    }];
    
    [self.view addSubview:self.cancBtn];
    [self.cancBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.openBtn);
        make.right.mas_equalTo(self.openBtn);
        make.top.mas_equalTo(self.openBtn.mas_bottom).offset(15);
        make.height.mas_equalTo(45);
    }];
}


- (void)openAction {
    GestureViewController *gestureVC = [[GestureViewController alloc]init];
    gestureVC.delegate = self;
    [gestureVC showInViewController:self type:LZGestureTypeSetting];
}

#pragma mark - GestureViewDelegate
- (void)gestureView:(GestureViewController *)vc didSetted:(NSString *)psw {
    
//    [self.navigationController popViewControllerAnimated:NO];
    if (self.finishBlock) {
        self.finishBlock();
    }
    [GestureTool saveGesturePsw:psw];
    [GestureTool saveGestureEnableByUser:YES];
}

- (void)gestureViewCanceled:(GestureViewController *)vc {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)cancAction {
    [GestureTool saveGestureEnableByUser:NO];
    [GestureTool saveDelayEnable:YES];
    if (self.cancleBlock) {
        self.cancleBlock();
    }
}

- (UIImageView *)gestureImgView {
    if (!_gestureImgView) {
        _gestureImgView = [[UIImageView alloc] init];
        _gestureImgView.image = [UIImage imageNamed:@"gesture_introduce"];
        _gestureImgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _gestureImgView;
}

- (BaseLabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[BaseLabel alloc] init];
        _titleLb.textColor = ThemeTextColor;
        _titleLb.font = TwentyFontSize;
        _titleLb.textAlignment = 1;
        _titleLb.text = kLocalizedString(@"gesture_title");
    }
    return _titleLb;
}

- (BaseLabel *)conentLb {
    if (!_conentLb) {
        _conentLb = [[BaseLabel alloc] init];
        _conentLb.colorName = @"login_conentColor";
        _conentLb.textAlignment = 1;
        _conentLb.font = FourteenFontSize;
        _conentLb.text = kLocalizedString(@"gesture_conent");
    }
    return _conentLb;
}

- (BaseButton *)openBtn {
    if (!_openBtn) {
        _openBtn = [[BaseButton alloc] init];
        _openBtn.colorName = @"reset_confirmTitle";
        _openBtn.backColorName = @"login_loginBtnCor";
        [_openBtn setTitle:kLocalizedString(@"gesture_open") forState:UIControlStateNormal];
        _openBtn.titleLabel.font = TwentyFontSize;
        [_openBtn addTarget:self action:@selector(openAction) forControlEvents:UIControlEventTouchUpInside];
        _openBtn.cornerRadius = 5;
    }
    return _openBtn;
}

- (BaseButton *)cancBtn {
    if (!_cancBtn) {
        _cancBtn = [[BaseButton alloc] init];
        [_cancBtn setTitleColor:ThemeGreenColor forState:UIControlStateNormal];
        _cancBtn.backgroundColor = ClearColor;
        [_cancBtn setTitle:kLocalizedString(@"gesture_cancel") forState:UIControlStateNormal];
        _cancBtn.titleLabel.font = TwentyFontSize;
        [_cancBtn addTarget:self action:@selector(cancAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancBtn;
}


@end
