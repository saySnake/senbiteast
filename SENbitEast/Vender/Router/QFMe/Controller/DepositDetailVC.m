//
//  DepositDetailVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/28.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "DepositDetailVC.h"

@interface DepositDetailVC ()
@property (nonatomic ,strong) UILabel *countLb;
@property (nonatomic ,strong) UILabel *typeLb;
@property (nonatomic ,strong) UILabel *typeText;
@property (nonatomic ,strong) UILabel *line1;
@property (nonatomic ,strong) UILabel *statLb;
@property (nonatomic ,strong) UILabel *statText;
@property (nonatomic ,strong) UILabel *line2;
@property (nonatomic ,strong) UILabel *timeLb;
@property (nonatomic ,strong) UILabel *timeText;
@property (nonatomic ,strong) UILabel *line3;
@property (nonatomic ,strong) UILabel *txidLb;
@property (nonatomic ,strong) UILabel *txidText;

@end

@implementation DepositDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"充币明细"];
}

- (void)setUpView {
    [self.view addSubview:self.countLb];
    [self.countLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(10);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(25);
    }];
    [self.view addSubview:self.typeLb];
    [self.typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.countLb.mas_bottom).offset(40);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(20);
    }];
    [self.view addSubview:self.typeText];
    [self.typeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.typeLb);
        make.right.mas_equalTo(-20);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(25);
    }];
    [self.view addSubview:self.line1];
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.typeText.mas_bottom).offset(10);
        make.height.mas_equalTo(kLinePixel);
    }];
    
    [self.view addSubview:self.statLb];
    [self.statLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.line1.mas_bottom).offset(15);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(20);
    }];
    [self.view addSubview:self.statText];
    [self.statText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(self.statLb);
        make.width.mas_equalTo(140);
        make.right.mas_equalTo(-20);
    }];
    [self.view addSubview:self.line2];
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statText.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kLinePixel);
    }];
    [self.view addSubview:self.timeLb];
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.line2.mas_bottom).offset(15);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(20);
    }];
    [self.view addSubview:self.timeText];
    
    [self.timeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(self.timeLb);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(25);
    }];
    [self.view addSubview:self.line3];
    [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeText.mas_bottom).offset(15);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kLinePixel);
    }];
    [self.view addSubview:self.txidLb];
    [self.txidLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.line3.mas_bottom).offset(15);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(20);
    }];
    [self.view addSubview:self.txidText];
    [self.txidText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(150);
        make.top.mas_equalTo(self.txidLb);
    }];
}

- (UILabel *)countLb {
    if (!_countLb) {
        _countLb = [[UILabel alloc] init];
        _countLb.textAlignment = 0;
        _countLb.textColor = ThemeGreenColor;
        _countLb.font = kFont18;
        _countLb.text = @"";
    }
    return _countLb;
}

- (UILabel *)typeLb {
    if (!_typeLb) {
        _typeLb = [[UILabel alloc] init];
        _typeLb.textAlignment = 0;
        _typeLb.font = kFont15;
        _typeLb.textColor = ThemeLittleTitleColor;
        _typeLb.text = @"类型";
    }
    return _typeLb;
}

- (UILabel *)typeText {
    if (!_typeText) {
        _typeText = [[UILabel alloc] init];
        _typeText.textColor = [UIColor blackColor];
        _typeText.font = kFont15;
        _typeText.textAlignment = 2;
        _typeText.text = @"普通充币";
    }
    return _typeText;
}

- (UILabel *)line1 {
    if (!_line1) {
        _line1 = [[UILabel alloc] init];
        _line1.backgroundColor = HEXCOLOR(0xF7F6FB);
    }
    return _line1;
}

- (UILabel *)statLb {
    if (!_statLb) {
        _statLb = [[UILabel alloc] init];
        _statLb.textColor = ThemeLittleTitleColor;
        _statLb.font = kFont15;
        _statLb.textAlignment = 0;
        _statLb.text = @"状态";
    }
    return _statLb;
}

- (UILabel *)statText {
    if (!_statText) {
        _statText = [[UILabel alloc] init];
        _statText.textColor = [UIColor blackColor];
        _statText.font = kFont15;
        _statText.textAlignment = 2;
        _statText.text = @"进行中";
    }
    return _statText;
}

- (UILabel *)line2 {
    if (!_line2) {
        _line2 = [[UILabel alloc] init];
        _line2.textColor = HEXCOLOR(0xF7F6FB);
    }
    return _line2;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.textColor = ThemeLittleTitleColor;
        _timeLb.font = kFont15;
        _timeLb.textAlignment = 0;
        _timeLb.text = @"时间";
    }
    return _timeLb;
}

- (UILabel *)timeText {
    if (!_timeText) {
        _timeText = [[UILabel alloc] init];
        _timeText.text = @"13:14 04/07";
        _timeText.font = kFont15;
        _timeText.textAlignment = 2;
        _timeText.textColor = [UIColor blackColor];
    }
    return _timeText;
}

- (UILabel *)line3 {
    if (!_line3) {
        _line3 = [[UILabel alloc] init];
        _line3.textColor = HEXCOLOR(0xF7F6FB);
    }
    return _line3;
}

- (UILabel *)txidLb {
    if (!_txidLb) {
        _txidLb = [[UILabel alloc] init];
        _txidLb.textColor = ThemeLittleTitleColor;
        _txidLb.font = kFont15;
        _txidLb.textAlignment = 0;
        _txidLb.text = @"区块链交易ID";
    }
    return _txidLb;
}

- (UILabel *)txidText {
    if (!_txidText) {
        _txidText = [[UILabel alloc] init];
        _txidText.font = kFont15;
        _txidText.textAlignment = 2;
        _txidText.textColor = [UIColor blackColor];
        _timeText.numberOfLines = 0;
    }
    return _txidText;
}

@end
