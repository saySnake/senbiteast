//
//  SocketIOHandler.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/25.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SocketIOHandler : NSObject
+(SocketIOHandler*)shareInstance;
 
- (void)connect;
- (void)disconnect;
- (NSUUID * _Nonnull)on:(NSString * _Nonnull)event callback:(void (^ _Nonnull)(NSArray * _Nonnull data))callback;
- (void)emitWithAck:(NSString * _Nonnull)event with:(NSArray * _Nonnull)items callback:(void (^ _Nonnull)(NSArray * _Nonnull data))callback;
- (void)removeAllHandlers;
 
@property(nonatomic,assign)BOOL isNotConnected;
@property(nonatomic,assign)BOOL isDisConnected;
@end

NS_ASSUME_NONNULL_END
