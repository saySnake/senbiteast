//
//  BaseLabel.m
//  Senbit
//
//  Created by 张玮 on 2019/12/27.
//  Copyright © 2019 zhangwei. All rights reserved.
//

#import "BaseLabel.h"
#import "ThemeManager.h"

@implementation BaseLabel

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SThemeDidChangeNotification object:nil];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //监听主题切换的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
    }
    return self;
}

- (id)initWithTextColor:(NSString *)colorName {
    self = [super initWithFrame:CGRectZero];
    if (self != nil) {
        self.colorName = colorName;
        
        //监听主题切换的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
        
    }
    return self;
}


- (void)awakeFromNib {
    //监听主题切换的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeNotification:) name:SThemeDidChangeNotification object:nil];
}


- (void)setColorName:(NSString *)colorName {
    _colorName = colorName;
    UIColor *viewColor = [[ThemeManager shareInstance] getColorWithName:self.colorName];
    self.textColor = viewColor;
}

- (void)setBackColor:(NSString *)backColor {
    _backColor = backColor;
    UIColor *viewColor = [[ThemeManager shareInstance] getColorWithName:self.backColor];
    [self setBackgroundColor:viewColor];
}

- (void)themeNotification:(NSNotification *)notification {
    [self loadThemeImage];
    
}

- (void)loadThemeImage {
    if (self.colorName) {
        [self setColorName:self.colorName];
    }
    
    if (self.backColor) {
        [self setBackColor:self.backColor];
    }
}
@end
