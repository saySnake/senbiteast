//
//  SocketIOTools.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/23.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "SocketIOTools.h"
#import "SENbitEast-Bridging-Header.h"
#import <SocketIO/SocketIO-Swift.h>

@interface SocketIOTools()
{
    SocketIOClient *socket;//socket
     SocketManager *manager;

}
 
@end
 
 @implementation SocketIOTools
 
+ (SocketIOTools *)shareInstance {
    static SocketIOTools * userModel = nil ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (userModel == nil) {
            BOOL s = [IWDefault boolForKey:@"secure"];
            NSURL *url = [NSURL URLWithString:WssUrl];
            
            userModel = [[SocketManager alloc]
                            initWithSocketURL:url
                            config:@{
                                @"log": @NO,
                                @"forcePolling": @NO,
                                @"secure": @(s),
                                @"forceNew":@YES,
                                @"forceWebsockets":@YES,
                                @"selfSigned":@YES,
                                @"reconnectWait":@1000,
                                @"transports":@"websocket",
                                @"reconnection":@YES,
                                }];
            socket = [userModel defaultSocket];

//            userModel  =  [[SocketIOTools alloc] initWithSocketURL:url config:@{
//                                                                               @"log": @NO,
//                                                                               @"forcePolling": @NO,
//                                                                               @"secure": @(s),
//                                                                               @"forceNew":@YES,
//                                                                               @"forceWebsockets":@YES,
//                                                                               @"selfSigned":@YES,
//                                                                               @"reconnectWait":@1000,
//                                                                               @"transports":@"websocket",
//                                                                               @"reconnection":@YES,
//                                                                               }];
        }
    });
    return userModel;
}

- (nonnull instancetype)initWithSocketURL:(NSURL * _Nonnull)socketURL config:(NSDictionary * _Nullable)config{
    self = [super init];
    if (self) {
        socket  = [[SocketIOTools alloc] initWithSocketURL:socketURL config:config];
    }
    return self;
}
 
/**
 Connect to the server.
 */
- (void)connect{
    [socket connect];
}
/**
 Disconnects the socket.
 */
- (void)disconnect{
    [socket disconnect];
}
- (NSUUID * _Nonnull)on:(NSString * _Nonnull)event callback:(void (^)(NSArray * data))callback{
    return [socket on:event callback:^(NSArray*   data  , SocketAckEmitter*  ack) {
        callback(data);
    }];
    
}
- (void)emitWithAck:(NSString * _Nonnull)event with:(NSArray * _Nonnull)items callback:(void (^ _Nonnull)(NSArray * _Nonnull data))callback{
    [[socket emitWithAck:event with:items] timingOutAfter:0 callback:callback];
}
- (void)removeAllHandlers{
    [socket removeAllHandlers];
}
- (void)dealloc{
    [socket removeAllHandlers];
    [socket disconnect];
    socket =  nil;
}
- (BOOL)isDisConnected {
    return socket.status == 0;//SocketIOClientStatusDisconnected;
}
- (BOOL)isNotConnected {
    return socket.status == 1;//SocketIOClientStatusNotConnected;
}
@end
