//
//  HomeCoinPairModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/26.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCoinPairModel : NSObject

/** ID **/
@property (nonatomic ,copy) NSString *ID;
/** cnName **/
@property (nonatomic ,copy) NSString *cnName;
/** coinIcon **/
@property (nonatomic ,copy) NSString *coinIcon;
/** defaultActive **/
@property (nonatomic ,copy) NSString *defaultActive;
/** enFullName **/
@property (nonatomic ,copy) NSString *enFullName;
/** enFullName **/
@property (nonatomic ,copy) NSString *coinName;


@end

NS_ASSUME_NONNULL_END
