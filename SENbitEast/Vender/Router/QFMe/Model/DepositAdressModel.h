//
//  DepositAdressModel.h
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DepositAdressModel : NSObject
/** address **/
@property (nonatomic ,copy) NSString *address;
/** tag **/
@property (nonatomic ,copy) NSString *memo;

@end

NS_ASSUME_NONNULL_END
