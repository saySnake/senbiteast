//
//  ModifyVC.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/14.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "ModifyVC.h"

@interface ModifyVC ()
@property (nonatomic ,strong) UILabel *titleLb;
@property (nonatomic ,strong) UILabel *contentLb;
@property (nonatomic ,strong) BaseField *accountTF;
@property (nonatomic ,strong) BaseField *neAccountTF;
@property (nonatomic ,strong) BaseField *reNewAccountTF;
@property (nonatomic ,strong) UIButton *confirBtn;


@property (nonatomic ,strong) UIButton *scrBtn;
@property (nonatomic ,strong) UIButton *scrBtn2;
@property (nonatomic ,strong) UIButton *scrBtn3;

@end

@implementation ModifyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navBar.backgroundColor = WhiteColor;
//    [self setNavBarTitle:kLocalizedString(@"modify_title")]; //kLocalizedString(@"search")];
    [self setLeftBtnImage:@"leftBack"];
    [self configKeyBoardRespond];
    [self setUpView];
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] initWithKeyboardTopMargin:MARGIN_KEYBOARD];
    __weak ModifyVC *weakSelf = self;
    [self.keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.accountTF,weakSelf.neAccountTF,weakSelf.reNewAccountTF, nil];
    }];
    #pragma explain - 获取键盘信息
    [self.keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}

- (void)setUpView {
    [self.view addSubview:self.titleLb];
    [self.view addSubview:self.contentLb];
    [self.view addSubview:self.accountTF];
    [self.view addSubview:self.scrBtn];
    [self.view addSubview:self.neAccountTF];
    [self.view addSubview:self.scrBtn2];
    [self.view addSubview:self.reNewAccountTF];
    [self.view addSubview:self.scrBtn3];
    [self.view addSubview:self.confirBtn];
    [self make_layout];
}

- (void)make_layout {
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.navBar.mas_bottom).offset(75);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(40);
    }];
    
    [self.contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLb);
        make.top.mas_equalTo(self.titleLb.mas_bottom).offset(25);
        make.width.mas_equalTo(3000);
        make.height.mas_equalTo(20);
    }];
    
    [self.accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentLb);
        make.top.mas_equalTo(self.contentLb.mas_bottom).offset(60);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(50);
    }];
    
    [self.scrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.accountTF.mas_right).offset(-15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.accountTF.mas_top).offset(30);
        make.width.mas_equalTo(15);
    }];

    [self.neAccountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountTF);
        make.right.mas_equalTo(self.accountTF);
        make.top.mas_equalTo(self.accountTF.mas_bottom).offset(60);
        make.height.mas_equalTo(50);
    }];
    
    [self.scrBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.neAccountTF.mas_right).offset(-15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.neAccountTF.mas_top).offset(30);
        make.width.mas_equalTo(15);
    }];


    [self.reNewAccountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.neAccountTF);
        make.top.mas_equalTo(self.neAccountTF.mas_bottom).offset(60);
        make.height.mas_equalTo(50);
    }];
    
    [self.scrBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.reNewAccountTF.mas_right).offset(-15);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.reNewAccountTF.mas_top).offset(30);
        make.width.mas_equalTo(15);
    }];


    [self.confirBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.reNewAccountTF);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(self.reNewAccountTF.mas_bottom).offset(60);
    }];
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.textColor = [UIColor blackColor];
        _titleLb.textAlignment = 0;
        _titleLb.font = BOLDSYSTEMFONT(30);
        _titleLb.text = kLocalizedString(@"modify_logPwd");
        _titleLb.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLb;
}

- (UILabel *)contentLb {
    if (!_contentLb) {
        _contentLb = [[UILabel alloc] init];
        _contentLb.text = kLocalizedString(@"modify_modifyPwd");
        _contentLb.textAlignment = 0;
        _contentLb.font = ThirteenFontSize;
        _contentLb.textColor = HEXCOLOR(0x666666);
        _contentLb.adjustsFontSizeToFitWidth = YES;
    }
    return _contentLb;
}

- (BaseField *)accountTF {
    if (!_accountTF) {
        _accountTF = [[BaseField alloc] init];
        _accountTF.placeholder = kLocalizedString(@"modify_account");
        _accountTF.placeHolderColor = PlaceHolderColor;
        _accountTF.textField.secureTextEntry = YES;
        [_accountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTF;
}

- (BaseField *)neAccountTF {
    if (!_neAccountTF) {
        _neAccountTF = [[BaseField alloc] init];
        _neAccountTF.placeholder = kLocalizedString(@"modify_password");
        _neAccountTF.placeHolderColor = PlaceHolderColor;
        _neAccountTF.textField.secureTextEntry = YES;
        [_neAccountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _neAccountTF;
}

- (BaseField *)reNewAccountTF {
    if (!_reNewAccountTF) {
        _reNewAccountTF = [[BaseField alloc] init];
        _reNewAccountTF.placeholder = kLocalizedString(@"modify_nePassword");
        _reNewAccountTF.placeHolderColor = PlaceHolderColor;
        _reNewAccountTF.textField.secureTextEntry = YES;
        [_reNewAccountTF.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _reNewAccountTF;
}

- (UIButton *)confirBtn {
    if (!_confirBtn) {
        _confirBtn = [[UIButton alloc] init];
        _confirBtn.cornerRadius = 3;
        [_confirBtn setTitle:kLocalizedString(@"modify_confirm") forState:UIControlStateNormal];
        [_confirBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
        _confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        _confirBtn.userInteractionEnabled = NO;
    }
    return _confirBtn;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.accountTF.textField.text.length > 0 && self.neAccountTF.textField.text.length > 0 && self.reNewAccountTF.textField.text.length > 0) {
        self.confirBtn.backgroundColor = ThemeGreenColor;
        self.confirBtn.userInteractionEnabled = YES;

    } else {
        self.confirBtn.backgroundColor = HEXCOLOR(0xC9C9D6);
        self.confirBtn.userInteractionEnabled = NO;
    }
}


- (void)confirm:(UIButton *)sender {
    BOOL judge = [NSString judgePassWordLegal:self.accountTF.textField.text];
    if (judge == NO) {
        [self showToastView:kLocalizedString(@"toast_pwdprotocol")];
        return;
    }

    if (self.accountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"modify_pleaseOrigin")];
        return;
    }
    
    if (self.neAccountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"modify_pleaseNewPwd")];
        return;
    }
    
    if (self.reNewAccountTF.textField.text.length == 0) {
        [self showToastView:kLocalizedString(@"modify_pleaseConfirmPwd")];
        return;
    }
    
    if ([self.accountTF.textField.text isEqualToString:self.neAccountTF.textField.text]) {
        [self showToastView:kLocalizedString(@"modify_samepwd")];
        return;
    }
    
    if (![self.reNewAccountTF.textField.text isEqualToString:self.neAccountTF.textField.text]) {
        [self showToastView:kLocalizedString(@"modify_scrNot")];
        return;
    }
    
    NSDictionary *dic = @{@"originalPassword":self.accountTF.textField.text,
                          @"newPassword":self.reNewAccountTF.textField.text
    };
    [SBNetworTool patchWithUrl:EastPassword params:dic isReadCache:NO success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@",responseObject);
        if (SuccessCode == 200) {
            [self showToastView:kLocalizedString(@"modify_success")];
            [self delay:1 task:^{
                [[CacheManager sharedMnager] clearCache];
                [self.navigationController popToRootViewControllerAnimated:YES];
                [IWNotificationCenter postNotificationName:KLoadLeftView object:nil];
            }];
        } else {
            [self showToastView:SuccessMessage];
        }
    } failed:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull error, id  _Nonnull responseObject) {
        [self showToastView:FailurMessage];
    }];
}



- (UIButton *)scrBtn {
    if (!_scrBtn) {
        _scrBtn = [[UIButton alloc] init];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn addTarget:self action:@selector(scr:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn;
}

- (UIButton *)scrBtn2 {
    if (!_scrBtn2) {
        _scrBtn2 = [[UIButton alloc] init];
        [_scrBtn2 setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn2 setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn2 addTarget:self action:@selector(scr2:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn2;
}

- (UIButton *)scrBtn3 {
    if (!_scrBtn3) {
        _scrBtn3 = [[UIButton alloc] init];
        [_scrBtn3 setImage:[UIImage imageNamed:@"assert_notSee"] forState:UIControlStateNormal];
        [_scrBtn3 setImage:[UIImage imageNamed:@"assert_see"] forState:UIControlStateSelected];
        [_scrBtn3 addTarget:self action:@selector(scr3:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scrBtn3;
}

- (void)scr:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.accountTF.textField.secureTextEntry = NO;
    } else {
        self.accountTF.textField.secureTextEntry = YES;
    }
}

- (void)scr2:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.neAccountTF.textField.secureTextEntry = NO;
    } else {
        self.neAccountTF.textField.secureTextEntry = YES;
    }
}

- (void)scr3:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.reNewAccountTF.textField.secureTextEntry = NO;
    } else {
        self.reNewAccountTF.textField.secureTextEntry = YES;
    }
}



@end
