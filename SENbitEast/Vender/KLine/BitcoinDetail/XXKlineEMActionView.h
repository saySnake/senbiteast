//
//  XXKlineEMActionView.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/20.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XXKlineEMActionView : UIView

/** 主图按钮数组 */
@property (strong, nonatomic) NSMutableArray *mainButtonsArray;

/** 副图按钮数组 */
@property (strong, nonatomic) NSMutableArray *fButtonsArray;

/** 分钟按钮 */
@property (strong, nonatomic) XXButton *minuteButton;

/** 是否处于显示状态 */
@property (assign, nonatomic) BOOL isShow;

- (void)show;

- (void)dismiss;

- (void)reloadUI;

/** 按钮回调 */
@property (strong, nonatomic) void(^kActionBlock)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
