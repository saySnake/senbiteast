//
//  LoginSheet.h
//  SENbitEast
//
//  Created by 张玮 on 2021/3/23.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    sheetTypeLogin, //登录
    sheetTypeBlindGoogle, //绑定谷歌
    sheetTypeModifyGoogle //更换谷歌
} sheetType;

NS_ASSUME_NONNULL_BEGIN
typedef void (^ClickBlock)(NSString *str);

@interface LoginSheet : UIView

@property (nonatomic, assign) sheetType type;

@property (nonatomic ,strong) UILabel *safeTitle;

@property (nonatomic ,strong) UIButton *cancBtn;

@property (nonatomic ,strong) UILabel *line1;

@property (nonatomic ,strong) UITextField *msgTF;

@property (nonatomic ,strong) UILabel *line2;

@property (nonatomic ,strong) UIButton *copBtn;

@property (nonatomic ,strong) UIButton *confirBtn;

@property (nonatomic ,strong) NSString *token;

@property (nonatomic ,copy) ClickBlock block;

@property (nonatomic ,copy) NSString *titleName;


- (void)showFromView:(UIView *)view;

+ (instancetype)customActionSheet;

@end

NS_ASSUME_NONNULL_END
