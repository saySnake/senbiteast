//
//  SocketIOHandler.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/25.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "SocketIOHandler.h"
@import SocketIO;


@interface SocketIOHandler ()  {
    
}

@property (strong, nonatomic) SocketManager *manager;
@property (strong, nonatomic) SocketIOClient *socket;

@end


@implementation SocketIOHandler
+ (SocketIOHandler *)shareInstance{
    static SocketIOHandler * userModel = nil ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (userModel == nil) {
            NSURL *url = [NSURL URLWithString:WssUrl];
            userModel  =  [[SocketIOHandler alloc]initWithSocketURL:url config:@{@"log":@"YES"}];
        }
    });
    return userModel;
}


- (nonnull instancetype)initWithSocketURL:(NSURL * _Nonnull)socketURL config:(NSDictionary * _Nullable)config{
    self = [super init];
    if (self) {
        BOOL s = [IWDefault boolForKey:@"secure"];
        self.manager = [[SocketManager alloc] initWithSocketURL:socketURL config:@{
                                                                                    @"log": @NO,
                                                                                    @"forcePolling": @NO,
                                                                                    @"secure": @(s),
                                                                                    @"forceNew":@YES,
                                                                                    @"forceWebsockets":@YES,
                                                                                    @"selfSigned":@YES,
                                                                                    @"reconnectWait":@1000,
                                                                                    @"transports":@"websocket",
                                                                                    @"reconnection":@YES,
                                                                                    }];
        self.socket = [self.manager defaultSocket];
    }
    return self;
}


/**
 Connect to the server.
 */
- (void)connect{
    [self.socket connect];
}
/**
 Disconnects the socket.
 */
- (void)disconnect{
    [self.socket disconnect];
}
- (NSUUID * _Nonnull)on:(NSString * _Nonnull)event callback:(void (^)(NSArray * data))callback{
    return [self.socket on:event callback:^(NSArray*   data  , SocketAckEmitter*  ack) {
        callback(data);
    }];
    
}
- (void)emitWithAck:(NSString * _Nonnull)event with:(NSArray * _Nonnull)items callback:(void (^ _Nonnull)(NSArray * _Nonnull data))callback{
    [[self.socket emitWithAck:event with:items] timingOutAfter:0 callback:callback];
}
- (void)removeAllHandlers{
    [self.socket removeAllHandlers];
}
- (void)dealloc{
    [self.socket removeAllHandlers];
    [self.socket disconnect];
    self.socket =  nil;
}
- (BOOL)isDisConnected{
    return self.socket.status == 1;//SocketIOClientStatusDisconnected;
}
- (BOOL)isNotConnected{
    return self.socket.status == 0;//SocketIOClientStatusNotConnected;
}
@end
