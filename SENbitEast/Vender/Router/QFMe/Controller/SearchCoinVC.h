//
//  SearchCoinVC.h
//  Senbit
//
//  Created by 张玮 on 2020/2/20.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "BaseViewController.h"
#import "CoinModel.h"
#import "WithCoinListModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol SearchCoinVCDelegate <NSObject>

@optional

- (void)selectCoin:(CoinModel *)model;

- (void)selectWithDrawCoin:(WithCoinListModel *)mode;

@end


@interface SearchCoinVC : BaseViewController

@property (nonatomic ,assign) BOOL isWithDraw;

@property (nonatomic ,strong) NSMutableDictionary *dic;

@property (nonatomic ,assign) id<SearchCoinVCDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
