//
//  TradeDetailHeaderVIew.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "TradeDetailHeaderVIew.h"

@implementation TradeDetailHeaderVIew

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
    }
    return self;
}

- (void)setUpview {
    [self addSubview:self.scroll];
    [self.scroll addSubview:self.line];
    [self.scroll addSubview:self.direct];
    [self.scroll addSubview:self.coin];
    [self.scroll addSubview:self.timeLb];
    [self.scroll addSubview:self.entrustLb];
    [self.scroll addSubview:self.entrustNum];
    [self.scroll addSubview:self.timeText];
    [self.scroll addSubview:self.entrustText];
    [self.scroll addSubview:self.entrustNumText];
    [self.scroll addSubview:self.totalLb];
    [self.scroll addSubview:self.typeLb];
    [self.scroll addSubview:self.orderNumLb];
    [self.scroll addSubview:self.totalLbText];
    [self.scroll addSubview:self.typeLbText];
    [self.scroll addSubview:self.orderText];
    
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW);
        make.height.mas_equalTo(250);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
   
    [self.direct mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(0);
        make.width.mas_lessThanOrEqualTo(100);
        make.height.mas_equalTo(25);
    }];
    
    [self.coin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.direct.mas_right);
        make.top.mas_equalTo(self.direct);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(self.direct);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.width.mas_equalTo(DScreenW);
        make.top.mas_equalTo(self.direct.mas_bottom);
    }];
    
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.direct.mas_bottom).offset(15);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(15);
    }];
    
    [self.entrustLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self.timeLb);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(self.timeLb);
    }];
    
    [self.entrustNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.width.mas_equalTo(120);
        make.top.mas_equalTo(self.entrustLb);
        make.height.mas_equalTo(self.entrustLb);
    }];
    
    [self.timeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.timeLb.mas_bottom).offset(10);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(20);
    }];

    [self.entrustText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.entrustLb);
        make.top.height.width.mas_equalTo(self.timeText);
    }];

    [self.entrustNumText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.height.mas_equalTo(self.entrustText);
        make.right.mas_equalTo(self.entrustNum);
    }];
    
    [self.totalLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(self.timeText.mas_bottom).offset(20);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(15);
    }];
    
    [self.typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.width.height.mas_equalTo(self.totalLb);
    }];
    
    [self.orderNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.width.height.mas_equalTo(self.typeLb);
    }];
    
    [self.totalLbText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.totalLb.mas_bottom).offset(15);
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(20);
    }];
    
    [self.typeLbText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.width.height.mas_equalTo(self.totalLbText);
    }];
    
    [self.orderText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.width.height.top.mas_equalTo(self.typeLbText);
    }];
    
    
}

#pragma  mark -Lay
- (UIScrollView *)scroll {
    if (!_scroll) {
        _scroll = [[UIScrollView alloc] init];
        _scroll.showsHorizontalScrollIndicator = NO;//隐藏水平滚动条
        _scroll.alwaysBounceVertical = NO;
        _scroll.backgroundColor = [UIColor whiteColor];
        _scroll.scrollEnabled = NO;
    }
    return _scroll;
}

- (UILabel *)line {
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = [UIColor whiteColor];
    }
    return _line;
}

- (UILabel *)direct {
    if (!_direct) {
        _direct = [[UILabel alloc] init];
        _direct.textColor = ThemeGreenColor;
        _direct.textAlignment = 0;
        _direct.font = kFont18;
    }
    return _direct;
}

- (UILabel *)coin {
    if (!_coin) {
        _coin = [[UILabel alloc] init];
        _coin.textColor = [UIColor blackColor];
        _coin.textAlignment = 0;
        _coin.font = kFont18;
    }
    return _coin;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.textAlignment = 0;
        _timeLb.font = kFont15;
        _timeLb.textColor = ThemeLittleTitleColor;
        _timeLb.text = kLocalizedString(@"detail_time");
    }
    return _timeLb;
}

- (UILabel *)entrustLb {
    if (!_entrustLb) {
        _entrustLb = [[UILabel alloc] init];
        _entrustLb.textColor = ThemeLittleTitleColor;
        _entrustLb.font = kFont15;
        _entrustLb.textAlignment = 1;
        _entrustLb.text = kLocalizedString(@"detail_entrustPice");
    }
    return _entrustLb;
}

- (UILabel *)entrustNum {
    if (!_entrustNum) {
        _entrustNum = [[UILabel alloc] init];
        _entrustNum.textColor = ThemeLittleTitleColor;
        _entrustNum.font = kFont15;
        _entrustNum.textAlignment = 2;
        _entrustNum.text = kLocalizedString(@"detail_entrustNum");
    }
    return _entrustNum;
}

- (UILabel *)timeText {
    if (!_timeText) {
        _timeText = [[UILabel alloc] init];
        _timeText.textColor = [UIColor blackColor];
        _timeText.textAlignment = 0;
        _timeText.font = kFont15;
    }
    return _timeText;
}

- (UILabel *)entrustText {
    if (!_entrustText) {
        _entrustText = [[UILabel alloc] init];
        _entrustText.textColor = [UIColor blackColor];
        _entrustText.textAlignment = 1;
        _entrustText.font = kFont15;
    }
    return _entrustText;
}

- (UILabel *)entrustNumText {
    if (!_entrustNumText) {
        _entrustNumText = [[UILabel alloc] init];
        _entrustNumText.textAlignment = 2;
        _entrustNumText.font = kFont15;
        _entrustNumText.textColor = [UIColor blackColor];
    }
    return _entrustNumText;
}

- (UILabel *)totalLb {
    if (!_totalLb) {
        _totalLb = [[UILabel alloc] init];
        _totalLb.font = kFont15;
        _totalLb.textColor = ThemeLittleTitleColor;
        _totalLb.textAlignment = 0;
        _totalLb.text = kLocalizedString(@"detail_total");
    }
    return _totalLb;
}

- (UILabel *)typeLb {
    if (!_typeLb) {
        _typeLb = [[UILabel alloc] init];
        _typeLb.textColor = ThemeLittleTitleColor;
        _typeLb.font = kFont15;
        _typeLb.textAlignment = 1;
        _typeLb.text = kLocalizedString(@"detail_average");
    }
    return _typeLb;
}

- (UILabel *)orderNumLb {
    if (!_orderNumLb) {
        _orderNumLb = [[UILabel alloc] init];
        _orderNumLb.textColor = ThemeLittleTitleColor;
        _orderNumLb.font = kFont15;
        _orderNumLb.textAlignment = 2;
        _orderNumLb.text = kLocalizedString(@"detail_tradeNum");
    }
    return _orderNumLb;
}

- (UILabel *)totalLbText {
    if (!_totalLbText) {
        _totalLbText = [[UILabel alloc] init];
        _totalLbText.textAlignment = 0;
        _totalLbText.font = kFont15;
        _totalLbText.textColor = [UIColor blackColor];
    }
    return _totalLbText;
}

- (UILabel *)typeLbText {
    if (!_typeLbText) {
        _typeLbText = [[UILabel alloc] init];
        _typeLbText.textColor = [UIColor blackColor];
        _typeLbText.font = kFont15;
        _typeLbText.textAlignment = 1;
    }
    return _typeLbText;
}

- (UILabel *)orderText {
    if (!_orderText) {
        _orderText = [[UILabel alloc] init];
        _orderText.textColor = [UIColor blackColor];
        _orderText.font = kFont15;
        _orderText.textAlignment = 2;
    }
    return _orderText;
}


- (void)setModel:(TradeListModel *)model {
    _model = model;
    if ([model.tradeType isEqualToString:@"buy"]) {
        self.direct.textColor = ThemeGreenColor;
        self.direct.text = kLocalizedString(@"detail_buy");
    } else {
        self.direct.textColor = ThemeRedColor;
        self.direct.text = kLocalizedString(@"detail_sel");
    }
    self.coin.text = model.market;
    self.timeText.text = model.createdAt;
    self.entrustText.text = model.price;
    self.entrustNumText.text = model.originVolume;
    self.totalLbText.text = [NSString mulV1:model.originVolume v2:model.price];
    self.typeLbText.text = model.orderType;
    self.orderText.text = [NSString subV1:model.originVolume v2:model.volume];
}

@end
