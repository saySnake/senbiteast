//
//  CycleImageViewCell.h
//  CycleIMG
//
//  Created by 张玮 on 2020/1/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneTableSockeModel.h"
@interface CycleImageViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *usdName;
@property (weak, nonatomic) IBOutlet UILabel *perName;
@property (weak, nonatomic) IBOutlet UILabel *cnyName;
@property (nonatomic ,strong) OneTableSockeModel *model;
@end
