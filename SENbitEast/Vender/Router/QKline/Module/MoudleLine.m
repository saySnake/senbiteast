//
//  MoudleLine.m
//  SENbitEast
//
//  Created by 张玮 on 2021/4/19.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "MoudleLine.h"
#import "XXBitcoinDetailVC.h"

@implementation MoudleLine

@synthesize callbackBlock;

@synthesize detailViewController;

@synthesize interfaceViewController;

@synthesize paramterForHome;

@synthesize titleString;

@synthesize descString;

- (UIViewController *)interfaceViewController {
    XXBitcoinDetailVC *homeViewController = [[XXBitcoinDetailVC alloc]init];
    homeViewController.interface = self;
    interfaceViewController = (UIViewController *)homeViewController;
    
    return interfaceViewController;
}

@end
