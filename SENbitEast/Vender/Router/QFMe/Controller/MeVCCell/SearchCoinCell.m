//
//  SearchCoinCell.m
//  Senbit
//
//  Created by 张玮 on 2020/4/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "SearchCoinCell.h"

@implementation SearchCoinCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"SearchCoinCell";
    SearchCoinCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[SearchCoinCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.backgroundColor = [UIColor colorWithLightColorStr:@"FFFFFF" DarkColor:@"FFFFFF"];
        cell.contentView.backgroundColor = [UIColor colorWithLightColorStr:@"FFFFFF" DarkColor:@"FFFFF"];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor colorWithLightColorStr:@"FFFFFF" DarkColor:@"FFFFFF"];
        [self setUpView];
    }
    return self;
}

- (void)setFrendModel:(CoinModel *)frendModel {
    _frendModel = frendModel;
    self.coinName.text = frendModel.coinName;
    if (_frendModel.able2charge == NO) {
        self.anableLb.text = kLocalizedString(@"stopDeposit");
        self.anableLb.hidden = NO;
        self.coinName.textColor = HEXCOLOR(0xDDDDDD);
    } else {
        self.anableLb.hidden = YES;
        self.coinName.textColor = MainWhiteColor;
    }
}


- (void)setIsWithdraw:(BOOL)isWithdraw {
    _isWithdraw = isWithdraw;

}

- (void)setWithDrawModel:(WithCoinListModel *)withDrawModel {
    _withDrawModel = withDrawModel;
    self.coinName.text = withDrawModel.coinName;
    if (self.withDrawModel.able2withdraw == NO ) {
        self.anableLb.text = kLocalizedString(@"stopWithdraw");

        self.anableLb.hidden = NO;
        self.coinName.textColor = HEXCOLOR(0xDDDDDD);
    } else {
        self.anableLb.hidden = YES;
        self.coinName.textColor = MainWhiteColor;
    }
}

- (void)setUpView {
    [self coinName];
}

- (UILabel *)coinName {
    if (!_coinName) {
        _coinName = [[UILabel alloc] init];
        _coinName.textAlignment = 0;
        _coinName.textColor = MainWhiteColor;
        _coinName.font = FourteenFontSize;
        [self.contentView addSubview:_coinName];
        [_coinName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(21);
            make.top.mas_equalTo(12);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(12);
        }];
    }
    return _coinName;
}


- (UILabel *)anableLb {
    if (!_anableLb) {
        _anableLb = [[UILabel alloc] init];
        _anableLb.textAlignment = 2;
        _anableLb.textColor = HEXCOLOR(0xDDDDDD);
        _anableLb.font = ThirteenFontSize;
        _anableLb.hidden = YES;
        _anableLb.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_anableLb];
        [_anableLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-50);
            make.top.mas_equalTo(12);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(15);
        }];
    }
    return _anableLb;
}


@end
