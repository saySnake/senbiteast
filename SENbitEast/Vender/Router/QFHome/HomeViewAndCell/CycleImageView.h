//
//  CycleImageView.h
//  CycleIMG
//
//  Created by 张玮 on 2020/1/3.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneTableSockeModel.h"

typedef NS_ENUM(NSUInteger, MovementDirectionType) {
    MovementDirectionForHorizontally = 0,
    MovementDirectionVertically,
};

@protocol CycleImageViewDelegate <NSObject>

@optional

- (void)didSelectItemAtIndex:(OneTableSockeModel *)index;

@end

@interface CycleImageView : UIView

@property(nonatomic,weak)id<CycleImageViewDelegate> delegate;

@property(nonatomic,assign)MovementDirectionType movementDirection;

@property(nonatomic,assign)BOOL hidePageControl;

@property(nonatomic,assign)BOOL canFingersSliding;

@end
