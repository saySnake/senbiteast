//
//  OtcExtinfo.m
//  Senbit
//
//  Created by 张玮 on 2020/4/6.
//  Copyright © 2020 zhangwei. All rights reserved.
//

#import "OtcExtinfo.h"

@implementation OtcExtinfo

- (instancetype)initWithOtcCoins:(NSArray *)array {
    if (self = [super init]) {
        
        NSArray *arr  = array;
        NSMutableArray *otcA = [[NSMutableArray alloc] init];
        OtcCoins * coin = [[OtcCoins alloc] init];
//        for (NSDictionary *dic in arr) {
//            OTCCoinModel *mo = [[OTCCoinModel alloc] init];
//            mo.otcDefaultActive = [dic[@"otcDefaultActive"] boolValue];
//            mo.otcSellLowPrice = dic[@"otcSellLowPrice"];
//            mo.otcSellHighPrice = dic[@"otcSellHighPrice"];
//            mo.otcBuyLowPrice = dic[@"otcBuyLowPrice"];
//            mo.otcBuyHighPrice = dic[@"otcBuyHighPrice"];
//            mo.ID = dic[@"_id"];
//            mo.coinName = dic[@"id"];
//            mo.enName = dic[@"enName"];
//            mo.enFullName = dic[@"enFullName"];
//            mo.coinIcon = dic[@"coinIcon"];
//            [otcA addObject:mo];
//        }
//        coin.otcArr = otcA;
        self.otcModel = coin;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.CNY forKey:@"CNY"];
    [coder encodeObject:self.KRW forKey:@"KRW"];
    [coder encodeObject:self.HKD forKey:@"HKD"];
    [coder encodeObject:self.USDT forKey:@"USDT"];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        self.CNY = [coder decodeObjectForKey:@"CNY"];
        self.KRW = [coder decodeObjectForKey:@"KRW"];
        self.HKD = [coder decodeObjectForKey:@"HKD"];
        self.USDT = [coder decodeObjectForKey:@"USDT"];
    }
    return self;
}

//#pragma mark - archiver
//- (void)encodeWithCoder:(NSCoder *)aCoder {
//    [self fastEncode:aCoder];
//}
//
////解码
//- (id)initWithCoder:(NSCoder *)aDecoder {
//    if (self = [super init]) {
//        [self fastDecode:aDecoder];
//    }
//    return self;
//}

@end

@implementation OtcCoins

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        [self fastDecode:aDecoder];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [self fastEncode:aCoder];
}


@end
