//
//  BalanceModel.h
//  SENbitEast
//
//  Created by 张玮 on 2021/4/2.
//  Copyright © 2021 张玮. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BalanceModel : NSObject
@property (nonatomic ,strong) NSString *balance;

@property (nonatomic ,strong) NSString *btcRate;

@property (nonatomic ,strong) NSString *currency;

@property (nonatomic ,strong) NSString *freezed;

@property (nonatomic ,strong) NSString *locked;

@property (nonatomic ,strong) NSString *usdtRate;
@end

NS_ASSUME_NONNULL_END
