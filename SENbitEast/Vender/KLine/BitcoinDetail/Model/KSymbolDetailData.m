//
//  KSymbolDetailData.m
//  iOS
//
//  Created by YiHeng on 2020/2/4.
//  Copyright © 2020 iOS. All rights reserved.
//

#import "KSymbolDetailData.h"

@implementation KSymbolDetailData
singleton_implementation(KSymbolDetailData)

- (void)setSymbolModel:(XXSymbolModel *)symbolModel {
    _symbolModel = symbolModel;
//    NSArray *digitMergeArray = [_symbolModel.digitMerge componentsSeparatedByString:@","];
//    self.priceDigit = [KDecimal scale:[digitMergeArray lastObject]];
//    self.priceDigit = [KDecimal scale:symbolModel.quotePrecision];
//    self.numberDigit = [KDecimal scale:_symbolModel.basePrecision];
}

//- (void)setPriceDigit:(NSInteger)priceDigit {
//    self.priceDigit = [KDecimal scale:KDetail.symbolModel.quotePrecision];
//    self.numberDigit = [KDecimal scale:KDetail.symbolModel.basePrecision];
//}
//


- (NSMutableArray *) symbolsArray {
    NSString * symbolString = [[NSUserDefaults standardUserDefaults] objectForKey:@"allSymbolsStringKey"];
    NSMutableArray *aa = [NSMutableArray arrayWithArray:[symbolString mj_JSONObject]];
    NSMutableArray *bb = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < aa.count; i ++) {
        XXSymbolModel *cModel = [XXSymbolModel mj_objectWithKeyValues:aa[i]];
        cModel.quote = [XXQuoteModel new];
        cModel.type = SymbolTypeCoin;
        [bb addObject:cModel];
    }
    return bb;
}

- (void)setSymbolsArray:(NSMutableArray *)symbolsArray {
   NSString *symbolString = [symbolsArray mj_JSONString];
    [[NSUserDefaults standardUserDefaults] setObject:symbolString forKey:@"allSymbolsStringKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setKlineIndex:(NSString *)klineIndex {
    [KDetail saveValeu:klineIndex forKey:@"klineIndexKey"];
}

- (void)setHomeIndex:(NSString *)homeIndex {
    [KDetail saveValeu:homeIndex forKey:@"homeIndexKey"];
}

- (void)setKlineMainIndex:(NSString *)klineMainIndex {
    [KDetail saveValeu:klineMainIndex forKey:@"klineMainIndexKey"];
}

- (void)setKlineAccessoryIndex:(NSString *)klineAccessoryIndex {
    [KDetail saveValeu:klineAccessoryIndex forKey:@"klineAccessoryIndexKey"];
}

- (NSString *)klineIndex {
    NSInteger klineValue = [[KDetail getValueForKey:@"klineIndexKey"] integerValue];
    if (klineValue == 0) {
        return @"4";
    } else {
        return [KDetail getValueForKey:@"klineIndexKey"];
    }
}

- (NSString *)homeIndex {
    NSInteger klineValue = [[KDetail getValueForKey:@"homeIndexKey"] integerValue];
    if (klineValue == 0) {
        return @"1";
    }
    return [KDetail getValueForKey:@"homeIndexKey"];
}

- (NSString *)klineMainIndex {
    NSInteger index = [[KDetail getValueForKey:@"klineMainIndexKey"] integerValue];
    if (index == 0) {
        return @"103";
    } else {
        return [KDetail getValueForKey:@"klineMainIndexKey"];
    }
}

- (NSString *)klineAccessoryIndex {
    NSInteger index = [[KDetail getValueForKey:@"klineAccessoryIndexKey"] integerValue];
    if (index == 0) {
        return @"102";
    } else {
        return [KDetail getValueForKey:@"klineAccessoryIndexKey"];
    }
}

#pragma mark 存取方法
- (id)getValueForKey:(NSString*)key{
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return value;
}

- (void)saveValeu:(id)value forKey:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



@end
