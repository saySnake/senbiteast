//
//  NotificationManager.m
//  iOS
//
//  Created by iOS on 2018/7/15.
//  Copyright © 2018年 iOS. All rights reserved.
//

#import "NotificationManager.h"

@implementation NotificationManager


#pragma mark - 1. 发送登录成功通知
+ (void)postLoginInSuccessNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:Login_In_NotificationName object:nil];
}

#pragma mark - 2. 发送退出登录成功通知
+ (void)postLoginOutSuccessNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:Login_Out_NotificationName object:nil];
}

#pragma mark - 3. 发送切换币对通知
+ (void)homePostSwitchTradeSymbolNotification:(NSDictionary *)dic {
    [IWNotificationCenter postNotificationName:Switch_TradeSymbol_NotificationName object:nil userInfo:dic];
}

#pragma mark 3.3 k线交易切换币对通知 //首页
+ (void)tradePostSwitchTradeSymbolNotification:(NSDictionary *)dic {
    [IWNotificationCenter postNotificationName:KnotificationExchangeSelected object:nil userInfo:dic];
}
#pragma mark 3.4 k线交易切换币选中通知 //交易或者k线
+ (void)tradeSelectedSwitchTradeSymbolNotification:(NSDictionary *)dic {
    [IWNotificationCenter postNotificationName:KnotificationSelected object:nil userInfo:dic];
}



#pragma mark - 4. 发送来网通知
+ (void)postComeNetNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:ComeNet_NotificationName object:nil];
}

 #pragma mark - 5. 发送app从后台进入前台通知
+ (void)postApplicationEnterForegroundNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:ApplicationEnterForegroundNotificationName object:nil];
}

#pragma mark - 7. 币对列表需要更新通知
+ (void)postSymbolListNeedUpdateNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:SymbolList_NeedUpdate_NotificationName object:nil];
}

/**
 11. 【合约】交易切换币对通知
 */
+ (void)postSwitchContractTradeSymbolNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:Switch_ContractTradeSymbol_NotificationName object:nil];
}


/**
12.添加自选通知
 */
+ (void)postAddFaviroutrNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:AddFavorioutficationNmae object:nil];

}
@end
