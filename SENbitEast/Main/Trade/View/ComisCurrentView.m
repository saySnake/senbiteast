//
//  ComisCurrentView.m
//  SENbitEast
//
//  Created by 张玮 on 2021/5/6.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "ComisCurrentView.h"
#import "ComisCurrentCell.h"
@interface ComisCurrentView () <UITableViewDataSource, UITableViewDelegate> {
    dispatch_queue_t serialQueue;
}

@property (strong, nonatomic) XXWebQuoteModel *webModel;

@property (strong, nonatomic) UITableView *tableView;

@end


@implementation ComisCurrentView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {

    [self addSubview:self.tableView];
    [self.tableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *v = nil;
    if (section == 0) {
        v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 80)];
        [v setBackgroundColor:[UIColor grayColor]];
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 200.0f, 30.0f)];
        [labelTitle setBackgroundColor:[UIColor clearColor]];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        labelTitle.text = @"第一个section 定制页面";
        [v addSubview:labelTitle];
    }
    return v;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 80;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ComisCurrentCell *cell = [ComisCurrentCell cellWithTbaleView:tableView];
    return cell;
}

#pragma mark - || 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 20 * [ComisCurrentCell ComisCurrentCellHeight]) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor purpleColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

@end
