//
//  AssertCell.m
//  SENbitEast
//
//  Created by 张玮 on 2021/2/22.
//  Copyright © 2021 张玮. All rights reserved.
//

#import "AssertCell.h"
#import "NSString+SuitScanf.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"

@implementation AssertCell
+ (instancetype)cellWithTbaleView:(UITableView *)tableView {
    static NSString *ID = @"AssertCell";
    AssertCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[AssertCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.cornerRadius = 5;
    }
    return cell;
}

- (void)dealloc {
    [IWNotificationCenter removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = WhiteColor;
        self.backgroundColor = WhiteColor;
        [self setUpView];
        [IWNotificationCenter addObserver:self selector:@selector(changeLanguage) name:ChangeLanguageNotificationName object:nil];
    }
    return self;
}

- (void)changeLanguage {
    self.leftTitle.text = kLocalizedString(@"assert_ableuse");
    self.centerTitle.text = kLocalizedString(@"assert_frozen");
    self.rightTitle.text = kLocalizedString(@"assert_cny");
}


- (void)setUpView {
    [self.contentView addSubview:self.img];
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(17);
        make.height.mas_equalTo(17);
    }];
    
    [self.contentView addSubview:self.coinName];
    [self.coinName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.img);
        make.width.mas_lessThanOrEqualTo(100);
        make.left.mas_equalTo(self.img.mas_right).offset(5);
        make.height.mas_equalTo(18);
    }];
    
    
    [self.contentView addSubview:self.leftTitle];
    [self.leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.img.mas_bottom).offset(16);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(16);
    }];

    [self.contentView addSubview:self.centerTitle];
    [self.centerTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.leftTitle.mas_top);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(16);
    }];

    [self.contentView addSubview:self.rightTitle];
    [self.rightTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(self.centerTitle.mas_top);
        make.width.mas_lessThanOrEqualTo(180);
        make.height.mas_equalTo(16);
    }];

    [self.contentView addSubview:self.leftLb];
    [self.leftLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.leftTitle.mas_bottom).offset(16);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(14);
    }];

    [self.contentView addSubview:self.centerLb];
    [self.centerLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.leftLb.mas_top);
        make.width.mas_lessThanOrEqualTo(150);
        make.height.mas_equalTo(14);
    }];

    [self.contentView addSubview:self.rightLb];
    [self.rightLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(self.centerLb.mas_top);
        make.width.mas_lessThanOrEqualTo(120);
        make.height.mas_equalTo(14);
    }];
}

- (void)setModel:(BalanceCoinsModel *)model {
    _model = model;
    self.coinName.text = model.ID;
    self.leftLb.text = [NSString stringWithFormat:@"%.8f",model.balance.floatValue];
    self.centerLb.text = [NSString stringWithFormat:@"%.8f",model.freezed.floatValue];;
    NSString *add = [NSString addV1:model.balance v2:model.locked v3:model.freezed];
    NSString *total = [NSString mulV1:add v2:model.usdtRate];
    self.rightLb.text = [[RatesManager sharedRatesManager] getRateAssertCellValue:[total floatValue]];
    NSString *url = [NSString stringWithFormat:@"%@%@",EastFiels,model.coinIcon];
    [self.img sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil];
}

- (UIImageView *)img {
    if (!_img) {
        _img = [[UIImageView alloc] init];
        _img.backgroundColor = [UIColor clearColor];
    }
    return _img;
}

- (BaseLabel *)coinName {
    if (!_coinName) {
        _coinName = [[BaseLabel alloc] init];
        _coinName.textColor = ThemeGreenColor;
        _coinName.font = BOLDSYSTEMFONT(20);
    }
    return _coinName;
}

- (BaseLabel *)leftTitle {
    if (!_leftTitle) {
        _leftTitle = [[BaseLabel alloc] init];
        _leftTitle.text = kLocalizedString(@"assert_ableuse");
        _leftTitle.textAlignment = 0;
        _leftTitle.textColor = ThemeLightGrayColor;
        _leftTitle.adjustsFontSizeToFitWidth = YES;
    }
    return _leftTitle;
}

- (BaseLabel *)centerTitle {
    if (!_centerTitle) {
        _centerTitle = [[BaseLabel alloc] init];
        _centerTitle.text = kLocalizedString(@"assert_frozen");
        _centerTitle.textAlignment = 0;
        _centerTitle.textColor = ThemeLightGrayColor;
    }
    return _centerTitle;
}

- (BaseLabel *)rightTitle {
    if (!_rightTitle) {
        _rightTitle = [[BaseLabel alloc] init];
        _rightTitle.text = kLocalizedString(@"assert_cny");
        _rightTitle.textAlignment = 2;
        _rightTitle.textColor = ThemeLightGrayColor;
    }
    return _rightTitle;
}

- (BaseLabel *)leftLb {
    if (!_leftLb) {
        _leftLb = [[BaseLabel alloc] init];
        _leftLb.textAlignment = 0;
        _leftLb.text = @"0.00000000";
        _leftLb.font = TwelveFontSize;
        _leftLb.adjustsFontSizeToFitWidth = YES;
        _leftLb.textColor = HEXCOLOR(0x333333);
    }
    return _leftLb;
}

- (BaseLabel *)centerLb {
    if (!_centerLb) {
        _centerLb = [[BaseLabel alloc] init];
        _centerLb.text = @"0.00000000";
        _centerLb.textAlignment = 0;
        _centerLb.font = TwelveFontSize;
        _centerLb.adjustsFontSizeToFitWidth = YES;
        _centerLb.textColor = HEXCOLOR(0x333333);
    }
    return _centerLb;
}

- (BaseLabel *)rightLb {
    if (!_rightLb) {
        _rightLb = [[BaseLabel alloc] init];
        _rightLb.text = @"0.00";
        _rightLb.textAlignment = 2;
        _rightLb.font = TwelveFontSize;
        _rightLb.adjustsFontSizeToFitWidth = YES;
        _rightLb.textColor = HEXCOLOR(0x333333);
    }
    return _rightLb;
}


@end
